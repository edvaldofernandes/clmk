({
	handleLoadPlanningReportInfo : function(component, event, helper) {
		var loadedPlanningReportInfo = event.getParam("PlanningInfo");
        component.set("v.PlanningInfo" , loadedPlanningReportInfo);
	}
})