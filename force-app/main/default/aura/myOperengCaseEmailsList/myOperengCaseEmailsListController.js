({
    doInit : function(component, event, helper) {
        // Prepare the action to load case record
        var action = component.get("c.getCaseEmails");
        action.setParams({"caseId": component.get("v.recordId")});
        
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                
                //

                var emails = response.getReturnValue();

                for(var i = 0; i < emails.length; i++){                 
                    if(emails[i].HtmlBody != null){      
                    	var limit_email = emails[i].HtmlBody;
                    	limit_email = limit_email.indexOf("--------------- Original Message ---------------");
                    	if(limit_email != -1){
                     	   emails[i].LastEmail = emails[i].HtmlBody.substring(0, limit_email);
                     	   emails[i].OthersEmails = emails[i].HtmlBody.substring(limit_email);
                  	  	}else{
                    		emails[i].LastEmail = emails[i].HtmlBody;
                    	    emails[i].OthersEmails = "";
                   	 	}   
                   }else{
                   		emails[i].HtmlBody = emails[i].TextBody;
                   }
                }
                
                //
              
                component.set("v.email", emails);
                //component.set("v.email", response.getReturnValue());
                var res = response.getReturnValue();
                if(!res || res.length === 0){
                    component.set("v.noEmailsText", 'No emails to be displayed.');
                } else {
                    component.set("v.noEmailsText", ' ');
                }
                
                component.set("v.initDone", true);
        		$A.util.addClass(spinner, "slds-hide");
                
				var action2 = component.get("c.getEmailAttachments");
                action2.setParams({"caseEmails": component.get("v.email")});
                
                // Configure response handler
                action2.setCallback(this, function(response2) {
                    var state2 = response2.getState();
                    if(state2 === "SUCCESS") {
                        component.set("v.emailAttachment", response2.getReturnValue());                
                    } else {
                        console.log('Problem getting attachments, response state: ' + state2);
                    }
                });
                $A.enqueueAction(action2);                
            } else {
                console.log('Problem getting case, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    toggleAccordion: function(component, event, helper) {
        var obj = event.currentTarget.id;
        var secName = 'section'+obj;
        var varSection = document.getElementById(secName);
        $A.util.toggleClass(varSection, 'slds-is-open');       
    },
    
    showOthersEmails: function(component, event, helper){
        var classComponent = document.getElementById('email-' + event.currentTarget.id);
        if(classComponent.className == "invisible"){
            classComponent.className = "visible";
            document.getElementById(event.currentTarget.id).innerHTML = "Hide previous emails";
        }else{
            classComponent.className = "invisible";
            document.getElementById(event.currentTarget.id).innerHTML = "View previous emails";
        } 
    }
    
})