({
    doInit : function(component, event, helper) {
        // Prepare the action to load case record
        var action = component.get("c.getCaseComments");
        action.setParams({"caseId": component.get("v.case.Id")});
        
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.comment", response.getReturnValue());                
            } else {
                console.log('Problem getting case, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    toggleAccordion: function(component, event, helper) {
        var obj = event.currentTarget.id;
        var secName = 'section'+obj;
        var varSection = document.getElementById(secName);
        $A.util.toggleClass(varSection, 'slds-is-open');       
    }
    
})