({
    navSelected : function(component, event, helper) {
        var id = event.currentTarget.id;
        if (id) {
            component.getSuper().navigate(id);
        }
    },
    toggleVerticalNav : function(component, event, helper) {
        var toggle = component.get("v.toggleNav");
        if(toggle){
            component.set("v.menuLabel", "Menu");
            component.set("v.homeLabel", "Cases");
            component.set("v.toggleNav", false);
        } else {
            component.set("v.menuLabel", "");
            component.set("v.homeLabel", "");
            component.set("v.toggleNav", true);
        }
    }
 })