({
	handleEvent : function(component, event, helper) {
		var record = event.getParam("PlanningReportRecord");
        component.set("v.Record", record);
        var region = record.summaryRecord.Region_and_Segment__c.substring(0,3);
        component.set("v.Region", region);
	}
})