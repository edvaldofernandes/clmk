({
  fetchRecord: function (component, event, helper) {
    var recordId = component.get("v.recordId");
    var getOOSRecords = component.get("c.getOOSRecords");
    getOOSRecords.setParams({ currentId: recordId });

    getOOSRecords.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.records", response.getReturnValue());
        component.set("v.doneRendering", true);
      } else {
        console.log(response.getError()[0]);
      }
    });
    $A.enqueueAction(getOOSRecords);

    var getProfileName = component.get("c.getProfileName");
    getProfileName.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var profileName = response.getReturnValue();
        helper.setComponentsByProfile(profileName, component);
      } else {
        alert("Error to get profile name!");
      }
    });
    $A.enqueueAction(getProfileName);
  }
});