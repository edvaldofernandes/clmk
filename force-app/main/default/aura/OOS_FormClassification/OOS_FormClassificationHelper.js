({
  setComponentsByProfile: function (profileName, component) {
    var title, firstColumn2dElements, secondColumn2dElements, column1dElements;
    if (profileName.includes("Service & Support Representative")) {
      title = "Time Breakdown";
      firstColumn2dElements = [
        "Parts_Unavailability__c",
        "Customer_Operation__c",
        "Time_to_Receive_Supplier_Disposition__c"
      ];
      secondColumn2dElements = [
        "Time_to_Receive_Embraer_Disposition__c",
        "Expected_Time_for_Troubleshooting__c",
        "Others__c"
      ];
      column1dElements = ["TechRep_Comments__c"];
    } else if (profileName.includes("Customer Data Reader")) {
      title = "Troubleshooting Analysis";
      firstColumn2dElements = [];
      secondColumn2dElements = [];
      column1dElements = ["EMIT_Comments__c", "RTS_Comments__c"];
    } else if (
      profileName.includes("Quality for Chatter Only") ||
      profileName.includes("Quality Full")
    ) {
      title = "Quality Analysis";
      firstColumn2dElements = ["Quality_Item_Classification__c"];
      secondColumn2dElements = ["Quality_Investigation_Status__c"];
      column1dElements = [
        "Corrective_Action_Document__c",
        "Quality_Comments__c"
      ];
    }

    component.set("v.title", title);
    component.set("v.firstColumn2dElements", firstColumn2dElements);
    component.set("v.secondColumn2dElements", secondColumn2dElements);
    component.set("v.column1dElements", column1dElements);
  }
});