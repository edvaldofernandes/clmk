({
    doInit : function(component, event, helper){ 
        helper.loadContacts(component, event, helper);
    },
    sendEmails : function(component, event, helper){
        var sendButton = component.find("sendButton");
		sendButton.set("v.disabled", "true");
        helper.sendFupDigest(component, event, helper);
    },
})