({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: 'Models', fieldName: 'Model_Name__c', type: 'text', editable: false, typeAttributes: { required: true }},
            {label: 'Firm', fieldName: 'Firm__c', type: 'number', editable: true},
            {label: 'Option', fieldName: 'Option__c', type: 'number', editable: true},
            {label: 'Purchase Right', fieldName: 'Purchase_Right__c', type: 'number', editable: true}
        ]); 
		helper.fetchData(cmp);
    },
    handleSaveEdition: function (cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log(draftValues);
        helper.saveEdition(cmp, draftValues);
        cmp.set('v.draftValues',[]);
        helper.fetchData(cmp);
    },
    handleCancelEdition: function (cmp) {
        // do nothing for now...
    }
});