({
	doInit: function(component, event, helper){
        var date = new Date(component.get('v.eod.Expiry_Date__c'));
        var d = new Date();
 
        if(d.getFullYear() > date.getFullYear()){// se o ano atual for maior que o ano de expirar a EOD
            component.set('v.valid', false);
        }else if(d.getFullYear() < date.getFullYear()){//  se o ano atual for menor que o ano de expirar a EOD
            component.set('v.valid', true);
        }else if(d.getMonth() > date.getMonth() || (d.getMonth() == date.getMonth && d.getDay() > date.getDay())){// se o ano atual for igual ao ano de expirar a EOD, faz as verificações com os meses e os dias
            component.set('v.valid', false);
        }else{// se o ano atual for igual ao da EOD mas nao ter chego ainda no dia de expirar
    		component.set('v.valid', true);
		}
        
        var action = component.get("c.getCaseNumber");
        action.setParams({"id": component.get('v.eod.Related_Case__c')});
        action.setCallback(this, function(result) {
            var state = result.getState();
            if (state === "SUCCESS") {
                component.set("v.CaseNumber", result.getReturnValue());
            }
             else if (state === "ERROR") {
                console.log("unknown error");
           }
        });
        $A.enqueueAction(action);

     
    },
	navigateToEod : function(component, event, helper) {
		var eod =component.get('v.eod');
        var sObjectEvent= $A.get("e.force:navigateToSObject");
        //console.log(eod); (there is just in fup, not in case)
        sObjectEvent.setParams({
            "recordId": eod.Id,
            "slideDevName": "datail"
        })
        sObjectEvent.fire();
	}
})