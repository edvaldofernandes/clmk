({
    doInit : function(component, event, helper){
        helper.loadUpdates(component, event, helper);
        setTimeout($A.getCallback(
            () => component.set("v.openSection", "last_update")
        ));
    }
})