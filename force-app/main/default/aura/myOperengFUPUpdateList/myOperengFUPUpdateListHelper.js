({
    loadUpdates : function(component, event, helper) {
        var action = component.get("c.getUpdates");
        action.setParams({ fupId : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.updates', result);
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    setSectionActive : function(component, event, helper, section) {
        component.find("accordion").set('v.activeSectionName', 'last_update');
    }
})