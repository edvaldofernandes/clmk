({
	handleEvent : function(component, event, helper) {
		var records = event.getParam("PlanningReportRecords");
        component.set("v.StockInfos", records);
        var apiName = event.getParam("APIName");
        component.set("v.APIName", apiName);
        var label = event.getParam("Label");
        component.set("v.Label", label);
	}
})