({
    initHelper : function(component) {
               
        var action = component.get("c.getCaseRecordType");
        action.setParams({"caseId" : component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            
            if (response.getState() === "SUCCESS") {
                const caso = response.getReturnValue();
				component.set('v.recordTypeName', caso.RecordType.Name);
                component.set('v.recordTypeDescription', caso.RecordType.Description);
            }
        });
        
        $A.enqueueAction(action);
    }
})