({
    initHelper : function(component) {
        
        var that = this;
        var caseRecordTypeDeveloperName = component.get("v.caseRecordTypeDeveloperName");
        var caseStatus = component.get("v.caseStatus");
        var caseRegion = component.get("v.caseRegion");
        var defaultAccountName = component.get("v.defaultAccountName");
        
        var action = component.get("c.getCases");
        action.setParams({"caseRecordTypeDeveloperName" : caseRecordTypeDeveloperName,
                          	"caseStatus" : caseStatus,
                          	"caseRegion" : caseRegion,
                         	"defaultAccountName" : defaultAccountName});
        
        action.setCallback(this, function(response) {
            
            if (response.getState() === "SUCCESS") {

                const data = response.getReturnValue();
                const dataSize = data.length;
                let markers = [];
                for(let i=0; i < dataSize; i += 1) {
                    const Case = data[i];
                    
                    markers.push({
                        'location': {
                            'City' : Case.Account.BillingCity,
                            'Country' : Case.Account.BillingCountry,
                            'Latitude' : Case.Account.BillingLatitude,
                            'Longitude' : Case.Account.BillingLongitude
                        },
                        'icon': Case.Status__c,
                        'title' : Case.CaseNumber + ' | ' + Case.Account.Name + ' | ' + Case.Study_Type__c,
                        /*'description' : '<b>#: </b>' + Case.CaseNumber + '<br/>' +
                        '<b>Airline: </b>' + Case.Account.Name + '<br/>' +
                        '<b>Owner: </b>' + Case.Owner.Name + '<br/>' +
                        '<b>Due Date: </b>' + Case.Due_Date__c + '<br/>' +
                        '<b>Study Type(s): </b>' + Case.Study_Type__c + '<br/>' +
                        '<b>Subject: </b>' + Case.Subject + '<br/>' +
                        '<br/>' +
                        '<a href="' + Case.SuppliedName + '/' + Case.Id + '" target="_blank">Go to Case</a>'*/
                        
                        'description' : 'Owner: ' + Case.Owner.Name + ' | ' +
                        'Due Date: ' + Case.Due_Date__c + ' | ' +
                        'Subject: ' + Case.Subject
                    });
                }
                component.set('v.mapMarkers', markers);
            }
        });
        
        $A.enqueueAction(action);
    }
})