({  
	//================================================================================================================================
    //================================================================================================================================
    initValues : function (component, helper) {
        component.set("v.searchText", "");
        component.set("v.showOpportunityActions", true);
    },
    
	//================================================================================================================================
    //================================================================================================================================
    initOpportunityItems : function (component, helper) {       
		let soql =  "SELECT Id, Aircraft_Availability__c, Aircraft__c, " + 
            			"Version__c, Engines__c, LOPA__c, Certification__c, " + 
            			"Operator__r.Id, Operator__r.Name, Owner__r.Id, Owner__r.Name, " +
            			"Vintage__c " +
                    "FROM Aircraft_Availability_Item__c " +
                    "WHERE Aircraft_Availability__r.Deal_Status__c = 'Open' " +
                    "ORDER BY Aircraft_Availability__c ASC, Serial_Number__r.Name ASC ";
        
        helper.soql(component, soql)
        .then(function (opportunityItems) {
            helper.initOpportunities(component, helper, opportunityItems);
		 })
        .catch(function (error) {
            console.log("SOQL: " + error);
        })
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initOpportunities : function (component, helper, opportunityItems) {
        let soql = "SELECT Id, Rating__c, Aircraft__c, Quantity__c, Status__c, Is_Recent__c, Comments__c, RVG__c, Due_Date__c, Availability__c " +
            		"FROM Aircraft_Availability__c " +
            		"WHERE Deal_Status__c = 'Open' " +
            		"ORDER BY Id ASC";
        
        helper.soql(component, soql)
        .then(function (opportunities) {
            
            let fields = {
                "List" : [
                    {"source": "Version__c", 		"destination": "Variant", 		"separator" : ", "},
                    {"source": "Engines__c", 		"destination": "Engines", 		"separator" : ", "},
                    {"source": "LOPA__c", 			"destination": "LOPA", 			"separator" : ", \n"},
                    {"source": "Certification__c",	"destination": "Certification",	"separator" : ", "},
                ],
                "First" : [
                    {"source": "Operator__r", 		"destination": "Operator"},
                    {"source": "Owner__r", 			"destination": "Lessor"},
                ],
                "Range" : [
                    {"source": "Vintage__c", 		"destination": "Vintage"},
                ]
			};            
            
            opportunities.forEach(function(opportunity) {
                  
                switch (opportunity["Rating__c"] || "") {
                    case "1":
                		opportunity["RatingDescription"] = "HIGH Priority for both Embraer and Leasing Company";
                    	break;
                    case "2":
                		opportunity["RatingDescription"] = "MEDIUM Priority for both Embraer and Leasing Company";
                    	break;
                    case "3":
                		opportunity["RatingDescription"] = "AVERAGE Priority for both Embraer and Leasing Company";
                    	break;
                    default:
                    	opportunity["RatingDescription"] = "";
                    	break;
                }
                
                opportunity["RVG"] = opportunity.RVG__c ? 1 : 2;
                opportunity["RVGText"] = opportunity.RVG__c ? "RVG" : "";
                
                opportunity["AvailabilityIndex"] = opportunity.Availability__c == "NOW" ? 1 : (opportunity.Availability__c == "TBD" ? 3 : 2);
                opportunity["Quantity"] = opportunity.Quantity__c || -1;
  
                opportunityItems = helper.setOpportunityItems(helper, opportunity, opportunityItems, fields);  
                opportunity["Variant"] = (opportunity["Aircraft__c"] + " " + opportunity["Variant"]).trim();
                    
				opportunity["MasterSortKey"] =  opportunity.Rating__c + "|" + 
                    							opportunity.AvailabilityIndex + "|" +
                    							helper.getOpportunityFieldFormatted(9999-(opportunity.Quantity__c || 0)) + "|" +
                    							opportunity.Due_Date__c;
                
                opportunity["SearchKey"] =  opportunity.Variant + "|" +
                    						opportunity.Engines + "|" +
                    						opportunity.Quantity__c + "|" +
                    						opportunity.Status__c + "|" +
                                            opportunity.Operator + "|" +
                                            opportunity.Lessor + "|" +
                    						opportunity.Availability__c + "|" +
                                            opportunity.Comments__c + "|" +
                                            opportunity.Vintage + "|" +
                                            opportunity.LOPA + "|" +
                                            opportunity.Certification + "|" +
                                        	opportunity.RVGText;
            });

            component.set("v.opportunities", opportunities);
            helper.sortByField(component, helper, "MasterSortKey", false);
            
            helper.initRatings(component);
            helper.initAircrafts(component);
            helper.initCertifications(component);
            helper.initRVGs(component);
            helper.initLessors(component);
            helper.initVintages(component);
            
            helper.filterOpportunities(component);
		 })
        .catch(function (error) {
            console.log("SOQL: " + error);
        })
    },
    
    //================================================================================================================================
    //================================================================================================================================
    setOpportunityItems : function (helper, opportunity, opportunityItems, fields) {
        let unique = opportunityItems.filter(item => item.Aircraft_Availability__c == opportunity.Id && item.Aircraft__c == opportunity.Aircraft__c);
        
        helper.getOpportunityItemsList(opportunity, unique, fields.List);
        helper.getOpportunityItemsFirst(opportunity, unique, fields.First);
		helper.getOpportunityItemsRange(opportunity, unique, fields.Range);      
        
        opportunityItems = opportunityItems.filter(item => item.Aircraft_Availability__c != opportunity.Id);
        return opportunityItems;
    },
    
    //================================================================================================================================
    //================================================================================================================================
    getOpportunityItemsList : function (opportunity, opportunityItems, fields) {
        fields.forEach(function(field) {
            let items = opportunityItems
            	.map(item => item[field.source])
            	.filter((value, index, self) => self.indexOf(value) === index)
            	.filter(n => n) 
            	|| "";
            
            items = items.toString().replaceAll(",", field.separator);
            opportunity[field.destination] = items;
        });
    },
    
    //================================================================================================================================
    //================================================================================================================================
    getOpportunityItemsFirst : function (opportunity, opportunityItems, fields) {
        let opportunityItem = opportunityItems[0] || "";
        fields.forEach(function(field) {
            opportunity[field.destination] = (opportunityItem[field.source + "_Name"] || "").replace(/(”|“)/gm, '"').replace(/(‘|’)/gm, "'");
            opportunity[field.destination + "Id"] = opportunityItem[field.source + "_Id"];
        });
    },
    
	//================================================================================================================================
    //================================================================================================================================
    getOpportunityItemsRange : function (opportunity, opportunityItems, fields) {
        fields.forEach(function(field) {
            
            let minimum = 0;
            let maximum = 0;

            if (opportunityItems.length >= 1) {
                minimum = opportunityItems[0][field.source];
                maximum = minimum;
                for (let i = 1; i < opportunityItems.length; i++) {
                    let value = opportunityItems[i][field.source];
                    minimum = (value < minimum) ? value : minimum;
                    maximum = (value > maximum) ? value : maximum;
                }
            }
                        
            opportunity[field.destination + "From"] = minimum;
            opportunity[field.destination + "To"] = maximum;
            
            if (minimum == 0 && maximum == 0) {
                opportunity[field.destination] = "";
            }
            else if (minimum == maximum) {
                opportunity[field.destination] = "" + minimum;
            }
                else {
                    opportunity[field.destination] = minimum + "-" + maximum;
                }
        });
    },
   
    //================================================================================================================================
    //================================================================================================================================
    filterOpportunities : function (component) {
        let opportunities = component.get("v.opportunities");
        let opportunitiesFiltered = opportunities.filter(opportunity => {
			return  this.hasFilterBySearchText(component, opportunity) &&
            		this.hasFilterByRating(component, opportunity) &&
            		this.hasFilterByAircraft(component, opportunity) &&
            		this.hasFilterByCertification(component, opportunity) &&
            		this.hasFilterByRVG(component, opportunity) &&
            		this.hasFilterByLessor(component, opportunity) &&
            		this.hasFilterByVintage(component, opportunity);
        });
        this.setOpportunitiesSummary(component, opportunitiesFiltered);
        component.set("v.opportunitiesFiltered", opportunitiesFiltered);
        //component.set("v.opportunitiesLoaded", opportunitiesFiltered);
        component.set("v.opportunitiesLoaded", opportunitiesFiltered.slice(0, 10));
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterBySearchText : function (component, opportunity) {
        let searchText = component.get("v.searchText") || "";
        let result = true;
        if (searchText != "") {
            result = this.includedInSearch(opportunity, searchText);
        }
        return result;
    },
        
	//================================================================================================================================
    //================================================================================================================================
	includedInSearch : function (item, searchText) {
		return item["SearchKey"].toLowerCase().includes(searchText.toLowerCase());
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    hasFilterByRating : function (component, opportunity) {
        let rating = component.get("v.opportunityRating") || "";
        if (rating != "") {
            return opportunity.Rating__c == rating;
        }
        return true;
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    hasFilterByAircraft : function (component, opportunity) {
        let aircraft = component.get("v.opportunityAircraft") || "";
        if (aircraft != "") {
            return opportunity.Aircraft__c == aircraft;
        }
        return true;
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    hasFilterByCertification : function (component, opportunity) {
        let certification = component.get("v.opportunityCertification") || "";
        if (certification != "") {
            return opportunity.Certification.toUpperCase().includes(certification);
        }
        return true;
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    hasFilterByRVG : function (component, opportunity) {
        let rvg = component.get("v.opportunityRVG") || "";
        if (rvg != "") {
            return opportunity.RVG == rvg;
        }
        return true;
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    hasFilterByLessor : function (component, opportunity) {
        let lessor = component.get("v.opportunityLessor") || "";
        let lessors = component.get("v.opportunityLessors");
        if (lessor != "") {
            if(lessor.substring(0, 3) == "001") {
                return opportunity.LessorId == lessor;
			}
            else if(lessor == "OWNED") {
                return opportunity.LessorId == opportunity.OperatorId;
            }
            return  opportunity.LessorId != opportunity.OperatorId &&
                	!lessors.some(item => opportunity.LessorId === item.value);
        }
        return true;
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    hasFilterByVintage : function (component, opportunity) {
        let vintageFrom = component.get("v.opportunityVintageFrom") || -1;
        let vintageTo = component.get("v.opportunityVintageTo") || -1;
        
        vintageTo = vintageTo == -1 ? 9999 : vintageTo;
        
        if (vintageFrom == -1 && vintageTo == 9999) {
            return true;
        }
        else if (vintageFrom <= vintageTo && opportunity.VintageFrom != 0) {
            return (opportunity.VintageFrom >= vintageFrom && opportunity.VintageFrom <= vintageTo) ||
                	(opportunity.VintageTo >= vintageFrom && opportunity.VintageTo <= vintageTo)
        }
        return false;
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initRatings : function (component) {
        let ratings = [];
		ratings.push({"label": "-- ALL --", "value": ""});
        ratings.push({"label": "1", "value": "1"});
        ratings.push({"label": "2", "value": "2"});
        ratings.push({"label": "3", "value": "3"});
        component.set("v.opportunityRatings", ratings);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initAircrafts : function (component) {
        let opportunities = component.get("v.opportunities");
        let aircrafts = [];

        aircrafts = opportunities.map((opportunity) => {
            return {
            	label: opportunity.Aircraft__c,
            	value: opportunity.Aircraft__c,
            	compare: (!opportunity.Aircraft__c.includes("E2") ? "1" : "2") + "|" + opportunity.Aircraft__c
        	}
		});
        
        aircrafts = aircrafts.filter((aircraft, index) => {
            const _aircraft = JSON.stringify(aircraft);
            return index === aircrafts.findIndex(item => {
            	return JSON.stringify(item) === _aircraft;
            });
        });
        
        aircrafts.sort(function(aircraft1, aircraft2) {
    		return aircraft1.compare.localeCompare(aircraft2.compare);
        });
    
        aircrafts.unshift({"label": "-- ALL --", "value": ""});	
        component.set("v.opportunityAircrafts", aircrafts);
    },
        
	//================================================================================================================================
    //================================================================================================================================
    initCertifications : function (component) {
        let certifications = [];
		certifications.push({"label": "-- ALL --", "value": ""});
        certifications.push({"label": "ANAC", "value": "ANAC"});
        certifications.push({"label": "CASA", "value": "CASA"});
        certifications.push({"label": "EASA", "value": "EASA"});
        certifications.push({"label": "FAA",  "value": "FAA"});
        certifications.push({"label": "TCCA", "value": "TCCA"});
        component.set("v.opportunityCertifications", certifications);
    },
        
	//================================================================================================================================
    //================================================================================================================================
    initRVGs : function (component) {
        let rvgs = [];
		rvgs.push({"label": "-- ALL --", "value": ""});
        rvgs.push({"label": "Yes", "value": "1"});
        rvgs.push({"label": "No", "value": "2"});
        component.set("v.opportunityRVGs", rvgs);
    },
        
	//================================================================================================================================
    //================================================================================================================================
    initLessors : function (component) {
        let lessors = [];
		lessors.push({"label": "-- ALL --", "value": ""});
        lessors.push({"label": "FALKO", "value": "001i000001DUXjZAAX"});
        lessors.push({"label": "GECAS", "value": "001i000000uhYg0AAE"});
        lessors.push({"label": "NAC", "value": "001i000000uhYjUAAU"});
        lessors.push({"label": "TrueNoord", "value": "001i000001vrgubAAA"});
        lessors.push({"label": "Other Companies", "value": "OTHER"});
        lessors.push({"label": "Operator Owned", "value": "OWNED"});
        component.set("v.opportunityLessors", lessors);
    },
        
	//================================================================================================================================
    //================================================================================================================================
    initVintages : function (component) {
        let vintages = [];
        let opportunities = component.get("v.opportunities");
        
        let minimum = 0;        
        if (opportunities.length >= 1) {
            minimum = opportunities[0].VintageFrom;
            for (let i = 1; i < opportunities.length; i++) {
                let value = opportunities[i].VintageFrom || 9999;
                minimum = (value < minimum) ? value : minimum;
            }
        }
        minimum = Math.max(2004, minimum);
        let maximum = new Date().getFullYear();
               
        vintages.push({"label": "-- ALL --", "value": -1});
        for (let year = minimum; year <= maximum; year++) {
        	vintages.push({"label": year.toString(), "value": year});
        }
        
        component.set("v.opportunityVintageFroms", vintages);
        component.set("v.opportunityVintageTos", vintages);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    setOpportunitiesSummary : function (component, opportunities) {
        let year = component.get("v.currentYear");
        let opportunitiesSummary = {};
                
        opportunitiesSummary["Total"] = opportunities.length;
        
        opportunitiesSummary["Aircraft"] = 0;
        if (opportunities.length > 0) {
            opportunitiesSummary["Aircraft"] = opportunities.reduce(function(previous, current) {
                return previous + (current.Quantity__c || 0);
            }, 0);
        }
        
        component.set("v.opportunitiesSummary", opportunitiesSummary);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    sortByField : function (component, helper, field, fieldSort) {
        
        if (field == "" || field != component.get("v.sortField")) {
            fieldSort = false;
        }
        fieldSort = !fieldSort;
        
		component.set("v.sortField", field);
        component.set("v.sortIsAsc", fieldSort);
              
        let opportunities = component.get("v.opportunities");
        opportunities.sort(function(opportunity1, opportunity2) {
            let opportunity1Field = helper.getOpportunityField(helper, opportunity1, field, fieldSort);
            let opportunity2Field = helper.getOpportunityField(helper, opportunity2, field, fieldSort);
            if (!opportunity1[field]) {
                return 1;
            }
            else if (!opportunity2[field]) {
                return -1;
            }
			else {
                return opportunity1Field.localeCompare(opportunity2Field) * (fieldSort ? 1 : -1);
            };
        });
        component.set("v.opportunities", opportunities);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    getOpportunityField : function (helper, opportunity, field, fieldSort) {
        let opportunityField = helper.getOpportunityFieldFormatted(opportunity[field] || "");
		return opportunityField + "|" + opportunity.Id + "|1|";
    },
        
    //================================================================================================================================
    //================================================================================================================================
    getOpportunityFieldFormatted : function (field) {
        if (typeof field == "number") {
            field = (Array(6).join("0") + field).slice(-6);
        }
        return field;
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizeForUser : function (component, helper) {
        helper.customizeDevice(component);
        let user;
        helper.soql(component, "SELECT Id FROM GroupMember WHERE Group.DeveloperName = 'DIM_Aircraft_Availability_Management' AND UserOrGroupId = '" + $A.get("$SObjectType.CurrentUser.Id") + "' LIMIT 1")
        .then(function (users) {
            component.set("v.hasEditAccess", users.length > 0);
            component.set("v.showOpportunityActions", users.length > 0);
            helper.initOpportunityItems(component, helper);
        })
        .catch(function (error) {
            console.log("SOQL: " + error);
        })
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizeDevice : function (component) {
        let device = $A.get("$Browser.formFactor");
        let isTablet = $A.get("$Browser.isTablet");
        if (device == "PHONE" || isTablet) {
            component.set("v.isMobile", true);
            component.set("v.showOrdersByYear", false);
        }
    },
        
	//================================================================================================================================
    //================================================================================================================================
    viewRecord : function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": recordId,
          "slideDevName": "detail"
        });
        navEvt.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================
    editRecord : function(recordId) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
             "recordId": recordId
       });
        editRecordEvent.fire();
    },
        
	//================================================================================================================================
    //================================================================================================================================
    goToURL : function(url) {            
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},
    
    //================================================================================================================================
    //================================================================================================================================
    goToList : function (listViewId) {
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": listViewId,
            "scope": "Aircraft_Availability__c"
        });
        navEvent.fire();
    },
        
	//================================================================================================================================
    //================================================================================================================================   
    lazyLoadRecords : function (component) {
        let opportunitiesLoaded = this.loadMoreOpportunities(component);
        component.set("v.opportunitiesLoaded", opportunitiesLoaded);
    },
        
	//================================================================================================================================
    //================================================================================================================================   
    loadMoreOpportunities : function (component) {
        let opportunitiesFiltered = component.get("v.opportunitiesFiltered");
        let opportunitiesLoaded = component.get("v.opportunitiesLoaded");
        let opportunitiesSlice = opportunitiesFiltered.slice(opportunitiesLoaded.length, opportunitiesLoaded.length + 10);
        return opportunitiesLoaded.concat(opportunitiesSlice);
    },
})