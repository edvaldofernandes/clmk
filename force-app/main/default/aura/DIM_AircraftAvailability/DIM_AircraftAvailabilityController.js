({    
    //================================================================================================================================
    //================================================================================================================================
    init : function (component, event, helper) {
                
        let today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set("v.today", today);
        component.set("v.currentYear", new Date().getFullYear());
        
        helper.initValues(component, helper);
       
        helper.customizeForUser(component, helper);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeRating : function (component, event, helper) {
        let rating = event.getParam("value");
        component.set("v.opportunityRating", rating);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeAircraft : function (component, event, helper) {
        let aircraft = event.getParam("value");
        component.set("v.opportunityAircraft", aircraft);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeCertification : function (component, event, helper) {
        let certification = event.getParam("value");
        component.set("v.opportunityCertification", certification);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeRVG : function (component, event, helper) {
        let rvg = event.getParam("value");
        component.set("v.opportunityRVG", rvg);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeLessor : function (component, event, helper) {
        let lessor = event.getParam("value");
        component.set("v.opportunityLessor", lessor);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeVintageFrom : function (component, event, helper) {
        let vintageFrom = parseInt(event.getParam("value"));
        component.set("v.opportunityVintageFrom", vintageFrom);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeVintageTo : function (component, event, helper) {
        let vintageTo = parseInt(event.getParam("value"));
        component.set("v.opportunityVintageTo", vintageTo);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    search : function (component, event, helper) {
        helper.filterOpportunities(component);
    },

    //================================================================================================================================
    //================================================================================================================================
    sortByField : function(component, event, helper) {
        let field = event.target.id;
        let fieldSort = component.get("v.sortIsAsc");
       	helper.sortByField(component, helper, field, fieldSort);
    },
    
	//================================================================================================================================
    //================================================================================================================================
	handleOpportunityAction: function(component, event, helper) {
        let opportunity = event.getSource().get("v.value");
        let action = event.getParam("value");

        switch (action) {
            case "view":
                helper.viewRecord(opportunity.Id);
                break;
            case "edit":
                helper.editRecord(opportunity.Id);
                break;
        }
    },
    
    //================================================================================================================================
    //================================================================================================================================
    createOpportunity : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Aircraft_Availability__c"
        });
        createRecordEvent.fire();
	},
    
	//================================================================================================================================
    //================================================================================================================================   
    goToRecord : function (component, event, helper) {
        let id = event.currentTarget.dataset.value;
        if (id) {
        	helper.viewRecord(id);
        }
    },
    
    //================================================================================================================================
    //================================================================================================================================
    goToList : function (component, event, helper) {
        let listViewid = event.currentTarget.dataset.value;
        if (listViewid) {
        	helper.goToList(listViewid);
        }
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    changeTab : function (component, event, helper) {
        let tabId = event.getParam("id");
        component.set("v.selectedTabId", tabId);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    resetView : function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    showTabsFilters : function (component, event, helper) {
        let showTabsFilters = component.get('v.showTabsFilters');
        component.set('v.showTabsFilters', !showTabsFilters);
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    export : function (component, event, helper) {
        
        let aircraft = event.currentTarget.dataset.value;
        if (aircraft.toUpperCase().includes("ALL")) {
            aircraft = "All";
        }
        
        let details = {
            "urlPath" : $A.get('$Resource.DIM_Aircraft_Availability') + "/",
            "disclaimer" : {
                "name" : "Confidential",
                "color" : {"R": 244, "G": 130, "B": 50} //SECRET = 192,0,0
            },
            "columnsAutoWidth" : false,
            "columns" : [
                {"index": "0",	"title": "Variant", 		"value": "Variant", 			"width" : "10%"},
                {"index": "1",	"title": "Engines", 		"value": "Engines", 			"width" : "10%"},
                {"index": "2",	"title": "Quantity", 	 	"value": "Quantity__c", 		"width" : "9%"},
                {"index": "3",	"title": "Operator", 	 	"value": "Operator", 			"width" : "15%"},
                {"index": "4",	"title": "Owner", 		 	"value": "Lessor", 				"width" : "15%"},
                {"index": "5",	"title": "Due Date", 	 	"value": "Availability__c", 	"width" : "9%"},
                {"index": "6",	"title": "Vintage", 		"value": "Vintage", 			"width" : "10%"},
                {"index": "7",	"title": "LOPA", 		 	"value": "LOPA", 				"width" : "10%"},
                {"index": "8",	"title": "Certification",	"value": "Certification", 		"width" : "12%"},
            ]
		};

        let opportunities = component.get("v.opportunitiesFiltered");
        opportunities = opportunities.filter(opportunity => {
            let filterAircraft = aircraft == "All" ? true : opportunity.Aircraft__c == aircraft;
            let filterQuantity = opportunity.Quantity__c > 0;
            return filterAircraft && filterQuantity;
		});              
                
        if (opportunities.length > 0) {
			
			let header = details.columns.map(column => column.title);
			let body = {"E170": [], "E175": [], "E190": [], "E195": [], "E190-E2": [], "E195-E2": []};
        
    		opportunities.forEach(function(opportunity) {
                let values = [];
                details.columns.forEach(function(column) {
                	values.push(opportunity[column.value]);
                });
                body[opportunity.Aircraft__c].push(values);
            });
        
        	for (const [key, value] of Object.entries(body)) {
                if (value.length == 0) {
                    delete body[key];
                }
            }
        
            let content = {
                "head" : header,
                "body" : body
            };
			createPDF(content, details);
        }
        else {
			alert("There are no records to be exported.");
		}
    },
                
    //================================================================================================================================
    //================================================================================================================================   
    setExportOptions : function (component, event, helper) {
        component.set("v.isExportReady", true);
    },
})