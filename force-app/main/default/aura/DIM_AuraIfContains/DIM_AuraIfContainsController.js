({
	doInit: function(component, event, helper) {
        var searchText = component.get('v.searchText');
        var element = component.get('v.element');
        
        if (searchText) {
			component.set('v.condition', element.toLowerCase().includes(searchText.toLowerCase()));
        }
        else {
            component.set('v.condition', true);
        }
    }
})