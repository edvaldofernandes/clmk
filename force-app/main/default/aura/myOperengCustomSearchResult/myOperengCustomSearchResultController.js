({
	navigateToCase:function(component){
        var caseVar  = component.get("v.simpleRecord");
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": caseVar.Id,
            "slideDevName": "detail"
        });
        sObectEvent.fire(); 
    }
})