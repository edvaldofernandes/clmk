({
    refresh : function(component, event) {
        /* Essa função faz com que a url do iframe
         * (a que referencia o pdf gerado) seja 
         * reescrita. Com isso, a página é forçada a 
         * recarregar, dando um refresh na informação no 
         * documento. 
         * 
         * This function causes the iframe's url (to which
         * the generated pdf references) to be rewritten.
         * With this, the page is forced to reload,
         * refreshing the information in the document.
         * 
         */
        var url = component.get("v.baseUrl");
        var id = component.get("v.recordId");
        // For some reason 'url + id' works but
        // url + id don't.
        // 
        // Por alguma razão 'url + id' funciona mas url + id
        // não.
        component.set("v.url", 'url + id');  
    }
})