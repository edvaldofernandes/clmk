({
    doInit : function(component, event, helper){ 
        helper.loadContacts(component, event, helper);
        helper.loadPreview(component, event, helper);
    },
    changeContact : function(component, event, helper){
        var whoId = component.find("whoFilter").get("v.value");
        component.set('v.whoId', whoId);
        helper.loadPreview(component, event, helper);
    },
    navigateToView : function(component, event, helper) {
        $A.createComponent(
            "c:FO_FupDigestSendConfirmation",
            {},
            function(view) {
               var content = component.find("content");
               content.set("v.body", view);
        });
    }
})