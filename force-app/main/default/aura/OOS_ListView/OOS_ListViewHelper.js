({
  setComponentsByProfile: function (profileName, component) {
    var fields = {};
    var sortColumnNumber;
    if (profileName.includes("Service & Support Representative")) {
      fields = {
        Name: "Id",
        Operator__c: "Operator",
        Aircraft_Register__c: "Register",
        Start_Date__c: "Start Date",
        Release_Date__c: "Release Date",
        Event_Description__c: "Event Description",
        Action_Description__c: "Action Description",
        OOS_Total_Time__c: "OOS Total Time"
      };
      sortColumnNumber = 3;
    } else if (
      profileName.includes("Customer Data Reader") ||
      profileName.includes("Quality for Chatter Only") ||
      profileName.includes("Quality Full")
    ) {
      fields = {
        Name: "Id",
        Operator__c: "Operator",
        Aircraft_Register__c: "Register",
        ATA__c: "ATA Chapter",
        Technology__c: "Technology",
        Fail_Code__c: "Fail Code",
        Start_Date__c: "Start Date",
        OOS_Total_Time__c: "OOS Total Time"
      };
      sortColumnNumber = 6;
    }

    component.set("v.apiNames", Object.keys(fields));
    component.set("v.fieldNames", Object.values(fields));
    component.set("v.sortColumnNumber", sortColumnNumber);
  }
});