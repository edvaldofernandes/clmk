({
	goToApp : function(component, event, helper) {
		var navService = component.find("navService");
    	var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__PlanningReportLightning"
            }
        };
     	navService.navigate(pageReference);
	}
})