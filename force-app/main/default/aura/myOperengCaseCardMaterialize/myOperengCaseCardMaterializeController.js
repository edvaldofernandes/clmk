({   
    navigateToCase:function(component){
        var caseVar  = component.get("v.case");
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": caseVar.Id,
            "slideDevName": "detail"
        });
        sObectEvent.fire(); 
    }
})