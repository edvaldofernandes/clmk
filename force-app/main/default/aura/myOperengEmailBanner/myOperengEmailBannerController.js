({
    doInit : function(component, event, helper) {
        // Prepare the action to load case record
        var action = component.get("c.getEmailDetails");
        action.setParams({"emailId": component.get("v.recordId")});
        
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.email", response.getReturnValue());                
            } else {
                console.log('Problem getting case, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})