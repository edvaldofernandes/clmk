({
    handleRecordChanged: function(component, event, helper) {
        switch(event.getParams().changeType) {
            case "ERROR":
              	break;
            case "LOADED":
                var oldOwnerId = component.get("v.thisCase.OwnerId");
                var newOwnerId = component.get("v.currentUser.Id");
                if(oldOwnerId != newOwnerId){
                    component.set("v.thisCase.OwnerId", newOwnerId);
                    helper.saveRecordWithLDS(component);
                }
                break;
            case "REMOVED":
              	break;
            case "CHANGED":
              	break;
        }
    }
})