({
    doInit : function(component, event, helper) {
        var handleTimer = window.setInterval(
            $A.getCallback(function() {
                helper.activateSlide(component,helper);
            }), 30000
        );
        component.set("v.timer",handleTimer);
    },

    handleClick : function(component, event, helper) {
        if (component.get("v.isPaused")=="true"){
            component.set("v.timer",window.setInterval(
                $A.getCallback(function() {
                    helper.activateSlide(component,helper);
                }), 30000
            )
                         );
            component.set('v.buttonIcon','utility:pause');
            component.set("v.isPaused","false");
        } else {
            window.clearInterval(component.get("v.timer"));
            component.set("v.buttonIcon","utility:play");
            component.set("v.isPaused","true");
        }
    },

    handleClick1 : function(component, event, helper) {
        document.getElementById('carousel').setAttribute("style", "transform:translateX(-0%)");
        document.getElementById('header-carousel').setAttribute("style", "transform:translateX(-0%)");
        document.getElementById('indicator-id-01').setAttribute("class","slds-carousel__indicator-action slds-is-active");
        document.getElementById('indicator-id-02').setAttribute("class","slds-carousel__indicator-action");
        document.getElementById('indicator-id-03').setAttribute("class","slds-carousel__indicator-action");

        component.set("v.pannelDisplay",1);
    },

    handleClick2 : function(component, event, helper) {
        document.getElementById('carousel').setAttribute("style", "transform:translateX(-100%)");
        document.getElementById('header-carousel').setAttribute("style", "transform:translateX(-0%)");
        document.getElementById('indicator-id-01').setAttribute("class","slds-carousel__indicator-action");
        document.getElementById('indicator-id-02').setAttribute("class","slds-carousel__indicator-action slds-is-active");
        document.getElementById('indicator-id-03').setAttribute("class","slds-carousel__indicator-action");
        component.set("v.pannelDisplay",2);
    },

    handleClick3 : function(component, event, helper) {
        document.getElementById('carousel').setAttribute("style", "transform:translateX(-200%)");
        document.getElementById('header-carousel').setAttribute("style", "transform:translateX(-100%)");
        document.getElementById('indicator-id-01').setAttribute("class","slds-carousel__indicator-action");
        document.getElementById('indicator-id-02').setAttribute("class","slds-carousel__indicator-action");
        document.getElementById('indicator-id-03').setAttribute("class","slds-carousel__indicator-action slds-is-active");
        component.set("v.pannelDisplay",3);
    }
})