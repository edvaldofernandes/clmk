({
    activateSlide: function(component,helper) {
        var active = component.get("v.pannelDisplay");
        
        var carousel = document.getElementById('carousel');
        var headerCarousel = document.getElementById('header-carousel');
        
        if (active == 1){
            
            if(carousel !== null)
          		carousel.setAttribute("style", "transform:translateX(-100%)");
            
            document.getElementById('indicator-id-01').setAttribute("class","slds-carousel__indicator-action");
            document.getElementById('indicator-id-02').setAttribute("class","slds-carousel__indicator-action slds-is-active");
            document.getElementById('indicator-id-03').setAttribute("class","slds-carousel__indicator-action");
        	component.set("v.pannelDisplay",2);
        } else if(active == 2) {
            
            if(carousel !== null)
          		carousel.setAttribute("style", "transform:translateX(-200%)");
            
            if(headerCarousel !== null)
          		headerCarousel.setAttribute("style", "transform:translateX(-100%)");
            
            document.getElementById('indicator-id-02').setAttribute("class","slds-carousel__indicator-action");
            document.getElementById('indicator-id-01').setAttribute("class","slds-carousel__indicator-action");
            document.getElementById('indicator-id-03').setAttribute("class","slds-carousel__indicator-action slds-is-active");
            component.set("v.pannelDisplay",3)
        } else{
            
            if(carousel !== null)
          		carousel.setAttribute("style", "transform:translateX(-0%)");
            if(headerCarousel !== null)
          		headerCarousel.setAttribute("style", "transform:translateX(-0%)");
            
            document.getElementById('indicator-id-01').setAttribute("class","slds-carousel__indicator-action slds-is-active");
            document.getElementById('indicator-id-02').setAttribute("class","slds-carousel__indicator-action");
            document.getElementById('indicator-id-03').setAttribute("class","slds-carousel__indicator-action");
            component.set("v.pannelDisplay",1);
        }

	}
})