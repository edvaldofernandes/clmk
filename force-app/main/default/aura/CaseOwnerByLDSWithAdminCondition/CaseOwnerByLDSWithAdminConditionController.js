({
    handleCaseChanged: function(component, event, helper) {
        switch(event.getParams().changeType) {
            case "ERROR":
              	break;
            case "LOADED":
                component.set("v.userId", $A.get('$SObjectType.CurrentUser.Id'));   //when case is loaded, then get user info
                component.find('userData').reloadRecord(true);                      //getting user info
                break;
            case "REMOVED":
              	break;
            case "CHANGED":
              	break;
        }
    },

    handleUserChanged: function(component, event, helper) {
        switch(event.getParams().changeType) {
            case "ERROR":
              	break;
            case "LOADED":
                var newOwnerId = component.get("v.currentUser.Id");
                if(helper.caseOwnerNeedsUpdate(component)){
                    component.set("v.thisCase.OwnerId", newOwnerId);
                    helper.saveRecordWithLDS(component);
                }
                break;
            case "REMOVED":
              	break;
            case "CHANGED":
              	break;
        }
    }
})