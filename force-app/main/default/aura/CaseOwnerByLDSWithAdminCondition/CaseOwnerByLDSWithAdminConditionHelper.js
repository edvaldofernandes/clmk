({
	saveRecordWithLDS : function(component) {
		component.find("recordEditor").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    console.log(JSON.stringify(saveResult.state));
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
        }));
	},
    caseOwnerNeedsUpdate : function(component) {
		var oldOwnerId = component.get("v.thisCase.OwnerId");
        var newOwnerId = component.get("v.currentUser.Id");
        var newOwnerProfile = component.get("v.currentUser.Profile.Name");
        var newOwnerRole = component.get("v.currrentUser.UserRole.Name");
        var newOwnerName = component.get("v.currentUser.Name");
        var userExceptionSet = new Set(["Ramon Ribeiro Lopes"]);
        
        return (oldOwnerId != newOwnerId && ((newOwnerProfile != 'System Administrator' && newOwnerRole != 'PRC Manager') || userExceptionSet.has(newOwnerName)));
		
    }
})