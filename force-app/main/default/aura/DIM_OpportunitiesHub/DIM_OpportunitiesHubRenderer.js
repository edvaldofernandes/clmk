({
    afterRender : function(component, helper) {

        this.superAfterRender();

        var didScroll = false;
        var oldScroll = 0;

        window.onscroll = function() {
            didScroll = true;
        };

        var scrollCheckIntervalId = setInterval($A.getCallback(function() {
            didScroll = window["scrollY"] > oldScroll;
            oldScroll = window["scrollY"];
            
            if (didScroll && component.isValid()) {
				let scrollHeight = Math.max(
                  document.body.scrollHeight, document.documentElement.scrollHeight,
                  document.body.offsetHeight, document.documentElement.offsetHeight
                );
                didScroll = false;
                if (window.scrollY > (scrollHeight - window.outerHeight - 100)) {
                    helper.lazyLoadRecords(component);
                }
            }
        }), 1000);

        component.set("v.scrollCheckIntervalId", scrollCheckIntervalId);
    },

    unrender : function(component, helper) {

        this.superUnrender();

        var scrollCheckIntervalId = component.get("v.scrollCheckIntervalId");

        if (!$A.util.isUndefinedOrNull(scrollCheckIntervalId)) {
            window.clearInterval(scrollCheckIntervalId);
        }
    }
})