({    
    //================================================================================================================================
    //================================================================================================================================
    init : function (component, event, helper) {
                
        let today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set("v.today", today);
        component.set("v.currentYear", new Date().getFullYear());
        
        helper.initLeaderships(component);
        helper.initRegions(component);
        helper.initConditions(component);
        helper.initAircraftFamilies(component);
        helper.initDeliveryYears(component);
        helper.initStages(component);
        helper.initValues(component, helper);
        helper.initTasks(component, helper);
       
        helper.customizeForUser(component, helper);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeRegion : function (component, event, helper) {
        let region = event.getParam("value");        
        helper.changeRegion(component, helper, region);
        helper.changeOwner(component, helper, "");
        helper.filterOpportunities(component);
        helper.initOwners(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeSubRegion : function (component, event, helper) {
        let subRegion = event.getParam("value");
        helper.changeSubRegion(component, helper, subRegion);
        helper.changeOwner(component, helper, "");
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeOwner : function (component, event, helper) {
        let owner = event.getParam("value");
        helper.filterOpportunities(component);
        helper.changeOwner(component, helper, owner);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeCondition : function (component, event, helper) {
        let condition = event.getParam("value");
        helper.changeCondition(component, helper, condition);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeAircraftFamily : function (component, event, helper) {
        let aircraftFamily = event.getParam("value");
        helper.changeAircraftFamily(component, helper, aircraftFamily);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeDeliveryYear : function (component, event, helper) {
        let deliveryYear = event.getParam("value");
        helper.changeDeliveryYear(component, helper, deliveryYear);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeStage : function (component, event, helper) {
        let stage = event.getParam("value");
        helper.changeStage(component, helper, stage);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    changeShowWeeklyMeeting : function (component, event, helper) {
        let showWeeklyMeeting = component.get("v.showWeeklyMeeting");
        helper.changeShowWeeklyMeeting(component, helper, showWeeklyMeeting);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    showTasks : function (component, event, helper) {
        if (component.get("v.showTasks")) {
        	component.set("v.showOrdersByYear", false);
            component.set("v.showComplianceStatus", false);
            helper.setTasks(component);
        }
        helper.filterOpportunities(component);
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    showOrdersByYear : function (component, event, helper) {
        if (component.get("v.showOrdersByYear")) {
        	component.set("v.showTasks", false);
        }
        helper.filterOpportunities(component);
    },

    //================================================================================================================================
    //================================================================================================================================   
    showComplianceStatus : function (component, event, helper) {
        let showComplianceStatus = component.get("v.showComplianceStatus");
        helper.showComplianceStatus(component, helper, showComplianceStatus);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    showPrintFormat : function (component, event, helper) {
        let showPrintFormat = component.get("v.showPrintFormat");        
        if (showPrintFormat) {
        	helper.initValues(component, helper);
        	component.set("v.showOpportunityActions", false);
        	component.set("v.showOrdersByYear", true);
        	component.set("v.showTasks", false);
        	component.set("v.showComplianceStatus", false);
        	helper.changeShowWeeklyMeeting(component, helper, true);
        }
        else {
            component.set("v.showOpportunityActions", true);
        }
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    search : function (component, event, helper) {
        helper.filterOpportunities(component);
    },

    //================================================================================================================================
    //================================================================================================================================
    sortByField : function(component, event, helper) {
        let field = event.target.id;
        let fieldSort = component.get("v.sortIsAsc");
       	helper.sortByField(component, helper, field, fieldSort);
    },
    
	//================================================================================================================================
    //================================================================================================================================
    openCRM360 : function(component, event, helper){
        let accountId = event.currentTarget.dataset.value;        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/CRM360_CustomerDashboardPage?id=" + accountId,
        });
        urlEvent.fire();
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    expandRow : function (component, event, helper) {
        let id = event.getSource().get("v.value");
        let opportunities = component.get("v.opportunitiesLoaded");
        opportunities = helper.loadMoreOpportunities(component);
        
        let index = opportunities.findIndex((opportunity) => opportunity.Id == id);
        opportunities[index].IsExpanded = !opportunities[index].IsExpanded;
        
        opportunities = opportunities.map((opportunity) => {
            if (opportunity.Parent_Opportunity__r_Id == id) {
            	opportunity.IsVisible = !opportunity.IsVisible;
        	}
            return opportunity;
        });
        component.set("v.opportunitiesLoaded", opportunities);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    createOpportunity : function (component, event, helper) {
        let recordTypeId = event.currentTarget.dataset.value;
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Opportunity",
            "recordTypeId" : recordTypeId
        });
        createRecordEvent.fire();
	},
    
    //================================================================================================================================
    //================================================================================================================================
    createTask : function (component, event, helper) {
        let opportunityId = event.getSource().get("v.value");
        let today = component.get("v.today");
        let dueDate = new Date(today);
        dueDate.setDate(dueDate.getDate() + 7);
            
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Task",
            "defaultFieldValues": {
                "WhatId" : opportunityId,
                "ActivityDate" : dueDate.toISOString()
            },
            "navigationLocation": "LOOKUP",
            "panelOnDestroyCallback": function(event) {
            }
        });
        createRecordEvent.fire();
	},

	//================================================================================================================================
    //================================================================================================================================
	previewFile: function(component, event, helper) {
        let fileId = event.getParam("value");
		$A.get('e.lightning:openFiles').fire({
		    recordIds: [fileId]
		});
	},
        
	//================================================================================================================================
    //================================================================================================================================
	handleTaskAction: function(component, event, helper) {
        let taskId = event.getSource().get("v.value");
        let action = event.getParam("value");

        switch (action) {
            case "view":
                helper.viewRecord(taskId);
                break;
            case "edit":
                helper.editRecord(taskId);
                break;
        }
    },
    
	//================================================================================================================================
    //================================================================================================================================
	handleOpportunityAction: function(component, event, helper) {
        let opportunity = event.getSource().get("v.value");
        let action = event.getParam("value");

        switch (action) {
            case "view":
                helper.viewRecord(opportunity.Id);
                break;
            case "edit":
                helper.editRecord(opportunity.Id);
                break;
			case "pbuy":
                helper.createPBuyVisualforce(opportunity.AccountId);
                break;
			case "pwin":
                helper.createPWinVisualforce(opportunity.Id);
                break;
			case "account":
                helper.viewRecord(opportunity.AccountId);
                break;
        }
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    goToDashboard : function (component, event, helper) {
        let url = event.currentTarget.dataset.value;
        helper.goToURL(url);
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    goToRecord : function (component, event, helper) {
        let id = event.currentTarget.dataset.value;
        if (id) {
        	helper.viewRecord(id);
        }
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    changeTab : function (component, event, helper) {
        let tabId = event.getParam("id");
        component.set("v.isTabMain", tabId == "main");
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    viewMyOpportunities : function (component, event, helper) {       
        let user = component.get("v.currentUser");
        
        helper.initValues(component, helper);
        component.set("v.showComplianceStatus", true);
        
        helper.changeCondition(component, helper, "");
        helper.changeOwner(component, helper, user.Id);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    resetView : function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    showTabsFilters : function (component, event, helper) {
        let showTabsFilters = component.get('v.showTabsFilters');
        component.set('v.showTabsFilters', !showTabsFilters);
    },
    
	//================================================================================================================================
    //================================================================================================================================   
    handleChangeOpportunityVisibility : function (component, event, helper) {
        event.preventDefault();
        let recordId = event.getSource().get("v.recordId");
  		let formFields = event.getParam("fields");
  		formFields.DIM_Show_In_Meeting_Reports__c = !formFields.DIM_Show_In_Meeting_Reports__c;
        
        let forms = component.find("changeOpportunityVisibility");
        let form = forms;
        if (forms.length > 1) {
        	form = forms.find((form) => form.get("v.recordId") == recordId);
        }
        
        component.set("v.visibilityForm", form);
        component.set("v.visibilityFormFields", formFields);
        
        if (!component.get("v.visibilitySkipConfirmation")) {
            component.set("v.showConfirmDialog", true);
        }
        else {
            helper.changeOpportunityVisibility(component);
        }
    },

    //================================================================================================================================
    //================================================================================================================================
    handleConfirmDialogYes : function(component, event, helper) {
        component.set("v.showConfirmDialog", false);
        helper.changeOpportunityVisibility(component);
    },

    //================================================================================================================================
    //================================================================================================================================
    handleConfirmDialogNo : function(component, event, helper) {
        component.set("v.showConfirmDialog", false);
    },
})