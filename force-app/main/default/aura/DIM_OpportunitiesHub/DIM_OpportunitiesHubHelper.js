({  
	//================================================================================================================================
    //================================================================================================================================
    initValues : function (component, helper) {
        component.set("v.searchText", "");

        component.set("v.showOpportunityActions", true);
        component.set("v.showTasks", false);
        component.set("v.showOrdersByYear", true);
    	component.set("v.showComplianceStatus", false);
        
        this.changeRegion(component, helper, "");
       	this.changeOwner(component, helper, "");
        this.changeCondition(component, helper, "New Aircraft Sales");
        this.changeAircraftFamily(component, helper, "");
        this.changeDeliveryYear(component, helper, "");
        this.changeStage(component, helper, "");
        this.changeShowWeeklyMeeting(component, helper, true);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initOpportunities : function (component, helper) {
        let year = component.get("v.currentYear");
        let soql = "SELECT Id, Name, StageName, CloseDate, OwnerId, Owner.Name, Owner.UserRole.Name, Timeframe__c, Lessor_if_any__c, " +
                        "DIM_Show_In_Meeting_Reports__c, Campaign_Priority__c, Probability, " + 
            			"Account.PBuy_Current__c, Account.PBuy_Delta_Previous__c, PWin_Current__c, PWin_Delta_Previous__c, PBuy_vs_PWin__c, " + 
                        "AccountId, Account.Name, Account.RecordType.Name, Account.BillingCountry, Account.Geographical_Area__c, Account.Has_CRM360_Data__c, " +
                        "DIM_Aircraft_Model__c, " +
                        "Leadership_Report_Firm_Order__c, Y" + (year) + "__c, Y" + (year+1) + "__c, Y" + (year+2) + "__c, Y" + (year+3) + "__c, Y" + (year+4) + "__c, Y" + (year+5) + "__c, " +
                        "Staff_Meeting_Report_Summary__c, Leadership_VSS_Update__c, RecordType.Name, " + 
                        "DIM_NDA_Id__c, DIM_NDA_Status__c, KYCSales__c, " + 
                        "Parent_Opportunity__c, Parent_Opportunity__r.Id, Parent_Opportunity__r.Campaign_Priority__c, Parent_Opportunity__r.DIM_Show_In_Meeting_Reports__c " +
                    "FROM Opportunity " +
                    "WHERE IsClosed = FALSE AND RecordType.Name IN ('New Aircraft Sales', 'Used Aircraft Sales') AND " + 
            			"Owner.UserRole.Name LIKE 'S&M%' AND Owner.UserRole.Name NOT IN ('S&M DIM', 'S&M DCT') " + 
        			"ORDER BY Owner.Name ASC";
        
        helper.soql(component, soql)
        .then(function (opportunities) {
            opportunities.forEach(function(opportunity) {
                opportunity["RelatedOpportunities"] = opportunities.filter((opp) => opp.Parent_Opportunity__r_Id == opportunity.Id).length;
                opportunity["IsExpanded"] = false;
                opportunity["IsVisible"] = !opportunity.Parent_Opportunity__r;
                
                for (let i = 0; i <= 5; i++) {
                    opportunity["CY_" + i] = opportunity["Y" + (year+i) + "__c"];
                }
                                                               
                opportunity["DIM_Aircraft_Model__c"] = (opportunity["DIM_Aircraft_Model__c"] || "").split(";").sort().join("\n").trim();
                opportunity["DIM_Aircraft_Model__c"] = opportunity["DIM_Aircraft_Model__c"].replace("A/C Not Defined", "");
                
                opportunity["Account_BillingCountry"] = (opportunity["Account_BillingCountry"] || "").replace("United States of America", "USA");
                opportunity["StageNameShort"] = opportunity["StageName"];
                opportunity["StageNameShort"] = opportunity["StageNameShort"].replace("Contract Negotiation", "Contract Neg.");
                opportunity["StageNameShort"] = opportunity["StageNameShort"].replace("Business Development", "Business Dev.");
                
                opportunity["Lessor_if_any__c"] = opportunity["Lessor_if_any__c"] || "";
                opportunity["Lessor_if_any__c"] = opportunity["Lessor_if_any__c"].replace(" / ", "/");
                opportunity["Lessor_if_any__c"] = opportunity["Lessor_if_any__c"].replace("/", " / ");
                opportunity["Lessor_if_any__c"] = opportunity["Lessor_if_any__c"].replace(", ", ",");
                opportunity["Lessor_if_any__c"] = opportunity["Lessor_if_any__c"].replace(",", ", ");
                
                switch (opportunity["StageName"]) {
                    case "Prospecting":
                        opportunity["StageColor"] = "rgb(255,192,0)";
                        break;
                    case "Business Development":
                        opportunity["StageColor"] = "rgb(248,121,16)";
                        break;
                    case "Proposal":
                        opportunity["StageColor"] = "rgb(0,164,222)";
                        break;
                    case "Contract Negotiation":
                        opportunity["StageColor"] = "rgb(104,46,182)";
                        break;
                    default:
                        opportunity["StageColor"] = "rgb(255,255,255)";
                        break;
                }
                
                opportunity["Region"] = opportunity["Account_Geographical_Area__c"];
                delete opportunity["Account_Geographical_Area__c"];
                helper.setConsolidatedRegion(opportunity);
                                
                if (opportunity.Parent_Opportunity__r) {
                	opportunity["ShowInReport"] = opportunity["Parent_Opportunity__r_DIM_Show_In_Meeting_Reports__c"] ? 1 : 2;
                    opportunity["DIM_Show_In_Meeting_Reports__c"] = opportunity["Parent_Opportunity__r_DIM_Show_In_Meeting_Reports__c"] || false;
                } else {
                    opportunity["ShowInReport"] = opportunity["DIM_Show_In_Meeting_Reports__c"] ? 1 : 2;
                    opportunity["DIM_Show_In_Meeting_Reports__c"] = opportunity["DIM_Show_In_Meeting_Reports__c"] || false;
                }
                delete opportunity["Campaign_Priority__c"];
                delete opportunity["Parent_Opportunity__r_Campaign_Priority__c"];
                delete opportunity["Parent_Opportunity__r_DIM_Show_In_Meeting_Reports__c"];                
                
                helper.setNDADetails(opportunity);
                helper.setKYCDetails(opportunity);
                
                opportunity["MasterSortKey"] = opportunity.ShowInReport + "|" + 
                    							helper.getOpportunityField((100 - (opportunity.Probability || 0))) + "|" +
                    							helper.getOpportunityField((100 - (opportunity.PBuy_vs_PWin__c || 0))) + "|" +
                                                helper.getOpportunityField((100 - (opportunity.PWin_Current__c || 0))) + "|" +
                                                helper.getOpportunityField((100 - (opportunity.Account_PBuy_Current__c || 0)));
                
                opportunity["SearchKey"] = opportunity.Name + "|" +
                    						opportunity.Account_Name + "|" + 
                    						opportunity.Account_BillingCountry + "|" + 
                    						opportunity.StageName + "|" + 
                    						opportunity.Owner_Name + "|" +
                    						opportunity.Lessor_if_any__c + "|" +
                    						opportunity.Timeframe__c + "|" +
                    						opportunity.DIM_Aircraft_Model__c + "|" +
                    						opportunity.Staff_Meeting_Report_Summary__c + "|" +
                							opportunity.Leadership_VSS_Update__c + "|" +
                    						opportunity.KYCTitle;
                
                opportunity["PBuy_Delta_Previous__c"] = opportunity["Account_PBuy_Delta_Previous__c"];
                delete opportunity["Account_PBuy_Delta_Previous__c"];
                
                helper.setPBuyDetails(opportunity);
                helper.setPWinDetails(opportunity);
            });
            
            component.set("v.opportunities", opportunities);
            helper.sortByField(component, helper, "MasterSortKey", false);
            helper.initOwners(component);
            helper.filterOpportunities(component);
		 })
        .catch(function (error) {
            console.log("SOQL: " + error);
        })
    },
   
    //================================================================================================================================
    //================================================================================================================================
    filterOpportunities : function (component) {
        let opportunities = component.get("v.opportunities");
        let opportunitiesFiltered = opportunities.filter(opportunity => {
			return 	this.hasFilterByShowWeeklyMeeting(component, opportunity) &&
            		this.hasFilterByRegion(component, opportunity) &&
            		this.hasFilterBySubRegion(component, opportunity) &&
            		this.hasFilterByOwner(component, opportunity) &&
            		this.hasFilterByCondition(component, opportunity) &&
                    this.hasFilterByAircraftFamily(component, opportunity) &&
                    this.hasFilterByDeliveryYear(component, opportunity) &&
            		this.hasFilterByStage(component, opportunity) &&
            		this.hasFilterBySearchText(component, opportunity);
        });
        this.setOpportunitiesSummary(component, opportunitiesFiltered);
        component.set("v.opportunitiesFiltered", opportunitiesFiltered);
        component.set("v.opportunitiesLoaded", opportunitiesFiltered.slice(0, 10));
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterByShowWeeklyMeeting : function (component, opportunity) {
        let showWeeklyMeeting = component.get("v.showWeeklyMeeting");
        if (showWeeklyMeeting) {
            return opportunity.DIM_Show_In_Meeting_Reports__c;
        }
        return true;
    },
    
    //================================================================================================================================
    //================================================================================================================================
    hasFilterByRegion : function (component, opportunity) {
        let region = component.get("v.opportunityRegion") || "";
        let isLeasing = opportunity.Account_RecordType_Name.startsWith("Leasing");
        switch (region) {
            case "Americas":
				return (opportunity.Region == "Latin America" || opportunity.Region == "North America") && !isLeasing;
            case "Asia Pacific":
                return opportunity.Region == "Asia Pacific";
            case "China":
                return opportunity.Region == "China" && !isLeasing;
            case "EMEA":
                return (opportunity.Region == "Europe" || opportunity.Region == "Middle East" || opportunity.Region == "Africa") && !isLeasing;
            case "Leasing":
				return isLeasing; 
        }
        return true;
	},
        
    //================================================================================================================================
    //================================================================================================================================
    hasFilterBySubRegion : function (component, opportunity) {
        let subRegion = component.get("v.opportunitySubRegion") || "";
        let isLeasing = opportunity.Account_RecordType_Name.startsWith("Leasing");
        switch (subRegion) {
            case "Latin America":
				return opportunity.Region == "Latin America" && !isLeasing;
            case "North America":
				return opportunity.Region == "North America" && !isLeasing;
            case "ERCA":
                return opportunity.Region == "Europe" && !isLeasing;
			case "MEA":
				return (opportunity.Region == "Middle East" || opportunity.Region == "Africa") && !isLeasing;
        }
        return true;      
    },
    
    //================================================================================================================================
    //================================================================================================================================
    hasFilterByOwner : function (component, opportunity) {
        let owner = component.get("v.opportunityOwner") || "";
        if (owner != "") {
            return opportunity.OwnerId == owner;
        }
        return true;
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterByCondition : function (component, opportunity) {
        let condition = component.get("v.opportunityCondition") || "";
        if (condition != "") {
            return opportunity.RecordType_Name == condition;
        }
        return true;
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterByAircraftFamily : function (component, opportunity) {
        let family = component.get("v.opportunityAircraftFamily") || "";
        switch (family) {
            case "E2":
				return opportunity.DIM_Aircraft_Model__c.includes("E2");
            case "E1":
				return !opportunity.DIM_Aircraft_Model__c.includes("E2") && !opportunity.DIM_Aircraft_Model__c.includes("ERJ") && !opportunity.DIM_Aircraft_Model__c.includes("TP");
            case "ERJ":
				return opportunity.DIM_Aircraft_Model__c.includes("ERJ");
			case "TP":
				return opportunity.DIM_Aircraft_Model__c.includes("TP");                
        }
        return true;
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterByDeliveryYear : function (component, opportunity) {
        let deliveryYear = component.get("v.opportunityDeliveryYear") || "";
        if (deliveryYear != "") {
            return opportunity["Y" + deliveryYear + "__c"] > 0;
        }
        return true;
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterByStage : function (component, opportunity) {
        let stage = component.get("v.opportunityStage") || "";
        if (stage != "") {
            return opportunity.StageName == stage;
        }
        return true;
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    hasFilterBySearchText : function (component, opportunity) {
        let searchText = component.get("v.searchText") || "";
        let showTasks = component.get("v.showTasks");
        let showPrintFormat = component.get("v.showPrintFormat");
        let result = true;
        if (searchText != "") {
            let opportunitySearch = this.includedInSearch(opportunity, searchText);            
            if (showTasks) {
                opportunity["ContainsTaskSearch"] = opportunity.Tasks.filter((task) => this.includedInSearch(task, searchText)).length > 0;
            }
            result = opportunitySearch || (showTasks && opportunity["ContainsTaskSearch"]);
        }
        if (opportunity.Parent_Opportunity__r) {
            opportunity.IsVisible = (searchText != "") || showPrintFormat;
        }
        opportunity.IsExpanded = (searchText != "") || showPrintFormat;
        return result;
    },
        
	//================================================================================================================================
    //================================================================================================================================
	includedInSearch : function (item, searchText) {
		return item["SearchKey"].toLowerCase().includes(searchText.toLowerCase());
    },
    
	//================================================================================================================================
    //================================================================================================================================
    setNDADetails : function (opportunity) {
        let nda = opportunity["DIM_NDA_Status__c"] || "";
        let stage = opportunity["StageName"] || "";
        
        opportunity["NDA_Class"] = "badge-status compliance ";
		
        if (nda == "Valid") {
            opportunity["NDA_Title"] = "Valid NDA";
            opportunity["NDA_Class"] = opportunity["NDA_Class"] + "status-ok";
        }
        else {
            if (stage == "Prospecting") {
                opportunity["NDA_Title"] = "NDA is recommneded but not mandatory at this stage";
            }
            else {
                opportunity["KYC_Title"] = "Missing or expired NDA";
                opportunity["NDA_Class"] = opportunity["NDA_Class"] + "status-error";
            }    
        }
        delete opportunity["DIM_NDA_Status__c"];
    },
    
    //================================================================================================================================
    //================================================================================================================================
    setKYCDetails : function (opportunity) {
        let kyc = opportunity["KYCSales__c"] || "";
        let stage = opportunity["StageName"] || "";
		
        opportunity["KYC_Class"] = "badge-status compliance ";
        
        //PROSPECTING
        if (stage == "Prospecting") {
            if (kyc.includes("Valid")) {
            	opportunity["KYC_Title"] = "KYC " + kyc;
                opportunity["KYC_Class"] = opportunity["KYC_Class"] + "status-ok";
            }
            else {
                opportunity["KYC_Title"] = "KYC is recommneded but not mandatory at this stage";
            }
        }
        // PROPOSAL, CONTRACT NEGOTIATION
        else if (stage == "Proposal" || stage == "Contract Negotiation") {
            if (kyc.includes("Proposal")) {
            	opportunity["KYC_Title"] = "KYC " + kyc;
                opportunity["KYC_Class"] = opportunity["KYC_Class"] + "status-ok";
            }
            else if (kyc.includes("Preliminary")) {
                opportunity["KYC_Title"] = "The KYC information must be updated";
                opportunity["KYC_Class"] = opportunity["KYC_Class"] + "status-warning";
            }
			else {
            	opportunity["KYC_Title"] = "Missing or expired KYC";
                opportunity["KYC_Class"] = opportunity["KYC_Class"] + "status-error";        
			}
        }
        // OTHERS, E.G.: BUSINESS DEVELOPMENT
		else {
			if (kyc.includes("Valid")) {
            	opportunity["KYC_Title"] = "KYC " + kyc;
                opportunity["KYC_Class"] = opportunity["KYC_Class"] + "status-ok";
            }
            else {
            	opportunity["KYC_Title"] = "Missing or expired KYC";
                opportunity["KYC_Class"] = opportunity["KYC_Class"] + "status-error";        
			}
		}
        
        // LABEL
        if (kyc.includes("Preliminary")) {
            opportunity["KYC_Label"] = "Preliminary KYC";
        }
        else if (kyc.includes("Proposal")) {
            opportunity["KYC_Label"] = "Proposal KYC";
        }
		else {
			opportunity["KYC_Label"] = "KYC";
		}
        
        delete opportunity["KYCSales__c"];
    },
    
    //================================================================================================================================
    //================================================================================================================================
    setPBuyDetails : function (opportunity) {
		this.setProbabilityDetails(opportunity, "PBuy");
    },
    
	//================================================================================================================================
    //================================================================================================================================
    setPWinDetails : function (opportunity) {
		this.setProbabilityDetails(opportunity, "PWin");
    },
    
    //================================================================================================================================
    //================================================================================================================================
    setProbabilityDetails : function (opportunity, type) {
        let delta = opportunity[type + "_Delta_Previous__c"] || 0;
        let deltaSign = Math.sign(delta);

        opportunity[type + "Delta"] = delta;
        
        if (deltaSign == -1) {
            opportunity[type + "DeltaTrendOption"] = "trend-down";
            opportunity[type + "DeltaTrendIconName"] = "utility:forward_up";
            opportunity[type + "DeltaTrendTitle"] = delta + " compared to the previous score";
        }
        else if (deltaSign == 1) {
            opportunity[type + "DeltaTrendOption"] = "trend-up";
            opportunity[type + "DeltaTrendIconName"] = "utility:forward_up";
            opportunity[type + "DeltaTrendTitle"] = "+" + delta + " compared to the previous score";
        }
		else {
            opportunity[type + "DeltaTrendOption"] = "trend-neutral";
            opportunity[type + "DeltaTrendIconName"] = "utility:forward";
            opportunity[type + "DeltaTrendTitle"] = "No changes in the score";                
		}
        delete opportunity[type + "_Delta_Previous__c"];
    },
    
	//================================================================================================================================
    //================================================================================================================================
    setConsolidatedRegion : function (opportunity) {
        if (opportunity.Region == "Latin America" || opportunity.Region == "North America") {
            opportunity["ConsolidatedRegion"] = "Americas";
        }
        else if (opportunity.Region == "Asia Pacific") {
            opportunity["ConsolidatedRegion"] = "Asia Pacific";
        }
		else if (opportunity.Region == "China") {
            opportunity["ConsolidatedRegion"] = "China";
		}
		else if (opportunity.Region == "Europe" || opportunity.Region == "Middle East" || opportunity.Region == "Africa") {
			opportunity["ConsolidatedRegion"] = "EMEA";
		}
		else {
        	opportunity["ConsolidatedRegion"] = "";        
		}
	},
    
    //================================================================================================================================
    //================================================================================================================================
    initLeaderships : function (component) {
        let leaderships = [];
        leaderships.push({"region": "Global",			"id": "005i0000005R32gAAC"});
        leaderships.push({"region": "Americas", 		"id": "005i0000007jq8kAAA"});
        leaderships.push({"region": "Latin America",	"id": "005i0000007jq8kAAA"});
        leaderships.push({"region": "North America",	"id": "005i0000007jq8kAAA"});
        leaderships.push({"region": "Asia Pacific", 	"id": "005i00000045t8iAAA"});
        leaderships.push({"region": "China", 			"id": "005i0000006GIixAAG"});
        leaderships.push({"region": "EMEA", 			"id": "005i0000006uP3dAAE"});
        leaderships.push({"region": "ERCA",				"id": "005i0000006uP3dAAE"});
        leaderships.push({"region": "MEA",				"id": "0050H00000ANwrVQAT"});
        leaderships.push({"region": "Leasing", 			"id": "005i0000005R32gAAC"});
        component.set("v.opportunityLeaderships", leaderships);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initRegions : function (component) {
        let regions = [];
        regions.push({"label": "-- ALL --", "value": ""});
        regions.push({"label": "Americas", "value": "Americas"});
        regions.push({"label": "Asia Pacific", "value": "Asia Pacific"});
        regions.push({"label": "China", "value": "China"});
        regions.push({"label": "EMEA", "value": "EMEA"});
        regions.push({"label": "Leasing", "value": "Leasing"});
        component.set("v.opportunityRegion", "");
        component.set("v.opportunityRegions", regions);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeRegion : function (component, helper, region) {
        component.set("v.opportunityRegion", region);
        let subRegions = [{"label": "-- ALL -- ", "value": ""}];                
        switch (region) {
            case "Americas":
        		subRegions.push({"label": "Latin America", "value": "Latin America"});
        		subRegions.push({"label": "North America", "value": "North America"});
                break;
            case "EMEA":
                subRegions.push({"label": "Europe, Russia and Central Asia", "value": "ERCA"});
        		subRegions.push({"label": "Middle East and Africa", "value": "MEA"});
                break;
        }
        component.set("v.opportunitySubRegions", subRegions);
        component.set("v.opportunitySubRegion", "");
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeSubRegion : function (component, helper, subRegion) {
        component.set("v.opportunitySubRegion", subRegion);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initOwners : function (component) {
        let opportunities = component.get("v.opportunities");
        let owners = [];
        
        opportunities = opportunities.filter(opportunity => {
			return 	this.hasFilterByRegion(component, opportunity);
        });
        
        owners = opportunities.map((opportunity) => {
            return {
            	label: opportunity.Owner_Name,
            	value: opportunity.OwnerId
        	}
		});
        
        owners = owners.filter((owner, index) => {
            const _owner = JSON.stringify(owner);
            return index === owners.findIndex(item => {
            	return JSON.stringify(item) === _owner;
            });
        });
    
    	owners.sort(function(owner1, owner2) {
    		return owner1.label.localeCompare(owner2.label);
        });
    
        owners.unshift({"label": "-- ALL -- ", "value": ""});
        component.set("v.opportunityOwners", owners);
    },
    
	//================================================================================================================================
    //================================================================================================================================
    changeOwner : function (component, helper, owner) {
        component.set("v.opportunityOwner", owner);
        this.changeOwnerPicture(component, helper);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeOwnerPicture : function (component, helper) {
        let region = component.get("v.opportunityRegion") || "Global";
        let subRegion = component.get("v.opportunitySubRegion") || "";
        let owner = component.get("v.opportunityOwner");
        let leaderships = component.get("v.opportunityLeaderships");
        
        region = subRegion == "" ? region : subRegion;

        if (owner == "") {
            owner = leaderships.find((leadership) => {
        		return leadership.region == region
        	}).id;
        }
        
        if (owner != "") {        
            helper.soql(component, "SELECT FullPhotoUrl FROM User WHERE Id = '" + owner + "' LIMIT 1")
            .then(function (user) {
                component.set("v.opportunityOwnerPicture", user[0].FullPhotoUrl || "");
            })
            .catch(function (error) {
                console.log("SOQL: " + error);
            })
        }
        else {
            component.set("v.opportunityOwnerPicture", "");
        }
    },
    
    //================================================================================================================================
    //================================================================================================================================
    initConditions : function (component) {
        let conditions = [];
        conditions.push({"label": "-- ALL -- ", "value": ""});
        conditions.push({"label": "New", "value": "New Aircraft Sales"});
        conditions.push({"label": "Pre-Owned", "value": "Used Aircraft Sales"});
        component.set("v.opportunityConditions", conditions);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeCondition : function (component, helper, condition) {
        component.set("v.opportunityCondition", condition);
    },
    
	//================================================================================================================================
    //================================================================================================================================
    initAircraftFamilies : function (component) {
        let aircraftFamilies = [];
        aircraftFamilies.push({"label": "-- ALL -- ", "value": ""});
        aircraftFamilies.push({"label": "E-Jets E2", "value": "E2"});
        aircraftFamilies.push({"label": "E-Jets E1", "value": "E1"});
        aircraftFamilies.push({"label": "ERJ", "value": "ERJ"});
        aircraftFamilies.push({"label": "TP-NG", "value": "TP"});
        component.set("v.opportunityAircraftFamilies", aircraftFamilies);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeAircraftFamily : function (component, helper, aircraftFamily) {
        component.set("v.opportunityAircraftFamily", aircraftFamily);       
    },
    
	//================================================================================================================================
    //================================================================================================================================
    initDeliveryYears : function (component) {
        let year = component.get("v.currentYear");
        let deliveryYears = [];
        deliveryYears.push({"label": "-- ALL -- ", "value": ""});
        for (var i = 0; i <= 5; i++) {
            deliveryYears.push({"label": (year+i).toString(), "value": (year+i).toString()});
        }
        component.set("v.opportunityDeliveryYears", deliveryYears);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeDeliveryYear : function (component, helper, deliveryYear) {
        component.set("v.opportunityDeliveryYear", deliveryYear);
    },
        
	//================================================================================================================================
    //================================================================================================================================
    initStages : function (component) {
        let stages = [];
        stages.push({"label": "-- ALL -- ", "value": ""});
        stages.push({"label": "Prospecting", 			"value": "Prospecting"});
        stages.push({"label": "Business Development", 	"value": "Business Development"});
        stages.push({"label": "Proposal", 				"value": "Proposal"});
        stages.push({"label": "Contract Negotiation", 	"value": "Contract Negotiation"});
        component.set("v.opportunityStages", stages);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    changeStage : function (component, helper, stage) {
        component.set("v.opportunityStage", stage);
    },
    
    //================================================================================================================================
    //================================================================================================================================   
    changeShowWeeklyMeeting : function (component, helper, showWeeklyMeeting) {
        component.set("v.showWeeklyMeeting", showWeeklyMeeting);
    },
        
    //================================================================================================================================
    //================================================================================================================================   
    showComplianceStatus : function (component, helper, showComplianceStatus) {
        component.set("v.showComplianceStatus", showComplianceStatus);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    setOpportunitiesSummary : function (component, opportunities) {
        let year = component.get("v.currentYear");
        let opportunitiesSummary = {};
                
        opportunitiesSummary["Total"] = 		opportunities.filter((opportunity) => (opportunity.Parent_Opportunity__r_Id || "") == "").length;
        opportunitiesSummary["Total_Related"] = opportunities.filter((opportunity) => (opportunity.Parent_Opportunity__r_Id || "") != "").length;
        opportunitiesSummary["Firm_Orders"] = 	this.getOpportunitySummary(component, opportunities, "Leadership_Report_Firm_Order__c");
        for (let i = 0; i <= 5; i++) {
            opportunitiesSummary["CY_" + i] = 	this.getOpportunitySummary(component, opportunities, "CY_" + i);
        }
        
        component.set("v.opportunitiesSummary", opportunitiesSummary);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    getOpportunitySummary : function (component, opportunities, field) {
        if (opportunities.length > 0) {
            let summary = opportunities.map((opportunity) => {
                if ((opportunity.Parent_Opportunity__r_Id || "") == "") {
                    return opportunity[field] || 0;
                }
                return 0;
            }).reduce((prev, next) => prev + next);
            return summary;
        }
        return 0;
    },
    
    //================================================================================================================================
    //================================================================================================================================
    sortByField : function (component, helper, field, fieldSort) {
        
        if (field == "" || field != component.get("v.sortField")) {
            fieldSort = false;
        }
        fieldSort = !fieldSort;
        
		component.set("v.sortField", field);
        component.set("v.sortIsAsc", fieldSort);
              
        let opportunities = component.get("v.opportunities");
        opportunities.sort(function(opportunity1, opportunity2) {
            let opportunity1FieldWithParent = helper.getOpportunityFieldWithParent(helper, opportunities, opportunity1, field, fieldSort);
            let opportunity2FieldWithParent = helper.getOpportunityFieldWithParent(helper, opportunities, opportunity2, field, fieldSort);
            if (!opportunity1[field]) {
                return 1;
            }
            else if (!opportunity2[field]) {
                return -1;
            }
			else {
                return opportunity1FieldWithParent.localeCompare(opportunity2FieldWithParent) * (fieldSort ? 1 : -1);
            };
        });
        component.set("v.opportunities", opportunities);
        helper.filterOpportunities(component);
    },
    
    //================================================================================================================================
    //================================================================================================================================
    getOpportunityFieldWithParent : function (helper, opportunities, opportunity, field, fieldSort) {
        let opportunityField = helper.getOpportunityField(opportunity[field] || "");
        if (opportunity.Parent_Opportunity__r_Id) {
            let parentOpportunity = opportunities.filter((opp) => opp.Id == opportunity.Parent_Opportunity__r_Id)[0] || "";
            let parentOpportunityField = helper.getOpportunityField(parentOpportunity[field] || "");
            return parentOpportunityField + "|" + parentOpportunity.Id + "|" + (fieldSort ? 2 : 0) + "|" + opportunityField + "|" + opportunity.Id;
        }
		return opportunityField + "|" + opportunity.Id + "|1|";
    },
        
    //================================================================================================================================
    //================================================================================================================================
    getOpportunityField : function (field) {
        if (typeof field == "number") {
            field = (Array(6).join("0") + field).slice(-6);
        }
        return field;
    },
    
	//================================================================================================================================
    //================================================================================================================================
    initTasks : function (component, helper) {       
        helper.soql(component, "SELECT Id, Subject, ActivityDate, WhatId, OwnerId, Owner.Name, Due_Date_Status__c FROM Task WHERE What.Type = 'Opportunity' AND IsClosed = FALSE ORDER BY ActivityDate ASC")
        .then(function (tasks) {
            tasks.forEach(function(task) {
                if (task.ActivityDate === undefined) {
                    task["ActivityDateString"] = "TBD";
                }
                else {
                	task["ActivityDateString"] = new Intl.DateTimeFormat("en-US", {year: "numeric", month: "short", day: "numeric"}).format(Date.parse(task.ActivityDate)) || "";
                }
                task["SearchKey"] = task.Subject + "|" + task.Owner_Name;
            });
            component.set("v.tasks", tasks);
        })
        .catch(function (error) {
            console.log("SOQL: " + error);
        })
    },
            
	//================================================================================================================================
    //================================================================================================================================
	setTasks : function (component) {
        let tasksLoaded = component.get("v.tasksLoaded");
        if (!tasksLoaded) {
            let opportunities = component.get("v.opportunities");
            let tasks = component.get("v.tasks");
            opportunities.forEach(function(opportunity) {
                let opportunityTasks = tasks.filter((task) => task.WhatId == opportunity.Id);
                opportunity["Tasks"] = opportunityTasks;
            })
            component.set("v.opportunities", opportunities);
            component.set("v.tasksLoaded", true);
        }
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizeForUser : function (component, helper) {
        helper.customizeDevice(component);
        let user;
        helper.soql(component, "SELECT Id, Name, Email, UserRole.Name, Profile.Name FROM User WHERE Id = '" + $A.get("$SObjectType.CurrentUser.Id") + "' LIMIT 1")
        .then(function (users) {
            user = users[0];
            user["Region"] = helper.customizeRegion(user);          
            
            helper.changeRegion(component, helper, user["Region"]);
            helper.changeOwner(component, helper, "");
            helper.customizeAccess(component, user.UserRole_Name);
            helper.customizePrint(user);
            
            user["IsLeader"] = component.get("v.opportunityLeaderships").some(leader => leader.id == user.Id) ||
                				(user.UserRole_Name == "S&M DIM" && user.Profile_Name == "System Administrator");
            
            helper.soql(component, "SELECT Id FROM Opportunity " +
                                    "WHERE IsClosed = FALSE AND RecordType.Name IN ('New Aircraft Sales', 'Used Aircraft Sales') AND " + 
                                    	"Owner.UserRole.Name LIKE 'S&M%' AND Owner.UserRole.Name NOT IN ('S&M DIM', 'S&M DCT') AND " +
                        				"OwnerId = '" + user.Id + "'")
        	.then(function (opportunities) {
                user["Opportunities"] = opportunities.length;
                component.set("v.currentUser", user);
            	helper.initOpportunities(component, helper);
        	})
            .catch(function (error) {
                console.log("SOQL: " + error);
            })
        })
        .catch(function (error) {
            console.log("SOQL: " + error);
        })
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizeRegion : function (user) {
        let region = "";
        let role = user.UserRole_Name;
        switch (role) {
            case "S&M Asia Pacific":
                region = "Asia Pacific";
                break;
            case "S&M China":
                region = "China";
                break;
            case "S&M Latin America":
            case "S&M North America":
                region = "Americas";
                break;
            case "S&M EMEA":
                region = "EMEA";
                break;  
        }

        if (user.Email.includes("priscilla.doro")) {
            region = "Americas"
        }
        return region;
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizePrint : function (user) {
        user["CanPrint"] = false;
		if (user.Email.includes("matheus.bruno") ||
           	user.Email.includes("dhalena.richards") || 
            user.Email.includes("erica.mendes") || 
            user.Email.includes("jennifer.leslie")
           ) {
            user["CanPrint"] = true;
        }
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizeDevice : function (component) {
        let device = $A.get("$Browser.formFactor");
        let isTablet = $A.get("$Browser.isTablet");
        if (device == "PHONE" || isTablet) {
            component.set("v.isMobile", true);
            component.set("v.showOrdersByYear", false);
        }
    },
        
	//================================================================================================================================
    //================================================================================================================================
    customizeAccess : function (component, role) {
        let isSalesMarketing = role.toUpperCase().includes("S&M") || role.toUpperCase().includes("DCT");
        component.set("v.isSalesMarketing", isSalesMarketing);
        if (!isSalesMarketing) {
            component.set("v.showOpportunityActions", false);
            component.set("v.showWeeklyMeeting", true);
        }
    },
        
	//================================================================================================================================
    //================================================================================================================================
    viewRecord : function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": recordId,
          "slideDevName": "detail"
        });
        navEvt.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================
    editRecord : function(recordId) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
             "recordId": recordId
       });
        editRecordEvent.fire();
    },
        
	//================================================================================================================================
    //================================================================================================================================
    createPBuy : function(accountId) {            
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "PBuy__c",
            "defaultFieldValues": {
                "Account__c" : accountId
            }
        });
        createRecordEvent.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================
    createPBuyVisualforce : function(accountId) {            
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/DIM_New_Edit_PBuy?CF00N0H00000IR2d2_lkid=" + accountId + "&sfdc.override=1"
        });
        urlEvent.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================
    createPWin : function(opportunityId) {            
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "PWin__c",
            "defaultFieldValues": {
                "Opportunity__c" : opportunityId
            }
        });
        createRecordEvent.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================
    createPWinVisualforce : function(opportunityId) {            
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/DIM_New_Edit_PWin?CF00N0H00000IR2da_lkid=" + opportunityId + "&sfdc.override=1"
        });
        urlEvent.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================
    goToURL : function(url) {            
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},
        
	//================================================================================================================================
    //================================================================================================================================   
    lazyLoadRecords : function (component) {
        let opportunitiesLoaded = this.loadMoreOpportunities(component);
        component.set("v.opportunitiesLoaded", opportunitiesLoaded);
    },
        
	//================================================================================================================================
    //================================================================================================================================   
    loadMoreOpportunities : function (component) {
        let opportunitiesFiltered = component.get("v.opportunitiesFiltered");
        let opportunitiesLoaded = component.get("v.opportunitiesLoaded");
        let opportunitiesSlice = opportunitiesFiltered.slice(opportunitiesLoaded.length, opportunitiesLoaded.length + 10);
        return opportunitiesLoaded.concat(opportunitiesSlice);
    },
        
	//================================================================================================================================
    //================================================================================================================================   
    changeOpportunityVisibility : function (component) {
		let form = component.get("v.visibilityForm");
        let formFields = component.get("v.visibilityFormFields");
        let recordId = form.get("v.recordId");
        
        form.submit(formFields);
        
        let opportunities = component.get("v.opportunities");
        let opportunitiesFiltered = component.get("v.opportunitiesFiltered");
        let opportunitiesLoaded = component.get("v.opportunitiesLoaded");
        
        opportunities.find((opportunity) => opportunity.Id == recordId)["DIM_Show_In_Meeting_Reports__c"] = formFields.DIM_Show_In_Meeting_Reports__c;
        opportunitiesFiltered.find((opportunity) => opportunity.Id == recordId)["DIM_Show_In_Meeting_Reports__c"] = formFields.DIM_Show_In_Meeting_Reports__c;
        opportunitiesLoaded.find((opportunity) => opportunity.Id == recordId)["DIM_Show_In_Meeting_Reports__c"] = formFields.DIM_Show_In_Meeting_Reports__c;
        
        component.set("v.opportunities", opportunities);
        component.set("v.opportunitiesFiltered", opportunitiesFiltered);
        component.set("v.opportunitiesLoaded", opportunitiesLoaded);
    },
})