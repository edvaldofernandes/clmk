({
	doInit : function(component, event, helper) {
        var record = component.get("v.StockInfo");
        var apiName = component.get("v.APIName");
        var value = record[apiName];
        component.set("v.Value", value);
	}
})