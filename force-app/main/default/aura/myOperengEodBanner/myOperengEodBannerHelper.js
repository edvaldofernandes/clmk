({
    loadEod : function(component, event, helper) {
        
        var action = component.get("c.getEod");
        action.setParams({ eodId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.eod', result);
                var action2 = component.get("c.getCaseNumber");
        		action2.setParams({"id": component.get('v.eod.Related_Case__c')});
        
        		action2.setCallback(this, function(result) {
            		var state = result.getState();
            
            		if (state === "SUCCESS") {
                		component.set("v.CaseNumber", result.getReturnValue());
            		}
             		else if (state === "ERROR") {
                		console.log("unknown error");
           			}
            
        		});
       			$A.enqueueAction(action2);
                
                //
                
                var date = new Date(component.get('v.eod.Expiry_Date__c'));
        		var d = new Date();
 
        		if(d.getFullYear() > date.getFullYear()){// se o ano atual for maior que o ano de expirar a EOD
            		component.set('v.valid', false);
        		}else if(d.getFullYear() < date.getFullYear()){//  se o ano atual for menor que o ano de expirar a EOD
            		component.set('v.valid', true);
        		}else if(d.getMonth() > date.getMonth() || (d.getMonth() == date.getMonth && d.getDay() > date.getDay())){// se o ano atual for igual ao ano de expirar a EOD, faz as verificações com os meses e os dias
            		component.set('v.valid', false);
        		}else{// se o ano atual foir igual ao da EOD mas nao ter chego ainda no dia de expirar
    				component.set('v.valid', true);
				}
                
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);  
    }
})