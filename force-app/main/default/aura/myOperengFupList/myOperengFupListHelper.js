({
    queryFups : function(component, event, helper) {
        component.set("v.Spinner", true);
        var family = component.find("familyFilter").get("v.value");
        var authority = component.find("authorityFilter").get("v.value");
        var status = component.find("statusFilter").get("v.value");

        
        var action = component.get("c.getFups");
        action.setParams({
            "family": family,
            "authority": authority,
            "status": status,            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.fups', result);
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(action);
    }
})