({
    newPBuy : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/lightning/o/PBuy__c/new?originalUrl=%2Fapex%2FDIM_New_Edit_PBuy%3FCF00N0H00000IR2d2_lkid%3D" + component.get("v.targetFields.AccountId")
        });
        urlEvent.fire();
    },
    newPWin : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/lightning/o/PWin__c/new?originalUrl=%2Fapex%2FDIM_New_Edit_PWin%3FCF00N0H00000IR2da_lkid%3D" + component.get("v.recordId")
        });
        urlEvent.fire();
    }
})