({
	toggleNavigationBar : function(component, event, helper) {
        var x = component.get("v.isActive");
        component.set("v.isActive", !x);
	},
	navigateTo : function(component, event, helper) {
        component.set("v.isActive", false);
        var nextPageId = event.currentTarget.id;
        component.getSuper().navigate(nextPageId);
	},
})