({
	optionSelected : function(component, event, helper) {
		helper.toggleNavigationBar(component, event, helper);
        helper.navigateTo(component, event, helper);
	},
	navigateToHome : function(component, event, helper) {
        helper.toggleNavigationBar(component, event, helper);
		helper.navigateTo(component, event, helper);
	},
    toggleNavigationBar : function(component, event, helper) {
		helper.toggleNavigationBar(component, event, helper);
	}
})