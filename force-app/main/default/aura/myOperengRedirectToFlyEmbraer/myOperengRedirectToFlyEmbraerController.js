({
	redirect : function(component, event, helper) {
        //var origin = 'https://embraercommercialaviation.force.com/myOpereng#';
        var currentUrl = window.location.href;
        var path = currentUrl.split('startURL=')[1];
        path = decodeURIComponent(path);
        var host = window.location.hostname
        var redirectUrl = 'https://' + host + path;
        
        var SSOUrl = component.get('v.SSOUrl');
        SSOUrl = SSOUrl + redirectUrl;
        component.set('v.SSOUrl', SSOUrl);
        window.open(SSOUrl,'_top');
	}
})