({
    //================================================================================================================================
    //================================================================================================================================
    apex: function(cmp, method, params) {
        return new Promise(function (resolve, reject) {
            var action = cmp.get("c." + method);
            action.setParams(params);
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    resolve(response.getReturnValue());
                }
                else if (state === "ERROR") {
                	reject("Method APEX Error");
                    console.error(response.getError());
                }
            });
            $A.enqueueAction(action);
        });
    },
    
    //================================================================================================================================
    //================================================================================================================================
    soql: function (cmp, query) {
        return new Promise(function (resolve, reject) {
            var action = cmp.get("c.executeSoql");
            action.setParams({soql: query});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {               
                    var results = response.getReturnValue();
                    results.forEach(row => {
                        for (const col in row) {
                            const curCol = row[col];
                            if (typeof curCol === 'object') {
                                const newVal = curCol.Id ? ('/' + curCol.Id) : null;
                        
                        		var topObject = row;
                        		var prefix = col + '_';
                        		var toBeFlattened = curCol;
                        
                        		for (const prop in toBeFlattened) {
                                    const curVal = toBeFlattened[prop];
                                    if (typeof curVal === 'object') {
                                        flattenStructure(topObject, prefix + prop + '_', curVal);
                                    }
                                    else {
                                        topObject[prefix + prop] = curVal;
                                    }
                                }
                        
                                if (newVal === null) {
                                    delete row[col];
                                }
                                else {
                                   row[col] = newVal;
                                }
                            }
                        }
                    });
                    resolve(results);
                }
                else if (state === "ERROR") {
                    reject("Method SOQL Error");
                    console.error(response.getError());
                }
            });
            $A.enqueueAction(action);
        });

        function flattenStructure(topObject, prefix, toBeFlattened) {
            for (const prop in toBeFlattened) {
                const curVal = toBeFlattened[prop];
                if (typeof curVal === 'object') {
                    flattenStructure(topObject, prefix + prop + '_', curVal);
                }
                else {
                    topObject[prefix + prop] = curVal;
                }
            }
        }
    }
})