({
    doInit: function(component, event, helper) {
        component.set("v.Spinner", true);
        var action = component.get("c.getEods");
        action.setCallback(this, function(result) {
            var state = result.getState();
            if (state === "SUCCESS") {
                component.set("v.eods" ,result.getReturnValue());
            }
             else if (state === "ERROR") {
                console.log("unknown error");
           }
            component.set("v.Spinner", false);
        });  
        $A.enqueueAction(action);
	}, 
    download : function (component, event, helper) {  
        helper.downloadPdfFile(component, event, helper);  
    },  
    sortByStatus: function(component, event, helper) {
        helper.sortBy(component, "Status");
    },
    sortBySubject: function(component, event, helper) {
        helper.sortBy(component, "Subject");
    },
    sortByIssueDate: function(component, event, helper) {
        helper.sortBy(component, "IssueDate");
    },
    sortByExpiryDate: function(component, event, helper) {
        helper.sortBy(component, "ExpiryDate");
    },
    changeComboBox: function(component, event, helper){
        var valideod = document.querySelectorAll("#valideod");
        var expiredeod = document.querySelectorAll("#expiredeod");
        
        valideod.forEach(function(userItem) {
  			if(component.find("eStatusFilter").get("v.value") == "Valid" || component.find("eStatusFilter").get("v.value") == "All"){
                userItem.style.display = "table-row";
            }else{
                userItem.style.display = "none";
            }
		});
        
       expiredeod.forEach(function(userItem) {
  			if(component.find("eStatusFilter").get("v.value") == "Expired" || component.find("eStatusFilter").get("v.value") == "All"){
                userItem.style.display = "table-row";
            }else{
                userItem.style.display = "none";
            }
		});
 
    }
})