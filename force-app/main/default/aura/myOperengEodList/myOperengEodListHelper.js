({
    sortBy: function(component, field) {
        var sortDesc = component.get("v.sortDesc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.allEods");
        sortDesc = sortField != field || !sortDesc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortDesc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortDesc", sortDesc);
        component.set("v.sortField", field);
        component.set("v.allEods", records);
        this.renderPage(component);
    },
	renderPage: function(component) {
		var records = component.get("v.allEods");    
	},
    //possíveis soluções
    // https://iwritecrappycode.wordpress.com/2013/08/28/visualforce-force-download-of-pdf-or-other-content/
    // https://salesforce.stackexchange.com/questions/174761/lightning-renderas-pdf-default-save-button
    // https://salesforce.stackexchange.com/questions/158489/print-download-pdf-from-vf-renderas-pdf-in-lightning-experience
    // Generating Pdf  through VisualForce Page
	downloadPdfFile : function(component,event,helper){
    var selectedwarehouse = component.get("v.selectedProduct"); //pass this value into vf page
    var fromDate = component.find("fromDate").get("v.value"); //pass this value into vf page
    var toDate = component.find("toDate").get("v.value");  //pass this value into vf page
    window.setTimeout(
      $A.getCallback(function() {
        // visualforce page URL
        window.location="/apex/WarehousePdf?fromDate="+fromDate+"&toDate="+toDate+"&selectedwarehouse="+selectedwarehouse;
     }), 1000
  );
},  
})