({
    fetchData: function (cmp) {
        var action = cmp.get("c.getOrderSizeItems");
        action.setParams({
        "recordId": cmp.get("v.recordId")
    	});
        // Register the callback function
    	action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var results = response.getReturnValue();
            cmp.set("v.data", results);
        }
    	});
    	// Invoke the service
    	$A.enqueueAction(action);
    },
    saveEdition: function (cmp, draftValues) {
        var action = cmp.get("c.saveEdits");
        action.setParams({"acs" : draftValues});
        action.setCallback(this, function(response) {
            var state = response.getState();
            //var results = response.getReturnValue();
            //cmp.set("v.data", results);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": "The record has been updated successfully if not submitted yet.",
                "type": "success"
            });
    		toastEvent.fire();
        });
        $A.enqueueAction(action);
        /*
        
        var action = cmp.get("c.saveEdits");
        action.setParams({
            "aircrafts": cmp.get("v.data")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                //var location = "/one/one.app?#/sObject/" + results + "/view";
                //parent.window.location.href=location;
            }
        });
        // Invoke the service
        $A.enqueueAction(action);*/
    }
});