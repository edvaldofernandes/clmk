({
    doInit : function(component, event, helper) {
        var record = component.get("v.SummaryRecord");
        var apiName = component.get("v.APIName");
        var value = record[apiName];
        component.set("v.Value", value);
	},
    
	handleClick : function(component, event, helper) {
        var records = component.get("v.StockInfos");
        var apiName = component.get("v.APIName");
        var label = component.get("v.Label");
        
		var navService = component.find("navService");
    	var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__PR_ChainView"
            }
        };
     	navService.navigate(pageReference);
        helper.fireShowChainView(component, records, apiName, label);
        setTimeout(
            $A.getCallback(function(){
                helper.fireShowChainView(component, records, apiName, label)
            }), 600
        );
	}
})