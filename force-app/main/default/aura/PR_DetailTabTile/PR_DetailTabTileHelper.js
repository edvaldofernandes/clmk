({
	fireShowChainView : function(component, planningReportRecords, apiName, label) {
		var createEvent = $A.get("e.c:showChainView");									//get application event
        createEvent.setParams({ "PlanningReportRecords": planningReportRecords, "APIName":  apiName, "Label": label});	    //set params
        createEvent.fire();																//fire the event
	}
})