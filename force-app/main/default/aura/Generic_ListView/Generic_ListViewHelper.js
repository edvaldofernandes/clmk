({
  sortByKey: function (array, key, sortType) {
    return array.sort(function (a, b) {
      if (sortType === "DESC") {
        var y = a[key];
        var x = b[key];
      } else {
        var x = a[key];
        var y = b[key];
      }
      return x < y ? -1 : x > y ? 1 : 0;
    });
  },

  getNearIds: function (component, records) {
    var recordId = component.get("v.recordId");
    var recordIds = [];

    records.forEach(function (record) {
      recordIds.push(record.Id);
    });

    var recordPos = recordIds.indexOf(recordId);
    var beforePos, nextPos;
    beforePos = recordPos - 1;
    nextPos = recordPos + 1;
    if (recordPos == 0) {
      beforePos = recordIds.length - 1;
    } else if (recordPos == recordIds.length - 1) {
      nextPos = 0;
    }

    window.localStorage.setItem("beforeId", recordIds[beforePos]);
    window.localStorage.setItem("nextId", recordIds[nextPos]);
  }
});