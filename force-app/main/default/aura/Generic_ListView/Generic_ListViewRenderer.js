({
  afterRender: function (component, helper) {
    this.superAfterRender();
    var table = component.find("dyn-table").getElement();
    var intervalId = setInterval(function () {
      var trs = table.getElementsByClassName("selected");
      if (trs.length > 0 && trs[0].offsetHeight > 0) {
        var height = trs[0].offsetHeight;
        table.scrollTop = (trs[0].getAttribute("data-index") - 2) * height;
        clearInterval(intervalId);
      }
    }, 1000);

    var headerIndex = window.localStorage.getItem("headerIndex");
    var defaultSortField = component.get("v.defaultSortField");
    if (headerIndex === null) {
      headerIndex = defaultSortField;
    }

    var sortType = window.localStorage.getItem("sortType");

    var intervalSort = setInterval(function () {
      var ths = table.getElementsByTagName("th");
      if (ths.length > 0) {
        if (ths[headerIndex] !== undefined && ths[headerIndex !== null]) {
          var children = ths[headerIndex].children;
          if (children.length > 0) {
            if (sortType !== null) {
              children[0].classList.add(sortType);
            }
          }
          clearInterval(intervalSort);
        }
      }
    }, 1000);
  }
});