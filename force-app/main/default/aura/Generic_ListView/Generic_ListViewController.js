({
  doInit: function (component, event, helper) {
    var records = component.get("v.records");
    var headerIndex = window.localStorage.getItem("headerIndex");
    var sortType = window.localStorage.getItem("sortType");
    var apiNames = component.get("v.apiNames");
    var defaultSortField = component.get("v.defaultSortField");

    component.set("v.originalRecords", records);

    if (!(headerIndex >= 0) || sortType === null) {
      sortType = "ASC";
      headerIndex = defaultSortField;
      window.localStorage.setItem("headerIndex", headerIndex);
      window.localStorage.setItem("sortType", sortType);

      console.log("First time. Saving localStorage!");
    }

    var key = apiNames[headerIndex];
    records = helper.sortByKey(records, key, sortType);
    component.set("v.records", records);
    helper.getNearIds(component, records);
  },

  selectRecord: function (component, event, helper) {
    var targetId = event.currentTarget.getAttribute("data-recordId");
    var pageReference = {
      type: "standard__recordPage",
      attributes: {
        objectApiName: component.get("v.apiObject"),
        recordId: targetId,
        actionName: "view",
        anchor: "selected"
      }
    };
    component.find("navService").navigate(pageReference);
  },

  sort: function (component, event, helper) {
    var records = component.get("v.records");
    var apiNames = component.get("v.apiNames");
    var headerIndex = event.currentTarget.getAttribute("data-index");
    var key = apiNames[headerIndex];
    var originalRecords = component.get("v.originalRecords");

    var element = event.currentTarget;
    var child = element.children[0];

    if (headerIndex !== window.localStorage.getItem("headerIndex")) {
      var headers = element.parentNode.children;
      records = originalRecords;
      for (let i = 0; i < headers.length; i++) {
        headers[i].children[0].classList.remove("DESC");
        headers[i].children[0].classList.remove("ASC");
      }
    }

    var sortType;
    if (child.classList.contains("ASC")) {
      child.classList.remove("ASC");
      child.classList.add("DESC");
      sortType = "DESC";
    } else if (child.classList.contains("DESC")) {
      child.classList.remove("DESC");
      records = originalRecords;
      key = "Start_Date__c";
      sortType = null;
    } else {
      child.classList.add("ASC");
      sortType = "ASC";
    }

    records = helper.sortByKey(records, key, sortType);
    component.set("v.records", records);
    helper.getNearIds(component, records);

    window.localStorage.setItem("headerIndex", headerIndex);
    window.localStorage.setItem("sortType", sortType);
  }
});