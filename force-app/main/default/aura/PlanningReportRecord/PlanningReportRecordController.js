({
	handleClick : function(component, event, helper) {
        var record = component.get("v.record");
        
		var navService = component.find("navService");
    	var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__PlanningReportSummaryView"
            }
        };
     	navService.navigate(pageReference);
        helper.fireShowPlanningReportRecord(component, record);
        setTimeout(
            $A.getCallback(function(){
                helper.fireShowPlanningReportRecord(component, record)
            }), 500
        );
	}
})