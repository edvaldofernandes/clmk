({
    fireShowPlanningReportRecord : function(component, planningReportRecord) {
		var createEvent = $A.get("e.c:showPlanningReportRecord");						//get application event
        createEvent.setParams({ "PlanningReportRecord": planningReportRecord });	    //set PlanningInfo with the database info
        createEvent.fire();																//fire the event
	}
})