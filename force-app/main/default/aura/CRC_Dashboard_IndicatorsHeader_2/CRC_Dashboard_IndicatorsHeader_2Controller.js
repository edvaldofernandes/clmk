({
    doInit : function(component, event, helper) {

        helper.updateHeader(component,event);

        var handleTimer = window.setInterval(
            $A.getCallback(function() {
                helper.updateHeader(component,event);
            }), 30000
        );
    }
})