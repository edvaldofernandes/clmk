({
    doInit: function (component, event, helper) {
        component.find('selectRegion').set('v.value', 'FLL COM');
        helper.getTop30ByRegion(component, event);
    },
	onChange: function (component, event, helper) {
        helper.getTop30ByRegion(component, event);
    },
    keyCheck : function(component, event, helper){
        if (event.which == 13){
            helper.searchPlanningReport(component, event)
        }    
	},
	handleSearch : function(component, event, helper) {
      	helper.searchPlanningReport(component, event);	
    }
})