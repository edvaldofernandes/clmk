({
	searchPlanningReport : function(component, event) {
		var searchCriteria = component.get("v.SearchCriteria");
        var region = component.find('selectRegion').get('v.value');
        var action = component.get("c.getCompiledSearchResults");   // Create the action
        action.setParams({ "searchCriteria" : searchCriteria, "regionAndSegment" : region });	// Pass the search string and region
        action.setCallback(this, function(response) {				// Add callback behavior
        	var state = response.getState();
         	if (state === "SUCCESS") {
                this.fireEventSetPlanningInfo(component, response.getReturnValue());
                component.set("v.SearchCriteria","");
            }
            else {
             	console.log("Failed with state: " + state);
            }
        });
       	$A.enqueueAction(action);	// Send action off to be executed
    },
    getTop30ByRegion : function(component, event) {
		var region = component.find('selectRegion').get('v.value');
        var action = component.get("c.getCompiledDataByRegion");		// Create the action
        action.setParams({ "regionAndSegment" : region });				// Pass the region and segment
        action.setCallback(this, function(response) {					// Add callback behavior
        	var state = response.getState();
         	if (state === "SUCCESS") {
              	this.fireEventSetPlanningInfo(component, response.getReturnValue());
            }
            else {
             	console.log("Failed with state: " + state);
            }
        });
       	$A.enqueueAction(action);											// Send action off to be executed
	},
    fireEventSetPlanningInfo : function(component, loadedPlanningReportInfo) {
        var createEvent = $A.get("e.c:loadPlanningReportInfoEvent");			//get application event
        createEvent.setParams({ "PlanningInfo": loadedPlanningReportInfo });	//set PlanningInfo with the database info
        createEvent.fire();														//fire the event
    }
})