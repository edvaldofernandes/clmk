({
	updateHeader : function(component,event) {
        
        var action = component.get("c.getIndicatorsHeader");
        console.log(action);
        action.setCallback(this, function(response) {
            console.log(response.getState());
            if(response.getState() === 'SUCCESS'){
                var list = response.getReturnValue();
                console.log(list);
            	component.set("v.headerValues", list);
            }
            else{
                console.log('Callback Error');
            }

        })
        $A.enqueueAction(action);
		
	}
})