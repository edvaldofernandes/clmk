({
	doInit : function(component, event, helper) {
        // Prepare the action to load case record
        var str = component.get("v.email.TextBody");
        var res = str.slice(0, 250);
        component.set("v.textOut", res); 
    }
})