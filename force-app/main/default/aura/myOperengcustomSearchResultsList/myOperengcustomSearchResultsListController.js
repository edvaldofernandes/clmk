({
    doInit: function(component, event, helper) {
        var idsJson = sessionStorage.getItem('customSearch--records');
        if (!$A.util.isUndefinedOrNull(idsJson)) {
            
            var action = component.get("c.deserializeJson");
        	action.setParams({"jsonString": idsJson});
            
            action.setCallback(this, function(result) {
                var records = result.getReturnValue();
                component.set("v.allCases", records);
                component.set("v.maxPage", Math.floor((records.length+9)/10));
                helper.sortBy(component, "Status");
                helper.sortBy(component, "Status");
                sessionStorage.removeItem('customSearch--records');
                if(!records || records.length === 0){
                    component.set("v.noResultsText", 'No results found.');
                } else {
                    component.set("v.noResultsText", ' ');
                }
            });
        	$A.enqueueAction(action);
            
        } else {
            component.set("v.noResultsText", 'No results found.');
        }
    },
    sortByStatus: function(component, event, helper) {
        helper.sortBy(component, "Status");
    },
    sortBySubject: function(component, event, helper) {
        helper.sortBy(component, "Subject");
    },
    sortByCreatedDate: function(component, event, helper) {
        helper.sortBy(component, "CreatedDate");
    },
    sortByClosedDate: function(component, event, helper) {
        helper.sortBy(component, "ClosedDate");
    },
    sortByEmail: function(component, event, helper) {
        helper.sortBy(component, "Incoming_email_i__c");
    },
    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    }
    
    
})