({
    handleClick : function(component, event, helper) {
        //var searchText = component.get('v.searchText');
        var searchText = document.getElementById('input_search').value;
        var action = component.get('c.searchForIds');
        action.setParams({searchText: searchText});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === 'SUCCESS') {
                var ids = response.getReturnValue();
                sessionStorage.setItem('customSearch--records', JSON.stringify(ids));
                var navEvt = $A.get('e.force:navigateToURL');
                navEvt.setParams({url: '/custom-search-results'});
                navEvt.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    searchEvents: function(component, event, helper) {
        if(event.which == 13){
            //var searchText = component.get('v.searchText');
            var searchText = document.getElementById('input_search').value;
            var action = component.get('c.searchForIds');
            action.setParams({searchText: searchText});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var ids = response.getReturnValue();
                    sessionStorage.setItem('customSearch--records', JSON.stringify(ids));
                    var navEvt = $A.get('e.force:navigateToURL');
                    navEvt.setParams({url: '/custom-search-results'});
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);
        }
    }
})