({
    sortBy: function(component, field) {
        var sortDesc = component.get("v.sortDesc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.allCases");
        sortDesc = sortField != field || !sortDesc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortDesc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortDesc", sortDesc);
        component.set("v.sortField", field);
        component.set("v.allCases", records);
        this.renderPage(component);
    },
	renderPage: function(component) {
		var records = component.get("v.allCases"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*20, pageNumber*20);
        component.set("v.currentList", pageRecords);
	}
    
})