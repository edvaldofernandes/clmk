({
    doInit: function(component, event, helper) {
        var action = component.get("c.getCases");
        action.setParams({"filter": component.find("filterStatus").get("v.value")});
        component.set("v.Spinner", true);
        action.setCallback(this, function(result) {
            console.log(result.toString());
            var records = result.getReturnValue();
            var state = result.getState();
            component.set("v.allCases", records);
            component.set("v.maxPage", Math.floor((records.length+9)/20)+1);
			component.set("v.Spinner", false);
            helper.sortBy(component, "Status");
            helper.sortBy(component, "Status");
        });
        $A.enqueueAction(action);
	},
    sortByStatus: function(component, event, helper) {
        helper.sortBy(component, "Status");
    },
    sortBySubject: function(component, event, helper) {
        helper.sortBy(component, "Subject");
    },
    sortByCreatedDate: function(component, event, helper) {
        helper.sortBy(component, "CreatedDate");
    },
    sortByClosedDate: function(component, event, helper) {
        helper.sortBy(component, "ClosedDate");
    },
    sortByEmail: function(component, event, helper) {
        helper.sortBy(component, "Incoming_email_i__c");
    },
    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    }  
})