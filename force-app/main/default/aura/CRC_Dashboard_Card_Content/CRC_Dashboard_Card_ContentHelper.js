({
	updateCards : function(component,event) {
		var action = component.get("c.getIndicatorsBody");
        action.setParams({
            cardName : component.get("v.cardName")
        });
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS'){
                console.log('Callback Success');
                var list = response.getReturnValue();
                console.log(list);
                component.set("v.cardData", list);
            }
            else{
                console.log('Callback Error');
            }
            
        })
        $A.enqueueAction(action);
	}
})