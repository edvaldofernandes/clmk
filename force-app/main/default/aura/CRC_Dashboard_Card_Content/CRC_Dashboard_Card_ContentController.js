({
    doInit : function(component, event, helper) {
        helper.updateCards(component,event);
        
        var handleTimer = window.setInterval(
            $A.getCallback(function() { 
                helper.updateCards(component,event);
            }), 30000
        );
    }
})