({
    init: function (cmp, event, helper) {
        //var today = $A.localizationService.formatDate(new Date(), "YYYY");
    	//cmp.set('v.year', today);
    	helper.fetchData(cmp);
    	helper.getYear(cmp);
    	var today =  cmp.get('v.year');
        /*cmp.set('v.columns', [
            {label: 'Models', fieldName: 'Model_Name__c', type: 'text', editable: false, typeAttributes: { required: true }},
            {label: today  , fieldName: 'Year0Quantity__c', type: 'number', editable: true},
            {label: today+1, fieldName: 'Year1Quantity__c', type: 'number', editable: true},
            {label: today+2, fieldName: 'Year2Quantity__c', type: 'number', editable: true},
            {label: today+3, fieldName: 'Year3Quantity__c', type: 'number', editable: true},
            {label: today+4, fieldName: 'Year4Quantity__c', type: 'number', editable: true},
            {label: today+5, fieldName: 'Year5Quantity__c', type: 'number', editable: true},
            {label: today+6, fieldName: 'Year6Quantity__c', type: 'number', editable: true},
            {label: today+7, fieldName: 'Year7Quantity__c', type: 'number', editable: true}
        ]);*/
    },
    handleSaveEdition: function (cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log(draftValues);
        helper.saveEdition(cmp, draftValues);
        cmp.set('v.draftValues',[]);
        helper.fetchData(cmp);
    },
    handleCancelEdition: function (cmp) {
        // do nothing for now...
    }
});