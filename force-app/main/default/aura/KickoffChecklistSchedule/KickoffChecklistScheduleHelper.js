({
    fetchData: function (cmp) {
        var action = cmp.get("c.getScheduleItems");
        action.setParams({
        "recordId": cmp.get("v.recordId")
    	});
        // Register the callback function
    	action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var results = response.getReturnValue();
            cmp.set("v.data", results);
        }
    	});
    	// Invoke the service
    	$A.enqueueAction(action);
    },
    saveEdition: function (cmp, draftValues) {
        var action = cmp.get("c.saveEdits");
        action.setParams({"acs" : draftValues});
        action.setCallback(this, function(response) {
            var state = response.getState();
            //var results = response.getReturnValue();
            //cmp.set("v.data", results);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": "The record has been updated successfully if not submitted yet.",
                "type": "success"
            });
    		toastEvent.fire();
        });
        $A.enqueueAction(action);
    },
    getYear: function(cmp){
        var action = cmp.get("c.getYear");
        action.setParams({
        "recordId": cmp.get("v.recordId")
    	});
        // Register the callback function
    	action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var today = response.getReturnValue();
            cmp.set('v.columns', [
            {label: 'Models', fieldName: 'Model_Name__c', type: 'text', editable: false, typeAttributes: { required: true }},
            {label: today  , fieldName: 'Year0Quantity__c', type: 'number', editable: true},
            {label: today+1, fieldName: 'Year1Quantity__c', type: 'number', editable: true},
            {label: today+2, fieldName: 'Year2Quantity__c', type: 'number', editable: true},
            {label: today+3, fieldName: 'Year3Quantity__c', type: 'number', editable: true},
            {label: today+4, fieldName: 'Year4Quantity__c', type: 'number', editable: true},
            {label: today+5, fieldName: 'Year5Quantity__c', type: 'number', editable: true},
            {label: today+6, fieldName: 'Year6Quantity__c', type: 'number', editable: true},
            {label: today+7, fieldName: 'Year7Quantity__c', type: 'number', editable: true}
        ]);
        }
    	});
    	// Invoke the service
    	$A.enqueueAction(action);
    }
});