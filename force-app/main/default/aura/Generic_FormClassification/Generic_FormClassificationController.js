({
  fetchRecord: function (component, event, helper) {
    var recordId = component.get("v.recordId"),
      records = component.get("v.records");

    if (records.length > 0) {
      var mapRecords = records.reduce(function (map, obj) {
        map[obj.Id] = obj;
        return map;
      }, {});

      if (records.length <= 1) {
        var next_btn = component.find("next-button");
        next_btn.set("v.disabled", true);
        var prev_button = component.find("prev-button");
        prev_button.set("v.disabled", true);
      }

      component.set("v.records", records);
      component.set("v.currentRecord", mapRecords[recordId]);
    } else {
      console.log(response.getError()[0]);
    }
  },

  next: function (component, event, helper) {
    var nextId = window.localStorage.getItem("nextId");
    helper.goToRecordId(component, nextId);
  },

  previous: function (component, event, helper) {
    var beforeId = window.localStorage.getItem("beforeId");
    helper.goToRecordId(component, beforeId);
  },

  saveAndNext: function (component, event, helper) {
    component.find("editForm").submit();
    component.set("v.goToNext", true);
  },

  handleSubmitSuccess: function (component, event, helper) {
    if (component.get("v.goToNext")) {
      if (component.find("next-button").get("v.disabled") === false) {
        var nextId = window.localStorage.getItem("nextId");
        helper.goToRecordId(component, nextId);
      } else {
        $A.get("e.force:navigateToURL")
          .setParams({ url: component.get("v.endPage") })
          .fire();
      }
    } else {
      alert("Your record was saved!");
    }
  },

  handleSubmitError: function (component, event, helper) {
    alert(event.getParam("detail"));
  }
});