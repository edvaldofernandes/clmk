({
  goToRecordId: function (component, recordId) {
    var pageReference = {
      type: "standard__recordPage",
      attributes: {
        objectApiName: component.get("v.apiObject"),
        recordId: recordId,
        actionName: "view"
      }
    };
    component.find("navService").navigate(pageReference);
  },

  save: function (currentRecord, action) {
    return new Promise(
      $A.getCallback(function (resolve, reject) {
        action.setParams({
          oosRecord: currentRecord
        });
        action.setCallback(this, function (response) {
          var state = response.getState();
          if (state === "SUCCESS") {
            var value = response.getReturnValue();
            if (value == true) {
              resolve(value);
            } else {
              reject("Please, check if the inputs are properly filled.");
            }
          } else {
            reject("It seems the server could not get the object properly");
          }
        });
        $A.enqueueAction(action);
      })
    );
  }
});