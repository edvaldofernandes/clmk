public class CRM360_FinanceController {

    public CRM360_FinanceController(){
        this.RecordTypeIdForCreditRequest = Schema.SObjectType.InputForm__c.getRecordTypeInfosByName().get('CREDIT REQUEST FORM').getRecordTypeId();
        getMfirs();
    }
    
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private Account acc = [SELECT Name, Id, COD_ORGz__c FROM Account WHERE Id = :accId];
    
    public Id RecordTypeIdForCreditRequest {get; set;}

	
    List<MFIR__c> mfirs = new list<MFIR__c>();
    
    public Decimal receivables {get; set;}
    public Decimal toBeDue {get; set;}
    public Decimal overdue {get; set;}
    public Decimal dispute {get; set;}
    public Decimal toBeApplied {get; set;}
    public Decimal creditLimit {get; set;}
    public Decimal exposure {get; set;}
    
    public Account getThisAccount(){
        return this.acc;
    }
    
    public List<MFIR__c> getMfirs (){
        this.toBeDue = 0;
        this.overdue = 0;
        this.dispute = 0;
        this.toBeApplied = 0;
        this.creditLimit = 0;
        this.exposure = 0;
        this.mfirs = [SELECT LastModifiedDate, MFIR_number__c, To_be_applied_QK__c, Block_Status_QK__c, Credit_Limit_QK__c, Dispute_QK__c, Exposure_QK__c, Overdue_QK__c, To_be_due_QK__c FROM MFIR__c mfir WHERE Account__c = :accId];
        if(this.mfirs.size() != 0){
            for(MFIR__c mfir : this.mfirs){
                If(mfir.To_be_due_QK__c != null && mfir.Overdue_QK__c != null && mfir.Dispute_QK__c != null && mfir.To_be_applied_QK__c != null && mfir.Credit_Limit_QK__c != null && mfir.Exposure_QK__c != null){
                       this.toBeDue += mfir.To_be_due_QK__c;
                       this.overdue += mfir.Overdue_QK__c;
                       this.dispute += mfir.Dispute_QK__c;
                       this.toBeApplied += mfir.To_be_applied_QK__c;
                       this.creditLimit += mfir.Credit_Limit_QK__c;
                       this.exposure += mfir.Exposure_QK__c;
                       
                   }
            }
            this.toBeDue = this.toBeDue/1000;
            this.overdue = this.overdue/1000;
            this.dispute = this.dispute/1000;
            this.toBeApplied = this.toBeApplied/1000;
            this.receivables = this.toBeDue + this.overdue;
            this.creditLimit = this.creditLimit/1000;
            this.exposure = this.exposure/1000;
        }
        return this.mfirs;
    }
}