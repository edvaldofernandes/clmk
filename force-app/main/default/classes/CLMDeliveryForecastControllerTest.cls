@isTest
public class CLMDeliveryForecastControllerTest { 
  
    static testMethod void myUnitTest(){  
        RecordType recordTypeProposal = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Proposal' limit 1];
        RecordType recordType2Purchase = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Purchase_Agreement' limit 1];
        RecordType recordTypeProduct2 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
        
        Account account = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        
        List<Product2> lstAircraft = new List<Product2>();
        for (Integer i = 0; i < 6; i++){
            Product2 aircraft = new Product2();
            aircraft.RecordTypeId = recordTypeProduct2.id;
            if (i < 3){
                aircraft.Name = 'E1_10001';
                aircraft.Aircraft_Family__c = 'E1';
                aircraft.DF_Grouping__c = '170/175';
                lstAircraft.add(aircraft);
            } else {
                aircraft.Name = 'E2_10002';
                aircraft.Aircraft_Family__c = 'E2';
                aircraft.DF_Grouping__c = '190/195';
                lstAircraft.add(aircraft);                    
            }
        }

        Database.insert(lstAircraft); 
        
        Agreement__c apttusAPTSAgreement = new Agreement__c();
        apttusAPTSAgreement.Name = 'Teste 1';
        apttusAPTSAgreement.Account__c = account.Id;
        apttusAPTSAgreement.Status_Category__c = 'Proposal Signed';
        apttusAPTSAgreement.Country__c = 'Brazil';
        apttusAPTSAgreement.RecordTypeId = recordTypeProposal.id;
        apttusAPTSAgreement.RecordType = recordTypeProposal;
        apttusAPTSAgreement.Agreement_Code__c = 'EMB';
        
        Database.insert(apttusAPTSAgreement);
        
        Agreement__c apttusAPTSAgreement2 = new Agreement__c();
        apttusAPTSAgreement2.Name = 'Teste 2';
        apttusAPTSAgreement2.Account__c = account.Id;
        apttusAPTSAgreement2.Status_Category__c = 'PA Signed';
        apttusAPTSAgreement2.Country__c = 'Brazil';
        apttusAPTSAgreement2.RecordTypeId = recordType2Purchase.id;
        apttusAPTSAgreement2.RecordType = recordType2Purchase;
        apttusAPTSAgreement2.Agreement_Code__c = 'EMB';
    
        Database.insert(apttusAPTSAgreement2);
        
        Aircraft_Price__c ap = new Aircraft_Price__c();
        ap.Model__c = lstAircraft.get(0).id;
        ap.Aircraft_Version__c = '';
        ap.Economic_Condition__c = ''; 
        ap.Commercial_Agreement__c = apttusAPTSAgreement.Id;
        ap.Aircraft_list_Price__c = 0;
        ap.Aircraft_Basic_Price__c = 0;
        Database.insert(ap);
        
        Aircraft_Price__c ap2 = new Aircraft_Price__c();
        ap2.Model__c = lstAircraft.get(1).id;
        ap2.Aircraft_Version__c = 'IGW';
        ap2.Economic_Condition__c = 'JAN/2018'; 
        ap2.Commercial_Agreement__c = apttusAPTSAgreement.Id;
        ap2.Aircraft_list_Price__c = 10000;
        ap2.Aircraft_Basic_Price__c = 60;
        Database.insert(ap2);
        
        //LRsM 03/Ago/2017 - MÉTODO JÁ ESTAVA COMENTADO ANTES DE 03/AGO/2017
        
        /*Apttus__AgreementLineItem__c lineItem1 = new Apttus__AgreementLineItem__c();
        lineItem1.Aircraft__c = lstAircraft.get(0).id;
        lineItem1.Aircraft__r = lstAircraft.get(0);
        lineItem1.Current_Snapshot_Version__c = 0;
        lineItem1.Apttus__AgreementId__c = apttusAPTSAgreement.id;
        lineItem1.Apttus__AgreementId__r = apttusAPTSAgreement;
        lineItem1.TREND_Delivery_Date__c = Date.today().addMonths(5);
        lineItem1.Contractual_Delivery_Month__c = Date.today().addMonths(5);
        lineItem1.Show_Aircraft_As__c = 'Firm';
        lineItem1.Order_Type__c = 'Firm';
        lineItem1.Aircraft_Price__c = ap.Id;  
        
        Database.insert(lineItem1);
        
        Apttus__AgreementLineItem__c lineItem2 = new Apttus__AgreementLineItem__c();
        lineItem2.Aircraft__c = lstAircraft.get(1).id;
        lineItem2.Aircraft__r = lstAircraft.get(1);
        lineItem2.Current_Snapshot_Version__c = 0;
        lineItem2.Apttus__AgreementId__c = apttusAPTSAgreement.id;
        lineItem2.Apttus__AgreementId__r = apttusAPTSAgreement;
        lineItem2.TREND_Delivery_Date__c = Date.today().addMonths(6);
        lineItem2.Contractual_Delivery_Month__c = Date.today().addMonths(6);
        lineItem2.Show_Aircraft_As__c = 'Option';
        lineItem2.Order_Type__c = 'Firm';
        lineItem2.Aircraft_Price__c = ap.Id;
        
        Database.insert(lineItem2);
        
        Apttus__AgreementLineItem__c lineItem3 = new Apttus__AgreementLineItem__c();
        lineItem3.Aircraft__c = lstAircraft.get(2).id;
        lineItem3.Aircraft__r = lstAircraft.get(2);
        lineItem3.Current_Snapshot_Version__c = 0;
        lineItem3.Apttus__AgreementId__c = apttusAPTSAgreement.id;
        lineItem3.Apttus__AgreementId__r = apttusAPTSAgreement;
        lineItem3.TREND_Delivery_Date__c = Date.today().addMonths(7);
        lineItem3.Contractual_Delivery_Month__c = Date.today().addMonths(7);
        lineItem3.Show_Aircraft_As__c = 'Proposal';
        lineItem3.Order_Type__c = 'Firm';
        lineItem3.Aircraft_Price__c = ap.Id;

        
        Database.insert(lineItem3);
        
        Apttus__AgreementLineItem__c lineItem4 = new Apttus__AgreementLineItem__c();
        lineItem4.Aircraft__c = lstAircraft.get(3).id;
        lineItem4.Aircraft__r = lstAircraft.get(3);
        lineItem4.Current_Snapshot_Version__c = 0;
        lineItem4.Apttus__AgreementId__c = apttusAPTSAgreement.id;
        lineItem4.Apttus__AgreementId__r = apttusAPTSAgreement;
        lineItem4.TREND_Delivery_Date__c = Date.today().addMonths(8);
        lineItem4.Contractual_Delivery_Month__c = Date.today().addMonths(8);
        lineItem4.Show_Aircraft_As__c = 'Hide';
        lineItem4.Order_Type__c = 'Firm';
        lineItem4.Aircraft_Price__c = ap.Id;
        
        
        Database.insert(lineItem4);*/
        

        Agreement_Aircraft__c lineItem5 = new Agreement_Aircraft__c();
        lineItem5.Aircraft__c = lstAircraft.get(1).id;
        lineItem5.Aircraft__r = lstAircraft.get(1);
        lineItem5.Status__c = 'Planned';
        lineItem5.Current_Snapshot_Version__c = 0;
        lineItem5.Agreement__c = apttusAPTSAgreement.id;
        lineItem5.Agreement__r = apttusAPTSAgreement;
        lineItem5.TREND_Delivery_Date__c = Date.today().addMonths(9);
        lineItem5.Contractual_Delivery_Month__c = Date.today().addMonths(9);
        lineItem5.Show_Aircraft_As__c = null;
        lineItem5.Order_Type__c = 'Firm';
        lineItem5.Aircraft_Price__c = ap2.Id;  

       
        Database.insert(lineItem5);
        
        Agreement_Aircraft__c lineItem6 = new Agreement_Aircraft__c();
        lineItem6.Aircraft__c = lstAircraft.get(1).id;
        lineItem6.Aircraft__r = lstAircraft.get(1);
        lineItem6.Status__c = 'Planned';
        lineItem6.Current_Snapshot_Version__c = 0;
        lineItem6.Agreement__c = apttusAPTSAgreement.id;
        lineItem6.Agreement__r = apttusAPTSAgreement;
        lineItem6.TREND_Delivery_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem6.Contractual_Delivery_Month__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem6.Show_Aircraft_As__c = null;
        lineItem6.Order_Type__c = 'Option';
        lineItem6.Aircraft_Price__c = ap2.Id;  
        
        Database.insert(lineItem6);
        
        String lineItensId = '';
        List<Agreement_Aircraft__c> lineItensList = new List<Agreement_Aircraft__c>();
        //lineItensList.add(lineItem1);
        //lineItensList.add(lineItem2);
        //lineItensList.add(lineItem3);
        //lineItensList.add(lineItem4);
        lineItensList.add(lineItem5);
        lineItensList.add(lineItem6);
        
        For(Agreement_Aircraft__c lineItem : lineItensList){
            lineItensId += lineItem.id + ';';
        }
        System.debug('CLMDeliveryForecastControllerTest lineItensList' + lineItensList);
        
        List<Production_Capability__c> pcs = new List<Production_Capability__c>();        
        for(Integer i = 0; i < 6; i++){
            Production_Capability__c pc = new Production_Capability__c();
            pc.Aircraft_Model__c = lstAircraft.get(i).id;
            pc.Aircraft_Model__r = lstAircraft.get(i);
            pc.Aircraft_Model__r.Name = lstAircraft.get(i).Name;
            pc.Aircraft_Model__r.DF_Grouping__c = lstAircraft.get(i).DF_Grouping__c;
            pc.Production_Capability__c = 10.0;
            pc.Aircraft_Model__r.Aircraft_Family__c = lstAircraft.get(i).Aircraft_Family__c;
            pc.Delivery_s_Date__c = Date.today().addMonths(i);
            pcs.add(pc);
        }
        Database.insert(pcs);
        
        CLMDeliveryForecastController clmTable = new CLMDeliveryForecastController();
        CLMDeliveryForecastController.InformationByMonthTotals info = new CLMDeliveryForecastController.InformationByMonthTotals(12, 2);
        CLMDeliveryForecastController.InformationByMonth info2 = new CLMDeliveryForecastController.InformationByMonth();
        clmTable.lineItensId = lineItensId;
        clmTable.getAgreementLineItens();
        clmTable.mapModeloCapability = new Map<String, Production_Capability__c>();
        clmTable.getCapability(lineItensList);
        clmTable.aircraftFamily = 'E1';
        clmTable.fillThePage();
        clmTable.selectedValueFamily = 'E1';
        clmTable.aircraftId = lstAircraft.get(0).id;
        for (Agreement_Aircraft__c lineItem : lineItensList){
            CLMDeliveryForecastController.LineItemView lineItemView = clmTable.fillLineItemView(lineItem);
            System.debug('CLMDeliveryForecastController.lineItemView: ' + lineItemView);
            clmTable.mapLineItemDetails.put(lineItem.Aircraft__r.name, lineItemView);
            clmTable.applyRules(lineItem);
            clmTable.fillMapSubTotal(lineItem);
            CLMDeliveryForecastController.LineItemView view =  clmTable.mapLineItemDetails.get(lineItem.Aircraft__r.Name);
            System.debug('AircraftTable.mapLineItemDetails.contract - Final: ' + view.contract);
            System.debug('AircraftTable.mapLineItemDetails.trend - Final: ' + view.trend);
            System.debug('AircraftTable.mapLineItemDetails.options - Final: ' + view.options);
            System.debug('AircraftTable.mapLineItemDetails.proposal - Final: ' + view.proposal);
        } 
        clmTable.setupFreeze();
        clmTable.showPopUp();
        clmTable.closePopUp();
        clmTable.closePopUpYes();
        clmTable.closePopUp();
        clmtable.setCurrentYear();
        clmTable.printPdf();
        clmTable.ExportRecords();
        
        
        System.debug('AircraftTable.mapSubtotal - Final: ' + clmTable.mapSubtotal);
        System.debug('AircraftTable.mapAircraftNames - Final: ' + clmTable.mapAircraftNames);
        System.assert(clmTable.filteredData);
    }
    
    static testMethod void myUnitTest2(){  
        RecordType recordTypeProposal = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Proposal' limit 1];
        RecordType recordType2Purchase = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Purchase_Agreement' limit 1];
        RecordType recordTypeProduct2 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
        
        Account account = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        
        List<Product2> lstAircraft = new List<Product2>();
        for (Integer i = 0; i < 6; i++){
            Product2 aircraft = new Product2();
            aircraft.RecordTypeId = recordTypeProduct2.id;
            if (i < 3){
                aircraft.Name = 'E1_10001';
                aircraft.Aircraft_Family__c = 'E1';
                aircraft.DF_Grouping__c = '170/175';
                lstAircraft.add(aircraft);
            } else {
                aircraft.Name = 'E2_10002';
                aircraft.Aircraft_Family__c = 'E2';
                aircraft.DF_Grouping__c = '190/195';
                lstAircraft.add(aircraft);                    
            }
        }

        Database.insert(lstAircraft); 
        
        Agreement__c apttusAPTSAgreement = new Agreement__c();
        apttusAPTSAgreement.Name = 'Teste 1';
        apttusAPTSAgreement.Account__c = account.Id;
        apttusAPTSAgreement.Status_Category__c = 'Proposal Signed';
        apttusAPTSAgreement.Country__c = 'Brazil';
        apttusAPTSAgreement.RecordTypeId = recordTypeProposal.id;
        apttusAPTSAgreement.RecordType = recordTypeProposal;
        apttusAPTSAgreement.Agreement_Code__c = 'EMB';
        
        
        Database.insert(apttusAPTSAgreement);
        
        
        Agreement__c apttusAPTSAgreement1 = new Agreement__c();
        apttusAPTSAgreement1.Name = 'Teste 1';
        apttusAPTSAgreement1.Account__c = account.Id;
        apttusAPTSAgreement1.Status_Category__c = 'Proposal Signed';
        apttusAPTSAgreement1.Country__c = 'Brazil';
        apttusAPTSAgreement1.RecordTypeId = recordTypeProposal.id;
        apttusAPTSAgreement1.RecordType = recordTypeProposal;
        apttusAPTSAgreement1.Agreement_Code__c = 'EMB';
        
        
        Database.insert(apttusAPTSAgreement1);
        
        Aircraft_Price__c ap = new Aircraft_Price__c();
        ap.Model__c = lstAircraft.get(5).id;
        ap.Aircraft_Version__c = '';
        ap.Economic_Condition__c = ''; 
        ap.Commercial_Agreement__c = apttusAPTSAgreement.Id;
        ap.Aircraft_list_Price__c = 0;
        ap.Aircraft_Basic_Price__c = 0;
        Database.insert(ap);
        
        Aircraft_Price__c ap2 = new Aircraft_Price__c();
        ap2.Model__c = lstAircraft.get(5).id;
        ap2.Aircraft_Version__c = '';
        ap2.Economic_Condition__c = ''; 
        ap2.Commercial_Agreement__c = apttusAPTSAgreement1.Id;
        ap2.Aircraft_list_Price__c = 0;
        ap2.Aircraft_Basic_Price__c = 0;
        Database.insert(ap2);
        
        Agreement_Aircraft__c lineItem5 = new Agreement_Aircraft__c();
        lineItem5.Aircraft__c = lstAircraft.get(5).id;
        lineItem5.Aircraft__r = lstAircraft.get(5);
        lineItem5.Current_Snapshot_Version__c = 0;
        lineItem5.Agreement__c = apttusAPTSAgreement.id;
        lineItem5.Agreement__r = apttusAPTSAgreement;
        lineItem5.TREND_Delivery_Date__c = Date.today().addMonths(9);
        lineItem5.Contractual_Delivery_Month__c = Date.today().addMonths(9);
        lineItem5.Show_Aircraft_As__c = 'Firm';
        lineItem5.Order_Type__c = 'Firm';
        lineItem5.Status__c = 'Planned';
        lineItem5.Aircraft_Price__c = ap.Id;
      //  lineItem5.TREND_DF_Code__c= 'teste';

       
        Database.insert(lineItem5);
        
        Agreement_Aircraft__c lineItem6 = new Agreement_Aircraft__c();
        lineItem6.Aircraft__c = lstAircraft.get(5).id;
        lineItem6.Aircraft__r = lstAircraft.get(5);
        lineItem6.Current_Snapshot_Version__c = 0;
        lineItem6.Agreement__c = apttusAPTSAgreement1.id;
        lineItem6.Agreement__r = apttusAPTSAgreement1;
        lineItem6.TREND_Delivery_Date__c = Date.today().addMonths(9);
        lineItem6.Contractual_Delivery_Month__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem6.Show_Aircraft_As__c = 'Option';
        lineItem6.Order_Type__c = 'Option';
        lineItem6.Status__c = 'Planned';
        lineItem6.Aircraft_Price__c = ap2.Id;  
        //lineItem6.TREND_DF_Code__c= 'teste';
        
        Database.insert(lineItem6);
        
        String lineItensId = '';
        List<Agreement_Aircraft__c> lineItensList = new List<Agreement_Aircraft__c>();
        
        lineItensList.add(lineItem5);
        lineItensList.add(lineItem6);
        
        For(Agreement_Aircraft__c lineItem : lineItensList){
            lineItensId += lineItem.id + ';';
        }
        System.debug('CLMDeliveryForecastControllerTest lineItensList' + lineItensList);
        
        List<Production_Capability__c> pcs = new List<Production_Capability__c>();        
        for(Integer i = 0; i < 6; i++){
            Production_Capability__c pc = new Production_Capability__c();
            pc.Aircraft_Model__c = lstAircraft.get(i).id;
            pc.Aircraft_Model__r = lstAircraft.get(i);
            pc.Aircraft_Model__r.Name = lstAircraft.get(i).Name;
            pc.Aircraft_Model__r.DF_Grouping__c = lstAircraft.get(i).DF_Grouping__c;
            pc.Production_Capability__c = 10.0;
            pc.Aircraft_Model__r.Aircraft_Family__c = lstAircraft.get(i).Aircraft_Family__c;
            pc.Delivery_s_Date__c = Date.today().addMonths(i);
            pcs.add(pc);
        }
        Database.insert(pcs);
        
        CLMDeliveryForecastController clmTable = new CLMDeliveryForecastController();
        CLMDeliveryForecastController.InformationByMonthTotals info = new CLMDeliveryForecastController.InformationByMonthTotals(12, 2);
        CLMDeliveryForecastController.InformationByMonth info2 = new CLMDeliveryForecastController.InformationByMonth();
        clmTable.lineItensId = lineItensId;
        clmTable.getAgreementLineItens();
        clmTable.mapModeloCapability = new Map<String, Production_Capability__c>();
        clmTable.getCapability(lineItensList);
        clmTable.aircraftFamily = 'E2';
        clmTable.fillThePage();
        clmTable.selectedValueFamily = 'E1';
        clmTable.aircraftId = lstAircraft.get(5).id; 
        for (Agreement_Aircraft__c lineItem : lineItensList){
            CLMDeliveryForecastController.LineItemView lineItemView = clmTable.fillLineItemView(lineItem);
            System.debug('CLMDeliveryForecastController.lineItemView: ' + lineItemView);
            clmTable.mapLineItemDetails.put(lineItem.Aircraft__r.name, lineItemView);
            clmTable.applyRules(lineItem);
            clmTable.fillMapSubTotal(lineItem);
            CLMDeliveryForecastController.LineItemView view =  clmTable.mapLineItemDetails.get(lineItem.Aircraft__r.Name);
            System.debug('AircraftTable.mapLineItemDetails.contract - Final: ' + view.contract);
            System.debug('AircraftTable.mapLineItemDetails.trend - Final: ' + view.trend); 
            System.debug('AircraftTable.mapLineItemDetails.options - Final: ' + view.options);
            System.debug('AircraftTable.mapLineItemDetails.proposal - Final: ' + view.proposal);
        } 
    } 


}