/*******************************************************************************
*                             Cloud2b - 2014
*-------------------------------------------------------------------------------
* Trigger responsável por criar registros de Related_accounts__c semelhantes aos originais sendo criados,
* mas com os valores dos campos de relacionamento com Account invertidos, ou seja, os valores dos campos 
* Origin_account__c e Target_account__c do registro original são atribuídos aos campos 
* Target_account__c e Origin_account__c respectivamente do novo registro.
*
* NAME: RelatedAccountCriaRegistro.trigger
* AUTHOR: RNM                                                 DATE: 08/05/2014
*******************************************************************************/
public class RelatedAccountCriaRegistro 
{
    private static map< id, set< id > > recTypeAllow = new map< id, set< id > >{
        RecordTypeMemory.getRecType( 'Account', 'Lessor' )=>new set< id >{
             RecordTypeMemory.getRecType( 'Account', 'Operator' ),
             RecordTypeMemory.getRecType( 'Account', 'MRO' ),
             RecordTypeMemory.getRecType( 'Account', 'Embraer_Site' )
        },
        RecordTypeMemory.getRecType( 'Account', 'Operator' )=>new set< id >{
       RecordTypeMemory.getRecType( 'Account', 'Training_Providers' ),
       RecordTypeMemory.getRecType( 'Account', 'Operator' ),
       RecordTypeMemory.getRecType( 'Account', 'MRO' ),
       RecordTypeMemory.getRecType( 'Account', 'Lessor' ),
       RecordTypeMemory.getRecType( 'Account', 'Embraer_Site' )
    },
    RecordTypeMemory.getRecType( 'Account', 'MRO' )=>new set< id >{
       RecordTypeMemory.getRecType( 'Account', 'Operator' ),
       RecordTypeMemory.getRecType( 'Account', 'Lessor' ),
       RecordTypeMemory.getRecType( 'Account', 'Embraer_Site' )
    },
    RecordTypeMemory.getRecType( 'Account', 'Embraer_Site' )=>new set< id >{
       RecordTypeMemory.getRecType( 'Account', 'Operator' ),
       RecordTypeMemory.getRecType( 'Account', 'Lessor' ),
       RecordTypeMemory.getRecType( 'Account', 'MRO' )
    },
    RecordTypeMemory.getRecType( 'Account', 'Training_Providers' )=>new set< id >{
       RecordTypeMemory.getRecType( 'Account', 'Operator' ),
       RecordTypeMemory.getRecType( 'Account', 'Airworthness_authority' )
    },
    RecordTypeMemory.getRecType( 'Account', 'Airworthness_authority' )=>new set< id >{
       RecordTypeMemory.getRecType( 'Account', 'Training_Providers' )
    }
    };
    
    public static boolean DELETE_IS_RUNNING = false;
    
  public static void processar() 
  {
    TriggerUtils.assertTrigger();
    
    list< Related_accounts__c > lRelatedAccList = new list< Related_accounts__c >();
    
    // Armazena o ID de todas as contas referenciadas
    set< id > lAccListOrig = new set< id >();
    set< id > lAccListTrgt = new set< id >();
    for (Related_accounts__c relatedAcc: (list<Related_accounts__c>)trigger.new) 
    {
        if ( !relatedAcc.Trigger_on__c ) continue;
        lAccListOrig.add( relatedAcc.Origin_account__c );
        lAccListTrgt.add( relatedAcc.Target_account__c );
        lRelatedAccList.add( relatedAcc );
        relatedAcc.Trigger_on__c = false;
    }
    
    if ( lRelatedAccList.isEmpty() ) return;
    
    // Recupera todas os relacionamentos e coloca em um mapa
    set< String > lRelatedExists = new set< String >();
    for ( Related_accounts__c relatedAcc: [ select Origin_account__c, Target_account__c
          from Related_accounts__c where Origin_account__c =:lAccListOrig and Target_account__c=:lAccListTrgt ] )
    {
        lRelatedExists.add( getKey( relatedAcc ) );
    }
    
    // Recupera todas as contas referenciadas
    map< id, Account > lAccMap = new map< id, Account > ( [ select id, recordTypeId from Account where id=:lAccListOrig or id=:lAccListTrgt ] );
    
    // Capturo registros com flag Trigger_on__c selecionada
    // Tal flag determina a criação ou não do novo registro
    list<Related_accounts__c> relatedAccCopyList = new list<Related_accounts__c>();
    for (Related_accounts__c relatedAcc: lRelatedAccList) 
    {
        if ( lRelatedExists.contains( getKey( relatedAcc ) ) )
        {
            relatedAcc.addError( 'There is a relationship for this accounts already' );
            continue;
        }
        
        if ( !validation( lAccMap.get( relatedAcc.Origin_account__c ), lAccMap.get( relatedAcc.Target_account__c ) ) )
        {
            relatedAcc.addError( 'The relationship is not allowed for the account record types' );
          continue;
        }
        
        // Cria uma cópia do registro corrente
        Related_accounts__c relatedAccCopy = relatedAcc.clone(false,true);
          
        // Inverte os valores dos campos de relacionamento com objeto Account
        relatedAccCopy.Acc_Origin_Type__c = '';
        relatedAccCopy.Origin_account__c = relatedAcc.Target_account__c;
        relatedAccCopy.Target_account__c = relatedAcc.Origin_account__c;
          
        relatedAccCopyList.add( relatedAccCopy );
        lRelatedExists.add( getKey( relatedAcc ) );
    }
    
    if(!relatedAccCopyList.isEmpty()) DMLExecution.insertCMD( relatedAccCopyList );
  }
  
  private static string getKey( Related_accounts__c aRelatedAcc )
  {
    return aRelatedAcc.Origin_account__c + '|' + aRelatedAcc.Target_account__c;
  }
  
  private static boolean validation( Account aRecTypeOrigin, Account aRecTypeTarget )
  {
    if ( aRecTypeOrigin == null || aRecTypeTarget == null ) return false;
    set< id > lRecTypeSet = recTypeAllow.get( aRecTypeOrigin.RecordTypeId );
    return lRecTypeSet != null && lRecTypeSet.contains( aRecTypeTarget.RecordTypeId );
  }
  
  public static void excluir()
  {
    TriggerUtils.assertTrigger();
    
    if ( DELETE_IS_RUNNING ) return;
    
    DELETE_IS_RUNNING = true;
    
    // Armazena o ID de todas as contas referenciadas
    set< id > lAccListOrig = new set< id >();
    set< id > lAccListTrgt = new set< id >();
    list< Related_accounts__c > lRelatedAccList = new list< Related_accounts__c >();
    for (Related_accounts__c relatedAcc: (list<Related_accounts__c>)trigger.old) 
    {
      lAccListOrig.add( relatedAcc.Origin_account__c );
      lAccListTrgt.add( relatedAcc.Target_account__c );
      lRelatedAccList.add( relatedAcc );
    }
    
    if ( lRelatedAccList.isEmpty() ) return;
    
    // Recupera todas os relacionamentos e coloca em um mapa
    map< String, Related_accounts__c > lRelatedExists = new map< String, Related_accounts__c >();
    for ( Related_accounts__c relatedAcc: [ select Origin_account__c, Target_account__c
          from Related_accounts__c where Origin_account__c =:lAccListTrgt and Target_account__c=:lAccListOrig ] )
    {
      lRelatedExists.put( getKey( relatedAcc ), relatedAcc );
    }
    
    list< Related_accounts__c > lRelAccDelete = new list< Related_accounts__c >();
    
    for (Related_accounts__c relatedAcc: lRelatedAccList)
    {
        Related_accounts__c lInverso = lRelatedExists.get( relatedAcc.Target_account__c + '|' + relatedAcc.Origin_account__c );
        if ( lInverso != null )
          lRelAccDelete.add( lInverso );
    }
    
    if ( !lRelAccDelete.isEmpty() ) DMLExecution.deleteCMD( lRelAccDelete );
  }
}