public class CLMTaskSetupUtil
{

    public Map<String,Map<String,String>> dateFieldsMap {get; private set;}
    private Map<Id,Task_Setup__c> taskSetupMap;

    private static BusinessHours bh;
    private static Map<Id,Task_Setup__c> taskSetupMapStatic;

    public CLMTaskSetupUtil (Id checklistId)
    {
        // PARAMETRO DE ENTRADA É IGNORADO PARA NÃO ULTRAPASSAR LIMITE DE QUERIES
        //populateDateFieldsMap();
        //loadTaskSetupInfo(checklistId);

        this ();
    }

    public CLMTaskSetupUtil (List<Task> taskList)
    {
        // PARAMETRO DE ENTRADA É IGNORADO PARA NÃO ULTRAPASSAR LIMITE DE QUERIES
        //populateDateFieldsMap();
        //loadTaskSetupInfo(taskList);

        this ();
    }


    public CLMTaskSetupUtil ()
    {
        populateDateFieldsMap();
        taskSetupMap = CLMTaskSetupUtil.getTaskSetupInfoInstance();
    }

    public static Map<Id,Task_Setup__c> getTaskSetupInfoInstance ()
    {

        if (taskSetupMapStatic == null ||  taskSetupMapStatic.isEmpty())
        {

            taskSetupMapStatic = new Map<Id,Task_Setup__c>( [ Select Id, Selected_Date__c, Before_Selected_date__c, Leap_Time__c, Leap_Time_Unit__c  From Task_Setup__c ] );

        }

        return taskSetupMapStatic;
    }


    /**
     * GSAMICO:
     * Função carrega informações de TaskSetup, baseado na
     * Lista de Tasks informada
     *
     * @returns: void
     *
     */
    /*public void loadTaskSetupInfo ( List<Task> taskList )
    {
        Set<String> taskSetupIds = new Set<String>();
        for ( Task task : taskList )
        {
            if(task.Task_Setup__c != null)
            {
                taskSetupIds.add(task.Task_Setup__c);
            }
        }

        taskSetupMap = new Map<Id,Task_Setup__c>( [ Select Id, Selected_Date__c, Before_Selected_date__c, Leap_Time__c, Leap_Time_Unit__c  From Task_Setup__c where Id IN:taskSetupIds ] );
    }*/

    /**
     * GSAMICO:
     * Função carrega informações de TaskSetup, baseado no
     * registro de Questionnaire__c informado
     *
     * @returns: void
     *
     */
    /*public void loadTaskSetupInfo ( Id checklistId )
    {
        List<Questionnaire__c> resultQuery = [Select Id, RecordType.Name  From Questionnaire__c where id =:checklistId ];

        if (resultQuery.size()>0)
        {
            taskSetupMap = new Map<Id,Task_Setup__c>( [ Select Id, Selected_Date__c, Before_Selected_date__c, Leap_Time__c, Leap_Time_Unit__c  From Task_Setup__c where RecordType.Name =:resultQuery.get(0).RecordType.Name ] );
        }

    }*/


    private void populateDateFieldsMap()
    {
        Map<String, Schema.SObjectType> schemaMap;
        Schema.SObjectType schemasObject;
        Map<String, SobjectField> fieldMap = new Map<String, SobjectField>();

        dateFieldsMap = new Map<String,Map<String,String>>();
        schemaMap = Schema.getGlobalDescribe();

        schemasObject = schemaMap.get('Agreement__c');
        fieldMap = schemasObject.getDescribe().fields.getMap();

        for(String field :  fieldMap.keySet())
        {
            if(fieldMap.get(field).getDescribe().getType() == Schema.DisplayType.Date)
                dateFieldsMap.put(fieldMap.get(field).getDescribe().getLabel(),
                                  new Map<String,String>{
                                      'object' => 'Agreement__c',
                                      'object__r' => 'Commercial_Agreement__r',
                                      'field'  => fieldMap.get(field).getDescribe().getName(),
                                      'label' => fieldMap.get(field).getDescribe().getLabel()
                                    }
                                 );

        }

        schemasObject = schemaMap.get('Agreement_Aircraft__c');
        fieldMap = schemasObject.getDescribe().fields.getMap();

        for(String field :  fieldMap.keySet())
        {
            if(fieldMap.get(field).getDescribe().getType() == Schema.DisplayType.Date)
                dateFieldsMap.put(fieldMap.get(field).getDescribe().getLabel(),
                                  new Map<String,String>{
                                      'object' => 'Agreement_Aircraft__c',
                                      'object__r' => 'Contract_Aircraft__r',
                                      'field'  => fieldMap.get(field).getDescribe().getName(),
                                      'label' => fieldMap.get(field).getDescribe().getLabel()
                                    }
                                 );
        }

        return ;

    }

    /**
     * GSAMICO:
     * Função busca informações do campo data do taskSetup informado e retorna um
     * mapeamento contendo:
     * ('objeto' => NOME_DO_OBJETO, 'field' => NOME_DO_FIELD_DE_DATA)
     *
     * @returns: Map<String,String> - Retorna mapeamento contendo informaçoes do campo de data
     *
     */
    public Map<String,String> getFieldInfo ( Id taskSetupId )
    {
        Task_Setup__c taskSetup;

        if ( taskSetupMap == null )
            return null;
        
        taskSetup = taskSetupMap.get( taskSetupId );

        return dateFieldsMap.get(taskSetup.Selected_Date__c);
    }

    /**
     * GSAMICO:
     * Função realiza a operação de adição de data, com base nas informações contidas em um
     * TaskSetup (incrementando ou decrementando dias corridos)
     *
     * @returns: Date - Data somada
     *
     */
    public Date calculateDate (Date dateValue, Id taskSetupId )
    {
        Task_Setup__c taskSetup;
        Integer signal, units;
        String calcType;

        system.debug('calculateDate 00');

        if ( dateValue == null )
        {
            system.debug('calculateDate 01 - retorno null');
            return null;
        }

        if ( taskSetupMap == null || !taskSetupMap.containsKey(taskSetupId) )
        {
            system.debug('calculateDate 02 - retorna mesma data');
            return dateValue;
        }

        system.debug('calculateDate 03 - calculo da data inicio');

        // GET TASK_SETUP VALUES
        taskSetup  = taskSetupMap.get( taskSetupId );
        signal     = taskSetup.Before_Selected_date__c ? -1 : 1;
        units      = ( taskSetup.Leap_Time__c ).intValue();
        calcType   = taskSetup.Leap_Time_Unit__c;

        system.debug('calculateDate 04 - variaveis inicializadas: taskSetup ' + taskSetup);
        system.debug('calculateDate 05 - variaveis inicializadas: signal ' + signal);
        system.debug('calculateDate 06 - variaveis inicializadas: units ' + units);
        system.debug('calculateDate 07 - variaveis inicializadas: calcType ' + calcType);

        // Adição de dias corridos
        if ( calcType == null || calcType.equalsIgnoreCase('calendar days') )
        {
            dateValue = addCalendarDays ( dateValue, units, signal );
        }
        // Adição de dias úteis
        else if ( calcType.equalsIgnoreCase('work days') )
        {
            dateValue = addWorkDays ( dateValue, units, signal );
        }
        // Adição de meses
        else if ( calcType.equalsIgnoreCase('months') )
        {
            dateValue = addMonths ( dateValue, units, signal );
        }
        // Adição de semanas
        else if ( calcType.equalsIgnoreCase('weeks') )
        {
            dateValue = addWeeks ( dateValue, units, signal );
        }

        return dateValue;
    }

    private static BusinessHours getBusinessHoursInstance ()
    {
        system.debug('CLMTaskSetupUtil.getBusinessHoursInstance: inicio: ' + bh);
        if (bh == null)
        {
            system.debug('CLMTaskSetupUtil.getBusinessHoursInstance: Entrando para instanciar: ');
            CLM_Configurations__c cs = CLM_Configurations__c.getInstance('Default');

            // BUSCA BUSINESS HOUR DEFAULT
            if ( cs == null || cs.Business_Hours_Name__c == null || cs.Business_Hours_Name__c.equals('') )
            {
                bh = ([SELECT Id FROM BusinessHours WHERE IsDefault=true]).get(0);
            }
            // BUSCA BUSINESS HOUR DE ACORDO COM A DEFINIÇÃO DO CUSTOM SETTINGS
            else
            {
                bh = ([SELECT Id FROM BusinessHours WHERE Name=:cs.Business_Hours_Name__c]).get(0);
            }
        }

        system.debug('CLMTaskSetupUtil.getBusinessHoursInstance: fim: ' + bh);

        return bh;
    }

    /**
     * GSAMICO:
     * Função realiza o cálculo de adição/subitração de dias corridos para uma
     * determinada data. (Caso o cálculo caia em um dia nao útil, função
     * retorna o dia útil anterior e mais próximo à data calculada)
     *
     * @returns: Date - Resultado de dateValue +/- dias corridos
     *
     */
    private Date addCalendarDays ( Date dateValue, Integer days, Integer signal )
    {
        return getWorkDayBefore ( dateValue.addDays(days*signal) );
    }

    /**
     * GSAMICO:
     * Função realiza o cálculo de adição/subitração de dias úteis para uma
     * determinada data.
     *
     * @returns: Date - Resultado de dateValue +/- dias úteis
     *
     */
    private Date addWorkDays ( Date dateValue, Integer days, Integer signal )
    {
        BusinessHours localBH = CLMTaskSetupUtil.getBusinessHoursInstance();
        Datetime currentDate = BusinessHours.nextStartDate( localBH.Id, Datetime.newInstance(dateValue, Time.newInstance(0, 0, 0, 0)));

        for ( Integer i = 0; i < days; i++ )
        {
            currentDate = currentDate.addDays(signal);
            if (signal>0)
                currentDate = BusinessHours.nextStartDate( localBH.Id, currentDate);
            else
            {
                currentDate = Datetime.newInstance( getWorkDayBefore (Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()) ), Time.newInstance(0, 0, 0, 0) );
            }

        }

        return Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()) ;
    }

    /**
     * GSAMICO:
     * Função realiza o cálculo de adição/subitração de meses para uma
     * determinada data. (Caso o cálculo caia em um dia nao útil, função
     * retorna o dia útil anterior e mais próximo à data calculada)
     *
     * @returns: Date - Resultado de dateValue +/- meses
     *
     */
    private Date addMonths ( Date dateValue, Integer months, Integer signal )
    {
        return getWorkDayBefore ( dateValue.addMonths( months*signal ) );
    }

    /**
     * GSAMICO:
     * Função realiza o cálculo de adição/subitração de semanas para uma
     * determinada data. (Caso o cálculo caia em um dia nao útil, função
     * retorna o dia útil anterior e mais próximo à data calculada)
     *
     * @returns: Date - Resultado de dateValue +/- weeks
     *
     */
    private Date addWeeks ( Date dateValue, Integer weeks, Integer signal )
    {
        return addCalendarDays( dateValue, weeks*7, signal );
    }

    /**
     * GSAMICO:
     * Função retorna o dia util anterior mais próximo à data informada
     * caso a data em questão seja um dia útil, a própria data é retornada.
     *
     * @returns: Date - Dia útil anterior
     *
     */
    private Date getWorkDayBefore ( Date dateValue )
    {
        BusinessHours localBH = CLMTaskSetupUtil.getBusinessHoursInstance();
        system.debug('CLMTaskSetupUtil.getWorkDayBefore: localBH: ' + localBH);
        Datetime nextStartDate = BusinessHours.nextStartDate( localBH.Id, Datetime.newInstance(dateValue, Time.newInstance(0, 0, 0, 0)));

        system.debug('CLMTaskSetupUtil.getWorkDayBefore: nextStartDate: ' + nextStartDate);

        if ( BusinessHours.isWithin(localBH.Id, Datetime.newInstance(dateValue, nextStartDate.time())) )
            return dateValue;
        else
            return getWorkDayBefore ( dateValue.addDays(-1) );
    }

}