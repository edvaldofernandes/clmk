// Used by DIM - Market Intelligence.
@isTest
public class DIM_CaseFeedbackControllerTest {

    //--------------------------------------------------------------------------
    static testMethod void validateEmailNotification() {
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        User user = [SELECT Id, Email FROM User WHERE Name LIKE 'Sales Tools%' LIMIT 1];
    
        Feedback__c feedback = new Feedback__c();
        feedback.Status__c = 'Pending';
        feedback.OwnerId = user.Id;
        feedback.Case__c = caso.Id;
        insert feedback;
        
        DIM_CaseFeedbackController.WrapperClass wc = new DIM_CaseFeedbackController.WrapperClass();
        wc.feedbackId = feedback.Id;
        wc.templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName LIKE 'DIM_Case_Feedback%' LIMIT 1].Id;
        wc.email = user.Email;
        
        Test.startTest();
            DIM_CaseFeedbackController.sendEmail(new List<DIM_CaseFeedbackController.WrapperClass>{wc});
        Test.stopTest();
    }
}