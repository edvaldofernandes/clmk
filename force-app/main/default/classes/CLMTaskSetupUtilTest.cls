@istest
public class CLMTaskSetupUtilTest {

    static testMethod void testDateMethods ()
    {
        
        List<Task_Setup__c> taskSetups = new List<Task_Setup__c>{
            SObjectInstanceTest.createTaskSetup(1991, 'Calendar days', 40, false),
            SObjectInstanceTest.createTaskSetup(1992, 'Calendar days', 60, true),
            SObjectInstanceTest.createTaskSetup(1993, 'Work days',  40, false),
            SObjectInstanceTest.createTaskSetup(1994, 'Work days',  40, true),
            SObjectInstanceTest.createTaskSetup(1995, 'Weeks', 40, false),
            SObjectInstanceTest.createTaskSetup(1996, 'Weeks', 40, true),
            SObjectInstanceTest.createTaskSetup(1997, 'Months',  40, false),
            SObjectInstanceTest.createTaskSetup(1998, 'Months',  40, true)
        };
        
        Database.insert (taskSetups);
        
         
        
        test.startTest();
        
        
        CLMTaskSetupUtil cUtil = new CLMTaskSetupUtil();
        
        Date referenceDate = Date.newInstance ( 2016, 12, 01 );
        
        cUtil.calculateDate ( referenceDate, taskSetups.get(0).Id );
        cUtil.calculateDate ( referenceDate, taskSetups.get(1).Id );
        
        cUtil.calculateDate ( referenceDate, taskSetups.get(2).Id  );
        cUtil.calculateDate ( referenceDate, taskSetups.get(3).Id  );
        
        cUtil.calculateDate ( referenceDate, taskSetups.get(4).Id  );
        cUtil.calculateDate ( referenceDate, taskSetups.get(5).Id  );
        
        cUtil.calculateDate ( referenceDate, taskSetups.get(6).Id );
        cUtil.calculateDate ( referenceDate, taskSetups.get(7).Id  );
        
        test.stopTest();
        
    }

    

}