@isTest
public class myOperengCaseList_Test {
    
    //Unit Test Case for Constructor
    @isTest static void testGetCasesWithContact(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        //Executing test
        System.runAs(testUser[0]){
            List<Case> results = new List<Case>();
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        	Database.insert(testCase[0]);
            results = myOperengCaseList.getCases('All'); 
            results = myOperengCaseList.getCases('Open'); 
            results = myOperengCaseList.getCases('Answered'); 
        }
        Test.stopTest(); 
    }
    
    @isTest static void testGetCasesWithoutContact(){
        Test.startTest();
        //Executing test
        List<Case> results = new List<Case>();
        results = myOperengCaseList.getCases('All'); 
        Test.stopTest(); 
    }
    
     @isTest static void testDeserialization(){
        Test.startTest();
        //Executing test
        String testString = '[{"Id":"5003B0000038vSCQAY","Subject":"Test Case","CreatedDate":"2018-01-22T12:06:29.000Z","Status":"New","Incoming_email_i__c":false,"RecordTypeId":"012i0000001J1JWAA0"},{"Id":"5003B000003BOWoQAO","Subject":"Second test with New Issue tab","CreatedDate":"2018-02-19T17:29:02.000Z","Status":"In Progress","Incoming_email_i__c":false,"RecordTypeId":"012i0000001J1JWAA0"},{"Id":"5003B000003b6CMQAY","Subject":"Teste para o Salesforce","CreatedDate":"2018-03-07T13:12:06.000Z","Status":"New","Incoming_email_i__c":true,"RecordTypeId":"012i0000001J1JWAA0"},{"Id":"5003B000003OniCQAS","Subject":"third new issue","CreatedDate":"2018-02-23T10:55:18.000Z","Status":"New","Incoming_email_i__c":false,"RecordTypeId":"012i0000001J1JWAA0"},{"Id":"5003B000003b6CCQAY","Subject":"teste","CreatedDate":"2018-03-07T13:09:13.000Z","Status":"New","Incoming_email_i__c":true,"RecordTypeId":"012i0000001J1JWAA0"}]';
        List<Case> results = new List<Case>();
        results = myOperengCaseList.deserializeJson(testString); 
        Test.stopTest(); 
    }
    

}