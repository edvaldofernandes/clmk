/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
* When a line item is created or updated this class searchs for available
* slots. If there is an available slot, the class will update the available quantity of
* the slot, acconding to the quantity of the line item and will update the fields
* Slot__c, Slot_Image__c and Slot_status__c of the line item with the success
* information. If there is no available slot, the class will update the fields Slot_Image__c
* and Slot_status__c with the error information.
*
* NAME: LineItemSearchSlot.cls
* AUTHOR: RLdO                                                DATE: 09/03/2015
*******************************************************************************/
public with sharing class LineItemSearchSlot
{
  private static final String recTypeName = 'Training';

  private static Integer ISRUNNING_SLOT = 0;
  private static Boolean ISRUNNING_INSERT = false;
  private static Boolean ISRUNNING_DELETE = false;

  @TestVisible private static final String OUT_OF_RANGE_DATE = 'There are no slot available to date range';
  @TestVisible private static final String MORE_THAN_ONE = 'There are more than one slot available to range date.';
  @TestVisible private static final String THERE_ARENT_AVAILABLE = 'There are not available quantity in this slot';

  private static void buscaSlot( list< SObject > alistOppItem )
  {
    list< Date > llistStart = new list< Date >();
    list< Date > llistEnd = new list< Date >();
    list<Id> llistPricebookEntryId = new list<Id>();

    Id idSlot, idPricebookEntry;
    Date dtStartDate, dtEndDate;
    String sUnit;

    for( SObject lOppItem : alistOppItem )
    {
      idSlot = (Id)lOppItem.get('Slot__c');
      if ( idSlot != null ) lOppItem.put('Slot__c', null);

      dtStartDate = (Date)lOppItem.get('Start_date__c');
      dtEndDate = (Date)lOppItem.get('End_date__c');
      idPricebookEntry = (Id)lOppItem.get('PricebookEntryId');

      llistStart.add( dtStartDate );
      llistEnd.add( dtEndDate );
      llistPricebookEntryId.add( idPricebookEntry );
    }

    if ( llistStart.isEmpty() ) return;

    map<Id, PricebookEntry > mapOliPricebookEntry = new map<Id, PricebookEntry>(
      [SELECT Product2.Product_Type__c FROM PricebookEntry
      WHERE Id =: llistPricebookEntryId ]);

    list< Slots__c > llistSlots = [ select id, Unit__c, Name, Start_date__c, End_date__c, Product_Type__c
      from Slots__c where Start_date__c <=:llistStart or End_date__c >=:llistEnd ];

    if ( llistSlots.isEmpty() ) return;

    map< integer, list< Slots__c > > lmapOppXSlot = new map< integer, list< Slots__c > >();

    integer i = 0;

    for( SObject lOppItem : alistOppItem )
    {
      for ( Slots__c lSlot : llistSlots )
      {
        idPricebookEntry = (Id)lOppItem.get('PricebookEntryId');
        PricebookEntry pbe = mapOliPricebookEntry.get(idPricebookEntry);
        if ( pbe == null ) continue;

        String lProdTypeOli = pbe.Product2.Product_Type__c;
        String lProdType = lSlot.Product_type__c;

        dtStartDate = (Date)lOppItem.get('Start_date__c');
        dtEndDate = (Date)lOppItem.get('End_date__c');
        sUnit = (String)lOppItem.get('Unit__c');

        if ( lSlot.Start_date__c <= dtStartDate  && lSlot.End_date__c >= dtEndDate
          && ( sUnit == lSlot.Unit__c )
          && ( String.isBlank( lProdTypeOli ) || ( String.isNotBlank( lProdType ) && String.isNotBlank( lProdTypeOli ) && lProdType.containsIgnoreCase( lProdTypeOli ) ) )
          )
        {
          list< Slots__c > lOppSlots = lmapOppXSlot.get( i );

          if ( lOppSlots == null )
          {
            lOppSlots = new list< Slots__c >();
            lmapOppXSlot.put( i, lOppSlots );
          }
          lOppSlots.add( lSlot );
        }
      }
      i++;
    }

    integer j = 0;

    for( SObject lOppItem : alistOppItem )
    {
      list< Slots__c > lOppSlots = lmapOppXSlot.get( j );

      if ( lOppSlots == null )
      {
        setSlotInfo( lOppItem, 'Error', OUT_OF_RANGE_DATE );
        continue;
      }
      if ( lOppSlots != null && lOppSlots.size() > 1 )
      {
        String lError = MORE_THAN_ONE;

        for ( Slots__c lSlot : lOppSlots )
          lError += '\n' + lSlot.Name;

        setSlotInfo( lOppItem, 'Error', lError );
        continue;
      }

      lOppItem.put('Slot__c', lOppSlots[ 0 ].Id);
      j++;
    }
  }

  public static void newProducts()
  {
    TriggerUtils.assertTrigger();

    if ( ISRUNNING_INSERT ) return;

    list<SObject> lstOli = new list<SObject>();

    for ( SObject oli : trigger.new )
    {
      if ( TriggerUtils.wasChanged(oli, 'Start_date__c') ||
           TriggerUtils.wasChanged(oli, 'End_date__c') ||
           TriggerUtils.wasChanged(oli, 'Quantity') )
      {
        lstOli.add(oli);
      }
    }
    if ( lstOli.isEmpty() ) return;

    newProducts( lstOli );
  }

  public static void newProducts(list< SObject > lstOppLineItem)
  {
    list<id> lstPricebookEntryId = new list<Id>();
    Id idSlot, idPricebookEntry;

    for (SObject oli : lstOppLineItem )
    {
      idPricebookEntry = (Id)oli.get('PricebookEntryId');
      lstPricebookEntryId.add(idPricebookEntry);
    }

    map<Id, PricebookEntry> mapPbe = new map<Id, PricebookEntry>( [SELECT Id, Product2.RecordType.Name
      FROM PricebookEntry
      WHERE Id =: lstPricebookEntryId ] );
    if ( mapPbe.isEmpty() ) return;

    list<SObject> lstOliTraining = new list<SObject>();

    for (SObject oli : lstOppLineItem )
    {
      idPricebookEntry = (Id)oli.get('PricebookEntryId');
      PricebookEntry pbe = mapPbe.get(idPricebookEntry);
      if ( pbe == null ) continue;
      if ( pbe.Product2.RecordType.Name == recTypeName )
      {
        lstOliTraining.add(oli);
      }
    }

    internalNewProducts(lstOliTraining);
  }

  private static void internalNewProducts(list< SObject > llistOppItem )
  {
    if ( !llistOppItem.isEmpty() ) buscaSlot( llistOppItem );

    list< Id > llistSlotsID = new list< Id >();
    Id idSlot, idPricebookEntry;
    String sSlotStatus;
    Double dQuantity;

    for ( SObject lOppItem : llistOppItem )
    {
      idSlot = (Id)lOppItem.get('Slot__c');
      sSlotStatus = (String)lOppItem.get('Slot_status__c');

      if ( idSlot != null  )
        llistSlotsID.add( idSlot );
      else
      {
        if ( sSlotStatus == null || sSlotStatus == 'Success' )
        {
          setSlotInfo( lOppItem, 'Error', OUT_OF_RANGE_DATE );
        }
      }
    }

    if ( llistSlotsID.isEmpty() ) return;

    map< id, Slots__c > lmaptSlot = new map< id, Slots__c > ( [ select id, Avalilable_quantity__c,
      Reserved_quantity__c from Slots__c where id =:llistSlotsID ] );

    if ( lmaptSlot.isEmpty() ) return;

    for ( SObject lOppItem : llistOppItem )
    {
      idSlot = (Id)lOppItem.get('Slot__c');
      dQuantity = (Double)lOppItem.get('Quantity');

      Slots__c lSlot = lmaptSlot.get( idSlot );
      if ( lSlot == null ) continue;

      if ( lSlot.Avalilable_quantity__c < dQuantity )
      {
        setSlotInfo( lOppItem, 'Warning', THERE_ARENT_AVAILABLE );
        continue;
      }

      setSlotInfo( lOppItem, 'Success', '' );
    }
  }

  public static void setSlotInfo( SObject aOppItem, String aStatus, String aError )
  {
    aOppItem.put('Slot_status__c', aStatus);
    if ( aStatus == 'Success' ) aOppItem.put('Slot_Image__c', '/img/msg_icons/confirm16.png');
    else if ( aStatus == 'Warning' ) aOppItem.put('Slot_Image__c', '/img/msg_icons/warning16.png');
    else if ( aStatus == 'Error' ) aOppItem.put('Slot_Image__c', '/img/msg_icons/error16.png');
    else aOppItem.put('Slot_Image__c', '');
    aOppItem.put('Slot_Errors__c', aError);
  }

  public static void deleteProducts( list< SObject > aLstOppItens )
  {
  	if (Trigger.isUpdate)
  	{
      //for ( SObject lOppItem : aLstOppItens ) lOppItem.put('Slot__c', null);
      database.update(aLstOppItens, false);
  	}
  	else
  	if (Trigger.isDelete)
  	{
      database.delete(aLstOppItens);
  	}
  }

  public static void recalcSlots()
  {
    TriggerUtils.assertTrigger();
    Id idObj, idSlot;
    SObject sobjOld;
    map< Id, Slots__c > mapSlots = new map< Id, Slots__c >();
    list<SObject> lstObj = (Trigger.isDelete) ? trigger.old : trigger.new;

    for ( SObject lOppItem : lstObj )
    {
      idSlot = (Id)lOppItem.get('Slot__c');
      if (idSlot != null) mapSlots.put(idSlot, new Slots__c(Id = idSlot));

      if (Trigger.isDelete || Trigger.oldmap == null) continue;

      idObj = (Id)lOppItem.get('Id');
      sobjOld = Trigger.oldmap.get(idObj);
      if (sobjOld == null) continue;

      idSlot = (Id)sobjOld.get('Slot__c');
      if (idSlot != null) mapSlots.put(idSlot, new Slots__c(Id = idSlot));
    }

    if (!mapSlots.isEmpty()) database.update(mapSlots.values());
  }

  public static void updateQuantity()
  {
    TriggerUtils.assertTrigger();

    ISRUNNING_SLOT++;

    map<Id, Slots__c> mapSlotUpd = new map<Id, Slots__c>();
    Slots__c pSlot;

    for (Slots__c lSlot: (list<Slots__c>)trigger.new)
    {
      if (lSlot.Id == null) continue;

      pSlot = new Slots__c(Id = lSlot.Id, Reserved_Quantity__c = 0);
      mapSlotUpd.put(lSlot.Id, pSlot);
    }

    if (mapSlotUpd.isEmpty()) return;

    for (OpportunityLineItem oli: [select Id, Quantity, Slot__c, Opportunity.IsWon, Opportunity.IsClosed
      from OpportunityLineItem where Slot__c = :mapSlotUpd.keyset()
      and Slot_status__c = 'Success'
      ])
    {
      if ( oli.Opportunity.IsClosed && !oli.Opportunity.IsWon ) continue;
      pSlot = mapSlotUpd.get(oli.Slot__c);

      if (pSlot == null)
      {
        pSlot = new Slots__c(Id = oli.Slot__c, Reserved_Quantity__c = 0);
        mapSlotUpd.put(oli.Slot__c, pSlot);
      }

      pSlot.Reserved_Quantity__c += oli.Quantity;
    }

    for (QuoteLineItem oli: [select Id, Quantity, Slot__c, Quote.Status
      from QuoteLineItem where Slot__c = :mapSlotUpd.keyset()
      and Slot_status__c = 'Success'
      ])
    {
      if ( oli.Quote.Status == 'Denied' ) continue;
      pSlot = mapSlotUpd.get(oli.Slot__c);

      if (pSlot == null)
      {
        pSlot = new Slots__c(Id = oli.Slot__c, Reserved_Quantity__c = 0);
        mapSlotUpd.put(oli.Slot__c, pSlot);
      }

      pSlot.Reserved_Quantity__c += oli.Quantity;
    }

    for (ContractLineItem oli: [select Id, Quantity, Slot__c, ServiceContract.Contract_Status__c
      from ContractLineItem where Slot__c = :mapSlotUpd.keyset()
      and Slot_status__c = 'Success'
      ])
    {
      if ( oli.ServiceContract.Contract_Status__c == 'Dropped' ) continue;
      pSlot = mapSlotUpd.get(oli.Slot__c);

      if (pSlot == null)
      {
        pSlot = new Slots__c(Id = oli.Slot__c, Reserved_Quantity__c = 0);
        mapSlotUpd.put(oli.Slot__c, pSlot);
      }

      pSlot.Reserved_Quantity__c += oli.Quantity;
    }

    for (Slots__c lSlot: (list<Slots__c>)trigger.new)
    {
      pSlot = mapSlotUpd.get(lSlot.Id);
      if (pSlot == null) continue;

    	if (lSlot.Reserved_Quantity__c == pSlot.Reserved_Quantity__c)
    	mapSlotUpd.remove(lSlot.Id);
    }

    if (!mapSlotUpd.isEmpty()) database.update(mapSlotUpd.values());
  }
}