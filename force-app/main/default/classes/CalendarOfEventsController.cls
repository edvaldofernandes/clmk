public class CalendarOfEventsController {
    public CalendarOfEventsController(){
        year = Date.Today().year();
        month= 01;
        day= 01;
        EvType = 'All';
        Site = 'All';
        //getYears();availableYears
        availableTypes = new set<String>();
        filterDate = Date.newInstance(year, month, day);
        updateEventList();
    }
    
    public String EvType {get;set;}
    public List<String> Evtypes = new List<String>();
    public String site {get;set;}
    public List<String> sites = new List<String>();
    public Integer year {get ; set;}
    public Integer month {get ; set;}
    public Integer day {get ; set;}
    public Date filterDate {get; set;}
    
    public set<String> availableTypes {get; set;}
    public set<integer> availableYears {get; set;}
    
    
    public List<CalendarofEvents__c> listOfEvents {get; set;}
    
    public List<CalendarofEvents__c> JANlistOfEvents {get; set;}
    public List<CalendarofEvents__c> FEBlistOfEvents {get; set;}
    public List<CalendarofEvents__c> MARlistOfEvents {get; set;}
    public List<CalendarofEvents__c> APRlistOfEvents {get; set;}
    public List<CalendarofEvents__c> MAYlistOfEvents {get; set;}
    public List<CalendarofEvents__c> JUNlistOfEvents {get; set;}
    public List<CalendarofEvents__c> JULlistOfEvents {get; set;}
    public List<CalendarofEvents__c> AUGlistOfEvents {get; set;}
    public List<CalendarofEvents__c> SEPlistOfEvents {get; set;}
    public List<CalendarofEvents__c> OCTlistOfEvents {get; set;}
    public List<CalendarofEvents__c> NOVlistOfEvents {get; set;}
    public List<CalendarofEvents__c> DEClistOfEvents {get; set;}
    
    public PageReference updateEventList(){
        updateType();
        updateSites();
        listOfEvents    = new List<CalendarofEvents__c>();
        JANlistOfEvents    = new List<CalendarofEvents__c>();
        FEBlistOfEvents    = new List<CalendarofEvents__c>();
        MARlistOfEvents    = new List<CalendarofEvents__c>();
        APRlistOfEvents    = new List<CalendarofEvents__c>();
        MAYlistOfEvents    = new List<CalendarofEvents__c>();
        JUNlistOfEvents    = new List<CalendarofEvents__c>();
        JULlistOfEvents    = new List<CalendarofEvents__c>();
        AUGlistOfEvents    = new List<CalendarofEvents__c>();
        SEPlistOfEvents    = new List<CalendarofEvents__c>();
        OCTlistOfEvents    = new List<CalendarofEvents__c>();
        NOVlistOfEvents    = new List<CalendarofEvents__c>();
        DEClistOfEvents    = new List<CalendarofEvents__c>();
        
        If(EvType == 'All'){}
        
        DEClistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'DECEMBER' order by Date__c asc];       
        NOVlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'NOVEMBER' order by Date__c asc];           
        OCTlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'OCTOBER' order by Date__c asc];
        SEPlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'SEPTEMBER' order by Date__c asc];
        AUGlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'AUGUST' order by Date__c asc];
        JULlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'JULY' order by Date__c asc];
        JUNlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'JUNE' order by Date__c asc];
        MAYlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'MAY' order by Date__c asc];
        APRlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'APRIL' order by Date__c asc];
        MARlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'MARCH' order by Date__c asc];
        FEBlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'FEBRUARY' order by Date__c asc];
        JANlistOfEvents = [Select Id, Name, Date__c, End_Date__c, Where__c, Company_Customer_Name__c,Year__c, Type_of_Event__c, Reference_Month__c, Site__c
                           from CalendarofEvents__c
                           where ((Date__c > :filterDate AND Date__c < :filterDate.addDays(365)) OR Year__c = :String.valueof(Year)) AND Site__c in :Sites
                           AND Type_of_Event__c in :EvTypes AND Reference_Month__c = 'JANUARY' order by Date__c asc];
        return null;
    }
    
    //public List<integer> loadYears(){
    //    List<integer> yearsList = new List<integer>();
    //    yearsList.add(Year);
    //    yearsList.add(Year+1);
    //    yearsList.add(Year+2);
    //    System.debug('YEAR LIST ' + yearsList);
    //    yearsList = CalendarOfEventsYearList__c.getInstance().Available_Years__c.split(',');
    //    List<Integer> intYearList = new list<Integer>();
    //    for(integer year : yearsList){
    //        intYearList.add(year);
    //    }
    //    return yearsList;
    //}
    
    public PageReference updateDate(){
        this.filterDate = Date.newInstance(year, month, day);
        System.debug('Date = ' + this.filterDate);
        
        updateEventList();
        System.debug('Date Update, List = ' + this.listOfEvents);
        return null;
    }
    
    public PageReference updateType(){
        if(EvType == 'All'){
            Schema.DescribeFieldResult fieldResult = CalendarofEvents__c.Type_of_Event__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            EvTypes = new List<String>();
            for( Schema.PicklistEntry f : ple)
            {
                EvTypes.add(f.getValue());
            }
            EvTypes.add('');
            
        } else {
            EvTypes.clear();
            EvTypes.add(EvType);
        }
        return null;
    }
    
    public PageReference updateSites(){
        if(site == 'All'){
            sites = new List<String>();
            sites.add('Asia Pacific');
            sites.add('China');
            sites.add('Europe and Central Asia');
            sites.add('Latin America');
            sites.add('Middle East and Africa');
            sites.add('North America');
        } else {
            sites.clear();
            sites.add(site);
        }
        return null;
    }   
    
    public List<SelectOption> getYears() {
        
        List<SelectOption> options = new List<SelectOption>();
        List<integer> yearsList = new List<integer>();
        System.debug( 'Year: ' + Date.Today().year());

        yearsList.add(year);
        yearsList.add(year+1);
        yearsList.add(year+2);
        System.debug( 'Years: ' + filterDate);
        for(Integer value : yearsList){
            //System.debug( 'Years: ' + value + string.valueof(value));
            
            if(value != null){
                options.add(new SelectOption(string.valueof(value),string.valueof(value)));
            }
        }
        return options;
        
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('2020','2020'));
        options.add(new SelectOption('2021','2021'));
        return options;*/
    } 
    public List<SelectOption> getTypes() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All', 'All'));
        
        Schema.DescribeFieldResult fieldResult = CalendarofEvents__c.Type_of_Event__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    } 
    public List<SelectOption> getSitesL() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All', 'All'));
        
        Schema.DescribeFieldResult fieldResult = CalendarofEvents__c.Site__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    } 
    public List<String> getSites(){
        return this.Sites;
    }    
    public List<String> getEvTypes(){
        return this.Evtypes;
    }
}