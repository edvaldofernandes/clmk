/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public class AccountTriggerHandlerTest  {
    static testMethod void myUnitTest() {
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert acc;
        
        try{
            Account acc1 = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
            acc1.FlyEmbraer_Name__c = 'teste';
            insert acc1;
            
        }
        catch(exception ex)
        {
            
        }
        
        try{
            acc.FlyEmbraer_Name__c = 'teste';
            update acc;
            
        }
        catch(exception ex)
        {
            
        }
        
        delete acc;
        undelete acc;
        
        AccountTriggerHandler handler = new AccountTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
        
    }

}