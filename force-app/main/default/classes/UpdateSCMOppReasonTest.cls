@isTest
private class UpdateSCMOppReasonTest
{


	@TestSetup
	static void setup(){
				Account acc=new Account(Name='test', Company_Nickname__c ='Testinho');
		insert acc;                
		
		//Pricebook2 pb2=[Select id from Pricebook2 limit 1];
		
		//Opportunity testOpp = new Opportunity(Name='TestOppty',closedate=date.parse('1/1/2222'),
		//                    stagename = 'Qualification',AccountID=acc.id,Pricebook2Id=pb2.id);
		//insert testOpp;
		
		id s = Test.getStandardPricebookId();
		
		// create the product
		Product2 p1 = new Product2(
			name='Test Product 1',
			IsActive=true,
			Description='My Product',
			ProductCode='12345'
		);
		insert p1;       
	
		// create the pricebookentry
		PricebookEntry pbe1 = new PricebookEntry(
			Pricebook2Id=s,
			Product2Id=p1.id,
			UnitPrice=0.00,
			IsActive=true,
			UseStandardPrice=false
		);
		insert pbe1;   
	
		// create the opportunity
		Opportunity opp1 = new Opportunity(
			name='Test Opp 1',
		///   recordtypeid='01260000000DXrWxxx',
			StageName = 'Qualification',
			CloseDate = Date.newInstance(2009,01,01), 
			Pricebook2Id=s,
			Reason__c = 'abcde',
			Fleet_Type__c = 'Turboprop'         
		);
		insert opp1;
		
	
		// add the line item
		OpportunityLineItem oli = new OpportunityLineItem();
		oli.Quantity = 1;
		oli.PricebookEntryId = pbe1.id;
		oli.OpportunityId = opp1.id;
		oli.UnitPrice=1;    
		insert oli;
		
		// add the 2nd line item
		OpportunityLineItem oli2 = new OpportunityLineItem();
		oli2.Quantity = 2;
		//oli2.TotalPrice = 2;
		oli2.PricebookEntryId = pbe1.id;
		oli2.OpportunityId = opp1.id;
		oli2.UnitPrice = 2;   
		insert oli2;
		
		ServiceContract sc =new ServiceContract(Name='test sc',AccountID=acc.id,Pricebook2Id=s, Opportunity__c=opp1.id );
		insert sc; 
		
		ContractLineItem cli = new ContractLineItem();
		cli.Quantity = oli.quantity;
		cli.PricebookEntryId = oli.PricebookEntryId;
		cli.ServiceContractId= sc.id;
		cli.UnitPrice= oli.UnitPrice;  
		insert cli;
		
		ContractLineItem cli2 = new ContractLineItem();
		cli2.Quantity = oli2.Quantity;
		cli2.PricebookEntryId = oli2.PricebookEntryId;
		cli2.ServiceContractId= sc.id;
		cli2.UnitPrice= oli2.UnitPrice;  
		insert cli2;
		

		Service_Contract_Management__c contract2 = new Service_Contract_Management__c(Contract_Line_Item_Master__c = cli.Id);
		insert contract2;
	}

	@isTest
	static void itShould()
	{
		// Given
		updateSCMOppReason reason = new updateSCMOppReason();
		database.executeBatch(reason);

		// When


		// Then

	}
}