public class Constants {
    
    public static String ETRACK_GET_QUESTIONS = 'ETRACK_GET_QUESTIONS';
    public static String RCP_OUT_BOUND_PROXY = 'RcpOutBoundProxy';
    
    public static String BASIC_AUTHORIZATION = 'Basic ';
    public static String AUTHORIZATION = 'Authorization';
    
    public static Integer DAYS = 1;
    public static String STATUS_CALL_OUT = 'Received';
    public static String STATUS_RESPONSE = 'SUCCESS';
    
    public static String NEW_OBJECT_CREATED = 'New Object';
    public static String OLD_OBJECT_UPDATED_OR_DELETED = 'Old Object';
    public static String STATUS_ACTIVE = 'Active';
    public static String STATUS_DELETED = 'Deleted';
    
    public static String PAYLOAD_SIZE_ERROR = 'STRING_TOO_LONG, Payload: data value too large:';
    
    public static String RECORD_TYPE_ID = '012i0000001IU3DAAW';
    
    public static String RCP_ACCOUNT = 'RCP_ACCOUNT';
    public static String RCP_AIRCRAFT = 'RCP_AIRCRAFT';
    public static String RCP_TEAM_MEMBER = 'RCP_TEAM_MEMBER';
    
    public static String ETRACK_OUT_BOUND_END_POINT = 'ETRACK_OUT_BOUND_END_POINT';
}