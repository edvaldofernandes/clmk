public class SnapshotAgreementLineItemDAO {
    public static List<Snapshot_Agreement_Line_Item__c> getLineItensBySetIds(String dfVersion, Integer year, String aircraftFamily){

        List<Snapshot_Agreement_Line_Item__c> daoList = new List<Snapshot_Agreement_Line_Item__c>(); 
        String queryString = 'SELECT ' + utils.getAllFields('Snapshot_Agreement_Line_Item__c') +
                             ' FROM Snapshot_Agreement_Line_Item__c WHERE '+
            				 ' DF_Version__c = ' + '\''+ dfVersion+'\''  + ' AND Aircraft_Family__c =' + '\''+  aircraftFamily +'\'' +
            				 ' AND (CALENDAR_YEAR(Delivery_Month__c) =' + year + ' OR CALENDAR_YEAR(TREND_Delivery_Date__c)  = ' + year+')';
        
        System.debug('Snapshot_Agreement_Line_Item__c getLineItensBySetIds QueryString: ' + queryString);
        daoList = database.query(queryString);
        
        System.debug('SnapshotAgreementLineItemDAO getLineItensBySetIds daoList YEAR  : ' +year);
        System.debug('SnapshotAgreementLineItemDAO getLineItensBySetIds daoList aircraftFamily  : ' + aircraftFamily);
        System.debug('SnapshotAgreementLineItemDAO getLineItensBySetIds daoList DFVERSION  : ' + dfVersion);
        return daoList;
    }
    public static List<Snapshot_Agreement_Line_Item__c> getLineItensBySetIds(Set<String> setIds, String dfVersion, Integer year){
        String setToString = ''; 
        for(String includeValue : setIds){
            setToString += '\''+ includeValue.substring(0, 18) + '\',';
        } 
        
        setToString = setToString.removeEnd(',');
        List<Snapshot_Agreement_Line_Item__c> daoList = new List<Snapshot_Agreement_Line_Item__c>();
        String queryString = 'SELECT ' + utils.getAllFields('Snapshot_Agreement_Line_Item__c') +
                             ' FROM Snapshot_Agreement_Line_Item__c WHERE '+
                                                                ' DF_Version__c = ' + '\''+ dfVersion+'\''  +
                                                                ' AND Aircraft__c IN ('+ setToString +')'+
                                                                ' AND CALENDAR_YEAR(Delivery_Month__c) = ' + year +
                                                                ' AND CALENDAR_YEAR(TREND_Delivery_Date__c)  = ' + year;
        System.debug('QueryString: ' + queryString);
        daoList = database.query(queryString);
        
        System.debug('SnapshotAgreementLineItemDAO getLineItensBySetIds daoList SET IDS : ' + daoList);
        return daoList;
    }

}