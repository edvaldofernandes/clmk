public with sharing class SyncLSIETrackOutboundService {
    
    private Map<String, String> aircraftMap;
    private ServicesUtils servicesUtilsNew;
    private List<ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element> etrackQuestionsList;
    private ETRACK_GET_QUESTIONS_NEW.getQuestionsRequest_element etrackGetRequest;
    private ETRACK_GET_QUESTIONS_NEW.getQuestionsRequestSoap11 etrackSend;
    
    public SyncLSIETrackOutboundService(){
        
        initWsService();
        initNativeObject();
    }
    
    private void initWsService(){
        
        etrackGetRequest = new ETRACK_GET_QUESTIONS_NEW.getQuestionsRequest_element();
        etrackSend = new ETRACK_GET_QUESTIONS_NEW.getQuestionsRequestSoap11();
        etrackQuestionsList = new List<ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element>();
    }
    
    private void initNativeObject(){
        
        if(!Test.isRunningTest()){
            aircraftMap = EtrackRepository.findAllAircraft();
        	servicesUtilsNew = new ServicesUtils();
        }
    }
    
    public void execute(){
        
        Date dateFrom  = Date.today() - Constants.DAYS;
        Date dateUntil = Date.today();
                
       	buildBasicAuthentication();
        
        Map<String,ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element> response =  etrackSend.getQuestions(dateFrom,dateUntil);
        Map<String,ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element> res = new Map<String,ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element>();
        	
        res.put('response', response.get('response_x'));
        
        parseToEtrackObject(res.values());
    }
    
    public void parseToEtrackObject(List<ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element> etrackQuestionsList){
        List<ETRACK__c> eTrackObjList = new List<ETRACK__c>();

        for(ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element element : etrackQuestionsList){

            for(ETRACK_GET_QUESTIONS_NEW.questions questions : element.questions){

                ETRACK__c eTrack = new ETRACK__c();
            	eTrack.questionNumber__c     = questions.questionNumber;
            	eTrack.operator__c           = questions.operator;
            	eTrack.priority__c           = questions.priority;
            	eTrack.etdcode__c            = questions.etdcode;
            	eTrack.creationDate__c       = questions.creationDate;
            	eTrack.closeDate__c          = questions.closeDate;
            	eTrack.mro__c                = questions.mro;
            	eTrack.doa__c                = questions.doa;
            	eTrack.dta__c                = questions.dta;
            	eTrack.chargedDisposition__c = questions.chargedDisposition; 
            	eTrack.subject__c            = questions.subject; 
            	eTrack.group_x__c            = questions.group_x;
            	eTrack.category__c           = questions.category;
            	eTrack.program__c            = questions.program;
            	eTrack.ata__c          		 = questions.ata;
            	eTrack.subAta__c             = questions.subAta;
            	eTrack.dueDate__c            = questions.dueDate;
            	eTrack.cdd__c                = questions.cdd;
            	eTrack.ead__c                = questions.ead;
          
            	ETRACK_GET_QUESTIONS_NEW.serialNumber serialNumber = questions.serialnumber;
                    
                if(serialNumber != null){
                   eTrack.serialNumber__c  = finalSerialNumber(serialNumber);
                   validateSerialNumber(eTrack);
                }
            
            	eTrackObjList.add(eTrack);
            }
                
        }
        
        RepositoryTemplate.save(eTrackObjList);
    }
    
    private String finalSerialNumber(ETRACK_GET_QUESTIONS_NEW.serialNumber serialNumber){
        
        String newSerialNumber = ServicesUtils.buildSerialNumber(serialNumber.serialNumberTailNumber);
        String newModel = ServicesUtils.breackSerialNumberReturnModel(newSerialNumber);
        String refactorSn = ServicesUtils.breackSerialNumberReturnSerial(newModel, newSerialNumber);
        String finalSerialNumber = servicesUtilsNew.compleTeWithZero(newModel, refactorSn);
        return finalSerialNumber;
    }
    
    private void validateSerialNumber(ETRACK__c eTrack){
        if(aircraftMap.get(eTrack.serialNumber__c) != null)
            eTrack.Account__c = aircraftMap.get(eTrack.serialNumber__c);
    }
    
    private void buildBasicAuthentication(){
        
        Blob headerValue = Blob.valueOf(WebServiceConfiguration.getAuthentication(Constants.ETRACK_GET_QUESTIONS));
        
        String authorizationHeader = Constants.BASIC_AUTHORIZATION + EncodingUtil.base64Encode(headerValue);
    
        etrackSend.inputHttpHeaders_x = new Map<String, String>();
        etrackSend.inputHttpHeaders_x.put(Constants.AUTHORIZATION,authorizationHeader);
    }
}