@isTest
public class CLMSOBDataWrapperTest {
	static testmethod void test10() {
        // Add the opportunity wrapper objects to a list.
        List <CLMSOBDataWrapper> sobList = new List<CLMSOBDataWrapper>();
        sobList.add( new CLMSOBDataWrapper(new SOB_Data__c(Country__c='Brasil', Customer__c='Jefferson')));
        sobList.add( new CLMSOBDataWrapper(new SOB_Data__c(Country__c='United States', Customer__c='Amanda')));
        sobList.add( new CLMSOBDataWrapper(new SOB_Data__c(Country__c='Japan', Customer__c='Niguiri')));
         
        // Sort the wrapper objects using the implementation of the
        // compareTo method.
        sobList.sort();
         
        // Verify the sort order
        System.assertEquals('Brasil', sobList[0].sob.Country__c);
        System.assertEquals('Jefferson', sobList[0].sob.Customer__c);
        
        System.assertEquals('Japan', sobList[1].sob.Country__c);
        System.assertEquals('Niguiri', sobList[1].sob.Customer__c);

        
        System.assertEquals('United States', sobList[2].sob.Country__c);
        System.assertEquals('Amanda', sobList[2].sob.Customer__c);
       
        
         

        // Write the sorted list contents to the debug log.
        System.debug(sobList);

    }
}