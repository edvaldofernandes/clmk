public class EOCSocialEventsController {  
  
   // Return a list of data points for a chart
    public List<Data> getData() {
        return EOCSocialEventsController.getChartData();
    }
    
    // Make the chart data available via JavaScript remoting
    // @RemoteAction
    //public static List<Data> getRemoteData() {
    //    return EOCSocialEventsController.getChartData();
    //}

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    
    // EOC 2016 Social Event's Name
    public static List<Data> getChartData() {
        
        String welcomeCocktailStr   = 'Welcome Cocktail';
        String specialDinnerStr     = 'Special Dinner';
        String welcomeDinnerStr     = 'Welcome Dinner (MCW/Training Workshop/MMEL)';
        String keynoteSpeakerStr    = 'Keynote Speaker';

        Integer welcomeCocktail     = 0;
        Integer specialDinner       = 0;
        Integer welcomeDinner       = 0;
        Integer keynoteSpeaker      = 0;

        List<CampaignMember> members = [ SELECT Functions_attending_EOCWW15__c                                                                                
                                           FROM CampaignMember 
                                          WHERE campaignID = '7010H000001TAgG' //EOC WW 2018 EJETS | London 
                                            AND Functions_attending_EOCWW15__c != '' ];


        for(CampaignMember member : members) {        
                
            for (String function : member.Functions_attending_EOCWW15__c.split(';')) {
                
                if(function.equals(welcomeCocktailStr))
                    welcomeCocktail++;
                
                if(function.equals(specialDinnerStr))
                    specialDinner++;

                if(function.equals(keynoteSpeakerStr))
                    keynoteSpeaker++;

                if(function.equals(welcomeDinnerStr))
                    welcomeDinner++;
                    
            }    
        }                                            

        //Chart
        List<Data> datas = new List<Data>();

        Data data1 = new Data(welcomeCocktailStr, welcomeCocktail);
        Data data2 = new Data(specialDinnerStr, specialDinner);
        Data data3 = new Data('Welcome Dinner', welcomeDinner);
        Data data4 = new Data(keynoteSpeakerStr, keynoteSpeaker);

        datas.add(data1);
        datas.add(data2);
        datas.add(data3);
        datas.add(data4);
        
        return datas;
    }
    
    // Wrapper class
    public class Data {
    
        public String name { get; set; }
        public Integer value { get; set; }
        
        public Data(String name, Integer value)  {
        
            this.name = name + ' [' + value + '] ';
            this.value = value;            
        }
    }
}