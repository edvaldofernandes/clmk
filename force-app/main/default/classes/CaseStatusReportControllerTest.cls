@isTest
private class CaseStatusReportControllerTest
{
    //CAUTION: This unit test is incomplete and relies on org data (SeeAllData=true)
    //		   Therefore, it may not provide enough coverage when deploying to an org without enough data
    @isTest(SeeAllData=true) 
    static void controllerTest(){
        //create test data
        Id rTypeInbox = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Inbox').getRecordTypeId();
        Case cas = new Case(RecordTypeId = rTypeInbox, Reason='INBOX', Priority='INBOX', Status='Dispatch', Phase__c='Inbox', Origin='Email', SLA_report_status__c='1. Green');
        insert cas;
        //get controller
        CaseStatusReportController controller = new CaseStatusReportController(new ApexPages.StandardController(new Case()));
        //test getlstCasoWrapper
        Test.startTest();
        	List<CasoWrapper> listCW = controller.getlstCasoWrapper();
        Test.stopTest();
        //lst caso wrapper should contain 5 caso wrappers
        system.AssertEquals(listCW.size(),5);
    }
    
    @isTest
    static void CasoWrapperTest(){
        CasoWrapper cw = new CasoWrapper();
        cw.urlReportDay = null;
        cw.urlReportMonth = null;
        System.Assert(true);
    }
  /*static testMethod void myUnitTest()
  {
    //Profile perfil = SObjectInstanceTest.getProfileAdmin();
    //User atendente = SObjectInstanceTest.createUser(perfil.Id);
    //Database.insert(atendente);
        
      Account Conta = SObjectInstanceTest.conta();
      Conta.Type = 'Airline';
      //lConta.BillingCountry = 'Brasil';
      Conta.Company_Nickname__c = 'VanTseng';
      database.insert( Conta );
        
    //SlaProcess sla = [select id from SlaProcess where isActive = true and name like '%SJK Parts%' limit 1];  
    //Entitlement ent = SObjectInstanceTest.createEntitlement(conta.Id);
    
      Entitlement ent = SObjectInstanceTest.createEntitlement( Conta.id );
      ent.Name = 'Default';
      Ent.Type = 'Phone Support';
      database.insert( Ent );
    
    //ent.SlaProcessId = sla.Id;
    //Database.insert(ent);
    
    Case casoINOX = SObjectInstanceTest.createCase();
    casoINOX.AccountId = conta.Id;
    casoINOX.Priority = 'INBOX'; 
    casoINOX.EntitlementId = ent.Id;
    casoINOX.RecordTypeId = CaseStatusReportController.recTypeCaso;
    casoINOX.Phase__C = 'INBOX';
    casoINOX.Status = CaseStatusReportController.DISPATCH;
    Database.insert(casoINOX);
    
        
    Case caso1 = SObjectInstanceTest.createCase();
    caso1.AccountId = conta.Id;
    caso1.Priority = CaseStatusReportController.AOG_NFO; 
    caso1.EntitlementId = ent.Id;
    caso1.RecordTypeId = CaseStatusReportController.TYPE_ECIP;
    caso1.Phase__C = 'Processing';
    caso1.Status = 'Processing';
    Database.insert(caso1);
             
    Case caso2 = SObjectInstanceTest.createCase();
    caso2.AccountId = conta.Id;
    caso1.Priority = CaseStatusReportController.AOG; 
    //caso2.Reason = CaseStatusReportController.PO_QUOTE;
    caso2.EntitlementId = ent.Id;
    caso2.RecordTypeId = CaseStatusReportController.Type_ECIP;
    caso2.Phase__C = 'Processing';
    caso2.Status = 'Processing';
    Database.insert(caso2);
        
    Case caso3 = SObjectInstanceTest.createCase();
    caso3.AccountId = conta.Id;
    caso3.Priority = CaseStatusReportController.CRI; 
    caso3.EntitlementId = ent.Id;
    caso3.RecordTypeId = CaseStatusReportController.TYPE_ECIP;
    caso3.Phase__C = 'Processing';
    caso3.Status = 'Processing';
    Database.insert(caso3);
        
    Case caso4 = SObjectInstanceTest.createCase();
    caso4.AccountId = conta.Id;
    caso4.Priority = CaseStatusReportController.RTN; 
    caso4.EntitlementId = ent.Id;
    caso4.RecordTypeId = CaseStatusReportController.TYPE_ECIP;
    caso4.Phase__C = 'Processing';
    caso4.Status = 'Processing';
    Database.insert(caso4);
        
          
    Test.startTest();
    CaseStatusReportController controller = new CaseStatusReportController(new ApexPages.StandardController(caso1));
    controller.reloadTime=null;	//cover these variables
    controller.isSF1=false;		//cover these variables
    controller.getlstCasoWrapper();
    Test.stopTest();
  }

  static testMethod void testCaseWrapper()
  { 
    //create account
    Account Conta = SObjectInstanceTest.conta();
    Conta.Type = 'Airline';
    //lConta.BillingCountry = 'Brasil';
    Conta.Company_Nickname__c = 'VanTseng';
    database.insert( Conta );
   
    //create entitlement
    Entitlement ent = SObjectInstanceTest.createEntitlement( Conta.id );
    ent.Name = 'Default';
    Ent.Type = 'Phone Support';
    database.insert( Ent );  
    
    //create case
    Case caso1 = SObjectInstanceTest.createCase();
    caso1.AccountId = conta.Id;
    caso1.Priority = CaseStatusReportController.AOG_NFO; 
    caso1.EntitlementId = ent.Id;
    caso1.RecordTypeId = CaseStatusReportController.TYPE_ECIP;
    caso1.Phase__C = 'Processing';
    caso1.Status = 'Processing';
    Database.insert(caso1);
    
    //get caseStatusReportController
    CaseStatusReportController controller = new CaseStatusReportController(new ApexPages.StandardController(caso1));
      
    CaseStatusReportController.CasoWrapper caseW = new CaseStatusReportController.CasoWrapper();
    CaseStatusReportController.CasoWrapper caseWINBOX;
    caseWINBOX = new CaseStatusReportController.CasoWrapper(CaseStatusReportController.INBOX);
    
    //cover all the public members of casoWrapper
    caseW.quantidadeTotal='';
    caseW.quantidadeVerde='';
    caseW.status='';
    caseW.dataH='';
    caseW.hora='';
    caseW.contato='';
    caseW.idContato=null;
    caseW.conta='';
    caseW.idConta=null;
    caseW.atendente='';
    caseW.idAtendente=null;
    caseW.idCaso=null;
    caseW.ownerIsQueue=false;
    caseW.urlReportMonth=null;
    caseW.month = '2';
    caseW.lastDay = '1';
    caseW.quantidadeVermelho='1';
    caseW = controller.verificaStatus(caseW);
    caseW.month='1';
    caseW.lastDay='2';
    caseW.quantidadeAmarelo='1';
    caseW.quantidadeVermelho=null;
    caseW = controller.verificaStatus(caseW); 
    
    Test.startTest();
    system.assert(!caseW.isInbox);
    system.debug(caseWINBOX.urlReportDay);
    // covers static methods
    system.assert(CaseStatusReportController.fmtPercent(2, 3) == '67');
    Test.stopTest();
  }*/
}