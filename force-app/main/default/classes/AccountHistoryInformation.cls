public class AccountHistoryInformation {
     
    private DateTime createdDate {set; get;}
    private String field;
    private String OldValue;
    private String newValue;
    private String createdBy;
    
    public AccountHistoryInformation(DateTime createdDate, String field, String OldValue, String newValue, String createdBy){
        
        this.createdDate = createdDate;
        this.field = field;
        this.OldValue = OldValue;
        this.newValue = newValue;
        this.createdBy = createdBy;
    }
    
    public DateTime getCreatedDate(){
        return this.createdDate;
    }
    
    public String getField(){
        return this.field;
    }
    
    public String getOldValue(){
        return this.OldValue;
    }
    
    public String getNewValue(){
        return this.newValue;
    }
    
    public String getCreatedBy(){
        return this.createdBy;
    }
}