public with sharing class FO_HomeController {

    Private Date startDate = date.newInstance(0001, 1, 1);
    public String dayValue{get;set;}
    public Datetime dataCaseFUP{get;set;}
    public List<Case> cases {get;set;}
    public Integer nCasesIF {get;set;}
    public List<Case> casesFUP {get;set;}
    public List<Task> tasks {get;set;}
    public Integer nCommitments {get;set;}
    public List<wrapper> tasksCases{get;set;}
    public Integer nCaseFUP {get;set;}
    private String soqlCase {get;set;}
    private String soqlCaseFUP {get;set;}
    private String soqlTask {get;set;}
    public String sortDirCase { get  { if (sortDirCase == null) {  sortDirCase = 'asc'; } return sortDirCase;} set; }
    public String sortDirCaseFUP { get  { if (sortDirCaseFUP == null) {  sortDirCaseFUP = 'desc'; } return sortDirCaseFUP;} set; }
    public String sortDirTask { get  { if (sortDirTask == null) {  sortDirTask = 'asc'; } return sortDirTask;} set; }
    public String sortFieldCase { get  { if (sortFieldCase == null) {sortFieldCase = 'FlightOps_Days_to_Milestone__c'; } return sortFieldCase;}set;}
    public String sortFieldCaseFUP { get  { if (sortFieldCaseFUP == null) {sortFieldCaseFUP = 'LastModifiedDate'; } return sortFieldCaseFUP;}set;}
    public String sortFieldTask { get  { if (sortFieldTask == null) {sortFieldTask = 'ActivityDate'; } return sortFieldTask;}set;} 
    String userName = UserInfo.getName(); 
    
    // classe para agrupar uma task e um case no mesmo objeto(wrapper)
    public class wrapper{
        
        public Task t {get;set;}
        public Case c {get;set;}
        
    }
      
    public FO_HomeController () {
        
        List<String> listDay = new List<String>{'Saturday' , 'Sunday' , 'Monday' , 'Tuesday' , 'Wednesday' , 'Thursday' , 'Friday'};
        Date selectedDate = System.today();
        dataCaseFUP = System.now();
        dataCaseFUP = dataCaseFUP.addMonths(-1);
        Integer remainder = Math.mod(startDate.daysBetween(selectedDate) , 7);
        dayValue = listDay.get(remainder);
        
    	soqlCase = 'SELECT CaseNumber,FlightOps_Initial_Feedback__c, FlightOps_KPI_Initial_Feedback__c,FlightOps_Days_to_Milestone__c,Next_Follow_up__c,Owner.Name,AccountId,Subject,Status, SuppliedEmail,Account.Name,CreatedDate FROM Case WHERE RecordType.Name = \'FlightOps Case Record Type\' AND FlightOps_Days_to_Milestone__c!=null ';
    	runCaseQuery();
        
    	//AND Owner.UserRole.Name = \'Flight Operations Liaison (SJK)\''  
    	//What.Type = \'Case\'
    	soqlTask = 'select Activity_Number__c,Description,Subject,Account.Name,X1st_date_indicator__c,Owner.Name,WhatId,Who.Name,ActivityDate,What.Name,Completion_date__c,Customer_Commitment_new__c FROM Task WHERE ActivityDate!=null AND What.RecordType.Name = \'FlightOps Case Record Type\' AND Customer_Commitment_new__c=\'YES\' AND X1st_date_indicator__c = \'Open Task\'';
    	runTaskQuery();
         
        soqlCaseFUP = 'SELECT CaseNumber,LastModifiedDate, Next_Follow_up__c,Owner.Name,AccountId,Subject,Status, SuppliedEmail,Account.Name,CreatedDate FROM Case WHERE RecordType.Name = \'FlightOps Case Record Type\' AND Status = \'In progress\' AND LastModifiedDate < LAST_N_DAYS:27';
        runCaseFUPQuery();
        
  }
    
  //---------------------------------------------Cases--------------------------------------------------

  public void toggleSortCase() {
      
    sortDirCase = sortDirCase.equals('asc') ? 'desc' : 'asc';
    runCaseQuery();
      
  }
  
  public void runCaseQuery() {
    try {
        cases = Database.query(soqlCase + ' order by ' + sortFieldCase + ' ' + sortDirCase + ' limit 50');
        nCasesIF = cases.size();
    } catch (Exception e) {
      
    }
  }
  
  public PageReference runCaseSearch() {
      
    runCaseQuery();
    return null;
      
  }
  
  //--------------------------------------------Tasks-------------------------------------------------
  public void toggleSortTask() {
      
    sortDirTask = sortDirTask.equals('asc') ? 'desc' : 'asc';
    runTaskQuery();
      
  }
    
  public void runTaskQuery() {
      
    try {
        tasksCases = new List<wrapper>();
        tasks = Database.query(soqlTask + ' order by ' + sortFieldTask + ' ' + sortDirTask + ' limit 50');  
        nCommitments = tasks.size();
        
        for(Task tt : tasks){
            
			wrapper w = new wrapper();            
            w.t = [SELECT Activity_Number__c,Description,Subject,Account.Name,X1st_date_indicator__c,Owner.Name,WhatId,Who.Name,ActivityDate,What.Name,Completion_date__c,Customer_Commitment_new__c FROM Task Where Id =: tt.Id];
            w.c = [SELECT Id,CaseNumber,Subject,Account.Name FROM Case WHERE Id =: tt.WhatId];
            tasksCases.add(w);
       
        }
        
    } catch (Exception e) {}      
  }
    
  public PageReference runTaskSearch() {
      
      runTaskQuery();
      return null;
      
    }
    //----------------------------------fim tasks --------------------------------------------------------

  //---------------------------------------------Cases FUP--------------------------------------------------

  public void toggleSortCaseFUP() {
      
    sortDirCaseFUP = sortDirCaseFUP.equals('asc') ? 'desc' : 'asc';
    runCaseFUPQuery();
      
  }
  
  public void runCaseFUPQuery() {
      
    try {
        casesFUP = Database.query(soqlCaseFUP + ' order by ' + sortFieldCaseFUP + ' ' + sortDirCaseFUP + ' limit 50');
        nCaseFUP = casesFUP.size();
    } catch (Exception e) {}
      
  }
  
  public PageReference runCaseFUPSearch() {
      
    runCaseFUPQuery();
    return null;
      
  }
    //-------------------------------------------------------------------------------------------------------
}