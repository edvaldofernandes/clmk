/* 
----------------------------------------------------------------------------------------------
-- - Company:     Embraer
-- - Name:        IndicatorsBodyDTO
-- - Description: Data Transfer Object to carry CRC Dashboard card data from apex to 
-- 				  lightning component.
-- - @Authors: Tiago de Jesus Rodrigues and Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version                   
----------------------------------------------------------------------------------------------
-- 29/03/2019       Felipe Gouvea      				1.0
-- 06/05/2019		Tiago de Jesus Rodrigues		1.1
----------------------------------------------------------------------------------------------
*/

public class IndicatorBodyDTO {
	
    @AuraEnabled
	public string onTime 								{get; set;} // Percentage of CRC cases currently on time. 
    
    @AuraEnabled
	public integer allCaseMonthlyCounter 				{get; set;} // Total number of CRC cases received this month that have already passed through the dispatch status.

    @AuraEnabled
	public integer caseMonthlyCounter 					{get; set;} // Total number of CRC cases received this month, from types Spare Parts, RSPL or Broker, 
    																// that have currently the processing status.

    @AuraEnabled
	public integer onTimeMilestonesCounter 				{get; set;} // Total number of CRC cases received this month, from types Spare Parts, RSPL or Broker, 
    																// that have currently the processing status and a green SLA.

    @AuraEnabled
	public integer warningMilestonesCounter 			{get; set;} // Total number of CRC cases received this month, from types Spare Parts, RSPL or Broker, 
    																// that have currently the processing status and a yellow SLA.

    @AuraEnabled
	public integer delayedMilestonesCounter 			{get; set;} // Total number of CRC cases received this month, from types Spare Parts, RSPL or Broker, 
    																// that have currently the processing status and a red SLA.

    @AuraEnabled
	public integer RFQ_PO_Counter 						{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																//RFQ, PO or Invoice that have currently the processing status.

    @AuraEnabled   
    public integer onTime_RFQ_PO_Counter 				{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																//RFQ, PO or Invoice that have currently the processing status and a green SLA.

    @AuraEnabled    
    public integer warning_RFQ_PO_Counter 				{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																//RFQ, PO or Invoice that have currently the processing status and a yellow SLA.

    @AuraEnabled    
    public integer delayed_RFQ_PO_Counter				{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																//RFQ, PO or Invoice that have currently the processing status and a red SLA.

    @AuraEnabled
	public integer care_Counter 						{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																// Care, RMA or Follow Up that have currently the processing status.

    @AuraEnabled   
    public integer onTime_care_Counter 					{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																// Care, RMA or Follow Up that have currently the processing status and a green SLA.

    @AuraEnabled    
    public integer warning_care_Counter 				{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																// Care, RMA or Follow Up that have currently the processing status and a yellow SLA.
 
    @AuraEnabled   
    public integer delayed_care_Counter 				{get; set;} // Number of CRC cases received this month, from types Spare Parts, RSPL or Broker, with reason
    																// Care, RMA or Follow Up that have currently the processing status and a red SLA.

}