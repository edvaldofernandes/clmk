// Used by DIM - Market Intelligence.
public with sharing class DIM_ApexHelperController {
    
    @AuraEnabled
    public static List<sObject> executeSoql(String soql) {
        return Database.query(soql);
    }
}