/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* class with helper methods to execution of triggers
*
* NAME: TriggerUtils.cls
* AUTHOR: LRSA                                                DATE: 21/01/2014
*
*******************************************************************************/

public with sharing class TriggerUtils {
  
  /*
     Método que verifica se as triggers do determinado objeto estão habilitadas 
     para execução. Retorna true se a trigger está habilitada para execução, caso
     contrário retorna false
     aObjectName: Nome do objeto
  */
  public static boolean isEnabled( String aObjectName )
  {
    Setup_Triggers__c lSetup = Setup_Triggers__c.getValues( aObjectName );
    return lSetup == null || lSetup.Enabled__c;
  }
  
  /*
     Método que verifica se a execução está no contexto de trigger ou no contexto de 
     teste. Se não estiver nesses contextos então uma exceção será gerada
  */
  public static void assertTrigger()
  {
    system.assert( trigger.isExecuting, 'Método chamado fora do contexto de trigger' );
  }
  
  /*
     Método que verifica se houve mudança no determinado campo e se o novo valor é 
     igual ao valor passado como parâmetro
     aObj: Instância do Objeto que está sofrendo a ação
     aField: Definição do campo a ser considerado na condição
     aExpectedValue: Valor esperado como novo valor do campo
  */
  public static boolean wasChangedTo( SObject aObj, Schema.sObjectField aField, Object aExpectedValue )
  {
    assertTrigger();
    if ( !trigger.isExecuting ) return false;
    Object lActualValue = aObj.get( aField );
    return lActualValue == aExpectedValue
       && ( trigger.isInsert || lActualValue != trigger.oldMap.get( aObj.id ).get( aField ) );
  }
  
  /*
     Método que verifica se houve mudança no determinado campo
     aObj: Instância do Objeto que está sofrendo a ação
     aField: Definição do campo a ser considerado na condição
  */
  public static boolean wasChanged( SObject aObj, Schema.sObjectField aField )
  {
    assertTrigger();
    return trigger.isInsert || aObj.get( aField ) != trigger.oldMap.get( aObj.id ).get( aField );
  }
  
  public static boolean wasChanged( SObject aObj, String aField )
  {
    assertTrigger();
    return trigger.isInsert || aObj.get( aField ) != trigger.oldMap.get( aObj.id ).get( aField );
  }
}