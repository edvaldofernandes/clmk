/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ContractItemBuscaSlot.cls
* NAME: ContractLineItemSearchSlotTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*
*******************************************************************************/
@isTest
private class ContractLineItemSearchSlotTest {


  private static final integer QTD_LOTE = 200;
  
  private static Slots__c fSlot;
  
    private static ContractLineItem newOppItem() {
      Id lPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      ServiceContract lOpp = SObjectInstanceTest.createServiceContract();
      lOpp.Validity_Date__c  = system.today().addDays(5);
      lOpp.Submission_Date__c = system.today();
      lOpp.Pricebook2Id = lPB;
      insert lOpp;
      
      Product2 lProd = SObjectInstanceTest.createProduct2();
      lProd.Product_Type__c = 'Training';
      lProd.RecordTypeId = RecordTypeMemory.getRecType( 'Product2', 'Training' );
      insert lProd;
      
      
      PricebookEntry lPBE = SObjectInstanceTest.createPricebookEntry( lPB, lProd.id );
      insert lPBE;
      
      fSlot = SObjectInstanceTest.createSlot();
      insert fSlot;
           
      ContractLineItem sobj = SObjectInstanceTest.createContractLineItem( lOpp.id, lPBE.id );
      return sobj;
    }
    
    static testMethod void addItem()
    { 
      ContractLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );    
      
      Test.StartTest();
      insert lOppItem;
      Test.StopTest();
     
    }
   
    static testMethod void changeItem()
    { 
      ContractLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      insert lOppItem;
      
      Slots__c fSlot2 = SObjectInstanceTest.createSlot();
      insert fSlot2;
      
      fSlot2 = [ select id, Avalilable_quantity__c from Slots__c where id=:fSlot2.id ];
      
      lOppItem.Slot__c = fSlot2.id; 
      
      Test.StartTest();
      update lOppItem;
      Test.StopTest();
      
    }
    
    static testMethod void deleteItem()
    { 
      ContractLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      insert lOppItem;
      
      Test.StartTest();
      delete lOppItem;
      Test.StopTest();
      
    }
    
    static testMethod void semSlot()
    { 
      ContractLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today().addDays(40); 
      lOppItem.End_date__c = System.today().addDays( 50 );
      
      Test.StartTest();
      Database.SaveResult lResult = database.insert( lOppItem, false );
      Test.StopTest();
    }
    
    static testMethod void maisSlot()
    { 
      Slots__c fSlot2 = SObjectInstanceTest.createSlot();
      insert fSlot2;
      
      ContractLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      
      Test.StartTest();
      Database.SaveResult lResult = database.insert( lOppItem, false );
      Test.StopTest();
      
    }
    
    static testMethod void semQtt()
    { 
      ContractLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      
      fSlot.Quantity__c = 0;
      update fSlot;
      
      Test.StartTest();
      Database.SaveResult lResult = database.insert( lOppItem, false );
      Test.StopTest();
      
    }
    
    static testMethod void lote()
    { 
      ServiceContract lOpp = SObjectInstanceTest.createServiceContract();
      lOpp.Pricebook2Id = SObjectInstanceTest.catalogoDePrecoPadrao();
      insert lOpp;
      
      Product2 lProd = SObjectInstanceTest.createProduct2();
      lProd.Product_Type__c = 'Training';
      lProd.RecordTypeId = RecordTypeMemory.getRecType( 'Product2', 'Training' );
      insert lProd;
      
      Id lPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      PricebookEntry lPBE = SObjectInstanceTest.createPricebookEntry( lPB, lProd.id );
      insert lPBE;
      
      fSlot = SObjectInstanceTest.createSlot();
      fSlot.Quantity__c = QTD_LOTE;
      fSlot.Start_date__c = System.today().addDays( 100 );
      fSlot.End_date__c = System.today().addDays( 110 );
      insert fSlot;
      
      fSlot = [ select Avalilable_quantity__c from Slots__c where id=:fSlot.id ];
      
      list< ContractLineItem > lLstOppItem = new list< ContractLineItem >();
      for ( integer i=0; i<QTD_LOTE; i++ )
      {
        ContractLineItem lOppItem = SObjectInstanceTest.createContractLineItem( lOpp.id, lPBE.id );
        lOppItem.Start_date__c = System.today().addDays( 100 ); 
        lOppItem.End_date__c = System.today().addDays( 103 );
        lLstOppItem.add( lOppItem );
      }
      
      Test.StartTest();
      insert lLstOppItem;
      Test.StopTest();
    }
    
}