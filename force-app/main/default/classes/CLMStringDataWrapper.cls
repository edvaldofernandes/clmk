global class CLMStringDataWrapper implements Comparable {
    
    public String str {get;set;}
    
    // Constructor
    public CLMStringDataWrapper (String str) {
        this.str = str;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        
        // Cast argument to CLMSOBDataWrapper
        CLMStringDataWrapper compareToStr = (CLMStringDataWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        String current = this.str != null && this.str.split('-').size() > 1 ? this.str.split('-')[1] + this.str.split('-')[0] : '';
        String compared = compareToStr.str != null && compareToStr.str.split('-').size() > 1 ? compareToStr.str.split('-')[1] + compareToStr.str.split('-')[0] : '';
       // system.debug('Current' +current);
       // system.debug('Compared' +Compared); 
       
        if (current > compared) { returnValue = 1;
        } else if (current < compared) {
            returnValue = -1;
        } else {  returnValue = 0;
        }
     //   system.debug('returnValueComparator' +str + ' compare to ' + compareToStr.str + 'result '+returnValue);    
        return returnValue;     
    }
}