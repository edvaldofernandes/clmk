@isTest(seeAllData=true)
public class SFAircraftInformationToEtrackTest {
    
    @isTest static void informationAfterInsertOrUpdateTest(){
        
        Test.startTest();
        
        DescribeFieldResult field = Aircraft__History.Field.getDescribe();
        List<PicklistEntry> availableValues = field.getPicklistValues();
        
        Profile p = [select id from Profile where name='System Administrator'];
        
        User user = new User(alias = 'utest7', email='Unit7.Test@unittest.com',emailencodingkey='UTF-8', 
                             firstName='First7', lastname='Last7', languagelocalekey='en_US', localesidkey='en_US', 
                             profileid = p.id, timezonesidkey='Europe/London', username='Unit7.Test@unittest.com');
        insert user;
        
        Account account = new Account();
        account.Name = 'Test aircraft trigger insert';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = 'TEST 123456';
        insert account;
        
        Aircraft__c aircraft = new Aircraft__c ();
        aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '09876543';
        aircraft.Operator__c = account.Id;
        aircraft.Owner__c =  account.Id;
        aircraft.CreatedById = user.id;
        aircraft.Aircraft_Status__c = 'In Service';
        aircraft.Commercial_Name__c = 'EMB-110P1';
        insert aircraft;
        
        Aircraft__History aircraftHistoryTest = new Aircraft__History();
        aircraftHistoryTest.Field = availableValues.get(0).getValue();
        aircraftHistoryTest.ParentId = aircraft.Id;
        insert aircraftHistoryTest;
        
        Aircraft__c aicraftUpdate = [SELECT Model_Type__c, name FROM Aircraft__c WHERE id =: aircraft.Id];
        
        aicraftUpdate.Model_Type__c = 'TEST 190';
        aicraftUpdate.name = '09876543';
        
        update aicraftUpdate;
        
        Aircraft__c aicraftDelete = [SELECT Model_Type__c, name FROM Aircraft__c WHERE id =: aircraft.Id];
        
        delete aicraftDelete;
        
        Test.setMock(WebServiceMock.class, new RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(null));
        
        Test.stopTest();
    }
}