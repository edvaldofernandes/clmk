public class DFProductionCapabilityDAO {
    public static List<DF_Production_Capability__c> getDfCapabilityByAircraftId(Integer currentYear, Set<String> aircraftFamilyIds, String dfVersion){
        string setToString = '';
        for(String includeValue : aircraftFamilyIds){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        
        System.debug('DFProductionCapabilityDAO getDfCapabilityByAircraftId dfVersion' + dfVersion);
        List<DF_Production_Capability__c> lstProduction = new List<DF_Production_Capability__c>();
        String querySoql = 'SELECT ' + utils.getAllFields('DF_Production_Capability__c') + ', Aircraft_Model__r.Name,' +
            ' Aircraft_Model__r.DF_Grouping__c, Aircraft_Model__r.DF_Group__c ' +
            ' FROM DF_Production_Capability__c WHERE '+
            ' DF_Version__c = ' + '\''+ dfVersion+'\''  + 'AND Aircraft_Family__c IN (' + setToString + ')' +
            ' AND CALENDAR_YEAR(Delivery_s_Date__c)  = ' + currentYear;

        System.debug('DFProductionCapabilityDAO getDfCapabilityByAircraftId querySoql' + querySoql);
        lstProduction = database.query(querySoql);
        System.debug('DFProductionCapabilityDAO getDfCapabilityByAircraftId lstProduction' + lstProduction);
        return lstProduction;
    }

}