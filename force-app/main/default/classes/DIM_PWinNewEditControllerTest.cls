// Used by DIM - Market Intelligence.
@isTest
public class DIM_PWinNewEditControllerTest {

    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkIfTheLastValueOfRelationshipIsImported() {
    
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.RecordTypeId = '012i0000001IyKG';
        opportunity.Name = 'Test Opp';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        insert opportunity;
        
        PWin__c pWinOld = new PWin__c();
        pWinOld.Opportunity__c = opportunity.Id;
        pWinOld.Relationship__c = '60';
        insert pWinOld;
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(new PWin__c());
        DIM_PWinNewEditController controller = new DIM_PWinNewEditController(standardController);
        
        controller.newPWin.Opportunity__c = opportunity.Id;
        controller.updateNewPWinAccordingToTheLastPWin();
        
        System.assertEquals(pWinOld.Relationship__c, controller.newPWin.Relationship__c);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkIfTheDefaultValueOfRelationshipIsImported() {

        ApexPages.StandardController standardController = new ApexPages.StandardController(new PWin__c());
        DIM_PWinNewEditController controller = new DIM_PWinNewEditController(standardController);
        controller.updateNewPWinAccordingToTheLastPWin();
        
        System.assertEquals((String) PWin__c.Relationship__c.getDescribe().getDefaultValue(), controller.newPWin.Relationship__c);
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkHasSumOfWeightsEqualTo100() {
    
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.RecordTypeId = '012i0000001IyKG';
        opportunity.Name = 'Test Opp';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        insert opportunity;
        
        PWin__c pWinOld = new PWin__c();
        pWinOld.Opportunity__c = opportunity.Id;
        insert pWinOld;
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(new PWin__c());
        DIM_PWinNewEditController controller = new DIM_PWinNewEditController(standardController);
        
        System.assertEquals(true, controller.hasSumOfWeightsEqualTo100());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
}