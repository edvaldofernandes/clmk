@isTest
private class CRM360_CustomerDashboardControllerTest {

    private static Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();
    private static String companyName = 'Test Account record';
    private static String EOCDescription = 'Expects to test stuff';
    private static String EOCIndicator = 'Action Required';
    private static String companyNullName = 'Null';
    
    @testSetup
    static void setup(){
        List<Account> accountsToInsert = new List<Account>();
        
        Account testAccount = new Account();
        testAccount.Name = companyName;
        testAccount.Company_Nickname__c = companyName;
        accountsToInsert.add(testAccount);
        
        Account testNullAccount = new Account();
        testNullAccount.Name = companyNullName;
        testNullAccount.Company_Nickname__c = companyNullName;
        accountsToInsert.add(testNullAccount);
        
        insert accountsToInsert;
        
        Account_Cockpit__c dashboardDataTest = new Account_Cockpit__c();
        dashboardDataTest.Name = 'Test Airlines CRM360 Data';
        dashboardDataTest.CRM360_EOC__c = EOCDescription;
        dashboardDataTest.CRM360_EOC_Indicator__c = EOCIndicator;
        dashboardDataTest.RecordTypeId = RecordTypeIdCRM360Data;
        dashboardDataTest.Account_Name__c = testAccount.Id;
        insert dashboardDataTest;
    }
    
    @isTest
    public static void testGetAccount(){
        List<Account> accounts = [SELECT ID FROM Account WHERE Name = :companyName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CustomerDashboardPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        CRM360_CustomerDashboardController testAccPlan = new CRM360_CustomerDashboardController();
        
        Account accTest = CRM360_CustomerDashboardController.getThisAccount();
        
        Test.stopTest();
        
        //Verify if account is not null
        System.assertNotEquals(null, accTest, 'accTest is null');
        
        //Verify if account id is correct
        System.assertEquals(accounts[0].Id, accTest.Id, 'Account Id is not equals');
    }
    
    @isTest
    public static void testGetDashboardData(){
        List<Account> accounts = [SELECT ID FROM Account WHERE Name = :companyName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CustomerDashboardPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        CRM360_CustomerDashboardController testAccPlan = new CRM360_CustomerDashboardController();
        
        Account_Cockpit__c dashboardData = CRM360_CustomerDashboardController.getDashboardData();
        
        Test.stopTest();
        
        //Verify if dashboard data is not null
        System.assertNotEquals(null, dashboardData, 'dashboardData is null');
        
        //Verify if dashboard data is correct
        System.assertEquals(EOCDescription, dashboardData.CRM360_EOC__c, 'Data is incorrect');
        System.assertEquals(EOCIndicator, dashboardData.CRM360_EOC_Indicator__c, 'Data is incorrect');
    }
    
    @isTest
    public static void testGetLicense(){
        List<Account> accounts = [SELECT ID FROM Account WHERE Name = :companyName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CustomerDashboardPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        CRM360_CustomerDashboardController testAccPlan = new CRM360_CustomerDashboardController();
        
        String license = CRM360_CustomerDashboardController.getLicense();
        
        Test.stopTest();
        
        //Asserts
        Profile profile = [SELECT Name, Id, UserLicenseId FROM Profile WHERE Id = :UserInfo.getProfileId()];
        UserLicense userLicense = [SELECT Name, Id FROM UserLicense WHERE Id = :profile.UserLicenseId];
        //Verify if user license is correct
        System.assertEquals(userLicense.Name, license, 'User license is incorrect');
    }
    
    @isTest
    public static void testEmptyData(){
    	List<Account> accounts = [SELECT Id FROM Account WHERE Name = :companyNullName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CustomerDashboardPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        CRM360_CustomerDashboardController testAccPlan = new CRM360_CustomerDashboardController();
        
        Account_Cockpit__c dashboardData = CRM360_CustomerDashboardController.getDashboardData();
        Account accTest = CRM360_CustomerDashboardController.getThisAccount();
        
        Test.stopTest();   
        
        //Verify if dashboard data is null
        System.assertEquals(null, dashboardData, 'DashboardData is not null');
        
        //Verify if account is not null
        System.assertNotEquals(null, accTest, 'AccTest is null');
        
        //Verify if account is correct
        System.assertEquals(companyNullName, accTest.Name, 'Account is incorrect');
	}
    
}