@IsTest
public class CaseAggregateStatusTest {

    @TestSetup
    private static void setup(){
        Case c = createCase();
        Database.insert(c);
    }
    
    @IsTest
    private static void shouldAssignStatusAggregate(){
        
        Case c = [SELECT Id, Status, AggregateStatus__c FROM Case LIMIT 1];
        
        Test.startTest();
        
            c.Status = 'WareHouse';
        	c.Sent_Customer_Notification__c = true;
            Database.update(c);
        
        Test.stopTest();
        
        Case caseUpdated = [SELECT Id, Status, AggregateStatus__c FROM Case WHERE Id = :c.Id];
        
        System.assertEquals(caseUpdated.AggregateStatus__c, Label.CRC_Interface);
    }
    
    @IsTest
    private static void shouldAssignStatusCustomerNotification(){
        
        Case c = [SELECT Id, Status, AggregateStatus__c FROM Case LIMIT 1];
        
        Test.startTest();
        
            c.Status = 'Processing';
        	c.Sent_Customer_Notification__c = true;
            Database.update(c);
        
        Test.stopTest();
        
        Case caseUpdated = [SELECT Id, Status, AggregateStatus__c FROM Case WHERE Id = :c.Id];
        
        System.assertEquals(caseUpdated.AggregateStatus__c, Label.CRC_CustomerNotification);
    }
    
    @IsTest
    private static void equalsStatus(){
        
        Case c = [SELECT Id, Status, AggregateStatus__c FROM Case LIMIT 1];
        
        Test.startTest();
        
            c.Subject = 'CRC - V2';
            Database.update(c);
        
        Test.stopTest();
        
        Case caseUpdated = [SELECT Id, Status, AggregateStatus__c FROM Case WHERE Id = :c.Id];
        
        System.assertEquals(caseUpdated.AggregateStatus__c, c.AggregateStatus__c);
    }
    
    @IsTest
    private static void shouldAssignStatus(){
        
        Case c = [SELECT Id, Status, AggregateStatus__c FROM Case LIMIT 1];
        
        Test.startTest();
        
            c.Status = 'Processing';
            Database.update(c);
        
        Test.stopTest();
        
        Case caseUpdated = [SELECT Id, Status, AggregateStatus__c FROM Case WHERE Id = :c.Id];
        
        System.assertEquals(caseUpdated.AggregateStatus__c, 'Processing');
    }
    
    private static Case createCase(){
        
        Case c = new Case(
        	RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRC').getRecordTypeId(),
            Status = 'New',
            Due_Date__c = Date.today().addMonths(1),
            Subject = 'CRC',
            Study_Type__c = 'Performance',
            Type = 'TBC - To be classified',
            Phase_c__c = 'Dispatch',
            Status__c = 'Dispatch.',
            Next_Action_required_details__c = 'Required Details',
            Next_Action_Required_Date_and_Time__c = System.now(),
            Requested_By__c = UserInfo.getUserId()
        );
        
        return c;
    }
}