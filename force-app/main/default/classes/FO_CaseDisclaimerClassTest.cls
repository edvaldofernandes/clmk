@isTest
private class FO_CaseDisclaimerClassTest {

    testMethod static void Testando() {
        
        Test.startTest();
        
        RecordType rt1 = [SELECT id from RecordType where SobjectType='Account' AND Name = 'Airline'];
        Account ac = new Account(Name='Embraer Airlines',Company_Nickname__c='EMB',RecordTypeId=rt1.Id);
      	Database.insert(ac);  
        
        RecordType rt2 = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];               
        Case casoToInsert1 = new Case(RecordTypeId = rt2.Id ,Status = 'New',
                                      AccountId= ac.Id ,E170__c=True,ERJ__c=True,Turboprops__c=True);
      	list<Case> lstcaso = new list<Case>();
      	lstcaso.add(casoToInsert1);
      	database.insert(lstcaso); 
                
        EmailMessage emToCreate = new EmailMessage(ParentId=casoToInsert1.Id,Status='0',Subject='Test',
                                                   FromAddress='abc@gmail.com',TextBody='Bla');
        database.insert(emToCreate);
        database.update(casoToInsert1);
        
        Attachment attToCreate = new Attachment(ParentId=emToCreate.id,Description='Bla',Name='aa',Body=Blob.valueOf('aa'));
        database.insert( attToCreate );
        database.update(emToCreate);
        

        FO_CaseDisclaimerClass.CaseDisclaimerClassMethod('Test', emToCreate.ParentId , emToCreate.Id);
        
        
        Test.stopTest();
    }
    
}