@isTest
private class CRM360_CompanyEocInfoControllerTest{

    private static Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();
    private static Id RecordTypeIdEOCSMeetings = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('EOC Side Meeting').getRecordTypeId();
    private static Integer leadSalesPotential = 25000;
    private static String accountName = 'Test Account';
    private static String accountNullName = 'Test Account Null';
    private static String attended = 'Attended';
    private static String contactFirstName = 'John';
    private static String contactLastName = 'Doe';
    private static String customerEspectation = 'Expects to test stuff';
    private static String dashboardDataName = 'Test Airlines CRM360 Data';
    private static String eocIndicator = 'Action Required';
    private static String high = 'High';
    private static String leadLevelOfInterest = 'Very interested';
    private static String leadEvent = 'Embraer E-Jets Worldwide Digital Conference 2020';
    private static String low = 'Low';
    private static String medium = 'Medium';
    private static String registered = 'Registered';
    private static String sideMeetingHighName = 'Test Meeting High';
    private static String sideMeetingMediumName = 'Test Meeting Medium';
    private static String sideMeetingLow1Name = 'Test Meeting Low 1';
    private static String sideMeetingLow2Name = 'Test Meeting Low 2';
    
    @testSetup
    static void setup(){

        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);
        
        //Create a null account
        Account accountNull = new Account();
        accountNull.Name = accountNullName;
        accountNull.Company_Nickname__c = accountNullName;
        accountsToInsert.add(accountNull);
        
        insert accountsToInsert;
        
        //Create contacts related to the account
        List<Contact> contactsToInsert = new List<Contact>();
        
        Contact testContact = new Contact();
        testContact.FirstName = contactFirstName;
        testContact.LastName = contactLastName;
        testContact.AccountId = account.Id;
        testContact.Account_Name_workflow__c = account.Name;
        contactsToInsert.add(testContact);
        
        Contact testContact2 = new Contact();
        testContact2.FirstName = contactFirstName;
        testContact2.LastName = contactLastName;
        testContact2.AccountId = account.Id;
        testContact2.Account_Name_workflow__c = account.Name;
        contactsToInsert.add(testContact2);
        
        insert contactsToInsert;
        
        //Create a lead
        S_S_Leads__c testLead = new S_S_Leads__c();
        testLead.Contact__c = testContact.Id;
        testLead.Sales_Potential__c = leadSalesPotential;
        testLead.Level_of_interest__c = leadLevelOfInterest;
        testLead.Event__c = leadEvent;
        insert testLead;
        
        //Create a CRM360 Dashboard
        Account_Cockpit__c dashboardDataTest 		= new Account_Cockpit__c();
        dashboardDataTest.Name 						= dashboardDataName;
        dashboardDataTest.CRM360_EOC__c 			= customerEspectation;
        dashboardDataTest.CRM360_EOC_Indicator__c	= eocIndicator;
        dashboardDataTest.RecordTypeId 				= RecordTypeIdCRM360Data;
        dashboardDataTest.Account_Name__c 			= account.Id;
        
        insert dashboardDataTest;
        
        //Create Side Meetings
        List<Account_Cockpit__c> summariesToInsert = new List<Account_Cockpit__c>();
        
        //Low attention level side meeting
        Account_Cockpit__c testSummary1 = new Account_Cockpit__c();
        testSummary1.Name = sideMeetingLow1Name;
        testSummary1.Customer_Expectation__c = customerEspectation;
        testSummary1.Att_Level__c = low;
        testSummary1.RecordTypeId = RecordTypeIdEOCSMeetings;
        testSummary1.Related_EOC__c = leadEvent;
        testSummary1.Account_Name__c = account.Id;
        summariesToInsert.add(testSummary1);
        
        //Medium attention level side meeting
        Account_Cockpit__c testSummary2 = new Account_Cockpit__c();
        testSummary2.Name = sideMeetingMediumName;
        testSummary2.Customer_Expectation__c = customerEspectation;
        testSummary2.Att_Level__c = medium;
        testSummary2.RecordTypeId = RecordTypeIdEOCSMeetings;
        testSummary2.Related_EOC__c = leadEvent;
        testSummary2.Account_Name__c = account.Id;
        summariesToInsert.add(testSummary2);

		//Another Low attention level side meeting
        Account_Cockpit__c testSummary3 = new Account_Cockpit__c();
        testSummary3.Name = sideMeetingLow2Name;
        testSummary3.Customer_Expectation__c = customerEspectation;
        testSummary3.Att_Level__c = low;
        testSummary3.RecordTypeId = RecordTypeIdEOCSMeetings;
        testSummary3.Related_EOC__c = leadEvent;
        testSummary3.Account_Name__c = account.Id;
        summariesToInsert.add(testSummary3);
        
        //High attention level side meeting
        Account_Cockpit__c testSummary4 = new Account_Cockpit__c();
        testSummary4.Name = sideMeetingHighName;
        testSummary4.Customer_Expectation__c = customerEspectation;
        testSummary4.Att_Level__c = high;
        testSummary4.RecordTypeId = RecordTypeIdEOCSMeetings;
        testSummary4.Related_EOC__c = leadEvent;
        testSummary4.Account_Name__c = account.Id;
        summariesToInsert.add(testSummary4);

		insert summariesToInsert;
        
        //Create the campaign
        Campaign testCampaign = new Campaign();
        testCampaign.Name = 'Embraer E-Jets Worldwide Digital Conference 2020';
        insert testCampaign;

		//Create campaign member status
		List<CampaignMemberStatus> cmStatus = new List<CampaignMemberStatus>();
		
		CampaignMemberStatus attendedStatus = new CampaignMemberStatus(
            CampaignID = testCampaign.id,
            Label = attended,
            IsDefault = false,
            HasResponded = false,
            SortOrder = 3
        ); 
        cmStatus.add(attendedStatus);
        
        CampaignMemberStatus registeredStatus = new CampaignMemberStatus(
            CampaignID = testCampaign.id,
            Label = registered,
            IsDefault = false,
            HasResponded = false,
            SortOrder = 4
        ); 
        cmStatus.add(registeredStatus);
        
        insert cmStatus;		
        
        //Create campaign members
		List<CampaignMember> campaignMembersToInsert = new List<CampaignMember>();
        
        CampaignMember campaignMember1 = new CampaignMember();
        campaignMember1.ContactId = testContact.Id;
        campaignMember1.CampaignId = testCampaign.Id;
        campaignMember1.Status = registeredStatus.Label;
        campaignMembersToInsert.add(campaignMember1);

        CampaignMember campaignMember2 = new CampaignMember();
        campaignMember2.CampaignId = testCampaign.Id;
        campaignMember2.ContactId = testContact2.Id;
        campaignMember2.Status = attendedStatus.Label;
        campaignMembersToInsert.add(campaignMember2);
        
        insert campaignMembersToInsert;
    }
    
    @isTest
    public static void testGetLeadsList(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
            
        List<S_S_Leads__c> leads = CRM360_CompanyEocInfoController.getLeadsList();
        Integer leadAmount = CRM360_CompanyEocInfoController.getLeadAmount();
        
        Test.stopTest();
        
        //Verify if lead amount is correct
        System.assertEquals(1, leadAmount, 'Lead amount is incorrect');
        
        //Verify if lead list is not null
        System.assertNotEquals(null, leads, 'Lead list is null');
        
        //Verify if recovered lead is correct
        System.assertEquals(leadLevelOfInterest, leads[0].Level_of_interest__c, 'Level of interest is incorrect');
        System.assertEquals(leadSalesPotential, leads[0].Sales_Potential__c, 'Sales Potential is incorrect');

    }
    
    @isTest
    public static void testGetSideMeetingsList(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
            
        List<Account_Cockpit__c> sideMeetings = CRM360_CompanyEocInfoController.getSideMeetingsList();
        Integer sideMeetingsAmount = CRM360_CompanyEocInfoController.getSMeetingsAmount();
        
        Test.stopTest();
        
        //Verify if side meetings amount is correct
        System.assertEquals(4, sideMeetingsAmount, 'Side meetings amount is incorrect');
        
        //Verify if side meetings list is not null
        System.assertNotEquals(null, sideMeetings, 'Side meetings list is null');
        
        //Verify if recovered side meetings list size is correct
        System.assertEquals(4, sideMeetings.size(), 'Side meetings list size is incorrect');
        
        //Verify if recovered data is correct
        for(Account_Cockpit__c sideMeeting : sideMeetings){
            System.assertEquals(customerEspectation, sideMeetings[0].Customer_Expectation__c, 'Data is incorrect');
        }
        
        //Verify if ordenation is correct
        System.assertEquals(high, sideMeetings[0].Att_Level__c, 'Data ordenation is incorrect');
        System.assertEquals(medium, sideMeetings[1].Att_Level__c, 'Data ordenation is incorrect');
        System.assertEquals(low, sideMeetings[2].Att_Level__c, 'Data ordenation is incorrect');
        System.assertEquals(low, sideMeetings[3].Att_Level__c, 'Data ordenation is incorrect');
        System.assertEquals(sideMeetingHighName, sideMeetings[0].Name, 'Data ordenation is incorrect');
        System.assertEquals(sideMeetingMediumName, sideMeetings[1].Name, 'Data ordenation is incorrect');
        System.assertEquals(sideMeetingLow1Name, sideMeetings[2].Name, 'Data ordenation is incorrect');
        System.assertEquals(sideMeetingLow2Name, sideMeetings[3].Name, 'Data ordenation is incorrect');	

    }
    
    @isTest
    public static void testGetCampaignMembers(){
        Account accounts = [SELECT Id FROM Account WHERE Name = :accountName];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage;
        pageRef.getParameters().put('id', String.valueOf(accounts.Id));
        Test.setCurrentPage(pageRef);
                    
        List<CampaignMember> campaignMembers = CRM360_CompanyEocInfoController.getCampaignMembers();
        Integer registeredAmount = CRM360_CompanyEocInfoController.getRegisteredAmount();
        
        Test.stopTest();
        
        //Verify if registered amount is correct
        System.assertEquals(2, registeredAmount, 'Registered amount is incorrect');
        
        //Verify if campain member list is not null
        System.assertNotEquals(null, campaignMembers, 'Campaign memebr list is null');
        
        //Verify if campain member list size is correct
        System.assertEquals(registeredAmount, campaignMembers.size(), 'campain member list size is incorrect');
        
        //Verify if recovered data is correct
        for(CampaignMember cm : campaignMembers){
            System.assertEquals(contactFirstName, cm.FirstName, 'Recovered data is incorrect');
            System.assertEquals(contactLastName, cm.LastName, 'Recovered data is incorrect');
        }
        
    }
    
    @isTest
    public static void testGetEOCIndicator(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
            
        String testEOCIndicatior = CRM360_CompanyEocInfoController.getEOCIndicator();
        
        Test.stopTest();
        
        //Verify if EOC Indicator is correct
        System.assertEquals(eocIndicator, testEOCIndicatior, 'EOC indicator is incorrect');
    }
    
    @isTest
    public static void testGetAccount(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
            
        Account account = CRM360_CompanyEocInfoController.getAcc();
        Account_Cockpit__c headsUp = CRM360_CompanyEocInfoController.getHeadsUpInfo();
        
        Test.stopTest();
        
        //Verify if recovered account is correct
        System.assertEquals(accounts[0].Id, account.Id, 'Account is incorrect');
    }
    
    @isTest
    public static void testNullAccount(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountNullName];
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
            
        List<S_S_Leads__c> leads = CRM360_CompanyEocInfoController.getLeadsList();
        List<CampaignMember> campainMemebrs = CRM360_CompanyEocInfoController.getCampaignMembers();
        List<Account_Cockpit__c> sideMeetings = CRM360_CompanyEocInfoController.getSideMeetingsList();
        Integer leadAmount = CRM360_CompanyEocInfoController.getLeadAmount();
        Integer campainMemebrsAmount = CRM360_CompanyEocInfoController.getRegisteredAmount();
        Integer sideMeetingsAmount = CRM360_CompanyEocInfoController.getSMeetingsAmount();
        String testEOCIndicatior = CRM360_CompanyEocInfoController.getEOCIndicator();
        Account account = CRM360_CompanyEocInfoController.getAcc();
        
        //Verify if recovered data is null
        System.assertEquals(null, leads, 'Lead list is not null');
        System.assertEquals(null, campainMemebrs, 'CampainMemebr list is not null');
        System.assertEquals(null, sideMeetings, 'Side meetings list is not null');
        System.assertEquals(null, testEOCIndicatior, 'EOCIndicator is not null');

		//Verify if all amounts are equals to zero
		System.assertEquals(0, leadAmount, 'Lead amount is not equals to zero');
        System.assertEquals(0, campainMemebrsAmount, 'Campain members amount is not equals to zero');
        System.assertEquals(0, sideMeetingsAmount, 'Side meetings amount is not equals to zero');
		
		//Verify if account is correct
		System.assertEquals(accounts[0].Id, account.Id, 'Account is incorrect');         
        
        Test.stopTest();
    }
}