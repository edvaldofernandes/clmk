public without sharing class FO_CaseCommentHandler {

    public Map<Id,CaseComment> newRecordsMap = new Map<Id,CaseComment>();
    public Map<Id,CaseComment> oldRecordsMap = new Map<Id,CaseComment>(); 
    public List<CaseComment> newRecords = new List<CaseComment>();
    public List<CaseComment> oldRecords = new List<CaseComment>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    public FO_CaseCommentHandler(boolean isExecuting){  this.isExecuting = isExecuting;   }

    public void OnBeforeInsert(){ 
        
        updateComment();
 
    }

    public void OnAfterInsert(){ }

    public void OnBeforeUpdate(){
        updateComment();
    }

    public void OnAfterUpdate(){}

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}
    
    public void updateComment(){
        
        CaseComment casoComment = this.newRecords[0];
        
        Case caso = [SELECT Id,RecordTypeId,FlightOps_LastComment__c FROM Case WHERE Id =: casoComment.ParentId ];
  
        RecordType rt = [SELECT Name FROM RecordType WHERE Id =: caso.RecordTypeId];
        
        if (rt.Name == 'FlightOps Case Record Type'){
        
            caso.FlightOps_LastComment__c = casoComment.CommentBody;
            
            Database.update(caso);
        
        }

    }
    
    
}