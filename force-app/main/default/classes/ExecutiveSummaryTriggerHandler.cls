/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; Feb-2017.
**/

public without sharing class ExecutiveSummaryTriggerHandler
{

    
    public Map<Id,Account_Cockpit__c> newRecordsMap = new Map<Id,Account_Cockpit__c>();
    public Map<Id,Account_Cockpit__c> oldRecordsMap = new Map<Id,Account_Cockpit__c>(); 
    public List<Account_Cockpit__c> newRecords = new List<Account_Cockpit__c>();
    public List<Account_Cockpit__c> oldRecords = new List<Account_Cockpit__c>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public ExecutiveSummaryTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {
    }

 

    public void OnAfterInsert()
    {
        referenceExecutiveSummaries();

    }

 

    public void OnBeforeUpdate()
    { 
    }

 

    public void OnAfterUpdate()
    {

         referenceExecutiveSummaries();   

    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }


    
    private void referenceExecutiveSummaries()
    {
        
        Id recordTypeSitesMonthlyId = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('Sites Monthly Overview').getRecordTypeId();
        Id recordTypeAirlineCustomerId = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('Airline Customers').getRecordTypeId();
        map<string,Id> mapSitesMonthlyKeyId = new map<string,Id>(); 
        map<string,list<Id>> mapAirlineCustomersKeyId = new map<string,list<Id>>();         
        list<Account_Cockpit__c> recordsToUpdate = new list<Account_Cockpit__c>();
        
        
        boolean eligibleRecord = false;
        list<id> recordsList = new list<id>();
        
        
        for(Account_Cockpit__c executiveSummary : this.newRecords)
        {
        
            eligibleRecord = false;
            
            if(executiveSummary.RecordTypeId == recordTypeSitesMonthlyId || executiveSummary.RecordTypeId == recordTypeAirlineCustomerId)
            {
                if(this.isInsert)
                {
                    eligibleRecord = executiveSummary.Month__c != null && executiveSummary.Year__c != null && executiveSummary.Account_Name__c!= null;
                }
                else
                {
                    eligibleRecord = executiveSummary.Month__c != this.oldRecordsMap.get(executiveSummary.Id).Month__c || executiveSummary.Year__c != this.oldRecordsMap.get(executiveSummary.Id).Year__c || executiveSummary.Account_Name__c != this.oldRecordsMap.get(executiveSummary.Id).Account_Name__c;
                } 
                
                if(eligibleRecord)            
                {
                    if(executiveSummary.RecordTypeId == recordTypeSitesMonthlyId)
                    {
                        mapSitesMonthlyKeyId.put(executiveSummary.reportKey__c,executiveSummary.Id);     
                    }
                    else
                    {
                        recordsList = new list<id>();
                        
                        if(mapAirlineCustomersKeyId.containsKey(executiveSummary.reportKey__c))
                            recordsList = mapAirlineCustomersKeyId.get(executiveSummary.reportKey__c); 
                        
                        recordsList.add(executiveSummary.Id);
                                 
                        mapAirlineCustomersKeyId.put(executiveSummary.reportKey__c,recordsList);     
                        
                        System.Debug(recordsList);
                        
                    }
                    
                
                }
            }        
        
        }
        
        if(!mapSitesMonthlyKeyId.isEmpty())
        {
            
            for(Account_Cockpit__c execSummary : [Select Id,siteExecutive_Summary__c,reportKey__c From Account_Cockpit__c Where RecordTypeId = : recordTypeAirlineCustomerId And ((reportKey__c in : mapSitesMonthlyKeyId.keySet() And siteExecutive_Summary__c = null) Or (siteExecutive_Summary__c in : mapSitesMonthlyKeyId.values()))  ])
            {
                if(mapSitesMonthlyKeyId.containsKey(execSummary.reportKey__c))
                {
                    execSummary.siteExecutive_Summary__c =  mapSitesMonthlyKeyId.get(execSummary.reportKey__c);
                }
                else
                {
                    execSummary.siteExecutive_Summary__c = null;
                }
                
                recordsToUpdate.add(execSummary);
            
            }                
            
        }
        
        if(!mapAirlineCustomersKeyId.isEmpty())
        {
            
            for(Account_Cockpit__c execSummary : [Select Id,siteExecutive_Summary__c,reportKey__c From Account_Cockpit__c Where reportKey__c in : mapAirlineCustomersKeyId.keySet() And RecordTypeId =  : recordTypeSitesMonthlyId])
            {
            
                System.Debug(execSummary.reportKey__c);
                
                
                    
                if(mapAirlineCustomersKeyId.containsKey(execSummary.reportKey__c))
                {
                                    
                        
                         for(id idRecord : mapAirlineCustomersKeyId.get(execSummary.reportKey__c))
                         {

                            recordsToUpdate.add(new Account_Cockpit__c (Id = idRecord,siteExecutive_Summary__c = execSummary.Id));
                            mapAirlineCustomersKeyId.remove(execSummary.reportKey__c);
                        }
                
                }
            
            }                
            
        }
        
        for(string keyNull : mapAirlineCustomersKeyId.keySet())
        {
             for(id idRecord : mapAirlineCustomersKeyId.get(keyNull))
                 recordsToUpdate.add(new Account_Cockpit__c(Id = idRecord,siteExecutive_Summary__c = null));
        
        }
        
        
        if(!recordsToUpdate.isEmpty())
            update recordsToUpdate;
        
        
        
   }





}