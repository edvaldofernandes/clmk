@isTest
private class RECTriggerHandlerTest {
  static Id embraerType = Schema.SObjectType.Account.getRecordTypeInfosByName()
    .get('Embraer Site')
    .getRecordTypeId();

  static Id airlineType = Schema.SObjectType.Account.getRecordTypeInfosByName()
    .get('Airline')
    .getRecordTypeId();

  @TestSetup
  static void makeData() {
    Communications__c rec = createREC();
    Database.insert(rec);

    Account embraerSite = createAccount('embraer', embraerType, null, '');
    Database.insert(embraerSite);
    Id idEmbraer = [SELECT Id FROM Account WHERE Name = 'embraer' LIMIT 1].Id;

    Account randomAirline = createAccount(
      'rndBlue',
      airlineType,
      idEmbraer,
      'Turboprop;EJet'
    );
    Database.insert(randomAirline);
    Account otherAirline = createAccount(
      'rndAir',
      airlineType,
      idEmbraer,
      'E2'
    );
    Database.insert(otherAirline);

    Id idRandomAirline = [SELECT Id FROM Account WHERE Name = 'rndBlue' LIMIT 1]
    .Id;
    Id idOtherAirline = [SELECT Id FROM Account WHERE Name = 'rndAir' LIMIT 1]
    .Id;

    List<Contact> contacts = new List<Contact>{
      createContact('contact1', idEmbraer, true),
      createContact('contact2', idRandomAirline, true),
      createContact('contact3', idRandomAirline, false),
      createContact('contact4', idOtherAirline, true)
    };
    Database.insert(contacts);

    List<ContentVersion> cvs = new List<ContentVersion>{
      createAttachment('testando1', rec),
      createAttachment('testandoRec1', rec)
    };

    rec.email_attachments__c = cvs[0].contentDocumentId + ';' + cvs[0].Title;
    rec.email_preview__c = 'performance@test.invalid.br';
    Database.update(rec);
  }

  private testMethod static void generalStructuredTest() {
    Communications__c rec = [
      SELECT
        id,
        name,
        recordtypeid,
        fleet_type__c,
        email_attachments__c,
        email_preview__c
      FROM communications__c
      LIMIT 1
    ];
    RECTriggerHandler incomingRec = new RECTriggerHandler(
      new List<Communications__c>{ rec }
    );

    System.assertEquals(
      incomingRec.getIdRECRecordType(),
      rec.RecordTypeId,
      '[ERROR] REC ids problem!'
    );
    System.assertEquals(
      incomingRec.getNewRec().values().get(0),
      rec,
      '[ERROR] REC is not the same'
    );
    System.assertEquals(
      incomingRec.normalizeRECFleetType('170/190'),
      new List<String>{ 'EJet', '-' },
      '[ERROR] REC Fleet Type convert problem!'
    );
    System.assertEquals(
      incomingRec.normalizeRECFleetType('190/195-E2'),
      new List<String>{ 'E2', '-' },
      '[ERROR] REC Fleet Type convert problem!'
    );
    System.assertEquals(
      incomingRec.normalizeRECFleetType('145/170/190'),
      new List<String>{ 'EJet', 'ERJ' },
      '[ERROR] REC Fleet Type convert problem!'
    );

    System.assertEquals(
      new List<String>{ 'emb', 'emb' },
      (List<String>) incomingRec.slice(
        new List<String>{ 'emb', 'emb', 'emb' },
        0,
        2
      ),
      '[ERROR] Slice function is not working properly!'
    );
    incomingRec.sendEmailToRECInterests();
    incomingRec.sendEmailPreview();
  }

  private testMethod static void getEmailsE2Test() {
    Communications__c rec = [
      SELECT id, name, recordtypeid, fleet_type__c
      FROM communications__c
      LIMIT 1
    ];
    RECTriggerHandler incomingRec = new RECTriggerHandler(
      new List<Communications__c>{ rec }
    );

    List<Id> emailIds = new List<Id>(incomingRec.getEmailsREC().keySet());
    Set<String> allEmails = new Set<String>();
    for (User u : [
      SELECT id, email
      FROM User
      WHERE REC_Relevant_Event_Communication__c = TRUE
    ]) {
      allEmails.add(u.email);
    }

    system.assertEquals(
      emailIds.size(),
      2 + allEmails.size(),
      '[ERROR] The number of ids to send email does not match!'
    );
  }

  private testMethod static void getEmailsEJetTest() {
    Communications__c rec = [
      SELECT id, name, recordtypeid, fleet_type__c
      FROM communications__c
      LIMIT 1
    ];

    rec.fleet_type__c = '170/190';
    Database.update(rec);

    RECTriggerHandler incomingRec = new RECTriggerHandler(
      new List<Communications__c>{ rec }
    );

    List<Id> emailIds = new List<Id>(incomingRec.getEmailsREC().keySet());
    Set<String> allEmails = new Set<String>();
    for (User u : [
      SELECT id, email
      FROM User
      WHERE REC_Relevant_Event_Communication__c = TRUE
    ]) {
      allEmails.add(u.email);
    }
    system.assertEquals(
      emailIds.size(),
      2 + allEmails.size(),
      '[ERROR] The number of ids to send email does not match!'
    );
  }

  private testMethod static void getRelatedAttachmentTest() {
    Communications__c rec = [
      SELECT id, name, recordtypeid, fleet_type__c, email_attachments__c
      FROM communications__c
      LIMIT 1
    ];
    RECTriggerHandler incomingRec = new RECTriggerHandler(
      new List<Communications__c>{ rec }
    );

    system.assertEquals(
      incomingRec.getRelatedAttachments().size(),
      1,
      '[ERROR] The number of attachments does not match!'
    );
  }

  private static Communications__c createREC() {
    Id idRECRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('REC')
      .getRecordTypeId();
    Communications__c rec = new Communications__c();
    rec.RecordTypeId = idRECRecordType;
    rec.communication_status__c = 'draft';
    rec.reference_date__c = system.today();
    rec.ata_chapter__c = '00 GENEREAL';
    rec.technology__c = 'AMS';
    rec.fleet_type__c = '195-E2';
    rec.Name = 'rectest';
    return rec;
  }

  private static Account createAccount(
    String name,
    id recordType,
    Id idEmbraerSite,
    String fleetType
  ) {
    Account acc = new Account();
    acc.Name = name;
    acc.Company_Nickname__c = name + 'cnc';
    acc.Geographical_Area__c = 'Latin America';
    acc.recordTypeId = embraerType;
    if (recordType == airlineType) {
      acc.Company_status__c = 'Active';
      acc.Training_status__c = 'Active';
      acc.recordTypeId = airlineType;
      acc.embraer_site__c = idEmbraerSite;
      acc.Quantity_Embraer_Aircraft_Operator__c = 10;
      acc.Embraer_a_c_in_service_only__c = 9;
      acc.Embraer_Fleet_Type__c = fleetType;
    }
    return acc;
  }

  private static Contact createContact(
    String name,
    Id accId,
    boolean recInterest
  ) {
    Contact ct = new Contact();
    // ct.Name = name + ' ' + name;
    ct.LastName = 'last' + name;
    ct.title = 'Engineer';
    ct.contact_status__c = 'Active';
    ct.email = name + '@wht.emb';
    ct.AccountId = accId;
    ct.Relevant_Events_Communication__c = recInterest;
    return ct;
  }

  private static ContentVersion createAttachment(
    String name,
    Communications__c rec
  ) {
    ContentVersion cv = new ContentVersion();
    cv.title = name;
    cv.pathOnClient = name + '.pdf';
    cv.versionData = Blob.valueOf(name);
    cv.isMajorVersion = true;
    Database.insert(cv);
    cv = [
      SELECT id, contentDocumentId, title
      FROM ContentVersion
      WHERE id = :cv.Id
      LIMIT 1
    ];

    ContentDocumentLink cdl = new ContentDocumentLink();
    cdl.ContentDocumentId = cv.ContentDocumentId;

    cdl.LinkedEntityId = rec.Id;
    cdl.ShareType = 'V';
    Database.insert(cdl);

    return cv;
  }
}