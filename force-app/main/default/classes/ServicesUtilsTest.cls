@isTest(SeeAllData=true)
public class ServicesUtilsTest {
    
    @isTest static void changeToAircrfatModelInSalesforceTest(){
        
        ServicesUtils servicesUtils = new ServicesUtils();
        
        String modelTest1 = '19500232';
        
        String newModeltest1 = servicesUtils.changeToAircrfatModelInSalesforce(modelTest1);
        
        System.assertEquals(newModeltest1, '19000232');
        
        String modelTest2 = '14500232';
        
        String newModeltest2 = servicesUtils.changeToAircrfatModelInSalesforce(modelTest2);
        
        System.assertEquals(newModeltest2, '14500232');
        
        String modelTest3 = '17500232';
        
        String newModeltest3 = servicesUtils.changeToAircrfatModelInSalesforce(modelTest3);
        
        System.assertEquals(newModeltest3, '17000232');
    }
    
    @isTest static void createFieldToRelationshipmentBetwenObjectTest(){
        
        ServicesUtils servicesUtils = new ServicesUtils();
        
        String modelTest = '19500232';
        String projectCode = '123456';
        
        String field = servicesUtils.createFieldToRelationshipmentBetwenObject(projectCode, modelTest);
        
        System.assertEquals(field, '12345619000232');
        
        String modelTest2 = '14500232';
        String projectCode2 = '123456';
        
        String field2 = servicesUtils.createFieldToRelationshipmentBetwenObject(projectCode2, modelTest2);
        
        System.assertEquals(field2, '12345614500232');
    }
    
    @isTest static void buildSerialNumber(){
        
        String serialNumber = 'SN: 145758 TN: N16151';
        
        String newSerialNumber = ServicesUtils.buildSerialNumber(serialNumber);
        System.assertEquals(newSerialNumber, '145758');
    }
    
    @isTest static void compleTeWithZero(){
        ServicesUtils servicesUtils = new ServicesUtils();
        
        String model = '190';
        String serialNumber = '232';
        
        String finalCode = servicesUtils.compleTeWithZero(model,serialNumber);
        
        System.assertEquals(finalCode, '19000232');
    }
    
    @isTest static void checkEtrackSerialNumber(){
         
        String serialNumber = 'SN: 145201 TN: G-EMBN';
        
        String newSerialNumber = ServicesUtils.buildSerialNumber(serialNumber);
        
        System.assertEquals(newSerialNumber, '145201');
        
        String newModel = ServicesUtils.breackSerialNumberReturnModel(newSerialNumber);
        
        System.assertEquals(newModel, '145');
        
        String refactorSn = ServicesUtils.breackSerialNumberReturnSerial(newModel, newSerialNumber);
        
        System.assertEquals(refactorSn, '201');
        
        ServicesUtils servicesUtilsNew = new ServicesUtils();
        
        String finalSerialNumber = servicesUtilsNew.compleTeWithZero(newModel, refactorSn);
        
        System.assertEquals(finalSerialNumber, '14500201');   
    }
    
    @isTest static void checksIfModelStartWithZeroTest(){
       
        String model;
        
        model = ServicesUtils.checksIfModelStartWithZero('0190');
        System.assertEquals('190', model);
        
        model = ServicesUtils.checksIfModelStartWithZero('0175');
        System.assertEquals('175', model);
        
        model = ServicesUtils.checksIfModelStartWithZero('0145');
        System.assertEquals('145', model);
        
        model = ServicesUtils.checksIfModelStartWithZero('190');
        System.assertEquals('190', model);
        
        model = ServicesUtils.checksIfModelStartWithZero('175');
        System.assertEquals('175', model);
        
        model = ServicesUtils.checksIfModelStartWithZero('145');
        System.assertEquals('145', model);
    }
}