public class CommercialAgreementTriggerHandler {

    public Map<Id,Agreement__c> newRecordsMap = new Map<Id,Agreement__c>();
    public Map<Id,Agreement__c> oldRecordsMap = new Map<Id,Agreement__c>(); 
    public List<Agreement__c> newRecords = new List<Agreement__c>();
    public List<Agreement__c> oldRecords = new List<Agreement__c>();
    
    public Map<Id,Agreement__c> newRecordsMapWithAgreementFields;
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    public static boolean isRecursive = false;

    public CommercialAgreementTriggerHandler(boolean isExecuting) {
        this.isExecuting = isExecuting;
    }

    public void OnBeforeInsert()
    {
        
        
    }

    public void OnBeforeUpdate()
    {
		
    }

    public void OnBeforeDelete()
    {

    }

    public void OnAfterInsert()
    {

    }

    public void OnAfterUpdate()
    {
        AutoNamingRules.autoNamingFromAgreement(oldRecordsMap, newRecords);
        CLMChangeShowAircraftAs.changeShowAircraftAs(oldRecordsMap, newRecords);
    }

    public void OnAfterDelete()
    {

    }

    public void OnUndelete()
    {

    }
}