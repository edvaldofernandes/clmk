/**
* Controller class for the visualforce page CFCContactUsController.
* Name: CFCContactUsController
* @author - Guilherme Nascimento - gjesus@deloitte.com
* @version 1.0 - 15/12/2015
**/

public class CFCContactUsFinishController {
    
    public String queryStringId {get;set;}
    
    public CFCContactUsFinishController(){
        
        queryStringId = ApexPages.currentPage().getParameters().get('id');
        system.debug('CFCContactUsFinishController.CFCContactUsFinishController().queryStringId >>> ' + queryStringId);
        
    }
}