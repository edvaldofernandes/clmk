@isTest(seeAllData=true)
private class eSightRepositoryTest {
    
    @isTest private static void buildMapWithAllAccountTest(){
        
        Test.startTest();
        Account account = new Account();
        account.Name = 'Account buildMapWithAllAccountTest';
        account.Company_Nickname__c = 'BMWAAT';
        account.COD_ORGz__c = 123456;
        insert account;
        
        String id = account.Id;
        
        Map<String,String> accountMap =  eSightRepository.buildMapWithAllAccount();
        
        System.assertEquals(id, accountMap.get('123456'));
        
        Test.stopTest();
    }
}