public with sharing class FO_CompanyChangeController {
    
    //case a ser alterado
    public Case caso {get;set;}
    //case de ponte apenas que recebe valor da pagina visualforce 
    @TestVisible private Case caseX;
    List<String> lcaseId;
    public String msg {get;set;}
    private ApexPages.StandardController lController;
       
    public FO_CompanyChangeController (ApexPages.StandardController Controller) {
                
        //pega o id dos cases a serem alterados,spassado por parametro na URL
        lcaseId = ApexPages.currentPage().getParameters().get('recs').split(',');
                    
        //Case selecionado na pagina visualforce
        //pega os valores mandados pela pagina visualforce, nesse caso, recebe um case para ser retirado o AccountId
        caseX = (Case)Controller.getRecord();
        
    }

    public PageReference save() {
        
        //atualiza AccountId em todos os cases selecionados
        for (Integer i=0 ; i<lcaseId.size() ; i++) {
            
            caso = [SELECT CaseNumber,AccountId FROM Case WHERE Id =: lcaseId.get(i)];
            caso.AccountId = caseX.AccountId;
            update caso;
            
        }
        adicionaMensagem('Record(s) updated!');
        return null;
        
    }
   
    private static void adicionaMensagem(String msg)
    {
     ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.CONFIRM, msg) );
    }
      
}