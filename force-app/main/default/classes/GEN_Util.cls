/**
* @author Marcilio Leite de Souza
* @date 03/07/2018
* @description: General Class for Utilities methods
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           03 JUL 2018             Original Version
**/
public class GEN_Util {

    /**
	* @description : Return SelectOptions from related picklist field
    * @param List<Schema.PicklistEntry> pickList : List of picklist return by the field Object.field.getDescribe().getPicklistValues()
    * @param Boolean sortList : true for a sort return list
	* @return List<SelectOption> : List select option from the picklist
	*/
    public static List<SelectOption> getSelectOptionFromPickList(List<Schema.PicklistEntry> pickList, Boolean sortList){
        List<SelectOption> opts = new List<SelectOption>();
        for (Schema.PicklistEntry p : pickList) {
            if (p.isActive()) {
                opts.add(new SelectOption(p.getValue(), p.getLabel()));
            }
        }
        if(sortList) opts.sort();
        return opts;
    }
    
    /**
	* @description : Return List<String> from related picklist field
    * @param List<Schema.PicklistEntry> pickList : List of picklist return by the field Object.field.getDescribe().getPicklistValues()
    * @param Boolean sortList : true for a sort return list
	* @return List<String> : List string from the picklist
	*/
    public static List<String> getListStringFromPickList(List<Schema.PicklistEntry> pickList, Boolean sortList){
        List<String> opts = new List<String>();
        for (Schema.PicklistEntry p : pickList) {
            if (p.isActive()) {
                opts.add(p.getValue());
            }
        }
        if(sortList) opts.sort();
        return opts;
    }
    
    /**
	* @description : Date from String DD/MM/AAAA
    * @param String str : String with format brazilian format
	* @return Date : Instance of Date with the values from String
	*/
    public static Date getDateFromStringDate(String str){
        List<String> aux = str.split('/');
        return Date.newInstance(Integer.valueOf(aux.get(2)), Integer.valueOf(aux.get(1)), Integer.valueOf(aux.get(0)));
    }
    
    /**
	* @description : Time from String HH:MM
    * @param String str : String with format time
	* @return Date : Instance of Time with the values from String
	*/
    public static Time getTimeFromStringTime(String str){
        List<String> aux = str.split(':');
        return Time.newInstance(Integer.valueOf(aux.get(0)), Integer.valueOf(aux.get(1)), 0, 0);
    }
    
    /**
	* @description : Copy all files relashioship to other record
    * @param Id parentId : Id of the master record
    * @param Id shareWith : Id of the related record
	* @return Integer : Number of file copied
	*/
    public static Integer sharedContentFiles(Id parentId, Id shareWith){
        List<ContentDocumentLink> listInsertCdl = new List<ContentDocumentLink>();
        
        for (ContentDocumentLink cdl : [SELECT ContentDocumentId, ShareType FROM ContentDocumentLink WHERE LinkedEntityId = :parentId]) {
            ContentDocumentLink newCdl = new ContentDocumentLink();
            newCdl.ContentDocumentId = cdl.ContentDocumentId;
            newCdl.ShareType = cdl.ShareType;
            newCdl.LinkedEntityId = shareWith;
            listInsertCdl.add(newCdl);
        }
        System.debug('GEN_Util.sharedContentFiles.listInsertCdl: '+ listInsertCdl);
        if(listInsertCdl.size() > 0){
            insert listInsertCdl; 
        }
        return listInsertCdl.size();
    }
    
    /**
	* @description : Count how many attached files has the agreement
    * @param Id parentId : Id of the master record
	* @return Integer : how many attached files
	*/
    public static Integer howManyContentDocumentLink(Id rowId){
        //Integer docCounter = 0;
        return [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId =: rowId];
        /*
        for (ContentDocumentLink cdl : [SELECT ContentDocumentId, ShareType FROM ContentDocumentLink WHERE LinkedEntityId = :rowId]){
        	docCounter++;
        }
        
        return docCounter;
        */
    }

    public static Id getRecordTypeIdbyName(String objectName, String strRecordTypeName){
        return  Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().get(strRecordTypeName).getRecordTypeId();
    }

    public static String getRecordTypeNameById(String objectName, Id strRecordTypeId){
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get(strRecordTypeId).getName();
    }
}