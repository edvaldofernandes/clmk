@isTest
public class FO_FupDigestSenderTest {

    /**
    * @description Test if it is possible to get the right contacts.
    **/
    @isTest static void testCanGetContacts(){
        User u = [SELECT Id FROM User LIMIT 1];
        System.runAs(u) {
            EmailTemplate emailTemplate = new EmailTemplate(
                DeveloperName = FO_FupRepository.FUP_DIGEST_EMAIL_TEMPLATE_NAME,
                Name = 'Lorem Ipsum',
                TemplateType = 'text',
                IsActive = true,
                FolderId = u.Id
            );
            insert emailTemplate;         
        }
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        Contact contact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;

        
        Test.StartTest();
       		FO_FupDigestSender sender = new FO_FupDigestSender();
        	List<Contact> actualContacts = (List<Contact>) sender.start(null);
        
        Test.stopTest();
	
        List<Contact> expectedContacts = FO_ContactRepository.getContactsReceivingFupReport();
        
        System.assert(!actualContacts.isEmpty()); 
        
        for(Contact c : actualContacts){
            System.assert(expectedContacts.contains(c));  
        }
    }
    
    
    /**
    * @description Test if it is possible to send FUP Digest to a contact list.
    **/
    @isTest static void testCanSendFupDigest(){
        User u = [SELECT Id FROM User LIMIT 1];
        System.runAs(u) {
            EmailTemplate emailTemplate = new EmailTemplate(
                DeveloperName = FO_FupRepository.FUP_DIGEST_EMAIL_TEMPLATE_NAME,
                Name = 'Lorem Ipsum',
                TemplateType = 'text',
                IsActive = true,
                FolderId = u.Id
            );
            insert emailTemplate;         
        }
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        
        Contact contact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;

        
        Test.StartTest();
            FO_FupDigestSender sender = new FO_FupDigestSender();
        	sender.execute(null, new List<Contact>{contact});
        	Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
            
        System.debug([SELECT Id, Subject FROM EmailMessage]);
        
            
        System.assertEquals(1, invocations, 'An email has not been sent');  
    }
    
    
    
}