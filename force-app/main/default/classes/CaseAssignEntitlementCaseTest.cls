@isTest
public with sharing class CaseAssignEntitlementCaseTest {

  //public static final Integer cont = 3;
  private static final Id TYPE_ECIP = RecordTypeMemory.getRecType('Case', 'ECIP_Sales');

  static testMethod void testPossitivoENTITLEMENT() {

      Account lConta = SObjectInstanceTest.conta();
      lConta.Type = 'Airline';
      //lConta.BillingCountry = 'Brasil';
      lConta.Company_Nickname__c = 'Tseng Aviation';
      database.insert( lConta );

      User lUser = SObjectInstanceTest.createUser(UserInfo.getProfileId());
      database.insert( lUser );
      
      
      Entitlement lEnt = SObjectInstanceTest.createEntitlement( lConta.id );
      lEnt.Name = 'Default';
      //lEnt.Type = 'Phone Support';
      database.insert( lEnt );
      

      User lUser2 = SObjectInstanceTest.createUser(UserInfo.getProfileId());
      lUser2.Username = 'user1name@mail.com.br';
      lUser2.LastName += 1;
      lUser2.Alias += 1;
      lUser2.CommunityNickname += 1;
      database.insert( lUser2 );


      Case lCaso = SObjectInstanceTest.createCase();
      lCaso.RecordTypeId = CaseAssignEntitlementCase.recPRC;
      lCaso.AccountId = lConta.id;
      lCaso.OwnerId = lUser.Id;
      lCaso.Phase__c =  'Inbox';
      lCaso.Status = 'Dispatch';
      //lCaso.Phase__c =  'Processing';
      //lCaso.Status = 'Processing';      
      lCaso.Priority = 'INBOX';
      //lCaso.EntitlementId = lEnt.Id;
      
    

      Test.startTest();
      database.insert( lCaso );
      Test.stopTest();

      Case caseResult = [SELECT Id, EntitlementId, RecordTypeId FROM Case WHERE Id = :lCaso.Id];
   //   System.assert(lent.Id == caseResult.EntitlementId, 'NAO FOI');
  }
  
    
}