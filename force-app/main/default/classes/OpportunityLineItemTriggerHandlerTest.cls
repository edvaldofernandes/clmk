/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class OpportunityLineItemTriggerHandlerTest 
{

	
	@testSetup static void createData() 
    {
    	
    	Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
    	insert acc;
    	
    	Contact contato = new Contact();
    	contato.firstName = 'Teste';
    	contato.LastName = 'unitário';
    	contato.accountId = acc.Id;
    	insert contato;
    	
    	
    
    	Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    	Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
	  	pbAirMod.Name = 'Aircraft Modification';
	  	pbAirMod.Type__c = 'Aircraft Modification';
    	insert pbAirMod;
    
    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
    	opp.Pricebook2Id = pbAirMod.id;
    	opp.stageName = 'Qualification';
    	insert opp ;
        
        OpportunityContactRole oppCr  = new OpportunityContactRole();
        oppCr.ContactId = contato.Id;
        oppCr.OpportunityId = opp.Id;
        insert oppCr;
        
    	Product2 prod = SObjectInstanceTest.createProduct2();
	  	prod.Unit__c = 'Aircraft';
	  	prod.Product_Type__c = 'Aircraft Modification';
	  	prod.Family = 'Aircraft Modification';
	  	prod.Applicability__c = 'Turboprop';
	  	prod.Rom__c = true;
    	insert prod;
    
    	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    	insert pbe;
    
    	PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    	insert pbeAirMod ;
      
    	list<OpportunityLineItem> oppItem = new list<OpportunityLineItem >();
    
    	OpportunityLineItem oppItemEntryFee = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
    	oppItemEntryFee.Princing__c = 'Entry fee';
    	oppItemEntryFee.opportunityId = opp.Id;
    	oppItem.add(oppItemEntryFee );
    
   	 	OpportunityLineItem oppItemNrec = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
	  	oppItemNrec.Princing__c = 'NREC';
	  	oppItemNrec.Entry_fee__c = 200;
	  	oppItemNrec.opportunityId = opp.Id;
    	oppItem.add(oppItemNrec);
    	
    	insert oppItem; 
    	
    	Quote quote = SObjectInstanceTest.createQuote( opp.Id );
    	quote.Pricebook2Id = pbAirMod.Id;
    	quote.OpportunityId= opp.Id;
    	quote.Name = 'First Quote';
    	insert quote;   
    	
    	Quote quote1 = SObjectInstanceTest.createQuote( opp.Id );
    	quote1.Pricebook2Id = pbAirMod.Id;
    	quote1.OpportunityId= opp.Id;
    	quote1.Name = 'Second Quote';
    	insert quote1;   
    
    
    	list< QuoteLineItem > quoteItem = new list< QuoteLineItem >();
    
    	QuoteLineItem qItemEntryFee = SObjectInstanceTest.createQuoteLineItem(quote.Id, pbeAirMod.Id);
    	qItemEntryFee.Princing__c = 'Entry fee';
    	quoteItem.add( qItemEntryFee );
    
    	QuoteLineItem qItemNrec = SObjectInstanceTest.createQuoteLineItem(quote.Id, pbeAirMod.Id);      
	  	qItemNrec.Princing__c = 'NREC';
	  	qItemNrec.Entry_fee__c = 200;
    	quoteItem.add( qItemNrec );
    	
    	QuoteLineItem qItemEntryFee1 = SObjectInstanceTest.createQuoteLineItem(quote1.Id, pbeAirMod.Id);
    	qItemEntryFee1.Princing__c = 'Entry fee';
    	quoteItem.add( qItemEntryFee1 );
    
    	QuoteLineItem qItemNrec1 = SObjectInstanceTest.createQuoteLineItem(quote1.Id, pbeAirMod.Id);      
	  	qItemNrec1.Princing__c = 'NREC';
	  	qItemNrec1.Entry_fee__c = 300;
    	quoteItem.add( qItemNrec1 );
    
    	insert quoteItem;
    	
    	
    	Aircraft__c aircraft = new Aircraft__c();
      	aircraft.Name = '12345678';
        aircraft.Aircraft_Status__c = 'In Service';
        aircraft.Commercial_Name__c = 'EMB-110P1';
      	insert aircraft;
      	
      	Aircraft__c aircraft2 = new Aircraft__c();
      	aircraft2.Name = '87654321';
        aircraft2.Aircraft_Status__c = 'In Service';
        aircraft2.Commercial_Name__c = 'EMB-110P1';
      	insert aircraft2;
      	
      	Related_Aircraft__c rA = new Related_Aircraft__c();
      	rA.Aircraft__c = aircraft.Id;
      	rA.Quote__c = quote.Id; 
      	ra.Quote_Line_Item__c = quoteItem[1].Id;
      	insert rA;
      	
      	Related_Aircraft__c rA2 = new Related_Aircraft__c();
      	rA2.Aircraft__c = aircraft2.Id;
      	rA2.Opportunity__c = opp.Id;
      	insert rA2;
    
        
        
    	
    
	}
	

	static testMethod void myUnitTest()
  	{    
  		test.startTest();
    	
    	List<Quote> quotes = [Select Id From Quote];    
       	List<Opportunity> opps = [Select StageName,SyncedQuoteId,Amount From Opportunity ];
        
               
        opps[0].StageName = 'Proposal/Price Quote';
        opps[0].Amount = 200;
        opps[0].SyncedQuoteId = quotes[0].Id;
        update opps[0];
        

        
        /*
        List<QuoteLineItem>  itens = [Select Quantity From QuoteLineItem Where Princing__c = 'NREC' ] ;
         
        itens[0].Quantity = 3;
        update itens;  
              
        //opp.StageName = 'Closed Lost';
        //opp.Reason_for_Rejection__c = 'Price too high';
        //opp.Comments_for_Rejection__c = 'teste';
        //update opp;    	
        */
       
       OpportunityLineItemTriggerHandler handler = new OpportunityLineItemTriggerHandler(true);
       System.AssertEquals(handler.IsTriggerContext,true);
    
    }
    
    
}