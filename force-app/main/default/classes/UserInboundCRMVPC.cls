/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; March-2016.
**/
global without sharing class UserInboundCRMVPC
{
    
                                                          
    //Classe que recebe os parâmetros do Evento
    global without sharing class EventQueue
    {
        webservice string businessDocumentNumber{get;set;}
        webservice string businessDocumentCorrelatedNumber{get;set;}
        webservice string eventName{get;set;}
        webservice string externalCreationDate{get;set;}
        webservice string internalID{get;set;}
        webservice string priority{get;set;}
        webservice string sender{get;set;}
        webservice string sourceObject{get;set;}
        webservice string standard{get;set;}
    }
    
    webservice static string syncUserInfo (EventQueue event, Account account, Contact contact, User user, List<String> profile )
    {
        
        string returnValue = 'User inserted/updated sucessfully!';
        validationCommunityUsers validation = new validationCommunityUsers();
        
        //Atribuo os objetos recebidos pelo serviço às variáveis da classe processadora
        validation.contactValidationClass = contact;
        validation.accountValidationClass = account;
        validation.userValidationClass = user;
        
        System.Debug('The method syncUserInfo was acessed in : ' + string.valueOf(system.now()) +  ' -  and it works.' );
        System.Debug('Account - ' + account);
        System.Debug('Contact - ' + contact);
        System.Debug('User - ' + user);
        System.Debug('Profile - ' + profile);
        System.Debug('Event - ' + event);
        
        
        //Chamo o método que vai fazer a validação do usuário/contato    
        if(!validation.validateCommunityUser())
            returnValue  = 'Error : ' + String.join(validation.messageErrors, ';');
    
                    
        return returnValue;
                    
    }
    
    global without sharing class validationCommunityUsers
    {
        
        public Contact contactValidationClass{private get;set;}
        public Account accountValidationClass{private get;set;}
        public User userValidationClass{private get;set;}
        public list<String> messageErrors{get;private set;}
        private boolean needUpdateContact;
        private boolean needUpdateUser;
        private User existingUser;
        private Contact existingContact;
        private UserInboundSettings__c settings = UserInboundSettings__c.getInstance();
        private final Id cfcDefaultProfileId = [Select Id from Profile Where Name = : settings.DefaultServicesCatalogUserProfile__c].Id;
        private final string cfcDefaultAccountFlyEmbraerId = settings.GenericAccountFlyEmbraerId__c;
        private List<String> contactFields = settings.ContactFieldsToCompare__c.split(',');
        private List<String> userFields = settings.UserFieldsToCompare__c.split(',');
        
        //Construtor
        public validationCommunityUsers()
        {
        	try
        	{
            	//Inicializo a lista que vai conter eventuais erros para exibição no retorno e outras variáveis
            	messageErrors = new list<String>();
            	needUpdateContact = false;
            	needUpdateUser = false;
        	}
        	catch(exception ex)
        	{
                messageErrors.add(ex.getMessage() + ' in validationCommunityUsers');
        	}
            
        }
        
        //Método principal, que vai validar informações e processar as inserções/atualizações necessárias
        public boolean validateCommunityUser()
        {
            boolean returnValue = true;
            Savepoint spDMLUserCommunity;
            Id accountId;
            Id contactId;
            
            try
            {
            	
            	//Verifico se o usuário existe
                existingUser = returnExistingUser();
                
                //Verifico se o contato existe
                existingContact = returnExistingContact();
                
                //Se o usuário existe, verifico se é necessário atualizar
                if(existingUser != Null)
                {
                    needUpdateUser = checkNeedUpdateUser();
                }
                else
                {
                    //Se o usuário não existe, preencho os campos padrão
                    completeNewUserInformation();
                }
                
                //Se o contato existe, verifico se é necessário atualizar
                if(existingContact != Null)
                {
                    needUpdateContact = checkNeedUpdateContact();
                }
                else
                {
                    //Se o contato não existe, preencho os campos padrão
                    contactValidationClass.Contact_Status__c = 'Active';
                }
                
                //Crio um savePoint para iniciar a transação DML
                spDMLUserCommunity = Database.setSavepoint();   
                
                //Recupero o Id da Conta 
                accountId= returnAccountId();
                
                //Se o contato não existe, crio um novo
                if(existingContact == Null)
                {
                	//Se o usuário está inativo, não insiro o contato
                	if(userValidationClass.IsActive )
                    {
                    	contactValidationClass.AccountId = accountId;
                    	insert contactValidationClass;
                    }
                }
                else
                {
                    //Se o contato já existe e deve ser atualizado, atualizo
                    if(needUpdateContact)
                    {
                    	existingContact.AccountId = accountId;
                        update existingContact;
                    }
                }
                
                //Se o id do contato já existia, populo o campo
				if(existingContact != Null)
				{
					contactId =  existingContact.Id;
                }
				else
				{
					if(userValidationClass.IsActive)
                    	contactId =  contactValidationClass.Id;
				}
                
                //Se o usuário não existe, crio um novo
                if(existingUser == Null)
                {
                	//Só insiro o contato se o usuário estiver ativo
                	if(userValidationClass.IsActive)
                    {
                		if(userValidationClass.ContactId == Null)
                			userValidationClass.ContactId = contactId;
                		
                		insert userValidationClass;
                    } 
                }
                else
                {
                   //Se o usuário já existir e deve ser atualizado, atualizo
                    if(existingUser.ContactId == Null)
                		existingUser.ContactId = contactId;
                	
                	if(needUpdateUser)
                		update existingUser;
                }
                
            }
            catch(exception ex)
            {
                //Se ocorrer qualquer exceção, faço o rollback da transação, insiro uma mensagem de erro e atribuo valor false ao retorno do método
                returnValue = false;
                Database.rollback(spDMLUserCommunity);
                System.Debug(' Erro ao inserir - ' + ex.getMessage());
                messageErrors.add(ex.getMessage() + ' in validateCommunityUser');
            } 
           
            return returnValue;
        
        }
        
        
        
        private User returnExistingUser()
        {
            User returnValue;
            List<User> userTempList;
            String federationId = '';
            
            try
            {
                federationId = userValidationClass.FederationIdentifier;
                if(string.IsEmpty(federationId))
                	federationId = userValidationClass.FlyEmbraerUserId__c;
            	
            	userTempList = database.query('SELECT ' + String.join(this.userFields, ',')  + ' FROM User WHERE IsPortalEnabled  = true And IsActive = true And FederationIdentifier = : federationId LIMIT 1');
        		
                if(!userTempList.isEmpty())
                	returnValue = userTempList[0];	
            }
            catch(exception ex)
            {
                 messageErrors.add(ex.getMessage() + ' in returnExistingUser'); 
            }
            return returnValue;
        
        }
        
        
        private Contact returnExistingContact()
        {
            Contact returnValue;
            List<Contact> contactTempList;
            List<User> userTempList;
            String flyEmbraerId;
            String lastNameContact;
            String emailContact;
            
            
            try
            {
                flyEmbraerId = contactValidationClass.FlyEmbraerUserId__c;
                emailContact = contactValidationClass.email;
                
                contactTempList = database.query('SELECT '  + String.join(contactFields, ',') + ' FROM Contact Where (Email = : emailContact) OR (FlyEmbraerUserId__c = : flyEmbraerId)');
               
				for(Contact contactTemp : contactTempList)
				{
					//A principio, atribuo o contato que vier ao valor
					returnValue = contactTemp;
					if(contactTemp.FlyEmbraerUserId__c == flyEmbraerId)
					{
						//se o contato tiver o flyEmbraerId, atribuo esse e saio do laço
               	 		returnValue = contactTemp;
               	 		break;
					}
				}
				if(returnValue != Null)
				{
					for(user userTemp : [Select Id,FederationIdentifier FROM User WHERE IsPortalEnabled  = true And IsActive = true And ContactId = : returnValue.Id])
					{
						if(userTemp.FederationIdentifier != userValidationClass.FederationIdentifier)
						{
							contactValidationClass.email = null;
							returnValue = null;
						}
					}	
				}
            }
            catch(exception ex)
            {
                messageErrors.add(ex.getMessage() + ' in returnExistingContact');       
            }
            return returnValue;
        
        }
        
        private boolean checkNeedUpdateContact()
        {
            
			boolean returnValue = false;
            try
            {
                //Faço o laço na lista de campos do contato para comparação
	            for(string field : contactFields)
	            {
	            	
	                //Se o campo está nulo no salesforce e há valor informado pelo serviço, atualizo
	                if(existingContact.get(field) == Null && contactValidationClass.get(field) != Null)
	                {   
	                	existingContact.put(field,contactValidationClass.get(field));
	                    returnValue = true;
	                }
	            }
	            
            }
            
            
        	catch(exception ex)
        	{
                messageErrors.add(ex.getMessage() + ' in checkNeedUpdateContact');
        	}
            return returnValue;
        }
        
        private boolean checkNeedUpdateUser()
        {
            boolean returnValue = false;
            
            try
            {
            	
	            //Faço o laço na lista de campos do usuário para comparação
	            for(string field : userFields)
	            {
	            	
	            	//Se o campo está nulo no salesforce e há valor informado pelo serviço, atualizo
	                if(existingUser.get(field) == Null && userValidationClass.get(field) != Null)
	                {   
	                    existingUser.put(field,userValidationClass.get(field));
	                    returnValue = true;
	                }
	                
	                
	            }
	            
            }
            catch(exception ex)
        	{
                messageErrors.add(ex.getMessage() + ' in checkNeedUpdateUser');
        	}
            return returnValue;
        }
        
        
        
        
        private void completeNewUserInformation()
        {
            try
            {
                //Preenche as informações estáticas do novo usuário
                if(string.isEmpty(userValidationClass.username))
                    userValidationClass.username = userValidationClass.FlyEmbraerUserId__c + '@flyembraer.com';
                
                userValidationClass.ProfileId = this.cfcDefaultProfileId;
                userValidationClass.TimeZoneSidKey = string.valueOf(UserInfo.getTimeZone());
                userValidationClass.LocaleSidKey = UserInfo.getLocale();
                userValidationClass.EmailEncodingKey = 'ISO-8859-1';
                userValidationClass.LanguageLocaleKey = UserInfo.getLanguage();
                userValidationClass.Alias = userValidationClass.FlyEmbraerUserId__c.left(8);
                userValidationClass.FederationIdentifier = userValidationClass.FlyEmbraerUserId__c;
                userValidationClass.CountryCode = userValidationClass.Country;
                
                if(userValidationClass.CountryCode == 'UK')
                	userValidationClass.CountryCode = 'GB';
                	
                userValidationClass.Country = '';
                userValidationClass.IsActive = true;
            }
            catch(exception ex)
            {
                messageErrors.add(ex.getMessage() + ' in completeNewUserInformation');
            } 
             
        
        }
        
        
        
        private Id returnAccountId()
        {
            Id returnValue = null;
            Id genericAccountId; 
            Id selectedAccountId; 
            Set<String> flyEbraerIdsSet = new Set<String>();
            flyEbraerIdsSet.add(cfcDefaultAccountFlyEmbraerId); //FlyEmbraer Id genérico
            
            if(accountValidationClass.Id != Null)
            	flyEbraerIdsSet.add(accountValidationClass.Id);
            
            if(accountValidationClass.FlyEmbraerId__c != Null)
            	flyEbraerIdsSet.add(accountValidationClass.FlyEmbraerId__c);
            
            
            try
            {
                if(existingContact == Null)
                {
                    
                    for(Account acc : [Select Id,FlyEmbraerId__c,Name From Account Where FlyEmbraerId__c in :  flyEbraerIdsSet])
                    {
                    	if(acc.FlyEmbraerId__c == cfcDefaultAccountFlyEmbraerId)
                        {
                            genericAccountId = acc.Id;
                        }
                        else
                        {
                            selectedAccountId = acc.Id; 
                        }
                        
                    }
                }
                else
                {
                	selectedAccountId = existingContact.AccountId;
                }
                
                returnValue = selectedAccountId;
                
                if(selectedAccountId == Null)
                    returnValue = genericAccountId;
                
                if(returnValue == Null)
                    messageErrors.add('Account not found with FlyEmbraer Id = '  + accountValidationClass.FlyEmbraerId__c);                        
                
                
            }
            catch(exception ex)
            {
                messageErrors.add(ex.getMessage() + ' in returnAccountId' );
            } 
             
            return returnValue;
        
        }
                    
        
               
    
    }

}