/*-------------------------------------------------------------------------------
*
* When a ServiceContract is deleted or updated to Dropped, this class
* updates the available quantity of the related slot.
* NAME: ServiceContractUpdateSlot.cls
* AUTHOR: DPF                                                DATE: 17/12/2014
*
*******************************************************************************/
public with sharing class ServiceContractUpdateSlot {

  public static void delOpp()
  {
    TriggerUtils.assertTrigger();

    // if tem slot devolve o saldo
    list< ContractLineItem > lLstOppItens = [ select Id, Slot__c, Product_Type__c, Quantity
      from ContractLineItem where ServiceContractId =:( list< ServiceContract > )trigger.old
      and Slot_status__c = 'Success' ];

    LineItemSearchSlot.deleteProducts( lLstOppItens );
  }

  public static void closeOpp()
  {
    TriggerUtils.assertTrigger();

    list< id > lListOppID = new list< id >();
    for ( ServiceContract lSC : ( List< ServiceContract > )trigger.new )
    {
      if ( TriggerUtils.wasChangedTo( lSC, ServiceContract.Contract_Status__c, 'Dropped' ) )
        lListOppID.add( lSC.id );
    }

    if ( lListOppID.isEmpty() ) return;

    list< ContractLineItem > lLstOppItens = [ select Slot__c, Product_Type__c, quantity
      from ContractLineItem where ServiceContractId =:lListOppID and Slot_status__c = 'Success' ];

    LineItemSearchSlot.deleteProducts( lLstOppItens );
  }
}