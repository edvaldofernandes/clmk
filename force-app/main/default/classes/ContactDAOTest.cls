@IsTest
public with sharing class ContactDAOTest {
    
    private static ContactDAO contactDAO;

    static {
        contactDAO = new ContactDAO();
    }

    @TestSetup
    private static void testSetup(){
        Contact contact = createContact();
        Database.insert(contact);
    }

    @IsTest
    public static void shouldGetContactsByEmail(){

        Test.startTest();

        List<Contact> contacts = contactDAO.getContactsByEmail(new Set<String>{'assign_account@test.com.br'});

        Test.stopTest();

        System.assert(contacts != null);
        System.assert(!contacts.isEmpty());
    }

    private static Contact createContact(){

        Contact contact = new Contact(
            FirstName = 'Test-First',
            LastName = 'Test-Second',
            Email = 'assign_account@test.com.br'
        );

        return contact;
    }
}