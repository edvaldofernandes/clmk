/**
* Controller class for the visualforce page CFCBillboardEdit.
* Name: CFCBillboardEditController
* @author - lucaoliveira@deloitte.com
* @version 1.0 - 02/02/2016
**/ 
public with sharing class CFCBillboardEditController 
{
	public Billboard__c fieldBillboard			{get; set;}
	
	public CFCBillboardEditController(ApexPages.StandardController stdController)
	{
		this.fieldBillboard = (Billboard__c)stdController.getRecord();
		fieldBillboard = [Select Publish_Status__c From Billboard__c Where Id = :fieldBillboard.Id ];
	}
	
	public pageReference validatePublication()
    {
    	PageReference page;
    	
    	System.debug('>>>> Publication_Status__c 2'+fieldBillboard.Publish_Status__c);		
    	if(fieldBillboard.Publish_Status__c.equals('Published'))
    	{
    		page = new PageReference('/'+ fieldBillboard.Id);       
			return page;
    	} else{
            //https://cs42.salesforce.com/01t5600000031gp/e?retURL=%2F01t5600000031gp
    		//page = new PageReference('/'+ fieldProduct.Id + '/e?nooverride=1');       
    		page = new PageReference('/'+ fieldBillboard.Id + '/e?nooverride=1&retURL=%2F'+fieldBillboard.Id);       
			return page;
    	}
	}
}