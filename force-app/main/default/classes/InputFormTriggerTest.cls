@isTest
public class InputFormTriggerTest {
    
    private static Id RecordTypeIdRFPOnline = Schema.SObjectType.InputForm__c.getRecordTypeInfosByName().get('RFP Online').getRecordTypeId();
    
    private static String accountName = 'Test Account';
    private static String attended = 'Attended';
    private static String contactFirstName = 'John';
    private static String contactLastName = 'Doe';
    private static String contactEmail = 'contactEmail@test.com';
    private static String contactNoAccountEmail = 'contactNoAccountEmail@test.com';

    private static String ecamRole ='ECAM - Embraer Customer Account Manager';
    private static String customerEspectation = 'Expects to test stuff';
    
    @testSetup static void setup() {
        /*
         * Test generic email
         * 		insert Input Form without contact
         * 		isnert Input Form with contact and account but no ECAM
         * 
         * Test Ecam email
         * 		Insert Input Form with contact, account and ecam
		*/
        
        insert new CRM_Customer_RFP__c(Enable_RFP_Notification_System__c = true, Email_address_to_send_by_default__c = 'mateus.caviglione@embraer.net.br', Account_Team_Roles_To_Receive_Email__c = 'ECAM - Embraer Customer Account Manager,CAM - Customer Account Manager,LCAM - Leasing Company Account Manager,Tech Rep');
        

        
        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);
        
        Account accountNoEcam = new Account();
        accountNoEcam.Name = accountName + '1';
        accountNoEcam.Company_Nickname__c = accountName + '1';
        accountsToInsert.add(accountNoEcam);

        insert accountsToInsert;
        
        //Create contacts related to the account
        List<Contact> contactsToInsert = new List<Contact>();
        
        Contact testContact = new Contact();
        testContact.FirstName = contactFirstName;
        testContact.LastName = contactLastName;
        testContact.AccountId = account.Id;
        testContact.Email = contactEmail;
        testContact.Account_Name_workflow__c = account.Name;
        contactsToInsert.add(testContact);
        
        Contact contactNoAccount = new Contact();
        contactNoAccount.FirstName = contactFirstName;
        contactNoAccount.AccountId = accountNoEcam.Id;
        contactNoAccount.Account_Name_workflow__c = accountNoEcam.Name;
        contactNoAccount.LastName = contactLastName;
        contactNoAccount.Email = contactNoAccountEmail;
        contactsToInsert.add(contactNoAccount);
        
        insert contactsToInsert;
        
        List<AccountTeamMember> accountTeam = new List<AccountTeamMember>();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User ecamUser = new User(Alias = 'standt', Email='mateus.caviglione@embraer.net.br', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='Anotherstandarduser@testorg.com');
        insert ecamUser;
        
        AccountTeamMember Ecam = new AccountTeamMember();
        Ecam.AccountId = account.id;
        Ecam.TeamMemberRole = ecamRole;
        Ecam.UserId = ecamUser.id;
        accountTeam.add(Ecam);
        
        insert accountTeam;
    }
    private static testmethod void Test01ECAMandClient(){

        InputForm__c input = new InputForm__c();
        input.RecordTypeId = RecordTypeIdRFPOnline;
        input.Subject__c = 'Test RFP';
        input.Requester_Name__c = 'Requester Name';
        input.Requester_Company__c ='Doest Not Matter';
        input.Requester_Email__c = contactEmail;
        input.Requester_Phone__c = '000';
        input.Send_RFP_to_requester__c = true;
        
        Test.startTest();
        
        insert input;
        
        Integer beforeInvocations = Limits.getEmailInvocations();
        
        test.stopTest();
        
        System.assertEquals(1,beforeInvocations,'1 Email sent');      
    }
    private static testmethod void Test02ECAM(){
        
        InputForm__c input = new InputForm__c();
        input.RecordTypeId = RecordTypeIdRFPOnline;
        input.Subject__c = 'Test RFP';
        input.Requester_Name__c = 'Requester Name';
        input.Requester_Company__c ='Doest Not Matter';
        input.Requester_Email__c = contactEmail;
        input.Requester_Phone__c = '000';
        input.Send_RFP_to_requester__c = false;
        
        Test.startTest();
        
        insert input;
        
        Integer beforeInvocations = Limits.getEmailInvocations();
        
        test.stopTest();
        
        System.assertEquals(1,beforeInvocations,'1 Email sent');      
    }
    private static testmethod void Test03GenandClient(){
        
        InputForm__c input = new InputForm__c();
        input.RecordTypeId = RecordTypeIdRFPOnline;
        input.Subject__c = 'Test RFP';
        input.Requester_Name__c = 'Requester Name';
        input.Requester_Company__c ='Doest Not Matter';
        input.Requester_Email__c = 'contactEmail@email.com';
        input.Requester_Phone__c = '000';
        input.Send_RFP_to_requester__c = true;
        
        Test.startTest();
        
        insert input;
        
        Integer beforeInvocations = Limits.getEmailInvocations();
        
        test.stopTest();
        
        System.assertEquals(1,beforeInvocations,'1 Email sent');      
    }
    private static testmethod void Test04Gen(){
        
        InputForm__c input = new InputForm__c();
        input.RecordTypeId = RecordTypeIdRFPOnline;
        input.Subject__c = 'Test RFP';
        input.Requester_Name__c = 'Requester Name';
        input.Requester_Company__c ='Doest Not Matter';
        input.Requester_Email__c = 'contactEmail@email.com';
        input.Requester_Phone__c = '000';
        input.Send_RFP_to_requester__c = false;
        
        Test.startTest();
        
        insert input;
        
        Integer beforeInvocations = Limits.getEmailInvocations();
        
        test.stopTest();
        
        System.assertEquals(1,beforeInvocations,'1 Email sent');      
    }
    private static testmethod void Test05GenandClient(){
        
        InputForm__c input = new InputForm__c();
        input.RecordTypeId = RecordTypeIdRFPOnline;
        input.Subject__c = 'Test RFP';
        input.Requester_Name__c = 'Requester Name';
        input.Requester_Company__c ='Doest Not Matter';
        input.Requester_Email__c = contactNoAccountEmail;
        input.Requester_Phone__c = '000';
        input.Send_RFP_to_requester__c = true;
        
        Test.startTest();
        
        insert input;
        
        Integer beforeInvocations = Limits.getEmailInvocations();
        
        test.stopTest();
        
        System.assertEquals(1,beforeInvocations,'1 Email sent');      
    }
        private static testmethod void Test06GenandClient(){
        
        InputForm__c input = new InputForm__c();
        input.RecordTypeId = RecordTypeIdRFPOnline;
        input.Subject__c = 'Test RFP';
        input.Requester_Name__c = 'Requester Name';
        input.Requester_Company__c ='Doest Not Matter';
        input.Requester_Email__c = contactNoAccountEmail;
        input.Requester_Phone__c = '000';
        input.Send_RFP_to_requester__c = false;
        
        Test.startTest();
        
        insert input;
        
        Integer beforeInvocations = Limits.getEmailInvocations();
        
        test.stopTest();
        
        System.assertEquals(1,beforeInvocations,'1 Email sent');      
    }
}