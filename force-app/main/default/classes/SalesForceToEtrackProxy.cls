public class SalesForceToEtrackProxy {
    
    @future(callout=true) 
    public static void executeAccount(){
        
        ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort salesForceRequest = new ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
        
        SalesForceToEtrackAccountService sfToEtrackAccountService = new SalesForceToEtrackAccountService();
          
        salesForceElement.account = sfToEtrackAccountService.buildAccount(); 
        
        RecordResponse.recordResponse(sendRequest(salesForceRequest, salesForceElement), Constants.ETRACK_OUT_BOUND_END_POINT);
        
        CallOutRepository.deleCalloutSent(sfToEtrackAccountService.getCalloutDeleteList());
        
    }
    
    @future(callout=true)
    public static void executeAircraft(){
        
        ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort salesForceRequest = new ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
        
        SalesForceEtrackAircraftService sfEtrackAircraftService = new SalesForceEtrackAircraftService();
        
        salesForceElement.aircraft = sfEtrackAircraftService.buildAircraft(); 
       
        RecordResponse.recordResponse(sendRequest(salesForceRequest, salesForceElement), Constants.ETRACK_OUT_BOUND_END_POINT);
        
        CallOutRepository.deleCalloutSent(sfEtrackAircraftService.getCalloutDeleteList());
    }
    
    @future(callout=true)
    public static void executeTeamMemberUpdated(){
        
        ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort salesForceRequest = new ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
        
        salesForceElement.teamMember = SalesForceToEtrackAccountService.buildTeamMemberUpdatedOrInserted(RcpRepository.searchAccountTeamMemberUpdated(), Constants.OLD_OBJECT_UPDATED_OR_DELETED); 
        sendRequest(salesForceRequest, salesForceElement);
        
    }
    
    @future(callout=true)
    public static void executeTeamMemberInserted(){
        
        ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort salesForceRequest = new ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
        
        salesForceElement.teamMember = SalesForceToEtrackAccountService.buildTeamMemberUpdatedOrInserted(RcpRepository.searchAccountTeamMemberInserted(), Constants.NEW_OBJECT_CREATED); 
        recordEvent(sendRequest(salesForceRequest, salesForceElement), 'ETRACK_OUT_BOUND_END_POINT');
    }
    
    private static String sendRequest(ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort salesForceRequest, 
                                      ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement){
       
        buildBasicAuthentication(salesForceRequest);
        
        String response = salesForceRequest.updateFromSalesForceRequest(salesForceElement);
                                        
        return response;                                
    }
    
    private static void buildBasicAuthentication(ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort salesForceRequest){
        
        Blob headerValue = Blob.valueOf(WebServiceConfiguration.getAuthentication(Constants.ETRACK_GET_QUESTIONS));
        
        String authorizationHeader = Constants.BASIC_AUTHORIZATION + EncodingUtil.base64Encode(headerValue);
		
        salesForceRequest.inputHttpHeaders_x = new Map<String, String>();
        salesForceRequest.inputHttpHeaders_x.put(Constants.AUTHORIZATION,authorizationHeader);
    }
    
    public static void recordEvent(String status, String application){
        
        EventTemplate eventTemplate = new EventTemplate();
        eventTemplate.buildInfoEvent(status, application);
    }
}