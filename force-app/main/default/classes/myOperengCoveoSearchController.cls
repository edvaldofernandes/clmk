public class myOperengCoveoSearchController {
    public static String EMAIL_DOMAIN_REGEX = '@[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9-]+)*';
    public final static Id FLIGHT_OPS_RECORD_TYPE = Schema.SObjectType.Case.getRecordTypeInfosByName(
    ).get('FlightOps Case Record Type').getRecordTypeId();
    
    // the filter template to be used as a filter on Coveo Lightning component
    // for more information, see the documentation:
    // https://docs.coveo.com/en/1552/cloud-v2-administrators/coveo-cloud-query-syntax-reference
    public final static String COVEO_FILTER_TEMPLATE = 
          '	('
        + '		@objecttype=EmailMessage'
        + '		AND @SfParentAccountId={0}'
        + '		AND  @SfParentRecordTypeId="'+ FLIGHT_OPS_RECORD_TYPE +'"'
        + '		AND ('
        + '			@sfFromAddress = {1}'
        + '			OR @sfToAddress = {1}'
        + '		) '
        + '	) '
        + '	OR ('
        + '		@objecttype=Case'
        + '		AND @SfAccountId={0}'
        + '	)';   

    /**
    * @description This method build the filter used in Coveo Search.
    *     It selects the "Case" and "EmailMessage SObjects and filter the first one
    * by the account and its Record type and the second one by the email domain from
    * the "fromAddress" and "ToAddress", by the parent case record type and the
    * parent case account.
    **/
    @AuraEnabled
    public static String getToken() {
		Map<String, Object> searchToken =  buildSearchToken();

        // Generate a token using the Globals class provided by Coveo.
        // See the Globals Class documentation:
        // https://docs.coveo.com/en/1075/coveo-for-salesforce/globals-class
        return CoveoV2.Globals.generateSearchToken(searchToken);
    }
    
    public static Map<String, Object> buildSearchToken(){
        Id accountId = getCurrentUserAccountId();
        Set<String> emailDomains =  getAccountEmailDomains(accountId);
        String emailDomainFilter = buildExternalEmailsOnlyFilter(emailDomains);
        
        String filter = String.format(
            COVEO_FILTER_TEMPLATE,
            new List<String>{
                accountId,
                emailDomainFilter
          	}
        );
        return new Map<String, Object> {
            'filter' => filter
        };
    }   
    
    
    
    public static Id getCurrentUserAccountId(){
        Id accountId = [
            SELECT contact.account.Id
            FROM
            	User
            WHERE
            	Id =: UserInfo.getUserId()
            LIMIT 1
        ].contact.account.Id;
        return accountId;
    }  
    
    /**
    * @description This method gets the list of email domain from an account and
    * build a string to me used as a filter on Coveo search.
    * e.g. : "("@embraer.com", "@embraer.com.br", "@asf.com")" (This is a String).
    * 
    * @param accountId  : The account Id which is wanted to get its emails filter.
    **/
    public static String buildExternalEmailsOnlyFilter(Set<String> emailDomains){
        String emailFilter = '(';
        
        Iterator<String> iter = emailDomains.iterator();
        while(iter.hasNext()){
            String domain = iter.next();
            emailFilter += '"'+domain+'"';
            if(iter.hasNext()){
                    emailFilter += ', ';
            }
        }
        emailFilter += ')';
        
        return emailFilter;
    }
    
    /**
    * @description Returns the e-mail domains from an account.
    * e.g. : "@embraer.com", "@embraer.com.br", "@asf.com"
    * 
    * @param accountId  : The account Id which is wanted to get its email domains.
    **/
    public static Set<String> getAccountEmailDomains(Id accountId){
        Set<String> domains = new Set<String>();
        Account account = [
            SELECT
            	FlightOps_Account_Domains__c
            FROM
            	Account
            WHERE Id =: accountId
        ];
        Boolean accountHasEmailDomain = !String.isBlank(
            account.FlightOps_Account_Domains__c
        );
        if(accountHasEmailDomain){
            domains = splitEmailDomains(account.FlightOps_Account_Domains__c);
        }else{
            String contactEmailAddress = getCurrentUserContactEmailAddressDomain();
            domains.add(contactEmailAddress);
        }
        return domains;
    } 
    
    /**
    * @description Returns the current user related contact address domain
    **/
    public static String getCurrentUserContactEmailAddressDomain(){
        String emailDomain = '';
        String emailAddress = [
            SELECT
            	Email
            FROM
            	Contact
            WHERE
                Id IN (
                    SELECT
                    	ContactId
                    FROM
                    	User
                    WHERE
                    	Id =: UserInfo.getUserId()
                )
            LIMIT 1
        ].Email;
        Boolean contactHasEmail = !String.isBlank(emailAddress);
        if(contactHasEmail){
            emailDomain = '@' + emailAddress.split('@')[1];
        }else{
            throw new FlightOpsException(
                'Can not find any valid E-mail for the user : '
                + UserInfo.getUserId()
            );
        }
        return emailDomain;
    } 

    /**
    * @description Extracts through regex multiple e-mails from a single string.
    * 
    * @param String domainsString : the email domain list in a single string.
    * @return Set<String> domains : the email domain list.
    **/
    public static Set<String> splitEmailDomains(String domainsString){
        Set<String> domains = new Set<String>();
        Pattern emailDomainPattern = Pattern.compile(EMAIL_DOMAIN_REGEX);
        matcher domainMatcher = emailDomainPattern.matcher(domainsString);
        while(domainMatcher.find()){
        	domains.add(domainMatcher.group());
        }
        return domains;
    }
}