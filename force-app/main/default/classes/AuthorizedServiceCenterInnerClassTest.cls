/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; Jan-2017.
**/

@isTest
public class AuthorizedServiceCenterInnerClassTest 
{
    static testMethod void myUnitTest()
    {
        
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Published';
        database.insert(billboard);
        
        CFC_Configurations__c cs = new CFC_Configurations__c();
        cs.Mobile_Limits_KB__c = 1;
        cs.Name = 'Test CS';
        cs.Tablet_Limits_KB__c = 1;
        cs.Desktop_Limit_KB__c = 1;
        
        cs.Billboard_max_published_number__c = 5;
        cs.URL_Communities__c = 'wwww.teste.com.br';
        cs.Billboard_Time__c = 5;
        cs.Email_to_send_error_job__c = 'teste@teste.com.br';
        cs.User_Name__c = 'teste@teste.com';
        
        database.insert(cs);
        
        //ATTACHMENT DOCUMENT DESKTOP
        Attachments__c attDocument = new Attachments__c();
        attDocument.Billboard__c = billboard.Id;
        attDocument.Active__c = true;
        attDocument.Description__c = 'teste';
        attDocument.Status__c = 'Published';
        attDocument.RecordTypeId = Schema.SObjectType.Attachments__c.getRecordTypeInfosByName().get('Billboards').getRecordTypeId();
        attDocument.Device_Type__c = 'Desktop';
        database.insert(attDocument);
        
        Attachment attachment = new Attachment();  
        attachment.ParentId = attDocument.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = Blob.toPdf('Test Attachment for Parent');
        database.insert(attachment);
        
        AuthorizedServiceCenterInnerClass classInstance = new AuthorizedServiceCenterInnerClass();
        classInstance.setAttachment(attachment);
        classInstance.setAttachmentCustom(attDocument);
        
        Attachment attachmentTest = classInstance.getAttachment();
        Attachments__c attachmentCustomTest = classInstance.getAttachmentCustom();
                 
        
     }
                                      
    
}