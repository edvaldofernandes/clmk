public without sharing class ProductCostControlTriggerHandler
{
    
    public Map<Id,Product_Cost_Control__c> newRecordsMap = new Map<Id,Product_Cost_Control__c>();
    public Map<Id,Product_Cost_Control__c> oldRecordsMap = new Map<Id,Product_Cost_Control__c>(); 
    public List<Product_Cost_Control__c> newRecords = new List<Product_Cost_Control__c>();
    public List<Product_Cost_Control__c> oldRecords = new List<Product_Cost_Control__c>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    public ProductCostControlTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }
    public void OnBeforeInsert()
    {
        deactivateCostControll();
        
    }
    public void OnAfterInsert()
    {
    }
    public void OnBeforeUpdate()
    {
        
    }
    public void OnAfterUpdate()
    {
    }
    
    public void OnBeforeDelete()
    {
        // BEFORE DELETE LOGIC
    }
    public void OnAfterDelete()
    {
        // AFTER DELETE LOGIC
    }
    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }
    public void deactivateCostControll(){
        for(Product_Cost_Control__c prodCC : newRecords){
            System.Debug(newRecords);
            List<Product_Cost_Control__c> productCCList = [
                                                            select Related_Product__c, Is_Active__c
                                                            from Product_Cost_Control__c 
                                                            where Related_Product__c = :prodCC.Related_Product__c
                                                            AND Is_Active__c = True
            											  ];
            prodCC.Is_Active__c = true;
            for(Product_Cost_Control__c prodCC2 : productCCList){
                prodCC2.Is_Active__c = false;
            }
            update productCCList;
        }
    }
}