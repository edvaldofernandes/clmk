/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; March-2016.
**/
@isTest
private without sharing class UserInboundCRMVPCTest 
{

	@testSetup
	static void createTestData()
	{
		List<Account> accountsList = new List<Account>();
		List<User> usersList = new List<User>();
		List<Contact> contactsList = new List<Contact>();
		Id cfcDefaultProfileId = [Select Id From Profile Where Name = 'Services Catalog'].Id;
		UserInboundSettings__c settings = new UserInboundSettings__c();
		
		settings.SetupOwnerId = UserInfo.getProfileId();
		settings.DefaultServicesCatalogUserProfile__c = 'Services Catalog';
        settings.GenericAccountFlyEmbraerId__c = '000000';
        settings.ContactFieldsToCompare__c = 'Id,AccountId,FlyEmbraerUserId__c,Email,FirstName,LastName,Phone,Fax,Title';
        settings.UserFieldsToCompare__c = 'City,CountryCode,ContactId,Email,Extension,Fax,FederationIdentifier,FirstName,LastName,FlyEmbraerUserId__c,IsActive,Phone,PostalCode,Street,Title,Username';
        insert settings;
		
		
		
		
		
		
		//Create Generic Account
		accountsList.add(
							new Account(
      									BillingCountry = 'Brazil',
      									Name = 'Generic Account',
      									Company_Nickname__c = 'Generic Account for Tests',
      									ICAO_Code__c = '0',
      									FlyEmbraerId__c = '000000',
      									RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()
      									)
						);
		//Create other Account
		accountsList.add(
							new Account(
      									BillingCountry = 'Brazil',
      									Name = 'Other Account',
      									Company_Nickname__c = 'Other Account for Tests',
      									ICAO_Code__c = '1',
      									FlyEmbraerId__c = '123456',
      									RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()
      									)
						);
						
		//Create another Account
		accountsList.add(
							new Account(
      									BillingCountry = 'Brazil',
      									Name = 'Another Account',
      									Company_Nickname__c = 'Another Account for Tests',
      									ICAO_Code__c = '2',
      									FlyEmbraerId__c = '789012',
      									RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()
      								)
						);
		
		
    	
		insert accountsList;
		
		contactsList.add(
							new Contact(
    										Title = 'Teste Contato',
    										LastName = 'Contato',
    										Gender__c = 'Male',
    										Contact_Status__c ='Active',
    										Email = 'contact@email.com',
    										FlyEmbraerUserId__c = 'teste1',
    										AccountId = accountsList[1].Id
    									)
    					);
    	
    	contactsList.add(
							new Contact(
    										Title = 'Teste Contato',
    										LastName = 'Contato 2',
    										Gender__c = 'Male',
    										Contact_Status__c ='Active',
    										Email = 'contact2@email.com',
    										AccountId = accountsList[2].Id
    									)
    					);
		
		insert contactsList;
		
		
 		
		
		//Create some users
		usersList.add(
						new User( 
      								Username = 'username1@mail.com.br', 
      								FirstName = 'Test ',
      								LastName = 'User 1', 
      								Email = 'email1@mail.com.br', 
      								Alias = 'Alias1', 
      								CommunityNickname = 'Community Nickname1', 
      								TimeZoneSidKey = 'GMT', 
      								LocaleSidKey = 'en_Us', 
      								EmailEncodingKey = 'ISO-8859-1', 
      								LanguageLocaleKey = 'en_Us', 
      								FederationIdentifier = 'teste1',
      								FlyEmbraerUserId__c = 'teste1',
      								IsActive = true,
      								ProfileId = cfcDefaultProfileId,
      								ContactId = contactsList[0].Id
      							)
					);	
					
				
		
		insert usersList;
		
		
	}
    static testMethod void unitTestUserNotExists() 
    {
    	
    	Contact contactTest = new Contact();
		Account accountTest = new Account();
		User userTest = new User();
		List<string> listProfile = new List<String>();
		String returnValue = '';
		List<User> returnedUsers = new List<User>();
		List<Contact> returnedContacts = new List<Contact>();
		String oldValueUser = '';
		String newValueUser = '';
		
		UserInboundCRMVPC myService = new UserInboundCRMVPC();
			
		UserInboundCRMVPC.EventQueue eventTest = new UserInboundCRMVPC.EventQueue();
		eventTest.businessDocumentNumber = '123';
        eventTest.businessDocumentCorrelatedNumber= '123';
        eventTest.eventName= '123';
        eventTest.externalCreationDate= '123';
        eventTest.internalID= '123';
        eventTest.priority= '123';
        eventTest.sender= '123';
        eventTest.sourceObject= '123';
        eventTest.standard= '123';
    
		
		
		accountTest.FlyEmbraerId__c = '123456'; 
		contactTest.FlyEmbraerUserId__c = 'teste2';
		contactTest.email = 'tester2@teste.com';
		contactTest.LastName = 'Teste 2';
		userTest.FederationIdentifier = 'teste2'; 
		userTest.Email = 'tester2@teste.com';
		userTest.FirstName = 'Test';
		userTest.LastName = 'Mixed';
		userTest.IsActive = true;
		userTest.FlyEmbraerUserId__c = 'teste2';
		
		
		returnedUsers = [Select Id,Country From User Where FederationIdentifier = 'teste2'];
		returnedContacts = [Select Id,Phone From Contact Where FlyEmbraerUserId__c = 'teste2'];
		
		System.AssertEquals(returnedUsers.size(),0);
		System.AssertEquals(returnedContacts.size(),0);
	
		returnValue = UserInboundCRMVPC.syncUserInfo(eventTest,accountTest,contactTest,userTest,listProfile);
			
			
		returnedUsers = [Select Id,Country From User Where FederationIdentifier = 'teste2'];
		returnedContacts = [Select Id,Phone From Contact Where FlyEmbraerUserId__c = 'teste2'];
			
		//Make sure there's only one contact and one user with the provided username
		System.AssertEquals(returnedUsers.size(),1);
		System.AssertEquals(returnedContacts.size(),1);
		
		
		
		
		
    }
    
    static testMethod void unitTestUserContactExists() 
    {
    	Contact contactTest = new Contact();
		Account accountTest = new Account();
		User userTest = new User();
		List<string> listProfile = new List<String>();
		String returnValue = '';
		List<User> returnedUsers = new List<User>();
		List<Contact> returnedContacts = new List<Contact>();
		String oldValueContact = '';
		String newValueContact = '';
		
		
		UserInboundCRMVPC myService = new UserInboundCRMVPC();
			
		UserInboundCRMVPC.EventQueue eventTest = new UserInboundCRMVPC.EventQueue();
		eventTest.businessDocumentNumber = '123';
        eventTest.businessDocumentCorrelatedNumber= '123';
        eventTest.eventName= '123';
        eventTest.externalCreationDate= '123';
        eventTest.internalID= '123';
        eventTest.priority= '123';
        eventTest.sender= '123';
        eventTest.sourceObject= '123';
        eventTest.standard= '123';
    
			
		//First, just check if there is an user and contact. Doesn't update any field
		accountTest.FlyEmbraerId__c = '123456'; 
		contactTest.FlyEmbraerUserId__c = 'teste1';
		contactTest.LastName = 'Teste 1';
		userTest.FederationIdentifier = 'teste1'; 
		userTest.IsActive = true;
		
		returnValue = UserInboundCRMVPC.syncUserInfo(eventTest,accountTest,contactTest,userTest,listProfile);
		
		returnedUsers = [Select Id,Country From User Where FederationIdentifier = 'teste1'];
		returnedContacts = [Select Id,Phone From Contact Where FlyEmbraerUserId__c = 'teste1'];
			
		//Make sure there's only one contact and one user with the provided username
		System.AssertEquals(returnedUsers.size(),1);
		System.AssertEquals(returnedContacts.size(),1);
		
		//Then, Update a contact Field
		contactTest.Phone = '1234-5678';
			
		//get the old Value first
		oldValueContact = returnedContacts[0].phone;
			
		returnValue = UserInboundCRMVPC.syncUserInfo(eventTest,accountTest,contactTest,userTest,listProfile);
			
		returnedContacts = [Select Id,Phone From Contact Where FlyEmbraerUserId__c = 'teste1'];
		
		//get the new value after processing the methods of the service
		newValueContact = returnedContacts[0].phone;
		
		//Make sure the value has changed
		System.AssertNotEquals(newValueContact,oldValueContact);
		
		//For this scenario, it wasn´t possible to test simultaneous update in User and Contact, for limitations in the platform		
	    //Although this test isn't possible in this test method, in the actual tests is possible and it works
		
    }
    
    static testMethod void unitTestUpdateUser() 
    {
    	Contact contactTest = new Contact();
		Account accountTest = new Account();
		User userTest = new User();
		List<string> listProfile = new List<String>();
		String returnValue = '';
		List<User> returnedUsers = new List<User>();
		List<Contact> returnedContacts = new List<Contact>();
		String oldValueUser = '';
		String newValueUser = '';
		
		
		UserInboundCRMVPC myService = new UserInboundCRMVPC();
			
		UserInboundCRMVPC.EventQueue eventTest = new UserInboundCRMVPC.EventQueue();
		eventTest.businessDocumentNumber = '123';
        eventTest.businessDocumentCorrelatedNumber= '123';
        eventTest.eventName= '123';
        eventTest.externalCreationDate= '123';
        eventTest.internalID= '123';
        eventTest.priority= '123';
        eventTest.sender= '123';
        eventTest.sourceObject= '123';
        eventTest.standard= '123';
    
			
		//First, just check if there is an user and contact. Doesn't update any field
		accountTest.FlyEmbraerId__c = '123456'; 
		contactTest.FlyEmbraerUserId__c = 'teste1';
		contactTest.LastName = 'Teste 1';
		userTest.FederationIdentifier = 'teste1'; 
		userTest.IsActive = true;
		
		returnValue = UserInboundCRMVPC.syncUserInfo(eventTest,accountTest,contactTest,userTest,listProfile);
		
		returnedUsers = [Select Id,Fax From User Where FederationIdentifier = 'teste1'];
		returnedContacts = [Select Id,Phone From Contact Where FlyEmbraerUserId__c = 'teste1'];
			
		//Make sure there's only one contact and one user with the provided username
		System.AssertEquals(returnedUsers.size(),1);
		System.AssertEquals(returnedContacts.size(),1);
		
		//Then, Update a User Field
		userTest.Fax = '12345678';
			
		//get the old Value first
		oldValueUser = returnedUsers[0].Fax;
			
		returnValue = UserInboundCRMVPC.syncUserInfo(eventTest,accountTest,contactTest,userTest,listProfile);
			
		returnedUsers = [Select Id,Fax From User Where FederationIdentifier = 'teste1'];
		
		//get the new value after processing the methods of the service
		newValueUser = returnedUsers[0].Fax;
		
		//Make sure the value has changed
		System.AssertNotEquals(newValueUser,oldValueUser);
		
		//For this scenario, it wasn´t possible to test simultaneous update in User and Contact, for limitations in the platform		
	    //Although this test isn't possible in this test method, in the actual tests is possible and it works
		
    }
}