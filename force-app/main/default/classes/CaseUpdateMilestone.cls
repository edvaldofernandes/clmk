public with sharing class CaseUpdateMilestone 
{
  @future
  public static void processar( Set< ID > aListCase )
  {
    list< Case > lCaseList = [ select id, Status from Case where id = :aListCase ];
   /* for ( Case lCase : lCaseList )
    {
      lCase.isClosed__c = (lCase.Status == 'Closed');
   }*/ 
   
    if ( !lCaseList.isEmpty() ) update lCaseList;

  }
}