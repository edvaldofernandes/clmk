public class FlyerExtension {
    
    public String ImageName {get;set;}
    public Product2 productDetail {get; set;}
    public Boolean showTrainingFields {get; set;}
    public String familiesClear {get;set;}
    private Id TrainingRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId();

    
    public FlyerExtension(ApexPages.StandardController stdController){
        
        productDetail = (Product2)stdController.getRecord();
        familiesClear = productDetail.Applicability__c.replaceAll( ';', ' / ');

        setFlyerImg();
        checkForTrainingFields();
    }
    public void checkForTrainingFields(){
        showTrainingFields = false;
        if(productDetail.RecordTypeId == TrainingRecordType)
        {
            showTrainingFields = true;
        }
    }
    public void setFlyerImg (){
        List<String> Families = new List<String>();
        Families = productDetail.Applicability__c.split(';');
        
        //System.debug(Families);
        if(Families.size() == 4){
            ImageName = 'FlyerImgERJE1E2TP';
        }
        else if (Families.size() == 3 && Families.contains('EJET') && Families.contains('E-JET E2') && Families.contains('ERJ') ) {
            ImageName = 'FlyerImgERJE1E2';
        }
        else if (Families.size() == 2 && Families.contains('EJET') && Families.contains('E-JET E2')) {
            ImageName = 'FlyerImgE1E2';
        }
        else if (Families.size() == 1 && Families[0] == 'EJET') {
            ImageName = 'FlyerImgEJET';
        }
        else if (Families.size() == 1 && Families[0] == 'E-JET E2') {
            ImageName = 'FlyerImgE2';
        }
        else if (Families.size() == 1 && Families[0] == 'Turboprop') {
            ImageName = 'FlyerImgTP';
        }
        else if (Families.size() == 1 && Families[0] == 'ERJ') {
            ImageName = 'FlyerImgERJ';
        }
        else {
            ImageName = 'FlyerImgDefault';
        }
        //System.debug(Families[0]);
    }
}