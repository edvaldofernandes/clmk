global class CLMBulkMigrationContract implements Database.Batchable<sObject>, Database.Stateful {
    
    
    global List<CLMMigration.CLMMigrationErroModel> listMigration = new List<CLMMigration.CLMMigrationErroModel>();
    
    global Database.Querylocator start( Database.BatchableContext BC ){
        
        return Database.getQueryLocator([SELECT ID,Name,
                                         Apttus__Account__c,Apttus__Account_Search_Field__c,Account_Type__c,Apttus__FF_Execute__c,Apttus__Activated_By__c,Apttus__Activated_Date__c,Agreement_Status__c,Admin_Mode__c,
                                         Apttus__Agreement_Category__c,Apttus__Contract_End_Date__c,AgreementExternalID__c,General_Comments__c,Agreement_ID__c,Apttus__FF_Agreement_Number__c,Apttus__Contract_Number__c,
                                         Apttus__Agreement_Number__c,Apttus__Contract_Start_Date__c,Aircraft_Basic_Price__c,Aircraft_General_Comments__c,Apttus__AllowableOutputFormats__c,Apttus__FF_Amend__c,CLM_Amend__c,
                                         Amendment_Done__c,Apttus__Amendment_Effective_Date__c,Applicable_Law__c,Applicable_Law_Comments__c,Apttus__Auto_Renewal__c,Basic_Certification__c,Business_Day__c,Business_Day_City__c,
                                         Business_Day_Country__c,Apttus__Business_Hours__c,Buyer__c,Bypass_Approval__c,Comments__c,Bypass_Comments__c,CLM_Bypass_Proposal_Approval__c,Apttus__FF_Cancel_Request__c,
                                         Certification_Basis__c,Certification_Comments__c,Client_Address__c,Client_Name__c,Agreement_Color__c,Color_Preview__c,COM_Code__c,Apttus__Company_Signed_By__c,
                                         Apttus__Company_Signed_Date__c,Apttus__Company_Signed_Title__c,Configuration_Comments__c,Contract_Code__c,Apttus__Contract_Duration_Days__c,ContractOwner__c,Contract_Signature_Date__c,
                                         Apttus__Contracted_Days__c,Country__c,Country_for_Validation__c,CreatedById,CurrencyIsoCode,DataReviewRequired__c,Deal_Value__c,Apttus__Description__c,Agreement_Code__c,DF_Color__c,
                                         DMCG__c,DOCCON_Number__c,DRG__c,EIS_Date__c,Contract_Elaboration_Date__c,Embraer_warranty__c,Enable_Sync__c,Escalation_CAP__c,Escalation_CAP_Comments__c,
                                         Apttus__Executed_Copy_Mailed_Out_Date__c,Expiration_Settlement_Conditions__c,Apttus__FF_Expire__c,ExternalId__c,Fleet_Reliability_Specialist__c,Flight_Attendant_Training__c,
                                         Apttus__FF_View_Final_Contract__c,CLM_Generate__c,Apttus__FF_Generate_Protected_Agreement__c,Apttus__FF_Generate_Supporting_Document__c,Apttus__FF_Generate_Unprotected_Agreement__c,
                                         Guarantee_Comments__c,Apttus__Import_Offline_Document__c,Apttus__InitiateTermination__c,Apttus__Initiation_Type__c,Apttus__Internal_Renewal_Notification_Days__c,
                                         Apttus__Internal_Renewal_Start_Date__c,Is_Amendment__c,Apttus__IsInternalReview__c,Apttus__IsLocked__c,Apttus__Is_System_Update__c,Apttus__Workflow_Trigger_Created_From_Clone__c,
                                         Kickoff_trigger_already_executed__c,LA_Main_Changes__c,LastModifiedById,Apttus__LatestDocId__c,LoadedRecord__c,Maintenance_Program_Specialist__c,Mechanic_Training__c,Mechanics__c,
                                         MFIR_Buyer__c,Nickname__c,Apttus__Non_Standard_Legal_Language__c,Number_of_Contract_Aircraft__c,Old_Total_Agreement_Value__c,On_Site_Support_Comments__c,Operations_Engineer__c,
                                         Opp_Requestor__c,Original_Agreement_for_Amendment__c,Original_PA_Signature_Date__c,Apttus__Other_Party_Returned_Date__c,Apttus__Other_Party_Sent_Date__c,
                                         Apttus__Other_Party_Signed_By__c,Apttus__Other_Party_Signed_By_Unlisted__c,Apttus__Other_Party_Signed_Date__c,Apttus__Other_Party_Signed_Title__c,Apttus__Outstanding_Days__c,
                                         OwnerId,Apttus__Owner_Expiration_Notice__c,PA_Already_set__c,Change_History__c,PA_Signature_Due_Date__c,Apttus__Parent_Agreement__c,Payment_Date__c,PDP_CAP__c,PDP_CAP_Comments__c,
                                         PEP_Agreement__c,Apttus__Perpetual__c,PG__c,Pilot_Training__c,Pilots__c,Apttus__FF_View_Draft_Contract__c,Apttus__Primary_Contact__c,Progress_Bar__c,Proposal_ID__c,
                                         Proposal_Validity__c,RecordTypeId,CLM_Record_Type_Name__c,Apttus__FF_Regenerate_Agreement__c,Region_Code__c,Apttus__Related_Opportunity__c,Related_Proposal__c,
                                         Apttus__Remaining_Contracted_Days__c,Apttus__FF_Renew__c,Apttus__Auto_Renew_Consent__c,Apttus__Renewal_Notice_Date__c,Apttus__Renewal_Notice_Days__c,Apttus__Auto_Renew_Term_Months__c,
                                         Apttus__Auto_Renewal_Terms__c,Apttus__Request_Date__c,Apttus__Requestor__c,Apttus__RetentionDate__c,Apttus__RetentionPolicyId__c,Apttus__FF_Return_To_Requestor__c,Revision_Service__c,
                                         Risk__c,Apttus__Risk_Rating__c,SAP_Contract__c,Apttus__FF_Send_To_Other_Party_For_Review__c,Apttus__FF_Send_To_Other_Party_For_Signatures__c,CLM_Send_to_Customer__c,
                                         Apttus__FF_Send_To_Third_Party__c,Settlement__c,Show_All_Aircraft_As__c,Signature_Date__c,PA_Signature_Duedate__c,SLG__c,SOB_Snapshot_Control_Field__c,Delivered_175__c,
                                         SOB_Firm_Backlog_175__c,SOB_Firm_Undisclosed_175__c,Firms_175__c,Option_175__c,SOB_Option_Undisclosed_175__c,SOB_PRA_Undisclosed_175__c,SOB_PRAs_175__c,Total_175__c,
                                         Apttus__Source__c,Special_Credit_Comments__c,Apttus__Special_Terms__c,Specific_Agreement_Aircrafts_Present_2__c,Specific_Agreement_Aircrafts__c,Start_Date_Is_Changed__c,
                                         Apttus__Status__c,Apttus__Status_Category__c,Apttus__FF_Submit_For_Changes__c,Apttus__FF_Submit_Request__c,Apttus__Submit_Request_Mode__c,Apttus__Subtype__c,TAV_Is_Changed__c,
                                         Tech_Pubs_Perf_Sw_Comments__c,Tech_Pubs_Specialist__c,Tech_Rep__c,Technical_Comments_Hot_Issues__c,Apttus__Term_Months__c,Apttus__FF_Terminate__c,CLM_Terminate__c,
                                         Apttus__TerminationComments__c,Apttus__Termination_Date__c,Termination_Date__c,Apttus__Termination_Notice_Days__c,Apttus__Termination_Notice_Issue_Date__c,
                                         Time_To_Exercise_Options__c,Time_To_Notify_Jurisdiction__c,Apttus__Total_Contract_Value__c,Training_Comments__c,Trigger_Execution__c,Undisclosed_Agreement__c,Undisclosed_Client__c,
                                         Undisclosed_Nickname__c,Vendor_warranty__c,Apttus__Version__c,Apttus__VersionAware__c,Apttus__Version_Number__c,Warranty_Comments__c,Apttus__Workflow_Trigger_Viewed_Final__c
                                         FROM Apttus__APTS_Agreement__c
                                         ORDER BY CreatedDate]);
    }
    
    global void execute( Database.BatchableContext BC, List<sObject> scope ){
        List<Apttus__APTS_Agreement__c> listApptAircrft = (List<Apttus__APTS_Agreement__c>) scope;
        
        List<Agreement__c> listAircraft = new List<Agreement__c>();
        
        Set<String> existingAgreement = new Set<String>();

        List<Agreement__c> agrList = new List<Agreement__c>([SELECT Apptus_Agreement_ID__c FROM Agreement__c WHERE Apptus_Agreement_ID__c <> NULL OR Apptus_Agreement_ID__c <> '']);
        
        for(Agreement__c agr : agrList){
            existingAgreement.add(agr.Apptus_Agreement_ID__c);
        }
        
        for(Apttus__APTS_Agreement__c obj : listApptAircrft){
            Agreement__c aggAir = CLMMigration.apttusToCommercial(obj);
            
            if(!existingAgreement.contains(aggAir.Apptus_Agreement_ID__c)){
               listAircraft.add(aggAir); 
            }
        }
        
        Schema.DescribeFieldResult objField = Agreement__c.Apptus_Agreement_ID__c.getDescribe();
        Schema.sObjectField extField = objField.getSObjectField();
        
        Database.UpsertResult[] srList = Database.upsert(listAircraft,extField,false);
        
        for (Database.UpsertResult sr : srList) {
            
            CLMMigration.CLMMigrationErroModel migErro = new CLMMigration.CLMMigrationErroModel();
            migErro.recordId = sr.getId();
            
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                //System.debug('Successfully inserted . Aircraft Id: ' + sr.getId());
                migErro.erroMsg = 'Successfully Inserted';
            }else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    
                    migErro.recordId = sr.getId();
                    migErro.erroCode = err.getStatusCode();
                    migErro.erroMsg = err.getMessage();
                    migErro.listFields = err.getFields();
                    
                    //System.debug('The following error has occurred.');                    
                    //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    //System.debug('Agreement fields that affected this error: ' + err.getFields());
                }
            }
            this.listMigration.add(migErro);
        }
    }
    
    global void finish( Database.BatchableContext bcMain ){
        
        Messaging.EmailFileAttachment csv = new Messaging.EmailFileAttachment();
        //system.debug('listMigration: ' + listMigration);
        blob xlsxBlob = Blob.valueOf(JSON.serialize(this.listMigration));     
        csv.setFileName('AgreementMigration.json');
        csv.setBody(xlsxBlob);
        csv.setContentType('text/json');  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'felipe.inacio@embraer.com.br'};
        mail.setToAddresses(toAddresses);        
        mail.setSubject('Agreement Migration');     
        mail.setPlainTextBody('Anexo resultado ');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csv});            
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
    
}