/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
public without sharing class OpportunityLineItemTriggerHandler 
{
    public Map<Id,OpportunityLineItem> newRecordsMap = new Map<Id,OpportunityLineItem>();
    public Map<Id,OpportunityLineItem> oldRecordsMap = new Map<Id,OpportunityLineItem>(); 
    public List<OpportunityLineItem> newRecords = new List<OpportunityLineItem>();
    public List<OpportunityLineItem> oldRecords = new List<OpportunityLineItem>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public OpportunityLineItemTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        populateRomField();

    }

 

    public void OnAfterInsert()
    {

        //sincronizeCustomFields();

    }

 

    public void OnBeforeUpdate()
    {
            
    }

 

    public void OnAfterUpdate()
    {
        //sincronizeCustomFields();
        updateRelatedAircrafts();   
    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    
 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }
    
    private void populateRomField()
    {
        Map<Id,Opportunity> mapaOppItem;
        List<OpportunityLineItem> listaItem = new List<OpportunityLineItem>();
        Set<Id> idOpp = new Set<Id>();
        
        
        for(OpportunityLineItem item : this.newRecords)
            item.Rom__c = item.RomFormula__c; 
    }
    
    private void updateRelatedAircrafts()
    {
        Map<Id,Opportunity> mapaOppItem;
        List<Related_Aircraft__c> listaRa = new List<Related_Aircraft__c>();
        List<Related_Aircraft__c> listaRaDel = new List<Related_Aircraft__c>();
        Set<Id> idOpp = new Set<Id>();
        
        
        for(OpportunityLineItem item : this.newRecords)
        {
            if(this.oldRecordsMap.get(item.Id).OppSyncedQuote__c != item.OppSyncedQuote__c || test.IsRunningTest())
                idOpp.add(item.OpportunityId);
        }           
        
        if(!idOpp.isEmpty())
        {
            mapaOppItem = new Map<Id,Opportunity>([Select Id, (Select OpportunityId,Unit__c,Princing__c,PricebookEntry.Product2Id,Quantity,UnitPrice  From OpportunityLineItems)  From Opportunity Where Id in : idOpp]); 
            listaRa = new List<Related_Aircraft__c>();
            listaRaDel = [Select Id From Related_Aircraft__c Where Opportunity__c in : idOpp And Quote_Line_Item__c = Null And Quote__c = Null];
            
            for(QuoteLineItem quoteItem :  [Select Unit__c,Princing__c,Quote.OpportunityId,PricebookEntry.Product2Id,Quantity,UnitPrice,(Select Id, Opportunity__c, Opportunity_product_id__c From Related_Aircraft__r) From QuoteLineItem where Quote.OpportunityId in : mapaOppItem.keySet() And Quote.IsSyncing = true  Order By Quote.OpportunityId ])
            {
                for(OpportunityLineItem item : mapaOppItem.get(quoteItem.Quote.OpportunityId).OpportunityLineItems)
                {
                    if(quoteItem.PricebookEntry.Product2Id == item.PricebookEntry.Product2Id && quoteItem.Quantity == item.Quantity && quoteItem.UnitPrice == item.UnitPrice)
                    {
                        for(Related_Aircraft__c Ra : quoteItem.Related_Aircraft__r)
                        {
                            ra.Opportunity__c = item.OpportunityId;
                            ra.Opportunity_product_id__c = item.id;
                            listaRa.add(ra);
                        }
                        break;
                        
                    }
                
                }
                
            }
            if(!listaRaDel.isEmpty())
                delete listaRaDel;
                
            if(!listaRa.isEmpty())
                update listaRa;
        }
    }
    /*
    private void sincronizeCustomFields()
    {
        Map<Id,Opportunity> mapaOppItem;
        List<OpportunityLineItem> listaItem = new List<OpportunityLineItem>();
        Set<Id> idOpp = new Set<Id>();
        
        
        for(OpportunityLineItem item : this.newRecords)
        {
            if(item.Unit__c == Null || item.Princing__c == Null)
                idOpp.add(item.OpportunityId);
                    
        }
        if(!idOpp.isEmpty())
        {
            mapaOppItem = new Map<Id,Opportunity>([Select Id, (Select Maximum_Discount__c,MaxDiscountAging__c,MaxDiscountFleet__c,Unit__c,Princing__c,PricebookEntry.Product2Id,Quantity,UnitPrice  From OpportunityLineItems)  From Opportunity Where Id in : idOpp]); 
            listaItem = new List<OpportunityLineItem>();
            for(QuoteLineItem quoteItem :  [Select Maximum_Discount__c,MaximumDiscountAge__c,MaximumDiscountFleet__c,Unit__c,Princing__c,Quote.OpportunityId,PricebookEntry.Product2Id,Quantity,UnitPrice From QuoteLineItem where Quote.OpportunityId in : mapaOppItem.keySet() And Quote.IsSyncing = true  Order By Quote.OpportunityId ])
            {
                for(OpportunityLineItem item : mapaOppItem.get(quoteItem.Quote.OpportunityId).OpportunityLineItems)
                {
                    System.Debug(item.PricebookEntry.Product2Id);
                    if(quoteItem.PricebookEntry.Product2Id == item.PricebookEntry.Product2Id && quoteItem.Quantity == item.Quantity && quoteItem.UnitPrice == item.UnitPrice)
                    {
                        item.Unit__c = quoteItem.Unit__c;
                        item.Princing__c = quoteItem.Princing__c;
                        item.Maximum_Discount__c = quoteItem.Maximum_Discount__c;
                        item.MaxDiscountAging__c = quoteItem.MaximumDiscountAge__c;
                        item.MaxDiscountAging__c = quoteItem.MaximumDiscountFleet__c;
                        
                        listaItem.add(item);
                        break;
                    }
                
                }
                
            }
            update listaItem;
        
        }
        
    }
    */
}