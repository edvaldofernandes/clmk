/* Classe implementadora de SOBjectDAO para operações DML no objeto Attachment__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 16/01/2016
*/
public without sharing class AttachmentCustomDao {
    
    private static final AttachmentCustomDao instance = new AttachmentCustomDao();    
    
    private AttachmentCustomDao(){
    }    
    
    public static AttachmentCustomDao getInstance(){
        return instance;
    }

    public Attachments__c getByProductIdAndRecordTypeId(String idProduct, String idRecordType){        
        List<Attachments__c> listAttach = [SELECT Type__c, Status__c, RecordTypeId, Product__c, OwnerId, Name, 
                                           Video_Link__c, 
                                           Id, 
                                           Device_Type__c, Description__c, Billboard__c, Aircraft_Model__c, 
                                           Active__c, Account__c, (SELECT Id, Name From Attachments__c.Attachments) 
                                           FROM Attachments__c 
                                           WHERE RecordTypeId = :idRecordType
                                           AND Product__c = :idProduct
                                           AND Status__c = 'Published' 
                                           AND Active__c = true
                                          	LIMIT 1];
        
        if(listAttach.size() > 0){
            return listAttach[0];
        }
        
      	return null;   
    }
    
    public list<Attachments__c> listByProductIdAndRecordTypeId(String idProduct, String idRecordType){
        List<Attachments__c> listAttach = [SELECT Type__c, Status__c, RecordTypeId, Product__c, OwnerId, Name, 
                                           Video_Link__c, 
                                           Id, 
                                           Device_Type__c, Description__c, Billboard__c, Aircraft_Model__c, 
                                           Active__c, Account__c,Pardot_Status__c,Custom_Redirect_Pardot__c,(SELECT Id, Name From Attachments__c.Attachments) 
                                           FROM Attachments__c 
                                           WHERE RecordTypeId = :idRecordType
                                           AND Product__c = :idProduct
                                           AND Status__c = 'Published' 
                                           AND Active__c = true];
        
        return listAttach;
    }
    
    public list<Attachments__c> listAttachmentAircraftModel() {
        return [SELECT Aircraft_Model__c, (SELECT Id From Attachments__c.Attachments)
                                           FROM Attachments__c 
                                           WHERE Active__c = true 
                                           AND RecordType.Name = 'Embraer Models'];
	}
    
    public list<Attachments__c> listBySetProductIdAndRecordTypeId(Set<String> setIdProduct, String idRecordType) {
		string setToString = '';
        for(String includeValue : setIdProduct){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        
        system.debug('GUI>>> ' + setToString);
        string strSOQL = '';
        if(String.isBlank(setToString)){
            strSOQL = ' Select ' + utils.getAllFields('Attachments__c') + 
                ' ,(SELECT Id, Name From Attachments__c.Attachments) FROM Attachments__c'+
                ' WHERE RecordTypeId =\'' +String.escapeSingleQuotes(idRecordType)+ '\'' +
                ' AND Status__c = \'Published\''+
                ' AND Active__c = true';
        }else{
            strSOQL = ' Select ' + utils.getAllFields('Attachments__c') + 
                ' ,(SELECT Id, Name From Attachments__c.Attachments) FROM Attachments__c'+
                ' WHERE RecordTypeId =\'' +String.escapeSingleQuotes(idRecordType)+ '\'' +
                ' AND Product__c IN ('+setToString+')'+
                ' AND Status__c = \'Published\''+
                ' AND Active__c = true';
        }
        return database.query(strSOQL);
    }
    
    public list<Attachments__c> listBySetAccountId(Set<String> setIdAccount) {        
        return [SELECT Type__c, Status__c, RecordTypeId, Product__c, OwnerId, Name, 
                Video_Link__c, 
                Id, Device_Type__c, Description__c, Billboard__c, Aircraft_Model__c, 
                Active__c, Account__c, Account__r.Maintance_Capability__c, Account__r.Name, Account__r.Phone, Account__r.Website, Account__r.EOSC__c,
                Account__r.National_Airworthiness_authority__r.Name, Account__r.BillingCountry,  (SELECT Id, Name From Attachments__c.Attachments) 
                FROM Attachments__c 
                WHERE Account__c IN : setIdAccount
               	AND Active__c = true];
    }
    
    public list<Attachments__c> listByRecordTypeId(String paramRecordTypeId) {        
        return [SELECT Type__c, Status__c, RecordTypeId, Product__c, OwnerId, Name, 
                Video_Link__c, 
                Id, Device_Type__c, Description__c, Billboard__c, Aircraft_Model__c, 
                Active__c, Account__c, Account__r.Maintance_Capability__c, Account__r.Name, Account__r.Phone, Account__r.Website, Account__r.EOSC__c,
                Account__r.National_Airworthiness_authority__r.Name, Account__r.BillingCountry,  (SELECT Id, Name From Attachments__c.Attachments) 
                FROM Attachments__c 
                WHERE RecordTypeId = :paramRecordTypeId
                AND Active__c = true];
    }
    
    public list<Attachments__c> listBySetBillboardIdAndRecordTypeId(Set<String> setIdBillboard, String idRecordType){                                                         
        return [SELECT Type__c, Status__c, Product__c, OwnerId, Name, 
                                           Video_Link__c, 
                Id, Device_Type__c, Description__c, Billboard__c, Aircraft_Model__c, 
                                           Active__c, Account__c, (SELECT Id, Name From Attachments__c.Attachments) 
                                           FROM Attachments__c 
                                           WHERE RecordTypeId = :idRecordType
                                           AND Billboard__c IN : setIdBillboard
                                           ];
    }
    
    public list<Attachments__c> listBySetIdAndRecordType(Set<String> setIdAttachmentC, String idRecordType){
        return [SELECT Type__c, Status__c, RecordTypeId, Product__c, OwnerId, Name, 
                                           Video_Link__c, 
                Id, Device_Type__c, Description__c, Billboard__c, Aircraft_Model__c, 
                                           Active__c, Account__c, (SELECT Id, Name From Attachments__c.Attachments) 
                                           FROM Attachments__c 
                                           WHERE RecordTypeId = :idRecordType
                                           AND id IN : setIdAttachmentC];
    }
}