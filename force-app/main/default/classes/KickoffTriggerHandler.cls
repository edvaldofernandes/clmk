public class KickoffTriggerHandler {
    public Map<Id,Kickoff__c> newRecordsMap = new Map<Id,Kickoff__c>();
    public Map<Id,Kickoff__c> oldRecordsMap = new Map<Id,Kickoff__c>(); 
    public List<Kickoff__c> newRecords = new List<Kickoff__c>();
    public List<Kickoff__c> oldRecords = new List<Kickoff__c>();
    
    public Map<Id,Kickoff__c> newRecordsMapWithAgreementFields;
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    public static boolean isRecursive = false;

    public KickoffTriggerHandler(boolean isExecuting) {
        this.isExecuting = isExecuting;
    }

    public void OnBeforeInsert()
    {
    }

    public void OnBeforeUpdate()
    {
    }

    public void OnBeforeDelete()
    {
        for(Kickoff__c KO: oldRecords) {
            if(KO.submitted__c == true) {
                KO.addError('You may not delete a submitted Kickoff Checklist.');
            }
        }
    }

    public void OnAfterInsert()
    {
    }

    public void OnAfterUpdate()
    {
    }

    public void OnAfterDelete()
    {
    }

    public void OnUndelete()
    {
    }
}