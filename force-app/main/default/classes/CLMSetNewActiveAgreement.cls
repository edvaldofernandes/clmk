/*
    * @description Synchronizes information from the new active agreement to all older requests that are not signed yet. 
	* informatiom from the objects: Commercial Agreement, Agreement Aircraft, AgreementTeam, Escalation, AircraftPrice, ExecutiveSummary, DocSharing, SpecialCredit, ProductSupport and ContractPDP.
 */

public class CLMSetNewActiveAgreement {
	
    public static String setNewActiveAgreement(String agreementId, String lastActiveAgreementId){
    	String sobjectsQuery =  'SELECT ' + 'id, Parent_Agreement__c' +
            ' FROM Agreement__c' +
            ' WHERE Parent_Agreement__c = :lastActiveAgreementId';
        List<Agreement__c> agreementList = database.query(sobjectsQuery);
        
        agreementList.add(new Agreement__c(Id=agreementId));
        system.debug('SetNetActiveAgreement > agreementList.size()' + agreementList.size());
        
        List<SObject> sobjectListToUpdate = new List<SObject>();
        List<SObject> sobjectListToInsert = new List<SObject>();
        
        //Agreement and Aircraft
        sobjectListToInsert.addAll(updateRelatedAircraft(agreementList, agreementId, sobjectListToUpdate));
        //AgreementTeam
        sobjectListToInsert.addAll(searchForNewAgreementsTeam(agreementList, agreementId, sobjectListToUpdate));
        //Escalation
        sobjectListToInsert.addAll(searchForNewEscalation(agreementList, agreementId));
        //AircraftPrice
        sobjectListToInsert.addAll(searchForNewAircraftPrice(agreementList, agreementId));
        //ExecutiveSummary
        sobjectListToInsert.addAll(searchForNewExecutiveSummaries(agreementList, agreementId));
        //DocSharing
        sobjectListToInsert.addAll(searchForNewDocSharing(agreementList, agreementId));
        //SpecialCredit
        sobjectListToInsert.addAll(searchForNewSpecialCredit(agreementList, agreementId));
        //ProductSupport
        sobjectListToInsert.addAll(searchForNewProductSupport(agreementList, agreementId));
        //ContractPDP
        sobjectListToInsert.addAll(searchForNewPaymentSetup(agreementList, agreementId));

        database.update(sobjectListToUpdate);
        database.insert(sobjectListToInsert);
        
		return String.join(sobjectListToInsert,',');
    }
    
    private static List<SObject> updateRelatedAircraft(List<Agreement__c> agreementList, String agreementId, List<SObject> sobjectListToUpdate){
    	String sobjectsQuery =  'SELECT ' + Utils.getAllFields('Agreement_Aircraft__c')  +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Agreement__c in :agreementList';
        List<Agreement_Aircraft__c> AgreementAircraftList = database.query(sobjectsQuery);
        
        Agreement_Aircraft__c newAC;
        List<SObject> listACToBeAdded = new List<SObject>();
        map<Id,Id> aircraftMap = new map<Id,Id>();
        
        for (Agreement_Aircraft__c ac : AgreementAircraftList) 
            if(ac.Agreement__c == agreementId && ac.Related_Agreement_Aircraft__c != null) 
            	aircraftMap.put(ac.Related_Agreement_Aircraft__c, ac.Id);

        for (Agreement__c agr : agreementList) 
            if(agr.Parent_Agreement__c != null && agr.Id != agreementId){            	
            	agr.Parent_Agreement__c = agreementId;
                sobjectListToUpdate.add((SObject)agr);
            }

        for (Agreement_Aircraft__c ac : AgreementAircraftList){
            if(ac.Related_Agreement_Aircraft__c != null && ac.Agreement__c != agreementId){
            	ac.Related_Agreement_Aircraft__c = aircraftMap.get(ac.Related_Agreement_Aircraft__c);
                sobjectListToUpdate.add((SObject)ac);
            }
 
            if(ac.Related_Agreement_Aircraft__c == null && ac.Agreement__c == agreementId){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        newAC = ac.clone(false, false, false, false);            
                        newAC.Agreement__c = String.ValueOf(agr.Id);
                        newAC.Escalation_Code__c = null;
                        listACToBeAdded.add((SObject)newAC);
                    }
                }
            } 
        }
        return listACToBeAdded;
    }
    
    private static List<SObject> searchForNewExecutiveSummaries(List<Agreement__c> agreementList, String agreementId){
        String sobjectsQuery =  'SELECT ' + Utils.getAllFields('Account_Cockpit__c')  +
            ' FROM Account_Cockpit__c' +
            ' WHERE Commercial_Agreement__c = :agreementId';
        List<Account_Cockpit__c> ExecutiveSummaryList = database.query(sobjectsQuery);

        List<SObject> listExecutiveSummary = new List<SObject>();
        Account_Cockpit__c executiveSummaryCloned;

        for (Account_Cockpit__c es : ExecutiveSummaryList){
            if(es.Commercial_Agreement__c == agreementId && es.Has_Parent__c != true){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        executiveSummaryCloned = es.clone(false, false, false, false);            
                        executiveSummaryCloned.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listExecutiveSummary.add((SObject)executiveSummaryCloned);
                    }
                }
            } 
        }
        return listExecutiveSummary;
    }

    private static List<SObject> searchForNewDocSharing(List<Agreement__c> agreementList, String agreementId){
        String sobjectsQuery =  'SELECT ' + Utils.getAllFields('Document_Sharing__c') + 
            ' FROM Document_Sharing__c' +
            ' WHERE Commercial_Agreement__c = :agreementId';
        List<Document_Sharing__c> DocumentSharingList = database.query(sobjectsQuery);

        List<SObject> listClonedDocSharing = new List<SObject>();
        Document_Sharing__c docSharingCloned;

        for (Document_Sharing__c ds : DocumentSharingList){
            if(ds.Commercial_Agreement__c == agreementId && ds.Has_Parent__c != true){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        docSharingCloned = ds.clone(false, false, false, false);            
                        docSharingCloned.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listClonedDocSharing.add((SObject)docSharingCloned);
                    }
                }
            }
        }
        return listClonedDocSharing;
    }

    private static List<SObject> searchForNewSpecialCredit(List<Agreement__c> agreementList, String agreementId){
        String sobjectsQuery =  'SELECT ' + Utils.getAllFields('Special_Credit__c') + 
            ' FROM Special_Credit__c' +
            ' WHERE Commercial_Agreement__c = :agreementId';
        List<Special_Credit__c> SpecialCreditList = database.query(sobjectsQuery);

        List<SObject> listClonedSpecialCredit = new List<Special_Credit__c>();
        Special_Credit__c specialCreditCloned;

        for (Special_Credit__c es : SpecialCreditList){
            if(es.Commercial_Agreement__c == agreementId && es.Has_Parent__c != true){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        specialCreditCloned = es.clone(false, false, false, false);            
                        specialCreditCloned.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listClonedSpecialCredit.add((SObject)specialCreditCloned);
                    }
                }
            } 
        }
        return listClonedSpecialCredit;
    }

    private static List<SObject> searchForNewPaymentSetup(List<Agreement__c> agreementList, String agreementId){
        String sobjectsQuery =  'SELECT ' + Utils.getAllFields('Contract_PDP__c') + 
            ' FROM Contract_PDP__c' +
            ' WHERE Commercial_Agreement__c = :agreementId';
        List<Contract_PDP__c> paymentSetupList = database.query(sobjectsQuery);

        List<SObject> listPaymentSetup = new List<Contract_PDP__c>();
        Contract_PDP__c PaymentSetupCloned;

        for (Contract_PDP__c es : paymentSetupList){
            if(es.Commercial_Agreement__c == agreementId && es.Has_Parent__c != true){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        PaymentSetupCloned = es.clone(false, false, false, false);            
                        PaymentSetupCloned.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listPaymentSetup.add((SObject)PaymentSetupCloned);
                    }
                }
            } 
        }
        return listPaymentSetup;
    }
    
    private static List<SObject> searchForNewProductSupport(List<Agreement__c> agreementList, String agreementId){
        String sobjectsQuery =  'SELECT ' + Utils.getAllFields('Product_Support__c')  +
            ' FROM Product_Support__c' +
            ' WHERE Commercial_Agreement__c = :agreementId';
        List<Product_Support__c> ProductSupportList = database.query(sobjectsQuery);

        List<SObject> listProductSupport = new List<Product_Support__c>();
        Product_Support__c ProductSupportCloned;

        for (Product_Support__c es : ProductSupportList){
            if(es.Commercial_Agreement__c == agreementId && es.Has_Parent__c != true){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        ProductSupportCloned = es.clone(false, false, false, false);            
                        ProductSupportCloned.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listProductSupport.add((SObject)ProductSupportCloned);
                    }
                }
            } 
        }
        return listProductSupport;
    }

    private static List<SObject> searchForNewAircraftPrice(List<Agreement__c> agreementList, String agreementId){
        String priceQuery =  'SELECT ' + Utils.getAllFields('Aircraft_Price__c') + 
            ' FROM Aircraft_Price__c' +
            ' WHERE Commercial_Agreement__c IN :agreementList';
        List<Aircraft_Price__c> AircraftPriceList = database.query(priceQuery);
        
        map<Id,Id> AircraftPriceMap = new map<Id,Id>();
        
        for (Aircraft_Price__c acp : AircraftPriceList) 
            if(acp.Commercial_Agreement__c == agreementId && acp.Related_Aircraft_Price__c != null) 
            	AircraftPriceMap.put(acp.Related_Aircraft_Price__c, acp.Id);

        List<SObject> listACPToBeAdded = new List<Aircraft_Price__c>();
        Aircraft_Price__c newACP;
        for (Aircraft_Price__c acp : AircraftPriceList){
            if(acp.Related_Aircraft_Price__c != null && acp.Commercial_Agreement__c != agreementId) 
            	acp.Related_Aircraft_Price__c = AircraftPriceMap.get(acp.Related_Aircraft_Price__c);

            if(acp.Commercial_Agreement__c == agreementId && acp.Related_Aircraft_Price__c == null){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        newACP = acp.clone(false, false, false, false);            
                        newACP.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listACPToBeAdded.add((SObject)newACP);
                    }
                }
            } 
        }
        return listACPToBeAdded;
    } 

    private static List<SObject> searchForNewEscalation(List<Agreement__c> agreementList, String agreementId){
        String priceQuery =  'SELECT ' + Utils.getAllFields('Escalation__c') + 
            ' FROM Escalation__c' +
            ' WHERE Commercial_Agreement__c IN :agreementList';
        List<Escalation__c> escalationList = database.query(priceQuery);
        
        map<Id,Id> escalationMap = new map<Id,Id>();
        
        for (Escalation__c esc : escalationList) 
            if(esc.Commercial_Agreement__c == agreementId && esc.Related_Escalation__c != null) 
            	escalationMap.put(esc.Related_Escalation__c, esc.Id);

        List<SObject> listEscToBeAdded = new List<Escalation__c>();
        Escalation__c newEsc;
        for (Escalation__c esc : escalationList){
            if(esc.Related_Escalation__c != null && esc.Commercial_Agreement__c != agreementId) 
            	esc.Related_Escalation__c = escalationMap.get(esc.Related_Escalation__c);

            if(esc.Commercial_Agreement__c == agreementId && esc.Related_Escalation__c == null){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        newEsc = esc.clone(false, false, false, false);            
                        newEsc.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listEscToBeAdded.add((SObject)newEsc);
                    }
                }
            } 
        }
        return listEscToBeAdded;
    } 

    private static List<SObject> searchForNewAgreementsTeam(List<Agreement__c> agreementList, String agreementId, List<SObject> sobjectListToUpdate){
        String priceQuery =  'SELECT ' + Utils.getAllFields('Agreements_Team__c') + 
            ' FROM Agreements_Team__c' +
            ' WHERE Commercial_Agreement__c IN :agreementList';
        List<Agreements_Team__c> agreementsTeamList = database.query(priceQuery);
        
        map<Id,Id> agreementsTeamMap = new map<Id,Id>();
        List<SObject> listATToBeAdded = new List<SObject>();
        Agreements_Team__c newAT;
        
        for (Agreements_Team__c at : agreementsTeamList) 
            if(at.Commercial_Agreement__c == agreementId && at.Related_Agreement_Team__c != null) 
            	agreementsTeamMap.put(at.Related_Agreement_Team__c, at.Id);

        for (Agreements_Team__c at : agreementsTeamList){
            if(at.Related_Agreement_Team__c != null && at.Commercial_Agreement__c != agreementId){
            	at.Related_Agreement_Team__c = agreementsTeamMap.get(at.Related_Agreement_Team__c);
                sobjectListToUpdate.add(at);
            }

            if(at.Commercial_Agreement__c == agreementId && at.Related_Agreement_Team__c == null){
                for (Agreement__c agr : agreementList){
                    if (String.ValueOf(agr.Id) != agreementId){
                        newAT = at.clone(false, false, false, false);            
                        newAT.Commercial_Agreement__c = String.ValueOf(agr.Id);
                        listATToBeAdded.add((SObject)newAT);
                    }
                }
            } 
        }
        return listATToBeAdded;
    } 
    
}