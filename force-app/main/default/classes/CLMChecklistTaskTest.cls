@isTest
public class CLMChecklistTaskTest {

    static testMethod void createTaskTestAll(){
		RecordType recordType = [Select SobjectType, Name, DeveloperName From RecordType Where SobjectType = 'Questionnaire__c' And DeveloperName = 'Contract_Administration' limit 1];
        RecordType recordType2 = [Select Name, Id From RecordType Where SobjectType = 'Task_Setup__c' And DeveloperName = 'Agreement' limit 1];

        Account account = SObjectInstanceTest.createAccount(recordType2.Id);

        Agreement__c agreementTest = new Agreement__c(
            Name = 'Test',
            Account__c = account.Id,
            Country__c = 'Brazil',
            Status__c = 'Request'
        );
        insert agreementTest;
        
		Task_Setup__c  taskSetup2 = new Task_Setup__c();    
        taskSetup2.Name = 'Task Setup Test02'; 
        taskSetup2.Leap_Time__c = 1; 
        taskSetup2.Selected_Date__c = 'AFA'; 
        taskSetup2.Answer__c = 'Answer test';
        taskSetup2.Question__c = 'All'; 
        taskSetup2.Question_API__c = 'All';
        taskSetup2.Before_Selected_date__c = true;
        taskSetup2.Description__c = 'Test Description';
        taskSetup2.Sequence__c = 999;
        //taskSetup2.Date_API__c = 'Agreement__c.Contract_Signature_Date__c';
        taskSetup2.RecordTypeId = recordType2.Id;
        insert taskSetup2;
        
        //create checklist AGREEMENT (questionaire)
        Questionnaire__c questionnaire = new Questionnaire__c();
        questionnaire.Factory_Tour__c = true;
        questionnaire.Acceptance_Flight_Card__c = 'Valor 1 TESTE';
        questionnaire.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire.Commercial_Agreement__c = agreementTest.Id;
        questionnaire.Foreign_Registration__c ='';
        questionnaire.RecordTypeId = recordType.Id;
        insert questionnaire;
        
        Id checkList = [SELECT Id From Questionnaire__c WHERE Acceptance_Flight_Card__c = 'Valor 1 TESTE' LIMIT 1].Id;
        
        CLMCheckListTask.createTask(checkList);
        
	    taskSetup2.Selected_Date__c = 'Contract Signature Date'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
	    taskSetup2.Selected_Date__c = 'Delivery Month'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
        taskSetup2.Selected_Date__c = 'Delivery Date'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
        
        

    }
    
    static testMethod void createTaskTestContractMilestones(){
		RecordType recordType = [Select SobjectType, Name, DeveloperName From RecordType Where SobjectType = 'Questionnaire__c' And DeveloperName = 'Contract_Administration' limit 1];
        RecordType recordType2 = [Select Name, Id From RecordType Where SobjectType = 'Task_Setup__c' And DeveloperName = 'Agreement' limit 1];
        RecordType recordType3 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];

        Account account = SObjectInstanceTest.createAccount(recordType.Id);

        Agreement__c agreementTest = new Agreement__c(
            Name = 'Test',
            Account__c = account.Id,
            Country__c = 'Brazil',
            Status__c = 'Request'
        );
        insert agreementTest;
        
		 //create product        
        Product2 Produto = new Product2();
        Produto.Name = 'Produto Teste';
        Produto.ProductCode = 'a488';
        Produto.Product_Status__c = 'Active';
        Produto.RecordTypeId = recordType3.Id;
        Produto.IsActive = true;
        database.insert(Produto);

        
		//create checklist AGREEMENT (questionaire)
        Questionnaire__c questionnaire = new Questionnaire__c();
        questionnaire.Factory_Tour__c = true;
        questionnaire.Acceptance_Flight_Card__c = 'Valor 2 TESTE';
        questionnaire.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire.Commercial_Agreement__c = agreementTest.Id;
        questionnaire.Foreign_Registration__c ='';
        questionnaire.RecordTypeId = recordType.Id;
        
        insert questionnaire;
        
		Task_Setup__c  taskSetup2 = new Task_Setup__c();    
        taskSetup2.Name = 'Task Setup Test02'; 
        taskSetup2.Leap_Time__c = 1; 
        taskSetup2.Selected_Date__c = 'Signature Date'; 
        taskSetup2.Answer__c = 'Test';
        taskSetup2.Question__c = 'Embraer pilots will do the ferry flight?'; 
        taskSetup2.Question_API__c = 'Embraer_pilots_will_do_the_ferry_flight__c';
        taskSetup2.Before_Selected_date__c = false;
        taskSetup2.Description__c = '';
        taskSetup2.Sequence__c = 999;
        //taskSetup2.Date_API__c = 'Agreement__c.Contract_Signature_Date__c';
        taskSetup2.RecordTypeId = recordType2.Id;
        insert taskSetup2;
        
        Id checkList = [SELECT Id From Questionnaire__c WHERE Acceptance_Flight_Card__c = 'Valor 2 TESTE' LIMIT 1].Id;
        
        CLMCheckListTask.createTask(checkList);

    }
    
        static testMethod void createTaskTestSupport(){
		RecordType recordType = [Select SobjectType, Name, DeveloperName From RecordType Where SobjectType = 'Questionnaire__c' And DeveloperName = 'Production_Support' limit 1];
        RecordType recordType1 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
		RecordType recordType2 = [Select Name, Id From RecordType Where SobjectType = 'Task_Setup__c' And DeveloperName = 'Production_Support' limit 1];


                
        Account account = SObjectInstanceTest.createAccount(recordType.Id);
        //create agreement record
        Agreement__c apttusAPTSAgreement1 = SObjectInstanceTest.createApttusAgreement(account.id);
        apttusAPTSAgreement1.Signature_Date__c = date.today();
        apttusAPTSAgreement1.Nickname__c = 'TST_FINAL';
        apttusAPTSAgreement1.Undisclosed_Nickname__c = 'UNDISCLOSED 01';
        apttusAPTSAgreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(new List<Agreement__c>{apttusAPTSAgreement1});
 
        
        
        //create product        
        Product2 Produto = new Product2();
        Produto.Name = 'Produto Teste';
        Produto.ProductCode = 'a488';
        Produto.Product_Status__c = 'Active';
        Produto.RecordTypeId = recordType1.Id;
        Produto.IsActive = true;
        database.insert(Produto);
        
        Aircraft_Price__c ap2 = new Aircraft_Price__c();
        ap2.Model__c = Produto.id;
        ap2.Aircraft_Version__c = 'IGW';
        ap2.Economic_Condition__c = 'JAN/2018'; 
        ap2.Commercial_Agreement__c = apttusAPTSAgreement1.Id;
        ap2.Aircraft_list_Price__c = 10000;
        ap2.Aircraft_Basic_Price__c = 60;
        Database.insert(ap2);
        
        
        //create agreement lineItem
        Agreement_Aircraft__c apttusAgreementLineItem = new Agreement_Aircraft__c(); 
        apttusAgreementLineItem.Aircraft__c  = Produto.Id;
        apttusAgreementLineItem.AFA__c = Date.today();
        apttusAgreementLineItem.Contractual_Delivery_Month__c = Date.today();
        apttusAgreementLineItem.Contract_Aircraft_Number__c = 5;
        apttusAgreementLineItem.Order_Type__c = 'Firm';
        apttusAgreementLineItem.Status__c = 'Planned';
        apttusAgreementLineItem.Basic_Price__c = 200;
        apttusAgreementLineItem.Aircraft_Configuration__c = 'AK';
        apttusAgreementLineItem.Agreement__c = apttusAPTSAgreement1.Id;
        apttusAgreementLineItem.Aircraft_Price__c = ap2.id;
        apttusAgreementLineItem.Agreement__r = apttusAPTSAgreement1;
        apttusAgreementLineItem.Agreement__r.Agreement_Code__c = apttusAPTSAgreement1.Id;
        apttusAgreementLineItem.Agreement__r.Status_Category__c = apttusAPTSAgreement1.Status_Category__c;
        apttusAgreementLineItem.Agreement__r.RecordTypeId = apttusAPTSAgreement1.RecordTypeId;
        apttusAgreementLineItem.Agreement__r.RecordType = apttusAPTSAgreement1.RecordType;
        
        insert apttusAgreementLineItem;
        
        Questionnaire__c questionnaire = new Questionnaire__c();
        questionnaire.Factory_Tour__c = true;
        questionnaire.Acceptance_Flight_Card__c = 'Valor 3 TESTE';
        questionnaire.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire.Contract_Aircraft__c = apttusAgreementLineItem.Id; 
        questionnaire.Foreign_Registration__c ='';
        questionnaire.RecordTypeId = recordType.Id;
        //questionnaire.Agreement_Line_Item__c = apttusAgreementLineItem.Id;
        insert questionnaire;
        
		Task_Setup__c  taskSetup = new Task_Setup__c(); 
        taskSetup.Name = 'Task Setup Test'; 
        taskSetup.Leap_Time__c = 6; 
        taskSetup.Selected_Date__c = 'Contractual Delivery Month'; 
        taskSetup.Question__c = 'Factory_Tour__c'; 
        taskSetup.Question_API__c = 'Factory_Tour__c';
        taskSetup.Answer__c = 'true';   
        taskSetup.Description__c = 'Test Description';
        taskSetup.Sequence__c = 999.99;
        taskSetup.RecordTypeId = recordType2.Id;
        //taskSetup.Date_API__c = 'Agreement_Aircraft__r.Contractual_Delivery_Month__c';
        insert taskSetup;
        
		Id checkList = [SELECT Id From Questionnaire__c WHERE Acceptance_Flight_Card__c = 'Valor 3 TESTE' LIMIT 1].Id;
        
        CLMCheckListTask.createTask(checkList);
        
                
		taskSetup.Selected_Date__c = 'Contract Signature Date'; 
        update taskSetup;
        CLMCheckListTask.createTask(checkList);
        
	    taskSetup.Selected_Date__c = 'Delivery Month'; 
        update taskSetup;
        CLMCheckListTask.createTask(checkList);
        
        taskSetup.Selected_Date__c = 'Delivery Date'; 
        update taskSetup;
        CLMCheckListTask.createTask(checkList);
    }
    
    static testMethod void createTaskTestAgreementLineItem(){
		RecordType recordType = [Select SobjectType, Name, DeveloperName From RecordType Where SobjectType = 'Questionnaire__c' And DeveloperName = 'Production_Support' limit 1];
        RecordType recordType1 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
		RecordType recordType2 = [Select Name, Id From RecordType Where SobjectType = 'Task_Setup__c' And DeveloperName = 'Agreement' limit 1];


                
        Account account = SObjectInstanceTest.createAccount(recordType.Id);
        //create agreement record
        Agreement__c apttusAPTSAgreement1 = SObjectInstanceTest.createApttusAgreement(account.id);
        apttusAPTSAgreement1.Signature_Date__c = date.today();
        apttusAPTSAgreement1.Nickname__c = 'TST_FINAL';
        apttusAPTSAgreement1.Undisclosed_Nickname__c = 'UNDISCLOSED 01';
        apttusAPTSAgreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(new List<Agreement__c>{apttusAPTSAgreement1});
 
        
        
        //create product        
        Product2 Produto = new Product2();
        Produto.Name = 'Produto Teste';
        Produto.ProductCode = 'a488';
        Produto.Product_Status__c = 'Active';
        Produto.RecordTypeId = recordType1.Id;
        Produto.IsActive = true;
        database.insert(Produto);
        
        Aircraft_Price__c ap2 = new Aircraft_Price__c();
        ap2.Model__c = Produto.id;
        ap2.Aircraft_Version__c = 'IGW';
        ap2.Economic_Condition__c = 'JAN/2018'; 
        ap2.Commercial_Agreement__c = apttusAPTSAgreement1.Id;
        ap2.Aircraft_list_Price__c = 10000;
        ap2.Aircraft_Basic_Price__c = 60;
        Database.insert(ap2);
        
        
        //create agreement lineItem
        Agreement_Aircraft__c apttusAgreementLineItem = new Agreement_Aircraft__c(); 
        apttusAgreementLineItem.Aircraft__c  = Produto.Id;
        apttusAgreementLineItem.AFA__c = Date.today();
        apttusAgreementLineItem.Contractual_Delivery_Month__c = Date.today();
        apttusAgreementLineItem.Contract_Aircraft_Number__c = 5;
        apttusAgreementLineItem.Order_Type__c = 'Firm';
        apttusAgreementLineItem.Status__c = 'Planned';
        apttusAgreementLineItem.Basic_Price__c = 200;
        apttusAgreementLineItem.Aircraft_Configuration__c = 'AK';
        apttusAgreementLineItem.Agreement__c = apttusAPTSAgreement1.Id;
        apttusAgreementLineItem.Aircraft_Price__c = ap2.id;
        apttusAgreementLineItem.Agreement__r = apttusAPTSAgreement1;
        apttusAgreementLineItem.Agreement__r.Agreement_Code__c = apttusAPTSAgreement1.Id;
        apttusAgreementLineItem.Agreement__r.Status_Category__c = apttusAPTSAgreement1.Status_Category__c;
        apttusAgreementLineItem.Agreement__r.RecordTypeId = apttusAPTSAgreement1.RecordTypeId;
        apttusAgreementLineItem.Agreement__r.RecordType = apttusAPTSAgreement1.RecordType;
        
        insert apttusAgreementLineItem;
        
        Questionnaire__c questionnaire = new Questionnaire__c();
        questionnaire.Factory_Tour__c = true;
        questionnaire.Acceptance_Flight_Card__c = 'Valor 3 TESTE';
        questionnaire.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire.Contract_Aircraft__c = apttusAgreementLineItem.Id; 
        questionnaire.Foreign_Registration__c ='';
        questionnaire.RecordTypeId = recordType.Id;
        //questionnaire.Agreement_Line_Item__c = apttusAgreementLineItem.Id;
        insert questionnaire;
        
		Task_Setup__c  taskSetup = new Task_Setup__c(); 
        taskSetup.Name = 'Task Setup Test'; 
        taskSetup.Leap_Time__c = 6; 
        taskSetup.Selected_Date__c = 'Contractual Delivery Month'; 
        taskSetup.Question__c = 'Factory_Tour__c'; 
        taskSetup.Question_API__c = 'Factory_Tour__c';
        taskSetup.Answer__c = 'true';   
        taskSetup.Description__c = 'Test Description';
        taskSetup.Sequence__c = 999.99;
        taskSetup.RecordTypeId = recordType2.Id;
        //taskSetup.Date_API__c = 'Agreement_Aircraft__c.Contractual_Delivery_Month__c';
        insert taskSetup;
        
		Id checkList = [SELECT Id From Questionnaire__c WHERE Acceptance_Flight_Card__c = 'Valor 3 TESTE' LIMIT 1].Id;
        
        CLMCheckListTask.createTask(checkList);
        
                
		taskSetup.Selected_Date__c = 'Contract Signature Date'; 
        update taskSetup;
        CLMCheckListTask.createTask(checkList);
        
	    taskSetup.Selected_Date__c = 'Delivery Month'; 
        update taskSetup;
        CLMCheckListTask.createTask(checkList);
        
        taskSetup.Selected_Date__c = 'Delivery Date'; 
        update taskSetup;
        CLMCheckListTask.createTask(checkList);
    }
    
    static testMethod void deleteTaskTest(){
        RecordType recordType = [Select SobjectType, Name, DeveloperName From RecordType Where SobjectType = 'Questionnaire__c' And DeveloperName = 'Contract_Administration' limit 1];
        
		Account account = SObjectInstanceTest.createAccount(recordType.Id);

        Agreement__c agreementTest = new Agreement__c(
            Name = 'Test',
            Account__c = account.Id,
            Country__c = 'Brazil',
            Status__c = 'Request'
        );
        insert agreementTest;
        
        
        //create checklist AGREEMENTLINEITEM (questionaire)
        questionnaire__c questionnaire1 = new questionnaire__c();
        questionnaire1.Factory_Tour__c = true;
        questionnaire1.Commercial_Agreement__c = agreementTest.Id;
        questionnaire1.Acceptance_Flight_Card__c = 'Valor 2 TESTE';
        questionnaire1.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire1.Foreign_Registration__c ='';
        questionnaire1.RecordTypeId = recordType.Id;
        insert questionnaire1;
        
        //add tasks created for the checklist above
        List<Task> lstTask = new List<Task>();
        for(Integer i = 0; i<3;i++){
            Task task = new task();
            task.WhatId = questionnaire1.Id;
            task.Subject = 'Other';
            task.Status = 'Open';
            task.Priority = 'Normal';
            task.Checklist__c= questionnaire1.Id;
            lstTask.add(task);
        }
        database.insert(lstTask);
        
        //get checkList ID
        Id checkList = [SELECT Id From Questionnaire__c WHERE RecordType.DeveloperName = 'Contract_Administration' LIMIT 1].Id;
        system.debug('CHECKLIST TEST>>> ' +checkList);
        List<Task> lstOpenTaskTest = new List<Task>();
        lstOpenTaskTest = [SELECT Id, Status, Checklist__c FROM Task where Status = 'Open' AND Checklist__c =:checkList];
        system.debug('List Open Task AQUI>>>>>>>> ' +lstOpenTaskTest);
        CLMCheckListTask.deleteTask(checkList);
        
    }

}