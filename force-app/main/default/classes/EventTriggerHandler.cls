/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Jul-2016.
**/
public without sharing class EventTriggerHandler
{

    
    public Map<Id,Event> newRecordsMap = new Map<Id,Event>();
    public Map<Id,Event> oldRecordsMap = new Map<Id,Event>(); 
    public List<Event> newRecords = new List<Event>();
    public List<Event> oldRecords = new List<Event>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public EventTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {



    }

 

    public void OnAfterInsert()
    {
        createPublicCalendarEvents();
    }

 

    public void OnBeforeUpdate()
    {
      
    }

 

    public void OnAfterUpdate()
    {
        createPublicCalendarEvents();
    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        deletePublicCalendarEvents();

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }

    
    
    private void createPublicCalendarEvents()
    {
    
        List<PublicCalendar__c> recordsToUpsert = new List<PublicCalendar__c>();
        PublicCalendar__c calendarTemp;
        string correctedId = '';
        Map<string, PublicCalendarSettings__c> calendars = PublicCalendarSettings__c.getAll();
        Map<String,String> mapCalendarKeyValue = new Map<String,String>();
        
        for (PublicCalendarSettings__c cld : calendars.values()) 
            mapCalendarKeyValue.put(cld.calendarId__c, cld.Name);
    
        for(Event ev : this.newRecords)
        {
            if(ev.PublicCalendar__c)
            {
               calendarTemp = new PublicCalendar__c();
               calendarTemp.Name = ev.Subject;
               calendarTemp.AllDayEvent__c = ev.IsAllDayEvent;
               calendarTemp.Description__c = ev.Description;
               calendarTemp.End__c = ev.EndDateTime;
               calendarTemp.EventType__c = ev.Event_Type__c;
               calendarTemp.Location__c = ev.Location;
               calendarTemp.OriginalEventId__c = ev.Id;
               calendarTemp.Start__c = ev.StartDateTime;
               correctedId = string.valueOf(ev.OwnerId);
               correctedId = correctedId.left(15);
               if(mapCalendarKeyValue.containsKey(correctedId))
               {
                   calendarTemp.RelatedTo__c = mapCalendarKeyValue.get(correctedId); 
               }
               else
               {
                   calendarTemp.RelatedTo__c = 'Not found in settings';
               }
               recordsToUpsert.add(calendarTemp);
            } 
        }
        
        if(!recordsToUpsert.isEmpty())
            upsert recordsToUpsert OriginalEventId__c;
    
    
    }
    
    private void deletePublicCalendarEvents()
    {
    
        Set<Id> recordsIdToDelete = new Set<Id>();
        List<PublicCalendar__c> recordsToDelete = new List<PublicCalendar__c>();
    
        for(Event ev : this.oldRecords)
        {
            if(ev.PublicCalendar__c)
                recordsIdToDelete.add(ev.Id);
        }
        if(!recordsIdToDelete.isEmpty())
        {
            recordsToDelete = [Select Id From PublicCalendar__c Where OriginalEventId__c in : recordsIdToDelete];
            
            if(!recordsToDelete.isEmpty())
                delete recordsToDelete;
        
        }   
    
    
    }


}