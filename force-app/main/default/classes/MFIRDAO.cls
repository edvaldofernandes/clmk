public class MFIRDAO {
    
    public MFIRDAO(){}
    
    public List<MFIR__c> getMFIRByNumberORSapName(Set<String> mfirNumberOrSApName){
        
        return [SELECT Id,
                       MFIR_number__c, 
                	   SAP_Name__c, 
                       Account__c 
                FROM MFIR__c
                WHERE (MFIR_Number__c IN :mfirNumberOrSApName OR SAP_Name__c = :mfirNumberOrSApName) 
               ];
    }

}