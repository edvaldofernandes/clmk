public class ProductionCapabilityDAO {
    public static List<Production_Capability__c> getCapabilityByAircraftId(Integer currentYear, Set<String> aircraftIds){
        string setToString = '';
        for(String includeValue : aircraftIds){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');

        List<Production_Capability__c> lstProduction = new List<Production_Capability__c>();
        lstProduction = database.query('SELECT ' + utils.getAllFields('Production_Capability__c') +  ', Aircraft_Model__r.Name,' +
                                       ' Aircraft_Model__r.DF_Grouping__c, Aircraft_Model__r.DF_Group__c FROM Production_Capability__c' +
                                       ' WHERE Aircraft_Model__c IN (' + setToString + ') AND CALENDAR_YEAR(Delivery_s_Date__c) =' + currentYear);
        //system.debug('Select.ProductionCapability: '+lstProduction);
        return lstProduction;
    }

    //getCapabilityByAircraftFamily DF Online
    public static List<Production_Capability__c> getCapabilityByAircraftFamily(Integer currentYear, Set<String> aircraftFamily){
        String setToString = '';
        //System.debug('AircraftFamily: ' + aircraftFamily);
        for(String includeValue : aircraftFamily){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');

        List<Production_Capability__c> lstProduction = new List<Production_Capability__c>();
        lstProduction = database.query('SELECT ' + utils.getAllFields('Production_Capability__c') +  ', Aircraft_Model__r.Name, Aircraft_Model__r.RecordType.Name,' +
                                       ' Aircraft_Model__r.DF_Grouping__c, Aircraft_Model__r.DF_Group__c  FROM Production_Capability__c' +
                                       ' WHERE Aircraft_Model__r.Aircraft_Family__c IN (' + setToString + ') AND CALENDAR_YEAR(Delivery_s_Date__c) =' + currentYear );
        return lstProduction;
    }

    //getCapabilityByAircraftFamily DF Online without year parameter
    public static List<Production_Capability__c> getCapabilityByAircraftFamily(){

        List<Production_Capability__c> lstProduction = new List<Production_Capability__c>();
        lstProduction = database.query('SELECT ' + utils.getAllFields('Production_Capability__c') +  ', Aircraft_Model__r.Name, Aircraft_Model__r.RecordType.Name,' +
                                       ' Aircraft_Model__r.DF_Grouping__c, Aircraft_Model__r.DF_Group__c  FROM Production_Capability__c'
                                       );
        System.debug('ProductionCapabilityDAO getCapabilityByAircraftFamily lstProduction'  + lstProduction);
        return lstProduction;
    }

    //getProductionByYear Online
    public static List<AggregateResult> getProductionByYear(Integer year, Set<String> dfGroup){
        //System.debug('dfGroup: ' + dfGroup);
        List<AggregateResult> lstAggregateResult = new List<AggregateResult>();
        lstAggregateResult = [SELECT
                                  Aircraft_Model__c,
                                  Aircraft_Model__r.name Name,
                              	  Aircraft_Model__r.DF_Grouping__c Grouping,
                              	  Aircraft_Model__r.DF_Group__c Groupe,
                                  SUM(Production_Capability__c) Total,
                                  CALENDAR_MONTH(Delivery_s_Date__c) Month,
                                  CALENDAR_YEAR(Delivery_s_Date__c) Year
                              FROM
                              	Production_Capability__c
                              WHERE
                              	CALENDAR_YEAR(Delivery_s_Date__c) = :year AND Aircraft_Model__r.Aircraft_Family__c IN :dfGroup
                              GROUP BY
                                  Aircraft_Model__c,
                                  Aircraft_Model__r.name,
                              	  Aircraft_Model__r.DF_Grouping__c,
                              	  Aircraft_Model__r.DF_Group__c,
                                  CALENDAR_MONTH(Delivery_s_Date__c),
                                  CALENDAR_YEAR(Delivery_s_Date__c)
            				  ORDER BY
            					  Aircraft_Model__r.name];
    	return lstAggregateResult;
    }

    //getProductionByYear Published
    public static List<AggregateResult> getProductionByYearPub(Integer year, Set<String> dfGroup, String idDfVersion){
        System.debug('dfGroup: ' + dfGroup);
        String dfVersionId = idDfVersion;
        system.debug('DAO DFVERSION ID>>> ' +dfVersionId);
        List<AggregateResult> lstAggregateResult = new List<AggregateResult>();
        lstAggregateResult = [SELECT
                                  Aircraft_Model__c,
                                  Aircraft_Model__r.name Name,
                              	  Aircraft_Model__r.DF_Grouping__c Grouping,
                              	  Aircraft_Model__r.DF_Group__c Groupe,
                                  SUM(Production_Capability__c) Total,
                                  CALENDAR_MONTH(Delivery_s_Date__c) Month,
                                  CALENDAR_YEAR(Delivery_s_Date__c) Year
                              FROM
                              	DF_Production_Capability__c
                              WHERE
                              	CALENDAR_YEAR(Delivery_s_Date__c) = :year AND Aircraft_Model__r.Aircraft_Family__c IN :dfGroup AND DF_Version__c = :dfVersionId
                              GROUP BY
                                  Aircraft_Model__c,
                                  Aircraft_Model__r.name,
                              	  Aircraft_Model__r.DF_Grouping__c,
                              	  Aircraft_Model__r.DF_Group__c,
                                  CALENDAR_MONTH(Delivery_s_Date__c),
                                  CALENDAR_YEAR(Delivery_s_Date__c)
            				  ORDER BY
            					  Aircraft_Model__r.name];
    	return lstAggregateResult;
    }


}