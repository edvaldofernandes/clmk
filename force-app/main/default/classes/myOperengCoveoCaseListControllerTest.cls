@isTest public class myOperengCoveoCaseListControllerTest {
    /**
    * @description Test if ....
    **/
    @isTest static void testCanGetCurrentUserAccountId(){
        Id accountIdTested;
        Account acc = new Account(
            Name = 'asdf', 
            Company_Nickname__c = 'test'
        );
        insert acc;
        Contact c = new Contact(
            LastName = 'asdf',
            accountId = acc.Id
        );
        insert c; 
        Profile p = [SELECT Id FROM Profile WHERE Name='Services Catalog']; 
        User u = new User(
            contactId = c.Id,
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
        insert u;
        
		Id accountIdExpected = acc.Id; 
        system.runAs(u)
        {
            Test.startTest();
                accountIdTested = myOperengCoveoCaseListController.getCurrentUserAccountId();
            Test.stopTest();
        }
		System.assertEquals(accountIdExpected, accountIdTested, 'Cant get the right account Id');        
    }
    
    /**
    * @description Test if ....
    **/
    @isTest static void testCanBuildCoveoToken(){
        String expectedFilter;
        Map<String, Object> searchToken;
        Account acc = new Account(
            Name = 'asdf', 
            Company_Nickname__c = 'test'
        );
        insert acc;
        Contact c = new Contact(
            LastName = 'asdf',
            accountId = acc.Id
        );
        insert c; 
        Profile p = [SELECT Id FROM Profile WHERE Name='Services Catalog']; 
        User u = new User(
            contactId = c.Id,
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
        insert u;
        
        
        expectedFilter = String.format(
            myOperengCoveoCaseListController.COVEO_FILTER_TEMPLATE,
            new List<String>{acc.Id}
		);
        System.debug('expectedFilter = ' + expectedFilter);
        system.runAs(u)
        {
            Test.startTest();
                searchToken = myOperengCoveoCaseListController.buildSearchToken();
            Test.stopTest();
        }
		System.assertEquals(expectedFilter, searchToken.get('filter'));   
    }
}