@IsTest 
public with sharing class CLMEndAgreementTest {
    
        
    static testMethod void controllerAndEndAgreementTestExpire(){
        product2 aircraft = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
        insert aircraft;
       
        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(Test.getStandardPricebookId(), aircraft.Id);
        insert pbe;  
        
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        insert acc;
            
        Agreement__c agreement = new Agreement__c();
        agreement.Name = 'Test 1';
        agreement.Account__c = acc.Id;
        agreement.Country__c = 'Iceland';
        agreement.Agreement_Code__c = 'ABCDEFG';
        agreement.Region_Code__c = 'RegionCODE';
        
        agreement.Status__c = 'Request';
        agreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        agreement.Company_Signed_Date__c = Date.today(); 
        agreement.Kickoff_trigger_already_executed__c = false; 
        agreement.Admin_Mode__c = 'true';
        agreement.Agreement_Color__c = '0000FF';
        insert agreement;
       	
       
       Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(agreement.Id, aircraft.id);
       insert aircraftPrice1;

        Agreement_Aircraft__c contractAircraft = new Agreement_Aircraft__c();
        contractAircraft.Aircraft__c  = aircraft.Id;
        contractAircraft.Delivery_Date__c = Date.today();
        contractAircraft.Contract_Aircraft_Number__c = 5;
        contractAircraft.Order_Type__c = 'Option';
        contractAircraft.Basic_Price__c = 200;
        contractAircraft.Aircraft_Configuration__c = 'AK';
        contractAircraft.Agreement__c = agreement.Id;
        contractAircraft.Actual_Delivery_Date__c = Date.today();
        contractAircraft.Skyline_Summary_Calculated__c = 'Firms E1';
        contractAircraft.MFA_SCORE__c = 100;
        contractAircraft.Contractual_Delivery_Month__c = Date.today();
        contractAircraft.Status__c = 'Planned';
       contractAircraft.Aircraft_Price__c = aircraftPrice1.Id;
        insert contractAircraft;

        Aircraft_PDP__c aircraftPDP = new Aircraft_PDP__c();
        aircraftPDP.Contract_Aircraft__c = contractAircraft.Id;
        aircraftPDP.Status_L__c = 'Pending';
        insert aircraftPDP;

        SOB_Event__c sobEvent = new SOB_Event__c();
        sobEvent.Commercial_Agreement__c = agreement.Id;
        sobEvent.Contract_Aircraft__c = contractAircraft.Id;
        sobEvent.SOB_Status__c = 'Firm Orders Included';
        insert sobEvent;

        Task task = new Task();
        task.Description = 'test';
        task.Subject = 'test';
        insert task;

        TaskRelation taskRelation = new TaskRelation();
        taskRelation.TaskId = task.Id;
        taskRelation.RelationId = agreement.Id;
        taskRelation.IsWhat = true;
        insert taskRelation;
       
        agreement.Status_Category__c ='PA Approved';
        update agreement;
        
        List<Agreement__c> listAgg = [Select Id, Status__c, Status_Category__c FROM Agreement__c];
        System.debug('is_agreement_set_to_expire.listAgg: ' + listAgg);

        List<Agreement_Aircraft__c> listAggAir = [Select Status__c FROM Agreement_Aircraft__c];
        System.debug('is_agreement_set_to_expire.listAggAir: ' + listAggAir);

        List<Aircraft_PDP__c> listAirPDP = [Select Contract_Aircraft__c, Status_L__c FROM Aircraft_PDP__c];
        System.debug('is_agreement_set_to_expire.listAirPDP: ' + listAirPDP);

        List<SOB_Event__c> listSOBEvt = [Select Commercial_Agreement__c, Contract_Aircraft__c, SOB_Status__c FROM SOB_Event__c];
        System.debug('is_agreement_set_to_expire.listSOBEvt: ' + listSOBEvt);

        Test.startTest();
   		ApexPages.currentPage().getParameters().put('id',listAgg[0].Id);
        //ApexPages.StandardController sc = new ApexPages.StandardController(listAgg[0]);
        //CLMEndAgreementController eac = new CLMRevertToRequestController(sc);
        CLMEndAgreementController.expire();

        Test.stopTest();
    }
    
    static testMethod void controllerAndEndAgreementTestTerminate(){
        product2 aircraft = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
        insert aircraft;
       
        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(Test.getStandardPricebookId(), aircraft.Id);
        insert pbe;  
        
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        insert acc;
            
        Agreement__c agreement = new Agreement__c();
        agreement.Name = 'Test 1';
        agreement.Account__c = acc.Id;
        agreement.Country__c = 'Iceland';
        agreement.Agreement_Code__c = 'ABCDEFG';
        agreement.Region_Code__c = 'RegionCODE';
        agreement.Status__c = 'Request';
        agreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        agreement.Company_Signed_Date__c = Date.today(); 
        agreement.Kickoff_trigger_already_executed__c = false; 
        agreement.Admin_Mode__c = 'true';
        agreement.Agreement_Color__c = '0000FF';
        insert agreement;
       	
       
       Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(agreement.Id, aircraft.id);
       insert aircraftPrice1;

        Agreement_Aircraft__c contractAircraft = new Agreement_Aircraft__c();
        contractAircraft.Aircraft__c  = aircraft.Id;
        contractAircraft.Delivery_Date__c = Date.today();
        contractAircraft.Contract_Aircraft_Number__c = 5;
        contractAircraft.Order_Type__c = 'Option';
        contractAircraft.Basic_Price__c = 200;
        contractAircraft.Aircraft_Configuration__c = 'AK';
        contractAircraft.Agreement__c = agreement.Id;
        contractAircraft.Actual_Delivery_Date__c = Date.today();
        contractAircraft.Skyline_Summary_Calculated__c = 'Firms E1';
        contractAircraft.MFA_SCORE__c = 100;
        contractAircraft.Contractual_Delivery_Month__c = Date.today();
        contractAircraft.Status__c = 'Planned';
       contractAircraft.Aircraft_Price__c = aircraftPrice1.Id;
        insert contractAircraft;

        Aircraft_PDP__c aircraftPDP = new Aircraft_PDP__c();
        aircraftPDP.Contract_Aircraft__c = contractAircraft.Id;
        aircraftPDP.Status_L__c = 'Pending';
        insert aircraftPDP;

        SOB_Event__c sobEvent = new SOB_Event__c();
        sobEvent.Commercial_Agreement__c = agreement.Id;
        sobEvent.Contract_Aircraft__c = contractAircraft.Id;
        sobEvent.SOB_Status__c = 'Firm Orders Included';
        insert sobEvent;

        Task task = new Task();
        task.Description = 'test';
        task.Subject = 'test';
        insert task;

        TaskRelation taskRelation = new TaskRelation();
        taskRelation.TaskId = task.Id;
        taskRelation.RelationId = agreement.Id;
        taskRelation.IsWhat = true;
        insert taskRelation;
       
        agreement.Status_Category__c ='PA Signed';
        update agreement;
        
        List<Agreement__c> listAgg = [Select Id, Status__c, Status_Category__c FROM Agreement__c];
        System.debug('is_agreement_set_to_expire.listAgg: ' + listAgg);

        List<Agreement_Aircraft__c> listAggAir = [Select Status__c FROM Agreement_Aircraft__c];
        System.debug('is_agreement_set_to_expire.listAggAir: ' + listAggAir);

        List<Aircraft_PDP__c> listAirPDP = [Select Contract_Aircraft__c, Status_L__c FROM Aircraft_PDP__c];
        System.debug('is_agreement_set_to_expire.listAirPDP: ' + listAirPDP);

        List<SOB_Event__c> listSOBEvt = [Select Commercial_Agreement__c, Contract_Aircraft__c, SOB_Status__c FROM SOB_Event__c];
        System.debug('is_agreement_set_to_expire.listSOBEvt: ' + listSOBEvt);

        Test.startTest();
   		ApexPages.currentPage().getParameters().put('id',listAgg[0].Id);
        //ApexPages.StandardController sc = new ApexPages.StandardController(listAgg[0]);
        //CLMEndAgreementController eac = new CLMRevertToRequestController(sc);
        CLMEndAgreementController.terminate();

        Test.stopTest();
    }
    
    static testMethod void controllerAndEndAgreementTestErrorCases(){
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        insert acc;
            
        Agreement__c agreement = new Agreement__c();
        agreement.Name = 'Test 1';
        agreement.Account__c = acc.Id;
        agreement.Country__c = 'Iceland';
        agreement.Agreement_Code__c = 'ABCDEFG';
        agreement.Region_Code__c = 'RegionCODE';
        agreement.Status__c = 'Request';
        agreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        agreement.Company_Signed_Date__c = Date.today(); 
        agreement.Kickoff_trigger_already_executed__c = false; 
        agreement.Admin_Mode__c = 'true';
        agreement.Agreement_Color__c = '0000FF';
        insert agreement;
        agreement.Status_Category__c ='PA Signed';
        update agreement;
        
        
        List<Agreement__c> listAgg = [Select Id, Status__c, Status_Category__c FROM Agreement__c];
        System.debug('is_agreement_set_to_expire.listAgg: ' + listAgg);

        Test.startTest();
   		ApexPages.currentPage().getParameters().put('id',null);
        CLMEndAgreementController.terminate();
        CLMEndAgreementController.expire();
        
        ApexPages.currentPage().getParameters().put('id',listAgg[0].Id);
        CLMEndAgreementController.terminate();
        
        agreement.Status_Category__c ='Superseded';
        update agreement;
        CLMEndAgreementController.terminate();
        
        agreement.Status_Category__c ='PA Request';
        update agreement;
        CLMEndAgreementController.terminate();
        
        agreement.Status_Category__c ='PA Signed';
        update agreement;
        CLMEndAgreementController.expire();
        
        agreement.Status_Category__c ='Expired';
        update agreement;
        CLMEndAgreementController.expire();
        CLMEndAgreementController.terminate();
        
        agreement.Status_Category__c ='Terminated';
        update agreement;
        CLMEndAgreementController.expire();
        CLMEndAgreementController.terminate();
        
        
        Test.stopTest();
        
    }
    
    
    
/*
   @TestSetup
   static void  insertAgreement(){
        
    }
*/
}