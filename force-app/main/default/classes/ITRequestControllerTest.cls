@IsTest 
public class ITRequestControllerTest
{
        
        
    static testMethod void unitTest1()
    {
            
            
        Profile profile = [Select Id From Profile Where UserType= 'Standard' Limit 1];
        List<User> lstUsers = new List<User>();
        
        User userTemp = SObjectInstanceTest.createUser(profile.Id);
        userTemp.email =  'unitTest1@mailtest.com'; 
        userTemp.Username = 'unitTest1@mail.com.br';
        userTemp.Alias = 'unt1'; 
        userTemp.CommunityNickname = 'Unit Test Nickname1'; 
        lstUsers.add(userTemp);
        
        userTemp = SObjectInstanceTest.createUser(profile.Id);
        userTemp.email =  'unitTest2@mailtest.com'; 
        userTemp.Username = 'unitTest2@mail.com.br';
        userTemp.Alias = 'unt2'; 
        userTemp.CommunityNickname = 'Unit Test Nickname2'; 
        lstUsers.add(userTemp);
        
        userTemp = SObjectInstanceTest.createUser(profile.Id);
        userTemp.email =  'unitTest3@mailtest.com'; 
        userTemp.Username = 'unitTest3@mail.com.br';
        userTemp.Alias = 'unt3'; 
        userTemp.CommunityNickname = 'Unit Test Nickname3'; 
        lstUsers.add(userTemp);
        
        userTemp = SObjectInstanceTest.createUser(profile.Id);
        userTemp.email =  'unitTest4@mailtest.com'; 
        userTemp.Username = 'unitTest4@mail.com.br';
        userTemp.Alias = 'unt4'; 
        userTemp.CommunityNickname = 'Unit Test Nickname4'; 

        lstUsers.add(userTemp);
        
        userTemp = SObjectInstanceTest.createUser(profile.Id);
        userTemp.email =  'unitTest5@mailtest.com'; 
        userTemp.Username = 'unitTest5@mail.com.br';
        userTemp.Alias = 'unt5'; 
        userTemp.CommunityNickname = 'Unit Test Nickname5'; 

        lstUsers.add(userTemp);
        
        insert lstUsers;
    
        List<ITRequestsEmailSupportUsers__c> customSettingList = new List<ITRequestsEmailSupportUsers__c>();
        customSettingList.add(new ITRequestsEmailSupportUsers__c(RelatedApllicationAreas__c ='TST',SupportAgentMail__c = 'unitTest1@mailtest.com',Name = 'Unit Test record 1'));     
        customSettingList.add(new ITRequestsEmailSupportUsers__c(RelatedApllicationAreas__c ='All',SupportAgentMail__c = 'unitTest2@mailtest.com',Name = 'Unit Test record 2'));     
        
        insert customSettingList;
         
        ITRequestApplication__c application = new  ITRequestApplication__c();
        application.Name = 'Unit test application';
        application.ApplicationtKey__c = 'TST';
        insert application;
        
        
        List<ITRequest__c> iTRequestList = new List<ITRequest__c>(); 
        iTRequestList.add(new ITRequest__c(ApplicationArea__c = application.Id,Status__c = 'New',RequestedOnBehalfOf__c = lstUsers[0].Id,RequestedBy__c = lstUsers[1].Id,Name  = 'Unit test record 1',Description__c = 'Description of the first record',AssignedTo__c = lstUsers[2].Id,ResolvedBy__c = lstUsers[3].Id));
        iTRequestList.add(new ITRequest__c(ApplicationArea__c = application.Id,Status__c = 'New',RequestedBy__c = lstUsers[4].Id,Name  = 'Unit test record 2',Description__c = 'Description of the second record',AssignedTo__c = lstUsers[3].Id));
        iTRequestList.add(new ITRequest__c(ApplicationArea__c = application.Id,Status__c = 'In Progress',RequestedBy__c = lstUsers[4].Id,Name  = 'Unit test record 3',Description__c = 'Description of the third record',AssignedTo__c = lstUsers[3].Id));
        iTRequestList.add(new ITRequest__c(ApplicationArea__c = application.Id,Status__c = 'Pending',RequestedBy__c = lstUsers[4].Id,Name  = 'Unit test record 4',Description__c = 'Description of the fourth record',AssignedTo__c = lstUsers[3].Id));
        iTRequestList.add(new ITRequest__c(ApplicationArea__c = application.Id,Status__c = 'On Hold',RequestedBy__c = lstUsers[4].Id,Name  = 'Unit test record 5',Description__c = 'Description of the fifth record',AssignedTo__c = lstUsers[3].Id,RecordTypeId = Schema.SObjectType.ITRequest__c.getRecordTypeInfosByName().get('New Feature').getRecordTypeId()));
        iTRequestList.add(new ITRequest__c(ApplicationArea__c = application.Id,Status__c = 'Done',RequestedBy__c = lstUsers[4].Id,Name  = 'Unit test record 6',Description__c = 'Description of the sixth record',AssignedTo__c = lstUsers[3].Id));
        
        insert iTRequestList;
        
        PageReference pageRef = Page.ITRequestPriorizationBoard;
        
        ApexPages.StandardController stdController = new ApexPages.standardController(iTRequestList[0]);
            
        ITRequestController controller = new ITRequestController(stdController); 
        
        Test.setCurrentPage(pageRef);
        
        List<ITRequest__c> requestsTest = controller.requests;
        integer testInt = controller.backlogSize;
        
        
        
        controller.save();
    
    }
}