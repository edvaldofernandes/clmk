@isTest(SeeAllData=true)
global class WebServiceRcpOutBoundTest implements WebServiceMock{
    
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               
           RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse SfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse();
           SfInfoElement.status = Constants.STATUS_CALL_OUT;        
       	   response.put('response_x', SfInfoElement); 
   }
    
    @isTest static void testRequest() {              
        
        Test.setMock(WebServiceMock.class, new WebServiceRcpOutBoundTest());
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element();
        RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort rcpSFRequest = new RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort();

        String output = rcpSFRequest.updateFromSalesForce(SfInfoElement);

        System.assertEquals(Constants.STATUS_CALL_OUT, output); 
    }
}