@isTest
public class viewSurveyResultsCompControll_Test {

    private static testmethod void testResultController()
{
    SurveyForce__c mySurvey = new SurveyForce__c();
    mySurvey.Submit_Response__c = 'empty';  
    insert mySurvey;
    
    viewSurveyResultsComponentController vsr = new viewSurveyResultsComponentController();
    vsr.surveyId = mySurvey.Id;
    
    String surveyId = vsr.surveyId;
    String  reportId = vsr.reportId;
    
    vsr.getResults();
    
    String mySurveyId = mySurvey.Id;
    PageReference pageRef = new PageReference ('/' + vsr.reportId + '?pv0=' + mySurveyId.substring(0,15));
    System.assertEquals(pageRef.getURL(),vsr.getResults().getURL());
    
}
}