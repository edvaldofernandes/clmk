@isTest
public class CLMChecklistTaskControllerTest 
{

    static testMethod void createTaskTestAll()
    {
    
        
        Account account = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());


        Agreement__c agreementTest = new Agreement__c(
            Name = 'Test',
            Account__c = account.Id,
            Country__c = 'Brazil',
            Status__c = 'Request',
            Contract_Signature_Date__c = date.today(),
            Signature_Date__c =  date.today()
        );
        insert agreementTest;
        
        Task_Setup__c  taskSetup = new Task_Setup__c();    
        taskSetup.Name = 'Task Setup Tes'; 
        taskSetup.Leap_Time__c = 1; 
        taskSetup.Selected_Date__c = 'Contract Signature Date'; 
        taskSetup.Answer__c = 'Answer test';
        taskSetup.Question__c = 'All'; 
        taskSetup.Question_API__c = 'All';
        taskSetup.Before_Selected_date__c = true;
        taskSetup.Description__c = 'Test Description';
        taskSetup.Sequence__c = 997;
        taskSetup.RecordTypeId = Schema.SObjectType.Task_Setup__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert taskSetup;

        Task_Setup__c  taskSetup2 = new Task_Setup__c();    
        taskSetup2.Name = 'Task Setup Test02'; 
        taskSetup2.Leap_Time__c = 1; 
        taskSetup2.Selected_Date__c = 'Signature Date'; 
        taskSetup2.Answer__c = 'Answer test';
        taskSetup2.Question__c = 'All'; 
        taskSetup2.Question_API__c = 'All';
        taskSetup2.Before_Selected_date__c = true;
        taskSetup2.Description__c = 'Test Description 2';
        taskSetup2.Sequence__c = 999;
        taskSetup2.RecordTypeId = Schema.SObjectType.Task_Setup__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert taskSetup2;

        
        //create checklist AGREEMENT (questionaire)
        Questionnaire__c questionnaire = new Questionnaire__c();
        questionnaire.Factory_Tour__c = true;
        questionnaire.Acceptance_Flight_Card__c = 'Valor 1 TESTE';
        questionnaire.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire.Commercial_Agreement__c = agreementTest.Id;
        questionnaire.Foreign_Registration__c ='';
        questionnaire.RecordTypeId = Schema.SObjectType.Questionnaire__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert questionnaire;
        
        

        
        PageReference pageRef = Page.CLMChecklistTaskPage;
        ApexPages.StandardController stdController = new ApexPages.standardController([Select Commercial_Agreement__c,Commercial_Agreement__r.Contract_Signature_Date__c , Contract_Aircraft__c , Delivery_Date__c , RecordTypeId, RecordType.Name ,
                                 AFA__c , Contract_Signature_Date__c ,Commercial_Agreement__r.Signature_Date__c, Delivery_Month__c , Assign_Task_to__c, Factory_Tour__c   
                                 From 
                                 Questionnaire__c 
                                 Where 
                                 Id = :questionnaire.id Limit 1]);
        
        
        
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('actionToPerform','createTasks');
        CLMChecklistTaskController controller = new CLMChecklistTaskController(stdController);  
        
        controller.processar();
        
        
        
        controller.returnPreviousPage();

        List<String>  fields = controller.populateFieldsList();
        
        string picklistValue = CLMChecklistTaskController.getPickListValue(null);
        
        //boolean testCreate = controller.createTasks();
        //boolean testDelete = controller.deleteTasks();
        
        /*
        CLMCheckListTask.createTask(checkList);
        
      taskSetup2.Selected_Date__c = 'Delivery Month'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
        taskSetup2.Selected_Date__c = 'Delivery Date'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
        */
        
        ApexPages.currentPage().getParameters().put('actionToPerform','deleteTasks');
        CLMChecklistTaskController controller2 = new CLMChecklistTaskController(stdController);  
        controller2.processar();

    }
    
     static testMethod void myUnitTest()
    {
    
        
        Account account = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());


        Agreement__c agreementTest = new Agreement__c(
            Name = 'Test',
            Account__c = account.Id,
            Country__c = 'Brazil',
            Status__c = 'Request',
            Contract_Signature_Date__c = date.today()
        );
        insert agreementTest;
        
        Task_Setup__c  taskSetup = new Task_Setup__c();    
        taskSetup.Name = 'Task Setup Tes'; 
        taskSetup.Leap_Time__c = 1; 
        taskSetup.Selected_Date__c = 'Contract Signature Date'; 
        taskSetup.Answer__c = 'Valor 1 TESTE';
        taskSetup.Question__c = 'Question Test'; 
        taskSetup.Question_API__c = 'Acceptance_Flight_Card__c';
        taskSetup.Before_Selected_date__c = true;
        taskSetup.Description__c = 'Test Description';
        taskSetup.Sequence__c = 997;
        taskSetup.RecordTypeId = Schema.SObjectType.Task_Setup__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert taskSetup;

        Task_Setup__c  taskSetup2 = new Task_Setup__c();    
        taskSetup2.Name = 'Task Setup Test02'; 
        taskSetup2.Leap_Time__c = 1; 
        taskSetup2.Selected_Date__c = 'Signature Date'; 
        taskSetup2.Answer__c = 'true';
        taskSetup2.Question__c = 'Factory_Tour'; 
        taskSetup2.Question_API__c = 'Factory_Tour__c';
        taskSetup2.Before_Selected_date__c = true;
        taskSetup2.Description__c = 'Test Description 2';
        taskSetup2.Sequence__c = 999;
        taskSetup2.RecordTypeId = Schema.SObjectType.Task_Setup__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert taskSetup2;

        
        //create checklist AGREEMENT (questionaire)
        Questionnaire__c questionnaire = new Questionnaire__c();
        questionnaire.Factory_Tour__c = true;
        questionnaire.Acceptance_Flight_Card__c = 'Valor 1 TESTE';
        questionnaire.Amendment_Acceptance_Flight_Card__c = '';
        questionnaire.Commercial_Agreement__c = agreementTest.Id;
        questionnaire.Foreign_Registration__c ='';
        questionnaire.RecordTypeId = Schema.SObjectType.Questionnaire__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert questionnaire;
        
        

        
        PageReference pageRef = Page.CLMChecklistTaskPage;
        ApexPages.StandardController stdController = new ApexPages.standardController([Select Commercial_Agreement__c,Commercial_Agreement__r.Contract_Signature_Date__c , Contract_Aircraft__c , Delivery_Date__c , RecordTypeId, RecordType.Name ,
                                 AFA__c , Contract_Signature_Date__c ,COmmercial_Agreement__r.Signature_Date__c, Delivery_Month__c , Assign_Task_to__c, Factory_Tour__c   
                                 From 
                                 Questionnaire__c 
                                 Where 
                                 Id = :questionnaire.id Limit 1]);
        
        
        
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('actionToPerform','createTasks');
        CLMChecklistTaskController controller = new CLMChecklistTaskController(stdController);  
        
        controller.processar();
        
        controller.returnPreviousPage();

        List<String>  fields = controller.populateFieldsList();
        
        string picklistValue = CLMChecklistTaskController.getPickListValue(null);
        
        //boolean testCreate = controller.createTasks();
        //boolean testDelete = controller.deleteTasks();
        
        /*
        CLMCheckListTask.createTask(checkList);
        
      taskSetup2.Selected_Date__c = 'Delivery Month'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
        taskSetup2.Selected_Date__c = 'Delivery Date'; 
        update taskSetup2;
        CLMCheckListTask.createTask(checkList);
        
        */
        

    }
    
      
}