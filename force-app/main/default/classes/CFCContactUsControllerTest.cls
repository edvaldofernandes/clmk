@isTest
private class CFCContactUsControllerTest{
    
    static testMethod void myUnitTest(){        
        RecordType recordType = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        RecordType recordTypeCase = RecordTypeDao.getInstance().getByDeveloperName('CFC_Cases');
        
        User userTest = CFCTestSetup.getUser();
        
        System.runAs(userTest)  
        {   
            Test.startTest();
            
            ProposalTriggerHandler.noRecursive = true; 
            
            Product2 Produto = new Product2();
            Produto.Name = 'Produto Teste';
            Produto.ProductCode = 'a488';
            Produto.Product_Status__c = 'Active';
            Produto.RecordTypeId = recordType.Id;
            Produto.IsActive = true;
            database.insert(Produto);
            
            ApexPages.currentPage().getParameters().put('id',Produto.Id);  
            
            Case caseTest = new Case();	
            caseTest.AccountId = userTest.Contact.AccountId; 
            caseTest.ContactId = userTest.ContactId;
            caseTest.Status = 'Open';        
            caseTest.RecordTypeId = recordTypeCase.id;
            database.insert(caseTest);
            system.debug('caseTest1:'+caseTest);
            
            CFC_Configurations__c config = new CFC_Configurations__c();
            config.User_Name__c  = 'anmelo@embraer.com.br.deloitte';
            config.Desktop_Limit_KB__c = 40;
            config.Tablet_Limits_KB__c = 35;
            config.Mobile_Limits_KB__c = 20;
            
            config.Billboard_max_published_number__c = 5;
            config.URL_Communities__c = 'wwww.teste.com.br';
            config.Billboard_Time__c = 5;
            config.Email_to_send_error_job__c = 'teste@teste.com.br';
            
            Database.insert(config);
            
            Attachment attach=new Attachment(); 
            attach.Name= 'Unit Test Attachment'; 
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body'); 
            attach.body = bodyBlob; 
            attach.parentId = caseTest.Id; 
            attach.ContentType = 'application/msword'; 
            attach.IsPrivate = false; 
            attach.Description = 'Test'; 
            
            CFCContactUsController contactUs = new CFCContactUsController();    			    
            contactUs.GenerateOwnerId();
            contactUs.newCase.Type_of_Service__c = 'Information';
            contactUs.newCase.Description = 'Test';		    		    
            contactUs.Submit();
            contactUs.attachment = attach;
            contactUs.insertAttachment();
            contactUs.removeAttachmentId = attach.Id;
            contactUs.removeAtt();
            
            CFCContactUsController controller = new CFCContactUsController();    
            system.assert(controller.newCase != null); 
            
            Test.stopTest();  
        }   	    
    }
    
    static testMethod void testSubmitNoRequiredsFields(){        
        RecordType recordType = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        RecordType recordTypeCase = RecordTypeDao.getInstance().getByDeveloperName('CFC_Cases');
        
        User userTest = CFCTestSetup.getUser();
        
        System.runAs(userTest)  
        {   
            Test.startTest();
            
            ProposalTriggerHandler.noRecursive = true; 
            
            Product2 Produto = new Product2();
            Produto.Name = 'Produto Teste';
            Produto.ProductCode = 'a488';
            Produto.Product_Status__c = 'Active';
            Produto.RecordTypeId = recordType.Id;
            Produto.IsActive = true;
            database.insert(Produto);
            
            ApexPages.currentPage().getParameters().put('id',Produto.Id);  
            
            Case caseTest = new Case();	
            caseTest.AccountId = userTest.Contact.AccountId; 
            caseTest.ContactId = userTest.ContactId;
            caseTest.Status = 'Open';        
            caseTest.RecordTypeId = recordTypeCase.id;
            database.insert(caseTest);
            system.debug('caseTest1:'+caseTest);
            
            CFC_Configurations__c config = new CFC_Configurations__c();
            config.User_Name__c  = 'anmelo@embraer.com.br.deloitte';
            config.Desktop_Limit_KB__c = 40;
            config.Tablet_Limits_KB__c = 35;
            config.Mobile_Limits_KB__c = 20;
            
            config.Billboard_max_published_number__c = 5;
            config.URL_Communities__c = 'wwww.teste.com.br';
            config.Billboard_Time__c = 5;
            config.Email_to_send_error_job__c = 'teste@teste.com.br';
            
            Database.insert(config);
            
            Attachment attach=new Attachment(); 
            attach.Name= 'Unit Test Attachment'; 
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body'); 
            attach.body = bodyBlob; 
            attach.parentId = caseTest.Id; 
            attach.ContentType = 'application/msword'; 
            attach.IsPrivate = false; 
            attach.Description = 'Test'; 
            
            CFCContactUsController contactUs = new CFCContactUsController();    			    
            contactUs.Submit();
            
            CFCContactUsController controller = new CFCContactUsController();    
            system.assert(controller.newCase != null); 
            
            Test.stopTest();  
        }   	    
    }
}