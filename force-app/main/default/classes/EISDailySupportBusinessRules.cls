/**
* @author Marcilio Leite de Souza
* @date 21/02/2018
* @description: Business rules for EIS Daily Support record type
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           21 FEV 2018             Original Version
**/
public without sharing class EISDailySupportBusinessRules {
	
    /*
     * Populate related aircrafts based on operador
     */
    public static void populateRelatedAircraft(Map<Id,List<Aircraft__c>> mapIdOperatorByAircraft, Map<String,String> mapIdOperByIdComm){
        system.debug('EISDailySupportBusinessRules.populateRelatedAircraft.mapIdOperatorByAircraft: ' + mapIdOperatorByAircraft);
        system.debug('EISDailySupportBusinessRules.populateRelatedAircraft.mapIdOperByIdComm: ' + mapIdOperByIdComm);
        
        List<Related_Aircraft__c> listInsRelAirc = new List<Related_Aircraft__c>();
        
        for(String idOpe : mapIdOperatorByAircraft.keySet()){
            
            for(Aircraft__c objAirc : mapIdOperatorByAircraft.get(idOpe)){
                Related_Aircraft__c relAir = new Related_Aircraft__c();
                relAir.EIS_Daily_Report__c = mapIdOperByIdComm.get(idOpe);
                relAir.Aircraft__c = objAirc.Id;
                relAir.RecordTypeId = Schema.SObjectType.Related_Aircraft__c.getRecordTypeInfosByName().get('Aircraft in EIS').getRecordTypeId();
                listInsRelAirc.add(relAir);
            }
        }
        system.debug('EISDailySupportBusinessRules.populateRelatedAircraft.listInsRelAirc: ' + listInsRelAirc);
        Database.insert(listInsRelAirc, false);
    }
    
     /*
     * Populate related MEL based on old communications and status OPEN
     */
    public static void populateRelatedMEL(Map<Id,Communications__c> newRecordsMap, Map<Id,List<Aircraft__c>> mapIdOperadorByAircraft){
       
        String strSetIds = Utils.getStringFromSet(mapIdOperadorByAircraft.keySet());
 	
        List<Minimum_Equipment_List__c> listMEL = database.query(' SELECT ' + utils.getAllFields('Minimum_Equipment_List__c') + ',' +
                                                                 ' Aircraft__r.Operator__c FROM Minimum_Equipment_List__c' +
                                                                 ' WHERE Aircraft__r.Operator__c IN (' + strSetIds +') ' + 
                                                                 '   AND Status__c = \'OPEN\''); 
        
        system.debug('EISDailySupportBusinessRules.populateRelatedMEL.listMEL: ' + listMEL);
        system.debug('EISDailySupportBusinessRules.populateRelatedMEL.listMEL.size(): ' + listMEL.size());
        
        List<Minimum_Equipment_List__c> listUpMEL = new List<Minimum_Equipment_List__c>();
        
        for(Minimum_Equipment_List__c obj : listMEL){
            Minimum_Equipment_List__c upMel = new Minimum_Equipment_List__c();
            upMel.Id = obj.Id;
            upMel.EIS_Daily_Report__c = newRecordsMap.values().get(0).Id;
            listUpMEL.add(upMel);
        }
        system.debug('EISDailySupportBusinessRules.populateRelatedMEL.listUpMEL: ' + listUpMEL);
        
        Database.update(listUpMEL, false);
    }
    
    /*
     * Send Email from EIS Daily report
     */
    public static void sendEmailEIS(Map<Id,Communications__c> newRecordsMap, Map<Id,List<Aircraft__c>> mapIdOperadorByAircraft, 
                                   Map<Id,List<Events__c>> mapIdCommuByInterRemol, Map<Id,List<Minimum_Equipment_List__c>> mapIdCommuByMEL){
   
        system.debug('EISDailySupportBusinessRules.sendEmail.mapIdOperadorByAircraft: ' + mapIdOperadorByAircraft);
        system.debug('EISDailySupportBusinessRules.sendEmail.mapIdCommuByInterRemol: ' + mapIdCommuByInterRemol);
        system.debug('EISDailySupportBusinessRules.sendEmail.mapIdCommuByMEL: ' + mapIdCommuByMEL);
        
        String sb = '';
        String commName = '';
        String referenceDate = '';                               
                                       
        for(Id idComm : newRecordsMap.keySet()){
        
           commName = newRecordsMap.get(idComm).name;
           referenceDate =  newRecordsMap.get(idComm).Reference_Date__c.format().left(10);
            
           sb = '<style type=\'text/css\'>' +
            '.tg  {border-collapse:collapse;border-spacing:0;}' +
            '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}' +
            '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}' +
            '.tg .tg-yw4l{vertical-align:top}' +
            '</style>' +

            '<p> <b> Current Fleet Status </b> </p>'+
            '<table class=\'tg\'>' +
            '  <tr>' +
            '    <th class=\'tg-yw4l\'>Aircraft</th>' +
            '    <th class=\'tg-yw4l\'>Registration</th>' +
            '    <th class=\'tg-yw4l\'>FH accumulated</th>' +
            '    <th class=\'tg-yw4l\'>FC accumulated</th>' +
            '    <th class=\'tg-yw4l\'>Daily Scheduled Flights</th>' +
            '    <th class=\'tg-yw4l\'>AOG Status</th>' +
            '    <th class=\'tg-yw4l\'>AOG Action to return to service</th>' +
            '  </tr>';
           
               //Iterate all aircrafts related by operator Id
               if(mapIdOperadorByAircraft.size() > 0){                   
                   for(Aircraft__c air : mapIdOperadorByAircraft.get(newRecordsMap.get(idComm).Operator__c)){
                       String sts = ''; if(air.EIS_AOG_Status__c) sts = '&#x2714;';
                       sb +='  <tr>';
                       sb +='<td class=\'tg-yw4l\'>'+air.Name+'</td>';
                       sb +='<td class=\'tg-yw4l\'>'+air.Registration__c+'</td>';
                       sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(air.FH_accumulated__c)+'</td>';
                       sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(air.FC_accumulated__c)+'</td>';
                       sb +='<td class=\'tg-yw4l\'>'+air.EIS_Daily_Scheduled_Flights__c+'</td>';
                       sb +='<td class=\'tg-yw4l\'>'+sts+'</td>';
                       sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(air.EIS_AOG_Action__c)+'</td>';
                       sb +='  </tr>';
                   }
               }
           sb+='</table>';
          
            List<Events__c> listInt = new List<Events__c>();
            List<Events__c> listRem = new List<Events__c>();
            List<Events__c> listPirep = new List<Events__c>();
            
               //Iterate all events interruptions / removals / pirep
            if(mapIdCommuByInterRemol.size() > 0){
                for(Events__c evt : mapIdCommuByInterRemol.get(idComm)){
                    if(evt.Daily_Summary_Interruption__c != null){
               			listInt.add(evt);
                    }else if(evt.Daily_Summary_Removals__c != null){
                        listRem.add(evt);
                    }else if(evt.Daily_Summary_Pirep__c != null){
                        listPirep.add(evt);
                    }
                }       
            }
            
            sb += '<p> <b> Interruptions ' + referenceDate + ' </b> </p>';
            if(listInt.size() > 0){
                sb += '<table class=\'tg\'>' +
                    '  <tr>' +
                    '    <th class=\'tg-yw4l\'>Aircraft</th>' +
                    '    <th class=\'tg-yw4l\'>ATA</th>' +
                    '    <th class=\'tg-yw4l\'>Fail Code</th>' +
                    '    <th class=\'tg-yw4l\'>Interruption Type</th>' +
                    '    <th class=\'tg-yw4l\'>Delay (Min)</th>' +
                    '    <th class=\'tg-yw4l\'>Event Description</th>' +
                    '    <th class=\'tg-yw4l\'>Maintenance Action</th>' +
                    '  </tr>';
                for(Events__c evt : listInt){
                    sb +='  <tr>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Aircraft__r.Name+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.ATA__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(evt.Fail_Code__c)+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Interruption_Type__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(String.valueOf(evt.Delay_Min__c))+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Event_Description__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(evt.Maintenance_Action__c)+'</td>';
                    sb +='  </tr>';
                }
                sb+='</table>';
            }else{
                sb += 'None';
            }
            
            sb += '<p> <b> Relevant PIREP/MAREP ' + referenceDate + ' </b> </p>';
            if(listPirep.size() > 0){
                sb += '<table class=\'tg\'>' +
                    '  <tr>' +
                    '    <th class=\'tg-yw4l\'>Aircraft</th>' +
                    '    <th class=\'tg-yw4l\'>ATA</th>' +
                    '    <th class=\'tg-yw4l\'>Event Description</th>' +
                    '    <th class=\'tg-yw4l\'>Maintenance Action</th>' +
                    '  </tr>';
                for(Events__c evt : listPirep){
                    sb +='  <tr>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Aircraft__r.Name+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.ATA__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Event_Description__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(evt.Maintenance_Action__c)+'</td>';
                    sb +='  </tr>';
                }
                sb+='</table>';
            }else{
                sb += 'None';
            }
            
            sb += '<p><b> Removals ' + referenceDate + ' </b></p>';
            if(listRem.size() > 0){
                sb +=  '<table class=\'tg\'>' +
                    '  <tr>' +
                    '    <th class=\'tg-yw4l\'>Aircraft</th>' +
                    '    <th class=\'tg-yw4l\'>ATA</th>' +
                    '    <th class=\'tg-yw4l\'>PN</th>' +
                    '    <th class=\'tg-yw4l\'>Reason for Removal</th>' +
                    '  </tr>';
                for(Events__c evt : listRem){
                    sb +='  <tr>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Aircraft__r.Name+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.ATA__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.PN__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+evt.Reason_for_Removal__c+'</td>';
                    sb +='  </tr>';
                }
                sb+='</table>';
            }else{
                sb += 'None';
            }

            sb += '<p><b> Minumum Equipment List ' + referenceDate + '</b></p>';
            
            if(mapIdCommuByMEL.size() > 0){
                sb += '<table class=\'tg\'>' +
                    '  <tr>' +
                    '    <th class=\'tg-yw4l\'>Aircraft</th>' +
                    '    <th class=\'tg-yw4l\'>MEL Code</th>' +
                    '    <th class=\'tg-yw4l\'>Status</th>' +
                    '    <th class=\'tg-yw4l\'>Open Date</th>' +
                    '    <th class=\'tg-yw4l\'>Days Open</th>' +
                    '    <th class=\'tg-yw4l\'>Days Open Allowed</th>' +
                    '    <th class=\'tg-yw4l\'>Description</th>' +
                    '    <th class=\'tg-yw4l\'>Associated Part Numbers</th>' +
                    '  </tr>';
                for(Minimum_Equipment_List__c mel : mapIdCommuByMEL.get(idComm)){
                    sb +='  <tr>';
                    sb +='<td class=\'tg-yw4l\'>'+mel.Aircraft__r.Name+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+mel.Name+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+mel.Status__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+mel.Open_Date__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+mel.Days_Open__c+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(String.valueOf(mel.Days_Open_Allowed__c))+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(mel.Description__c)+'</td>';
                    sb +='<td class=\'tg-yw4l\'>'+Utils.getStringWhenNull(mel.PN_PO__c)+'</td>';
                    sb +='  </tr>';
                }
                sb+='</table>';
            }else{
                sb += 'None';
            }

            sb+='<p><b> Other relevant information: : </b>'+ Utils.getStringWhenNull(newRecordsMap.get(idComm).Other_relevant_information__c) +'</p>';
        }
                               
        sb+='<p>'+Utils.getStringWhenNull(EIS_Daily_Report_Settings__c.getInstance().Email_Footer__c)+'</p> <br>';
        sb+='This message is intended solely for the use of its addressee and may contain privileged or ';
        sb+='confidential information. All information contained herein shall be treated as confidential ';
        sb+='and shall not be disclosed to any third party without Embraer’s prior written approval. If you';
        sb+='are not the addressee you should not distribute, copy or file this message. In this case, please';
        sb+='notify the sender and destroy its contents immediately.';
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(EIS_Daily_Report_Settings__c.getInstance().Email_to__c.split(';'));
        mail.setSubject(commName+ ' Daily Report');
        mail.setHtmlBody(sb);
        
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}