public class CasoWrapper {
	public String nomeRecType { get; set; }
    public String quantidadeTotal { get; set; }
    //public String status { get; set; }
    public String month { get; set; }
    public String lastDay { get; set; }
    //public String rTime { get; set; }
    public String quantidadeVermelho { get; set; }
    public String quantidadeAmarelo { get; set; }
    public String quantidadeVerde { get; set; }
    /*public String dataH { get; set; }
    public String hora { get; set; }
    public String contato { get; set; }
    public Id idContato {get; set; }
    public String conta { get; set; }
    public Id idConta { get; set; }
    public String atendente { get; set; }
    public Id idAtendente { get; set; }*/
    public String urlCarinha { get; set; }
    public String seta { get; set; }
    /*public Id idCaso { get; set; }
    public String urlFotoUser { get; set; }
    public boolean ownerIsQueue { get; set; }*/
    public boolean isInBox { get; set; }
    public String urlReportDay { get; set; }	
    public String urlReportMonth { get; set; }	

    public CasoWrapper(String recTypeName){
      this.urlCarinha = 'Carinha_OK.png';
      this.seta = 'Vazia.png';
      this.nomeRecType = recTypeName;
      this.isInbox = (String.isNotBlank(recTypeName) && recTypeName == 'INBOX');
    }
    public CasoWrapper(){
      this(null);
    }
}