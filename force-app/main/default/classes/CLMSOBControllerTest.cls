@isTest

private class CLMSOBControllerTest {

    private static Agreement__c createData(){
        Product2 productTest1 = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        productTest1.Show_In_SOB__c = TRUE;
        productTest1.DF_Group__c = '190/195';
        Product2 productTest2 = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        productTest2.Show_In_SOB__c = TRUE;
        productTest2.DF_Group__c = '170/175';
        database.insert(new List<Product2>{productTest1,productTest2});
        
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record
        Agreement__c apttusAPTSAgreement1 = new Agreement__c();
        apttusAPTSAgreement1.Name = 'apttusAPTSAgreement1Test';
        apttusAPTSAgreement1.Account__c = accountTest.id;
        apttusAPTSAgreement1.Country__c = 'Brazil';
        apttusAPTSAgreement1.Status_Category__c = 'PA Request';
        apttusAPTSAgreement1.Signature_Date__c = date.today();
        apttusAPTSAgreement1.Nickname__c = 'TST_FINAL';
        apttusAPTSAgreement1.Undisclosed_Nickname__c = 'UNDISCLOSED 01';
        apttusAPTSAgreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        //AgreementTriggerHandler.recursiveAmendProcess = false;
        insert(apttusAPTSAgreement1);
        

        List<Aircraft_Price__c> prices = new List<Aircraft_Price__c>
        {
            SObjectInstanceTest.createAircraftPrice ( apttusAPTSAgreement1.Id, productTest1.Id, 100000000 ),
            SObjectInstanceTest.createAircraftPrice ( apttusAPTSAgreement1.Id, productTest2.Id, 200000000 )
        };

        database.insert(prices); 
        
        //create line item Agreement_Aircraft__c
        List<Agreement_Aircraft__c> lsAgreementLine = new List<Agreement_Aircraft__c>();
        Agreement_Aircraft__c apttusAgreementLineItem1 = SObjectInstanceTest.createAgreementAircraft(productTest1.Id, apttusAPTSAgreement1.Id, 'Firm');
        apttusAgreementLineItem1.Aircraft_Price__c = prices.get(0).Id;
        apttusAgreementLineItem1.Status__c = 'Planned';
        lsAgreementLine.add(apttusAgreementLineItem1);
        Agreement_Aircraft__c apttusAgreementLineItem2 = SObjectInstanceTest.createAgreementAircraft(productTest2.Id, apttusAPTSAgreement1.Id, 'Option');
        apttusAgreementLineItem2.Aircraft_Price__c = prices.get(1).Id;
        apttusAgreementLineItem2.Status__c = 'Planned';
        lsAgreementLine.add(apttusAgreementLineItem2);
        Agreement_Aircraft__c apttusAgreementLineItem3 = SObjectInstanceTest.createAgreementAircraft(productTest1.Id, apttusAPTSAgreement1.Id, 'Firm');
        apttusAgreementLineItem3.Aircraft_Price__c = prices.get(0).Id;
        apttusAgreementLineItem3.Status__c = 'Planned';
        lsAgreementLine.add(apttusAgreementLineItem3);
        Agreement_Aircraft__c apttusAgreementLineItem4 = SObjectInstanceTest.createAgreementAircraft(productTest2.Id, apttusAPTSAgreement1.Id, 'Purchase Right');
        apttusAgreementLineItem4.Aircraft_Price__c = prices.get(1).Id;
        apttusAgreementLineItem4.Status__c = 'Planned';
        lsAgreementLine.add(apttusAgreementLineItem4);
        Agreement_Aircraft__c apttusAgreementLineItem5 = SObjectInstanceTest.createAgreementAircraft(productTest1.Id, apttusAPTSAgreement1.Id, 'Firm');
        apttusAgreementLineItem5.Aircraft_Price__c = prices.get(0).Id;
        apttusAgreementLineItem5.Status__c = 'Planned';
        lsAgreementLine.add(apttusAgreementLineItem5);
        database.insert(lsAgreementLine);
        
		apttusAPTSAgreement1.Status__c = 'PA Signed';
        database.update(apttusAPTSAgreement1);
        
        //create events to lineItemFirm
        List<SOB_Event__c> eventsList = new List<SOB_Event__c> 
        {
            // FIRM EVENTS
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2014, 1, 1), 'Firm Included'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2014, 1, 1), 'Firm Included'),
                
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2014, 1, 1), 'Firm converted from 175-E1 Firm'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2014, 1, 1), 'Firm converted from 175-E1 Firm'),
            
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2014, 2, 1), 'Firm converted from 175-E1 Firm'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2014, 2, 1), 'Firm converted from 175-E1 Firm'),
            
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2016, 1, 1), 'Delivered'),
            
            //LRsM 14/Ago/2017 - Valor do Campo Inativo em PRD pelo Adilson Rodrigues de Paula em 19/Jul/2017.    
            //SObjectInstanceTest.createSOBEvent(agreementsMap.get('firm2').Id, 'Firm', date.newInstance(2015, 1, 1), 'Firm Cancelled'),
            
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2016, 2, 1), 'Firm Transferred'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem1.Id, 'Firm', date.newInstance(2016, 12, 1), 'Delivered'),

            // OPTION EVENTS
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem2.Id, 'Firm', date.newInstance(2014, 1, 1), 'Option Included'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem2.Id, 'Firm', date.newInstance(2014, 1, 1), 'Option Included'),

            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem3.Id, 'Firm', date.newInstance(2015, 7, 1), 'Option Expired'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem3.Id, 'Firm', date.newInstance(2015, 8, 1), 'Option to Firm'),

            // PRA EVENTS
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem4.Id, 'Firm', date.newInstance(2014, 11, 1), 'PRA Included'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem4.Id, 'Firm', date.newInstance(2014, 11, 1), 'PRA Included'),

            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem5.Id, 'Firm', date.newInstance(2015, 1, 1), 'PRA Expired'),
            SObjectInstanceTest.createSOBEventForAircraft(apttusAgreementLineItem5.Id, 'Firm', date.newInstance(2016, 1, 1), 'PRA to Option')
        };

        database.insert(eventsList);

        
        //update agreement's Apttus__Status_Category__c 
        apttusAPTSAgreement1.Status_Category__c = 'PA Signed';
        database.update(new List<Agreement__c>{apttusAPTSAgreement1});
        
       
        ApexPages.currentPage().getParameters().put('source', 'realtime');
        
        system.debug('SELECT EVENTS >>> ' + [SELECT Id,Name,Event_Date__c,Book_Status__c FROM SOB_Event__c]);
        system.debug('SELECT EVENTS >>> ' + [SELECT Id,Name,Event_Date__c,Book_Status__c FROM SOB_Event__c].size());
        
        return apttusAPTSAgreement1;
    }
    
    @isTest static void publishedMainChangesReportTestSOE() {
        Agreement__c apttusAPTSAgreement1 = createData();
        test.startTest();

        SOB_Version__c testSOB = new SOB_Version__c();

        insert testSOB;
        // REALIZA O FREEZE
        ApexPages.StandardController sc = new ApexPages.StandardController(testSOB);
        CLMSOBController controller = new CLMSOBController(sc);

        controller.soeSelected = 'SOE';
        controller.getMonthList();
        controller.getSOEList();
        controller.yearSelected = string.valueOf(2014);
        controller.monthSelected = string.valueOf(1);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(2);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(3);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(4);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(5);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(6);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(7);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(8);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(9);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(11);
        controller.SOBFilter();
        
        test.stopTest();
    }

    @isTest static void publishedMainChangesReportTestSOI() {
        Agreement__c apttusAPTSAgreement1 = createData();
        test.startTest();
        // REALIZA O FREEZE
       
        SOB_Version__c testSOB = new SOB_Version__c();
        insert testSOB;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testSOB);
        CLMSOBController controller = new CLMSOBController(sc);

        controller.soeSelected = 'SOI';
        controller.getMonthList();
        controller.getYearList();
        controller.yearSelected = string.valueOf(2014);
        controller.monthSelected = string.valueOf(1);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(2);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(3);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(4);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(5);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(6);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(7);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(8);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(9);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(11);
        controller.SOBFilter();
        
        test.stopTest();
    }

    @isTest static void publishedMainChangesReportTestSOY() {
        Agreement__c apttusAPTSAgreement1 = createData();
        test.startTest();
        // REALIZA O FREEZE
        SOB_Version__c testSOB = new SOB_Version__c();
        insert testSOB;

        ApexPages.StandardController sc = new ApexPages.StandardController(testSOB);
        CLMSOBController controller = new CLMSOBController(sc);

        controller.soeSelected = 'SOY';
        controller.getMonthList();
        controller.getSOEList();
        controller.yearSelected = string.valueOf(2014);
        controller.monthSelected = string.valueOf(2);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(2);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(3);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(4);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(5);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(6);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(7);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(8);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(9);
        controller.SOBFilter();
        controller.monthSelected = string.valueOf(11);
        controller.SOBFilter();

        // TOTAL JETS
        
        system.debug('total jets: ' + controller.listTotalJets);
        system.assertEquals(5, controller.listTotalJets.size());
        system.assertEquals('Produto Teste2', controller.listTotalJets.get(2).Model__c); 
        system.assertEquals('1', controller.listTotalJets.get(2).Firm_Orders__c);
        system.assertEquals('0', controller.listTotalJets.get(2).Options__c);
        system.assertEquals('0', controller.listTotalJets.get(2).Purchase_Right__c);
        system.assertEquals('1', controller.listTotalJets.get(2).Total__c);
        system.assertEquals('0', controller.listTotalJets.get(2).Delivered__c);
        system.assertEquals('1', controller.listTotalJets.get(2).Firm_Order_Backlog__c);
        
        system.assertEquals('Produto Teste3', controller.listTotalJets.get(0).Model__c); 
        system.assertEquals('0', controller.listTotalJets.get(0).Firm_Orders__c);
        system.assertEquals('1', controller.listTotalJets.get(0).Options__c);
        system.assertEquals('1', controller.listTotalJets.get(0).Purchase_Right__c);
        system.assertEquals('2', controller.listTotalJets.get(0).Total__c);
        system.assertEquals('0', controller.listTotalJets.get(0).Delivered__c);
        system.assertEquals('0', controller.listTotalJets.get(0).Firm_Order_Backlog__c);

        // TOTAL CLIENTS
        system.debug('total clients: ' + controller.listTotalClients);
        system.assertEquals(4, controller.listTotalClients.size());
        system.assertEquals('Produto Teste3', controller.listTotalClients.get(3).Model__c); 
        system.assertEquals('', controller.listTotalClients.get(3).Customer__c); 
        system.assertEquals('Total', controller.listTotalClients.get(3).Country__c); 
        system.assertEquals('0', controller.listTotalClients.get(3).Firm_Orders__c);
        system.assertEquals('1', controller.listTotalClients.get(3).Options__c);
        system.assertEquals('1', controller.listTotalClients.get(3).Purchase_Right__c);
        system.assertEquals('2', controller.listTotalClients.get(3).Total__c);
        system.assertEquals('0', controller.listTotalClients.get(3).Delivered__c);
        system.assertEquals('0', controller.listTotalClients.get(3).Firm_Order_Backlog__c);

        system.assertEquals('Produto Teste2', controller.listTotalClients.get(0).Model__c); 
        system.assertEquals('TST_FINAL', controller.listTotalClients.get(0).Customer__c); 
        system.assertEquals('Brazil', controller.listTotalClients.get(0).Country__c); 
        system.assertEquals('1', controller.listTotalClients.get(0).Firm_Orders__c);
        system.assertEquals('0', controller.listTotalClients.get(0).Options__c);
        system.assertEquals('0', controller.listTotalClients.get(0).Purchase_Right__c);
        system.assertEquals('1', controller.listTotalClients.get(0).Total__c);
        system.assertEquals('0', controller.listTotalClients.get(0).Delivered__c);
        system.assertEquals('1', controller.listTotalClients.get(0).Firm_Order_Backlog__c);

        system.assertEquals('Produto Teste2', controller.listTotalClients.get(1).Model__c); 
        system.assertEquals('', controller.listTotalClients.get(1).Customer__c); 
        system.assertEquals('Total', controller.listTotalClients.get(1).Country__c); 
        system.assertEquals('1', controller.listTotalClients.get(1).Firm_Orders__c);
        system.assertEquals('0', controller.listTotalClients.get(1).Options__c);
        system.assertEquals('0', controller.listTotalClients.get(1).Purchase_Right__c);
        system.assertEquals('1', controller.listTotalClients.get(1).Total__c);
        system.assertEquals('0', controller.listTotalClients.get(1).Delivered__c);
        system.assertEquals('1', controller.listTotalClients.get(1).Firm_Order_Backlog__c);
        
        test.stopTest();
     }
    
     @isTest static void publishedTotalClientsReportTest(){
        Id rTypeTotalClients = RecordTypeMemory.getRecType('SOB_Data__c', 'Total_Clients');
        
        SOB_Version__c SOBVersion = new SOB_Version__c(Status__c='Draft',Month__c='April',Year__c='2016'); 
        database.insert(new List<SOB_Version__c>{SOBVersion});
        
        //create SOB Total Clients 
        SOB_Data__c SOBMainChanges1 = new SOB_Data__c(Type__c='Data',
                                                      SOI_M1__c='1',
                                                      SOI_M2__c='2',
                                                      SOI_M3__c='3',
                                                      SOE__c='4',
                                                      SOY__c='5',
                                                      SOB_Version__c=SOBVersion.Id,
                                                      Customer__c='No Customer',
                                                      Country__c='No Country',
                                                      Model__c='No Model',
                                                     RecordTypeId = rTypeTotalClients);
        
        database.insert(new List<SOB_Data__c>{SOBMainChanges1});

        ApexPages.currentPage().getParameters().put('source', 'published');
        ApexPages.currentPage().getParameters().put('id', SOBVersion.Id);        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(SOBVersion);
        CLMSOBController controller = new CLMSOBController(sc);
        controller.soeSelected = 'SOI';
        controller.monthSelected = string.valueOf(date.today().month());
        controller.yearSelected = string.valueOf(date.today().year());
        test.stopTest();
     } 
}