public without sharing class DocumentDAO {
    
    private static final DocumentDAO instance = new DocumentDAO();    
    
    private DocumentDAO(){
    }    
    
    public static DocumentDAO getInstance(){
        return instance;
    }
    
    public Document getListDocumentFolderCFC() {
        /*List<Document> lstDocuments = database.query(' Select ' + utils.getAllFields('Document') +
                                                     ' where folder.Name = \'CFC\' and Name = \'Reference Guide MRO\' ');*/
                                                  
        List<Document> lstDocuments = [Select id, Name from Document where folder.Name = 'CFC' and Name = 'Reference Guide MRO'];
                                                  
        if(lstDocuments.size() > 0){
            return lstDocuments[0];
        }
        
      	return null;
    }
}