@isTest  
private class CLMWatermarkWebServiceTest
{    

    @isTest static void testResponse() 
    {              
        Test.setMock(WebServiceMock.class, new CLMWatermarkWebServiceMockImpl());
        
        CLMWatermarkWebService.serviceEndpoint = 'www.servic.com';
        CLMWatermarkWebService.WatermarkDocServicePort service = new CLMWatermarkWebService.WatermarkDocServicePort();
        String output = service.getWatermarkedFile('Hello World!') ;
        
        System.assertEquals('Mock response', output); 
    }


}