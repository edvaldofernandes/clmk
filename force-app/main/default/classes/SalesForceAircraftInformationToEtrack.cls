public class SalesForceAircraftInformationToEtrack {
    
    private List<AircraftInformation> aircraftInformationList;
    private List<AircraftHistoryInformation> aircraftHistoryList;
    
    
    public SalesForceAircraftInformationToEtrack(){
        
        initCommonObejcts();
    }
    
    private void initCommonObejcts(){
        
        aircraftInformationList = new List<AircraftInformation>();
        aircraftHistoryList = new List<AircraftHistoryInformation>();
    }
    
    public void informationAfterInsertOrUpdate(Aircraft__c[] aircraftNewOnTrigger){
        
        Aircraft__c aircraftNew = aircraftNewOnTrigger[0];
        
        List<Account> accountList = findAccount(aircraftNew);
        
        Account account = accountList.size() > 0 ? accountList.get(0) : new Account();
        
        for(Aircraft__History aircraftHistory : RcpRepository.findAircraftHistoryByAircratId(aircraftNew.id)){

            String namePhoneAndEmail = RcpRepository.findUserByUserId(aircraftHistory.CreatedById);
            aircraftHistoryList.add(new AircraftHistoryInformation(aircraftHistory.CreatedDate, aircraftHistory.Field, String.valueOf(aircraftHistory.OldValue), String.valueOf(aircraftHistory.NewValue), namePhoneAndEmail));
        }
        
        historyInformationAfterInsertOrUpdate(aircraftNewOnTrigger, account);
        
        saveAndFireCallout();
    }
    
    private void historyInformationAfterInsertOrUpdate(Aircraft__c[] aircraftNewOnTrigger, Account account){
        
        for(Aircraft__c aircraft : aircraftNewOnTrigger){
            aircraftInformationList.add(new AircraftInformation(aircraft, account, Constants.NEW_OBJECT_CREATED, Constants.STATUS_ACTIVE));
        }
    }
    
    public void informationBeforeUpdate(Aircraft__c[] aircraftOldTrigger){
        
        Aircraft__c aircraftOld = aircraftOldTrigger[0];
        
        List<Account> accountList = findAccount(aircraftOld);
        
        Account account = accountList.size() > 0 ? accountList.get(0) : new Account();
        
        for(Aircraft__History aircraftHistory : RcpRepository.findAircraftHistoryByAircratId(aircraftOld.id)){
            
            String namePhoneAndEmail = RcpRepository.findUserByUserId(aircraftHistory.CreatedById);
            
            aircraftHistoryList.add(new AircraftHistoryInformation(aircraftHistory.CreatedDate, 
                                                                   aircraftHistory.Field,
                                                                   String.valueOf(aircraftHistory.OldValue),
                                                                   String.valueOf(aircraftHistory.NewValue),
                                                                   namePhoneAndEmail));
        }
        
        historyInformationAfterUpdate(aircraftOldTrigger, account);
        
        saveAndFireCallout();
    }
    
    private void historyInformationAfterUpdate(Aircraft__c[] aircraftOldTrigger, Account account){
        
        for(Aircraft__c aircraft : aircraftOldTrigger){
            
            aircraftInformationList.add(new AircraftInformation(aircraft, 
                                                                account,  
                                                                Constants.OLD_OBJECT_UPDATED_OR_DELETED, 
                                                                Constants.STATUS_ACTIVE));
        }
        
    }
    
    public void informationDelete(Aircraft__c[] aircraftOldTrigger){
        
        Aircraft__c aircraftDelete = aircraftOldTrigger[0];
        
        List<Account> accountList = findAccount(aircraftDelete);
        
        Account account = accountList.size() > 0 ? accountList.get(0) : new Account();
        
        for(Aircraft__History aircraftHistory : RcpRepository.findAircraftHistoryByAircratId(aircraftDelete.id)){
            
            String namePhoneAndEmail = RcpRepository.findUserByUserId(aircraftHistory.CreatedById);
            
            aircraftHistoryList.add(new AircraftHistoryInformation(aircraftHistory.CreatedDate, 
                                                                   aircraftHistory.Field,
                                                                   String.valueOf(aircraftHistory.OldValue),
                                                                   String.valueOf(aircraftHistory.NewValue),
                                                                   namePhoneAndEmail));
        }
        
        historyInformationDelete(aircraftOldTrigger, account);
        
        saveAndFireCallout();
    }
    
    private void historyInformationDelete(Aircraft__c[] aircraftOldTrigger, Account account){
        
        for(Aircraft__c aircraft : aircraftOldTrigger){
            aircraftInformationList.add(new AircraftInformation(aircraft, 
                                                                account, 
                                                                Constants.OLD_OBJECT_UPDATED_OR_DELETED, 
                                                                Constants.STATUS_DELETED));
        }
    }
    
    private List<Account> findAccount(Aircraft__c aircraft){
        
        List<Account> accountList = [SELECT id, flyEmbraerId__c FROM Account WHERE id = :aircraft.Owner__c];
        
        if(accountList == null)
            accountList = [SELECT id, flyEmbraerId__c FROM Account WHERE id = :aircraft.Operator__c];
        
        return accountList;
    }
    
    public void saveAndFireCallout(){
        
        Visitator visitator = new CallOutRepository();
        visitator.visit(aircraftInformationList, aircraftHistoryList,String.valueOf(AircraftInformation.class)); 
        
        if(Test.isRunningTest())
            return;
        
        RcpOutBoundProxy.executeAircraft();        
        SalesForceToEtrackProxy.executeAircraft();
    }
}