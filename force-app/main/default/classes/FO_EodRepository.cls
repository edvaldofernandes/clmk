public class FO_EodRepository {
    public static EOD__c getById(Id eodId){
        return [
			SELECT
                EOD_Number__c,
                Related_Case__r.account.Name,
            	Issue_Date__c,
                Applicability__c,
                Description__c,
                Expiry_Date__c,
                Related_Case__c,
                Status__c,
                Subject__c,
                Operational_Disposition__c,
            	Show_Disclaimers__c,
            	Original_Report_Date__c,
            	Original_Report_Type__c,
            	Parent_EOD__c
            FROM
            	EOD__c
            WHERE
            	Id = :eodId
        ];
    }
    
	/**
    * @description Returns a list of aircraft on which the EOD is applicable.
    * 
    * @param Id eodId : The ID of the EOD.
	* @return List<Aircraft__c> : The related aircraft.
    **/
    public static List<Aircraft__c> getAssociatedAircraft(Id eodId){
        return [
            SELECT
            	Name
            FROM
            	Aircraft__c
            WHERE
            	Id IN (
                    SELECT
                    	Aircraft__c
                    FROM
                    	EOD_Aircraft_Association__c
                    WHERE EOD__c = :eodId
                )
        ];
    }
    
	/**
    * @description Returns all files' ids related to an Eod.
    * @param Id eodId : The ID of the EOD.
	* @return List<Id> : The files' ids related to the EOD.
    **/
    public static List<Id> getRelatedDocumentsIds(Id eodId){
        List<Id> documentIds = new List<Id>();
        List<ContentDocumentLink> links = [
            SELECT
            	ContentDocumentId
            FROM
            	ContentDocumentLink
            WHERE
            	LinkedEntityId =: eodId
        ];
        for(ContentDocumentLink l : links){
            documentIds.add(l.ContentDocumentId);
        }
        return documentIds;
    }
}