/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for copying the related aircraft from the opportunity line items
* to related service contract line items when they are created.
*
* NAME: ContractLineItemCopyRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 16/12/2014
*
*******************************************************************************/
public with sharing class ContractLineItemCopyRelatedAircraft 
{
  public static void execute()
  {
    TriggerUtils.assertTrigger();

    set<Id> lstServiceContractId = new set<Id>();
    list<ContractLineItem> lstContractLineItem = new list<ContractLineItem>();
    for ( ContractLineItem iServiceContract : (list<ContractLineItem>) trigger.new  )
    {
      lstContractLineItem.add(iServiceContract);
      lstServiceContractId.add(iServiceContract.ServiceContractId);
    }

    map<Id, Id> mapServiceContractOpportunityId = new map<Id, Id>();
    for ( ServiceContract iServiceContract : [SELECT Id, Opportunity__c FROM ServiceContract WHERE Id =: lstServiceContractId AND Contract_Status__c != 'Signed'])
    {
      mapServiceContractOpportunityId.put(iServiceContract.Id, iServiceContract.Opportunity__c);
    }
    if ( mapServiceContractOpportunityId.isEmpty() ) return;

    list<String> lstOliId = new list<String>();
    map<Id, list<OpportunityLineItem>> mapOppOli = new map<Id, list<OpportunityLineItem>>();

    for ( OpportunityLineItem oli: [SELECT OpportunityId, PricebookEntryId,
      Quantity, UnitPrice, princing__c, Catalogue_Price__c
      FROM OpportunityLineItem
      WHERE OpportunityId =: mapServiceContractOpportunityId.Values() ])
    {
      list<OpportunityLineItem> lstOli = mapOppOli.get(oli.OpportunityId);
      if ( lstOli == null )
      {
        lstOli = new list<OpportunityLineItem>();
        mapOppOli.put(oli.OpportunityId, lstOli);
      }
      lstOli.add(oli);
      lstOliId.add(oli.Id);
    }
    if ( mapOppOli.isEmpty() ) return;

    map<String, list<Related_Aircraft__c>> mapOliAircraft = new map<String, list<Related_Aircraft__c>>();

    for ( Related_Aircraft__c relAir: [SELECT Aircraft__c, Opportunity_Fleet_Type__c, Opportunity_product_id__c
      FROM Related_Aircraft__c
      WHERE Opportunity_product_id__c =: lstOliId ])
    {
      list<Related_Aircraft__c> lstRelAircraft = mapOliAircraft.get(relAir.Opportunity_product_id__c);
      if ( lstRelAircraft == null )
      {
        lstRelAircraft = new list<Related_Aircraft__c>();
        mapOliAircraft.put(relAir.Opportunity_product_id__c, lstRelAircraft);
      }
      lstRelAircraft.add(relAir);
    }
    if ( mapOliAircraft.isEmpty() ) return;

    list<Related_Aircraft__c> lstServiceContractRelatedAircraft = new list<Related_Aircraft__c>();
    for ( ContractLineItem iServiceContract : lstContractLineItem )
    {
      Id idOpp = mapServiceContractOpportunityId.get(iServiceContract.ServiceContractId);
      if ( IdOpp == null ) continue;

      list<OpportunityLineItem> lstOli = mapOppOli.get(idOpp);
      if ( lstOli == null ||  lstOli.isEmpty() ) continue;

      Integer i = -1;
      for ( OpportunityLineItem oli: lstOli)
      {
        i++;

        if ( oli.PricebookEntryId == iServiceContract.PricebookEntryId  && oli.Quantity == iServiceContract.Quantity
          && oli.UnitPrice == iServiceContract.UnitPrice && oli.princing__c == iServiceContract.princing__c )
        {
          String idOli = oli.Id;
          list<Related_Aircraft__c> lstRelAircraft = mapOliAircraft.get(idOli);
          if ( lstRelAircraft == null ) continue;

          for ( Related_Aircraft__c relAirOpp : lstRelAircraft )
          {
            Related_Aircraft__c relAirServiceContract = new Related_Aircraft__c();
            relAirServiceContract.Aircraft__c = relAirOpp.Aircraft__c;
            relAirServiceContract.Opportunity_Fleet_Type__c = relAirOpp.Opportunity_Fleet_Type__c;
            relAirServiceContract.Service_Contract__c = iServiceContract.ServiceContractId;
            relAirServiceContract.Contract_Line_Item__c = iServiceContract.Id;
            lstServiceContractRelatedAircraft.add(relAirServiceContract);
          }
          break;
        }
      }
      lstOli.remove(i);
    }

    if ( !lstServiceContractRelatedAircraft.isEmpty() ) insert lstServiceContractRelatedAircraft;
  }
}