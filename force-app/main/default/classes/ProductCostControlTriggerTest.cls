@isTest
public class ProductCostControlTriggerTest {
    
    @testSetup
    static void setup(){
        Product2 prod = new Product2(
        Name = 'Product name');
        
        insert prod;
        
    }
    
    static testmethod void test01(){
        
        Product2 Prod = [ select Name, Id From Product2 Where Name = 'Product name'];
        Test.startTest();
        Product_Cost_Control__c pcc1 = new Product_Cost_Control__c(
        	Related_Product__c = Prod.Id
        );
        insert pcc1;
        Test.stopTest();
        
        List<Product_Cost_Control__c> pccList = [select Id, Is_Active__c from Product_Cost_Control__c where Related_Product__c = :Prod.Id];
        
        System.assert(pccList.size() == 1, 'More then 1 Acitve PCC');
    }
    
    static testmethod void test02(){
        
        Product2 Prod = [ select Name, Id From Product2 Where Name = 'Product name'];
        
        Test.startTest();
        
        Product_Cost_Control__c pcc1 = new Product_Cost_Control__c( Related_Product__c = Prod.Id);
        
        insert pcc1;
        
        Product_Cost_Control__c pcc2 = new Product_Cost_Control__c( Related_Product__c = Prod.Id);
        
        insert pcc2;
        Test.stopTest();
        
        List<Product_Cost_Control__c> pccList2 = [select Id, Is_Active__c from Product_Cost_Control__c where Related_Product__c = :Prod.Id];
        List<Product_Cost_Control__c> pccList = [select Id, Is_Active__c from Product_Cost_Control__c where Related_Product__c = :Prod.Id and Is_Active__c = :True];
        System.debug('PCC LIST:' + pccList.size());
        System.assert(pccList.size() == 1, 'More then 1 Acitve PCC');
       	
        System.assertEquals(2, pccList2.size(), 'Ammount of PCCs');
    }
}