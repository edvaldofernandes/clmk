/* Author: Fabiano Albino Ferreira - TCS
 * Description: Populate Next Action Date Automatically.
 * CreatedDate: 25/09/2019.
 * LastModifiedDate: 30/09/2019
 */
public class CaseNextAction {
    
    //RecordTypes Case
    private Set<Id> RECORD_TYPES = new Set<Id>{
        Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId(),
        Schema.SObjectType.Case.getRecordTypeInfosByName().get('Backorder').getRecordTypeId(),
        Schema.SObjectType.Case.getRecordTypeInfosByName().get('Spare Parts Price').getRecordTypeId()
    };
    
    private Set<String> STATUS = new Set<String>{
        'back order', 'contract creation', 'pricing', 'financial', 
        'engineering', 'follow up', 'new customer', 'rts', 'warehouse'
    };
    
    private String BUSINESS_HOUR_NAME = Label.CRC_BusinessHourNextAction;

    private List<Case> cases;
    private Map<Id, Case> oldCases;
    private BusinessHours bh;
    private NextActionDatemdtDAO nextActionDatemdtDAO;
         
    public CaseNextAction(List<Case> cases, Map<Id, Case> oldCases){
        this.cases = cases;
        this.oldCases = oldCases;
        this.nextActionDatemdtDAO = new NextActionDatemdtDAO();
        setBusinessHour();
    }
    
    //Update NextActionDate
    public void setDateNextAction(){
        
        List<Case> parentCases = new List<Case>();
        
        try{
            
            Map<String, Integer> mapMetadataNextActionDate = buildMapMetadataNextActionDate();
            
            Datetime now = System.now();
            
            for(Case c : cases){
                
                Case oldCase = new Case();
                
                if(String.isBlank(c.ParentId) && oldCases.containsKey(c.Id)){
                    
                    oldCase = oldCases.get(c.Id);

                    System.debug('##New Case: ' + c.Next_Action_Required_Date_and_Time__c);
                    System.debug('##Old Case: ' + oldCase.Next_Action_Required_Date_and_Time__c);
                    
                    if((String.isBlank(c.ParentId) && Trigger.isInsert) || oldCase.Status == c.Status){

                        if(oldCase.Next_Action_Required_Date_and_Time__c != null){
                            c.Next_Action_Required_Date_and_Time__c = oldCase.Next_Action_Required_Date_and_Time__c;
                        }
                        
                        continue;
                    }
                }
                
                System.debug('##Case test trigger: ' + c);
                
                if(RECORD_TYPES.contains(c.RecordTypeId) && (String.isNotBlank(c.Status) && STATUS.contains(c.Status.toLowerCase()))){
                    
                    if(!mapMetadataNextActionDate.containsKey(c.Priority))
                        continue;
                    
                    Integer milliseconds = mapMetadataNextActionDate.get(c.Priority);
                    
                    Datetime nextDate = BusinessHours.add(bh.Id, now, milliseconds);
                    
                    System.debug('##nextDate: ' + nextDate);
                    
                    if(String.isBlank(c.ParentId) && Trigger.isUpdate){
                        c.Next_Action_Required_Date_and_Time__c = nextDate;
                        continue;
                    }else if(String.isNotBlank(c.parentId)){
                        c.Next_Action_Required_Date_and_Time__c = nextDate;
                    }
                    
                    Case parentCase = new Case(
                        Id = c.ParentId,
                        Next_Action_Required_Date_and_Time__c = nextDate
                    );
                    
                    parentCases.add(parentCase);
                }
            }   
            
            if(!parentCases.isEmpty())
                update parentCases; 
            
        }catch(Exception ex){
            System.debug('Falha ao atualizar Next_Action_Required_Date_and_Time__c: ' + ex.getMessage());
        }
        
    }
    
    private Map<String, Integer> buildMapMetadataNextActionDate(){
        
        Map<String, Integer> millisecondsByStatus = new Map<String, Integer>();
        
        List<Next_Action_Date__mdt> records = nextActionDatemdtDAO.getAllNext_Action_Date();
        
        for(Next_Action_Date__mdt record : records){
            millisecondsByStatus.put(
            	record.Priority__c, Integer.valueOf(record.Miliseconds__c)
            );
        }
        
        return millisecondsByStatus;
    }
        
    private void setBusinessHour(){
        bh = [SELECT Id, Name, SundayEndTime, MondayEndTime, TuesdayEndTime, TuesdayStartTime, MondayStartTime,                        SundayStartTime, WednesdayStartTime, ThursdayStartTime, WednesdayEndTime, ThursdayEndTime,                        FridayStartTime, FridayEndTime, SaturdayStartTime, SaturdayEndTime 
              FROM BusinessHours 
              WHERE Name = :BUSINESS_HOUR_NAME];
    }
}