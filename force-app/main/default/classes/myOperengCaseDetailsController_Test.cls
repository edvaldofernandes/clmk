@isTest
public class myOperengCaseDetailsController_Test {
    //Unit Test Case for get Case Details
    @isTest static void testGetCaseDetails(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        //Executing test
        List<Case> results = new List<Case>();
        results = myOperengCaseDetailsController.getCaseDetails(testCase[0].id);
        	
        Test.stopTest();
    }
    
    //Unit Test Case for if string contains embraer
    @isTest static void testgetContainEmbraer(){
        Test.startTest();
        //list with embraer
        List<String> domainswith = new List<String>();
        domainswith.add('test@embraer.com');
        Boolean resultsTrue;        
        resultsTrue = myOperengCaseDetailsController.getContainEmbraer(domainswith);
        
        Boolean resultsNull;
        List<String> domainswithout = new List<String>();
        //list without any items
        resultsNull = myOperengCaseDetailsController.getContainEmbraer(domainswithout);
        domainswithout.add('test@aa.com');
        Boolean resultsFalse;
        //list without embraer
		resultsFalse = myOperengCaseDetailsController.getContainEmbraer(domainswithout);
             	
        Test.stopTest();
    }
   
    //Unit Test Case for getting Case Comments
    @isTest static void testgetCaseComments(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        List<CaseComment> results = new List<CaseComment>();
        //Executing test
        results = myOperengCaseDetailsController.getCaseComments(testCase[0].id);
        Test.stopTest();
    }
    
    //Unit Test Case for getting email attachments
    @isTest static void testgetEmailAttachments(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 2);
        Database.insert(testEmail);
            Attachment attach = new Attachment();   	
            attach.Name = 'Unit Test Attachment';
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
            attach.Body = bodyBlob;
            attach.ParentId = testEmail[0].id;
            insert attach;
        List<Attachment> results = new List<Attachment>();
        //Executing test
        results = myOperengCaseDetailsController.getEmailAttachments(testEmail);
        Test.stopTest();
    }
    @isTest static void testCanGetUserAccount(){
        Account testedAccount;
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert acc;
        Contact c = new Contact(
                FirstName = 'test',
                LastName = 'test',
                AccountId = acc.Id,
                Email = 'test@test.com.br'
        );
        insert c;
        User u = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName,
                ContactId = c.Id,
                Username = c.Email,
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0' 
        ); 
        insert u;
        System.runAs(u) {
        Test.startTest();

            testedAccount = myOperengCaseDetailsController.getUserAccount();
            
        Test.stopTest();
        }       
 
        System.assertEquals(testedAccount.Id, acc.Id);
    }    
    @isTest static void testCanGetEmailDomainFromAccount(){
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec',
            FlightOps_Account_Domains__c = '@test.com.br'
        );
        insert acc;
        Set<String> domains = myOperengCaseDetailsController.getAccountEmailDomains(acc);
        
        System.assert(domains.contains('@test.com.br'));
    }
    @isTest static void testCanGetDomainFromEmail(){
        Set<String> emailDomains = new Set<String>();
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert acc;
        Contact c = new Contact(
                FirstName = 'test',
                LastName = 'test',
                AccountId = acc.Id,
                Email = 'test@test.com.br'
        );
        insert c;
        User u = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName,
                ContactId = c.Id,
                Username = c.Email,
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0' 
        ); 
        insert u;
        System.runAs(u) {
        Test.startTest();

            emailDomains = myOperengCaseDetailsController.getAccountEmailDomains(acc);
            
        Test.stopTest();
        }       
        System.assert(emailDomains.contains('@test.com.br'));
    }
    @isTest static void testCanGetEmailMessages(){      
        Set<String> domains = new Set<String>{'@test.com.br'};
        Case c = New Case(
            Subject = 'test',
            Customer_Visible__c = TRUE
        );
        insert c;
        
        EmailMessage email = New EmailMessage(
            Subject = 'test',
            parentId = c.Id,
            FromAddress = 'test@test.com.br'
        );
        insert email;
        
        List<EmailMessage> emails = myOperengCaseDetailsController.getCaseEmailMessagesByDomain(c.Id, domains);
       
        System.assertEquals(emails[0].Id, email.Id);
        
    }	
    @isTest static void testCannotGetInternalEmails(){
        Set<String> domains = new Set<String>{'@asdf.com.br'};
        Case c = New Case(
            Subject = 'test',
            Customer_Visible__c = TRUE
        );
        insert c;
        
        EmailMessage email = New EmailMessage(
            Subject = 'test',
            parentId = c.Id,
            FromAddress = 'test@test.com.br',
            ToAddress = 'test@test.com.br',
            BccAddress = 'test@test.com.br',
            CcAddress = 'test@test.com.br'
        );
        insert email;

        List<EmailMessage> emails = myOperengCaseDetailsController.getCaseEmailMessagesByDomain(c.Id, domains);
               
        System.assertEquals(emails.size(), 0);
        
    }
   	@isTest static void testIsGettingNameFromContact(){
        String name;
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert acc;
        Contact cc = new Contact(
                FirstName = 'asdf',
                LastName = 'asdf',
                AccountId = acc.Id,
                Email = 'test@test.com.br'
        );
        insert cc;
        User u = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id,
                Email = cc.Email,
                FirstName = 'a',
                LastName = 'a',
                ContactId = cc.Id,
                Username = cc.Email,
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0' 
        ); 
        insert u;
        System.runAs(u) { 
            Test.startTest();
                name = myOperengCaseDetailsController.getUserName();
            Test.stopTest();
        }       
        System.assertEquals(name, cc.FirstName + ' ' + cc.LastName);
    }
    /*
    @isTest static void testCanGetCaseEmails(){
        List<EmailMessage> emails;
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert acc;
        Contact cc = new Contact(
                FirstName = 'test',
                LastName = 'test',
                AccountId = acc.Id,
                Email = 'test@test.com.br'
        );
        insert cc;
        User u = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id,
                Email = cc.Email,
                FirstName = cc.FirstName,
                LastName = cc.LastName,
                ContactId = cc.Id,
                Username = cc.Email,
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0' 
        ); 
        insert u;
        Case c = New Case(
            Subject = 'test',
            Customer_Visible__c = TRUE,
            ContactId = cc.Id,
            AccountId = acc.Id
        );
        insert c;
        
        EmailMessage email = New EmailMessage(
            Subject = 'test',
            parentId = c.Id,
            FromAddress = 'test@test.com.br'
        );
        insert email;  
        
        System.runAs(u) {
        Test.startTest();
            emails = myOperengCaseDetailsController.getCaseEmails(c.Id);
        Test.stopTest();
        } 
        
        System.assert(emails.size() > 0);    
    }
	*/
}