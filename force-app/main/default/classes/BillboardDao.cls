/* Classe implementadora de SOBjectDAO para operações DML no objeto Billboard__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 07/01/2016
*/
public without sharing class BillboardDao {
    //Device_Type__c
    private static final BillboardDao instance = new BillboardDao();    
    
    private BillboardDao(){
    }    
    
    public static BillboardDao getInstance(){
        return instance;
    }
    
    public List<Billboard__c> listPublishedBillboard(){
        return database.query('Select ' + utils.getAllFields('Billboard__c') + ' FROM Billboard__c WHERE Publish_Status__c = \'Published\'');      
    }
    
    public List<Billboard__c> listById(String idBillboard)	{
        List<Billboard__c> listBillboard = database.query(' Select ' + utils.getAllFields('Billboard__c') + ' FROM Billboard__c WHERE id =\''+String.escapeSingleQuotes(idBillboard)+'\'');

        if(listBillboard.size() > 0){
            return listBillboard;
        }
        
        return null;        
    }
    
}