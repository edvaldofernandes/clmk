public without sharing class ContractLineItemDao {
    private static final ContractLineItemDao instance = new ContractLineItemDao();    
    
    private ContractLineItemDao(){
    }    
    
    public static ContractLineItemDao getInstance(){
        return instance;
    }
    
    public List<ContractLineItem> listBySetServiceContract(Set<string> setServiceContract){
        return [SELECT id, ServiceContractId, ServiceContract.Signature_Date__c, CreatedDate, ServiceContract.ContractNumber, 
                PricebookEntry.Product2.Name,PricebookEntry.Product2.Pardot_Status__c,PricebookEntry.Product2.Custom_Redirect_Pardot__c, Product_Type__c, PricebookEntry.Product2.Publication_status__c, Quantity, PricebookEntry.Product2.id, ServiceContract.Contract_Status__c
               FROM ContractLineItem
               Where ServiceContractId IN :setServiceContract];
    }
}