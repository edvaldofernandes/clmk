/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class which provides a service to clone a ServiceContract record. This service
* also clones related records of ContractLineItem, Service_Contract_Management__c
* and Related_Aircraft__c.
*
* NAME: JSCloneServiceContract.cls
* AUTHOR: LMdO                                                DATE: 21/12/2014
*
*******************************************************************************/

global with sharing class JSCloneServiceContract {
	
	private static final String CAMPOS = SObjectDescriber.getSObjectFieldsString( ServiceContract.sObjectType );
	private static final String CONTRACT_CAMPOS = SObjectDescriber.getSObjectFieldsString( ContractLineItem.sObjectType );
  private static final String MANAGEMENT_CAMPOS = SObjectDescriber.getSObjectFieldsString( Service_Contract_Management__c.sObjectType );
  private static final String AIRCRAFT_CAMPOS = SObjectDescriber.getSObjectFieldsString( Related_Aircraft__c.sObjectType );
  private static final String STATUS = 'Signed';
	
	webservice static String processar(String strServiceCotract, Boolean isConvert) {
		
		if ( strServiceCotract == null ) return 'Service Contract ID to clone informed. Not performed cloning.';
		
		List<ServiceContract> lstOldService = Database.query( 'SELECT ' + CAMPOS + ' FROM ServiceContract WHERE Id = \'' + strServiceCotract + '\'' );
		
		if (lstOldService.size() == 0) return 'Internal error. Service Contract not found. Contact your system administrator.';
       
    return clonarList( lstOldService[ 0 ], isConvert );
	}
    
    private static String clonarList( ServiceContract aServ, Boolean isConvert ) {
  
	    ServiceContract servNew = aServ.clone( false, true, false, false );
      if(servNew == Null) return 'Internal error. Not cloned the record.';
      
	    servNew.Contract_hierarchy__c = aServ.id;
	    servNew.DOCCON__c = Null;	   
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
      try {
      	String erro; 
        Database.SaveResult result = Database.insert( servNew, false );
        if( !result.isSuccess() ) {
        	for( Database.Error error : result.getErrors() ) {
            System.debug('[@@@]' + error.getMessage() + ' :: ' + error.getStatusCode() );
            erro = error.getMessage() + ' \\ ' + error.getStatusCode();
          }
          return erro;
        }
      } catch ( Exception e ) { return ( '##' + e.getDmlMessage(0) ); }
      
       
      list<ServiceContract> lstUpdateServNew = new list<ServiceContract>();
      
      if ( servNew.Contract_Status__c == STATUS )
      {
      // Clona os ContractLine vinculados.,
      String lqueryContract = 'SELECT ' + CONTRACT_CAMPOS + ' FROM ContractLineItem WHERE ServiceContractId = \'' + aServ.id + '\'';
      List<ContractLineItem> lstContractVelho = Database.query( lqueryContract );
    
      Map<Id, ContractLineItem> lstContractNovo = new Map<Id, ContractLineItem>();
    
      for ( ContractLineItem contract : lstContractVelho ) {
        ContractLineItem contractNovo = contract.clone( false, true, false, false );
        contractNovo.ServiceContractId = servNew.Id;
        if ( isConvert ) contractNovo.Status__c = 'Converted';
        lstContractNovo.put(contract.Id, contractNovo);
      }
      
      insert lstContractNovo.values();
    
      // Clona as Service Management vinculados.
      String lqueryManag = 'SELECT ' + MANAGEMENT_CAMPOS + ' FROM Service_Contract_Management__c WHERE Service_Contract__c = \'' + aServ.id + '\'';
      lqueryManag += ' OR Service_Contract_deliverable__c = \'' + aServ.id + '\'';
      List<Service_Contract_Management__c> lstManagVelha = Database.query( lqueryManag );
    
      List<Service_Contract_Management__c> lstManagNova = new List<Service_Contract_Management__c>();
    
      for ( Service_Contract_Management__c servManag : lstManagVelha ) {
        Service_Contract_Management__c managNova = servManag.clone( false, true, false, false );
        
        if(servManag.Service_Contract__c == aServ.id) {
        	managNova.Service_Contract__c = servNew.Id;
        }
       	if(servManag.Service_Contract_deliverable__c == aServ.id) { 
       		managNova.Service_Contract_deliverable__c = servNew.Id;
       	}
       	
       	ContractLineItem contractNovo = lstContractNovo.get(managNova.Contract_Line_Item_Deliverable__c);
       	if ( contractNovo != null ) managNova.Contract_Line_Item_Deliverable__c = contractNovo.Id;
       	
       	contractNovo = lstContractNovo.get(managNova.Service_Contract_line_item__c);
        if ( contractNovo != null ) managNova.Service_Contract_line_item__c = contractNovo.Id;
        
        lstManagNova.add(managNova);
      }
      
      // Clona as Related Aircraft vinculados.
      String lqueryAircraft = 'SELECT ' + AIRCRAFT_CAMPOS + ' FROM Related_Aircraft__c WHERE Service_Contract__c = \'' + aServ.id + '\'';
      List<Related_Aircraft__c> lstAircraftVelha = Database.query( lqueryAircraft );
    
      List<Related_Aircraft__c> lstAircraftNova = new List<Related_Aircraft__c>();
    
      for ( Related_Aircraft__c aircraft : lstAircraftVelha ) {
        Related_Aircraft__c aircraftNova = aircraft.clone( false, true, false, false );
        aircraftNova.Service_Contract__c = servNew.Id;
        
        lstAircraftNova.add(aircraftNova);
      }
      
     	servNew.Contract_Status__c = 'Draft';
      servNew.Signature_Date__c = null;
     	lstUpdateServNew.add(servNew);
      
      try {
        insert lstManagNova;
        insert lstAircraftNova;
        update lstUpdateServNew; 
      } catch ( Exception e ) { return ( '!!!!!' + e.getDmlMessage(0) ); }
      }
      
      return servNew.id;
    }
		
}