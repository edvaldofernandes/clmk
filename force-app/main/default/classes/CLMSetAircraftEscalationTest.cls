@isTest
public class CLMSetAircraftEscalationTest {
    
    static testMethod void setEscalation(){
        List<SObject> listToInsert = new List<SObject>(); 
        
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        listToInsert.add(productTest);

        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        listToInsert.add(accountTest);
        
        database.insert(listToInsert);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        //Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(new List<Agreement__c>{Agreement1});
        
        //create aircraft prices to be cloned
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2});
        
        //create escalation
        Escalation__c escalation1 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 50);
        escalation1.Escalation_Code__c = 'E1';
        escalation1.Rate_Effective_Date__c = Date.newInstance(2020, 10, 10);
        Escalation__c escalation2 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 35);
        escalation2.Rate_Effective_Date__c = Date.newInstance(2019, 11, 11);
        escalation2.Escalation_Code__c = 'E2';
        database.insert(new List<Escalation__c>{escalation1,escalation2});
        
        //create line item to be cloned
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft1.Contract_Aircraft_Number__c = 1;
        agreementAircraft1.Contractual_Delivery_Month__c = Date.newInstance(2020, 10, 10);
        agreementAircraft2.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft2.Contract_Aircraft_Number__c = 2;
        agreementAircraft2.Contractual_Delivery_Month__c = Date.newInstance(2020, 11, 11);
        agreementAircraft3.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft3.Contract_Aircraft_Number__c = 3;
        agreementAircraft3.Contractual_Delivery_Month__c = Date.newInstance(2020, 9, 9);
        database.insert(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3});
        
        Escalation__c escalation3 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 35);
        escalation3.Rate_Effective_Date__c = Date.newInstance(2020, 9, 9);
        escalation3.Escalation_Code__c = 'E3';
        database.insert(new List<Escalation__c>{escalation3});
        
        system.assert([SELECT id FROM Escalation__c].size() == 3);
        
        Agreement_Aircraft__c ac1 = [SELECT id, Escalation_Code__c, Contract_Aircraft_Number__c FROM Agreement_Aircraft__c WHERE Contract_Aircraft_Number__c = 1 LIMIT 1];
        Escalation__c esc1 = [SELECT id, Escalation_Code__c FROM Escalation__c WHERE Escalation_Code__c = 'E1' LIMIT 1];
        system.assert(ac1.Escalation_Code__c==esc1.Id);
        
        Agreement_Aircraft__c ac2 = [SELECT id, Escalation_Code__c, Contract_Aircraft_Number__c FROM Agreement_Aircraft__c WHERE Contract_Aircraft_Number__c = 2 LIMIT 1];
        Escalation__c esc2 = [SELECT id, Escalation_Code__c FROM Escalation__c WHERE Escalation_Code__c = 'E2' LIMIT 1];
        system.assert(ac2.Escalation_Code__c==null);
        
        esc2.Rate_Effective_Date__c = Date.newInstance(2020, 11, 11);
        database.update(new List<Escalation__c>{esc2});
        
        Agreement_Aircraft__c ac22 = [SELECT id, Escalation_Code__c, Contract_Aircraft_Number__c FROM Agreement_Aircraft__c WHERE Contract_Aircraft_Number__c = 2 LIMIT 1];
        Escalation__c esc22 = [SELECT id, Escalation_Code__c FROM Escalation__c WHERE Escalation_Code__c = 'E2' LIMIT 1];
        system.assert(ac22.Escalation_Code__c==esc22.Id);
        
        Agreement_Aircraft__c ac3 = [SELECT id, Escalation_Code__c, Contract_Aircraft_Number__c FROM Agreement_Aircraft__c WHERE Contract_Aircraft_Number__c = 3 LIMIT 1];
        Escalation__c esc3 = [SELECT id, Escalation_Code__c FROM Escalation__c WHERE Escalation_Code__c = 'E3' LIMIT 1];
        system.assert(ac3.Escalation_Code__c==esc3.Id);
        
        ac3.Contractual_Delivery_Month__c = Date.newInstance(2020, 10, 10);
		database.update(new List<Agreement_Aircraft__c>{ac3});
        
        Agreement_Aircraft__c ac33 = [SELECT id, Escalation_Code__c, Contract_Aircraft_Number__c FROM Agreement_Aircraft__c WHERE Contract_Aircraft_Number__c = 3 LIMIT 1];
        //system.assert(ac3.Escalation_Code__c==esc1.Id);
        
    }
}