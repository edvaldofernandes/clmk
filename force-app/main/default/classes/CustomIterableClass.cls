/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Mar-2017.
**/

global class CustomIterableClass implements Iterable<SObject>,Iterator<SObject>
{ 
   
    public List<SObject> sObjectList {get; set;} 
    public  Integer counter {get; set;} 
    public Iterator<SObject> iterator() { return this; }

   
    public CustomIterableClass()
    { 
        
        sObjectList = new List<SObject>();
        
        set<id> oppIds = new set<id>();
        set<id> leadIds = new set<id>();
        map<string,string> sObjectFieldsMap = new map<string,string>();
        map<string,string> sObjectFiltersMap = new map<string,string>();
        
        //Account
        sObjectFieldsMap.put('Account',populateEmailAndPhoneFieldsString('Account'));
        
        //Contact
        sObjectFieldsMap.put('Contact',populateEmailAndPhoneFieldsString('Contact'));

        //Agreement
        sObjectFieldsMap.put('Apttus__APTS_Agreement__c','Id');
       
        //Production Capability
        sObjectFieldsMap.put('DF_Production_Capability__c','Id'); 

        //SOB Snapshot
        sObjectFieldsMap.put('SOB_Snapshot__c','Id'); 

        //SOB Version
        sObjectFieldsMap.put('SOB_Version__c','Id'); 

        //Blue Sheet Competition
        sObjectFieldsMap.put('Blue_Sheet_Competition__c','Id'); 
        
        //Blue Sheet Task
        sObjectFieldsMap.put('BlueSheet_Task__c','Id'); 
        
        //Blue Sheet Contact
        sObjectFieldsMap.put('BlueSheet_Contact__c','Id'); 

        //Blue Sheet
        sObjectFieldsMap.put('BlueSheet__c','Id'); 

        //Customer Contact Report 
        sObjectFieldsMap.put('Customer_Contact_Report__c','Id'); 

        //Customer Finance Deal
        sObjectFieldsMap.put('Customer_Finance_Deal__c','Id'); 

        //Sales Finance and Leasing Companies Deal
        sObjectFieldsMap.put('Sales_Finance_and_Leasing_Companies_Deal__c','Id'); 

        //ACMI Solution        
        sObjectFieldsMap.put('ACMI_Solution__c','Id');  

        //ACMI Deal
        sObjectFieldsMap.put('ACMI_Deal__c','Id');  

        oppIds.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Used Aircraft Sales').getRecordTypeId());
        oppIds.add(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Aircraft Sales').getRecordTypeId());

        //Opportunity
        sObjectFieldsMap.put('Opportunity','Id');
        sObjectFiltersMap.put('Opportunity','RecordTypeId in : oppIds');           
        
        leadIds.add(Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Aircraft Prospecting').getRecordTypeId());
        leadIds.add(Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Used Aircraft Prospecting').getRecordTypeId());
        
        //Lead
        sObjectFieldsMap.put('Lead','Id');
        sObjectFiltersMap.put('Lead','RecordTypeId in : leadIds');          

        
        for(string sObjectKey : sObjectFieldsMap.keySet())
        {
            
            for(sObject sObjectTemp : database.query('SELECT ' + sObjectFieldsMap.get(sObjectKey) + ' FROM ' + sObjectKey + (sObjectFiltersMap.containsKey(sObjectKey) ? ' Where ' + sObjectFiltersMap.get(sObjectKey) : '')))
                sObjectList.add(sObjectTemp);
            
        }
        
        counter = 0; 
        
    }   
   
    
    
    private string populateEmailAndPhoneFieldsString(string p_sObjectName)
    {
        map<String,Schema.SobjectType> describeMap = Schema.getGlobalDescribe();
        map<String, Schema.SObjectField> fieldMap = describeMap.get(p_sObjectName).getDescribe().fields.getMap();
        Schema.SObjectField field;
        map<string,string> fieldTypeMap = new map<string,string>();
           
        string returnValue = '';
           
        for(string fieldName : fieldMap.keySet())
        {
            field = fieldMap.get(fieldName);
            if(string.ValueOf(field.getDescribe().getType()) == 'EMAIL' || string.ValueOf(field.getDescribe().getType()) == 'PHONE')
                returnValue +=  (!string.isEmpty(returnValue) ? ',' + fieldName : fieldName);
        }    
       
        return returnValue;
   }
   
   
   
    global boolean hasNext()
    { 
       return counter < sObjectList.size();
    }
       
    global SObject next()
    { 
       counter++; 
       return sObjectList[counter-1]; 
    } 
    
    
    
}