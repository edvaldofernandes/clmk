public with sharing class PlanningReportController {
    
    //wrapper class to hold all the planning report data
    public class StockCoresAndRepairsWrapper{
        @AuraEnabled public Stock_Info_FLL__c summaryRecord{get;set;}
        @AuraEnabled public List<Stock_Info_FLL__c> stockList{get;set;}
        @AuraEnabled public List<Core_Information__c> coresList{get;set;}
        @AuraEnabled public List<Repair_Information__c> repairsList{get;set;}
        @AuraEnabled public List<RESS_Info__c> ressList{get;set;}
    }

    @AuraEnabled
    public static List<StockCoresAndRepairsWrapper> getCompiledSearchResults(string searchCriteria, string regionAndSegment){
        if(searchCriteria!=null && !String.isBlank(searchCriteria)){
            searchCriteria=searchCriteria.trim();
        	List<Stock_Info_FLL__c> searchResults = getSummaryResultsByTopmostOrEcode(searchCriteria, regionAndSegment);
        	return compilePlanningReportData(searchResults);
        }
        return null;
    }
    
    @AuraEnabled
    public static List<StockCoresAndRepairsWrapper> getCompiledDataByRegion(string regionAndSegment){
        List<Stock_Info_FLL__c> top30 = getTop30Results(regionAndSegment);
        return compilePlanningReportData(top30);
    }
    
    //creates wrapper objects given a list of Stock_Info_FLL__c records
    private static List<StockCoresAndRepairsWrapper> compilePlanningReportData(List<Stock_Info_FLL__c> summaryRecords){
        if(!(summaryRecords == null || summaryRecords.isEmpty())){
            List<StockCoresAndRepairsWrapper> resultsList = new List<StockCoresAndRepairsWrapper>();
        	List<String> topMostStrings = new List<String>();									//list of top most ecodes to be used in queries
        	final string regionAndSegment = summaryRecords[0].Region_And_Segment__c; 			//set region and segment based on the first record
            
            //create a wrapper instance for each summary record and get the top most from each record
            for(Stock_Info_FLL__c record : summaryRecords){
                topMostStrings.add(record.Top_Most__c);
                StockCoresAndRepairsWrapper result = new StockCoresAndRepairsWrapper();
                result.summaryRecord = record;
                resultsList.add(result);
            }
            
            //get related info for all the summary records
            List<Stock_Info_FLL__c> stockInfos = getStockInfosByRegionAndSegment(topMostStrings, regionAndSegment);
            List<Repair_Information__c> repairs = getRepairInfosByRegionAndSegment(topMostStrings, regionAndSegment);
            List<Core_Information__c> cores = getCoreInfosByRegionAndSegment(topMostStrings, regionAndSegment.left(3), regionAndSegment.right(3)+'%');
            List<RESS_Info__c> resses = getRessInfosByRegionAndSegment(topMostStrings, regionAndSegment.left(3), regionAndSegment.right(3)+'%');
            
            //add related records to their wrapper object
            for(StockCoresAndRepairsWrapper result : resultsList){
                result.stockList = new List<Stock_Info_FLL__c>();
                for(Stock_Info_FLL__c stockInfo : stockInfos){
                    if(stockInfo.Top_Most__c == result.summaryRecord.Top_Most__c)
                        result.stockList.add(stockInfo);
                }
                result.repairsList = new List<Repair_Information__c>();
                for(Repair_Information__c repair : repairs){
                    if(repair.Topmost__c == result.summaryRecord.Top_Most__c)
                        result.repairsList.add(repair);
                }
                result.coresList = new List<Core_Information__c>();
                for(Core_Information__c core : cores){
                    if(core.Topmost__c == result.summaryRecord.Top_Most__c)
                        result.coresList.add(core);
                }
                result.ressList = new List<RESS_Info__c>();
                for(RESS_Info__c ress : resses){
                    if(ress.Topmost__c == result.summaryRecord.Top_Most__c)
                        result.ressList.add(ress);
                }
            }
            return resultsList;
        }
        return null;
    }
    
    private static List<Stock_Info_FLL__c> getSummaryResultsByTopmostOrEcode(string searchCriteria, string regionAndSegment){
        List<Stock_Info_FLL__c> searchResults = [SELECT Top_Most__c FROM Stock_Info_FLL__c WHERE Is_Summary_Record__c=false AND Region_and_Segment__c=:regionAndSegment AND (Top_Most__c=:searchCriteria OR Ecode__c=:searchCriteria OR Part_Number__r.Name=:searchCriteria)];
        if(searchResults != null && !searchResults.isEmpty()){
        	List<String> topMostStrings = new List<String>();
            for(Stock_Info_FLL__c result : searchResults){
            	topMostStrings.add(result.Top_Most__c);
            }
            if(regionAndSegment=='FLL COM'){
                return [SELECT Id, POOL_CUSTOMERS__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, TQA_145__c, TQA_170__c, TQA_190__c, FLL2_OH__c, US13_OH__c, 
                        FLL3_NEW_USED__c, FLL3_OVHL_0693__c, FLL3_0730_COMPLIANCE__c, FLL3_0684_EX__c, BNA_0660__c, FLL3_0785_LLP_COMPLETE__c, FLL3_0787_LLP_PEND__c, AT_REPAIR__c, 
                        FLL2_0646_UN__c, FLL3_0697_PEND_TEC__c, FLL3_0630_SCRAP__c, US13_5149_SCRAP__c, REPAIR_ADMIN_ORDERS__c, FLL3_BLKD__c, US13_BLKD__c, FLL3_QUAR__c, US13_QUAR__c, 
                        FLL3_0673_SHELF_LIFE_EXP__c, FLL3_D_S__c, US13_D_S__c, FLL3_TO_REP__c, US13_TO_REP__c, NON_REPAIRABLE__c, LLP__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, 
                        OSS_ALLOCATION__c, STOCK_MAX__c, OVERAGE_SHORTAGE__c, USAGE_12M__c, AVG_USAGE__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, 
                        CORES__c, QMS_OPEN__c, OPEN_TOS__c, OH_Total__c, Top_Most__c, Region_and_Segment__c, Part_Number_Name__c
                        FROM Stock_Info_FLL__c 
                        WHERE Is_Summary_Record__c=true AND Region_and_Segment__c=:regionAndSegment AND Top_Most__c IN :topMostStrings];
            }else if(regionAndSegment=='LBG COM'){
                return [SELECT Id, POOL_CUSTOMERS__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c, STOCK_MAX__c, 
                        OVERAGE_SHORTAGE__c, USAGE_12M__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, CORES__c, QMS_OPEN__c, OPEN_TOS__c, TQA__c,
                        LBG2__c, LBG3_New__c, LBG3_Used__c, LBG3_OVH__c, At_Repair_LBG2__c, At_Repair_LBG3__c, X0904_Scrap__c, X0910_BUs_Unserv__c,	X0941_OLD_Scrap_Custom__c, 
                        Blocked_LBG3__c, Blocked_LBG2__c, X0909_PartOut_KASI__c, X0921_Buyback__c, X0929_OLD_TOOL_TOT__c, X0930_OLD_Com_Quarantine__c, X0942_COM_EXCESS__c, 
                        X0957_BUS_AT_RS__c, X0980_COM_ENG_OGMA__c, X0950_COM_DC_JNB__c, X0913_BUs_In_Transit__c, OH_Total__c, Top_Most__c, Region_and_Segment__c, Part_Number_Name__c
                        FROM Stock_Info_FLL__c 
                        WHERE Is_Summary_Record__c=true AND Region_and_Segment__c=:regionAndSegment AND Top_Most__c IN :topMostStrings];
            }
        }
        return null;
    }
    
    private static List<Stock_Info_FLL__c> getTop30Results(string regionAndSegment){
        if(regionAndSegment=='FLL COM')
            return [Select Id, POOL_CUSTOMERS__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, TQA_145__c, TQA_170__c, TQA_190__c, FLL2_OH__c, US13_OH__c, 
                    FLL3_NEW_USED__c, FLL3_OVHL_0693__c, FLL3_0730_COMPLIANCE__c, FLL3_0684_EX__c, BNA_0660__c, FLL3_0785_LLP_COMPLETE__c, FLL3_0787_LLP_PEND__c, AT_REPAIR__c, 
                    FLL2_0646_UN__c, FLL3_0697_PEND_TEC__c, FLL3_0630_SCRAP__c, US13_5149_SCRAP__c, REPAIR_ADMIN_ORDERS__c, FLL3_BLKD__c, US13_BLKD__c, FLL3_QUAR__c, US13_QUAR__c, 
                    FLL3_0673_SHELF_LIFE_EXP__c, FLL3_D_S__c, US13_D_S__c, FLL3_TO_REP__c, US13_TO_REP__c, NON_REPAIRABLE__c, LLP__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, 
                    OSS_ALLOCATION__c, STOCK_MAX__c, OVERAGE_SHORTAGE__c, USAGE_12M__c, AVG_USAGE__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, 
                    CORES__c, QMS_OPEN__c, OPEN_TOS__c, OH_Total__c, Top_Most__c, Region_and_Segment__c, Part_Number_Name__c, Repairs_In_Transit__c, RESS_Count__c, Cores_In_Transit__c
                    FROM Stock_Info_FLL__c 
                    WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) AND OH_Total__c<2 AND Is_Summary_Record__c=true 
                    	AND Region_and_Segment__c=:regionAndSegment AND Part_Number__r.Hide_From_Planning_Report__c=False
                    ORDER BY USAGE_12M__c DESC
                    LIMIT 30];
       else if(regionAndSegment=='LBG COM')
            return [Select Id, POOL_CUSTOMERS__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c, STOCK_MAX__c, 
                    OVERAGE_SHORTAGE__c, USAGE_12M__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, CORES__c, QMS_OPEN__c, OPEN_TOS__c, TQA__c,
                    LBG2__c, LBG3_New__c, LBG3_Used__c, LBG3_OVH__c, At_Repair_LBG2__c, At_Repair_LBG3__c, X0904_Scrap__c, X0910_BUs_Unserv__c,	X0941_OLD_Scrap_Custom__c, 
                    Blocked_LBG3__c, Blocked_LBG2__c, X0909_PartOut_KASI__c, X0921_Buyback__c, X0929_OLD_TOOL_TOT__c, X0930_OLD_Com_Quarantine__c, X0942_COM_EXCESS__c, 
                    X0957_BUS_AT_RS__c, X0980_COM_ENG_OGMA__c, X0950_COM_DC_JNB__c, X0913_BUs_In_Transit__c, OH_Total__c, Top_Most__c, Region_and_Segment__c, Part_Number_Name__c,
                    Repairs_In_Transit__c, RESS_Count__c, Cores_In_Transit__c
                    FROM Stock_Info_FLL__c 
                    WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) AND OH_Total__c<2 AND Is_Summary_Record__c=true 
                    	AND Region_and_Segment__c=:regionAndSegment AND Part_Number__r.Hide_From_Planning_Report__c=False
                    ORDER BY USAGE_12M__c DESC
                    LIMIT 30];
        return null;
    }
    
    @TestVisible private static List<Stock_Info_FLL__c> getStockInfosByRegionAndSegment(List<string> topmosts, string regionAndSegment){
        if(regionAndSegment=='FLL COM')
            return [SELECT Id, POOL_CUSTOMERS__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, TQA_145__c, TQA_170__c, TQA_190__c, FLL2_OH__c, US13_OH__c,
                    FLL3_NEW_USED__c, FLL3_OVHL_0693__c, FLL3_0730_COMPLIANCE__c, FLL3_0684_EX__c, BNA_0660__c, FLL3_0785_LLP_COMPLETE__c, FLL3_0787_LLP_PEND__c, AT_REPAIR__c, 
                    FLL2_0646_UN__c, FLL3_0697_PEND_TEC__c, FLL3_0630_SCRAP__c, US13_5149_SCRAP__c, REPAIR_ADMIN_ORDERS__c, FLL3_BLKD__c, US13_BLKD__c, FLL3_QUAR__c, US13_QUAR__c, 
                    FLL3_0673_SHELF_LIFE_EXP__c, FLL3_D_S__c, US13_D_S__c, FLL3_TO_REP__c, US13_TO_REP__c, NON_REPAIRABLE__c, LLP__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, 
                    OSS_ALLOCATION__c, STOCK_MAX__c, OVERAGE_SHORTAGE__c, USAGE_12M__c, AVG_USAGE__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, 
                    CORES__c, QMS_OPEN__c, OPEN_TOS__c, OH_Total__c, Top_Most__c, Region_and_Segment__c, Part_Number_Name__c
                    FROM Stock_Info_FLL__c 
                    WHERE Is_Summary_Record__c=false AND Region_and_Segment__c=:regionAndSegment AND Top_Most__c IN :topmosts];
        else if(regionAndSegment=='LBG COM')
        	return [SELECT Id, POOL_CUSTOMERS__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c, STOCK_MAX__c, 
                    OVERAGE_SHORTAGE__c, USAGE_12M__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, CORES__c, QMS_OPEN__c, OPEN_TOS__c, TQA__c,
                    LBG2__c, LBG3_New__c, LBG3_Used__c, LBG3_OVH__c, At_Repair_LBG2__c, At_Repair_LBG3__c, X0904_Scrap__c, X0910_BUs_Unserv__c,	X0941_OLD_Scrap_Custom__c, 
                    Blocked_LBG3__c, Blocked_LBG2__c, X0909_PartOut_KASI__c, X0921_Buyback__c, X0929_OLD_TOOL_TOT__c, X0930_OLD_Com_Quarantine__c, X0942_COM_EXCESS__c, 
                    X0957_BUS_AT_RS__c, X0980_COM_ENG_OGMA__c, X0950_COM_DC_JNB__c, X0913_BUs_In_Transit__c, OH_Total__c, Top_Most__c, Region_and_Segment__c, Part_Number_Name__c
                    FROM Stock_Info_FLL__c 
                    WHERE Is_Summary_Record__c=false AND Region_and_Segment__c=:regionAndSegment AND Top_Most__c IN :topmosts];
        return null;
    }
    
    private static List<RESS_Info__c> getRessInfosByRegionAndSegment(List<string> topmosts, string region, string segment){
        return [SELECT Id, Topmost__c, Customer_Name__c, Part_Number__c, Part_Number__r.Name, RESS_ID__c, Sales_Order__r.SO__c, Sales_Order__r.Transfer_PO__c, Sales_Order__r.Id, rm_comments__c, Site__c
                FROM RESS_Info__c 
                WHERE Topmost__c =:topmosts AND Site__c=:region AND Segment__c LIKE :segment AND Customer_Name__c='EMBRAER SERVICES INC.' AND RESS_ID__c!=null 
                AND Tracking_Sent_Date_and_Time__c=null AND Alternate_Action_Date_and_Time__c=null 
                ORDER BY Site__c ASC];
    }
    
    private static List<Repair_Information__c> getRepairInfosByRegionAndSegment(List<string> topmosts, string regionAndSegment){
        return [SELECT Topmost__c, Actual_Repair_Station__c, eRepair_Queue__c, Repair_Contract_Terms__c, Repair_Customer__c, Repair_Ecode__c, Repair_Exchange_Prov__c, 
                Repair_Notification__c, Repair_Part_Number__c, Repair_Part_Number__r.Name, Repair_User_Notes__c, RM_TAT_At_Repair__c, SOD_Notes__c, TAT_At_Repair__c, AWB_From_Repair__c, AWB_From_Repair_Deliv_Date__c
                FROM Repair_Information__c WHERE Topmost__c IN :topmosts AND Site__c=:regionAndSegment];
    }
    
    private static List<Core_Information__c> getCoreInfosByRegionAndSegment(List<string> topmosts, string region, string segment){
        return [SELECT Id, Topmost__c, Category__c, Category_Aging__c, Customer_Name__c, Epool_status__c, Notification__c, Part_Number__c, Part_Number__r.Name, Problem_Description__c, QM_Number__c, 
                Responsible_Department__c, Total_Aging__c
                FROM Core_Information__c WHERE Topmost__c IN :topmosts AND Support_Plant__c=:region AND Segment__c LIKE :segment];
    }
}