@isTest(seeAllData=true)
public class PRC_Incoming_Email_PageTest{

    static testMethod void unitTest(){
    
        List<Case> casesIncoming = new List<Case>();
        
         Id idRecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId();
        
        Account fAccount = SObjectInstanceTest.createAccount(idRecordTypeAccount);
        fAccount.name = 'Embraer, SA';
        fAccount.RecordTypeId = idRecordTypeAccount;
        database.insert(fAccount);
        
        Aircraft__c fAircraft = SObjectInstanceTest.createAircraft();
        database.insert(fAircraft);
            
        Case fCase = SObjectInstanceTest.createCase();
        fCase.SuppliedEmail = 'teste@teste.com';
        fCase.Incoming_email_i__c = true;
        casesIncoming.add(fCase);
        database.insert(casesIncoming); 

        Test.startTest();        
        PRC_Incoming_Email pagetest = new PRC_Incoming_Email();
        pagetest.AnalyseCases();

        Test.stopTest();   
    }
    
}