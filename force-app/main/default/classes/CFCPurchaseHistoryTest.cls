@isTest
public class CFCPurchaseHistoryTest {
    static testMethod void myTestUnit()
    {
        Date myDate = Date.newInstance(2016, 3, 22);

        CFCPurchaseHistory.contractLineItemWrapper contractLineItemWrapper = new CFCPurchaseHistory.contractLineItemWrapper();
        contractLineItemWrapper.Name = 'teste';
        contractLineItemWrapper.ContractNumber = 'teste';
        contractLineItemWrapper.SignatureDate = myDate;
        contractLineItemWrapper.Quantity = 0;
        contractLineItemWrapper.CreatedDate = myDate;
        contractLineItemWrapper.ContractStatus = 'teste';
		contractLineItemWrapper.ProductName = 'teste';
        contractLineItemWrapper.ProductId = 'teste';
        contractLineItemWrapper.ProductPublicationStatus = 'teste';

        List<CFCPurchaseHistory.contractLineItemWrapper> contractLineList = new List<CFCPurchaseHistory.contractLineItemWrapper>();
        contractLineList.add(contractLineItemWrapper);
        
        CFCPurchaseHistory.serviceContractWrapper serviceContractWrapper = new CFCPurchaseHistory.serviceContractWrapper();
        serviceContractWrapper.ContractNumber = 'teste';
        serviceContractWrapper.SignatureDate = myDate;
        serviceContractWrapper.ContractStatus = 'teste';
        serviceContractWrapper.CreatedDate = myDate;
        serviceContractWrapper.ContractItemList = database.query(' Select Id FROM ContractLineItem');
                
		List<ServiceContract> pServiceContractList = new List<ServiceContract>();
        ServiceContract serviceContract = new ServiceContract();
        pServiceContractList.add(serviceContract);

		List<ContractLineItem> pContractItemList = new List<ContractLineItem>();
        ContractLineItem contractLineItem = new ContractLineItem();
        pContractItemList.add(contractLineItem);
            
        CFCPurchaseHistory controller = new CFCPurchaseHistory(pServiceContractList, pContractItemList);
        
        controller.serviceContractList.add(serviceContractWrapper);
        controller.contractItemList.add(contractLineItemWrapper);
        system.assert(controller != null); 
        
    }
}