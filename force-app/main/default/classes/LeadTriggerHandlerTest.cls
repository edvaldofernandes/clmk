/********************************************************************************\
*                               Embraer - 2015                                   *
*--------------------------------------------------------------------------------*
*                                                                                *
* Class for testing and covering the code of the class LeadTriggerHandler        *
*                                                                                *
* NAME:   LeadTriggerHandlerTest.cls                                             *
* AUTHOR: Bruno Severino                                CREATED DATE: 25/05/2015 *
*         Rodrigo Gimenes Rodrigues            LAST MODIFICATED DATE: 26/06/2015 *
*                                                                                *
\********************************************************************************/
@isTest
public class LeadTriggerHandlerTest {
    public static Contact contact;
    public static Contact contact2; 
    public static Campaign campaign;
    
    static testMethod void CONTACT_REGISTERED(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
			//add to Custom Settings Eventos
			addNewEvent(campaign.Id);     
            
            //add Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'bruno.severino@embraer.com.br';          
            
            Test.startTest();
            insert(lead);
            Test.stopTest();
        }
    } 
    
    static testMethod void LEAD_NOT_REGISTERED_FOR_EVENT(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
			//add to Custom Settings Eventos
			//addNewEvent(campaign.Id);     
            
            //add Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'bruno.severino@embraer.com.br';          
            
            Test.startTest();
            insert(lead);
            Test.stopTest();
        }
    } 
    
    static testMethod void DOUBLE_REGISTER(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
            //add to Custom Settings Eventos
			addNewEvent(campaign.Id);     
            
            //create Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'bruno.severino@embraer.com.br';
            
            insert(lead);
            Test.startTest();            
            lead.status = 'Not Invited';	//already registered and was Not Invited
            update(lead);              
            LeadTriggerHandler handler = new LeadTriggerHandler(true);
            System.AssertEquals(handler.IsTriggerContext,true);
            Test.stopTest();
            
            system.assert(lead.Id != null, 'Não Funcionou');
        }
    }
    
    static testMethod void CONTACT_NOT_INVITED(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
            //add to Custom Settings Eventos
			addNewEvent(campaign.Id);     
            
            //create Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'fabio.ramos@embraer.com.br';
            
            Test.startTest();
            insert(lead);
            lead.Paid_the_event__c = false;
            update(lead);
            delete(lead);
            undelete(lead);
            Test.stopTest();
        }
    } 
    
    static testMethod void LEAD_NOT_INVITED(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
            //add to Custom Settings Eventos
			addNewEvent(campaign.Id);     
            
            //create Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'bo.severino@gmail.com';
                        
            Test.startTest();
            insert(lead);
            LeadTriggerHandler handler = new LeadTriggerHandler(true);
            System.AssertEquals(handler.IsTriggerContext,true);
            Test.stopTest();
            
            system.assert(lead.Id != null, 'Não Funcionou');
        }
    } 
    
    static testMethod void LEAD_UNINVITED(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
            //add to Custom Settings Eventos
			addNewEvent(campaign.Id);     
            
            //create Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'bo.severino@gmail.com';
            
            insert(lead);
            Test.startTest();            
            lead.status = 'Uninvited';
            update(lead);              
            LeadTriggerHandler handler = new LeadTriggerHandler(true);
            System.AssertEquals(handler.IsTriggerContext,true);
            Test.stopTest();
            
            system.assert(lead.Id != null, 'Não Funcionou');
        }
    }
    
    static testMethod void CONTACT_PRESENT(){               
        //Creating Users to Lead Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System 
        insert(user1);            
        System.RunAs(user1)
        {
            //create a baseforTest
            createBaseForTeste();
            
            //add to Custom Settings Eventos
			addNewEvent(campaign.Id);     
            
            //create Lead
            Lead lead = createLead(campaign.Id);
            lead.Email = 'bruno.severino@embraer.com.br';
           
            insert(lead);
            Test.startTest();            
            lead.status = 'Present';
            update(lead);              
            LeadTriggerHandler handler = new LeadTriggerHandler(true);
            System.AssertEquals(handler.IsTriggerContext,true);
            Test.stopTest();
            
            system.assert(lead.Id != null, 'Não Funcionou');
        }
    }
    
    public static void createBaseForTeste(){
        contact = createContact();
        contact.Email = 'bruno.severino@embraer.com.br';
        insert(contact);
        
        contact2 = createContact();
        contact2.Salutation = 'Mr.';
        contact2.FirstName = 'Fabio';
        contact2.LastName = 'Ramos';
        contact2.Email = 'fabio.ramos@embraer.com.br';
        insert(contact2);
        
        //create Campaign
        campaign = createCampaign();
        insert(campaign);
        
        //add Contact in campaign member
        addCampaignMember(campaign.Id, contact.Id,'Added Only');
        //addCampaignMember(campaign.Id, contact2.Id,'Not Invited');
    }
    
    public static void addCampaignMember(Id campaignId, Id contactId,string status)
    {
        CampaignMember campaignMember_temp = new CampaignMember();
        campaignMember_temp.CampaignId = campaignId;
        campaignMember_temp.ContactId = contactId;
        campaignMember_temp.Status = status;
        insert(campaignMember_temp);
    }
    Public static void addNewEvent(String IDcampanha){
        Eventos__c evento = new Eventos__c();
        evento.Name = IDcampanha;
        insert(evento);
    }
    Public static Campaign createCampaign(){
        Campaign campaign_temp = new Campaign();
        campaign_temp.Name = 'EOC WW 15';
        campaign_temp.Status = 'Planned';
        campaign_temp.IsActive = true;
        campaign_temp.Type = 'Seminar / Conference';
        return campaign_temp;
    }
    public static Contact createContact(){
        Contact contact_temp = new Contact();
        contact_temp.Title = 'IT Analyst';
        contact_temp.Salutation = 'Mr.';
        contact_temp.FirstName = 'Bruno';
        contact_temp.LastName = 'Severino';
        contact_temp.Gender__c = 'Male';
        contact_temp.Contact_Status__c ='Active';
        contact_temp.AccountId = '001i000000uhklI';
        return contact_temp;
    }
    public static Lead createLead(Id campaignId){
        Lead lead_temp = new Lead();                    
        lead_temp.Salutation = 'Mr.';
        lead_temp.FirstName = 'Bruno';
        lead_temp.LastName = 'Severino';
        lead_temp.User_Type__c =  'Embraer';
        lead_temp.phone = '+551239270000';
        lead_temp.street =  'Rua A, 123';
        lead_temp.company =  'Embraer São José dos Campos';
        lead_temp.title =  'IT Analyst ';
        lead_temp.Description = 'blood A+, no alergy';
        lead_temp.Functions_attending__c = 'Welcome Cocktail; Special Dinner; Techinical; MCW; MCW Dinner';
        lead_temp.events_participate__c = campaignId;
        lead_temp.Companion__c = 'Fabio Ramos';
        lead_temp.Paid_the_event__c = true;
        return lead_temp;
    }
}