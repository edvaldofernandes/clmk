/**
* @description: This class represents the service layer of Follow-ups(FUPs). 
* For more information, visit: https://martinfowler.com/eaaCatalog/serviceLayer.html
**/
public class FO_FupService {
    public final static Integer FUP_DIGEST_INTERVAL_IN_MONTHS = 1;
    
    
    /**
    * @description Returns a published follow-up by its Id. Throws an exception
    * otherwise.
    **/
    public static Fup__c getPublishedFupById(Id fupId){
        if(FO_FupService.isPublished(fupId)){
            return FO_FupRepository.getById(fupId);
        }else{
            throw new FlightOpsException('This Follow-up is not Published');    
        }
    }
    
    /**
    * @description Return true if a follow-up is published, false otherwise.
    **/
    public static Boolean isPublished(Id fupId){
        String status = FO_FupRepository.getById(fupId).Publish_Status__c;
        
        return status == 'Published';
    }
    
    /**
    * @description Returns all follow-up updates in the last
    * N months.
    **/
    public static List<Fup_Update__c> getUpdatesForDigestByContact(Id customerId){
        Contact customer = FO_ContactRepository.getContactById(customerId);
        List<String> aircraftFamilies = new List<String>();
        if(customer.FlightOps_FUP_Report__c){
            aircraftFamilies.add('EJet');
        }
        if(customer.FlightOps_ERJ_WW_FUP_Report__c){
            aircraftFamilies.add('ERJ');
        }
        if(customer.FlightOps_E2_FUP_Report__c){
            aircraftFamilies.add('E2');
        } 
        
        List<FUP_Update__c> updates;
	
         
      
            updates = FO_FupRepository.getLastNMonthsPublicUpdatesByFleetFamily(
            FUP_DIGEST_INTERVAL_IN_MONTHS,
            aircraftFamilies
        	);
   
        return updates;
    }
    
    public static List<Fup__c> getNewsForDigestByContact(Id customerId){
        Contact customer = FO_ContactRepository.getContactById(customerId);
        List<String> aircraftFamilies = new List<String>();
        if(customer.FlightOps_FUP_Report__c){
            aircraftFamilies.add('EJet');
        }
        if(customer.FlightOps_ERJ_WW_FUP_Report__c){
            aircraftFamilies.add('ERJ');
        }
        if(customer.FlightOps_E2_FUP_Report__c){
            aircraftFamilies.add('E2');
        } 
        
       
		List<FUP__c> news;
        
     
            news = FO_FupRepository.getLastNMonthsPublicNewsByFleetFamily(
            FUP_DIGEST_INTERVAL_IN_MONTHS,
            aircraftFamilies
        	);
        
        return news;
    }
    
    /**
    * @description Send an e-mail with all recent published updates to all contacts
    * who wnats to receive it.
    **/
    public static void sendFupDigestToContactsWhoWantsToReceiveIt(){
        Integer batchSize = 25; //Approximate number of emails that cause no problem.

        FO_FupDigestSender sender = new FO_FupDigestSender();
		Id batchId = Database.executeBatch(sender, 25);
    }
    
    /**
    * @description converts a fup update list into a map where the keys are their
    * parent fups.
    **/
    public static Map<FUP__c, List<FUP_Update__c>> buildFupUpdateMap(List<FUP_Update__c> updates){
        Map<FUP__c, List<FUP_Update__c>> updateMap = new Map<FUP__c, List<FUP_Update__c>>();
        for(FUP_Update__c u : updates){
            FUP__c fup = FO_FupRepository.getById(u.FUP__c);            
            List<FUP_Update__c> fupUpdates = new List<FUP_Update__c>();
            
            if(updateMap.containsKey(fup)){
                fupUpdates = updateMap.get(fup);
            }
            fupUpdates.add(u);
            updateMap.put(fup, fupUpdates);
        }
        return updateMap;
    }
    
    public static Map<FUP__c, List<FUP__c>> buildFupNewMap(List<FUP__c> updates){
        Map<FUP__c, List<FUP__c>> updateMap = new Map<FUP__c, List<FUP__c>>();
        for(FUP__c u : updates){
            FUP__c fup = FO_FupRepository.getById(u.Id);            
            List<FUP__c> fupUpdates = new List<FUP__c>();
            
            if(updateMap.containsKey(fup)){
                fupUpdates = updateMap.get(fup);
            }
            fupUpdates.add(u);
            updateMap.put(fup, fupUpdates);
        }
        return updateMap;
    }
    
    public static Map<Id, List<FUP_Update__c>> getPublishedFupsWithUpdatesByAircraftFamily(List<String> aircraftFamilies){
        List<FUP__c> publishedFups = FO_FupRepository.getPublishedFupsByAircraftFamily(aircraftFamilies);
        List<Id> publishedFupsIds = new List<Id>(new Map<Id, FUP__c>(publishedFups).keySet());
        List<FUP_Update__c> updates = FO_FupRepository.getPublicUpdatesByFup(publishedFupsIds);
        Map<Id, List<FUP_Update__c>> updateMap = new Map<Id, List<FUP_Update__c>>();
        
        
        for(FUP__c fup : publishedFups){
            updateMap.put(fup.Id, new List<FUP_Update__c>());
        }
        
        for(FUP_Update__c u : updates){
            List<FUP_Update__c> fupUpdates = updateMap.get(u.FUP__c);
            fupUpdates.add(u);
            updateMap.put(u.FUP__c, fupUpdates);          
        }
        return updateMap;
    } 
}