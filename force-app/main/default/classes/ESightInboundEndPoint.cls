global with sharing class ESightInboundEndPoint {
    
    webservice static void execute(List<ESightSrcr> eSightSrcrList){
        
        if(eSightSrcrList == null)
            return;
        
        ESightSrcrService eSightSrcrService = new ESightSrcrService(eSightSrcrList);
        eSightSrcrService.parseToObject();
    }

}