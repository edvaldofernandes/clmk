/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ProductListController
*
* NAME: ProductListControllerTest.cls
* AUTHOR: RLdO                                                DATE: 23/12/2014
*******************************************************************************/
@isTest
private class ProductListControllerTest
{
  static testMethod void myUnitTest()
  {
    Product2 produto = SObjectInstanceTest.createProduct2();
    produto.Name = 'Test Embraer';
    Database.insert(produto);

    test.startTest();
    Test.setCurrentPage(system.Page.ProductList);
    ProductListController controlador = new ProductListController();
    controlador.searchKey = 'Test';

    controlador.fetchProducts();
    test.stopTest();

    system.assert(controlador.LstProducts != null && !controlador.LstProducts.isEmpty(), 'Unable to fetch product!');
  }
}