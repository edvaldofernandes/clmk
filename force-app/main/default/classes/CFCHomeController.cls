public with sharing class CFCHomeController{
    
    public User userCFC {get; set;}
    
    public Map<String,Billboard__c> mapBillboardImages {get;set;}
    public Map<String,Billboard__c> mapBillboardImagesTablet {get;set;}
    public Map<String,Billboard__c> mapBillboardImagesMobile {get;set;}
    
    public List<Product2> lstOthers {get; set;}
    
    public Set<String> setFleetType {get; set;}
    public Map<String,List<Product2>> mapProducts {get; set;}
    
    public Map<String, String> mapThumbnailsRelatedProducts {get; set;}
    public Map<String, String> mapThumbnailsOthersProducts {get; set;}
    
    public Map<String, String> mapBannerBillboard {get; set;}
    public Map<String, String> mapBannerBillboardTablet {get; set;}
    public Map<String, String> mapBannerBillboardMobile {get; set;}
    
    List<Billboard__c> lstBillboard {get; set;}
    
    public Boolean showOthers{get; set;}
    public Boolean isShowBillboardDesktop {get;set;}
    public Boolean isShowBillboardTablet {get;set;}
    public Boolean isShowBillboardMobile {get;set;}
    
    public String intervalBillboard {get;set;}
    public Boolean isPardotActive {get;set;}
    
    public CFCHomeController(){        
        try{
            userCFC = UserDAO.getInstance().getUserById(UserInfo.getUserId());
            system.debug('CFCHomeController.userCFC >>> ' + userCFC);
            
            VerifyPardot();
            GenerateIntervalBillboard();
            GenerateBillboard();
            GenerateViewProducts();
        } catch(Exception ex){
            system.debug('ERROR >>> ' + ex + ' line number >>> ' + ex.getLineNumber());
        }
    }
    
    public void VerifyPardot(){
        system.debug('CFCHomeController.GenerateViewProducts.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCHomeController.GenerateViewProducts.isPardotActive >>> ' + isPardotActive);
    }
    
    public void GenerateIntervalBillboard(){
        system.debug('CFCHomeController.GenerateViewProducts.GenerateIntervalBillboard()');
        intervalBillboard = '3000';
        
        CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults();
        if(cs.Billboard_Time__c != 0 && cs.Billboard_Time__c != null){
            system.debug('CFCHomeController.GenerateViewProducts.cs.Billboard_Time__c >>> ' + cs.Billboard_Time__c);
            intervalBillboard = string.valueOf(cs.Billboard_Time__c * 1000);
        }
        
        system.debug('CFCHomeController.GenerateViewProducts.intervalBillboard >>> ' + intervalBillboard);
    }
    
    public void GenerateBillboard(){
        system.debug('CFCHomeController.GenerateViewProducts.GenerateBillboard()');    
        
        lstBillboard = BillboardDao.getInstance().listPublishedBillboard();
        system.debug('CFCHomeController.GenerateBillboard.lstBillboard >>> ' + lstBillboard);
        
        mapBillboardImages = new Map<String,Billboard__c>();
		mapBillboardImagesTablet = new Map<String,Billboard__c>();
        mapBillboardImagesMobile = new Map<String,Billboard__c>();
        
        mapBannerBillboard = new Map<String, String>();
		mapBannerBillboardTablet = new Map<String, String>();
    	mapBannerBillboardMobile = new Map<String, String>();

        GenerateBannerBillboard(lstBillboard);
    }
    
    public void GenerateViewProducts(){
        system.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts()');
        system.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.userCFC.Contact.Account.id >>> ' + userCFC.Contact.Account.id);
        List<Aircraft__c> lstAircraft = new List<Aircraft__c>();
        if(userCFC.Contact.Account.id != null){
            lstAircraft = AircraftDao.getInstance().listByOwnerCId(userCFC.Contact.Account.id);        
        }
		system.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.lstAircraft >>> ' + lstAircraft);
        
        setFleetType = new Set<String>();
        for(Aircraft__c aircraft : lstAircraft){
            setFleetType.add(aircraft.Fleet_Type__c);
        }
        system.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.setFleetType >>> ' + setFleetType);
        
        List<Product2> lstProducts = new List<Product2>();
        if(setFleetType.size() > 0){
            lstProducts = Product2Dao.getInstance().listPublishedBySetApplicability(setFleetType);
        }        
        System.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.lstProducts >>> ' + lstProducts);
        System.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.lstProducts.size() >>> ' + lstProducts.size());    
        
        mapProducts = new Map<String, List<Product2>>();        
        for(String category : setFleetType)
        {
            List<Product2> prodCat = new List<Product2>();
            for(Product2 prod : lstProducts)
            {
                if(prod.Applicability__c.contains(category))
                {
                    prodCat.add(prod);
                }
            }

            if(prodCat.size() > 0){
              mapProducts.put(category, prodCat);  
            }            
        }
        System.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.mapProducts >>> ' + mapProducts);
       	
        lstOthers = new List<Product2>(); 
        lstOthers = Product2Dao.getInstance().listPublishedByNotSetApplicability(setFleetType);
        System.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.lstTempOthers >>> ' + lstOthers);
        System.debug('CFCHomeController.GenerateViewProducts.GenerateViewProducts.lstTempOthers.size() >>> ' + lstOthers.size());
        
        if(lstProducts.size() > 0){
            GenerateThumbnailRelatedProducts(lstProducts);
        }
        
        if(lstOthers.size() > 0){
            GenerateThumbnailOthersProducts(lstOthers);        
            showOthers =  true;
        } else {
            showOthers =  false;
        }
        
    }
    
    public void GenerateThumbnailRelatedProducts(List<Product2> lstProducts){
        System.debug('CFCHomeController.GenerateThumbnailRelatedProducts()');
        
        mapThumbnailsRelatedProducts = new Map<String, String>();        
        Set<String> setIdProducts = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_home');
        
        //creating a set of products, just to ensure that there is not equals id
        for(Product2 item : lstProducts){
            setIdProducts.add(item.id);
        }
        
        //pass to a map the ids
        for(String item : setIdProducts){
            mapThumbnailsRelatedProducts.put(item, 'no image');
        }
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetProductIdAndRecordTypeId(setIdProducts, rType.Id);
        System.debug('CFCHomeController.GenerateThumbnailRelatedProducts.lstAttch >>> ' + lstAttch);        
        
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            try{
                mapThumbnailsRelatedProducts.put(lstAttch.get(i).Product__c, lstAttch.get(i).Attachments[0].id);    
            } catch(Exception ex){
                mapThumbnailsRelatedProducts.put(lstAttch.get(i).Product__c, 'no image');    
            }
        }
        
        System.debug('CFCHomeController.GenerateThumbnailRelatedProducts.mapThumbnailsRelatedProducts >>> ' + mapThumbnailsRelatedProducts);        
    }
    
    public void GenerateThumbnailOthersProducts(List<Product2> lstOthers){
        System.debug('CFCHomeController.GenerateThumbnailOthersProducts()');
        
        mapThumbnailsOthersProducts = new Map<String, String>();        
        Set<String> setIdProducts = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_home');
        
        //creating a set of products, just to ensure that there is not equals id
        for(Product2 item : lstOthers){
            setIdProducts.add(item.id);
        }
        
        //pass to a map the ids
        for(String item : setIdProducts){
            mapThumbnailsOthersProducts.put(item, 'no image');
        }
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetProductIdAndRecordTypeId(setIdProducts, rType.Id);
        
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            try{
                mapThumbnailsOthersProducts.put(lstAttch.get(i).Product__c, lstAttch.get(i).Attachments[0].id);
            }catch(Exception ex){
                mapThumbnailsOthersProducts.put(lstAttch.get(i).Product__c, 'no image');
            }            
        }
        
        System.debug('CFCHomeController.GenerateThumbnailRelatedProducts.mapThumbnailsOthersProducts >>> ' + mapThumbnailsOthersProducts);
    }
    
    
    public static String getLicense(){
        String profileId = UserInfo.getProfileId();
        Profile profile = [select Name, Id, UserLicenseId from Profile where Id = :profileId];
        UserLicense userLicense = [select Name, Id from UserLicense where Id = :profile.UserLicenseId];
        return userLicense.Name;
        
    }
    
    
    public void GenerateBannerBillboard(List<Billboard__c> lstBillboard){
        System.debug('CFCHomeController.GenerateBannerBillboard()');
        
        isShowBillboardDesktop = false;
        isShowBillboardTablet = false;
        isShowBillboardMobile = false;
        
        Set<String> setIdBillboard = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Billboards');
        System.debug('CFCHomeController.GenerateBannerBillboard.rType >>> ' + rType.Id);
        
        //creating a set of products, just to ensure that there is not equals id
        for(Billboard__c item : lstBillboard){
            setIdBillboard.add(item.id);
            mapBillboardImages.put(item.Id, item);
        }
        System.debug('CFCHomeController.GenerateBannerBillboard.setIdBillboard >>> ' + setIdBillboard);
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetBillboardIdAndRecordTypeId(setIdBillboard, rType.Id);
        System.debug('CFCHomeController.GenerateBannerBillboard.lstAttch >>> ' + lstAttch);
        System.debug('CFCHomeController.GenerateBannerBillboard.lstAttch.size() >>> ' + lstAttch.size());
        
         
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            if(lstAttch.get(i).Attachments.size() > 0){ //set image billboard in your map by device
                if(lstAttch.get(i).Device_Type__c == 'Mobile'){
                    mapBannerBillboardMobile.put(lstAttch.get(i).Billboard__c, lstAttch.get(i).Attachments[0].id);
                } else if (lstAttch.get(i).Device_Type__c == 'Tablet'){
                    mapBannerBillboardTablet.put(lstAttch.get(i).Billboard__c, lstAttch.get(i).Attachments[0].id);
                } else{
                    mapBannerBillboard.put(lstAttch.get(i).Billboard__c, lstAttch.get(i).Attachments[0].id);
                }
            } else { //remove billboard without images
                system.debug('item >>> NULL');
                if(mapBillboardImages.containsKey(lstAttch.get(i).Billboard__c)){
                    mapBillboardImages.remove(lstAttch.get(i).Billboard__c);
                }
            }            
        }
        
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboard >>> ' + mapBannerBillboard);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboard.size() >>> ' + mapBannerBillboard.size());
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboardMobile >>> ' + mapBannerBillboardMobile);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboardMobile.size() >>> ' + mapBannerBillboardMobile.size());
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboardTablet >>> ' + mapBannerBillboardTablet);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboardTablet.size() >>> ' + mapBannerBillboardTablet.size());
        
        //fill map billboard by device according with map image by device
        for(Billboard__c item : lstBillboard){
            if(mapBannerBillboardTablet.containsKey(item.Id)){
                mapBillboardImagesTablet.put(item.Id, item);
            }
            
            if(mapBannerBillboardMobile.containsKey(item.Id)){
                mapBillboardImagesMobile.put(item.Id, item);
            }
        }
        
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImages >>> ' + mapBillboardImages);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImages.size() >>> ' + mapBillboardImages.size());
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImagesTablet >>> ' + mapBillboardImagesTablet);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImagesTablet.size() >>> ' + mapBillboardImagesTablet.size());
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImagesMobile >>> ' + mapBillboardImagesMobile);   
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImagesMobile.size() >>> ' + mapBillboardImagesMobile.size());
        
        //prevent crash in case of billboard published but without images
        isShowBillboardDesktop = mapBannerBillboard.size() > 0 ? true : false;
        isShowBillboardTablet = mapBannerBillboardTablet.size() > 0 ? true : false;
        isShowBillboardMobile = mapBannerBillboardMobile.size() > 0 ? true : false;
    }
    
}