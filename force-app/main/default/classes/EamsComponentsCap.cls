public class EamsComponentsCap {
    
    public List<Components_Capability__c> cl{get; set;}
    
    public EamsComponentsCap()
    {
        cl = new List<Components_Capability__c>();
        for (Components_Capability__c mcl : [SELECT name, 
                                                    Add_Date__c,
                                                    Aircraft_Type__c,
                                                    ATA__c,Description__c,
                                                    Insp__c,
                                                    Manufacture__c, 
                                                    Repair__c, 
                                                    Shop__c  
                                             FROM Components_Capability__c ])
        {
            cl.add(mcl);
        }
    }
}