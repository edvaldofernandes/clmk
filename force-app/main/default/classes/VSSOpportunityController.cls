public with sharing class VSSOpportunityController
{


public Integer APC_1 {get; set;}
public Integer APC_2 {get; set;}
public Integer APC_3 {get; set;}
public Integer APC_4 {get; set;}
public Integer APC_5 {get; set;}
public Integer APC_6 {get; set;}
public Integer CHI_1 {get; set;}
public Integer CHI_2 {get; set;}
public Integer CHI_3 {get; set;}
public Integer CHI_4 {get; set;}
public Integer CHI_5 {get; set;}
public Integer CHI_6 {get; set;}
public Integer EUC_1 {get; set;}
public Integer EUC_2 {get; set;}
public Integer EUC_3 {get; set;}
public Integer EUC_4 {get; set;}
public Integer EUC_5 {get; set;}
public Integer EUC_6 {get; set;}
public Integer MEA_1 {get; set;}
public Integer MEA_2 {get; set;}
public Integer MEA_3 {get; set;}
public Integer MEA_4 {get; set;}
public Integer MEA_5 {get; set;}
public Integer MEA_6 {get; set;}
public Integer SJK_1 {get; set;}
public Integer SJK_2 {get; set;}
public Integer SJK_3 {get; set;}
public Integer SJK_4 {get; set;}
public Integer SJK_5 {get; set;}
public Integer SJK_6 {get; set;}
public Integer LAT_1 {get; set;}
public Integer LAT_2 {get; set;}
public Integer LAT_3 {get; set;}
public Integer LAT_4 {get; set;}
public Integer LAT_5 {get; set;}
public Integer LAT_6 {get; set;}
public Integer NAM_1 {get; set;}
public Integer NAM_2 {get; set;}
public Integer NAM_3 {get; set;}
public Integer NAM_4 {get; set;}
public Integer NAM_5 {get; set;}
public Integer NAM_6 {get; set;}



public VSSOpportunityController(ApexPages.StandardController stdController)
{

APC_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
APC_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
APC_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
APC_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
APC_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
APC_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];
CHI_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
CHI_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
CHI_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
CHI_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
CHI_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
CHI_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];
EUC_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
EUC_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
EUC_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
EUC_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
EUC_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
EUC_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];
MEA_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
MEA_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
MEA_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
MEA_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
MEA_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
MEA_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];
SJK_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
SJK_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
SJK_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
SJK_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
SJK_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
SJK_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];
LAT_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
LAT_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
LAT_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
LAT_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
LAT_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
LAT_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];
NAM_1 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true];
NAM_2 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true];
NAM_3 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true];
NAM_4 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true];
NAM_5 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true];
NAM_6 = [SELECT count() FROM  Opportunity  WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true];




}



List<Opportunity> deals_APC_1; public List<Opportunity> getDeals_APC_1()    { if(deals_APC_1 == null) deals_APC_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_APC_1;}
List<Opportunity> deals_APC_2; public List<Opportunity> getDeals_APC_2()    { if(deals_APC_2 == null) deals_APC_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_APC_2;}
List<Opportunity> deals_APC_3; public List<Opportunity> getDeals_APC_3()    { if(deals_APC_3 == null) deals_APC_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_APC_3;}
List<Opportunity> deals_APC_4; public List<Opportunity> getDeals_APC_4()    { if(deals_APC_4 == null) deals_APC_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_APC_4;}
List<Opportunity> deals_APC_5; public List<Opportunity> getDeals_APC_5()    { if(deals_APC_5 == null) deals_APC_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_APC_5;}
List<Opportunity> deals_APC_6; public List<Opportunity> getDeals_APC_6()    { if(deals_APC_6 == null) deals_APC_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Asia Pacific' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_APC_6;}
List<Opportunity> deals_CHI_1; public List<Opportunity> getDeals_CHI_1()    { if(deals_CHI_1 == null) deals_CHI_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_CHI_1;}
List<Opportunity> deals_CHI_2; public List<Opportunity> getDeals_CHI_2()    { if(deals_CHI_2 == null) deals_CHI_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_CHI_2;}
List<Opportunity> deals_CHI_3; public List<Opportunity> getDeals_CHI_3()    { if(deals_CHI_3 == null) deals_CHI_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_CHI_3;}
List<Opportunity> deals_CHI_4; public List<Opportunity> getDeals_CHI_4()    { if(deals_CHI_4 == null) deals_CHI_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_CHI_4;}
List<Opportunity> deals_CHI_5; public List<Opportunity> getDeals_CHI_5()    { if(deals_CHI_5 == null) deals_CHI_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_CHI_5;}
List<Opportunity> deals_CHI_6; public List<Opportunity> getDeals_CHI_6()    { if(deals_CHI_6 == null) deals_CHI_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer China' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_CHI_6;}
List<Opportunity> deals_EUC_1; public List<Opportunity> getDeals_EUC_1()    { if(deals_EUC_1 == null) deals_EUC_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_EUC_1;}
List<Opportunity> deals_EUC_2; public List<Opportunity> getDeals_EUC_2()    { if(deals_EUC_2 == null) deals_EUC_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_EUC_2;}
List<Opportunity> deals_EUC_3; public List<Opportunity> getDeals_EUC_3()    { if(deals_EUC_3 == null) deals_EUC_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_EUC_3;}
List<Opportunity> deals_EUC_4; public List<Opportunity> getDeals_EUC_4()    { if(deals_EUC_4 == null) deals_EUC_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_EUC_4;}
List<Opportunity> deals_EUC_5; public List<Opportunity> getDeals_EUC_5()    { if(deals_EUC_5 == null) deals_EUC_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_EUC_5;}
List<Opportunity> deals_EUC_6; public List<Opportunity> getDeals_EUC_6()    { if(deals_EUC_6 == null) deals_EUC_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Europe and Central Asia' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_EUC_6;}
List<Opportunity> deals_MEA_1; public List<Opportunity> getDeals_MEA_1()    { if(deals_MEA_1 == null) deals_MEA_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_MEA_1;}
List<Opportunity> deals_MEA_2; public List<Opportunity> getDeals_MEA_2()    { if(deals_MEA_2 == null) deals_MEA_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_MEA_2;}
List<Opportunity> deals_MEA_3; public List<Opportunity> getDeals_MEA_3()    { if(deals_MEA_3 == null) deals_MEA_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_MEA_3;}
List<Opportunity> deals_MEA_4; public List<Opportunity> getDeals_MEA_4()    { if(deals_MEA_4 == null) deals_MEA_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_MEA_4;}
List<Opportunity> deals_MEA_5; public List<Opportunity> getDeals_MEA_5()    { if(deals_MEA_5 == null) deals_MEA_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_MEA_5;}
List<Opportunity> deals_MEA_6; public List<Opportunity> getDeals_MEA_6()    { if(deals_MEA_6 == null) deals_MEA_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Middle East & Africa' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_MEA_6;}
List<Opportunity> deals_SJK_1; public List<Opportunity> getDeals_SJK_1()    { if(deals_SJK_1 == null) deals_SJK_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_SJK_1;}
List<Opportunity> deals_SJK_2; public List<Opportunity> getDeals_SJK_2()    { if(deals_SJK_2 == null) deals_SJK_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_SJK_2;}
List<Opportunity> deals_SJK_3; public List<Opportunity> getDeals_SJK_3()    { if(deals_SJK_3 == null) deals_SJK_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_SJK_3;}
List<Opportunity> deals_SJK_4; public List<Opportunity> getDeals_SJK_4()    { if(deals_SJK_4 == null) deals_SJK_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_SJK_4;}
List<Opportunity> deals_SJK_5; public List<Opportunity> getDeals_SJK_5()    { if(deals_SJK_5 == null) deals_SJK_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_SJK_5;}
List<Opportunity> deals_SJK_6; public List<Opportunity> getDeals_SJK_6()    { if(deals_SJK_6 == null) deals_SJK_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer São José dos Campos' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_SJK_6;}
List<Opportunity> deals_LAT_1; public List<Opportunity> getDeals_LAT_1()    { if(deals_LAT_1 == null) deals_LAT_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_LAT_1;}
List<Opportunity> deals_LAT_2; public List<Opportunity> getDeals_LAT_2()    { if(deals_LAT_2 == null) deals_LAT_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_LAT_2;}
List<Opportunity> deals_LAT_3; public List<Opportunity> getDeals_LAT_3()    { if(deals_LAT_3 == null) deals_LAT_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_LAT_3;}
List<Opportunity> deals_LAT_4; public List<Opportunity> getDeals_LAT_4()    { if(deals_LAT_4 == null) deals_LAT_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_LAT_4;}
List<Opportunity> deals_LAT_5; public List<Opportunity> getDeals_LAT_5()    { if(deals_LAT_5 == null) deals_LAT_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_LAT_5;}
List<Opportunity> deals_LAT_6; public List<Opportunity> getDeals_LAT_6()    { if(deals_LAT_6 == null) deals_LAT_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer Latin America' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_LAT_6;}
List<Opportunity> deals_NAM_1; public List<Opportunity> getDeals_NAM_1()    { if(deals_NAM_1 == null) deals_NAM_1 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Prospecting' and Leadership_Opportunity_report__c=true]; return deals_NAM_1;}
List<Opportunity> deals_NAM_2; public List<Opportunity> getDeals_NAM_2()    { if(deals_NAM_2 == null) deals_NAM_2 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Qualification' and Leadership_Opportunity_report__c=true]; return deals_NAM_2;}
List<Opportunity> deals_NAM_3; public List<Opportunity> getDeals_NAM_3()    { if(deals_NAM_3 == null) deals_NAM_3 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Proposal/Price Quote' and Leadership_Opportunity_report__c=true]; return deals_NAM_3;}
List<Opportunity> deals_NAM_4; public List<Opportunity> getDeals_NAM_4()    { if(deals_NAM_4 == null) deals_NAM_4 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Contract Negotiation & Review' and Leadership_Opportunity_report__c=true]; return deals_NAM_4;}
List<Opportunity> deals_NAM_5; public List<Opportunity> getDeals_NAM_5()    { if(deals_NAM_5 == null) deals_NAM_5 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Closed Won' and Leadership_Opportunity_report__c=true]; return deals_NAM_5;}
List<Opportunity> deals_NAM_6; public List<Opportunity> getDeals_NAM_6()    { if(deals_NAM_6 == null) deals_NAM_6 =     [SELECT Id,Amount,ExpectedRevenue,Account.Name,StageName,Account_Embraer_Site__c,RecordType.Name,Product__c FROM  Opportunity      WHERE Account_Embraer_Site__c='Embraer North America' and  StageName= 'Closed Lost' and Leadership_Opportunity_report__c=true]; return deals_NAM_6;}


}