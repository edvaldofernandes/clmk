@isTest
public class myOpereng_Controller_test {
	
    static testMethod void Testing() {
     	test.startTest();
 		
        Account cia = new Account(Name='Embraer Airlines', Company_Nickname__c='EMB' );
        Database.insert(cia);
        Contact c = new Contact( LastName = UserInfo.getLastName(), Email = UserInfo.getUserEmail() , accountId = cia.id );
        Database.insert(c);
        
        System.runAs( new user( LastName = UserInfo.getLastName() , ID = UserInfo.getUserID()) ) {
        	
            case cl = new case( AccountId = c.AccountId , status = 'closed' );
            database.insert(cl);
            
        	myOpereng_Controller controller = new myOpereng_Controller();
            
            controller.caseRadio = '0';
            controller.rodaBusca();
        
        }
        
        test.stopTest();
    }
    
}