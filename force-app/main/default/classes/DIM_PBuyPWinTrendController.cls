// Used by DIM - Market Intelligence.
public with sharing class DIM_PBuyPWinTrendController {

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public DIM_PBuyPWinTrendController() {
        account = System.currentPageReference().getParameters().get('AccountId');
        opportunity = System.currentPageReference().getParameters().get('OpportunityId');
        date1 = String.valueOf(Date.today().addMonths(-6));
        date2 = String.valueOf(Date.today());
        
        if(!String.isBlank(account)) {
            Account accountSupport = [SELECT Embraer_Site__c FROM Account WHERE Id =: account];
            if(accountSupport.Embraer_Site__c != null) {
                office = '\'' + String.valueOf(accountSupport.Embraer_Site__c).subString(0,15) + '\'';
            }
            else {
                office = '\'All\'';
            }
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public String date1 { get; set; }
    public String date2 { get; set; }
    public String opportunity { get; set; }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public String office { get; set; }
    public String officeLabel { get; set; }
    
    private String officeImage;
    public String getOfficeImage() {
        String iconURL = '/resource/DIM_PBuy_PWin/Icons/PWin_Office_' + officeLabel + '.png';
        String defaultIconURL = '/resource/DIM_PBuy_PWin/Icons/PWin_Office_All.png';
        return getFinalURL(iconURL, defaultIconURL);
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public String account { get; set; }
    public String accountLabel { get; set; }
    
    private String accountImage;
    public String getAccountImage() {
        String iconURL = '/resource/DIM_PBuy_PWin/Tails/' + getFormattedAccountLabel(accountLabel) + '.png';
        String defaultIconURL = '/resource/DIM_PBuy_PWin/Tails/_Undefined.png';
        return getFinalURL(iconURL, defaultIconURL);
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private String getFormattedAccountLabel(String accountLabel) {
        if (accountLabel != null) {
            accountLabel = accountLabel.replace(' ','');
            accountLabel = accountLabel.replace('&','');
            accountLabel = accountLabel.replace('(','');
            accountLabel = accountLabel.replace(')',''); 
            return accountLabel.trim();
        }
        return '';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------   
    
    @TestVisible
    private String getFinalURL(String iconURL, String defaultIconURL) {
        try {
            new PageReference(iconURL).getContent();
            return iconURL;
        } 
        catch(VisualforceException e) {
            return defaultIconURL;
        }
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    @RemoteAction
    public static List<List<sObject>> getSeries(String account, String opportunity, String stringDate1, String stringDate2) {
    
        Date date1 = Date.valueOf(stringDate1);
        Date date2 = Date.valueOf(stringDate2);
        
        List<PBuy__c> pBuys = new List<PBuy__c>([          
            SELECT Id, Date__c, Score_Value__c, Account__c
            FROM PBuy__c
            WHERE Account__c =: account AND Date__c >=: date1 AND Date__c <=: date2
            ORDER BY Date__c, CreatedDate ASC
        ]);
        
        List<PWin__c> pWins = new List<Pwin__c>([
            SELECT Id, Date__c, Score_Value__c, Deal_Positioning__c, Opportunity__c
            FROM PWin__c
            WHERE Opportunity__c =: opportunity AND Date__c >=: date1 AND Date__c <=: date2
            ORDER BY Date__c, CreatedDate ASC
        ]);
        
        List<List<sObject>> pBuysPWins = new List<List<sObject>>();
        pBuysPWins.add(pBuys);
        pBuysPWins.add(pWins);
        
        return pBuysPWins;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public List<SelectOption> getAccounts() {

        List<AggregateResult> accounts = Database.query(
            'SELECT MAX(AccountId) Id, Account.Name, MAX(Account.DIM_Name__c) DIM_Name ' +
            'FROM Opportunity ' +
            'WHERE RecordTypeId IN (\'012i0000001IyKG\', \'012i0000001IyKH\') AND  ' +
            '    (NOT StageName LIKE \'Close%\') AND  ' +
            '    (NOT StageName LIKE \'Archive%\') ' +
            getOfficeQuery(office) + ' ' +
            'GROUP BY Account.Name ' +
            'ORDER BY Account.Name ASC'
        );
        
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('None', '-- NONE --'));
        
        for (AggregateResult account : accounts) {
            String value = String.valueOf(account.get('Id'));
            String label = getAccountName(String.valueOf(account.get('Name')), String.valueOf(account.get('DIM_Name')));
            options.add(new SelectOption(value, label));
        }
        
        return getSelectOptionsSortByLabel(options);
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @TestVisible
    private static String getOfficeQuery(String office) {
        if(office != null) {
            if(!office.equalsignorecase('\'ALL\'')) {
                return ' AND Account.Embraer_Site__c IN (' + office + ')';
            }
        }
        return '';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private List<SelectOption> getSelectOptionsSortByLabel(List<Selectoption> options) {
        
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();

        Integer suffix = 1;
        for (Selectoption option : options) {
            mapping.put((option.getLabel() + suffix++), option);   
        }
        
        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();

        options.clear();
        
        for (String key : sortKeys) {
            options.add(mapping.get(key));
        }
        
        return options;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public Boolean hasOneOpportunity { get; set; }
    
    public List<SelectOption> getOpportunities() {

        List<Opportunity> opportunities = [SELECT Id, Name, Opp_Name__c
                                            FROM Opportunity
                                            WHERE AccountId =: account AND
                                                RecordTypeId IN ('012i0000001IyKG', '012i0000001IyKH') AND
                                                (NOT StageName LIKE 'Close%') AND
                                                (NOT StageName LIKE 'Archive%')
                                            ORDER BY RecordType.Name ASC, Leadership_Report_Firm_Order__c ASC];
        
        List<SelectOption> options = new List<SelectOption>();
        
        if (opportunities.size() != 1) {
            options.add(new SelectOption('None', '-- NONE --'));
        }
            
        if (opportunities.size() > 1) {
            hasOneOpportunity = false;
        }
        else {
            hasOneOpportunity = true;
        }
                
        for (Opportunity opportunity : opportunities) {
            options.add(new SelectOption(opportunity.Id, opportunity.Opp_Name__c));
        }
        
        return options;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static String getAccountName(String name, String dimName) {
        if(!String.isBlank(dimName)) {
            return dimName;
        }        
        return name;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public void addLog() {
        DIM_Audit__c log = new DIM_Audit__c();
        log.Content__c = Apexpages.currentPage().getParameters().get('contentTitle');
        log.Is_Mobile__c = Boolean.valueOf(Apexpages.currentPage().getParameters().get('isMobile'));
        insert log;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
}