/* 
----------------------------------------------------------------------------------------------
-- - Company:     Stefanini
-- - Name:        TRG_CaseCRC_Handler
-- - Description: Handle All Case events related to CRC Process
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               Version                   
----------------------------------------------------------------------------------------------
-- 03/12/2019       Felipe Gouvea      1.0   
-- 07/25/2019       Marcelo Nicoletti  1.1
----------------------------------------------------------------------------------------------
*/

public with sharing class TRG_CaseCRC_Handler extends TriggerHandler {

	public List<Case> contextCases { get; set;}

	public TRG_CaseCRC_Handler() {
        
        contextCases = new List<Case>();
        for(Case cs : (List<Case>)Trigger.new){
            if(cs.Record_Type_Developer_Name__c == 'CRC'){
                contextCases.add(cs);
            }       
        }
	}
    
	override
	public void beforeInsert(){
		new CustomerResponseCenterProcess(contextCases).start();
	}
	
	
	override
	public void beforeUpdate(){
	//	new CustomerResponseCenterProcess(contextCases).start();
	}
}