/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Jul-2016.
**/

global class UpdatePublicCalendarEventsBatch implements Database.Batchable<sObject>, Database.Stateful 
{

       

    public UpdatePublicCalendarEventsBatch()
    {
          
    }
    
    global Database.Querylocator start( Database.BatchableContext BC )
    {
              
        string query = '';
        
        
        query = 'Select  Subject,PublicCalendar__c,IsAllDayEvent,Description,EndDateTime,Event_Type__c,Location,Id,StartDateTime,OwnerId From Event Where PublicCalendar__c = true' + (test.IsRunningTest() ? ' Limit 50' : '')  ;

        return Database.getQueryLocator( query );
    }
    
    
    global void execute( Database.BatchableContext BC, List<sObject> scope )
    {
        createNewRecords(scope);
    }
    
    private void createNewRecords(List<sObject> scope)
    {
        EventTriggerHandler handler = new EventTriggerHandler(false);
        handler.newRecords  = new List<Event>();
        handler.isInsert = true;
        for ( sObject obj : scope )
            handler.newRecords.add((Event) obj);
        
        handler.OnAfterInsert();
    
    }
    
    global void finish( Database.BatchableContext bcMain )
    {
        
    }   

}