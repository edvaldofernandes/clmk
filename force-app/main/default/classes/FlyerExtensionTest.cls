@isTest
public class FlyerExtensionTest {
    private static Id TrainingRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId();
    
    private static testmethod void Test(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'ERJ';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
    private static testmethod void Test2(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'EJET';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
    private static testmethod void Test3(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'E-JET E2';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
    private static testmethod void Test4(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'Turboprop';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
    private static testmethod void Test5(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'ab;cd;ef';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
    private static testmethod void Test6(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'ab;cd;ef;ab';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
    private static testmethod void Test7(){
        
        List<Product2> prodsToInsert = new List<Product2>();
        //Create a null account
        Product2 product = new Product2();
        product.RecordTypeId = TrainingRecordType;
        product.Name = 'Test';
        product.Applicability__c = 'ab;cd';
        prodsToInsert.add(product);
        
        insert prodsToInsert;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(product);
        FlyerExtension extension = new FlyerExtension(sc);
        
        PageReference pageRef = Page.ProductFlyerPage;
        pageRef.getParameters().put('id', String.valueOf(product.Id));
        Test.setCurrentPage(pageRef);
        
        test.stopTest();        
    }
}