public class FO_FupQueryBuilder {
    @TestVisible
    private String DEFAULT_VALUE = 'All';
    public static final String QUERY_TEMPLATE = 'SELECT Id {0} FROM FUP__c WHERE Id != null {1} ORDER BY Last_Public_Update__c DESC NULLS LAST, Publish_Date__c DESC';
    @TestVisible
    private List<String> fields = new List<String>();
    @TestVisible
    private List<String> conditions = new List<String>();

    public FO_FupQueryBuilder(){
        this.addField('Reference_Number__c');
        this.addField('Status__c');
        this.addField('Name');
        this.addField('Publish_Date__c');
        this.addField('Last_Public_Update__c');
        
        this.addField('Authority__c');
        this.addField('Aircraft_Family__c');
        
        this.conditions.add('Publish_Status__c = \'Published\'');
    }    
    public List<FUP__c> get(){
        String query = buildQuery();

        return Database.query(query);
    }
    public String buildQuery(){
        String fieldsSplitByComma = buildFieldsString();
        String conditions = buildConditionsString();
        
        String query = String.format(
            QUERY_TEMPLATE, 
            new List<String>{
                fieldsSplitByComma,
                conditions
            }
        );
        return query;
    }
    private void addField(String field){
        this.fields.add(field);
    }
    @TestVisible
    private String buildConditionsString(){
        String conditionsString = '';
        for(String condition : conditions){
			conditionsString += ' AND ' + condition + ' ';    
        }
		return conditionsString;
    }
    @TestVisible
    private String buildFieldsString(){
        String fieldsSplitByComma = '';
        for(String field : this.fields){
            fieldsSplitByComma += ', ' + field;
        }
        return fieldsSplitByComma;
    }
    public FO_FupQueryBuilder withStatus(String status){
        if(String.isEmpty(status)){
            return this;
        }
        if(status.equals(DEFAULT_VALUE)){
            return this;
        }
        this.conditions.add('Status__c = \'' + status + '\'');
        return this;
    }
    public FO_FupQueryBuilder withFamily(String family){
        if(String.isEmpty(family)){
            return this;
        }
        if(family.equals(DEFAULT_VALUE)){
            return this;
        }       
        this.conditions.add('Aircraft_Family__c INCLUDES ( \'' + family + '\' )');
        
        return this;
    }
    public FO_FupQueryBuilder withAuthority(String authority){
        if(String.isEmpty(authority)){
            return this;
        }
        if(authority.equals(DEFAULT_VALUE)){
            return this;
        }  
        
        this.conditions.add('Authority__c INCLUDES ( \'' + authority + '\' )');
        return this;
    }
}