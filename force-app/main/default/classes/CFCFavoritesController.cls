/**
* Controller class for the visualforce page CFCFavorites
* Name: CFCFavoritesController
* @author - Guilherme Nascimento / Lucas Calegari - gjesus@deloitte.com / lucaoliveira@deloitte.com
* @version 1.0 - 22/12/2015
**/
public with sharing class CFCFavoritesController {
    
    public String selectedValueOrderBy {get; set;}
    public List<Favorite__c> lstFav {get; set;}    
    public Integer contFav {get;set;}     
    public String deleteItemId {get;set;}    
    public Map<String, String> mapThumbnails {get; set;}
    public Boolean isPardotActive {get;set;}
    
    public CFCFavoritesController(){
        VerifyPardot();
		searchFavorites();
    }
    
    public void VerifyPardot(){
        system.debug('CFCHomeController.GenerateViewProducts.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCHomeController.GenerateViewProducts.isPardotActive >>> ' + isPardotActive);
    }
    
    public pageReference DeleteItem(){
        system.debug('CFCFavoritesController.DeleteItem()');
        system.debug('CFCFavoritesController.DeleteItem.deleteItemId >>> ' + deleteItemId);
        
        if(deleteItemId != null){
            //Favorite__c fav = [SELECT Id, Name, Product__c, Active__c FROM Favorite__c WHERE User__c =: IDUSERLOGGED AND Active__c = true AND Id =: deleteItemId];
            Favorite__c fav = FavoriteDao.getInstance().getByUserIdAndProductId(UserInfo.getUserId(),deleteItemId);
            system.debug('fav>>>'+fav);
            if(fav != null){
                fav.Active__c = false;
                update fav;
            }
        }
        
        PageReference page = new PageReference('/apex/CFCFavorites');
        page.setRedirect(true);
        return page;
    }
    
    public List<SelectOption> getOptionsOrderBy() {
        List<SelectOption> options = new LIST<SelectOption>();
        options.add(new SelectOption('Relevance', 'Relevance'));
        options.add(new SelectOption('CreatedDate', 'CreatedDate'));
        options.add(new SelectOption('Products A-Z', 'Products A-Z'));
        options.add(new SelectOption('Products Z-A', 'Products Z-A'));
        return options;
    }  
    
    public pageReference MethodOne() { 
        system.debug('CFCFavoritesController.MethodOne.selectedValueOrderBy >>> ' + selectedValueOrderBy);
        searchFavorites();
        return null;
    }    
    
    public void searchFavorites() {
        contFav = 0;
            
        system.debug('CFCFavoritesController.searchFavorites()');
        
        String querySOQL = 'SELECT id, Name, Product__c, CreatedDate, Product__r.Name, Product__r.Commercial_Description__c, LastModifiedDate, Product__r.id, Product__r.Pardot_Status__c, Product__r.Custom_Redirect_Pardot__c'
            + ' FROM Favorite__c'
            + ' WHERE User__c = ' +'\'' + UserInfo.getUserId() + '\'' 
            + ' AND Active__c = '  + true; 
        
        if(!String.isBlank(selectedValueOrderBy)){            
            selectedValueOrderBy = GenerateValueOrderBy(selectedValueOrderBy);            
            querySOQL += selectedValueOrderBy;
        }       
        
		system.debug('CFCFavoritesController.searchFavorites.querySOQL >>> ' + querySOQL);
        lstFav = Database.query(querySOQL);
        system.debug('CFCFavoritesController.searchFavorites.lstFav >>> ' + lstFav);
        
        contFav = lstFav.size();
        system.debug('CFCFavoritesController.searchFavorites.contFav >>> ' + contFav);
        
        GenerateThumbnails(lstFav);
    }
    
    public String GenerateValueOrderBy(string opt){        
        if(opt=='Products A-Z'){
            return ' Order By Product__r.Name ASC';
        }else if(opt=='Products Z-A'){
            return  ' Order By Product__r.Name DESC';
        }else{
            return  ' Order By LastModifiedDate DESC';
        }
    }

    public void GenerateThumbnails(List<Favorite__c> items){
        system.debug('CFCFavoritesController.GenerateThumbnails()');
        
        mapThumbnails = new Map<String, String>();        
        Set<String> setIdProducts = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_home');
        
        system.debug('CFCFavoritesController.GenerateThumbnails.items >>> ' + items);
        //creating a set of products, just to ensure that there is not equals id
        for(Favorite__c item : items){
            setIdProducts.add(item.Product__c);
        }
        
        //populate the map
        for(String item : setIdProducts){
            mapThumbnails.put(item, 'no image');
        }
        system.debug('CFCFavoritesController.GenerateThumbnails.mapThumbnails >>> ' + mapThumbnails);
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetProductIdAndRecordTypeId(setIdProducts, rType.Id);
        System.debug('CFCFavoritesController.GenerateThumbnails.lstAttch >>> ' + lstAttch);
        
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            if(mapThumbnails.containsKey(lstAttch.get(i).Product__c)){
                try{
                    mapThumbnails.put(lstAttch.get(i).Product__c, lstAttch.get(i).Attachments[0].id); 
                } catch(Exception ex){
                    mapThumbnails.put(lstAttch.get(i).Product__c, 'no image');    
                }
            }            
        }
        
        System.debug('CFCFavoritesController.GenerateThumbnails.mapThumbnails >>> ' + mapThumbnails);
    }    
    
}