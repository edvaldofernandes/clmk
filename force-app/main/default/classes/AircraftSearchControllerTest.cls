/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; Jan-2017.
**/
@isTest
public with sharing class AircraftSearchControllerTest
{
    
    static testMethod void unitTest() 
    {
             
        test.startTest();
        
        List<Account> accountList = new List<Account>();
        accountList.add(
                            new Account(
                                          BillingCountry = 'Brazil',
                                          Name = 'Airline 1',
                                          Company_Nickname__c = 'Test Airline 1',
                                          ICAO_Code__c = '1',
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()
                                        )
                        );
                        
         accountList.add(
                            new Account(
                                          BillingCountry = 'Germany',
                                          Name = 'Airline 2',
                                          Company_Nickname__c = 'Test Airline 2',
                                          ICAO_Code__c = '2',
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()
                                        )
                        );     
                        
        accountList.add(
                            new Account(
                                          BillingCountry = 'Uruguay',
                                          Name = 'Airline 3',
                                          Company_Nickname__c = 'Test Airline 3',
                                          ICAO_Code__c = '3',
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()
                                        )
                        );                         
        
        accountList.add(
                            new Account(
                                          BillingCountry = 'Brazil',
                                          Name = 'Operator 1',
                                          Company_Nickname__c = 'Test Operator 1',
                                          ICAO_Code__c = '4',
                                          Geographical_Area__c = 'Latin America', 
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MRO').getRecordTypeId()
                                        )
                        );
                        
       accountList.add(
                            new Account(
                                          BillingCountry = 'Uruguay',
                                          Name = 'Operator 2',
                                          Company_Nickname__c = 'Test Operator 2',
                                          ICAO_Code__c = '5',
                                          Geographical_Area__c = 'Latin America', 
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MRO').getRecordTypeId()
                                        )
                        );
        
        accountList.add(
                            new Account(
                                          BillingCountry = 'Italy',
                                          Name = 'Operator 3',
                                          Company_Nickname__c = 'Test Operator 3',
                                          ICAO_Code__c = '6',
                                          Geographical_Area__c = 'Europe', 
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MRO').getRecordTypeId()
                                        )
                        );
        accountList.add(
                            new Account(
                                          BillingCountry = 'Spain',
                                          Name = 'Operator 4',
                                          Company_Nickname__c = 'Test Operator 4',
                                          ICAO_Code__c = '7',
                                          Geographical_Area__c = 'Europe',                                           
                                          RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MRO').getRecordTypeId()
                                        )
                        );
                        
        
        insert accountList;
        
        List<Aircraft__c> aircraftList = new List<Aircraft__c>();
        
        for(integer i=0;i<=600;i++)
        {
        
            Aircraft__c aircraft = new Aircraft__c();
            aircraft.Owner__c = accountList[i/200].Id;
            aircraft.Operator__c = accountList[(i/200) + 3].Id;
            aircraft.Flying_For__c = accountList[(i/300)].Id;
            aircraft.RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Embraer').getRecordTypeId();
            aircraft.Name = 'Embraer' + string.valueOf(i);
            
            if(i <= 300)
            {
                aircraft.Aircraft_Status__c = 'In Service';
                aircraft.Model_Type__c = 'E170';
            }
            else
            {
                aircraft.Aircraft_Status__c = 'Parked';
                aircraft.Model_Type__c = 'E175';
            }
            
            
            aircraftList.add(aircraft);
            
        }
        
        
        /*
            To do - Assertions
        */
        PageReference pageRef = Page.AircraftSearchTool;

        
        Test.setCurrentPageReference(pageRef);
        AircraftSearchController controller = new AircraftSearchController();    
        string debug = controller.debugSoql;
        controller.toggleSort();
        ApexPages.currentPage().getParameters().put('Owner','Airline 3');
        ApexPages.currentPage().getParameters().put('Operator','Operator 3');
        ApexPages.currentPage().getParameters().put('Flying','Airline 2');
        ApexPages.currentPage().getParameters().put('Model_type','E170');
        ApexPages.currentPage().getParameters().put('Status','In Service');
        ApexPages.currentPage().getParameters().put('Geoloc','Europe');
        
        controller.runSearch();
        
        controller.sortField = 'exceptionFieldName';
        controller.runSearch();
        
        
        test.stopTest();
           
    }    

}