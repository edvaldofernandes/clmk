global class sandboxPostCopyClass implements SandboxPostCopy 
{

    global void runApexClass(SandboxContext context) 
    {
        database.executeBatch(new postCopyScriptClass());
        
    }

}