@isTest(seeAllData=true)
global class SalesForceToEtrackProxyTest implements WebServiceMock{
    
     global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               
               ETRACK_OUT_BOUND_END_POINT.updateFromSalesForceResponse SfInfoElement = new ETRACK_OUT_BOUND_END_POINT.updateFromSalesForceResponse();
                      
           	   SfInfoElement.status = Constants.STATUS_CALL_OUT;        
       	       response.put('response_x', SfInfoElement); 
   }
    
    @isTest static void testRequest() {
        
        Test.setMock(WebServiceMock.class, new SalesForceToEtrackProxyTest());
        
        List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount> sfInfoAccountList = new  List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount>();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount sfInfoAccount = new  ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount();
        sfInfoAccount.accountType = 'test';
        sfInfoAccount.companyName = 'test';
        sfInfoAccount.companyNickname = 'test';
        sfInfoAccount.companyStatus = 'test';
        sfInfoAccount.flyEmbraerID = 'test';
        sfInfoAccount.objectType = 'test';
        sfInfoAccount.status = 'test';
        sfInfoAccountList.add(sfInfoAccount);
        
        List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember> sfTeamMemberList = new List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember>();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember sfTeamMember = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember();
        sfTeamMember.accountId = '0010S000008jmlrQAA';
        sfTeamMember.email = 'test';
        sfTeamMember.name = 'test';
        sfTeamMember.teamMemberRole = 'test';
        sfTeamMember.phone = 'test';
        sfTeamMember.flyEmbraerID = 'test';
        sfTeamMember.objectType = 'test';
        sfTeamMember.status = 'test';
        sfTeamMemberList.add(sfTeamMember);
        
        List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft> sfAircraftList = new List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft>();
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft sfAircraft = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft();
        sfAircraft.modelType = 'test';
        sfAircraft.operator = 'test';
        sfAircraft.owner = 'test';
        sfAircraft.registration = 'test';
        sfAircraft.serialNumber = 'test';
        sfAircraft.flyEmbraerID = 'test';
        sfAircraft.objectType = 'test';
        sfAircraft.status = 'test';
        sfAircraftList.add(sfAircraft);
               
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element sfElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
        sfElement.account = sfInfoAccountList;
        sfElement.aircraft = sfAircraftList;     
        sfElement.teamMember = sfTeamMemberList;
        
        ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort eTRackSFRequest = new ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort();

        String output = eTRackSFRequest.updateFromSalesForceRequest(sfElement);
                
        System.assertEquals(Constants.STATUS_CALL_OUT, output);
        
    }
}