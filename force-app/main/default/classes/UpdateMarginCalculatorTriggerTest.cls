@isTest
public class UpdateMarginCalculatorTriggerTest {
    
    private static Id ACMOD_RecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
    
    static testmethod void test06_Insert(){
        //Create products
        Product2 prodNormal = new Product2(Name = 'Normal Product', recordtypeid = ACMOD_RecordType);
        insert prodNormal;
        Product2 prodNoCostControl = new Product2(Name = 'No Cost Product', recordtypeid = ACMOD_RecordType);
        insert prodNoCostControl;
        
        //Create Product Cost Control
        Product_Cost_Control__c productCostControl1 = new Product_Cost_Control__c(
            Related_Product__c = prodNormal.Id,
            NREC__c = 10000,
            REC__c = 3500,
            Sales_Deduction__c = 5.2,
            Tech_Pubs_SB_Revision_Costs__c = 120
        );
        insert productCostControl1;        
        
        //Create Opportunities
        Opportunity oppTest1 = new Opportunity(
            Name= 'OppTest 1',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        insert oppTest1; 
        
        //Create Pricebook
        PricebookEntry pricebookEntry1 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prodNormal.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry1;
        
        //Create Opportunity Products
        OpportunityLineItem oppProduct1 = new OpportunityLineItem(
            OpportunityId = oppTest1.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 7200,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 72000,
            Princing__c = 'REC'
        );
        insert oppProduct1;
        
        OpportunityLineItem oppProduct2 = new OpportunityLineItem(
            OpportunityId = oppTest1.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000,
            Princing__c = 'NREC'
        );
        test.startTest();
        insert oppProduct2;
        
        oppProduct2.UnitPrice = 17500;
        oppProduct2.Quantity = 20;
        update oppProduct2;
        
        delete oppProduct1;
        test.stopTest();
        
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 1' limit 1];
        
        MarginCalculatorHandler MCH = new MarginCalculatorHandler();
        MCH.calculateOpportunityMargin(opp.Id);
        
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid', opp.Calculation_Status__c, 'Status');
        System.assertEquals(37.6, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(37.6, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
}