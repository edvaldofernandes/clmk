/* Classe implementadora de SOBjectDAO para operações DML no objeto RecordType, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 07/01/2016
*/
public without sharing class RecordTypeDao {
    
    private static final RecordTypeDao instance = new RecordTypeDao();    
    
    private RecordTypeDao(){
    }    
    
    public static RecordTypeDao getInstance(){
        return instance;
    }

    public RecordType getById(String idRecordType){
        List<RecordType> listRecordType = database.query('Select ' + utils.getAllFields('RecordType') + ' FROM RecordType WHERE id =\''+String.escapeSingleQuotes(idRecordType)+'\' LIMIT 1 ');
        
        //List<RecordType> listRecordType = [SELECT SobjectType, Name, Id, DeveloperName, Description FROM RecordType WHERE id = :idRecordType LIMIT 1];
        
        if(listRecordType.size() > 0)	{
            return listRecordType[0];
        }
        
      	return null;   
    }
    
    public List<RecordType> listBySetId(Set<String> setIdRecordType){
        string setToString = '';
        for(String includeValue : setIdRecordType){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        
        List<RecordType> listRecordType = database.query('Select ' + utils.getAllFields('RecordType') + ' FROM RecordType WHERE id IN ('+setToString+')');
        //List<RecordType> listRecordType = [SELECT SobjectType, Name, Id, DeveloperName, Description FROM RecordType WHERE id IN :setIdRecordType];
        if(listRecordType.size() > 0){
            return listRecordType;
        }
        
      	return null;        
    }
    
    public RecordType getByDeveloperName(String developerName){
        List<RecordType> listRecordType = database.query('Select ' + utils.getAllFields('RecordType') + ' FROM RecordType WHERE DeveloperName =\''+String.escapeSingleQuotes(developerName)+'\' LIMIT 1 ');
        //List<RecordType> listRecordType = [SELECT SobjectType, Name, Id, DeveloperName, Description FROM RecordType WHERE DeveloperName = :developerName LIMIT 1];
        
        if(listRecordType.size() > 0){
            return listRecordType[0];
        }
        
      	return null;   
    }
    
}