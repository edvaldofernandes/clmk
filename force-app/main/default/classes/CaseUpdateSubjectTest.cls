@IsTest
public with sharing class CaseUpdateSubjectTest {

    private static final Id CRC_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();


    @IsTest
    public static void shouldUpdateSubjectAddCaseNumber(){

        Case c = createCase();

        Test.startTest();

        Database.insert(c);

        Test.stopTest();

        c = [SELECT Id, Subject, CaseNumber FROM Case WHERE Id = :c.Id];

        System.assert(c.Subject.contains(c.CaseNumber));
    }

    private static Case createCase(){
        
        Case c = new Case(
            RecordTypeId = CRC_ID,
            Status = 'New',
            Due_Date__c = Date.today().addMonths(1),
            Subject = 'CRC',
            Study_Type__c = 'Performance',
            Type = 'TBC - To be classified',
            Phase_c__c = 'Dispatch',
            Status__c = 'Dispatch.',
            Next_Action_required_details__c = 'Required Details',
            Next_Action_Required_Date_and_Time__c = System.now(),
            Requested_By__c = UserInfo.getUserId(),
            Description = 'Customer Number:0000335353 *'
        );
        
        return c;
    }
    
    @IsTest
    public static void shouldUpdateSubjectWithoutSubject(){

        Case cs = createCaseNoSubject();

        Test.startTest();

        Database.insert(cs);

        Test.stopTest();

        cs = [SELECT Id, Subject, CaseNumber FROM Case WHERE Id = :cs.Id];

        System.assert(cs.Subject.contains(cs.CaseNumber));
    }

    private static Case createCaseNoSubject(){
        
        Case cs = new Case(
            RecordTypeId = CRC_ID,
            Status = 'New',
            Due_Date__c = Date.today().addMonths(1),
            Subject = '',
            SuppliedEmail = 'test@embraer.net.br',
            Study_Type__c = 'Performance',
            Type = 'TBC - To be classified',
            Phase_c__c = 'Dispatch',
            Status__c = 'Dispatch.',
            Next_Action_required_details__c = 'Required Details',
            Next_Action_Required_Date_and_Time__c = System.now(),
            Requested_By__c = UserInfo.getUserId(),
            Description = 'Customer Number:0000335353 *'
        );
        
        return cs;
    }
    
    
}