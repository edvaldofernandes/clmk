@isTest
/*
 * Test Class for CLMSetNewActiveAgreement, 
 * Similar to CLMCloneAgreementTest (CLMCloneAgreement, AutoNamingRules, CLMChangeShowAircraftAs and CLMSyncAmendment)
*/
public class CLMSetNewActiveAgreementTest {
    private static final String PROMOTE_ACTION = 'Promote';
    private static final String AMEND_ACTION = 'Amend';
    
	
    static testMethod void testingMultiverse(){
        List<SObject> listToInsert = new List<SObject>();
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        listToInsert.add((SObject)productTest);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userTest1 = SObjectInstanceTest.createUser(p.Id);
        userTest1.Username = 'usertest1@mail.com.br'; userTest1.CommunityNickname = 'Community Nickname1';
        User userTest2 = SObjectInstanceTest.createUser(p.Id);
        userTest2.Username = 'usertest2@mail.com.br'; userTest2.CommunityNickname = 'Community Nickname2';
        User userTest3 = SObjectInstanceTest.createUser(p.Id);
        userTest3.Username = 'usertest3@mail.com.br'; userTest3.CommunityNickname = 'Community Nickname3';
        listToInsert.addAll(new List<SObject>{(SObject)userTest1,(SObject)userTest2,(SObject)userTest3});
        
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        listToInsert.add((SObject)accountTest);
        database.insert(listToInsert);
        
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        Agreement1.Country__c = 'Brazil';
        Agreement1.Signature_Date__c = system.today();
        Agreement1.Nickname__c = 'TestNick';
        Agreement1.Change_History__c = 'pa main changes';
        Agreement1.Name = 'AGR 1';
        
        Agreement__c Agreement2 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement2.Signature_Date__c = date.today();
        Agreement2.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        Agreement2.Country__c = 'Brazil';
        Agreement2.Signature_Date__c = system.today();
        Agreement2.Parent_Agreement__c = Agreement1.Id;
        Agreement2.Nickname__c = 'TestNick2';
        Agreement2.Change_History__c = 'pa main changes2';
        Agreement2.Name = 'AGR 2';
        database.insert(new List<Agreement__c>{Agreement1, Agreement2});
        
        Agreement__c Agreement3 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement3.Signature_Date__c = date.today();
        Agreement3.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        Agreement3.Country__c = 'Brazil';
        Agreement3.Signature_Date__c = system.today();
        Agreement3.Nickname__c = 'TestNick3';
        Agreement3.Change_History__c = 'pa main changes3';
        Agreement3.Parent_Agreement__c = Agreement1.Id;
        Agreement3.Name = 'AGR 3';
        
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement2.Id, productTest.id);
        aircraftPrice1.Commercial_Agreement__c = Agreement2.Id;

        database.insert(new List<SObject>{(SObject)Agreement3,(SObject)aircraftPrice1});
        
        Escalation__c escalation1 = SObjectInstanceTest.createDefaultEscalation(Agreement2.Id, 50);
		escalation1.Escalation_Code__c='testMAR/19';
        escalation1.EscalationExternalID__c='testMAR/19';
        escalation1.Escalation_Rate__c=1.2;
        escalation1.Rate_Effective_Date__c=system.today();
                        
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement2.Id, 'Option');
        agreementAircraft1.Contractual_Delivery_Month__c=system.today();
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        
        //agreementAircraft1.Aircraft_Price__r.Commercial_Agreement__r.Id = Agreement2.Id;
        //agreementAircraft1.Aircraft_Price__r.Aircraft_Price__r.Model__r.Id = productTest.id;
        
        Agreements_Team__c team1 = new Agreements_Team__c(Commercial_Agreement__c = Agreement2.Id, User__c = userTest1.id);
        Contract_PDP__c contract1 = new Contract_PDP__c(Commercial_Agreement__c = Agreement2.Id, Installment_Type__c = 'Initial Deposit', Payment_ammount__c = 1, Payment_Date__c = Date.today().addDays(1));
        Agreements_Team__c teamX = new Agreements_Team__c(Commercial_Agreement__c = Agreement2.Id, User__c = userTest1.id);
        Aircraft_Price__c aircraftPriceX = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        aircraftPriceX.Commercial_Agreement__c = Agreement2.Id;
        Escalation__c escalationX = SObjectInstanceTest.createDefaultEscalation(Agreement2.Id, 111);
        
        system.debug('DBG>>> ' + Agreement2.Id + agreementAircraft1.Aircraft_Price__r.Commercial_Agreement__r.Id);
        system.debug('DBG>>> ' + productTest.id + agreementAircraft1.Aircraft_Price__r.Aircraft_Price__r.Model__r.Id);
		
        database.insert(new List<SObject>{(SOBject)escalation1, (SObject)contract1, (SObject)team1, (SObject)teamX, /*(SObject)aircraftPriceX,*/ (SObject)escalationX});
        //database.insert(agreementAircraft1);
        //addSalesforceFileToTestObject(Id.valueOf(clonedAgreement), 'Number 1');
        //addSalesforceFileToTestObject(Id.valueOf(clonedAgreement), 'Number 2');
        
        CLMSetNewActiveAgreement.setNewActiveAgreement(String.valueOf(Agreement2.Id), String.valueOf(Agreement1.Id));
        
        System.assertEquals(2, [SELECT Id FROM Contract_PDP__c].size());
        System.assertEquals(4, [SELECT Id FROM Agreements_Team__c].size());
        System.assertEquals(2, [SELECT Id FROM Aircraft_Price__c].size());
        System.assertEquals(3, [SELECT Id FROM Agreement__c].size());
        System.assertEquals(4, [SELECT Id FROM Escalation__c].size());
        System.assertEquals(0, [SELECT Id FROM Account_Cockpit__c].size());
        System.assertEquals(0, [SELECT Id FROM Document_Sharing__c].size());
        System.assertEquals(0, [SELECT Id FROM Special_Credit__c].size());
        System.assertEquals(0, [SELECT Id FROM Product_Support__c].size());
        System.assertEquals(0, [SELECT Id FROM Agreement_Aircraft__c].size());
    }
    
    private static void addSalesforceFileToTestObject(Id sObjectId, String variable){
           
        ContentVersion contentVersion = new ContentVersion(
          Title = 'Penguins ' + variable,
          PathOnClient = variable + ' Penguins.jpg',
          VersionData = Blob.valueOf('Test Content' + variable),
          IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = sObjectId;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        } 
    
}