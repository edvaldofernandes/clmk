/*******************************************************************************
*                               Embraer - 2015
*-------------------------------------------------------------------------------
* AUTHOR: Rodrigo Gimenes Rodrigues                             DATE: 16/12/2015
*
*******************************************************************************/
public without sharing class ServiceContractManagementTriggerHandler
{
     public static void test(){
        String teste = 'Deploying and this class will be deleted';
    }
/*
   
    public Map<Id,Service_Contract_Management__c> newRecordsMap = new Map<Id,Service_Contract_Management__c>();
    public Map<Id,Service_Contract_Management__c> oldRecordsMap = new Map<Id,Service_Contract_Management__c>(); 
    public List<Service_Contract_Management__c> newRecords = new List<Service_Contract_Management__c>();
    public List<Service_Contract_Management__c> oldRecords = new List<Service_Contract_Management__c>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public ServiceContractManagementTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

  /*

    public void OnBeforeInsert()
    {
        updateContractLineItemMaster();
        updateOppReason();
    }

 

    public void OnAfterInsert()
    {
        updateProductRecordType();
        recalculateItemsQuantity();
    }

 

    public void OnBeforeUpdate()
    {
        updateContractLineItemMaster();
    }

 

    public void OnAfterUpdate()
    {
        updateProductRecordType();
        recalculateItemsQuantity();   

    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        recalculateItemsQuantity();

    }

 

    public void OnUndelete()
    {

        recalculateItemsQuantity();

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }

    
    private void updateProductRecordType()
    {
        List<Service_Contract_Management__c> recordList = new List<Service_Contract_Management__c>();
        
        
        Set<Id> eligibleIds = new Set<Id>();
        
        if(this.IsInsert)
        {
            eligibleIds = this.newRecordsMap.keySet();
        }
        else
        {
            for(Service_Contract_Management__c scm : this.newRecords)
            {
                if(scm.Contract_Line_Item_Master__c != this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c)
                    eligibleIds.add(scm.Id);    
            }
        }
        
        
        for(Service_Contract_Management__c scm : [Select Id,Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name from Service_Contract_Management__c Where id in : eligibleIds] )
        {
            scm.productRecordType__c = scm.Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name;
            recordList.add(scm);
        }
        
        if(!recordList.isEmpty())
            update recordList;
        
    }

    private void recalculateItemsQuantity()
    {
        Set<Id> eligibleIds = new Set<Id>();
        eligibleIds.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId());
        eligibleIds.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable eSolutions').getRecordTypeId());
        eligibleIds.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable Embraer').getRecordTypeId());
        eligibleIds.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable (Third Party)').getRecordTypeId());
        eligibleIds.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable').getRecordTypeId());
        
        Set<Id> contractLineItemsIds = new Set<Id>();
        List<Service_Contract_Management__c> records = new List<Service_Contract_Management__c>();
        
        if(this.IsDelete)
        {
            records = this.oldRecords;    
        }
        else
        {
            records = this.newRecords;
        }        
        
        for(Service_Contract_Management__c scm : records)
        {
            if(eligibleIds.contains(scm.RecordTypeId))
            {
                if(this.IsDelete || this.IsInsert || this.IsUndelete)
                {    
                    contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);
                }
                else
                {
                    System.Debug('Id Master novo - ' + scm.Contract_Line_Item_Master__c);
                    System.Debug('Id Master old - ' + this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c);
                    if((scm.Quantity__c != this.oldRecordsMap.get(scm.Id).Quantity__c) || (scm.Contract_Line_Item_Master__c != this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c))
                    {
                        contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);                        
                        contractLineItemsIds.add(this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c);                        
                    }
                    
                }
            }    
        }
        
        if(!contractLineItemsIds.isEmpty())
            utils.recalculateSCMQuantities(contractLineItemsIds);
        
        
    }
   

    private void updateContractLineItemMaster()
    {
        for(Service_Contract_Management__c scm : this.newRecords)
        {
        
            if(scm.Service_Contract_line_item__c != null)
            {
                scm.Contract_Line_Item_Master__c = scm.Service_Contract_line_item__c;
            }
            else if(scm.Contract_Line_Item_conversion__c != null)
            {
                scm.Contract_Line_Item_Master__c = scm.Contract_Line_Item_conversion__c;
            }
            else if(scm.Contract_Line_Item_Deliverable__c != null)
            {
                scm.Contract_Line_Item_Master__c = scm.Contract_Line_Item_Deliverable__c;

            }
            
        }
        
        
        
    
    
    }


    private void updateOppReason()
    {
        map<id,string> idContractOppReasonMap = new map<id,string>();
        
        for(Service_Contract_Management__c scm : this.newRecords)
            idContractOppReasonMap.put(scm.Contract_Line_Item_Master__c,'');

        for(ContractLineItem cli : [Select id,Opp_Reason__c From ContractLineItem Where Id in : idContractOppReasonMap.keySet()])
            idContractOppReasonMap.put(cli.Id,cli.Opp_Reason__c);
        
        for(Service_Contract_Management__c scm : this.newRecords)
            scm.Opp_Reason__c = idContractOppReasonMap.get(scm.Contract_Line_Item_Master__c);
    
    
    }


*/

}