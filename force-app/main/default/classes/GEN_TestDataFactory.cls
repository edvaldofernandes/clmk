/**
* @author Marcilio Leite de Souza
* @date 21/08/2018
* @description: Help to populate required fields on objects
* @reference: https://github.com/jongpie/SmartTestDataFactory       
* @notes: Use @isTest annotation so that we don't accidentally use it to create real data
* @example: Account acc = new Account();
*           new GEN_TestDataFactory(acc).populateRequiredFields();
*           NOT POPULATE RELATED FIELDS
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           21 AGO 2018             Original Version
**/
@isTest 
public class GEN_TestDataFactory {

    private final Schema.SobjectType sobjectType;
    private final List<Schema.SobjectField> requiredFields;
    private Sobject record;

    /**
    * @description : Construct with SObjectType
    * @param Schema.SobjectType sobjectType : SObject type to be create
    **/
    public GEN_TestDataFactory(Schema.SobjectType sobjectType) {
        this(sobjectType.newSobject());
    }

    /**
    * @description : Construct with SObject
    * @param SObject record : SObject to be create
    **/
    public GEN_TestDataFactory(SObject record) {
        this.record      = record;
        this.sobjectType = record.getSobjectType();
        this.requiredFields = this.getRequiredFields();
    }

    /**
    * @description : Populate the required fields
    * @return SObject : SObject related to the factory
    **/
    public SObject populateRequiredFields() {
        this.setRequiredFieldsOnRecord();
        return this.record;
    }

    /**
    * @description : Get the required fields from the SObject
    * @return List<Schema.SobjectField> : List of required fields
    **/
    private List<Schema.SobjectField> getRequiredFields() {
        List<SobjectField> requiredFields = new List<SobjectField>();
      
        for(Schema.SobjectField field : this.sobjectType.getDescribe().fields.getMap().values()) {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();

            // Guard clauses for fields that aren't required
            if(fieldDescribe.isNillable() == true) continue;
            if(fieldDescribe.isCreateable() == false) continue;

            // If a field is not nillable & it is createable, then it's required
            requiredFields.add(field);
        }

        return requiredFields;
    }

    /**
    * @description : Set the required fields from the SObject
    * @return void
    **/
    private void setRequiredFieldsOnRecord() {
        Map<String, Object> populatedFields = this.record.getPopulatedFieldsAsMap();
        for(Schema.SobjectField field : this.requiredFields) {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            // If a field was already populated by using the constructor 'TestDataFactory(Sobject record)', then don't change it
            if(populatedFields.containsKey(fieldDescribe.getName())) continue;

            Object fieldValue;
            if(fieldDescribe.getDefaultValue() != null) {
                // If there is a default value setup for the field, use it
                fieldValue = fieldDescribe.getDefaultValue();
            } else {
                // Otherwise, we'll generate our own test value to use, based on the field's metadata
                fieldValue = this.getTestValue(fieldDescribe);
            }

            // If we now have a value to use, set it on the record
            if(fieldValue != null) this.record.put(field, fieldValue);
        }
    }

    /**
    * @description : Get the values to set in the SObject
    * @param Schema.DescribeFieldResult fieldDescribe : related field
    * @return Object : Generic object
    * @notes : Some more complex data types, like ID & Reference, require other objects to be created
    *          This implementation delegates that responsibility to the test classes since DML is required to get a valid ID,
    *          but the logic below could be updated to support creating parent objects if needed
    *          Unsupported display types have been commented-out below:
    *              - Schema.DisplayType.Address, Schema.DisplayType.AnyType, Schema.DisplayType.Base64,
    *              - Schema.DisplayType.DataCategoryGroupReference, Schema.DisplayType.Id, Schema.DisplayType.Reference
    **/
    private Object getTestValue(Schema.DescribeFieldResult fieldDescribe) {
        Schema.DisplayType displayType = fieldDescribe.getType();
        
        switch on displayType {
            when Boolean {		
                return false;
            }	
            when Combobox {		
                return 'Test combobox';
            }
            when Currency {		
                return 19.85;
            }
            when Date {		
                return System.today();
            }	
            when DateTime {		
                return System.now();
            }
            when Double {		
                return 3.14;
            }
            when Email {		
                return 'test@example.com';
            }	
            when EncryptedString {		
                return this.getStringValue(fieldDescribe);
            }
            when MultiPicklist {		
                return fieldDescribe.getPicklistValues()[0].getValue();
            }
            when Percent {		
                return 0.42;
            }	
            when Phone {		
                return '+55 999 11 22 99';
            }
            when Picklist {		
                return fieldDescribe.getPicklistValues()[0].getValue();
            }
            when String {		
                return this.getStringValue(fieldDescribe);
            }	
            when TextArea {		
                return this.getStringValue(fieldDescribe);
            }	
            when Time {		
                return Time.newInstance(13, 30, 6, 20);
            }
            when Url {		
                return 'https://salesforce.com';
            }
            when else {		  
                return null;
            }
        } 
    }

    /**
    * @description :Get max value from a string field
    * @param Schema.DescribeFieldResult fieldDescribe : Related string field
    * @return : A populate string
    **/
    private String getStringValue(Schema.DescribeFieldResult fieldDescribe) {
        String strValue   = 'Test string for ' + fieldDescribe.getType();
        Integer maxLength = fieldDescribe.getLength();

        return strValue.length() <= maxLength ? strValue : strValue.left(maxLength);
    }
}