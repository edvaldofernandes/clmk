// Used by DIM - Market Intelligence.
@isTest
public class DIM_ApexHelperControllerTest{

    static testMethod void validateSOQL() {
        List<RecordType> results = DIM_ApexHelperController.executeSoql('SELECT DeveloperName FROM RecordType WHERE Name = \'Sales Support\' AND SobjectType = \'Case\'');
        System.assertEquals('Sales_Support', results[0].DeveloperName);
    }
}