/**
* @description: This class is the controller for generating an EOD Document. For more information about PDF generation, access:
* https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_output_pdf_renderas.htm
**/
// Request URL:https://commercialaviation--flightops1--c.cs66.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=SVGZ&versionId=0680v000000GaF6&operationContext=CHATTER&contentId=05T0v000001hT5H&page=0
// <apex:image URL="/sfc/servlet.shepherd/version/download/{!attachment.Id}"/>
// <img src="/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId={!attachment.id}" />
public class FO_EodDocumentController {
    private final Id eodId;
    
    /**
    * @description The class constructor. It gets the current record Id.
    * 
    * @param ApexPages.StandardController stdController : The standard controller.
    **/
    public FO_EodDocumentController(ApexPages.StandardController stdController) {
        this.eodId = stdController.getId();
    }
    
    /**
    * @description Gets the review log from the service layer.
    * 
    * @return Map<String, Date> : A Map representing the version log.
    **/
    public Map<String, Date> getReviewLog(){
        return FO_EODService.getRevisionLog(this.eodId);
    }
    
    /**
    * @description Gets a list of aircraft on which the EOD is applicable.
    * 
    * @return List<Aircraft__c> : The related aircraft.
    **/
    public List<Aircraft__c> getAssociatedAircraft(){
        return FO_EODService.getEodAssociatedAircraft(this.eodId);
    }
    
    public Map<String, List<Attachment>> getAttachments(){
        List<Id> annexIds = FO_EODService.getEodRelatedDocumentsIds(eodId);
        Map<String, List<Attachment>> images = FO_Document.getAttachmentsImages(annexIds);
        return images;
    }
}