@isTest
public class DosTestDataFactory {
    
    //method which creates all data necessary for DOS testing
    //creates Accounts, Cases, 
    @isTest public static void createDosCases(){
        List<sObject> accts = Test.loadData(Account.sObjectType, 'testDataAccounts');				//create account test data from static resource
        List<sObject> parts = Test.loadData(LLPDatabase__c.sObjectType, 'testDataPartNumbers');     //create part number test data from static resource csv file     
        
        //create cases test data
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('MSPReplesh').getRecordTypeId();	//get recordtypeid for "Pool Exchanges"
		List<Case> cases = new List<Case>();
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[1].id, SO__c='1', priority = 'AOG', Phase__c='Back Order', Ecode__c='1', RecordTypeId = rTypeId));
    	cases.add(new Case(AccountID=accts[1].id, Part_Number__c=parts[2].id, SO__c='2', priority = 'AOG', Phase__c='Back Order', Ecode__c='2', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[2].id, Part_Number__c=parts[3].id, SO__c='3', priority = 'AOG', Phase__c='Back Order', Ecode__c='3', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[0].id, SO__c='4', priority = 'AOG', Phase__c='Back Order', Ecode__c='4', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[1].id, Part_Number__c=parts[1].id, SO__c='5', priority = 'AOG', Phase__c='Back Order', Ecode__c='5', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[2].id, Part_Number__c=parts[0].id, SO__c='6', priority = 'AOG', Phase__c='Back Order', Ecode__c='6', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[4].id, SO__c='7', priority = 'AOG', Phase__c='Back Order', Ecode__c='7', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[5].id, SO__c='8', priority = 'AOG', Phase__c='Back Order', Ecode__c='8', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[2].id, Part_Number__c=parts[1].id, SO__c='9', priority = 'AOG', Phase__c='Back Order', Ecode__c='100', RecordTypeId = rTypeId));
        insert cases;
        
        //create core test data
        list<Core_Information__c> cores = new List<Core_Information__c>();
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
		cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[12].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[14].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[12].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        insert cores;
		
        //create repair test data
        List<Repair_Information__c> repairs = new List<Repair_Information__c>();
		repairs.add(new Repair_Information__c(Name='1713029', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712792', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712798', Repair_Part_Number__c=parts[14].id));
        repairs.add(new Repair_Information__c(Name='1713131', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1713145', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712865', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712884', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712925', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712926', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712953', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712348', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712471', Repair_Part_Number__c=parts[19].id));
        repairs.add(new Repair_Information__c(Name='1712227', Repair_Part_Number__c=parts[0].id));
        repairs.add(new Repair_Information__c(Name='1712433', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712674', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712309', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712643', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712718', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712704', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712359', Repair_Part_Number__c=parts[12].id));
		insert repairs;
        
        //create ress test data
       /* List<RESS_Info__c> resses = new List<RESS_Info__c>();
        resses.add(new RESS_Info__c(RESS_ID__c='3748', Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3892', Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4077', Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4080',	Part_Number__c=parts[4].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4081',	Part_Number__c=parts[4].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4117',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='16971',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='18855',	Part_Number__c=parts[4].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2121',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2581',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2582',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2676',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2683',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3125',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3421',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3470',	Part_Number__c=parts[4].id));
        insert resses;*/
        
        //List<sObject> cores = Test.loadData(Core_Information__c.sObjectType, 'testDataCores');			//create cores test data
		//List<sObject> repairs = Test.loadData(Repair_Information__c.sObjectType, 'testDataRepairInfo');	//create repair info test data
        //List<sObject> resses = Test.loadData(Ress_Info__c.sObjectType, 'testDataRessInfo');				//create ress data
    }
    
    @isTest public static void createCommentsData(){
        List<sObject> accts = Test.loadData(Account.sObjectType, 'testDataAccounts');				//create account test data from static resource
        List<sObject> parts = Test.loadData(LLPDatabase__c.sObjectType, 'testDataPartNumbers');     //create part number test data from static resource csv file     
        
        //create cases test data
		List<Case> cases = new List<Case>();
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[1].id, SO__c='1', Phase__c='Back Order', Ecode__c='1'));
        insert cases;
        
        //create some comments data
        case c = [SELECT Id FROM Case WHERE SO__c='1'];
        List<Comments__c> comments = new List<Comments__c>();
        comments.add(new Comments__c(Department__c='PRC', Case_Number__c=c.id, Comments__c='test comment 1'));
        comments.add(new Comments__c(Department__c='POS', Case_Number__c=c.id, Comments__c='test comment 2'));
        insert comments;
    }
    
    @isTest public static void createAllData(){
        List<sObject> accts = Test.loadData(Account.sObjectType, 'testDataAccounts');				//create account test data from static resource
        List<sObject> parts = Test.loadData(LLPDatabase__c.sObjectType, 'testDataPartNumbers');     //create part number test data from static resource csv file     
        
        //create cases test data
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();	//get recordtypeid for "Pool Exchanges"
		List<Case> cases = new List<Case>();
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[1].id, SO__c='1', priority = 'AOG', Phase__c='Back Order', Ecode__c='1', RecordTypeId = rTypeId));
    	cases.add(new Case(AccountID=accts[1].id, Part_Number__c=parts[2].id, SO__c='2', priority = 'AOG', Phase__c='Back Order', Ecode__c='2', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[2].id, Part_Number__c=parts[3].id, SO__c='3', priority = 'AOG', Phase__c='Back Order', Ecode__c='3', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[0].id, SO__c='4', priority = 'AOG', Phase__c='Back Order', Ecode__c='4', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[1].id, Part_Number__c=parts[1].id, SO__c='5', priority = 'AOG', Phase__c='Back Order', Ecode__c='5', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[2].id, Part_Number__c=parts[0].id, SO__c='6', priority = 'AOG', Phase__c='Back Order', Ecode__c='6', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[4].id, SO__c='7', priority = 'AOG', Phase__c='Back Order', Ecode__c='7', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[0].id, Part_Number__c=parts[5].id, SO__c='8', priority = 'AOG', Phase__c='Back Order', Ecode__c='8', RecordTypeId = rTypeId));
        cases.add(new Case(AccountID=accts[2].id, Part_Number__c=parts[1].id, SO__c='9', priority = 'AOG', Phase__c='Back Order', Ecode__c='100', RecordTypeId = rTypeId));
        insert cases;
        
        //create core test data
        list<Core_Information__c> cores = new List<Core_Information__c>();
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
		cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[12].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[14].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[4].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[12].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        cores.add(new Core_Information__c(Part_Number__c=parts[19].id));
        insert cores;
	
        //create repair test data
        List<Repair_Information__c> repairs = new List<Repair_Information__c>();
		repairs.add(new Repair_Information__c(Name='1713029', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712792', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712798', Repair_Part_Number__c=parts[14].id));
        repairs.add(new Repair_Information__c(Name='1713131', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1713145', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712865', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712884', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712925', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712926', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712953', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712348', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712471', Repair_Part_Number__c=parts[19].id));
        repairs.add(new Repair_Information__c(Name='1712227', Repair_Part_Number__c=parts[0].id));
        repairs.add(new Repair_Information__c(Name='1712433', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712674', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712309', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712643', Repair_Part_Number__c=parts[4].id));
        repairs.add(new Repair_Information__c(Name='1712718', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712704', Repair_Part_Number__c=parts[12].id));
        repairs.add(new Repair_Information__c(Name='1712359', Repair_Part_Number__c=parts[12].id));
		insert repairs;
        
        //create ress test data
       /* List<RESS_Info__c> resses = new List<RESS_Info__c>();
        resses.add(new RESS_Info__c(RESS_ID__c='3748', Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3892', Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4077', Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4080',	Part_Number__c=parts[4].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4081',	Part_Number__c=parts[4].id));
        resses.add(new RESS_Info__c(RESS_ID__c='4117',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='16971',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='18855',	Part_Number__c=parts[4].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2121',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2581',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2582',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2676',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='2683',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3125',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3421',	Part_Number__c=parts[12].id));
        resses.add(new RESS_Info__c(RESS_ID__c='3470',	Part_Number__c=parts[4].id));
        insert resses;*/
        
        //create some comments through process builder
       case c = [SELECT id, CaseNumber, PRC_Comments_For_DOS__c, POS_Comments_For_DOS__c FROM Case WHERE SO__c='1'];
        c.POS_Comments_For_DOS__c='pos comment test';
        update c;
        c = [SELECT id, CaseNumber, PRC_Comments_For_DOS__c, POS_Comments_For_DOS__c FROM Case WHERE SO__c='1'];
        c.PRC_Comments_For_DOS__c='prc comment test';
        update c;
       /* c = [SELECT id, CaseNumber, PRC_Comments_For_DOS__c, POS_Comments_For_DOS__c, RESS_Comments__c FROM Case WHERE SO__c='1'];
        c.RESS_Comments__c='rm comment test';
        update c;*/
    }
}