public class CLMDeliveryForecastPDFController { 
    public Integer currentYear                                          {get;set;}
    public Integer today                                                {get;set;}
    public List<ProductionByMonth> lstProductionByMonth                 {get;set;}
    public List<Product2> lstProduction_By_Month                        {get;set;}
    public Set<String> aircraftsId                                      {get;set;}
    public String lineItensId                                           {get;set;}
    public String aircraftId                                            {get;set;}
    public String aircraftFamily                                        {get;set;}
    public Integer totalYearAvaliable                                   {get;set;}
    public Integer totalYearWithout                                     {get;set;}
    public Integer subTotalAvaliableYear190                             {get;set;}
    public Integer subTotalWithoutYear190                               {get;set;}
    public Integer subTotalAvaliableYear170                             {get;set;}
    public Integer subTotalWithoutYear170                               {get;set;}
    public Map<String, Integer> mapSubtotalYearAvaliable                {get;set;}
    public Map<String, Integer> mapSubtotalYearWithout                  {get;set;}
    public Date todayValue                                              {get;set;}
    public String todayRelat                                            {get;set;}
   
    public CLMDeliveryForecastPDFController(){
        //System.debug('CLMDeliveryForecastPDFController inicio');
        aircraftFamily = ApexPages.currentPage().getParameters().get('aircraftFamily');
        String year = ApexPages.currentPage().getParameters().get('year');
        try{
            currentYear = Integer.valueOf(year);
            today = System.Today().year();
            aircraftsId = new Set<String>();
            //System.debug('CLMDeliveryForecastPDFController.aircraftsId: ' + aircraftsId);
            fillThePage();
            
            //date pdf version
            todayValue = Date.today();
            todayRelat = string.valueof(todayValue.day());
            todayRelat+= '/';
            todayRelat+= string.valueof(todayValue.month());
            todayRelat+='/';
            todayRelat+= string.valueof(todayValue.year());
            
            
        } catch (Exception e){
            System.debug(e.getCause());
            System.debug(e.getStackTraceString());
        }
        //System.debug('CLMDeliveryForecastPDFController Fim');
    }
    
    public void getProductionByYear(Integer yearValue, Set<String> dfGroup){
        List<AggregateResult> results = ProductionCapabilityDAO.getProductionByYear(yearValue, dfGroup);
        
        lstProductionByMonth = new List<ProductionByMonth>();
        for (AggregateResult ar : results) {
            Boolean unique = true;
            for(productionByMonth production : lstProductionByMonth){
                if(production.Aircraft_Model.equalsIgnoreCase(String.valueOf(ar.get('Aircraft_Model__c')))){
                    unique = false;
                    break;
                }
            }
            if (unique){
                lstProductionByMonth.add(new ProductionByMonth(ar));
            }
        }
        
         //fill results with all models
        List<AggregateResult> lstAllModelsInProduct = [SELECT id, name, Aircraft_Family__c FROM product2 WHERE recordtype.developername = 'DCT_Aircraft' AND Aircraft_Family__c IN :dfGroup AND Show_In_DF__c = true GROUP BY name, id, Aircraft_Family__c];
        ProductionByMonth pByMonth = new productionByMonth();
        pByMonth.Aircraft_Model = '/';
        pByMonth.Aircraft_Model_Name = 'Total';
        
        for (AggregateResult ar : results) {
            Integer total = Integer.valueOf(ar.get('Total'));
            Integer month = (Integer) ar.get('Month');
            Integer year = (Integer) ar.get('Year');
            String model = (String) ar.get('Aircraft_Model__c');
            String name = (String) ar.get('Name');
            
            
            for(ProductionByMonth productionByMonth : lstProductionByMonth) {
                aircraftsId.add(productionByMonth.Aircraft_Model);
                if ((productionByMonth.Aircraft_Model_Name.equalsIgnoreCase(name)) && (productionByMonth.Year == year)) {
                    productionByMonth.Total += total;
                    pByMonth.total += total;
                    if (month == 1) {
                        productionByMonth.Jan = total;
                        pByMonth.jan += total;
                    } else if (month == 2) {
                        productionByMonth.Feb = total;
                        pByMonth.feb += total;
                    } else if (month == 3) {
                        productionByMonth.Mar = total;
                        pByMonth.mar += total;
                    } else if (month == 4) {
                        productionByMonth.Apr = total;
                        pByMonth.apr += total;
                    } else if (month == 5) {
                        productionByMonth.May = total;
                        pByMonth.may += total;
                    } else if (month == 6) {
                        productionByMonth.Jun = total;
                        pByMonth.jun += total;
                    } else if (month == 7) {
                        productionByMonth.Jul = total;
                        pByMonth.jul += total;
                    } else if (month == 8) {
                        productionByMonth.Aug = total;
                        pByMonth.aug += total;
                    } else if (month == 9) {
                        productionByMonth.Sep = total;
                        pByMonth.sep += total;
                    } else if (month == 10) {
                        productionByMonth.Oct = total;
                        pByMonth.oct += total;
                    } else if (month == 11) {
                        productionByMonth.Nov = total;
                        pByMonth.nov += total;
                    } else if (month == 12) {
                        productionByMonth.Dec = total;
                        pByMonth.dec += total;
                    }
                }
            }
        }
        lstProductionByMonth.add(pByMonth);
    }
    
    public class ProductionByMonth {
        public String Aircraft_Model        { get; private set; }
        public String Aircraft_Model_Name   { get; private set; }
        public Integer Total                { get; private set; }
        public Integer Jan                  { get; private set; }
        public Integer Feb                  { get; private set; }
        public Integer Mar                  { get; private set; }
        public Integer Apr                  { get; private set; }
        public Integer May                  { get; private set; }
        public Integer Jun                  { get; private set; }
        public Integer Jul                  { get; private set; }
        public Integer Aug                  { get; private set; }
        public Integer Sep                  { get; private set; }
        public Integer Oct                  { get; private set; }
        public Integer Nov                  { get; private set; }
        public Integer Dec                  { get; private set; }
        public Integer Year                 { get; private set; }
        public Integer Month                { get; private set; }
    
        public ProductionByMonth(){
            Jan = 0;
            Feb = 0;
            Mar = 0;
            Apr = 0;
            May = 0;
            Jun = 0;
            Jul = 0;
            Aug = 0;
            Sep = 0;
            Oct = 0;
            Nov = 0;
            Dec = 0;
            Total = 0;
        }
        //Boas práticas da EMBRAER pedem variáveis com nomes auto explicativos
        public ProductionByMonth(AggregateResult aggResult) {
            Year = Integer.valueOf(aggResult.get('Year'));
            Month = Integer.valueOf(aggResult.get('Month'));
            Aircraft_Model = String.valueOf(aggResult.get('Aircraft_Model__c'));
            Aircraft_Model_Name = String.valueOf(aggResult.get('Name'));
            Jan = 0;
            Feb = 0;
            Mar = 0;
            Apr = 0;
            May = 0;
            Jun = 0;
            Jul = 0;
            Aug = 0;
            Sep = 0;
            Oct = 0;
            Nov = 0;
            Dec = 0;
            Total = 0;
        }
    }
    
   /*========LÓGICA DE PREENCHIMENTO DAS TABELAS========*/
    public List<Agreement_Aircraft__c> agreementLineItens                {get;set;}
    public Map<String, LineItemView> mapLineItemDetails                         {get;set;}
    public Map<String, String> mapAircraftNames                                 {get;set;}
    public Map<String, Subtotal> mapSubtotal                                    {get;set;}
    public Integer [] subtotalAvaiable                                          {get;set;}
    public Integer [] subtotalWithoutOp                                         {get;set;}
    public map<String, Production_Capability__c> mapModeloCapability            {get;set;}
    public InformationByMonthTotals avaiableTotal                               {get;set;}
    public InformationByMonthTotals withoutTotal                                {get;set;}
    public Set<String> groups                                                   {get;set;}
    public Map<String,Map<String, LineItemView>> mapGroupDetail                 {get;set;}

       
    public void fillThePage(){
        //System.debug('CLMDeliveryForecastController.fillThePage início');
        lstProduction_By_Month = new List<Product2>();
        lstProduction_By_Month = Product2DAO.getInstance().getAllProducts(aircraftFamily);
        Set<String> setStr = new Set<String>();
        system.debug('AIRCRAFT FAMILY>>> ' +aircraftFamily);
        setStr.add(aircraftFamily);
        //System.debug('CLMDeliveryForecastController.lstProduction_By_Month: ' + lstProduction_By_Month);
        getProductionByYear(currentYear, setStr);
        subtotalAvaiable = new Integer[12];
        subtotalWithoutOp = new Integer[12];
        mapSubtotal = new Map<String, SubTotal>();
        mapSubtotalYearAvaliable = new Map<String, Integer>();
        mapSubtotalYearWithout = new Map<String, Integer>();
        subTotalAvaliableYear190 = 0;
        subTotalWithoutYear190 = 0;
        subTotalAvaliableYear170 = 0;
        subTotalWithoutYear170 = 0;
        totalYearAvaliable = 0;
        totalYearWithout = 0;
        mapLineItemDetails = new Map<String, LineItemView>();
        mapAircraftNames = new Map<String, String>();
        mapModeloCapability = new Map<String, Production_Capability__c>();
        mapAircraftNames.put('170/175', '');
        mapAircraftNames.put('190/195', '');
        avaiableTotal = new InformationByMonthTotals();
        withoutTotal = new InformationByMonthTotals();
        groups = new Set<String>();
        mapGroupDetail = new Map<String, Map<String, LineItemView>>();

        try {
            agreementLineItens = getAgreementLineItens();
            getCapability(agreementLineItens);
            
            for (Agreement_Aircraft__c lineItem : agreementLineItens)
            {
                if ( !groups.contains(lineItem.Aircraft__r.DF_Group__c) )
                {
                    groups.add(lineItem.Aircraft__r.DF_Group__c);
                }
                String resultTotalLine = '';
                resultTotalLine = applyRules(lineItem);
                fillMapSubTotal(lineItem); 
            }
            
            for (String groupName : groups)
            {
                mapGroupDetail.put(groupName, getMapDetailForGroup( groupName, agreementLineItens ));
            }
            
            //system.debug('MAP GROUP DETAIL: ' +mapGroupDetail);
            
            /*for (Apttus__AgreementLineItem__c lineItem : agreementLineItens){
                LineItemView lineItemView = fillLineItemView(lineItem);
                //System.debug('CLMAircraftTableComponentController.lineItemView: ' + lineItemView);
                mapLineItemDetails.put(lineItem.Aircraft__r.name, lineItemView);
                fillMapSubTotal(lineItem);
            }*/
        } catch (Exception e){
            System.debug(e.getCause());
            System.debug(e.getStackTraceString());
        }
        //System.debug('CLMDeliveryForecastController.aircraftFamily: ' + aircraftFamily);
    }
    
    private Map<String, LineItemView> getMapDetailForGroup ( String groupName, List<Agreement_Aircraft__c> agreementLineItens )
    {
        Map<String, LineItemView> map_aux = new Map<String, LineItemView>(); 
        
        for (Agreement_Aircraft__c lineItem : agreementLineItens)
        {
            if ( groupName ==  lineItem.Aircraft__r.DF_Group__c )
            {
                LineItemView lineItemView = fillLineItemView(lineItem);
                mapLineItemDetails.put(lineItem.Aircraft__r.name, lineItemView);
                map_aux.put(lineItem.Aircraft__r.name, lineItemView);
            }               
        }
        
        return map_aux;
    }
    
   public void fillMapSubTotal(Agreement_Aircraft__c lineItem){
        system.debug('INICIO FillSubtotal');
        Subtotal subtotal = calculateSubTotal(lineItem);
        String dfGrouping = lineItem.Aircraft__r.DF_Grouping__c;
        //system.debug('Variable dfGrouping: ' +dfGrouping);
        //mapAircraftNames.put(dfGrouping, '');
        //
        if(!mapAircraftNames.containsKey(dfGrouping)){
            mapAircraftNames.put(dfGrouping, '');
        }
        mapSubtotal.put(dfGrouping, subtotal);
        String aircraftNames = mapAircraftNames.get(dfGrouping);
        
        if(lineItem.Aircraft__r.DF_Grouping__c != null){
        
        if(!aircraftNames.contains(lineItem.Aircraft__r.Name)){
            aircraftNames += ' / ' + lineItem.Aircraft__r.name;    
        }
        aircraftNames = aircraftNames.removeStart(' / ');
            

            String teste = mapAircraftNames.get(dfGrouping);

        mapAircraftNames.put(dfGrouping, aircraftNames);
         
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Field DF_Grouping__c is null and it may affect the total. Product ID: ' + lineItem.Aircraft__r.ID));
        }
        fillTotals();
    }
    
    public void fillTotals (){
        
        avaiableTotal = new InformationByMonthTotals();
        withoutTotal = new InformationByMonthTotals();
        
        for (String dfGrouping  : mapSubtotal.keySet()) {
            Subtotal avaliableTotalLine = mapSubtotal.get (dfGrouping);
            Subtotal withoutTotalLine = mapSubtotal.get (dfGrouping);
            
            avaiableTotal.jan += avaliableTotalLine.avaiable.jan;
            avaiableTotal.fev += avaliableTotalLine.avaiable.fev;
            avaiableTotal.mar += avaliableTotalLine.avaiable.mar;
            avaiableTotal.apr += avaliableTotalLine.avaiable.apr;
            avaiableTotal.may += avaliableTotalLine.avaiable.may;
            avaiableTotal.jun += avaliableTotalLine.avaiable.jun;
            avaiableTotal.jul += avaliableTotalLine.avaiable.jul;
            avaiableTotal.aug += avaliableTotalLine.avaiable.aug;
            avaiableTotal.sep += avaliableTotalLine.avaiable.sep;
            avaiableTotal.oct += avaliableTotalLine.avaiable.oct;
            avaiableTotal.nov += avaliableTotalLine.avaiable.nov;
            avaiableTotal.dec += avaliableTotalLine.avaiable.dec;
            
            withoutTotal.jan += withoutTotalLine.without.jan;
            withoutTotal.fev += withoutTotalLine.without.fev;
            withoutTotal.mar += withoutTotalLine.without.mar;
            withoutTotal.apr += withoutTotalLine.without.apr;
            withoutTotal.may += withoutTotalLine.without.may;
            withoutTotal.jun += withoutTotalLine.without.jun;
            withoutTotal.jul += withoutTotalLine.without.jul;
            withoutTotal.aug += withoutTotalLine.without.aug;
            withoutTotal.sep += withoutTotalLine.without.sep;
            withoutTotal.oct += withoutTotalLine.without.oct;
            withoutTotal.nov += withoutTotalLine.without.nov;
            withoutTotal.dec += withoutTotalLine.without.dec;
            
        }      
    }
    
    public Subtotal calculateSubTotal(Agreement_Aircraft__c lineItem){
        //System.debug('calculateSubTotal.lineItem: ' + lineItem);
        String resultRule = '';
        resultRule = applyRules(lineItem);
        //System.debug('AircraftTable.resultRule: ' + resultRule);
        if(mapSubtotal.isEmpty()){
            initializeSubtotal();
        }
        Double capability;
        Subtotal subTotal = mapSubtotal.get(lineItem.Aircraft__r.DF_Grouping__c);
        InformationByMonthTotals subTotalAvaiable = new InformationByMonthTotals();
        InformationByMonthTotals subTotalWithout = new InformationByMonthTotals();
        Integer month = 0;
        if (resultRule.contains('Trend')){
            month = discoverMonth(lineItem.TREND_Delivery_Date__c);
            if(month != 0 && mapModeloCapability.get(lineItem.Aircraft__r.Name+lineItem.TREND_Delivery_Date__c.month()) != null){
                capability = mapModeloCapability.get(lineItem.Aircraft__r.Name+lineItem.TREND_Delivery_Date__c.month()).production_capability__c;    
            }
        } else {
            month = discoverMonth(lineItem.Contractual_Delivery_Month__c);
            if(month != 0 && mapModeloCapability.get(lineItem.Aircraft__r.Name+lineItem.Contractual_Delivery_Month__c.month()) != null){
                capability = mapModeloCapability.get(lineItem.Aircraft__r.Name+lineItem.Contractual_Delivery_Month__c.month()).production_capability__c;   
            }
        }
        if(capability == null){
            capability = 0;
        }
        Integer resultSubTotalAvaiable = 0;
        Integer resultSubTotalOption = 0;
        if (subTotal != null){
            subTotalAvaiable = subTotal.avaiable;
            subTotalWithout = subTotal.without;
            resultSubTotalAvaiable = returnValueFromMonth(month, subTotalAvaiable);
            resultSubTotalOption = returnValueFromMonth(month, subTotalWithout);
        } else {
            subtotal = new Subtotal();
        }
        
        if(resultRule.contains('Trend') || resultRule.contains('Option')){
            if(resultRule.contains('Trend')){
                if (resultSubTotalOption == 0) {
                    resultSubTotalOption = Integer.valueOf(capability) - 1;
                } else {
                    resultSubTotalOption--;
                }
                if(resultSubTotalAvaiable == 0){
                    resultSubTotalAvaiable = Integer.valueOf(capability) - 1;
                } else {
                    resultSubTotalAvaiable--;
                }
            } else {
                if(resultSubTotalAvaiable == 0){
                    resultSubTotalAvaiable = Integer.valueOf(capability) - 1;
                } else {
                    resultSubTotalAvaiable--;
                }
            }
        }
        
        //System.debug('AircraftTable.resultSubTotalOption: ' + resultSubTotalOption);
        //System.debug('AircraftTable.resultSubTotalAvaiable: ' + resultSubTotalAvaiable);
        subTotalAvaiable = setValueToMonth(month, subTotalAvaiable, resultSubTotalAvaiable);
        subTotalWithout = setValueToMonth(month, subTotalWithout, resultSubTotalOption);
        subTotal.avaiable = subTotalAvaiable;
        subtotal.without = subTotalWithout;
        setTotals(subtotal.avaiable, avaiableTotal, month);
        setTotals(subtotal.without, withoutTotal, month);
        return subtotal;
    }
    
    public void initializeSubtotal(){
        Set<String> keySet = mapModeloCapability.keySet();
        for (String key : keySet){
            Production_Capability__c pc = mapModeloCapability.get(key);
            Integer month = discoverMonth(pc.Delivery_s_Date__c);
            Subtotal subtotal;
            InformationByMonthTotals infoByMonth = new InformationByMonthTotals();
            if(String.isNotBlank(pc.Aircraft_Model__r.DF_Group__c)){
                
                subtotal = mapSubtotal.get(pc.Aircraft_Model__r.DF_Group__c);
                subtotal = populateSubTotal(subtotal, infoByMonth, pc, month);
                mapSubtotal.put(pc.Aircraft_Model__r.DF_Group__c, subtotal);
            }            
        }
    }
    
    public Subtotal populateSubTotal(Subtotal subtotal, InformationByMonthTotals infoByMonth, Production_Capability__c pc, Integer month){
       if(subtotal == null){
            subtotal = new Subtotal();
            infoByMonth = setValueToMonth(month, infoByMonth, Integer.valueOf(pc.Production_Capability__c));
            subtotal.avaiable = infoByMonth;
            subtotal.without = infoByMonth.clone();
        } else {
            infoByMonth = new InformationByMonthTotals(month, Integer.valueOf(pc.Production_Capability__c));
            setTotals(infoByMonth, subtotal.avaiable, month);
            subtotal.without = subtotal.avaiable.clone();
        }
        return subtotal;
    }
    
    public LineItemView fillLineItemView (Agreement_Aircraft__c lineItem){
        String resultRules = applyRules(lineItem);
        String lineItemViewParam = '';
        LineItemViewParam = resultRules+';'+ 
            discoverMonth(lineItem.Contractual_Delivery_Month__c) + ',' + lineItem.DF_Code__c + ';' + 
            discoverMonth(lineItem.TREND_Delivery_Date__c) + ',' + lineItem.DF_Code__c;
        return new LineItemView(lineItemViewParam, mapLineItemDetails.get(lineItem.Aircraft__r.Name), lineItem);
    }
    
    public String applyRules(Agreement_Aircraft__c lineItem){
        String result = '';
        if(lineItem.Show_Aircraft_As__c != null){
            if(lineItem.Show_Aircraft_As__c.equalsIgnoreCase('Option')){
                result += 'Option|';
            } else if (lineItem.Show_Aircraft_As__c.equalsIgnoreCase('Firm')){
                result += 'Trend|';
                if(lineItem.Agreement__r.Status_Category__c.equalsIgnoreCase('PA Signed')){
                    result +='Firm|';
                }    
            } else if (lineItem.Show_Aircraft_As__c.equalsIgnoreCase('Proposal')){
                result += 'Proposal|';
            } else if(lineItem.Show_Aircraft_As__c.equalsIgnoreCase('Hide')){
                result += 'Hide|';
            }                
        } else {
            if(lineItem.Agreement__r.RecordType.name != null &&
               lineItem.Agreement__r.RecordType.name.equalsIgnoreCase('Purchase Agreement')){
                   if(lineItem.Agreement__r.Status_Category__c.equalsIgnoreCase('PA Signed')){
                       if(lineItem.Order_Type__c.equalsIgnoreCase('Firm')){ 
                           result += 'Trend|';
                           result += 'Firm|';
                       } else if (lineItem.Order_Type__c.equalsIgnoreCase('Option')){
                           result += 'Option|';
                       }
                   } else if (lineItem.Agreement__r.Status_Category__c.equalsIgnoreCase('PA Request') ||
                             lineItem.Agreement__r.Status_Category__c.equalsIgnoreCase('PA Pending Approval') ||
                             lineItem.Agreement__r.Status_Category__c.equalsIgnoreCase('PA Approved')){
                                 if(lineItem.Order_Type__c.equalsIgnoreCase('Firm') || lineItem.Order_Type__c.equalsIgnoreCase('Option')){result += 'Option|';}
                   }
               } else if (lineItem.Agreement__r.RecordType.name != null &&
                             lineItem.Agreement__r.RecordType.name.equalsIgnoreCase('Proposal')){
                   if(lineItem.Agreement__r.Status_Category__c.equalsIgnoreCase('Proposal Signed')){    
                       if(lineItem.Order_Type__c.equalsIgnoreCase('Firm') || lineItem.Order_Type__c.equalsIgnoreCase('Option')){
                           result += 'Trend|';
                           result += 'Option|';    
                       }
                   } else {
                       if (lineItem.Order_Type__c.equalsIgnoreCase('Firm') || lineItem.Order_Type__c.equalsIgnoreCase('Option')){
                           result += 'Proposal|';
                       }
                   }
               } 
        }
        //System.debug('result: ' + result);
        return result; 
    }
    
    public Integer discoverMonth (Date data){
        if(data != null){
            return data.month(); 
        }
        return 0;
    }
    
    public void setTotals (InformationByMonthTotals totalsFrom, InformationByMonthTotals totalsTo, Integer month){
        //System.debug('AircraftTable.setTotals.totalsFrom: ' + totalsFrom);
        //System.debug('AircraftTable.setTotals.totalsTo: ' + totalsTo);

        if(month == 1 && totalsFrom.jan != null){totalsTo.jan += totalsFrom.jan;}
        if(month == 2 && totalsFrom.fev != null){totalsTo.fev += totalsFrom.fev;}
        if(month == 3 && totalsFrom.mar != null){totalsTo.mar += totalsFrom.mar;}
        if(month == 4 && totalsFrom.apr != null){totalsTo.apr += totalsFrom.apr;}
        if(month == 5 && totalsFrom.may != null){totalsTo.may += totalsFrom.may;}
        if(month == 6 && totalsFrom.jun != null){totalsTo.jun += totalsFrom.jun;}
        if(month == 7 && totalsFrom.jul != null){totalsTo.jul += totalsFrom.jul;}
        if(month == 8 && totalsFrom.aug != null){totalsTo.aug += totalsFrom.aug;}
        if(month == 9 && totalsFrom.sep != null){totalsTo.sep += totalsFrom.sep;}
        if(month == 10 && totalsFrom.oct != null){totalsTo.oct += totalsFrom.oct;}
        if(month == 11 && totalsFrom.nov != null){totalsTo.nov += totalsFrom.nov;}
        if(month == 12 && totalsFrom.dec != null){totalsTo.dec += totalsFrom.dec;}
    }
    
    public Integer returnValueFromMonth(Integer month, InformationByMonthTotals subtotal){
        if (month == 1){return subtotal.jan;}
        else if (month == 2){return subtotal.fev;}
        else if (month == 3){return subtotal.mar;}
        else if (month == 4){return subtotal.apr;}
        else if (month == 5){return subtotal.may;}
        else if (month == 6){return subtotal.jun;}
        else if (month == 7){return subtotal.jul;}
        else if (month == 8){return subtotal.aug;}
        else if (month == 9){return subtotal.sep;}
        else if (month == 10){return subtotal.oct;}
        else if (month == 11){return subtotal.nov;}
        else if (month == 12){return subtotal.dec;}
        return 0;
    }
    
    public InformationByMonthTotals setValueToMonth(Integer month, InformationByMonthTotals subtotal, Integer value){
        if(value == null){return subtotal;}
        if (month == 1){subtotal.jan = value;}
        else if (month == 2){subtotal.fev = value;}
        else if (month == 3){subtotal.mar = value;}
        else if (month == 4){subtotal.apr = value;}
        else if (month == 5){subtotal.may = value;}
        else if (month == 6){subtotal.jun = value;}
        else if (month == 7){subtotal.jul = value;}
        else if (month == 8){subtotal.aug = value;}
        else if (month == 9){subtotal.sep = value;}
        else if (month == 10){subtotal.oct = value;}
        else if (month == 11){subtotal.nov = value;}
        else if (month == 12){subtotal.dec = value;}
        return subtotal;
    }
    
    public List<Agreement_Aircraft__c> getAgreementLineItens(){
        //System.debug('AircraftTable.lineItensId:' + aircraftsId);
        List<Agreement_Aircraft__c> lineItens = new List<Agreement_Aircraft__c>();
        try {
            lineItens = AgreementLineItemDAO.getLineItensBySetIds(aircraftsId, currentYear);
        } catch (Exception e){
            System.debug(e.getCause());
            System.debug(e.getStackTraceString());
        }
        //System.debug('AircraftTable.lineItens:' + lineItens);
        return lineItens;
    }
    
    public void getCapability(List<Agreement_Aircraft__c> lineItens){
        Set<String> setIds = new Set<String>();
        setIds.add(aircraftFamily);
        List<Production_Capability__c> pcs = ProductionCapabilityDAO.getCapabilityByAircraftFamily(currentYear, setIds);
        for (Production_Capability__c pc : pcs){
            /* System.debug('AircraftTable.pc: ' + pc);
            System.debug('AircraftTable.pc: ' + pc.Aircraft_Model__r.Name);
            System.debug('AircraftTable.pc: ' + pc.Production_Capability__c);
            System.debug('AircraftTable.mapModeloCapability: ' + mapModeloCapability);*/
            //System.debug('pc.Aircraft_Model__r.Name+pc.Delivery_s_Date__c.month(): ' + pc.Aircraft_Model__r.Name+pc.Delivery_s_Date__c.month());
            mapModeloCapability.put(pc.Aircraft_Model__r.Name+pc.Delivery_s_Date__c.month(), pc);
        }
        //System.debug('AircraftTable.getCapability.mapModeloCapability: ' + mapModeloCapability);
    }
    
    public class Subtotal {
        public InformationByMonthTotals avaiable    {get;private set;}
        public InformationByMonthTotals without     {get;private set;}
        public Subtotal(){}
    }
    
    public class LineItemView {
        public InformationByMonth contract      { get; private set; }
        public InformationByMonth trend         { get; private set; }
        public InformationByMonth options       { get; private set; }
        public InformationByMonth proposal      { get; private set; }
            
        public LineItemView (String constructorParams, LineItemView lineItem, Agreement_Aircraft__c lineItemObj){
            //System.debug('LineItemView.constructor: ' + constructorParams + ' ' + lineItem);
            List<String> parameters = constructorParams.split(';');
            if (lineItem != null){
                this.contract = lineItem.contract;
                this.trend = lineItem.trend;
                this.options = lineItem.options;
                this.proposal = lineItem.proposal;
            }
            if (parameters.get(0).contains('Firm')){
                contract = new InformationByMonth(parameters.get(1), contract, lineItemObj);
            }
            if (parameters.get(0).contains('Trend')){
                trend = new InformationByMonth(parameters.get(2), trend, lineItemObj);
            }
            if (parameters.get(0).contains('Option')){
                options = new InformationByMonth(parameters.get(1), options, lineItemObj);
            }
            if (parameters.get(0).contains('Proposal')){
                proposal = new InformationByMonth(parameters.get(1), proposal, lineItemObj);
            }
        }
    }
    
    public class InformationByMonth {
        public List<Agreement_Aircraft__c> jan { get;set; }
        public List<Agreement_Aircraft__c> fev { get;set; }
        public List<Agreement_Aircraft__c> mar { get;set; }
        public List<Agreement_Aircraft__c> apr { get;set; }
        public List<Agreement_Aircraft__c> may { get;set; }
        public List<Agreement_Aircraft__c> jun { get;set; }
        public List<Agreement_Aircraft__c> jul { get;set; }
        public List<Agreement_Aircraft__c> aug { get;set; }
        public List<Agreement_Aircraft__c> sep { get;set; }
        public List<Agreement_Aircraft__c> oct { get;set; }
        public List<Agreement_Aircraft__c> nov { get;set; }
        public List<Agreement_Aircraft__c> dec { get;set; }
        
        public InformationByMonth (String constructorParams, InformationByMonth oldObj, Agreement_Aircraft__c lineItem){
            //System.debug('Constructor informationByMonth: ' + constructorParams + ' ' + oldObj);
            if (oldObj != null){
                this.jan = oldObj.jan;
                this.fev = oldObj.fev;
                this.mar = oldObj.mar;
                this.apr = oldObj.apr;
                this.may = oldObj.may;
                this.jun = oldObj.jun;
                this.jul = oldObj.jul;
                this.aug = oldObj.aug;
                this.sep = oldObj.sep;
                this.oct = oldObj.oct;
                this.nov = oldObj.nov;
                this.dec = oldObj.dec;
            } else {
                jan = new List<Agreement_Aircraft__c>();
                fev = new List<Agreement_Aircraft__c>();
                mar = new List<Agreement_Aircraft__c>();
                apr = new List<Agreement_Aircraft__c>();
                may = new List<Agreement_Aircraft__c>();
                jun = new List<Agreement_Aircraft__c>();
                jul = new List<Agreement_Aircraft__c>();
                aug = new List<Agreement_Aircraft__c>();
                sep = new List<Agreement_Aircraft__c>();
                oct = new List<Agreement_Aircraft__c>();
                nov = new List<Agreement_Aircraft__c>();
                dec = new List<Agreement_Aircraft__c>();
            }
            List<String> parameters = constructorParams.split(',');
            //System.debug('Constructor parameters: ' + parameters);
            if(parameters.get(0).equalsIgnoreCase('1')){jan.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('2')){fev.add(lineItem);}
            else if(parameters.get(0).equalsIgnoreCase('3')){mar.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('4')){apr.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('5')){may.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('6')){jun.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('7')){jul.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('8')){aug.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('9')){sep.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('10')){oct.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('11')){nov.add(lineItem);} 
            else if(parameters.get(0).equalsIgnoreCase('12')){dec.add(lineItem);}
        }
        
        public InformationByMonth(){}
    }
    
    public class InformationByMonthTotals{
        public Integer jan { get;set; }
        public Integer fev { get;set; }
        public Integer mar { get;set; }
        public Integer apr { get;set; }
        public Integer may { get;set; }
        public Integer jun { get;set; }
        public Integer jul { get;set; }
        public Integer aug { get;set; }
        public Integer sep { get;set; }
        public Integer oct { get;set; }
        public Integer nov { get;set; }
        public Integer dec { get;set; }
        
        public InformationByMonthTotals(){
            jan = 0; fev = 0; mar = 0; apr = 0; may = 0; jun = 0;
            jul = 0; aug = 0; sep = 0; oct = 0; nov = 0; dec = 0;
        }
        
        public InformationByMonthTotals(Integer month, Integer value){
            if(month==1){jan = value;}
            else if(month==2){fev = value;}
            else if(month==3){mar = value;}
            else if(month==4){apr = value;}
            else if(month==5){may = value;}
            else if(month==6){jun = value;}
            else if(month==7){jul = value;}
            else if(month==8){aug = value;}
            else if(month==9){sep = value;}
            else if(month==10){oct = value;}
            else if(month==11){nov = value;}
            else {dec = value;}
        }
    }
}