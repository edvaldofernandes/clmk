/* Classe implementadora de SOBjectDAO para operações DML no objeto AccountTeamMember, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 14/01/2016
*/
public without sharing class AccountTeamMemberDao {
    
    private static final AccountTeamMemberDao instance = new AccountTeamMemberDao();    
    
    private AccountTeamMemberDao(){
    }    
    
    public static AccountTeamMemberDao getInstance(){
        return instance;
    }
    
    public List<AccountTeamMember> listByTeamMemberRole(String teamMemberRole){
        List<AccountTeamMember> lstAccountTeamMember = database.query(' Select ' + utils.getAllFields('AccountTeamMember') + ' FROM AccountTeamMember' +
                                                        + ' WHERE TeamMemberRole =\''+String.escapeSingleQuotes(teamMemberRole)+'\' ');        
        return lstAccountTeamMember;
    }
    
}