// Used by DIM - Market Intelligence.
@isTest
public class DIM_CaseTypeControllerTest {

    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateReturn() {
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        System.assertEquals('Sales Support', DIM_CaseTypeController.getCaseRecordType(caso.Id).RecordType.Name);
    }
}