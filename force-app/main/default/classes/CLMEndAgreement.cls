public class CLMEndAgreement {
    
    public static final String EXPIRED = 'Expired';
    public static final String CANCELLED = 'Cancelled';
    public static final String TERMINATED = 'Terminated';
    public static final String DELIVERED = 'Delivered';
    public static final String PAID = 'Paid';
    public static final String CONVERTED = 'Converted';
    public static final String FIRM = 'Firm';
    

    public static void expiredAgreement(Agreement__c agreement){
        System.debug('CLMExpiredAgreement.expiredAgreement.agreement: ' + agreement);

        CLMUpdateAgreement clmUpAgg = new CLMUpdateAgreement(new List<Agreement__c>{agreement});
        clmUpAgg.updateAgreementStatus(EXPIRED, EXPIRED);
        clmUpAgg.updateAgreementAircraftStatus(EXPIRED,new Set<String>{DELIVERED});
        clmUpAgg.updateAgreementAircraftPDPStatus(CANCELLED,CANCELLED);
        clmUpAgg.deleteAgreementTasks();
    }
    
    public static void terminateAgreement(List<Agreement__c> agreement){

        CLMUpdateAgreement clmUpAgg = new CLMUpdateAgreement(agreement);
        clmUpAgg.updateAgreementStatus(TERMINATED, TERMINATED);
        clmUpAgg.updateAgreementAircraftStatus(TERMINATED,new Set<String>{DELIVERED,CONVERTED,EXPIRED,TERMINATED});
        clmUpAgg.updateAgreementAircraftPDPStatus(CANCELLED,PAID);
        clmUpAgg.deleteAgreementTasks();
    }
}