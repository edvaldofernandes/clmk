public class PlanningReportFLLController {
    //lists of our main data
    Public List<Stock_Info_FLL__c> StockInfos{get;set;}
    Public List<Repair_Information__c> RepairInfos{get;set;}
    Public Repair_Information__c CurrentRepair{get;set;}
    Public List<Core_Information__c> CoreInfos{get;set;}
    Public List<RESS_Info__c> RessInfos{get;set;}
    Public List<QM__c> QMInfos{get;set;}
    Public List<PO__c> POInfos{get;set;}
    Public Core_Information__c CurrentCore{get;set;}
    Public QM__c CurrentQM{get;set;}
    Public PO__c CurrentPO{get;set;}
    
    //various string public members
    Public String UserNotesNotification{get;set;}
    Public String UserNotesContent{get;set;}
    Public String MultiSearchInput{get;set;}
    Public String SelectedTopmost{get;set;}
    Public String Anchor{get;set;}
    Public String CoreCommentBody{get;set;}
    Public String POCommentBody{get;set;}
    Public String POIndex{get;set;}
    Public String CoreIndex{get;set;}
    Public String QMCommentBody{get;set;}
    Public String QMIndex{get;set;}
    Public String RepairCommentBody{get;set;}
    Public String RepairIndex{get;set;}
    
    
    //flags
    Public Boolean IsNoMatch{get;set;}
    Public Boolean ShowCores{get;set;}
    Public Boolean ShowPartsAtRepair{get;set;}
    Public Boolean ShowRESS{get;set;}
    Public Boolean ShowPO{get;set;}
    Public Boolean ShowQM{get;set;}
    Public Boolean isE2Mode{get;set;}
    Public Boolean isE2ModeLBG{get;set;}
    
    //Totals class and Usage Info
    Public PlanningFLLTotals Totals{get;set;}
    Public List<PlanningReportUtilities.UsageInfoWrapper> UsageInfoPool{get;set;}
    Public List<PlanningReportUtilities.UsageInfoWrapper> UsageInfoRepAdmin{get;set;}
    Public List<PlanningReportUtilities.UsageInfoWrapper> UsageInfoEPEP{get;set;}
    Public Map<String, Date> Last24MonthsMap{get;set;}
    
    //advanced search section
    Public String RegionAndSegment{get;set;}
    public List<Stock_Info_FLL__c> AdvancedSearchList{get;set;} 
    
    //constructor
    Public PlanningReportFLLController(){
        StockInfos = new List<Stock_Info_FLL__c>();
        POInfos = new List<PO__c>();
       	RepairInfos = new List<Repair_Information__c>();
       	CoreInfos = new List<Core_Information__c>();
        RessInfos = new List<RESS_Info__c>();
        QMInfos = new List<QM__c>();
        IsNoMatch = false;
        IsE2Mode = false;
        isE2ModeLBG = FALSE;
        RegionAndSegment = (ApexPages.currentPage().getParameters().containsKey('RegionAndSegment')) ? ApexPages.currentPage().getParameters().get('RegionAndSegment') : 'FLL COM';
       
        if(RegionAndSegment == 'E2'){
            RegionAndSegment = 'FLL COM';
            IsE2Mode = true;
        }else if(RegionAndSegment == 'E2LBG'){
            RegionAndSegment = 'LBG COM';
            isE2ModeLBG = true;
        } else iF(RegionAndSegment == 'E2Mode'){
            RegionAndSegment = 'E2';
           
        } else if (RegionAndSegment == 'E2ModeLBG'){
            RegionAndSegment = 'E2LBG';
            
        }
        ShowCores = (ApexPages.currentPage().getParameters().get('Anchor')=='Cores');
        ShowPartsAtRepair = (ApexPages.currentPage().getParameters().get('Anchor')=='PartsAtRepair');
        ShowRESS = (ApexPages.currentPage().getParameters().get('Anchor')=='RESS');
        ShowQM = (ApexPages.currentPage().getParameters().get('Anchor')=='QM');
        ShowPO = (ApexPages.currentPage().getParameters().get('Anchor')=='PO');
        
        AdvancedSearchList = PlanningReportUtilities.getAdvancedSearchResults(RegionAndSegment);	//init advanced search results
        SelectedTopmost = ApexPages.currentPage().getParameters().get('topmost');					//get topmost from url param
        if(!String.isBlank(SelectedTopmost))
        	GetPlanningDataByTopmost();																//init planning page data
    }
    
    //set all the data for the planning report page
    Private void getPlanningDataByTopmost(){
        RegionAndSegment = (RegionAndSegment == 'E2') ? 'FLL COM' : RegionAndSegment;
        IF (RegionAndSegment == 'FLL COM') {
        String region = RegionAndSegment.left(3);
        String segmentWildcard = RegionAndSegment.right(3)+'%';
        StockInfos = PlanningReportUtilities.getStockInfosByRegionAndSegment(SelectedTopmost, RegionAndSegment);
        RepairInfos = PlanningReportUtilities.getRepairInfosByRegionAndSegment(SelectedTopmost, RegionAndSegment);
        CoreInfos = PlanningReportUtilities.getCoreInfosByRegionAndSegment(SelectedTopmost, region, segmentWildcard);
        RessInfos = PlanningReportUtilities.getRessInfosByRegionAndSegment(SelectedTopmost, region, segmentWildcard);
        POInfos = PlanningReportUtilities.getPOInfosByRegionAndSegment(SelectedTopmost, region, segmentWildcard);    
        QMInfos = PlanningReportUtilities.getQMInfosByRegion(SelectedTopmost, region);
        Totals = new PlanningFLLTotals(StockInfos, RegionAndSegment);
        UsageInfoPool = PlanningReportUtilities.getUsageInformationPool(SelectedTopmost, region);
        UsageInfoRepAdmin = PlanningReportUtilities.getUsageInformationRepAdmin(SelectedTopmost, region);
        UsageInfoEPEP = PlanningReportUtilities.getUsageInformationEPEP(SelectedTopmost, region);
        Last24MonthsMap = PlanningReportUtilities.getLast24MonthsMap();
        } ELSE RegionAndSegment = (RegionAndSegment == 'E2LBG') ? 'LBG COM' : RegionAndSegment; 
        
        String region = RegionAndSegment.left(3);
        String segmentWildcard = RegionAndSegment.right(3)+'%';
        StockInfos = PlanningReportUtilities.getStockInfosByRegionAndSegment(SelectedTopmost, RegionAndSegment);
        RepairInfos = PlanningReportUtilities.getRepairInfosByRegionAndSegment(SelectedTopmost, RegionAndSegment);
        CoreInfos = PlanningReportUtilities.getCoreInfosByRegionAndSegment(SelectedTopmost, region, segmentWildcard);
        RessInfos = PlanningReportUtilities.getRessInfosByRegionAndSegment(SelectedTopmost, region, segmentWildcard);
        POInfos = PlanningReportUtilities.getPOInfosByRegionAndSegment(SelectedTopmost, region, segmentWildcard);
        QMInfos = PlanningReportUtilities.getQMInfosByRegion(SelectedTopmost, region);
        Totals = new PlanningFLLTotals(StockInfos, RegionAndSegment);
        UsageInfoPool = PlanningReportUtilities.getUsageInformationPool(SelectedTopmost, region);
        UsageInfoRepAdmin = PlanningReportUtilities.getUsageInformationRepAdmin(SelectedTopmost, region);
        UsageInfoEPEP = PlanningReportUtilities.getUsageInformationEPEP(SelectedTopmost, region);
        Last24MonthsMap = PlanningReportUtilities.getLast24MonthsMap();
            
  	}
    
    //searches for ecode or part number, if match found gets planning report data for that topmost
    Public PageReference SearchChain(){
        String regionSegment = (RegionAndSegment == 'E2') ? 'FLL COM' : RegionAndSegment;
        SelectedTopmost = (String.isBlank(MultiSearchInput)) ? null: PlanningReportUtilities.searchPlanningReport(MultiSearchInput.trim(), regionSegment);
        IsNoMatch = (SelectedTopmost == null); 
        if(!IsNoMatch){
            getPlanningDataByTopmost();
            return redirectToPlanningPage();           
        }
        return null;
    }
    
    public void getAdvancedSearchResults(){
        AdvancedSearchList = PlanningReportUtilities.getAdvancedSearchResults(RegionAndSegment);
    }
    // ONCE YOU CLICK AN ITEM IN THE PLANNING REPORT IT GOES TO A SUMMARY PAGE OF THAT TOPMOST
    // THIS IS TO GO BACK TO THE PREVIOUS REGION YOU WERE ON
    public PageReference goToAdvancedSearch(){
        PageReference searchPage = Page.PlanningReportSearchPage;
        IF (RegionAndSegment == 'FLL COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'LBG COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
   
        searchPage.setRedirect(true);
      	return searchPage;
    }
    
    //goes to planning report page, adding topmost parameter to URL and redirecting
    public PageReference redirectToPlanningPage(){
        PageReference planningPage = Page.Planning_Report_FLL;
		planningPage.getParameters().put('topmost', SelectedTopmost);
        planningPage.getParameters().put('RegionAndSegment', RegionAndSegment);
        if(!String.isBlank(Anchor)){
            planningPage.setAnchor(Anchor);
            planningPage.getParameters().put('Anchor', Anchor);
        }
        planningPage.setRedirect(true);
        return planningPage;
    }
    
    public PageReference CallRESSCount(){
        PlanningReportUtilities.doRESSCount();
        PageReference searchPage = Page.PlanningReportSearchPage;
        IF (RegionAndSegment == 'E2') {
            IsE2Mode=TRUE;
        	searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'E2LBG') {
            IsE2ModeLBG=TRUE;
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
   		IF (RegionAndSegment == 'FLL COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'LBG COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
        searchPage.setRedirect(true);
         return searchPage;
       
        
    }
    
    public PageReference CallRepairsInTransitCount(){
        PlanningReportUtilities.doRepairsInTransitCount();
      //  redirectToPlanningPage();
        PageReference searchPage = Page.PlanningReportSearchPage;
        IF (RegionAndSegment == 'E2') {
            IsE2Mode=TRUE;
        	searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'E2LBG') {
            IsE2ModeLBG=TRUE;
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
   		IF (RegionAndSegment == 'FLL COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'LBG COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
        searchPage.setRedirect(true);
         return searchPage;
    }
    
    public PageReference CallCoresInTransitCount(){
        PlanningReportUtilities.doCoresInTransitCount();
       PageReference searchPage = Page.PlanningReportSearchPage;
        IF (RegionAndSegment == 'E2') {
            IsE2Mode=TRUE;
        	searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'E2LBG') {
            IsE2ModeLBG=TRUE;
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
   		IF (RegionAndSegment == 'FLL COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2Mode) ? 'E2Mode' : RegionAndSegment);}
        IF (RegionAndSegment == 'LBG COM') {
            searchPage.getParameters().put('RegionAndSegment', (IsE2ModeLBG) ? 'E2ModeLBG' : RegionAndSegment);}
        searchPage.setRedirect(true);
         return searchPage;
    }
    
    public void togglePartsAtRepair(){
        ShowPartsAtRepair = !ShowPartsAtRepair;
    }
    
    public void toggleCores(){
        ShowCores = !ShowCores;
    }
    
    public void toggleRESS(){
        ShowRESS = !ShowRESS;
    }
     public void togglePO(){
        ShowPO = !ShowPO;
    }
    
    public void toggleQM(){
        ShowQM = !ShowQM;
    }
    
    public void setCurrentCore(){
        CurrentCore = CoreInfos[Integer.valueOf(CoreIndex)];
        CoreIndex = '';
    }
    
    public void setCurrentQM(){
        CurrentQM = QMInfos[Integer.valueOf(QMIndex)];
        QMIndex = '';
    }
    public void setCurrentPO(){
        CurrentPO = POInfos[Integer.valueOf(POIndex)];
        POIndex = '';
    }
    public void setCurrentRepair(){
        CurrentRepair = RepairInfos[Integer.valueOf(RepairIndex)];
        RepairIndex = '';
    }
    
    
    public PageReference createCoreComment(){
        try{
            PlanningReportUtilities.addNewCoreComment(CurrentCore, CoreCommentBody);
            CurrentCore = null;
            CoreCommentBody = '';
        }
        catch(Exception e){
            System.Debug('The following exception occured: ' + e.getMessage());
        }
        Anchor='Cores';
        return redirectToPlanningPage();
    }
     
    public PageReference createPOComment(){
        try{
            PlanningReportUtilities.addNewPOComment(CurrentPO, POCommentBody);
            CurrentPO = null;
            POCommentBody = '';
        }
        catch(Exception e){
            System.Debug('The following exception occured: ' + e.getMessage());
        }
        Anchor='PO';
        return redirectToPlanningPage();
    }

    public PageReference createQMComment(){
        try{
            PlanningReportUtilities.addNewQMComment(CurrentQM, QMCommentBody);
            CurrentQM = null;
            QMCommentBody = '';
        }
        catch(Exception e){
            System.Debug('The following exception occured: ' + e.getMessage());
        }
        Anchor='QM';
        return redirectToPlanningPage();
    }
    
        public PageReference createRepairComment(){
        try{
            PlanningReportUtilities.addNewRepairComment(CurrentRepair, RepairCommentBody);
            CurrentRepair = null;
            RepairCommentBody = '';
        }
        catch(Exception e){
            System.Debug('The following exception occured: ' + e.getMessage());
        }
        Anchor='PartsAtRepair';
        return redirectToPlanningPage();
    }
    
}