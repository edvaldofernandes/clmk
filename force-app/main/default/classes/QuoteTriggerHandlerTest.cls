/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class QuoteTriggerHandlerTest
{


    static testMethod void myUnitTest() 
    {
    
         
       Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
       
       Opportunity oportunidade = SObjectInstanceTest.createOpportunity();
       Database.insert(oportunidade);
       
       Quote opp = SObjectInstanceTest.createQuote(oportunidade.Id);
       opp.Pricebook2Id = stdPB;
       opp.Status = 'Draft';
       opp.Quote_Submission_Date__c = System.today();
       opp.recordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
       opp.Approval_Status__c = 'test';
       Database.insert(opp);
       
       Product2 prod = SObjectInstanceTest.createProduct2();
       prod.Unit__c = 'Aricraft';
       prod.Product_Type__c = 'eSolutions';
       prod.Family = 'eSolutions';
       prod.Applicability__c = 'Turboprop';
       prod.Name = 'teste unitário';
       prod.recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
       database.insert( prod );
       
       	Product2 prod2 = SObjectInstanceTest.createProduct2();
       	prod2.Unit__c = 'Aircraft';
       	prod2.Product_Type__c = 'eSolutions';
       	prod2.Family = 'eSolutions';
       	prod2.Applicability__c = 'EJET';
       	prod2.Name = 'teste unitário 2';
       	prod2.ProductCode = '1234';
      	prod2.recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
       	database.insert( prod2 );
    
       PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
       database.insert( pbe );
       
       PricebookEntry pbe2 = SObjectInstanceTest.createPricebookEntry( stdPB, prod2.Id );
       database.insert( pbe2 );
       
       list< QuoteLineItem > lstQItem = new list< QuoteLineItem >();
    
       QuoteLineItem qItemEntryFee = SObjectInstanceTest.createQuoteLineItem(opp.Id, pbe.Id);
       qItemEntryFee.Princing__c = 'NREC';
       qItemEntryFee.Product_type_updated__c = 'eSolutions';
       lstQItem.add(qItemEntryFee);
       
       QuoteLineItem qItemNrec = SObjectInstanceTest.createQuoteLineItem(opp.Id, pbe2.Id);      
       qItemNrec.Princing__c = 'NREC';
       qItemNrec.Product_type_updated__c = 'eSolutions';
       
       lstQItem.add(qItemNrec);

       insert lstQItem;
       
       opp.Status = 'Presented to Customer';
       opp.Approval_Status__c = 'Approved';
       update opp;
       
       opp.Status = 'Accepted by Customer';
       opp.Quote_Acceptance_Date__c = date.today();
       update opp;
     
     
     	
    	Aircraft__c air = SObjectInstanceTest.createAircraft();
    	insert air;
        
        Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
    	insert rel;	
    	
    	rel.Quote__c = opp.Id;
    	update rel;
    	
    	delete rel;
    	undelete rel;
      
      	RelatedAircraftTriggerHandler handlerRA = new RelatedAircraftTriggerHandler(true);
      	System.AssertEquals(handlerRA.IsTriggerContext,true); 
       
       	delete opp;
      	undelete opp;
       
       	QuoteTriggerHandler handler = new QuoteTriggerHandler(true);
       	System.AssertEquals(handler.IsTriggerContext,true);
       
       
            
    }


}