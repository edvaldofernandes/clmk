// Used by DIM - Market Intelligence.
@isTest
public class DIM_CaseAcknowledgeControllerTest {

    //--------------------------------------------------------------------------
    static testMethod void validateAcknowledge() {
         
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        Test.startTest();
            List<Id> caseIds = new List<Id>();
            caseIds.add(caso.Id);
            DIM_CaseAcknowledgeController.sendEmail(caseIds);
            System.assertEquals(1, [SELECT count() FROM EmailMessage WHERE ParentId = :caso.Id]);
        Test.stopTest();
    }
}