public without sharing class RelatedAircraftTriggerHandler 
{
	
	public Map<Id,Related_Aircraft__c> newRecordsMap = new Map<Id,Related_Aircraft__c>();
    public Map<Id,Related_Aircraft__c> oldRecordsMap = new Map<Id,Related_Aircraft__c>(); 
    public List<Related_Aircraft__c> newRecords = new List<Related_Aircraft__c>();
    public List<Related_Aircraft__c> oldRecords = new List<Related_Aircraft__c>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public RelatedAircraftTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {


    }

 

    public void OnAfterInsert()
    {

     	updateAircraftsSerialNumbers() ;

    }

 

    public void OnBeforeUpdate()
    {

       
        

    }

 

    public void OnAfterUpdate()
    {
    	updateAircraftsSerialNumbers();	
    }

 

    public void OnBeforeDelete()
    {

        

    }

 

    public void OnAfterDelete()
    {
		updateAircraftsSerialNumbers();
        

    }

 

    public void OnUndelete()
    {

        

    }

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }
    
    
     private void updateAircraftsSerialNumbers()
    {
        Set<Id> quotesIds = new Set<Id>(); 
       	List<Quote> quoteToUpdate = new List<Quote>();
        Map<Id,List<Id>> mapaQuoteItem = new Map<Id,List<Id>>();
        Map<Id,String> mapaItemSerials = new Map<Id,String>();
        Map<Id,String> mapaItemNomes = new Map<Id,String>();
        String serials = '';
        List<Id> itemsList = new List<Id>();
        List<Related_Aircraft__c> lista = new List<Related_Aircraft__c>(); 
        String chaveItemAnterior = '';
        boolean registroElegivel = false;

        lista = this.newRecords;
        
        if(this.isDelete)
        	lista = this.oldRecords;
        
        for(Related_Aircraft__c ra : lista)
        {
        	
        	if(this.isInsert || this.isDelete )
        	{
        		registroElegivel = ra.Quote__c != Null;
        	}
        	else
        	{
        		registroElegivel = (ra.Quote__c != this.oldRecordsMap.get(ra.Id).Quote__c || ra.Quote_Line_Item__c  != this.oldRecordsMap.get(ra.Id).Quote_Line_Item__c);
        	}	
            if(registroElegivel )
                quotesIds.add(ra.Quote__c);
        }
        System.Debug('Quotes is Empty ' + quotesIds.IsEmpty());
        if(!quotesIds.IsEmpty())
        { 
        	System.Debug('Quotes ids - ' + quotesIds);
            for(Related_Aircraft__c ra : [SELECT Quote_Line_Item__c,Quote_Line_Item__r.PricebookEntry.Product2.Name,Quote__c,Aircraft__r.Name FROM Related_Aircraft__c WHERE Quote__c in  : quotesIds Order By Quote__c,Quote_Line_Item__c])
            {
            	serials = '';
            	itemsList = new List<Id>();
            	
            	if(mapaQuoteItem.containsKey(ra.Quote__c))
            		itemsList = mapaQuoteItem.get(ra.Quote__c);
            	
            	if(chaveItemAnterior != (ra.Quote__c + '-' + ra.Quote_Line_Item__c))
            		itemsList.add(ra.Quote_Line_Item__c);
            	
            	System.Debug('Line Item ' + ra.Quote_Line_Item__c);
            	
            	if(!mapaItemNomes.containsKey(ra.Quote_Line_Item__c))
            		mapaItemNomes.put(ra.Quote_Line_Item__c,ra.Quote_Line_Item__r.PricebookEntry.Product2.Name);
            	
            	if(mapaItemSerials.containsKey(ra.Quote_Line_Item__c))
            	{
            		
            		serials = mapaItemSerials.get(ra.Quote_Line_Item__c);
            		serials += (string.isEmpty(serials) ? '' : ',');
            		if(math.mod(serials.split(',').size(), 11) == 0)
                		serials += '\n';
                	
                }		
            	serials += ra.Aircraft__r.Name ;
            	System.Debug('serials ' + serials);
            	mapaItemSerials.put(ra.Quote_Line_Item__c,serials);
            	mapaQuoteItem.put(ra.Quote__c,itemsList);
            	chaveItemAnterior = (ra.Quote__c + '-' + ra.Quote_Line_Item__c);
            	
            }
            System.Debug('MapaQuoteItens - ' + mapaQuoteItem);
            if(!mapaQuoteItem.IsEmpty())
            {
                for(Id idCotacao : mapaQuoteItem.keySet())
                {
                	serials = '';
                	for(Id idItem : mapaQuoteItem.get(idCotacao))
                		serials += (string.isEmpty(serials) ? '' : '\n') + mapaItemNomes.get(idItem) + ' - ' + mapaItemSerials.get(idItem);
                	
                		 
                	quoteToUpdate.add(new Quote(Id= idCotacao,AircraftSerialNumbers__c = serials));
                }
                update quoteToUpdate;
            }
            
        }    
    
    }
    
	

}