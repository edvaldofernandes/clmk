/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Controller class for the visualforce page ProductEditPricebook   

* NAME: ProductEditPricebookController.cls
* AUTHOR:JFS                                                DATE: 08/10/2014
*
* Alterações solicitadas pelo Eduardo
* AUTHOR: DPF                                               DATE: 30/10/2014
*
* Alterações solicitadas pelo Eduardo
* AUTHOR: JFS                                               DATE: 05/12/2014
*******************************************************************************/
public with sharing class ProductEditPricebookController
{
  private static final Id SRecProductTailored = RecordTypeMemory.getRecType('Product2', 'Tailored');
  private static final Id SRecProductPilots = RecordTypeMemory.getRecType('Product2', 'Pilot_services');
  private static final Id SRecProductTraining = RecordTypeMemory.getRecType('Product2', 'Training');
  
  public id RecProductTailored { get { return SRecProductTailored; } }
  public id RecProductPilots { get { return SRecProductPilots; } }
  public id RecProductTraining { get { return SRecProductTraining; } }
  
  public String labelPrice1 { get; set; }
  public String labelPrice2 { get; set; }
  public String labelPrice3 { get; set; }

  public list<PricebookEntryWrapper> Pricebookprodutos {get;set;}
  public Id IdProduct2;

  public list<SelectOption> moedas { get; set; }
  public Boolean isNew { get; set; }
  public id RecordTypeProd { get; set; }
  
  private set< String > lSetPBE;
  private map<String, PricebookEntry> mapPricebookPadrao;

  public class PricebookEntryWrapper
  {
    public String pricebook2Name { get; set; }
    public Boolean isStandardPricebook { get; set; }
    public String Id { get; set; }
    public String Product2Id { get; set; }
    public String Pricebook2id { get; set; }
    public Boolean UseStandardPrice { get; set; }
    public String UnitPrice { get; set; }
    public String RECPrice { get; set; }
    public Boolean IsActive { get; set; }
    public String Margin_0 { get; set; }
    public String Maximum_Discount { get; set; }
    public String Maximum_Discount_REC { get; set; }
    public String Margin_0_REC { get; set; }
    public String Entry_fee { get; set; }
    public String Maximum_disccount_Entry_fee { get; set; }
    public String CurrencyIsoCode { get; set; }

    public PricebookEntryWrapper( String pricebook2Name,  Boolean isStandardPricebook,
      String Id, String Product2Id, String Pricebook2id, Boolean UseStandardPrice,
      String UnitPrice, String RECPrice, Boolean IsActive, String Margin_0,
      String Maximum_Discount, String Maximum_Discount_REC, String Margin_0_REC,
      String Entry_fee, String Maximum_disccount_Entry_fee, String CurrencyIsoCode )
    {
      this.pricebook2Name = pricebook2Name;
      this.isStandardPricebook = isStandardPricebook;
      this.Id = Id;
      this.Product2Id = Product2Id;
      this.Pricebook2id = Pricebook2id;
      this.UseStandardPrice = UseStandardPrice;
      this.UnitPrice = UnitPrice;
      this.RECPrice = RECPrice;
      this.IsActive = IsActive;
      this.Margin_0 = Margin_0;
      this.Maximum_Discount = Maximum_Discount;
      this.Maximum_Discount_REC = Maximum_Discount_REC;
      this.Margin_0_REC = Margin_0_REC;
      this.Entry_fee = Entry_fee;
      this.Maximum_disccount_Entry_fee = Maximum_disccount_Entry_fee;
      this.CurrencyIsoCode = CurrencyIsoCode;
    }
  }

  public ProductEditPricebookController(ApexPages.StandardController controller)
  {
    IdProduct2 = controller.getId();
    isNew = false;

    moedas = new list<SelectOption>();

    for (Schema.PicklistEntry a: PricebookEntry.getSObjectType().getDescribe().fields.getmap().get('CurrencyIsoCode').getDescribe().getPicklistValues() )
    {
    	SelectOption lSelOpt = new SelectOption(a.getValue(), a.getLabel());
      if ( a.getValue() == 'USD' ) moedas.add( 0, lSelOpt );
      else moedas.add( lSelOpt );
    }

    list<Product2> prod = [SELECT RecordTypeId, RecordType.DeveloperName FROM Product2 WHERE Id =: IdProduct2 ];
    if( !prod.isEmpty() )
    {
    	RecordTypeProd = prod[0].RecordTypeId;
    	labelPrice1 = Utils.returnLabelField(prod[0].RecordType.DeveloperName, 'Price1__c');
      labelPrice2 = Utils.returnLabelField(prod[0].RecordType.DeveloperName, 'Price2__c');
      labelPrice3 = Utils.returnLabelField(prod[0].RecordType.DeveloperName, 'Price3__c');
    }

    Pricebookprodutos = new list<PricebookEntryWrapper>();

    String ids = apexpages.currentPage().getParameters().get('lstaPb');
    
    if ( ids != null )
    {
      list<String> lstaPb = new list<String>();
      for ( String id : ids.split(',')) lstaPb.add(id);
      SelectPricebook(lstaPb);
      isNew = true;
    }
    else
    {
      for ( PricebookEntry iPbe : [select Id, name, Product2Id, ProductCode, Pricebook2id,
        Pricebook2.Annual_Escalation__c, UseStandardPrice, UnitPrice, RECPrice__c, IsActive, Margin_0__c,
        Maximum_Discount__c, Pricebook2.Name, Maximum_Discount_REC__c, Margin_0_REC__c,
        Pricebook2.IsStandard, Product2.RecordTypeId, Entry_fee__c, Maximum_disccount_Entry_fee__c,
        CurrencyIsoCode
        from PricebookEntry
        where Product2Id = :IdProduct2
        order by Pricebook2.IsStandard DESC, Pricebook2.Name
        ])
      {
        PricebookEntryWrapper pbeWrapper = new PricebookEntryWrapper(
          iPbe.Pricebook2.name,
          iPbe.Pricebook2.IsStandard,
          String.valueOf(iPbe.Id),
          String.valueOf(iPbe.Product2Id),
          String.valueOf(iPbe.Pricebook2id),
          iPbe.UseStandardPrice,
          String.valueOf(iPbe.UnitPrice),
          String.valueOf(iPbe.RECPrice__c),
          iPbe.IsActive,
          String.valueOf(iPbe.Margin_0__c),
          String.valueOf(iPbe.Maximum_Discount__c),
          String.valueOf(iPbe.Maximum_Discount_REC__c),
          String.valueOf(iPbe.Margin_0_REC__c),
          String.valueOf(iPbe.Entry_fee__c),
          String.valueOf(iPbe.Maximum_disccount_Entry_fee__c),
          iPbe.CurrencyIsoCode
        );
        Pricebookprodutos.add(pbeWrapper);
      }
    }
        
    lSetPBE = new set< String >();
    mapPricebookPadrao = new map<String, PricebookEntry>();
    for ( PricebookEntry std : [Select Id, Product2Id, RECPrice__c, UnitPrice, Pricebook2.IsStandard,
      Maximum_Discount__c, Maximum_Discount_REC__c, Margin_0__c, Margin_0_REC__c,
      Entry_fee__c, Maximum_disccount_Entry_fee__c, CurrencyIsoCode, PriceBook2Id
      from PricebookEntry
      Where Product2Id =:IdProduct2 ])
    {
      if ( std.Pricebook2.IsStandard ) mapPricebookPadrao.put(std.CurrencyIsoCode, std);
      lSetPBE.add( std.PriceBook2Id + '|' + std.Product2Id + '|' + std.CurrencyIsoCode );
    }
    
  }

  public PageReference mysave()
  {  	
    list<PricebookEntry> lstStd = new list<PricebookEntry>();
    list<PricebookEntryWrapper> lstStdWrapper = new list<PricebookEntryWrapper>();
    
    list<PricebookEntry> lstPbe = new list<PricebookEntry>();
    list<PricebookEntryWrapper> lstPbeWrapper = new list<PricebookEntryWrapper>();
    
  	for (PricebookEntryWrapper iPbeWrapper : Pricebookprodutos )
    {
      if ( !iPbeWrapper.useStandardPrice && String.isBlank(iPbeWrapper.UnitPrice) )
      {
        adicionaErro('List price - NREC ($) is required.');
        return null;
      }  
      if ( lSetPBE.contains( iPbeWrapper.Pricebook2id + '|' + iPbeWrapper.Product2Id + '|' + iPbeWrapper.CurrencyIsoCode ) && iPbeWrapper.Id == null )
      {
        adicionaErro('There is Pricebook Entry added with ' + iPbeWrapper.CurrencyIsoCode + ' currency.');
        return null;
      }
      
      if ( iPbeWrapper.isStandardPricebook ) lstStdWrapper.add(iPbeWrapper);
      else lstPbeWrapper.add(iPbeWrapper);
    }    
    
    if ( !preencheValidaNumeros( lstStd, lstStdWrapper ) ) return null;
    
    Savepoint sp = Database.setSavepoint();
    
    if ( !lstStd.isEmpty())
    {
      Database.Upsertresult[] result = Database.upsert( lstStd, false );
    
      String erros = '';
      for ( Database.Upsertresult r : result )
      {
        if ( !r.isSuccess() ) erros += r.getErrors()[0].getMessage() + '\n ';
      }
      if ( String.isNotBlank(erros) )
      {
        adicionaErro(erros);
        Database.rollback(sp);
        return null;
      }
    }

    if ( !preencheValidaNumeros( lstPbe, lstPbeWrapper ) )
    {
       Database.rollback(sp);
    	 return null;
    }
    
    if ( !lstPbe.isEmpty())
    {
    	Database.Upsertresult[] result = Database.upsert( lstPbe, false );
    
	    String erros = '';
	    for ( Database.Upsertresult r : result )
	    {
	    	if ( !r.isSuccess() ) erros += r.getErrors()[0].getMessage() + '\n ';
	    }
	    if ( String.isNotBlank(erros) )
	    {
	    	adicionaErro(erros);
        Database.rollback(sp);
	    	return null;
	    }
    }

    PageReference pageRef = new PageReference('/' + IdProduct2);
    pageRef.setRedirect(true);
    return pageRef;

  }

  private void SelectPricebook(list<String> lstaPb)
  {
    PricebookEntry pbeStandard = null;
    list<PricebookEntry> lstPbeStd = [SELECT Id, Product2Id, RECPrice__c, UnitPrice,
      Maximum_Discount__c, Maximum_Discount_REC__c, Margin_0__c, Margin_0_REC__c,
      Entry_fee__c, Maximum_disccount_Entry_fee__c, CurrencyIsoCode
      FROM PricebookEntry
      WHERE Pricebook2.IsStandard = true
      AND Product2Id =: IdProduct2
      AND Pricebook2.IsActive = true ];
    for ( PricebookEntry pbe : lstPbeStd )
    {
      if ( pbe.CurrencyIsoCode == 'USD' ) pbeStandard = pbe;
    }
    if ( pbeStandard == null && !lstPbeStd.isEmpty() ) pbeStandard = lstPbeStd[0];

    //list < Pricebook2 > lstPb2 = ;
    Pricebookprodutos = new list<PricebookEntryWrapper>();
    for(Pricebook2 lpb: [SELECT Id, Name, Description, LastModifiedDate, IsActive, IsStandard
          FROM Pricebook2 WHERE id =:lstaPb ORDER BY IsStandard DESC])
    {
      PricebookEntryWrapper pbeWrapper = new PricebookEntryWrapper(
          lpb.Name,
          lpb.IsStandard,
          null,
          String.valueOf(IdProduct2),
          String.valueOf(lpb.Id),
          false,
          pbeStandard != null ? ( pbeStandard.UnitPrice != null ? String.valueOf(pbeStandard.UnitPrice) : null ) : null ,
          pbeStandard != null ? ( pbeStandard.RECPrice__c != null ? String.valueOf(pbeStandard.RECPrice__c) : null )  : null,
          true,
          pbeStandard != null ? ( pbeStandard.Margin_0__c != null ? String.valueOf(pbeStandard.Margin_0__c) : null )  : null,
          pbeStandard != null ? ( pbeStandard.Maximum_Discount__c != null ? String.valueOf(pbeStandard.Maximum_Discount__c) : null )  : null,
          pbeStandard != null ? ( pbeStandard.Maximum_Discount_REC__c != null ? String.valueOf(pbeStandard.Maximum_Discount_REC__c) : null )  : null,
          pbeStandard != null ? ( pbeStandard.Margin_0_REC__c != null ? String.valueOf(pbeStandard.Margin_0_REC__c) : null )  : null,
          pbeStandard != null ? ( pbeStandard.Entry_fee__c != null ? String.valueOf(pbeStandard.Entry_fee__c) : null )  : null,
          pbeStandard != null ? ( pbeStandard.Maximum_disccount_Entry_fee__c != null ? String.valueOf(pbeStandard.Maximum_disccount_Entry_fee__c) : null )  : null,
          null
        );
      Pricebookprodutos.add(pbeWrapper);
    }
  }

  private void adicionaErro(String msg)
  {
    ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.ERROR, msg) );
  }
  
  private Boolean preencheValidaNumeros( List<PricebookEntry> lstPbe, List<PricebookEntryWrapper> lPbeWrapper )
  {
    try {
      for ( PricebookEntryWrapper iPbeWrapper : lPbeWrapper )
      {
      	PricebookEntry pbeStandard = mapPricebookPadrao.get(iPbeWrapper.CurrencyIsoCode);
	      PricebookEntry pbe = new PricebookEntry ();
	      pbe.Id = iPbeWrapper.Id ;
	      if ( pbe.Id == null )
	      {
	        pbe.Product2Id = iPbeWrapper.Product2Id ;
	        pbe.Pricebook2id = iPbeWrapper.Pricebook2id ;
	      }
	      pbe.UseStandardPrice = iPbeWrapper.UseStandardPrice ;
	      pbe.UnitPrice = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.UnitPrice  : null ) : Utils.formatNumber(iPbeWrapper.UnitPrice) ;
	      pbe.RECPrice__c = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.RECPrice__c : null ) : Utils.formatNumber(iPbeWrapper.RECPrice) ;
	      pbe.IsActive = iPbeWrapper.IsActive ;
	      pbe.Margin_0__c =  pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.Margin_0__c : null ) : Utils.formatNumber(iPbeWrapper.Margin_0) ;
	      pbe.Maximum_Discount__c = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.Maximum_Discount__c : null ) : Utils.formatNumber(iPbeWrapper.Maximum_Discount) ;
	      pbe.Maximum_Discount_REC__c = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.Maximum_Discount_REC__c : null ) : Utils.formatNumber(iPbeWrapper.Maximum_Discount_REC) ;
	      pbe.Margin_0_REC__c = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.Margin_0_REC__c : null ) : Utils.formatNumber(iPbeWrapper.Margin_0_REC) ;
	      pbe.Entry_fee__c = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.Entry_fee__c : null ) : Utils.formatNumber(iPbeWrapper.Entry_fee) ;
	      pbe.Maximum_disccount_Entry_fee__c = pbe.UseStandardPrice ? ( pbeStandard != null ? pbeStandard.Maximum_disccount_Entry_fee__c : null ) : Utils.formatNumber(iPbeWrapper.Maximum_disccount_Entry_fee) ;
	      if ( isNew )
	      {
	        pbe.CurrencyIsoCode = iPbeWrapper.CurrencyIsoCode;
	      }
	      if ( iPbeWrapper.isStandardPricebook )
        {
          mapPricebookPadrao.put(iPbeWrapper.CurrencyIsoCode, pbe);
        }
	      lstPbe.add(pbe);
      }
    }
    catch( Exception e )
    {
    	adicionaErro(e.getMessage());
    	return false;
    }
    return true;
  }
}