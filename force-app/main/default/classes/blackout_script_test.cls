@isTest
public class blackout_script_test {
    static testMethod void run(){
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        System.runAs (thisUser){
            User user1 = SObjectInstanceTest.createUser(p.Id);
            user1.FirstName = 'Jose';
            user1.Email = 'jose.jose@embraer.com.br';
            user1.Alias = 'jjose';
            user1.Username = 'jose.jose@embraer.com.br';
            user1.CommunityNickname = 'Community Nickname1';
    
            User user2 = SObjectInstanceTest.createUser(p.Id);
            user2.FirstName = 'Carlos';
            user2.Email = 'carlos.carlos@embraer.com.br';
            user2.Alias = 'ccarlos';
            user2.CommunityNickname = 'Community Nickname2';
            
            Carve_Out_Headcount__c coh1 = new Carve_Out_Headcount__c();
            coh1.Name = 'Jose';
            coh1.Email__c = 'jose.jose@embraer.com.br';
            coh1.Username__c = 'jjoseX';
            coh1.Company_Assignment__c = 'BBC';
            
            Carve_Out_Headcount__c coh2 = new Carve_Out_Headcount__c();
            coh2.Name = 'Carlos';
            coh2.Email__c = 'carlos.carlos@embraer.com.br';
            coh2.Username__c = 'ccarlosX';
            coh2.Company_Assignment__c = 'Embraer';
            
            database.insert(new List<SObject>{user1,user2,coh1,coh2});
        }
        
        test.startTest();
        System.runAs(thisUser){
            blackout_script.runScript();
        }
        test.stopTest();
        
        //system.debug('debug ' + [SELECT Name, Alias, Email, Username FROM User WHERE Alias = 'ccarlos']);
        //system.debug('debug ' + [SELECT Name, Alias, Email, Username FROM User WHERE Alias = 'jjoseX']);
        //system.assert([SELECT Count() FROM User WHERE Email = 'jose.jose@embraer.net.br'] == 1);
        //system.assert([SELECT Id, UserId, IsFrozen From UserLogin WHERE UserId = :[SELECT Id FROM User WHERE Email = 'carlos.carlos@embraer.com.br'][0].Id][0].isFrozen == true);
    }

}