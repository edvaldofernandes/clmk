/**
* @author Marcilio Leite de Souza
* @date 30/05/2018
* @description: Data Access Object for Contact all select methods
*
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           30 MAY 2018             Original Version
**/
public class GEN_ContactDAO {

    public static Contact getCommunityContact(String email, String flyEmbraerId){
        String str = 'SELECT  Id, LastName,FirstName,Phone,Email,Contact_Status__c, AccountId, Account.Name FROM Contact Where (Email = \'' + String.escapeSingleQuotes(email) + '\' )';
               str +=' OR (FlyEmbraerUserId__c = \'' + String.escapeSingleQuotes(flyEmbraerId) +'\' )';
        return getContactByQuery(str);
    }
    
     public static Contact getCommunityContactByFlyEmbraerId(String flyEmbraerId){
        String str = 'SELECT Id, LastName,FirstName,Phone,Email,Contact_Status__c, AccountId, Account.Name FROM Contact Where FlyEmbraerUserId__c = \'' + String.escapeSingleQuotes(flyEmbraerId) + '\'';
        System.debug('SOQL '+str);
         return getContactByQuery(str);
    }
    
    private static Contact getContactByQuery(String query){
        System.debug('SOQL '+query);
        List<Contact> listCon = Database.query(query);
        if(listCon.size() > 0){ return listCon[0];}
        return null;
    }
    
   
}