/*
    * @description Reverts the approved and signed status to request status when it's possible.
 */

public class CLMRevertToRequestController {
    
    public Agreement__c agreement { get; set; }
    
    public CLMRevertToRequestController(ApexPages.StandardController controller){
        
        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID parameter not found'));
            return;
        }
        
        agreement = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        if(agreement == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Agreement not found','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
    	}
    }
    
    public PageReference revert(){
        agreement = [SELECT Id, Edit_Mode__c, recordtype.name, Status_Category__c, Bypass_Approval__c, Signature_SObject_Creation__c FROM Agreement__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')]; 
        if (agreement.Edit_Mode__c){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Edit Mode is ON - Can\'t revert.'));
            system.debug('EDIT MODE');
            return null;
        }
        if  (agreement.Status_Category__c=='PA Approved' || agreement.Status_Category__c=='Proposal Approved' || agreement.Status_Category__c=='Terminated'){
            revertStatus(agreement);
        }
        else { 
            if  (agreement.Status_Category__c=='Proposal Signed' || agreement.Status_Category__c=='PA Signed'){
                Id profileId=userinfo.getProfileId();
				String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                If(profileName == 'System Administrator' || profileName == 'DCT Contract Administrator' || profileName == 'DCT System Administrator'){
                	if (agreement.Signature_SObject_Creation__c == 'Blocked Record'){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'This Agreement cannot be edited.'));
                		return null;
                    }
                    revertSignature(agreement);
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must be a DCT Contract Administrator.'));
                	return null;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The ' + agreement.recordtype.name + ' must be approved.'));
                return null;
            }
        }
        
        PageReference returnPage = new PageReference('/'+ agreement.id);  
        returnPage.setRedirect(true);
        return returnPage;
    }
    
    public void revertStatus(Agreement__c agr){
        if (agr.recordtype.name == 'Purchase Agreement')
        	agr.Status_Category__c = 'PA Request';
        else
        	agr.Status_Category__c = 'Proposal Request';
        agr.Status__c = 'Request';
        agr.Bypass_Approval__c = false;
        agr.DOCCON_Number__c = '';
        agr.Comments__c = '';
        agr.Bypass_Comments__c = '';
        database.update(agr);
    }
    
    public void revertSignature(agreement__c agr){
        if (agr.recordtype.name == 'Purchase Agreement'){
        	agr.Status_Category__c = 'PA Request';
            agr.Status__c = 'Request';
            //Delete
            if (agr.Signature_SObject_Creation__c != null && agr.Signature_SObject_Creation__c != '')
            	database.delete(agr.Signature_SObject_Creation__c.split(','),false);
            agr.Signature_SObject_Creation__c = 'Correction Mode Flag';
        }
        else{
        	agr.Status_Category__c = 'Proposal Request';
            agr.DOCCON_Number__c = '';
        }
        agr.Status__c = 'Request';
        agr.Bypass_Approval__c = false;
        agr.DOCCON_Number__c = '';
        agr.Comments__c = '';
        agr.Bypass_Comments__c = '';
        database.update(agr);
    }

}