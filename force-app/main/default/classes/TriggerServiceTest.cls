@isTest(seeAllData=true)
public class TriggerServiceTest {

    @isTest static void findUserByCreatedByIdTest(){

        Test.startTest();
        
        Schema.DescribeFieldResult fieldResult = AccountHistory.Field.getDescribe();
        List<Schema.PicklistEntry> picklistField = fieldResult.getPicklistValues();
        
        Account account = new Account();
        account.Name = 'Test test test ';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
        
        AccountHistory accountHistoryNew = new AccountHistory();
        accountHistoryNew.Field = picklistField.get(0).getValue();
        accountHistoryNew.AccountId = account.id;
        insert accountHistoryNew;
        
        TriggerService.findUserByCreatedById(account.id);
        
        Test.stopTest();

    }
}