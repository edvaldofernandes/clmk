@istest
public class CLMStringDataWrapperTest {
        static testmethod void test1() {       
        List<CLMStringDataWrapper> empList = new List<CLMStringDataWrapper>();
        empList.add(new CLMStringDataWrapper ('Embraer 170-E1'));
        empList.add(new CLMStringDataWrapper ('Embraer 175-E1'));
        empList.add(new CLMStringDataWrapper ('Embraer 190-E1'));
        
         
        // Sort using the custom compareTo() method
        empList.sort();
         
        // Write list contents to the debug log
        System.debug(empList);
         
        // Verify list sort order.
        System.assertEquals('Embraer 170-E1', empList[0].str);
        System.assertEquals('Embraer 175-E1', empList[1].str);
        System.assertEquals('Embraer 190-E1', empList[2].str);
    }

}