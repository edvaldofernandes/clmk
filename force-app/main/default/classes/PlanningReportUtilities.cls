public class PlanningReportUtilities {
    private static Set<String> last24MonthKeys = getLast24MonthsMap().keySet();
    private static Set<String> repAdminCustomers = new Set<String>{'SKYWEST AIRLINES, INC.', 'COPA AIRLINES', 'AZUL LINHAS AEREAS BRASILEIRAS S/A', 'HOP!'};
    
    //subclass for usage information
    public class UsageInfoWrapper{
        public String Customer{get;set;}
        public Map<String,Integer> MonthYearCount{get;set;}
        
        public UsageInfoWrapper(String cust){
            this.Customer = cust;
            this.MonthYearCount = New Map<String, Integer>();
            for(String yearMonth : last24MonthKeys){
                this.MonthYearCount.put(yearMonth, 0);
            }
        }
    }
    
    //creates a map of the last 24 months
    //the keys are the dates formatted as a string "YYYYMM"
    //the values are date instances
    public static Map<String, Date> getLast24MonthsMap(){
        Map<String, Date> last24MonthsMap = new Map<String, Date>();
        integer numOfMonths = 24;
        while(numOfMonths > 0){
            Date tempDate = date.today().addMonths(-numOfMonths);
            String yearMonth =  string.valueOf(tempDate.year()) + string.valueOf(tempDate.month()).leftPad(2, '0');
            last24MonthsMap.put(yearMonth, tempDate);
            numOfMonths--;
        }
        return last24MonthsMap;
    }
    
	public static string searchPlanningReport(string searchCriteria, string regionAndSegment){
        List<Stock_Info_FLL__c> Results = [Select Part_Number__r.Top_Most__c FROM Stock_Info_FLL__c 
                                           WHERE (Ecode__c=:searchCriteria OR Part_Number__r.Name=:searchCriteria) AND Region_and_Segment__c=:regionAndSegment];
        return !Results.isEmpty() ? Results[0].Part_Number__r.Top_Most__c : null;
    }
    
    public static List<Stock_Info_FLL__c> getStockInfosByRegionAndSegment(string topmost, string regionAndSegment){
        if(regionAndSegment == 'FLL COM'){
            return [Select Id, POOL_CUSTOMERS__c, Safety_Stock__c,OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, TQA_145__c, TQA_170__c, TQA_190__c, US13_OH__c, 
                    FLL3_NEW_USED__c, FLL3_OVHL_0693__c,FLL2_OH__c, FLL3_0730_COMPLIANCE__c,FLL3_0643__c, FLL3_0684_EX__c, BNA_0660__c, FLL3_0785_LLP_COMPLETE__c, FLL3_0787_LLP_PEND__c, AT_REPAIR__c, 
                    FLL2_0646_UN__c, FLL3_0697_PEND_TEC__c, FLL3_0630_SCRAP__c, US13_5149_SCRAP__c, REPAIR_ADMIN_ORDERS__c, FLL3_BLKD__c, US13_BLKD__c, FLL3_QUAR__c, US13_QUAR__c, 
                    FLL3_0673_SHELF_LIFE_EXP__c, FLL3_D_S__c, US13_D_S__c, FLL3_TO_REP__c, US13_TO_REP__c, NON_REPAIRABLE__c, LLP__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, 
                    OSS_ALLOCATION__c, STOCK_MAX__c, OVERAGE_SHORTAGE__c, USAGE_12M__c, AVG_USAGE__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c,OPEN_PRS__c, PRS_OLDEST_DATE__c, 
                    CORES__c, QMS_OPEN__c,OpenPOFLL1andFLL2__c,OpenPRFLL1andFLL2__c,OH_FLL1_FLL2__c,QMFLL1andFLL2__c, OPEN_TOS__c, OH_Total__c, Top_Most__c
                    FROM Stock_Info_FLL__c WHERE Top_Most__c =:topmost AND Is_Summary_Record__c=false AND Region_and_Segment__c=:regionAndSegment];
        }else if(regionAndSegment == 'LBG COM'){
            return [Select Id, POOL_CUSTOMERS__c,Safety_Stock__c, OSS_OH__c, Prime__c, Description__c, Essentiality__c, Program__c, Part_Number__c, Ecode__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c, STOCK_MAX__c, 
                    OVERAGE_SHORTAGE__c, USAGE_12M__c, SCRAP_12M__c, ACQUISITION_COST__c, OPEN_POS__c, OPEN_PRS__c, PRS_OLDEST_DATE__c, CORES__c, QMS_OPEN__c, OPEN_TOS__c, TQA__c,
                    LBG2__c, LBG3_New__c, LBG3_Used__c, LBG3_OVH__c, At_Repair_LBG2__c, At_Repair_LBG3__c, X0904_Scrap__c, X0910_BUs_Unserv__c,	X0941_OLD_Scrap_Custom__c, 
                    Blocked_LBG3__c, Blocked_LBG2__c, X0909_PartOut_KASI__c, X0921_Buyback__c,QMLBG1andLBG2__c,OpenPOLBG1andLBG2__c,OpenPRLBG1andLBG2__c,OH_LBG1_LBG2__c, X0929_OLD_TOOL_TOT__c, X0930_OLD_Com_Quarantine__c, X0942_COM_EXCESS__c, 
                    X0957_BUS_AT_RS__c, X0980_COM_ENG_OGMA__c, X0950_COM_DC_JNB__c, X0913_BUs_In_Transit__c, OH_Total__c, Top_Most__c
                    FROM Stock_Info_FLL__c WHERE Top_Most__c =:topmost AND Is_Summary_Record__c=false AND Region_and_Segment__c=:regionAndSegment];
        }else{
        	return null;
        }
    }
    
    public static List<RESS_Info__c> getRessInfosByRegionAndSegment(string topmost, string region, string segment){
        return [SELECT Id, Topmost__c, Customer_Name__c, Part_Number__c, RESS_ID__c, Sales_Order__r.SO__c, Sales_Order__r.Transfer_PO__c, Sales_Order__r.Id, rm_comments__c, Site__c
                FROM RESS_Info__c 
                WHERE Topmost__c =:topmost AND Site__c=:region AND Segment__c LIKE :segment AND Customer_Name__c='EMBRAER SERVICES INC.' AND RESS_ID__c!=null 
                AND Tracking_Sent_Date_and_Time__c=null AND Alternate_Action_Date_and_Time__c=null];
    }
    
    public static List<Repair_Information__c> getRepairInfosByRegionAndSegment(string topmost, string regionAndSegment){
        return [SELECT Topmost__c, Repair_comment_body__c, Repair_comment__c, Actual_Repair_Station__c, eRepair_Queue__c, Repair_Contract_Terms__c, Repair_Customer__c, Repair_Ecode__c, Repair_Exchange_Prov__c, 
                Repair_Notification__c, Repair_Part_Number__c, Repair_User_Notes__c, RM_TAT_At_Repair__c, SOD_Notes__c, TAT_At_Repair__c, AWB_From_Repair__c, AWB_From_Repair_Deliv_Date__c
                FROM Repair_Information__c WHERE Topmost__c =:topmost AND Site__c=:regionAndSegment ORDER BY Tat_At_Repair_Formula__c DESC];
    }
    
    public static List<Core_Information__c> getCoreInfosByRegionAndSegment(string topmost, string region, string segment){
        return [SELECT Id, Topmost__c, Category__c, Category_Aging__c, Customer_Name__c, Epool_status__c, Notification__c, Part_Number__c, Problem_Description__c, QM_Number__c, 
                Responsible_Department__c, Total_Aging__c, Core_Comment__c, Core_Comment_Body__c
                FROM Core_Information__c WHERE Topmost__c =:topmost AND Support_Plant__c=:region AND Segment__c LIKE :segment ORDER BY Total_Aging__c DESC];
    }
    
    public static List<QM__c> getQMInfosByRegion(string topmost, string region){
        return [SELECT Id, Name,QM_Comment__c, QM_Comments__c, Responsible__c, QM_Comment2__c, Aging__c, Last_Comment__c, Site__c, Part_Number__r.Top_Most__c,QM_comment2_body__c
                FROM QM__c 
                WHERE Part_Number__r.Top_Most__c = :topmost AND Region__c = :region AND Closed__c = false
                ORDER BY Aging__c DESC];
    }
    
        public static List<PO__c> getPOInfosByRegionAndSegment(string topmost, string region, string segment){
        return [SELECT Id, Part_Number__r.Top_Most__c,PO_Comment__c,Notification__c, po_comment_body__c,Ecode__c, Vendor_Name__c, Segment__c, Plant__c, Part_Number__c, Agreed_Date__c, Description__c, Total_Qty__c, Price__c, Po_Issue_Date__c, Name, Emb_Balance__c, Line_Item__c
                FROM PO__c 
                WHERE Part_Number__r.Top_Most__c =:topmost AND Segment__c = :region  
                ];
    }
    
    //gets top 30 results
    public static List<Stock_Info_FLL__c> getAdvancedSearchResults(string regionAndSegment){
        if(regionAndSegment=='FLL COM'){
            return [Select Id, OSS_OH__c, Safety_Stock__c, Top_Most__c, FLL2_OH__c, Description__c,OpenPOFLL1andFLL2__c,OpenPRFLL1andFLL2__c,OH_FLL1_FLL2__c,QMFLL1andFLL2__c, Essentiality__c, AT_REPAIR__c, FLL3_BLKD__c, US13_BLKD__c, FLL3_QUAR__c, US13_QUAR__c, FLL3_TO_REP__c, 
                	US13_TO_REP__c, Part_Number__c, Part_Number_Name__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c,USAGE_12M__c, OPEN_PRS__c, CORES__c, QMS_OPEN__c, OPEN_POS__c, RESS_Count__c,TQA__c,
                    Ecode__c, OH_Total__c, Repairs_In_Transit__c, Cores_In_Transit__c
                    FROM Stock_Info_FLL__c 
                    WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) AND OH_Total__c<2 AND Is_Summary_Record__c=true 
                    	AND Region_and_Segment__c=:regionAndSegment AND Part_Number__r.Hide_From_Planning_Report__c=False
                    ORDER BY USAGE_12M__c DESC
                    LIMIT 30];
        }else if(regionAndSegment=='LBG COM'){
            return [Select Id, OSS_OH__c,Safety_Stock__c, Top_Most__c, Description__c, Essentiality__c, Part_Number__c, Part_Number_Name__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c, USAGE_12M__c, OPEN_PRS__c, 
                    CORES__c, QMS_OPEN__c, OPEN_POS__c, At_Repair_LBG3__c, QMLBG1andLBG2__c,OpenPOLBG1andLBG2__c,OpenPRLBG1andLBG2__c,OH_LBG1_LBG2__c,Blocked_LBG3__c,TQA__c, X0910_BUs_Unserv__c, X0909_PartOut_KASI__c, RESS_Count__c, Ecode__c, OH_Total__c, 
                    Repairs_In_Transit__c, Cores_In_Transit__c
                    FROM Stock_Info_FLL__c 
                    WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) AND OH_Total__c<2 AND Is_Summary_Record__c=true 
                    	AND Region_and_Segment__c=:regionAndSegment AND Part_Number__r.Hide_From_Planning_Report__c=False
                    ORDER BY USAGE_12M__c DESC
                    LIMIT 30];
        }else if(regionAndSegment=='E2'){
            return [Select Id, OSS_OH__c, Safety_Stock__c,Top_Most__c, Description__c, Essentiality__c,FLL2_OH__c, AT_REPAIR__c, FLL3_BLKD__c, US13_BLKD__c, FLL3_QUAR__c, US13_QUAR__c, FLL3_TO_REP__c, 
                	US13_TO_REP__c, Part_Number__c, Part_Number_Name__c, TOTAL_NETWORK__c,OpenPOFLL1andFLL2__c,OpenPRFLL1andFLL2__c,OH_FLL1_FLL2__c,QMFLL1andFLL2__c, OSS_ALLOCATION__c, USAGE_12M__c, OPEN_PRS__c, CORES__c, QMS_OPEN__c, OPEN_POS__c, RESS_Count__c,
                    Ecode__c, OH_Total__c, Repairs_In_Transit__c, Cores_In_Transit__c
                    FROM Stock_Info_FLL__c 
                    WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true) AND Is_Summary_Record__c=true AND Region_and_Segment__c='FLL COM' AND POOL_CUSTOMERS__c LIKE '%E2%'
                    ORDER BY OH_Total__c ASC, USAGE_12M__c DESC LIMIT 50];
        } else if(regionAndSegment=='E2LBG'){
            return [Select Id,  OSS_OH__c, Safety_Stock__c, Top_Most__c, Description__c, Essentiality__c, Part_Number__c, Part_Number_Name__c, TOTAL_NETWORK__c, OSS_ALLOCATION__c, USAGE_12M__c, OPEN_PRS__c, TQA__c,
                    CORES__c, QMS_OPEN__c, OPEN_POS__c, At_Repair_LBG3__c,QMLBG1andLBG2__c,OpenPOLBG1andLBG2__c,OpenPRLBG1andLBG2__c,OH_LBG1_LBG2__c, Blocked_LBG3__c, X0910_BUs_Unserv__c, X0909_PartOut_KASI__c, RESS_Count__c, Ecode__c, OH_Total__c, 
                    Repairs_In_Transit__c, Cores_In_Transit__c
                    FROM Stock_Info_FLL__c 
                    WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true) AND Is_Summary_Record__c=true AND Region_and_Segment__c='LBG COM' AND POOL_CUSTOMERS__c LIKE '%E2%'
                    ORDER BY OH_Total__c ASC, USAGE_12M__c DESC LIMIT 50];
        }else{
            return null;
        }
    }
    
    public static void doRESSCount(){
        for(String region : new Set<String>{'FLL COM', 'LBG COM'}){			//***Queries and DML in a loop. Not a best practice, but in this case the loop will only run twice.***
            List<Stock_Info_FLL__c> results = [Select Id, Top_Most__c, RESS_Count__c
                                               FROM Stock_Info_FLL__c 
                                               WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) 
                                               AND OH_Total__c<2 AND Is_Summary_Record__c=true AND Region_and_Segment__c=:region
                                               ORDER BY USAGE_12M__c DESC
                                               LIMIT 30];
            countRessesUpdateDB(results, region);
        }
    }
    
    public static void countRessesUpdateDB(List<Stock_Info_FLL__c> advancedSearchResults, string regionAndSegment){
        integer maxLength_Short = 255;						//max length for the RESS_IDs__c field
        integer maxLength_Long = 1000;						//max length for the RESS_RM_Comments__c field
        string region = regionAndSegment.left(3);			//parse regionAndSegment to get region
        string segment = RegionAndSegment.right(3)+'%';		//parse regionAndSegment to get segment, add % for use with "LIKE" in the query
        
        //put search results in a map for easier access, and reset RESS Count to 0
        Map<String, Stock_Info_FLL__c> StockInfosMap = new Map<string, Stock_Info_FLL__c>();
        for(Stock_Info_FLL__c record : advancedSearchResults){
            record.RESS_Count__c=0;
            record.RESS_IDs__c='';
            record.RESS_RM_Comments__c='';
            StockInfosMap.put(record.Top_Most__c, record);
        }
        
        //get all resses matching the search results, add to each search result's RESS count
        set<string> TopMosts = StockInfosMap.keySet();
        for(Ress_Info__c ress : [SELECT Id, Topmost__c, RESS_ID__c, rm_comments__c FROM RESS_Info__c WHERE Topmost__c IN :TopMosts AND Site__c=:region AND Segment__c LIKE :segment AND Customer_Name__c='EMBRAER SERVICES INC.' and RESS_ID__c!=null and Tracking_Sent_Date_and_Time__c=null and Alternate_Action_Date_and_Time__c=null ORDER BY Site__c ASC]){
            Stock_Info_FLL__c temp = StockInfosMap.get(ress.Topmost__c);
            if(temp!=null){
            	temp.RESS_Count__c++;																					//count quantity of resses
                string newValue = (temp.RESS_IDs__c + ' ' + ress.RESS_ID__c).trim();									//new RESS IDs value
                temp.RESS_IDs__c = newValue.length() <= maxLength_Short ? newValue : temp.RESS_IDs__c;					//do not exceed max length of RESS IDs field
                newValue = (temp.RESS_RM_Comments__c + ' (' + ress.rm_comments__c + ')').trim();						//new RM Comments value
                temp.RESS_RM_Comments__c = newValue.length() <= maxLength_Long ?  newValue : temp.RESS_RM_Comments__c;	//do not exceed max length of RM Comments field
            }
        }
        update advancedSearchResults;
    }
    
    public static void doRepairsInTransitCount(){
        for(String region : new Set<String>{'FLL COM', 'LBG COM'}){			//***Queries and DML in a loop. Not a best practice, but in this case the loop will only run twice.***
            List<Stock_Info_FLL__c> results = [Select Id, Top_Most__c, Repairs_In_Transit__c
                                               FROM Stock_Info_FLL__c 
                                               WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) 
                                               AND OH_Total__c<2 AND Is_Summary_Record__c=true AND Region_and_Segment__c=:region
                                               ORDER BY USAGE_12M__c DESC
                                               LIMIT 30];
            countRepairsInTransitUpdateDB(results, region);
        }
    }
    
    public static void countRepairsInTransitUpdateDB(List<Stock_Info_FLL__c> advancedSearchResults, string regionAndSegment){
        //put search results in a map for easier access, and reset In Transit Repairs Count to 0
        Map<String, Stock_Info_FLL__c> StockInfosMap = new Map<string, Stock_Info_FLL__c>();
        for(Stock_Info_FLL__c record : advancedSearchResults){
            record.Repairs_In_Transit__c=0;
            StockInfosMap.put(record.Top_Most__c, record);
        }
        //get all repairs in transit matching the search results, add to each search result's Repairs In Transit count
        set<string> TMs = StockInfosMap.keySet();
        for(Repair_Information__c repair : [SELECT Topmost__c FROM Repair_Information__c WHERE Topmost__c IN :TMs AND Site__c=:regionAndSegment AND AWB_From_Repair__c!='' AND AWB_From_Repair_Deliv_Date__c!=null]){
            Stock_Info_FLL__c temp = StockInfosMap.get(repair.Topmost__c);
            if(temp!=null){
            	temp.Repairs_In_Transit__c++;
            }
        }
        update advancedSearchResults;
    }
    
    public static void doCoresInTransitCount(){
        for(String region : new Set<String>{'FLL COM', 'LBG COM'}){			//***Queries and DML in a loop. Not a best practice, but in this case the loop will only run twice.***
            List<Stock_Info_FLL__c> results = [Select Id, Top_Most__c, Cores_In_Transit__c
                                               FROM Stock_Info_FLL__c 
                                               WHERE (Top_Removal_Commercial__c=true OR Top_Missed__c=true OR USAGE_12M__c>12) 
                                               AND OH_Total__c<2 AND Is_Summary_Record__c=true AND Region_and_Segment__c=:region
                                               ORDER BY USAGE_12M__c DESC
                                               LIMIT 30];
            countCoresInTransitUpdateDB(results, region);
        }
    }
    
    //cores are in transit if Epool Status says shipped, or if user adds the substring "awb" to the core comment
    public static void countCoresInTransitUpdateDB(List<Stock_Info_FLL__c> advancedSearchResults, string regionAndSegment){
        string region = regionAndSegment.left(3);			//parse regionAndSegment to get region
        string segment = RegionAndSegment.right(3)+'%';		//parse regionAndSegment to get segment, add % for use with "LIKE" in the query
        
        //put search results in a map for easier access, and reset In Transit Cores Count to 0
        Map<String, Stock_Info_FLL__c> StockInfosMap = new Map<string, Stock_Info_FLL__c>();
        for(Stock_Info_FLL__c record : advancedSearchResults){
            record.Cores_In_Transit__c=0;
            StockInfosMap.put(record.Top_Most__c, record);
        }
        //get all cores in transit matching the search results, add to each search result's Cores In Transit count
        set<string> TopMosts = StockInfosMap.keySet();
        for(Core_Information__c core : [SELECT Topmost__c FROM Core_Information__c WHERE Topmost__c IN :TopMosts AND Support_Plant__c=:region AND Segment__c LIKE :segment AND (Epool_Status__c LIKE '%shipped%' OR Core_Comment_Body__c LIKE '%awb%')]){
            Stock_Info_FLL__c temp = StockInfosMap.get(core.Topmost__c);
            if(temp!=null){
            	temp.Cores_In_Transit__c++;
            }
        }
        update advancedSearchResults;
    }
    
    //handles creation of a new core comment record
    public static void addNewCoreComment(Core_Information__c CurrentCore, String CoreCommentBody){
        if(CurrentCore.Core_Comment__c != null)
        	delete new Core_Comment__c(Id = CurrentCore.Core_Comment__c);	//delete old core comment if there is one
        Core_Comment__c newComment = new Core_Comment__c(Body__c = CoreCommentBody, Notification__c = CurrentCore.Notification__c, Timestamp__c = datetime.now());
        insert newComment;													//insert new core comment record
        CurrentCore.Core_Comment__c = newComment.Id;						//link the new core comment record to its parent core record
        update CurrentCore;
    }
    
        //handles creation of a new PO comment record
    	public static void addNewPOComment(PO__c CurrentPO, String POCommentBody){
        if(CurrentPO.PO_Comment__c != null)
        	delete new PO_Comment__c(Id = CurrentPO.PO_Comment__c);	//delete old PO comment if there is one
        PO_Comment__c newComments = new PO_Comment__c(Body__c = POCommentBody, Notification__c = CurrentPO.name, Timestamp__c = datetime.now());
        insert newComments;													//insert new PO comment record
        CurrentPO.PO_Comment__c = newComments.Id;						//link the new PO comment record to its parent PO record
        update CurrentPO;
    }
    
    //handles creation of a new QM comment record
    	public static void addNewQMComment(QM__c CurrentQM, String QMCommentBody){
        if(CurrentQM.QM_Comment2__c != null)
       delete new QM_Comment__c(Id = CurrentQM.QM_Comment2__c);	//delete old QM comment if there is one
        QM_Comment__c newCommentQM = new QM_Comment__c(Body__c = QMCommentBody, Notification__c = CurrentQM.name, Timestamp__c = datetime.now());
        insert newCommentQM;													//insert new QM comment record
        CurrentQM.QM_Comment2__c = newCommentQM.Id;						//link the new PO comment record to its parent PO record
        update CurrentQM;
   }
    
    //handles creation of a new PO comment record
    	public static void addNewRepairComment(Repair_Information__c CurrentRepair, String RepairCommentBody){
        if(CurrentRepair.Repair_Comment__c != null)
        	delete new Repair_Comment__c(Id = CurrentRepair.Repair_Comment__c);	//delete old PO comment if there is one
        Repair_Comment__c newCommentRepair = new Repair_Comment__c(Body__c = RepairCommentBody, Notification__c	 = CurrentRepair.Repair_Notification__c	, Timestamp__c = datetime.now());
        insert newCommentRepair;													//insert new PO comment record
        CurrentRepair.Repair_Comment__c = newCommentRepair.Id;						//link the new PO comment record to its parent PO record
        update CurrentRepair;
    }
    
    
    
    public static List<UsageInfoWrapper> getUsageInformationPool(String topMost, String region){
        List<AggregateResult> groupedResults = [SELECT Customer__c, Notification_Date__c, Program__c, Count(Id)ct
                                                FROM Usage_Information__c 
                                                WHERE Top_Most__c=:topMost AND Region__c=:region AND Customer__c NOT IN :repAdminCustomers AND Program__c IN ('POOL', 'SER')
                                                GROUP BY Customer__c, Notification_Date__c, Program__c
                                                ORDER BY Customer__c];
        return initializeUsageInformation(topMost, groupedResults);
    }
    
    public static List<UsageInfoWrapper> getUsageInformationRepAdmin(String topMost, String region){
        List<AggregateResult> groupedResults = [SELECT Customer__c, Notification_Date__c, Count(Id)ct, Exchange_Provided__c, Program__c
                                                FROM Usage_Information__c 
                                                WHERE Top_Most__c=:topMost AND Region__c=:region AND Customer__c IN :repAdminCustomers AND Program__c IN ('POOL', 'SER')
                                                GROUP BY Customer__c, Notification_Date__c, Exchange_Provided__c, Program__c
                                                ORDER BY Customer__c];
        return initializeUsageInformation(topMost, groupedResults);
    }
    
    public static List<UsageInfoWrapper> getUsageInformationEPEP(String topMost, String region){
        List<AggregateResult> groupedResults = [SELECT Customer__c, Notification_Date__c, Count(Id)ct, Program__c
                                                FROM Usage_Information__c 
                                                WHERE Top_Most__c=:topMost AND Region__c=:region AND Program__c = 'EPEP'
                                                GROUP BY Customer__c, Notification_Date__c, Exchange_Provided__c, Program__c
                                                ORDER BY Customer__c];
        return initializeUsageInformation(topMost, groupedResults);
    }
    
    public static List<UsageInfoWrapper> initializeUsageInformation(string topMost, List<AggregateResult> groupedResults){
        Map<String, UsageInfoWrapper> UsageInfoWrapperMap = new Map<String, UsageInfoWrapper>();	//create a map to hold a usage info wrapper instance for each customer
        final String TOTAL_LABEL = 'Total';
        
        for(AggregateResult groupedResult : groupedResults){
            String currentCustomer = (String)groupedResult.get('Customer__c');
            
            countUsageInformation(currentCustomer, groupedResult, UsageInfoWrapperMap);							//add one for this customer
            countUsageInformation(TOTAL_LABEL, groupedResult, UsageInfoWrapperMap);								//add one again to the total
            if(isRepairAdminExchange(groupedResult)){															//if repair admin and exchange, count again
                String exchangeCustomer = currentCustomer.left(currentCustomer.length() - 5) + ' (exc)';		//build the exchange name for the customer
                countUsageInformation(exchangeCustomer, groupedResult, UsageInfoWrapperMap);
            }
        }
        
        //move totals to end of map
        UsageInfoWrapper totals = UsageInfoWrapperMap.remove(TOTAL_LABEL);
        UsageInfoWrapperMap.put(TOTAL_LABEL, totals);
        
        return UsageInfoWrapperMap.values();
    }
    
    private static void countUsageInformation(String currentCustomer, AggregateResult groupedResult, Map<String, UsageInfoWrapper> UsageInfoWrapperMap){
        Date currentNotifDate = (Date)groupedResult.get('Notification_Date__c');
        String currentYearMonth = (currentNotifDate == null) ? null : String.valueOf(currentNotifDate.year()) + String.valueOf(currentNotifDate.month()).leftPad(2, '0');
            
        //get the usage info wrapper for the current customer
        if(!UsageInfoWrapperMap.containsKey(currentCustomer)){
         	UsageInfoWrapperMap.put(currentCustomer, new UsageInfoWrapper(currentCustomer));
        }
        UsageInfoWrapper currentWrapper = UsageInfoWrapperMap.get(currentCustomer);
        
        //get the month/year map from this UsageInfoWrapper instance
        Map<String, Integer> currentMonthYearMap = currentWrapper.MonthYearCount;
            
        //get the count for the current month/year
        if(!currentMonthYearMap.containsKey(currentYearMonth)){
        	currentMonthYearMap.put(currentYearMonth, 0);
        }
        Integer currentCount = currentMonthYearMap.get(currentYearMonth);
            
        //add to the count for the current month/year
        currentCount += (Integer)groupedResult.get('ct');
        currentMonthYearMap.put(currentYearMonth, currentCount);
    }
    
    private static boolean isRepairAdminExchange(AggregateResult groupedResult){
        return groupedResult.get('Program__c') != 'EPEP' 
               && repAdminCustomers.contains((String)groupedResult.get('Customer__c')) 
               && (Boolean)groupedResult.get('Exchange_Provided__c');
    }
}