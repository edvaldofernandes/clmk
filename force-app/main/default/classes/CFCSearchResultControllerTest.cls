@isTest
public class CFCSearchResultControllerTest {
    
static testMethod void myUnitTestFlightOperations() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 

        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    }
    
    static testMethod void myUnitTestMaintenanceEngineering() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 

        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    }
    
    static testMethod void myUnitTestTraining() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 

        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    }
    
    static testMethod void myUnitTestMaterialsLogistics() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 

        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    }
    
    static testMethod void myUnitTestLessors() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 

        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    }
    
    static testMethod void myUnitTestAircraftModification() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 

        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    }
    
    static testMethod void myUnitTesteESolutions() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId(); 

        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordTypeId;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        produto.Aircraft_Model__c = 'E195';
        
        produto.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
        produto.Indicates_MRO__c = true;
        
        database.insert(produto); 
        
        system.debug('CFCSearchResultControllerTest.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('q', '%%');
        ApexPages.currentPage().getParameters().put('productCategory', 'Maintenance');
        ApexPages.currentPage().getParameters().put('fleetType', 'teste');
        ApexPages.currentPage().getParameters().put('operationContext', 'teste');
        ApexPages.currentPage().getParameters().put('motivationForSales', 'teste');
        ApexPages.currentPage().getParameters().put('applicabilityCompany', 'teste');
        ApexPages.currentPage().getParameters().put('selectedValueOrderBy', 'teste');
        ApexPages.currentPage().getParameters().put('productFamily', 'Aircraft Modification;Pilots');
        ApexPages.currentPage().getParameters().put('productType','Aircraft Modification;eTechPubs');
        ApexPages.currentPage().getParameters().put('ataChapter','ATA 02 -General Requirements;ATA 33 - Lights');
        
        CFCSearchResultController controller = new CFCSearchResultController();
        controller.requestProductId = produto.Id;
        controller.getOptionsOrderBy();
        controller.GenerateValueOrderBy('Products A-Z');
        controller.GenerateValueOrderBy('Products Z-A');
        controller.GenerateValueOrderBy('teste');
        controller.MethodOne();
        controller.SearchFilter();

        controller.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');
        
        CFCSearchResultController controller2 = new CFCSearchResultController(new ApexPages.StandardController(Produto));
        controller2.requestProductId = produto.Id;
        controller2.getOptionsOrderBy();
        controller2.GenerateValueOrderBy('Products A-Z');
        controller2.GenerateValueOrderBy('Products Z-A');
        controller2.GenerateValueOrderBy('teste');
        controller2.MethodOne();
        controller2.SearchFilter();

        controller2.subTitle = 'teste';
        controller.GeneratePageTitle('Flight');
        controller.GeneratePageTitle('Maintenance');
        controller.GeneratePageTitle('Training');
        controller.GeneratePageTitle('Materials');
        controller.GeneratePageTitle('Lessors');
        controller.GeneratePageTitle('Modifications');
        controller.GeneratePageTitle('eSolutions');
        controller.GeneratePageTitle(null);
        controller.GenerateProductCategory('Flight');
        controller.GenerateProductCategory('Maintenance');
        controller.GenerateProductCategory('Training');
        controller.GenerateProductCategory('Materials');
        controller.GenerateProductCategory('Lessors');
        controller.GenerateProductCategory('Modifications');
        controller.GenerateProductCategory('eSolutions');

        system.assert(controller.lstProducts.size() == 0);                
        system.assert(controller2.lstProducts.size() == 0);                
    } 
    
    static testMethod void testClickRequestProposal(){
        
        Id recordTypeProductId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
        system.debug('CFCSearchResultControllerTest.testClickRequestProposal.recordTypeProduct >>> ' + recordTypeProductId);

        User userTest = CFCTestSetup.getUser();
        system.debug('CFCSearchResultControllerTest.testClickRequestProposal.userTest >>> ' + userTest);
        
        System.runAs(userTest){
            Product2 prodTest = new Product2();
            prodTest.Name = 'Produto Teste';
            prodTest.ProductCode = 'a488';
            prodTest.Product_Status__c = 'Active';
            prodTest.RecordTypeId = recordTypeProductId;
            prodTest.IsActive = true;
            prodTest.Publication_Status__c = 'Published';
            prodTest.Aircraft_Model__c = 'E195';
            prodTest.Applicability__c = 'ERJ';
            prodTest.Maintance_Capability__c = 'EMB 120-Aircraft Capab.-Line';
            prodTest.Indicates_MRO__c = true;            
            database.insert(prodTest);
            system.debug('CFCSearchResultControllerTest.testClickRequestProposal.prodTest >>> ' + prodTest);            
                       
            CFCSearchResultController controller3 = new CFCSearchResultController();
            controller3.requestProductId = prodTest.Id;
            controller3.RequestProposal();
        }
    }

}