/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class to post the service contract relationship on chatter.
* 
* NAME: ServiceContractChatter.cls
* AUTHOR: LRSA                                                DATE: 04/12/2014
*
*******************************************************************************/

public with sharing class ServiceContractChatter {
	
	private static boolean ServiceContractChatter = false;
	
	private static void publish( list< ServiceContract > aListSC, set< id > aSetParent )
	{
		if ( aListSC.isEmpty() ) return;
    
    map< id, ServiceContract > lMapParentSC = new map< id, ServiceContract >( [ select id, name, Ownerid, Account.Ownerid
      from ServiceContract where id=:aSetParent ] );
      
    if ( lMapParentSC.isEmpty() ) return;
    
    list< FeedItem > lListFeedItem = new list< FeedItem >();
    
    for ( ServiceContract lSC : aListSC )
    {
      //FeedItem lFI = new FeedItem();
      ServiceContract lParent = lMapParentSC.get( lSC.Contract_hierarchy__c );
      ChatterUtils.mentionTextPost( lParent.id, new list< id >{ lParent.Ownerid, lParent.Account.Ownerid }, lSC.id, 
        '\n' + lSC.name +'\nView Contract:' );
     }
    
    ServiceContractChatter = true;
    
    //insert lListFeedItem;
	}
	
	public static void newServiceContract()
	{
		TriggerUtils.assertTrigger();
		
		if ( ServiceContractChatter ) return;
		
		list< ServiceContract > lListSC = new list< ServiceContract >();
		set< id > lSetSC = new set< id >();
		
		for ( ServiceContract lSC : ( list< ServiceContract > )trigger.new )
		{
			if ( lSC.Contract_hierarchy__c != null )
			{
			  lListSC.add( lSC );
			  lSetSC.add( lSC.Contract_hierarchy__c );
			}
		}
		publish( lListSC, lSetSC );
	}
	
	public static void updateServiceContract()
	{
		TriggerUtils.assertTrigger();
		
		if ( ServiceContractChatter ) return;
    
    list< ServiceContract > lListSC = new list< ServiceContract >();
    set< id > lSetSC = new set< id >();
    
    for ( ServiceContract lSC : ( list< ServiceContract > )trigger.new )
    {
      if ( TriggerUtils.wasChanged( lSC, ServiceContract.Contract_hierarchy__c ) && lSC.Contract_hierarchy__c != null )
      {
        lListSC.add( lSC );
        lSetSC.add( lSC.Contract_hierarchy__c );
      }
    }
    
    publish( lListSC, lSetSC );
	}

}