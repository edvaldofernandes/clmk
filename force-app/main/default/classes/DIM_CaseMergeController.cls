// Used by DIM - Market Intelligence.
public without sharing class DIM_CaseMergeController { 

    public String message {get;set;}
    
    private String caseFromId;
    public Case caseFrom {get;set;}
    public Case caseTo {get;set;}
    
    public boolean isValid {get;set;}
    public boolean isMerged {get;set;}
    
    public List<Case> results {get;set;}
    public string searchString {get;set;}
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public DIM_CaseMergeController(ApexPages.StandardController controller) {
    
        this.caseFromId = ApexPages.currentPage().getParameters().get('sourceId');
        if(this.caseFromId != '') {
            this.caseFrom = [SELECT Id, CaseNumber, Subject, Status, Account.Name, RecordTypeId FROM Case WHERE Id =: caseFromId];
        }

        if(validate()) {
            searchString = '';
            runSearch();
        }
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    private Boolean validate() {
        
        isValid = true;
        
        if (caseFrom == null) {
            addErrorMessage('The source case must not be empty.');
            isValid = false;
        }    
        else if (caseFrom.Status != 'Open') {
            addErrorMessage('The source case must be in the "Open" status in order to be merged.');
            isValid = false;
        }
        
        return isValid;
    }
        
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public PageReference save() {
        
        if (caseTo == null) {
            addErrorMessage('The target case must not be empty.');
            isValid = false;
        } 
        else if(validate()) {

            List<EmailMessage> fromEmails = [SELECT ToAddress, CcAddress, BccAddress, FromAddress, FromName, HasAttachment, Headers, HtmlBody, Id, Incoming, MessageDate, ParentId, Status, Subject, TextBody FROM EmailMessage WHERE ParentId =: caseFrom.Id];
                
            Set<Id> fromEmailIds = new Set<Id>();    
            for(EmailMessage fromEmail : fromEmails) {
                fromEmailIds.add(fromEmail.Id);
            }
                
            List<Attachment> fromAttachments = [SELECT Body, BodyLength, ContentType, CreatedById, Description, Id, Name, OwnerId, ParentId FROM Attachment WHERE ParentId IN :fromEmailIds];
            
            Map<EmailMessage, List<Attachment>> toEmailAttachments = new Map<EmailMessage, List<Attachment>>();
            List<EmailMessage> toEmails = new List<EmailMessage>();
            
            for(EmailMessage fromEmail : fromEmails){
                EmailMessage toEmail = fromEmail.clone(false, true, true, true);
                toEmail.ParentId = caseTo.Id;
                toEmails.add(toEmail);
                
                List<Attachment> toAttachments = new List<Attachment>();
                
                for (Attachment fromAttachment : fromAttachments) {
                    if (fromAttachment.ParentId == fromEmail.Id) {
                        toAttachments.add(fromAttachment.clone(false, true, true, true));
                    }
                }
                toEmailAttachments.put(toEmail, toAttachments);
            }
            insert toEmails;
            
            List<Attachment> toAttachments = new List<Attachment>();
            
            for(EmailMessage toEmail : toEmailAttachments.keySet()) {
                for(Attachment toEmailAttachment : toEmailAttachments.get(toEmail)) {
                    toEmailAttachment.ParentId = toEmail.Id;
                    toAttachments.add(toEmailAttachment);
                }
            }
            insert toAttachments;
            
            delete caseFrom;
            
            isMerged = true;

            return new ApexPages.StandardController(caseTo).view();
        }          
        return null;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------
    private static void addErrorMessage(String err) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, err));
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    private static void addSuccessMessage(String message) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, message));
    }  
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    // Performs the keyword search.
    public PageReference search() {
        runSearch();
        return null;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public PageReference clear() {
        searchString = '';
        return search();
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    // Prepare the query and issue the search command.
    private void runSearch() {
        results = performSearch(searchString);               
    } 
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    // Run the search and return the records found.
    private List<Case> performSearch(string searchString) {
    
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Support').getRecordTypeId();
        
        String soql = 'SELECT Id, CaseNumber, Subject, Status FROM Case';
        
        if(searchString != '') {
            soql = soql + ' WHERE RecordTypeId = :recordTypeId AND Id !=: caseFromId AND (Subject LIKE \'%' + searchString +'%\' OR CaseNumber LIKE \'%' + searchString +'%\') AND (Status IN (\'Open\', \'In Progress\', \'Done\', \'Delivered\') OR Archive__c = TRUE)';
        }
        else {
            soql = soql + ' WHERE RecordTypeId = :recordTypeId AND Id !=: caseFromId AND (Status IN (\'Open\', \'In Progress\', \'Done\', \'Delivered\') OR Archive__c = TRUE)';
        }
        
        soql = soql +  ' ORDER BY CaseNumber DESC';
        soql = soql + ' LIMIT 50';
        return database.query(soql); 
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------    
    public void updateCaseTo() {
        String caseToId = Apexpages.currentPage().getParameters().get('caseToId');
        caseTo = [SELECT Id, CaseNumber, Subject, Status, Account.Name, RecordTypeId FROM Case WHERE Id =: caseToId];
    }
}