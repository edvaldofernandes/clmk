@isTest(seeAllData=true)
global  class SalesForceRequestQuestionsSchedullerTest implements WebServiceMock{
    
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            
            ETRACK_OUT_BOUND_END_POINT.updateFromSalesForceResponse SfInfoElement = new ETRACK_OUT_BOUND_END_POINT.updateFromSalesForceResponse();
            SfInfoElement.status = Constants.STATUS_CALL_OUT;        
            response.put('response_x', SfInfoElement); 
        }
    
    @isTest static void salesForceRequestQuestionsSchedullerTest(){
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new SalesForceRequestQuestionsSchedullerTest());
       
        String sch = '0 0 10 * * ?';
        
        SalesForceRequestQuestionsScheduller sfQuestionScheduller = new SalesForceRequestQuestionsScheduller();
        
        System.Schedule('Etrack Questions Scheduller',sch, sfQuestionScheduller);
                
        Test.stopTest();
    }
    
    @isTest static void testRequest() {              
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new SalesForceRequestQuestionsSchedullerTest());
        
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element SfInfoElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
        ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort rcpSFRequest = new ETRACK_OUT_BOUND_END_POINT.SFDCtoEtrackWSPort();
        
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount accountInfo = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount();
        accountInfo.accountType = 'test';
        accountInfo.companyName = 'test';
        accountInfo.companyNickname = 'test';
        accountInfo.companyStatus = 'test';
        accountInfo.flyEmbraerID = 'test';
        accountInfo.objectType = 'test';
        accountInfo.status = 'test';
        
        ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember teamMember = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember();
        teamMember.accountId = 'test';
        teamMember.email = 'test';
        teamMember.name = 'test';
        teamMember.teamMemberRole = 'test';
        teamMember.phone = 'test';
        teamMember.flyEmbraerID = 'test';
        teamMember.objectType = 'test';
        teamMember.status = 'test';
   
        String output = rcpSFRequest.updateFromSalesForceRequest(SfInfoElement);
        
        System.assertEquals(Constants.STATUS_CALL_OUT, output);
        
        Test.stopTest();
    }
}