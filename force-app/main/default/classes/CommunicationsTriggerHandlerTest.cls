@isTest
public with sharing class CommunicationsTriggerHandlerTest {
  static Id embraerType = Schema.SObjectType.Account.getRecordTypeInfosByName()
    .get('Embraer Site')
    .getRecordTypeId();

  static Id airlineType = Schema.SObjectType.Account.getRecordTypeInfosByName()
    .get('Airline')
    .getRecordTypeId();

  private testMethod static void EISCommunicationTest() {
    Account embraerSite = createAccount('embraer', embraerType, null, '');
    Database.insert(embraerSite);
    Id idEmbraer = [SELECT Id FROM Account WHERE Name = 'embraer' LIMIT 1].Id;

    Account randomAirline = createAccount(
      'rndBlue',
      airlineType,
      idEmbraer,
      'Turboprop;EJet'
    );
    Database.insert(randomAirline);
    Account acc = [SELECT Id FROM Account WHERE Name = 'rndBlue'];

    Account otherAirline = createAccount(
      'rndAir',
      airlineType,
      idEmbraer,
      'EJet;E2'
    );
    Database.insert(otherAirline);
    Account accOther = [SELECT Id FROM Account WHERE Name = 'rndAir'];

    Aircraft__c ac = createAircraft('19000012', 'XF-1284', acc);
    Database.insert(ac);

    ac = createAircraft('19000112', 'XF-5234', acc);
    Database.insert(ac);

    ac = createAircraft('19000102', 'DF-3484', accOther);
    Database.insert(ac);

    Communications__c eis = createEIS(acc);
    Database.insert(eis);
    eis = [SELECT Id, communication_status__c FROM Communications__c LIMIT 1];
    eis.communication_status__c = 'Issued';
    Database.update(eis);

    CommunicationsTriggerHandler commTrigger = new CommunicationsTriggerHandler(
      true
    );

    commTrigger.newRecords = [
      SELECT id, communication_status__c
      FROM Communications__c
      LIMIT 1
    ];
    system.assertEquals(
      commTrigger.newRecords[0].communication_status__c,
      'Issued',
      '[ERROR] Failed to create a new EIS daily report!'
    );
  }

  private testMethod static void RECCommunicationTest() {
    Communications__c rec = createREC();
    Database.insert(rec);

    Contact cont = createContact('contancttest', true);
    Database.insert(cont);

    rec.update__c = '<p>new updates</p>';
    rec.resubmit__c = true;
    Database.update(rec);

    rec = [
      SELECT id, summary__c, resubmit__c
      FROM communications__c
      WHERE name = 'rectesting'
      LIMIT 1
    ];
    system.assertEquals(
      rec.resubmit__c,
      false,
      '[ERROR] The Resubmit Field (REC Communication) is not working properly!'
    );

    try {
      rec.resubmit__c = true;
      rec.summary__c = '<p> teste </p><img href="https://test.test/servlet/rtaImage?id=test" alt="test"></img>';
      Database.update(rec);
    } catch (AsyncException e) {
      system.assert(
        e.getMessage().contains('You need save changes before send an email!'),
        '[ERROR] Error message "need save changes before send an email" is not working properly!'
      );
    }
  }

  private static Account createAccount(
    String name,
    id recordType,
    Id idEmbraerSite,
    String fleetType
  ) {
    Account acc = new Account();
    acc.Name = name;
    acc.Company_Nickname__c = name + 'cnc';
    acc.Geographical_Area__c = 'Latin America';
    acc.recordTypeId = embraerType;
    if (recordType == airlineType) {
      acc.Company_status__c = 'Active';
      acc.Training_status__c = 'Active';
      acc.recordTypeId = airlineType;
      acc.embraer_site__c = idEmbraerSite;
      acc.Quantity_Embraer_Aircraft_Operator__c = 10;
      acc.Embraer_a_c_in_service_only__c = 9;
      acc.Embraer_Fleet_Type__c = fleetType;
    }
    return acc;
  }

  private static Communications__c createEIS(Account acc) {
    Id idEISRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('EIS Daily Report')
      .getRecordTypeId();

    Communications__c eis = new Communications__c();
    eis.RecordTypeId = idEISRecordType;
    eis.reference_date__c = system.today();
    eis.operator__c = acc.Id;
    eis.Name = 'eistest';
    eis.communication_status__c = 'draft';
    return eis;
  }

  private static Communications__c createREC() {
    Id idRECRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('REC')
      .getRecordTypeId();
    Communications__c rec = new Communications__c();
    rec.RecordTypeId = idRECRecordType;
    rec.communication_status__c = 'Issued';
    rec.reference_date__c = system.today();
    rec.ata_chapter__c = '00 GENEREAL';
    rec.technology__c = 'AMS';
    rec.fleet_type__c = '195-E2';
    rec.Name = 'rectesting';
    return rec;
  }

  private static Aircraft__c createAircraft(
    String sn,
    String reg,
    Account acc
  ) {
    Aircraft__c ac = new Aircraft__c();
    ac.name = sn;
    ac.registration__c = reg;
    ac.aircraft_status__c = 'In Service';
    ac.market__c = 'Commercial';
    ac.fleet_type__c = 'E-JET E2';
    ac.model_type__c = 'E2 190';
    ac.commercial_name__c = 'E-190 E2';
    ac.operator__c = acc.id;
    ac.owner__c = acc.id;
    ac.fh_accumulated__c = '1900';
    ac.fc_accumulated__c = '1500';
    ac.OEM_delivery_date__c = system.today();
    ac.EIS_daily_scheduled_flights__c = 3;
    ac.EIS_AOG_Action__c = 'test';
    ac.EIS_AOG_Status__c = true;

    return ac;
  }

  private static Contact createContact(String name, boolean recInterest) {
    Contact ct = new Contact();
    // ct.Name = name + ' ' + name;
    ct.LastName = 'last' + name;
    ct.title = 'Engineer';
    ct.contact_status__c = 'Active';
    ct.email = name + '@wht.emb';
    // ct.AccountId = accId;
    ct.Relevant_Events_Communication__c = recInterest;
    return ct;
  }
}