// Used by DIM - Market Intelligence.
public with sharing class DIM_PBuyPWinController {

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public DIM_PBuyPWinController() {
        setDefaultOffice();
        setDefaultRSM();
        setDefaultType();
        setDefaultPeriod();
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    private void setDefaultOffice() {
    
        String userRoleId = UserInfo.getUserRoleId();
        List<UserRole> roles = [SELECT Name FROM UserRole WHERE Id =: userRoleId];
        String officeId;
        
        if (roles.size() > 0) {
        
            String roleName = roles[0].Name.toUpperCase();
        
            if (roleName.contains('ASIA PACIFIC')) {
                officeId = '001i000000sVUaE';
                officeLabel = 'Asia Pacific';
            }
            else if (roleName.contains('CHINA')) {
                officeId = '001i000000tLR7f';
                officeLabel = 'China';
            }
            else if (roleName.contains('AMERICA')) {
                officeId = '001i000000sX5Mc\',\'001i000000sVbmM';
                officeLabel = 'Americas';
            }
            else if (roleName.contains('EMEA')) {
                officeId = '001i000000sX8Ye\',\'001i000000tLR7a';
                officeLabel = 'Americas';
            }
            else {
                officeId = 'All';
                officeLabel = '-- ALL --';
            }
            office = '\'' + officeId + '\'';
        }
        else {
            office = '\'All\'';
            officeLabel = '-- ALL --';
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    private void setDefaultRSM() {

        String userId = UserInfo.getUserId();
        
        if (userId == '005i0000006uP3dAAE' || userId == '005i0000006GIixAAG' || userId == '005i00000045t8iAAA' || 
            userId == '005i0000005R32gAAC' || userId == '005i0000007jq8kAAA' || userId == '005i0000007sCB8AAM') {
            rsm = '\'All\'';
        }
        else {
            List<Opportunity> opportunities = [SELECT Id FROM Opportunity WHERE OwnerId =: userId LIMIT 1];
            
            if (opportunities.size() > 0) {
                rsm = '\'' + userId + '\'';
            }
            else {
                rsm = '\'All\'';
            }
        }
        rsmLabel = '';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    private void setDefaultType() {
        List<RecordType> records = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND Name IN ('New Aircraft Sales', 'Used Aircraft Sales')];
        String newAircraftSalesId = String.valueOf(records[0].Id).subString(0,15);
        String usedAircraftSalesId = String.valueOf(records[1].Id).subString(0,15);
        
        type = '\'' + newAircraftSalesId + '\', ' + '\'' + usedAircraftSalesId + '\'';
        typeLabel = '-- ALL --';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    private void setDefaultPeriod() {
        period = formatDate(Date.today().toStartofWeek().addDays(6));
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public String office { get; set; }
    public String officeLabel { get; set; }
    
    private String officeImage;
    public String getOfficeImage() {
        String iconURL = '/resource/DIM_PBuy_PWin/Icons/PWin_Office_' + officeLabel + '.png';
        String defaultIconURL = '/resource/DIM_PBuy_PWin/Icons/PWin_Office_All.png';
        return getFinalURL(iconURL, defaultIconURL);
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
   
    public String rsm { get; set; }
    public String rsmLabel { get; set; }
    
    private String rsmImage;
    public String getRSMImage() {
        if(String.isBlank(rsmLabel)) {
            String id = rsm.replace('\'', '');
            List<User> users =  [SELECT SmallPhotoUrl FROM User WHERE Id =: id];
            if (users.size() > 0) {
                return users.get(0).SmallPhotoUrl;
            }
        }
        else {
            return [SELECT SmallPhotoUrl FROM User WHERE Id =: rsmLabel].SmallPhotoUrl;
        }
        return '/profilephoto/005/T';
    }
    
//---------------------------------------------------------------------------------------------------------------------------------------------------- 
    
    public String type { get; set; }
    public String typeLabel { get; set; }
    
    private String typeImage;
    public String getTypeImage() {
        String iconURL = '/resource/DIM_PBuy_PWin/Icons/PWin_Type_' + typeLabel + '.png';
        String defaultIconURL = '/resource/DIM_PBuy_PWin/Icons/PWin_Type_All.png';
        return getFinalURL(iconURL, defaultIconURL);
    }
    
//---------------------------------------------------------------------------------------------------------------------------------------------------- 
       
    public String period { get; set; }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------   
    
    @TestVisible
    private String getFinalURL(String iconURL, String defaultIconURL) {
        try {
            new PageReference(iconURL).getContent();
            return iconURL;
        } 
        catch(VisualforceException e) {
            return defaultIconURL;
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @RemoteAction
    public static List<Bubble> getBubbles(String office, String rsm, String type, String period) {

        List<Bubble> bubbles = new List<Bubble>();
        List<Opportunity> opportunities = Database.query(
            'SELECT Id, StageName, Leadership_Report_Firm_Order__c, ' +
            '    AccountId, Account.Name, Account.Company_Nickname__c, Account.DIM_Nickname__c, OwnerId, Owner.Name, DIM_Aircraft_Model__c, RecordType.Name ' +
            'FROM Opportunity ' +
            'WHERE RecordTypeId IN (' + type + ') AND  ' +
            '    (NOT StageName LIKE \'Close%\') AND  ' +
            '    (NOT StageName LIKE \'Archive%\') ' + 
            getOfficeQuery(office) + getRSMQuery(rsm) + ' ' +
            'ORDER BY Probability ASC, Leadership_Report_Firm_Order__c DESC'
        );
        
        List<String> periodSplit = period.split('-');
        Date referenceDate = Date.newInstance(Integer.valueOf(periodSplit[0]), Integer.valueOf(periodSplit[1]), Integer.valueOf(periodSplit[2]));
        
        List<AggregateResult> pBuyResults = new List<AggregateResult>([          
            SELECT Account__c Id, AVG(Score_Value__c) Score
            FROM PBuy__c
            WHERE Date__c <=: referenceDate
            GROUP BY Account__c, Date__c
            ORDER BY Account__c, Date__c DESC
        ]);
        
        Map<Id, AggregateResult> pBuys = new Map<Id, AggregateResult>();
        for (AggregateResult result : pBuyResults) {
            String key = String.valueOf(result.get('Id'));
            if (!pBuys.containsKey(key)) {
                pBuys.put(key, result);
            }
        }
        
        List<AggregateResult> pWinResults = new List<AggregateResult>([
            SELECT Opportunity__c Id, AVG(Score_Value__c) Score, MIN(Deal_Positioning__c) Positioning
            FROM PWin__c
            WHERE Date__c <=: referenceDate
            GROUP BY Opportunity__c, Date__c
            ORDER BY Opportunity__c, Date__c DESC
        ]);
        
        Map<Id, AggregateResult> pWins = new Map<Id, AggregateResult>();
        for (AggregateResult result : pWinResults) {
            String key = String.valueOf(result.get('Id'));
            if (!pWins.containsKey(key)) {
                pWins.put(key, result);
            }
        }
        
        for (Opportunity opportunity : opportunities) {
            Integer pBuyScore = getScoreFromMap(pBuys, opportunity.AccountId);
            Integer pWinScore = getScoreFromMap(pWins, opportunity.Id);
            
            
            if(!(pBuyScore == -1 && pWinScore == -1)) {
            
                if(pBuyScore == -1) {pBuyScore = 0;}
                if(pWinScore == -1) {pWinScore = 0;}
            
                String pWinPositioning = getPositioningFromMap(pWins, opportunity.Id);
                bubbles.add(new Bubble(opportunity, pBuyScore, pWinScore, pWinPositioning));
            }
        }
        
        return bubbles;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static Integer getScoreFromMap(Map<Id, AggregateResult> mapResult, String id) {
        AggregateResult result = mapResult.get(id);
        if (result != null) {
            return Integer.valueOf(result.get('Score'));
        }
        return -1;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static String getPositioningFromMap(Map<Id, AggregateResult> mapResult, String id) {
        AggregateResult result = mapResult.get(id);
        if (result != null) {
            return String.valueOf(result.get('Positioning'));
        }
        return 'Neutral';
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @TestVisible
    private static String getOfficeQuery(String office) {
        if(office != null) {
            if(!office.equalsignorecase('\'ALL\'')) {
                return ' AND Account.Embraer_Site__c IN (' + office + ')';
            }
        }
        return '';
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @TestVisible
    private static String getRSMQuery(String rsm) {
        if(rsm != null) {
            if(!rsm.equalsignorecase('\'ALL\'')) {
                return ' AND OwnerId IN (' + rsm + ')';
            }
        }
        return '';
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static String getAccountShortName(Opportunity opp) {
        if(!String.isBlank(opp.Account.DIM_Nickname__c)) {
            return opp.Account.DIM_Nickname__c;
        }
        else if(!String.isBlank(opp.Account.Company_Nickname__c)) {
            return opp.Account.Company_Nickname__c;
        }
        
        return opp.Account.Name;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    class Bubble {
        public String id { get; set; }
        public Integer pWin { get; set; }
        public Integer pBuy { get; set; }
        public String stage { get; set; }
        public Integer quantity { get; set; }
        
        public String accountId { get; set; }
        public String opportunityId { get; set; }
        public String rsm { get; set; }
        public String aircraft { get; set; }
        public String condition { get; set; } 
        public String positioning { get; set; }

        public Bubble(Opportunity opp, Integer pBuy, Integer pWin, String positioning) {
            this.id = getAccountShortName(opp);
            this.pBuy = pBuy;
            this.pWin = pWin;
            this.stage = opp.StageName.replace('&', 'and');
            this.quantity = Integer.valueOf(opp.Leadership_Report_Firm_Order__c);
            
            this.opportunityId = opp.Id;
            this.accountId = opp.AccountId;
            this.rsm = opp.Owner.Name;
            
            this.aircraft = opp.DIM_Aircraft_Model__c.removeEnd(';');            
            this.condition = opp.RecordType.Name.split(' ')[0].replace('Used', 'Pre-Owned');
            this.positioning = positioning;
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public List<SelectOption> getRSMs() {

        List<AggregateResult> names = Database.query(
            'SELECT OwnerId, Owner.Name ' +
            'FROM Opportunity ' +
            'WHERE RecordTypeId IN (\'012i0000001IyKG\', \'012i0000001IyKH\') AND  ' +
            '    (NOT StageName LIKE \'Close%\') AND  ' +
            '    (NOT StageName LIKE \'Archive%\') ' + 
            getOfficeQuery(office) + ' ' +
            'GROUP BY OwnerId, Owner.Name ' +
            'ORDER BY Owner.Name ASC'
        );
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('\'All\'', '-- ALL --'));
        
        for (AggregateResult name : names) {
            options.add(new SelectOption('\'' + String.valueOf(name.get('OwnerId')) + '\'', String.valueOf(name.get('Name'))));
        }
        
        return options;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public List<SelectOption> getPeriods() {

        List<SelectOption> options = new List<SelectOption>();
        
        for (Integer i = 0; i < 10; i++) {
        
            Date currentDate = Date.today() - 7 * i;
            Date weekStartDate = currentDate.toStartofWeek();
            DateTime weekStartDateTime = DateTime.newInstance(weekStartDate.year(), weekStartDate.month(), weekStartDate.day());
            
            Integer week = Integer.valueOf(DateTime.newInstance(weekStartDate.year(), weekStartDate.month(), weekStartDate.day()).format('w'));
            String formatedWeekStartDate = weekStartDateTime.format('MMM dd') + getDayOfMonthSuffix(weekStartDate.day()) + ', ' + weekStartDateTime.format('yyyy');
            
            String formatedWeekStartNumber = String.valueOf(week);
            if (formatedWeekStartNumber.length() == 1) {formatedWeekStartNumber = '0' + formatedWeekStartNumber;}
        
            String value = formatDate(weekStartDate.addDays(6));
            String label = 'Week ' + formatedWeekStartNumber + ' - ' + formatedWeekStartDate;
            options.add(new SelectOption(value, label));
        }
        
        return options;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private String formatDate(Date d) {
        DateTime dTime = DateTime.newInstance(d.year(), d.month(), d.day());
        return dTime.format('yyyy-MM-dd');
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private String getDayOfMonthSuffix(Integer day) {
        if (day == null) {
            return '';
        }

        if (day >= 11 && day <= 13) {
            return 'th';
        }

        Integer modResult = Math.mod(day, 10);        
        if (modResult == 1) { 
            return 'st'; 
        }
        else if (modResult == 2) { 
            return 'nd'; 
        }
        else if (modResult == 3) { 
            return 'rd'; 
        }
        else { 
            return 'th';
        }
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public void addLog() {
        DIM_Audit__c log = new DIM_Audit__c();
        log.Content__c = Apexpages.currentPage().getParameters().get('contentTitle');
        log.Is_Mobile__c = Boolean.valueOf(Apexpages.currentPage().getParameters().get('isMobile'));
        insert log;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
}