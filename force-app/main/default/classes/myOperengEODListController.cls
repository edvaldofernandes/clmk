public with sharing class myOperengEODListController {
    @auraEnabled
    public static List <EOD__c> getEods()
    {   
        List<EOD__c> list_eods = new List<EOD__c>();
        
        String email = UserInfo.getUserEmail();
        
        if(email.indexOf('embraer') != -1){
            
            EOD__c [] eods = [SELECT Id, Name, Related_Case__c, Operational_Disposition__c, Description__c, Status__c, Subject__c, Applicability__c, LastModifiedDate, Expiry_Date__c, EOD_Number__c FROM EOD__c];
            for(integer i = 0; i < eods.size(); i++){
            	list_eods.add(eods[i]);
            }
            
        }else{
            
        	User[] u =[SELECT ContactId FROM User WHERE Id=: UserInfo.getUserId() LIMIT 1];
        	Contact[] contact = [SELECT AccountId FROM Contact WHERE Id =: u[0].ContactId LIMIT 1];
        	if(contact.size()>0)
        	{           
            	for(integer i = 0; i < contact.size(); i++){
                	Case[] cases =[SELECT Id, Owner.Name, CaseNumber FROM Case WHERE Case.AccountId =: contact[i].AccountId]; 
                	EOD__c [] eods = [SELECT Id, Name, Related_Case__c, Operational_Disposition__c, Description__c, Status__c, Subject__c, Applicability__c, LastModifiedDate, Expiry_Date__c, EOD_Number__c FROM EOD__c];
                	for(integer linha = 0; linha < cases.size(); linha++){
                    	for(integer coluna = 0; coluna < eods.size(); coluna++){
                        	if(cases[linha].Id == eods[coluna].Related_Case__c){
                            	list_eods.add(eods[coluna]);
                        	}
                    	}
                	}
            	}
        	}
            
        }
        
        

        return list_eods;
    }
    
    @auraEnabled
    public static String getCaseNumber(String id){
        Case[] cases = [SELECT CaseNumber FROM Case WHERE Id =: id];
        return cases[0].CaseNumber;
    } 

}