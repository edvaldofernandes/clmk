@isTest
public class CFCGlobalSearchComponentControllerTest 
{
    static testMethod void myUnitTest() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 

        Id recordTypeProdId = Schema.SObjectType.Proposal__c.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 

        User userTest = CFCTestSetup.getUser();
                
        System.runAs(userTest) 
        { 
            system.debug('userTest: '+userTest);
            
            Contact conCFC = [select id,AccountId from Contact where id =:userTest.ContactId];
            system.debug('conCFC: '+conCFC);
            
            Product2 Produto = new Product2();
            Produto.Name = 'Produto Teste';
            Produto.ProductCode = 'a488';
            Produto.Product_Status__c = 'Active';
            Produto.RecordTypeId = recordTypeId;
            Produto.Publication_Status__c = 'Published';
            Produto.IsActive = true;
            database.insert(Produto);  
            system.debug('produto: ' +Produto);
            
            Proposal__c proposal = new Proposal__c();
            proposal.Email__c = 'anmelo@deloitte.com';  
            proposal.Aircraft_Type__c = 'ERJ'; 
            proposal.Aircraft_Model__c = 'E170';
            proposal.RecordTypeId = recordTypeProdId;
            proposal.Opportunity_Create__c = FALSE; 
            proposal.Contact__c = userTest.ContactId;
            proposal.Account__c = conCFC.AccountId;
            database.insert(proposal);
            system.debug('proposal: ' +proposal);
            
            Proposal_Items__c proposalItems = new Proposal_Items__c();
            proposalItems.Proposal__c = proposal.Id;
            proposalItems.Product__c = Produto.Id;
            database.insert(proposalItems); 
            system.debug('proposalItems: ' +proposalItems);            
            
            CFCGlobalSearchComponentController globalSearchComponent = New CFCGlobalSearchComponentController(); 
            globalSearchComponent.ProductName = 'Produto Teste';
            globalSearchComponent.ProductChoosen = 'Produto Teste';
            CFCGlobalSearchComponentController.autoCompleteList('Produto Teste');
            globalSearchComponent.SearchProduct();
            
            system.assert(globalSearchComponent.ProductName != '');
        }       
    }  
    
    static testMethod void myUnitTest2() 
    {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 

        Id recordTypeProdId = Schema.SObjectType.Proposal__c.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
        
        User userTest = CFCTestSetup.getUser(); 
                
        System.runAs(userTest) 
        { 

            Product2 Produto = new Product2();
            Produto.Name = 'Produto Teste';
            Produto.ProductCode = 'a488';
            Produto.Product_Status__c = 'Active';
            Produto.RecordTypeId = recordTypeId;
            Produto.Publication_Status__c = 'Published';
            Produto.IsActive = true;
            database.insert(Produto);   
            
            Proposal__c proposal = new Proposal__c();
            proposal.Aircraft_Type__c = 'ERJ'; 
            proposal.Aircraft_Model__c = 'E170';
            proposal.RecordTypeId = recordTypeProdId;
            proposal.Opportunity_Create__c = FALSE; 
            proposal.ID__c = null;
            database.insert(proposal);   
            
            Proposal_Items__c proposalItems = new Proposal_Items__c();
            proposalItems.Proposal__c = proposal.Id;
            proposalItems.Product__c = Produto.Id;
            database.insert(proposalItems); 
            
            CFCGlobalSearchComponentController globalSearchComponent = New CFCGlobalSearchComponentController(); 
            globalSearchComponent.ProductName = '';
            globalSearchComponent.SearchProduct();
            
            system.assert(globalSearchComponent.ProductName == '');
        }       
    }   
    
}