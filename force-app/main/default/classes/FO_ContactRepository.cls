/**
* @description: This class represents the FlightOps contacts repository. 
* For more information about the pattern, visit:
* https://martinfowler.com/eaaCatalog/repository.html
**/
public class FO_ContactRepository {
    public static Contact getContactById(Id contactId){
        Contact contact = [
            SELECT
            	Id,
            	Name,
            	Email,
            	FlightOps_FUP_Report__c,
            	FlightOps_ERJ_WW_FUP_Report__c,
            	FlightOps_E2_FUP_Report__c,
            	Account.Name
            FROM
            	Contact
            WHERE Id =: contactId
        ];
        return contact;
    }
    
    /**
    * @description Returns all contacts who wants to receive an Fup Report.
    **/
    public static List<Contact> getContactsReceivingFupReport(){
        List<Contact> contacts = [
            SELECT
            	Id,
            	Name,
            	Email,
            	FlightOps_FUP_Report__c,
            	FlightOps_ERJ_WW_FUP_Report__c,
            	FlightOps_E2_FUP_Report__c,
            	Account.Name
            FROM
            	Contact
            WHERE
            	Contact_Status__c = 'Active'
            	AND Account.Company_Status__c IN ('EIS Process', 'Active')
            	AND (
                    FlightOps_FUP_Report__c = TRUE
                    OR FlightOps_ERJ_WW_FUP_Report__c = TRUE
                    OR FlightOps_E2_FUP_Report__c = TRUE
                )
            ORDER BY
            	Account.Name ASC,
            	NAME ASC
        ];
        return contacts;
    }
}