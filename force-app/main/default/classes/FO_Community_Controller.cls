public class FO_Community_Controller {

    
    public Account cia {get;set;}
    public Contact contato {get;set;}
    public String caseRadio {get;set;}
    public list<case> CasosAbertos {get;set;}
    public list<case> CasosFechados {get;set;}
    
    //Construtor
    public FO_Community_Controller(){
        
        contato = [SELECT AccountId, Name, Id FROM Contact WHERE Email =: UserInfo.getUserEmail()];
        if(contato!=null) cia = [ SELECT Name FROM Account WHERE Id=:contato.AccountId ];
        
        CaseRadio = '1';		//All Cases
        rodaBusca(CaseRadio);
    }
    
    public void rodaBusca(string CaseRadio){
        if(caseRadio == '0'){
        	BuscaMeus();
        }else if(CaseRadio == '1'){
        	 BuscaTodos();
        }
    }
        
    public void BuscaMeus() {
        CasosAbertos =
          [ SELECT CaseNumber, Subject, FlightOps_LastPublicComment__c, CreatedDate, Status, Description
            FROM Case
            WHERE AccountId =: contato.AccountId AND (Status='New' OR Status='In Progress') AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c = true
            ORDER BY CaseNumber
            LIMIT 50 ];
        CasosFechados =
          [ SELECT CaseNumber, Subject, FlightOps_LastPublicComment__c, CreatedDate, Status, Description
            FROM Case
            WHERE AccountId =: contato.AccountId AND Status = 'Closed' AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c = true
            ORDER BY CaseNumber
            LIMIT 50 ];
	}
    public void BuscaTodos() {
        CasosAbertos =
          [ SELECT CaseNumber, Subject, FlightOps_LastPublicComment__c, CreatedDate, Status, Description
            FROM Case
            WHERE AccountId =: contato.AccountId AND (Status='New' OR Status='In Progress') AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c = true
            ORDER BY CaseNumber
            LIMIT 50 ];
        CasosFechados =
          [ SELECT CaseNumber, Subject, FlightOps_LastPublicComment__c, CreatedDate, Status, Description
            FROM Case
            WHERE AccountId =: contato.AccountId AND Status = 'Closed' AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c = true
            ORDER BY CaseNumber
            LIMIT 50 ];
	}
    
    
}