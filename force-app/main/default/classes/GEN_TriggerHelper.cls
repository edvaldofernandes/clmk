/**
* @author Marcilio Leite de Souza
* @date 20/08/2018
* @description: Trigger Helper DML methods
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           20 AGO 2018             Original Version
**/
public without sharing class GEN_TriggerHelper {
   
   @TestVisible private static Boolean triggerEnabled = true;
    
    /**
    * @description : Enable apex default trigger
	* @return void
    **/
    public static void enableTrigger() {
        triggerEnabled = true;
    }

    /**
    * @description : Disable apex default trigger
	* @return void
    **/
    public static void disableTrigger() {
        triggerEnabled = false;
    }

    /**
    * @description : Check if trigger is enable
	* @return Boolean
    **/
    public static Boolean isTriggerEnabled() {
        return triggerEnabled;
    }
    
    /**
    * @description : Insert SObject apex default method with trigger disable
    * @param SObject obj : SObject to be insert
	* @return void
    **/
    public static void insertObjectTriggerDisabled(SObject obj) {
        GEN_TriggerHelper.DisableTrigger();
        Database.insert(obj);
        GEN_TriggerHelper.EnableTrigger();        
    }
    
    /**
    * @description : Insert list SObject apex default method with trigger disable
    * @param List<SObject> obj : List SObject to be insert
	* @return void
    **/
    public static void insertObjectListTriggerDisabled(List<SObject> obj) {
        GEN_TriggerHelper.DisableTrigger();
        Database.insert(obj);
        GEN_TriggerHelper.EnableTrigger();        
    }
    
    /**
    * @description : Update SObject apex default method with trigger disabled
    * @param SObject obj : SObject to be update
	* @return void
    **/
    public static void updateObjectTriggerDisabled(SObject obj) {
        GEN_TriggerHelper.DisableTrigger();
        Database.update(obj);
        GEN_TriggerHelper.EnableTrigger();
    }
    
    /**
    * @description : Update list SObject apex default method with trigger disabled
    * @param List<SObject> obj : List SObject to be update
	* @return void
    **/
    public static void updateObjectListTriggerDisabled(List<SObject> obj) {
        GEN_TriggerHelper.DisableTrigger();
        Database.update(obj,false);
        GEN_TriggerHelper.EnableTrigger();
    }
    
    /**
    * @description : Delete SObject apex default method with trigger disabled
    * @param SObject obj : SObject to be delete
	* @return void
    **/
    public static void deleteObjectTriggerDisabled(SObject obj) {
        GEN_TriggerHelper.DisableTrigger();
        Database.delete(obj);
        GEN_TriggerHelper.EnableTrigger();
    }
    
    /**
    * @description : Delete list SObject apex default method with trigger disabled
    * @param List<SObject> obj : List SObject to be delete
	* @return void
    **/
    public static void deleteObjectListTriggerDisabled(List<SObject> obj) {
        GEN_TriggerHelper.DisableTrigger();
        Database.delete(obj);
        GEN_TriggerHelper.EnableTrigger();
    }
    
    /**
    * @description : Upsert SObject apex default method with trigger disabled
    * @param SObject obj : SObject to be upsert
    * @param Schema.SObjectField field : Field reference to upsert
	* @return void
    **/
    public static void upsertObjectTriggerDisabled(SObject obj, Schema.SObjectField field) {
        GEN_TriggerHelper.DisableTrigger();
        Database.upsert(obj, field);
        GEN_TriggerHelper.EnableTrigger();
    }
    
    /**
    * @description : Upsert list SObject apex default method with trigger disabled
    * @param List<SObject> obj : List SObject to be upsert
    * @param Schema.SObjectField field : Field reference to upsert
	* @return void
    **/
    public static void upsertObjectListTriggerDisabled(List<SObject> obj, Schema.SObjectField field) {
        GEN_TriggerHelper.DisableTrigger();
        Database.upsert(obj, field);
        GEN_TriggerHelper.EnableTrigger();
    }
}