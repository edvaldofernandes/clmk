/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class SCMUpdateQuantityContractLineItem
*
* NAME: SCMUpdateQuantityContractLineItemTest.cls
* AUTHOR: KHPS                                                DATE: 30/12/2014
*
*******************************************************************************/

@isTest
private class SCMUpdateQuantityContractLineItemTest {
    
    private static final id recTypeAcc = RecordTypeMemory.getRecType( 'Account', 'Operator' );
    private static final id recTypeSvc = RecordTypeMemory.getRecType( 'ServiceContract', 'Aircraft_Modification' );
    private static final id recTypeScm = RecordTypeMemory.getRecType( 'Service_Contract_Management__c', 'NREC' );   

    static testMethod void testeUpdate() {
        
        Id stdPbk = SObjectInstanceTest.catalogoDePrecoPadrao();
    
        Product2 prod = SObjectInstanceTest.createProduct2();
        Database.insert(prod);

        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPbk, prod.Id);
        Database.insert(pbe);       
        
        Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
      Database.insert(acc);
      
      ServiceContract svc = SObjectInstanceTest.createServiceContract(acc.Id, recTypeSvc);
      svc.Pricebook2Id = stdPbk;
      Database.insert(svc);
            
      ContractLineItem conli = SObjectInstanceTest.createContractLineItem(svc.Id, pbe.Id);
      conli.Quantity = 5.00;
      conli.Delivered__c = 0.00;
      Database.insert(conli);
      
      Service_Contract_Management__c scm = SObjectInstanceTest.createSvcContractMgmt(acc.Id, svc.Id, null, recTypeScm);
      scm.Contract_Line_Item_Deliverable__c = conli.Id;
      
    
      scm.Delivered__c = true;
      scm.Delivered_Date__c = system.today();
      Test.startTest();
      scm.Quantity__c = 1;
      Database.insert(scm);
      Test.stopTest();
      
      ContractLineItem conliResult = [SELECT Id, Delivered__c, Quantity FROM ContractLineItem WHERE Id = :conli.Id];
        
    }
    
    static testMethod void testeDelete() {
        
        Id stdPbk = SObjectInstanceTest.catalogoDePrecoPadrao();
    
        Product2 prod = SObjectInstanceTest.createProduct2();
        Database.insert(prod);

        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPbk, prod.Id);
        Database.insert(pbe);       
        
        Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
      Database.insert(acc);
      
      ServiceContract svc = SObjectInstanceTest.createServiceContract(acc.Id, recTypeSvc);
      svc.Pricebook2Id = stdPbk;
      Database.insert(svc);
           
      ContractLineItem conli = SObjectInstanceTest.createContractLineItem(svc.Id, pbe.Id);
      conli.Quantity = 5.00;
      conli.Delivered__c = 0.00;
      Database.insert(conli);
      
      Service_Contract_Management__c scm = SObjectInstanceTest.createSvcContractMgmt(acc.Id, svc.Id, null, recTypeScm);
      scm.Contract_Line_Item_Deliverable__c = conli.Id;
      scm.Quantity__c = 1;
      Database.insert(scm);
      
      Test.startTest();
      Database.delete(scm);
      Test.stopTest();      
      
      ContractLineItem conliResult = [SELECT Id, Delivered__c, Quantity FROM ContractLineItem WHERE Id = :conli.Id];
      System.assertEquals(0, conliResult.Delivered__c);
        
    }    
    
}