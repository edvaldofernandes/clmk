public without sharing class ProposalTriggerHandler {
    
    public Map<Id,Proposal__c> newRecordsMap = new Map<Id,Proposal__c>();
    public Map<Id,Proposal__c> oldRecordsMap = new Map<Id,Proposal__c>(); 
    public List<Proposal__c> newRecords = new List<Proposal__c>();
    public List<Proposal__c> oldRecords = new List<Proposal__c>();
    
    public static boolean noRecursive = false;
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    public ProposalTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }
    
    public boolean IsTriggerContext
    {
        get{return isExecuting;}
    }
    
    public void OnBeforeInsert(){}

    public void OnAfterInsert(){}

    public void OnBeforeUpdate(){
        
    }

    public void OnAfterUpdate(){
        if(!noRecursive)
        {
            noRecursive = true;
            CreateCFCOpportunity();
        }
        
    }

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}
    
    private void CreateCFCOpportunity(){
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity');
        
        List<PricebookEntry> listPriceBookEntry;
        List<Opportunity_History__c> listOpportunityHistory = new List<Opportunity_History__c>();
        List<Proposal_Items__c> lstProposalItems = new List<Proposal_Items__c>();

        List<Attachment> listAttachments = new List<Attachment>();        
        
        Map<String, List<Proposal_Items__c>> mapProposalItems = new Map<String, List<Proposal_Items__c>>();
        Map<String, PricebookEntry> mapPriceBookEntry = new Map<String, PricebookEntry>();
        Map<String, Proposal__c> mapProposal = new Map<String, Proposal__c>();
        
        //get pricebook
        Boolean isValidPriceBookEntry = false;
        Pricebook2 pricebook = PricebookDao.getInstance().getByName('Standard Price Book');
        system.debug('CFCRequestProposalController.SaveProposal.pricebook >>> ' + pricebook);
        if(pricebook != null){
            isValidPriceBookEntry = true;
            
            //create list of items
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.newRecordsMap.keySet() >>> ' + newRecordsMap.keySet());
            lstProposalItems = ProposalItemsDao.getInstance().listBySetIdProposal(newRecordsMap.keySet());
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.lstProposalItems >>> ' + lstProposalItems);
            
            //create set of products
            Set<String> setIdProduct = new Set<String>();
            for(Proposal_Items__c item : lstProposalItems)
            {
                setIdProduct.add(item.Product__c);
                
                if(mapProposalItems.containsKey(item.Proposal__c))
                {
                    mapProposalItems.get(item.Proposal__c).add(item);
                } 
                else 
                {
                    List<Proposal_Items__c> lst = new List<Proposal_Items__c>();
                    lst.add(item);
                	mapProposalItems.put(item.Proposal__c, lst);
                }               
            }
            
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.setIdProduct >>> ' + setIdProduct);
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposalItems >>> ' + mapProposalItems);
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposalItems.size() >>> ' + mapProposalItems.size());
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposalItems.values() >>> ' + mapProposalItems.values());
            
            listPriceBookEntry = PricebookEntryDao.getInstance().listByPricebookIdAndSetProductId(pricebook.id, setIdProduct);
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listPriceBookEntry >>> ' + listPriceBookEntry);
            
            for(PricebookEntry entry : listPriceBookEntry){
                mapPriceBookEntry.put(entry.Product2Id, entry);
            }
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapPriceBookEntry >>> ' + mapPriceBookEntry);
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapPriceBookEntry.size() >>> ' + mapPriceBookEntry.size());
        }
        
        //create a set of proposalId
        Set<String> setIdCProposal = new Set<String>();
        Set<String> setIdProposal = new Set<String>();
        for(Proposal__c prop : (List<Proposal__c>) trigger.new){
            setIdCProposal.add(prop.ID__c);
            setIdProposal.add(prop.id);
            mapProposal.put(prop.ID__c, prop);
        }
		system.debug('ProposalTriggerHandler.CreateCFCOpportunity.setIdCProposal >>> ' + setIdProposal);        
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.setIdCProposal >>> ' + setIdCProposal);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposal >>> ' + mapProposal);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposal.size() >>> ' + mapProposal.size());
        
        //get attachments by set of proposal id
        listAttachments = AttachmentDao.getInstance().listBySetParentId(setIdProposal);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listAttachments >>> ' + listAttachments);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listAttachments.size() >>> ' + listAttachments.size());
        
        //map the existents opportunities
        Map<String, Opportunity> mapExistentsOpportunities = new Map<String, Opportunity>();
		
		List<Opportunity> lstOpportunity = [SELECT ID__c, name FROM Opportunity WHERE ID__c IN :setIdCProposal];
        for(Opportunity opp : lstOpportunity){
            mapExistentsOpportunities.put(opp.ID__c, opp);
        }        
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapExistentsOpportunities >>> ' + mapExistentsOpportunities);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapExistentsOpportunities.size() >>> ' + mapExistentsOpportunities.size());
        
        List<Opportunity> lstOpportunityToUpdate = new List<Opportunity>();        
        List<Opportunity> lstOpportunityToInsert = new List<Opportunity>();
        
        for(Proposal__c prop : (List<Proposal__c>) trigger.new){
            system.debug('ProposalTriggerHandler.CreateCFCOpportunity.prop >>> ' + prop);
            
            if(!mapExistentsOpportunities.containsKey(prop.ID__c)){
                system.debug('new opportunity');
                Opportunity opp = new Opportunity();
                
                //generate fleet_type to opportunity by proposal products
                //String fleetType = 'EJET';
                String fleetType = '';
                if(mapProposalItems.containsKey(prop.id)){
                	fleetType = GenerateStringFleetType(mapProposalItems.get(prop.Id));    
                }            	
                
                
                opp.Contact__c = prop.Contact__c;
                opp.Email__c = prop.Email__c;
                opp.RFP_Date__c = Date.today();
                opp.Phone__c = prop.Phone__c;
                opp.Description = prop.Remarks__c;
                
                if(!String.isBlank(prop.Opportunity_Name__c)){
                    opp.Name = prop.Opportunity_Name__c;
                } else {
                    opp.Name = prop.Name.replace('PRP', 'CFC');
                }
                    
                
                
                opp.StageName = 'Qualification';
                opp.Probability__c = 'Low';
                opp.Source__c = 'CFC';
                opp.OwnerId = prop.OwnerId;
                opp.AccountId = prop.Account__c;  
                opp.Fleet_Type__c = fleettype;
                opp.CloseDate = Date.today().addMonths(1);                
                opp.ID__c = prop.ID__c;                
                
                /*if(listPriceBookEntry.size() != lstProposalItems.size()){
                    system.debug('pricebookentry is not valid');
                    opp.Product_selection_error__c = true;
                }*/
                
                system.debug('ProposalTriggerHandler.CreateCFCOpportunity.opp >>> ' + opp);                
                lstOpportunityToInsert.add(opp);
            } else {
				system.debug('existent opportunity');                
                Opportunity_History__c oppHistory = new Opportunity_History__c();
                oppHistory.Account__c = prop.Account__c;
                oppHistory.Contact_Name__c = prop.Contact__c;
                oppHistory.Opportunity__c = mapExistentsOpportunities.get(prop.ID__c).Id;
                //oppHistory.Name = mapExistentsOpportunities.get(prop.ID__c).Name;
                oppHistory.Name = prop.Name.replace('PRP', 'CFC');
                oppHistory.Email__c = prop.Contact__r.email;
                oppHistory.Create_Date__c = Date.today();
                oppHistory.Remarks__c = prop.Remarks__c;
                system.debug('ProposalTriggerHandler.CreateCFCOpportunity.oppHistory >>> ' + oppHistory);
                listOpportunityHistory.add(oppHistory);
            }            
        }

              
        update lstOpportunityToUpdate;
		insert lstOpportunityToInsert;        
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.lstOpportunityToUpdate >>> ' + lstOpportunityToUpdate);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.lstOpportunityToInsert >>> ' + lstOpportunityToInsert);          
        
        insert listOpportunityHistory;        
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listOpportunityHistory >>> ' + listOpportunityHistory);
        
        //------------------------------------INSERT OPPORTUNITY LINE ITEM--------------------------------------------//
        List<OpportunityLineItem> listLineitemToInsert = new List<OpportunityLineItem>();
        
        
        for(Opportunity opp : lstOpportunityToInsert){
            if(mapProposal.containsKey(opp.ID__c)){
                Proposal__c prop = mapProposal.get(opp.ID__c);
                system.debug('ProposalTriggerHandler.CreateCFCOpportunity.prop >>> ' + prop);
                if(mapProposalItems.containsKey(prop.Id)){
                    List<Proposal_Items__c> lst = mapProposalItems.get(prop.Id);
                    system.debug('ProposalTriggerHandler.CreateCFCOpportunity.lst >>> ' + lst);
                    for(Proposal_Items__c item : lst){
                        if(mapPriceBookEntry.containsKey(item.Product__c)){
                            OpportunityLineItem line = new OpportunityLineItem();
                            line.OpportunityId = opp.Id;
                            line.PricebookEntryId = mapPriceBookEntry.get(item.Product__c).id;
                            line.Quantity = 1;
                            line.TotalPrice = 0;
                            listLineitemToInsert.add(line);
                        }
                    }                    
                }
            } 
        }
        
        insert listLineitemToInsert;
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listLineitemToInsert >>> ' + listLineitemToInsert);
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listLineitemToInsert.size() >>> ' + listLineitemToInsert.size());
		
        //------------------------------------UPDATE PROPOSAL REFERENCE TO NEW OPPORTUNITY--------------------------------------------//
        List<Proposal__c> listProposalToUpdate = new List<Proposal__c>();
        Map<String, String> mapProposalToOpportunity = new Map<String, String>();
        
        for(Opportunity opp : lstOpportunityToInsert){
            if( mapProposal.containsKey(opp.ID__c)){
                Proposal__c newProp = new Proposal__c();
                newProp.Id = mapProposal.get(opp.ID__c).id;
                newProp.Opportunity__c = opp.Id;
                
                if(listPriceBookEntry.size() != lstProposalItems.size()){
                    system.debug('pricebookentry is not valid');
                    newProp.Product_selection_error__c = true;
                }
                
                listProposalToUpdate.add(newProp);
                mapProposalToOpportunity.put(mapProposal.get(opp.ID__c).id,  opp.Id);
            }            
        }
        for(Opportunity opp : mapExistentsOpportunities.values()){
            if( mapProposal.containsKey(opp.ID__c)){
                Proposal__c newProp = new Proposal__c();
                newProp.Id = mapProposal.get(opp.ID__c).id;
                newProp.Opportunity__c = opp.Id;
                
                if(listPriceBookEntry.size() != lstProposalItems.size()){
                    system.debug('pricebookentry is not valid');
                    newProp.Product_selection_error__c = true;
                }
                
                listProposalToUpdate.add(newProp);
                mapProposalToOpportunity.put(mapProposal.get(opp.ID__c).id,  opp.Id);
            }            
        }
        update listProposalToUpdate;
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listProposalToUpdate >>> ' + listProposalToUpdate);
        
     	//--------------------------------------------UPDATE PROPOSAL ATTACHMENTS TO OPPORTUNITY ATTACHMENTS--------------------------------------------//
     	//system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposalToOpportunity >>> ' + mapProposalToOpportunity);
        //system.debug('ProposalTriggerHandler.CreateCFCOpportunity.mapProposalToOpportunity.size() >>> ' + mapProposalToOpportunity.size());
        
        List<Attachment> listAttachmentToInsert = new List<Attachment>();
        for(Proposal__c prop : listProposalToUpdate){
            for(Attachment att : listAttachments){
                if(att.ParentId == prop.Id){
                    Attachment newAtt = new Attachment();
                    newAtt.ParentId = prop.Opportunity__c;
                    newAtt.Name = att.Name;
                    newAtt.Body = att.Body;
                    listAttachmentToInsert.add(newAtt);
                }
            }
        }
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listAttachmentToInsert >>> ' + listAttachmentToInsert);
        insert listAttachmentToInsert;
        delete listAttachments;
        
        
        //--------------------------------------------UPDATE INPUT FORM TO OPPORTUNITY --------------------------------------------//
        List<InputForm__c> listInputFormToUpdate = new List<InputForm__c>();
        List<InputForm__c> listInputFormToProposal = new List<InputForm__c>();
        listInputFormToProposal = InputFormDao.getInstance().listBySetProposal(newRecordsMap.keySet());
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listInputFormToProposal >>> ' + listInputFormToProposal);
        
        for(Opportunity opp : lstOpportunityToInsert){
            if(mapProposal.containsKey(opp.ID__c)){
                for(InputForm__c inputFormOld : listInputFormToProposal){
                    if(inputFormOld.Proposal__c == mapProposal.get(opp.ID__c).id){
                        inputFormOld.Opportunity__c = opp.Id;
                        listInputFormToUpdate.add(inputFormOld);
                    }
                }
            }
        }
        
        for(Opportunity opp : mapExistentsOpportunities.values()){
            if(mapProposal.containsKey(opp.ID__c)){
                for(InputForm__c inputFormOld : listInputFormToProposal){
                    if(inputFormOld.Proposal__c == mapProposal.get(opp.ID__c).id){
                        inputFormOld.Opportunity__c = opp.Id;
                        listInputFormToUpdate.add(inputFormOld);
                    }
                }
            }
        }
        
        system.debug('ProposalTriggerHandler.CreateCFCOpportunity.listInputFormToUpdate >>> ' + listInputFormToUpdate);
        update listInputFormToUpdate;
    }    
    
    private String GenerateStringFleetType(List<Proposal_Items__c> lstItemsProposal){
        system.debug('ProposalTriggerHandler.GenerateStringFleetType.lstItemsProposal >>> ' + lstItemsProposal);        
        string fleetType = '';        
        for(Proposal_Items__c item : lstItemsProposal){
            if(item.Product__r.Applicability__c != null && item.Product__r.Applicability__c != ''){
                if(fleetType == ''){
                    fleetType+= item.Product__r.Applicability__c;
                } else {
                    fleetType+= ';'+item.Product__r.Applicability__c;
                }
            }
        }
        System.debug('ProposalTriggerHandler.GenerateStringFleetType.fleetType >>> ' + fleetType);        
        return fleetType;
    }

}