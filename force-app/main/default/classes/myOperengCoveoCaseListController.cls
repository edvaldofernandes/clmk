public class myOperengCoveoCaseListController {
    public final static Id FLIGHT_OPS_RECORD_TYPE = Schema.SObjectType.Case.getRecordTypeInfosByName(
    ).get('FlightOps Case Record Type').getRecordTypeId();
    
    public final static String COVEO_FILTER_TEMPLATE = 
          '	@objecttype=Case'
        + ' AND @SfAccountId={0}'
        + ' AND  @SfRecordTypeId="'+ FLIGHT_OPS_RECORD_TYPE +'"';   
    
    /**
    * @description This method build the filter used in Coveo Search.
    *     It selects the "Case" by the account and its Record type.
    **/
    @AuraEnabled
    public static string getToken() {
        Map<String, Object> searchToken =  buildSearchToken();
             
        // Generate a token using the Globals class provided by Coveo.
        // See the Globals Class documentation: https://docs.coveo.com/en/1075/coveo-for-salesforce/globals-class
        return CoveoV2.Globals.generateSearchToken(searchToken);

    }
    
    public static Map<String, Object> buildSearchToken(){
        Id accountId = getCurrentUserAccountId();
        String query = String.format(
            COVEO_FILTER_TEMPLATE,
            new List<String>{
                accountId
            }
        );
        return new Map<String, Object> {
            'filter' => query
        };
    }   
    
    public static Id getCurrentUserAccountId(){
        return [
            SELECT contact.account.Id
            FROM
            	User
            WHERE
            	Id =: UserInfo.getUserId()
            LIMIT 1
        ].contact.account.Id;
    } 
}