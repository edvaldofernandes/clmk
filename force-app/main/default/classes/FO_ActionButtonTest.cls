/**
* @description: FO_ActionButton test class.
**/
@isTest
public class FO_ActionButtonTest {
    public class ConcreteActionButton extends FO_ActionButton{
        
        public ConcreteActionButton(ApexPages.StandardController stdController) {
            super(stdController);
        }
        public override void doAction(){
            System.debug('do nothing!');
        }
    }
    /**
    * @description Test if getPageReferenceUrl() returns the relative URL to the record page.
    **/
    @isTest static void testIsGettingCorrectRedirectUrl(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        
		ApexPages.currentPage().getParameters().put('id',eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
        
	    ConcreteActionButton controller  = new ConcreteActionButton(stdController);
        System.assertEquals(controller.getPageReferenceUrl(), '/' + eod.Id);
    }
    
    /**
    * @description Test if the PageReference object has the correct redirect URL.
    **/
    @isTest static void testIsGettingCorrectRedirectPage(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        
		ApexPages.currentPage().getParameters().put('id',eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
        
	    ConcreteActionButton controller  = new ConcreteActionButton(stdController);
        PageReference recordPage = controller.redirect();
        
        System.assertEquals(recordPage.getUrl(), controller.getPageReferenceUrl());
    }
    
    
    
}