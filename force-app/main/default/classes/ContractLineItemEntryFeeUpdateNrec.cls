/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* When Contract Line Item Entry Fee is deleted, the field Entry Fee in 
* Contract Line Item NREC must be zero.
*
* NAME: ContractLineItemEntryFeeUpdateNrec.cls
* AUTHOR: AFC                                                  DATE: 19/03/2015
*******************************************************************************/
public with sharing class ContractLineItemEntryFeeUpdateNrec 
{
	public static void clearEntryFee()
	{
		TriggerUtils.assertTrigger();
		
		set< id > setPbe = new set< id >();
		set< id > setSContract = new set< id >();
		
		for( ContractLineItem lineItemDeleted : ( list< ContractLineItem > )trigger.old  )
    {			
		  if( lineItemDeleted.Princing__c == 'Entry fee' )
		  {
		  	setPbe.add( lineItemDeleted.PricebookEntryId );
		  	setSContract.add( lineItemDeleted.ServiceContractId );	        
		  }
		}		
		if( setSContract.isEmpty() ) return;
				
		list< ContractLineItem > lstLineItemNrec = new list< ContractLineItem >( [  
		  SELECT Id, Princing__c, PricebookEntryId, ServiceContractId, Entry_fee__c
	    FROM ContractLineItem WHERE Princing__c =: 'NREC'
	    AND PricebookEntryId =: setPbe 
	    AND ServiceContractId =: setSContract ] );
	    
	  if ( lstLineItemNrec.isEmpty() ) return;
		
		for( ContractLineItem lineItemNrec : lstLineItemNrec )
    {       
	    lineItemNrec.Entry_fee__c = 0;
    }
    update lstLineItemNrec;
	}
}