@isTest
public class CFCRequestProposalControllerTest 
{  
    
    
    static testMethod void myUnitTest()   
    {
                
       Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
       Id recordTypeMediaCenterId = Schema.SObjectType.Attachments__c.getRecordTypeInfosByName().get('Images to Media Center').getRecordTypeId(); 
       Id recordTypeProdId = Schema.SObjectType.Proposal__c.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
 
        User userTest = CFCTestSetup.getUser(); 
        
        System.runAs(userTest) 
        { 
            ProposalTriggerHandler.noRecursive = true;  
            List<Proposal_Items__c> lstItemsProposal = New List<Proposal_Items__c>();                   
                   
            Product2 Produto = new Product2();
            Produto.Name = 'Produto Teste';
            Produto.ProductCode = 'a488';
            Produto.Product_Status__c = 'Active';
            Produto.RecordTypeId = recordTypeId;
            Produto.Publication_Status__c = 'Published';
            Produto.IsActive = true;
            Produto.Applicability__c = 'EJET';
            Produto.Commercial_Description__c = 'test';
            Produto.Family = 'Training';
            database.insert(Produto);   

            CFC_Configurations__c config = new CFC_Configurations__c();
            config.User_Name__c  = 'anmelo@embraer.com.br.deloitte';
            config.Desktop_Limit_KB__c = 40;
            config.Tablet_Limits_KB__c = 35;
            config.Mobile_Limits_KB__c = 20; 
            config.Name = 'Test CS';
            
            config.Billboard_max_published_number__c = 5;
            config.URL_Communities__c = 'wwww.teste.com.br';
            config.Billboard_Time__c = 5;
            config.Email_to_send_error_job__c = 'teste@teste.com.br';
            
            Database.insert(config);
            
            System.debug('>>> userTest.ContactId'+userTest.Contact.Account.Id);
            System.debug('>>> userTest.ACCOUNTID'+[select id, name, AccountId, Account.Name FROM Contact WHERE id = :userTest.ContactId]);
            
            Contact contactTest = [select Id, name, AccountId, Account.Name FROM Contact WHERE id = :userTest.ContactId];
            Account accountTest = [select id, name FROM Account WHERE id = :contactTest.AccountId];
            system.debug('ACCOUNTTEST>>> ' +accountTest);
            Proposal__c proposal = new Proposal__c();
            proposal.Email__c = 'anmelo@deloitte.com';  
            proposal.Contact__c = contactTest.Id;
            proposal.Contact__r = contactTest;
            proposal.Account__c = contactTest.AccountId;
            proposal.Aircraft_Type__c = 'ERJ'; 
            proposal.Aircraft_Model__c = 'E170';
            proposal.RecordTypeId = recordTypeProdId;
            proposal.Opportunity_Create__c = FALSE; 
            proposal.ID__c = null;
            proposal.Remarks__c = 'testee';
                    
            database.insert(proposal);
            
            system.debug('ACCOUNT C VALUE>>>' +proposal.Account__c);
            system.debug('TESTESTESTESTE ID>>>' +proposal.Aircraft_Type__c);
            system.debug('PROPOSAL ID>>>' +proposal);
            system.debug('USER NAME' +userTest.Contact.Account.Name);
           
            Proposal_Items__c proposalItems = new Proposal_Items__c();
            proposalItems.Proposal__c = proposal.Id;
            proposalItems.Product__c = Produto.Id;
            database.insert(proposalItems); 
            
            system.debug('PROPOSAL ITEMS>>>' +proposalItems);

            lstItemsProposal.add(proposalItems);
            
            system.debug('PROPOSAL LIST>>>' +lstItemsProposal);            
            
            Blob b = Blob.valueOf('Test Data'); 
            Attachment attachment = new Attachment();  
            attachment.ParentId = proposal.Id;  
            attachment.Name = 'Test Attachment for Parent';  
            attachment.Body = b;        
            database.insert(attachment); 
            system.debug('CFCProductDetailsControllerTest.myUnitTest.attachment >>> ' + attachment);

            CFCRequestProposalController requestProposal = New CFCRequestProposalController();  

            requestProposal.showForm = true;
            requestProposal.itemsProposal = lstItemsProposal;
            requestProposal.deleteItemId = '0';
            requestProposal.proposalCreatedDate = '';
            requestProposal.aircraft = 'ERJ'; 
            requestProposal.proposalType = 'ERJ'; 
            requestProposal.productionLine = 'Produto Teste';
            requestProposal.serialNumber = 'a488';
            requestProposal.descriptionRequest = 'test';
            requestProposal.reasonRequest = 'test';
            requestProposal.isAircraftModification = true;
            requestProposal.GenerateThumbnails(lstItemsProposal);
            requestProposal.DeleteItem();
            requestProposal.GenerateOwnerId();
            requestProposal.getAircraftItems();
            requestProposal.getProductionLineItems();
            requestProposal.GenerateStringFleetType(lstItemsProposal);
            requestProposal.getProposalTypeItems();
            requestProposal.insertAttachment();
            requestProposal.removeAtt();
            requestProposal.GenerateIDProposal(lstItemsProposal);
            requestProposal.SaveProposal();
            
            system.assert(proposal.Id != null);
        }       
    }
}