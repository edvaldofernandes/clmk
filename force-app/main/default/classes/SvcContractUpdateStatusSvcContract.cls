/*******************************************************************************
*                              Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class responsible for updating the field status of the parent service contract 
* to 'Amended' when the status of the child service contract is updated to 'Signed'.
*
* NAME: SvcContractUpdateStatusSvcContract.cls
* AUTHOR: KHPS                                                 DATE: 02/01/2015
*******************************************************************************/

public with sharing class SvcContractUpdateStatusSvcContract {
	
	/**
	public static void execute() {
		
		TriggerUtils.assertTrigger();
		
		set<Id> lstSvcParentId = new set<Id>();
		
		for(ServiceContract svc : (list<ServiceContract>)trigger.new) {
			if(TriggerUtils.wasChangedTo(svc, ServiceContract.Contract_Status__c, 'Signed') && svc.Contract_hierarchy__c != null) {
				lstSvcParentId.add(svc.Contract_hierarchy__c);
			}
		}
		
		if(lstSvcParentId.isEmpty()) return;
		
		list<ServiceContract> lstUpdateSvcContract = [SELECT Id, Contract_Status__c FROM ServiceContract WHERE Id  = :lstSvcParentId FOR UPDATE];
		
		if(lstUpdateSvcContract.isEmpty()) return;
		
		for(ServiceContract svc : lstUpdateSvcContract) {
			svc.Contract_Status__c = 'Amended';
			svc.Signature_Date__c = null;
		}
		
		if(!lstUpdateSvcContract.isEmpty()) update lstUpdateSvcContract;
		
	}
**/
}