//Classe aproveitada
public without sharing class FO_MergeCaseController {    
      
    private static final integer LIMIT_CASES = 15;
    RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'FlightOps Case Record Type' ];
                
    public class wrapperCase {
        
        public Case caso {get;set;}
        public boolean atribuir {get;set;}
        private list< EmailMessage > email{get;set;}
        private list< Attachment > anexo{get;set;}
                
    }
    
    public list<wrapperCase> lstCase {get;set;}
    //private list<Case> lstSearchCase;
    private map<id,Case> lMapCase;
    public String localizarid {get;set;}
    public String caseId {get;set;}
    public String msg {get;set;}
    @TestVisible private Case casoController;
    private ApexPages.StandardsetController lController;
     
    public FO_MergeCaseController(ApexPages.StandardsetController controller)
    {
        lController = controller;
        casoController = (Case) controller.getRecord();
        String lIdPage = ApexPages.currentPage().getParameters().get('Refid');

        if(lIdPage != null)
        {   
            list<Case> lstIdpage = [SELECT CaseNumber,Subject FROM Case WHERE id =:lIdPage];
            if ( !lstIdpage.isEmpty())
            {
                caseId = lstIdpage.get(0).CaseNumber;
                localizarid = lstIdpage.get(0).Subject;
            }
        }    
        InternalLocalizarCase( false, controller.getSelected() );
  
    }    
    
    private void carregar( boolean aFlag )
    {   
        map <id, list< EmailMessage > > mapEmail = new map <id, list< EmailMessage> >();
      
        for(EmailMessage email: [SELECT id,Subject, BccAddress, MessageDate, ParentId, TextBody, 
                                 Status, CcAddress,  ToAddress ,FromAddress, Incoming FROM 
                                 EmailMessage WHERE ParentId =: lMapCase.keyset()]){
       
            list< EmailMessage > lListEmail = mapEmail.get( email.ParentId );
            if ( lListEmail == null )
            {
                lListEmail = new list< EmailMessage >();
                mapEmail.put( email.ParentId, lListEmail );
            }
            lListEmail.add(email);
        }
        
        lstCase = new list<wrapperCase>(); 
    
        for(id lcasoId: mapEmail.keySet())
        {
     
            wrapperCase lwp = new  wrapperCase();
            lwp.caso = lMapCase.get(lcasoId);  
            lwp.atribuir = aFlag;
            lwp.email = mapEmail.get(lcasoId);
            lstCase.add(lwp);   
        }
    
        if ( lstCase.isEmpty() )
        {
            adicionaErro('Selected cases do not meet the criteria for merge.');
        }
    
    }
      
    public PageReference Cancel(){
        PageReference returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
    	return returnPage;
    }
    
    public PageReference LocalizarCase()
    {
        return InternalLocalizarCase( false, null );
    }
  
    private PageReference InternalLocalizarCase( boolean aFlag, list< SObject > aListSelected )
    {
              
    if ( localizarid != null && localizarid != '' )
    {
        lMapCase = new Map<id,Case>([SELECT id,CaseNumber, Subject, Contact.name, Account.name, Status, Contact.Email FROM Case WHERE RecordTypeId =: rt.Id AND Subject like: '%' + localizarid + '%' limit :LIMIT_CASES]);
    }else{
     
        if ( aListSelected == null || aListSelected.isEmpty() ){
            lMapCase = new Map<id,Case>([SELECT id,CaseNumber, Subject, Contact.name, Account.name, Status, Contact.Email FROM Case WHERE RecordTypeId =: rt.Id limit :LIMIT_CASES ]);
        }else{
            lMapCase = new Map<id,Case>([SELECT id,CaseNumber, Subject, Contact.name, Account.name, Status, Contact.Email FROM Case WHERE RecordTypeId =: rt.Id  AND id =: aListSelected limit :LIMIT_CASES ]);
        }
     }
    carregar( aFlag );
    return null;
    }

    public PageReference save()
    {        
        caseId = casoController.ParentId;
    
        list<Case> lstSearchCase = [SELECT id FROM Case WHERE Id =: caseId];
  
        if( lstSearchCase == null || lstSearchCase.IsEmpty())
        {
            adicionaErro('There is no selected target case.');
            return null;
        }else{
     
            for ( Integer i = lstCase.size() - 1; i >= 0; i-- )
            {
                if( lstCase[i].caso.CaseNumber == caseId ){
                    lstCase.remove(i);
                    break;
                }
            }
        }
        
        
        //------------- last email received---------------------------
        Case LastDate = [SELECT Id,FlightOps_Last_Email_Received__c FROM Case WHERE Id=: caseId ];
        if(LastDate.FlightOps_Last_Email_Received__c!=null){	
            
            Datetime AuxDT = LastDate.FlightOps_Last_Email_Received__c;
                
        	for(wrapperCase lcasoW: lstCase )
    		{
      			if ( !lcasoW.atribuir ) continue;
            
            	Case Aux = [SELECT Id,FlightOps_Last_Email_Received__c FROM Case WHERE Id =: lcasoW.caso.Id];
            
            	if(Aux.FlightOps_Last_Email_Received__c!=null){
               
                    //pega a maior data
                	if (AuxDT < Aux.FlightOps_Last_Email_Received__c ) {
                    	AuxDT = Aux.FlightOps_Last_Email_Received__c;
                	} 
            	}
        	}
            LastDate.FlightOps_Last_Email_Received__c = AuxDT;
            update(LastDate);
            
        }else{
            
            //data qualquer criada apenas para comparar
            Datetime AuxDT = Datetime.newInstance(2000, 01, 01);
            
            for(wrapperCase lcasoW: lstCase )
    		{
      			if ( !lcasoW.atribuir ) continue;
            
            	Case Aux = [SELECT Id,FlightOps_Last_Email_Received__c FROM Case WHERE Id =: lcasoW.caso.Id];
            
            	if(Aux.FlightOps_Last_Email_Received__c!=null){
               
                    //pega a maior data
                	if (AuxDT < Aux.FlightOps_Last_Email_Received__c ) {
                    	AuxDT = Aux.FlightOps_Last_Email_Received__c;
                	} 
            	}
        	}
            LastDate.FlightOps_Last_Email_Received__c = AuxDT;
            update(LastDate);
            
        }        
        //----------- fim last email received-------------------------
        
        
        
    //----------------------- copia as tasks para novo case----------------
    list< id > lListTaskId = new list< id >();
    for(wrapperCase lcasoW: lstCase )
    {
      if ( !lcasoW.atribuir ) continue;
      List<Task> ttAux = [SELECT Id FROM Task WHERE WhatId=: lcasoW.caso.Id];
        if(ttAux!=null){
            for(Task taskId : ttAux)
        	lListTaskId.add(taskId.Id);
        }
    }
        
        for (Id t : lListTaskId){
            Task tAux = [SELECT Id,WhoId,Subject,ActivityDate,Status,Customer_Commitment_new__c,WhatId,X1st_date_indicator__c,Reschedule_Date__c,Completion_date__c,Comments__c,SendClosureNotification__c FROM Task WHERE Id =: t];
            tAux.WhatId = caseId;
            update tAux;
        }
   //----------------------------------------------------
   
   //selecionando attachments
   
    list< id > lListEmailId = new list< id >();
    for(wrapperCase lcasoW: lstCase )
    {
      if ( !lcasoW.atribuir ) continue;
      for ( EmailMessage lE : lcasoW.email )
      {
        lListEmailId.add(lE.Id);
      }
    }
    
    map <id, list< Attachment > > mapAnexo = new map <id, list< Attachment> >();      
    for(Attachment anexo: [SELECT id, ParentId, Body, BodyLength, ContentType, Description, Name, OwnerId FROM Attachment WHERE ParentId =: lListEmailId]){
       
      list< Attachment > lListAnexo = mapAnexo.get( anexo.ParentId );
      if ( lListAnexo == null )
      {
          lListAnexo = new list< Attachment >();
          mapAnexo.put( anexo.ParentId, lListAnexo );
      }
      lListAnexo.add(anexo);
    }
    
    for(wrapperCase lcasoW: lstCase )
    {
      lcasoW.anexo = new List<Attachment>();
      for ( EmailMessage lE : lcasoW.email )
      {
        list< Attachment > lLstE = mapAnexo.get( lE.id );
        if ( lLstE != null && !lLstE.isEmpty() )
        {          
          lcasoW.anexo.addAll( lLstE );
        }
      }      
    }
   
   list<Case> lstDelete = new list<Case>();
   
   // Mapa que relaciona o id antigo do EmailMessage com o novo objeto
   map<id, EmailMessage> lstEmail = new map<id, EmailMessage>();
   list<Attachment> lstAnexo = new list<Attachment>();
   
   Boolean existeSelecionado = false;  
   for(wrapperCase wp:  lstCase)
   {          
      if(wp.atribuir &&  wp.email != null )
      {
        existeSelecionado = true;
        for ( EmailMessage lOldEmail : wp.email )
        {
            EmailMessage email = lOldEmail.clone( false, true, true, true );
            email.ParentId = lstSearchCase.get(0).id;
            
            //como o email sera inserido, para que nao atualize FlightOps_Last_Email_Received__c do case, criei o campo FlightOps_flag__c no email como flag
            //ou seja, se o valor dessa flag é true ele atualiza FlightOps_Last_Email_Received__c no case e se for false, nao atualiza
            //assim, FlightOps_Last_Email_Received__c no case só altera quando o email realmente chega( valor padrao é true ) e nao quando da o merge ( alterado para false ) 
            email.FlightOps_Flag__c = false;
                        
            lstEmail.put(lOldEmail.id,email);
        }
          
        lstDelete.add(wp.caso);
      }     
   }   
   if ( !existeSelecionado )
   {
     adicionaErro('There is no added case.');
     return null;
   }

    if(!lstEmail.isEmpty())
    { 
      insert lstEmail.values();
      //update new Case ( Id = lstSearchCase.get(0).id, Incoming_email_i__c = true );
    }
    
    // Clonar os Anexos
    for(wrapperCase wp:  lstCase)
    {
      if ( wp.atribuir )
      {
        for ( Attachment lOldAnexo : wp.anexo )
        {
          Attachment anexo = lOldAnexo.clone( false, true, true, true );
          anexo.ParentId = lstEmail.get( lOldAnexo.ParentId ).id;
          lstAnexo.add(anexo);
        }
      }
    }
    
    if(!lstAnexo.isEmpty()) insert lstAnexo;
    
    if(!lstDelete.isEmpty() && !Test.isRunningTest()) delete lstDelete;
        
     for ( Integer i = lstCase.size() - 1; i >= 0; i-- )
     {
        if( lstCase[i].atribuir )
        {
          lstCase.remove(i);
        }
     }
    
    //lController.cancel();
    adicionaMensagem('The case(s) was(were) merged.');
    
    for(wrapperCase wp:  lstCase)
    { 
      wp.anexo = null;
    }
    
    return null;
  }

    private static void adicionaErro(String err)
    {
     ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.ERROR, err) );
  }

    private static void adicionaMensagem(String msg)
    {
     ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.CONFIRM, msg) );
  }
}