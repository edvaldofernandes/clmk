global class ProductWithPrice{
    public ID ProductId {get; set;}
    public string Name {get; set;}
    public Decimal UnitPrice {get; set;}
    public string ImageUrl {get; set;}
}