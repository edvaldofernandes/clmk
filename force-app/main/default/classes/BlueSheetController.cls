public with sharing class BlueSheetController
    {





            public Integer AMS_1 {get; set;}
            public Integer AMS_2 {get; set;}
            public Integer AMS_3 {get; set;}
            public Integer AMS_4 {get; set;}
            public Integer AMS_5 {get; set;}
            public Integer AMS_6 {get; set;}
            public Integer AMS_7 {get; set;}
            public Integer SIN_1 {get; set;}
            public Integer SIN_2 {get; set;}
            public Integer SIN_3 {get; set;}
            public Integer SIN_4 {get; set;}
            public Integer SIN_5 {get; set;}
            public Integer SIN_6 {get; set;}
            public Integer SIN_7 {get; set;}
            public Integer FLL_1 {get; set;}
            public Integer FLL_2 {get; set;}
            public Integer FLL_3 {get; set;}
            public Integer FLL_4 {get; set;}
            public Integer FLL_5 {get; set;}
            public Integer FLL_6 {get; set;}
            public Integer FLL_7 {get; set;}
            public Integer BJS_1 {get; set;}
            public Integer BJS_2 {get; set;}
            public Integer BJS_3 {get; set;}
            public Integer BJS_4 {get; set;}
            public Integer BJS_5 {get; set;}
            public Integer BJS_6 {get; set;}
            public Integer BJS_7 {get; set;}
            public Integer SJK_1 {get; set;}
            public Integer SJK_2 {get; set;}
            public Integer SJK_3 {get; set;}
            public Integer SJK_4 {get; set;}
            public Integer SJK_5 {get; set;}
            public Integer SJK_6 {get; set;}
            public Integer SJK_7 {get; set;}



    public BlueSheetController(ApexPages.StandardController stdController) {
            AMS_1 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Prospecting'];
            AMS_2 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Business Development'];
            AMS_3 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'LOI/Proposal Negotiation'];
            AMS_4 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Contract Negotiation'];
            AMS_5 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Closed Won'];
            AMS_6 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Lost/Dropped'];
            AMS_7 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Open'];

            SIN_1 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Prospecting'];
            SIN_2 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Business Development'];
            SIN_3 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'LOI/Proposal Negotiation'];
            SIN_4 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Contract Negotiation'];
            SIN_5 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Closed Won'];
            SIN_6 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Lost/Dropped'];
            SIN_7 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Open'];

            FLL_1 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Prospecting'];
            FLL_2 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Business Development'];
            FLL_3 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'LOI/Proposal Negotiation'];
            FLL_4 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Contract Negotiation'];
            FLL_5 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Closed Won'];
            FLL_6 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Lost/Dropped'];
            FLL_7 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Open'];

            BJS_1 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Prospecting'];
            BJS_2 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Business Development'];
            BJS_3 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'LOI/Proposal Negotiation'];
            BJS_4 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Contract Negotiation'];
            BJS_5 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Closed Won'];
            BJS_6 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Lost/Dropped'];
            BJS_7 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Open'];

            SJK_1 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Prospecting'];
            SJK_2 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Business Development'];
            SJK_3 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'LOI/Proposal Negotiation'];
            SJK_4 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Contract Negotiation'];
            SJK_5 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Closed Won'];
            SJK_6 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Lost/Dropped'];
            SJK_7 = [SELECT count() FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Open'];


    }




// AMS------------------------------------------------------------------
// ----------------------------------------------------------------------

List<BlueSheet__c> deals_AMS_1; public List<BlueSheet__c> getDeals_AMS_1()    { if(deals_AMS_1 == null) deals_AMS_1 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Prospecting']; return deals_AMS_1;}
List<BlueSheet__c> deals_AMS_2; public List<BlueSheet__c> getDeals_AMS_2()    { if(deals_AMS_2 == null) deals_AMS_2 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Business Development']; return deals_AMS_2;}
List<BlueSheet__c> deals_AMS_3; public List<BlueSheet__c> getDeals_AMS_3()    { if(deals_AMS_3 == null) deals_AMS_3 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'LOI/Proposal Negotiation']; return deals_AMS_3;}
List<BlueSheet__c> deals_AMS_4; public List<BlueSheet__c> getDeals_AMS_4()    { if(deals_AMS_4 == null) deals_AMS_4 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Contract Negotiation']; return deals_AMS_4;}
List<BlueSheet__c> deals_AMS_5; public List<BlueSheet__c> getDeals_AMS_5()    { if(deals_AMS_5 == null) deals_AMS_5 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Closed Won']; return deals_AMS_5;}
List<BlueSheet__c> deals_AMS_6; public List<BlueSheet__c> getDeals_AMS_6()    { if(deals_AMS_6 == null) deals_AMS_6 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Lost/Dropped']; return deals_AMS_6;}
List<BlueSheet__c> deals_AMS_7; public List<BlueSheet__c> getDeals_AMS_7()    { if(deals_AMS_7 == null) deals_AMS_7 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M EMEA' and  Business_Stage__c = 'Open']; return deals_AMS_7;}


// BJS------------------------------------------------------------------
// ----------------------------------------------------------------------

List<BlueSheet__c> deals_BJS_1; public List<BlueSheet__c> getDeals_BJS_1()    { if(deals_BJS_1 == null) deals_BJS_1 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Prospecting']; return deals_BJS_1;}
List<BlueSheet__c> deals_BJS_2; public List<BlueSheet__c> getDeals_BJS_2()    { if(deals_BJS_2 == null) deals_BJS_2 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Business Development']; return deals_BJS_2;}
List<BlueSheet__c> deals_BJS_3; public List<BlueSheet__c> getDeals_BJS_3()    { if(deals_BJS_3 == null) deals_BJS_3 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'LOI/Proposal Negotiation']; return deals_BJS_3;}
List<BlueSheet__c> deals_BJS_4; public List<BlueSheet__c> getDeals_BJS_4()    { if(deals_BJS_4 == null) deals_BJS_4 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Contract Negotiation']; return deals_BJS_4;}
List<BlueSheet__c> deals_BJS_5; public List<BlueSheet__c> getDeals_BJS_5()    { if(deals_BJS_5 == null) deals_BJS_5 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Closed Won']; return deals_BJS_5;}
List<BlueSheet__c> deals_BJS_6; public List<BlueSheet__c> getDeals_BJS_6()    { if(deals_BJS_6 == null) deals_BJS_6 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Lost/Dropped']; return deals_BJS_6;}
List<BlueSheet__c> deals_BJS_7; public List<BlueSheet__c> getDeals_BJS_7()    { if(deals_BJS_7 == null) deals_BJS_7 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M China' and  Business_Stage__c = 'Open']; return deals_BJS_7;}

// SIN------------------------------------------------------------------
// ----------------------------------------------------------------------

List<BlueSheet__c> deals_SIN_1; public List<BlueSheet__c> getDeals_SIN_1()    { if(deals_SIN_1 == null) deals_SIN_1 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Prospecting']; return deals_SIN_1;}
List<BlueSheet__c> deals_SIN_2; public List<BlueSheet__c> getDeals_SIN_2()    { if(deals_SIN_2 == null) deals_SIN_2 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Business Development']; return deals_SIN_2;}
List<BlueSheet__c> deals_SIN_3; public List<BlueSheet__c> getDeals_SIN_3()    { if(deals_SIN_3 == null) deals_SIN_3 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'LOI/Proposal Negotiation']; return deals_SIN_3;}
List<BlueSheet__c> deals_SIN_4; public List<BlueSheet__c> getDeals_SIN_4()    { if(deals_SIN_4 == null) deals_SIN_4 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Contract Negotiation']; return deals_SIN_4;}
List<BlueSheet__c> deals_SIN_5; public List<BlueSheet__c> getDeals_SIN_5()    { if(deals_SIN_5 == null) deals_SIN_5 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Closed Won']; return deals_SIN_5;}
List<BlueSheet__c> deals_SIN_6; public List<BlueSheet__c> getDeals_SIN_6()    { if(deals_SIN_6 == null) deals_SIN_6 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Lost/Dropped']; return deals_SIN_6;}
List<BlueSheet__c> deals_SIN_7; public List<BlueSheet__c> getDeals_SIN_7()    { if(deals_SIN_7 == null) deals_SIN_7 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M Asia Pacific' and  Business_Stage__c = 'Open']; return deals_SIN_7;}

// FLL------------------------------------------------------------------
// ----------------------------------------------------------------------

List<BlueSheet__c> deals_FLL_1; public List<BlueSheet__c> getDeals_FLL_1()    { if(deals_FLL_1 == null) deals_FLL_1 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Prospecting']; return deals_FLL_1;}
List<BlueSheet__c> deals_FLL_2; public List<BlueSheet__c> getDeals_FLL_2()    { if(deals_FLL_2 == null) deals_FLL_2 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Business Development']; return deals_FLL_2;}
List<BlueSheet__c> deals_FLL_3; public List<BlueSheet__c> getDeals_FLL_3()    { if(deals_FLL_3 == null) deals_FLL_3 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'LOI/Proposal Negotiation']; return deals_FLL_3;}
List<BlueSheet__c> deals_FLL_4; public List<BlueSheet__c> getDeals_FLL_4()    { if(deals_FLL_4 == null) deals_FLL_4 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Contract Negotiation']; return deals_FLL_4;}
List<BlueSheet__c> deals_FLL_5; public List<BlueSheet__c> getDeals_FLL_5()    { if(deals_FLL_5 == null) deals_FLL_5 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Closed Won']; return deals_FLL_5;}
List<BlueSheet__c> deals_FLL_6; public List<BlueSheet__c> getDeals_FLL_6()    { if(deals_FLL_6 == null) deals_FLL_6 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Lost/Dropped']; return deals_FLL_6;}
List<BlueSheet__c> deals_FLL_7; public List<BlueSheet__c> getDeals_FLL_7()    { if(deals_FLL_7 == null) deals_FLL_7 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M North America' and  Business_Stage__c = 'Open']; return deals_FLL_7;}

// SJK------------------------------------------------------------------
// ----------------------------------------------------------------------

List<BlueSheet__c> deals_SJK_1; public List<BlueSheet__c> getDeals_SJK_1()    { if(deals_SJK_1 == null) deals_SJK_1 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Prospecting']; return deals_SJK_1;}
List<BlueSheet__c> deals_SJK_2; public List<BlueSheet__c> getDeals_SJK_2()    { if(deals_SJK_2 == null) deals_SJK_2 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Business Development']; return deals_SJK_2;}
List<BlueSheet__c> deals_SJK_3; public List<BlueSheet__c> getDeals_SJK_3()    { if(deals_SJK_3 == null) deals_SJK_3 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'LOI/Proposal Negotiation']; return deals_SJK_3;}
List<BlueSheet__c> deals_SJK_4; public List<BlueSheet__c> getDeals_SJK_4()    { if(deals_SJK_4 == null) deals_SJK_4 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Contract Negotiation']; return deals_SJK_4;}
List<BlueSheet__c> deals_SJK_5; public List<BlueSheet__c> getDeals_SJK_5()    { if(deals_SJK_5 == null) deals_SJK_5 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Closed Won']; return deals_SJK_5;}
List<BlueSheet__c> deals_SJK_6; public List<BlueSheet__c> getDeals_SJK_6()    { if(deals_SJK_6 == null) deals_SJK_6 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Lost/Dropped']; return deals_SJK_6;}
List<BlueSheet__c> deals_SJK_7; public List<BlueSheet__c> getDeals_SJK_7()    { if(deals_SJK_7 == null) deals_SJK_7 = [SELECT id,Account_Name__r.name,Product1__c,Qty_of_Aircraft1__c,Owner.name FROM  BlueSheet__c  WHERE Owner_Role__c='S&M LATAM' and  Business_Stage__c = 'Open']; return deals_SJK_7;}






}