/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class OpportunityCheckContactRole
*
* NAME: OpportunityCheckContactRoleTest.cls
* AUTHOR: JFS                                                DATE: 19/12/2014
*
*******************************************************************************/
@isTest
private class OpportunityCheckContactRoleTest 
{
  static testMethod void TestFuncional()
  {
    Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');

    Account acc = SObjectInstanceTest.createAccount(recTypeId);
    database.insert(acc);

    Opportunity opp = SObjectInstanceTest.createOpportunity();
    opp.StageName = 'Qualification';
    database.insert(opp);
    
    Opportunity opp2 = SObjectInstanceTest.createOpportunity();
    opp2.StageName = 'Qualification';
    database.insert(opp2);    

    Contact contact = SObjectInstanceTest.createContact(acc.id);
    database.insert(contact);

    OpportunityContactRole Oppcontact = new OpportunityContactRole();
    Oppcontact.OpportunityId = opp.id;
    Oppcontact.ContactId = contact.id;
    Oppcontact.IsPrimary = true;
    database.insert(Oppcontact);

    opp.StageName = 'Proposal/Price Quote';
    opp2.StageName = 'Proposal/Price Quote';

    test.startTest();
    list<Database.Saveresult> lstOppResult = database.update(new list<Opportunity>{opp, opp2}, false);
    test.stopTest();

  }
  
	static testMethod void TestUnitario()
  	{
    	Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');

    	Account acc = SObjectInstanceTest.createAccount(recTypeId);
    	database.insert(acc);
    	
    	Account acc1 = SObjectInstanceTest.createAccount(recTypeId);
    	database.insert(acc1);

    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.StageName = 'Qualification';
    	opp.accountId = acc1.Id;
    	database.insert(opp);
    
    	Opportunity opp2 = SObjectInstanceTest.createOpportunity();
    	opp2.StageName = 'Qualification';
    	database.insert(opp2);    

    	Contact contact = SObjectInstanceTest.createContact(acc.id);
    	database.insert(contact);

    	OpportunityContactRole Oppcontact = new OpportunityContactRole();
    	Oppcontact.OpportunityId = opp.id;
   	 	Oppcontact.ContactId = contact.id;
    	Oppcontact.IsPrimary = true;
    	database.insert(Oppcontact);

    	opp.StageName = 'Proposal/Price Quote';
    	opp2.StageName = 'Proposal/Price Quote';

    	test.startTest();
    	list<Database.Saveresult> lstOppResult = database.update(new list<Opportunity>{opp, opp2}, false);
    	try
    	{
    		opp.accountId = acc.Id;
    		update opp;
    	}
    	catch(exception ex)
    	{
    		System.Assert(ex.getMessage().contains('It is not possible to change the account when the opportunity has contacts.'));
    	}
    	test.stopTest();

  	}
}