/*
----------------------------------------------------------------------------------------------
-- - Company:     Stefanini
-- - Name:        CRC_Dashboard_Controller
-- - Description: This class control CRC_Dashboard Lightning Component
-- - @Author: Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               Version
----------------------------------------------------------------------------------------------
-- 29/03/2019       Felipe Gouvea      1.0
-- 12/06/2019				André Leinio       1.1
----------------------------------------------------------------------------------------------
*/

public class CRC_Dashboard_Controller {

	public static CRC_CaseService service = new CRC_CaseService();


	@AuraEnabled
    // Method that calls the correct service method to retrive the header informations.
	public static IndicatorHeaderDTO getIndicatorsHeader(){
		IndicatorHeaderDTO header = service.retrieveIndicatorsHeading();
		return header;
	}
	@AuraEnabled
	public static ActionIndicatorHeader getActionIndicatorsHeader(){
		ActionIndicatorHeader header = service.getActionIndicatorsHeader();
		return header;
	}




}