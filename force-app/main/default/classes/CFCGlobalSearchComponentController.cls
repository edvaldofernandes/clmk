/**
* Visual Force  Controller 
* Classe reponsavel pela consulta de dados utilizados em auto-complete  
*/
public without sharing class CFCGlobalSearchComponentController {
    
    public String ProductName {get; set;}
    public String ProductChoosen {get;set;}
    public String countProposal {get; set;}
    public String htmlError     {get;set;}
    public Boolean countProposalEmpty {get;set;}
    public static List<Product2> listProduct {get;set;}
    public Boolean isPardotActive {get;set;}
    public String customLinkPardot {get;set;}
    public User userCFC {get; set;}
    
    public CFCGlobalSearchComponentController() {
        system.debug('CFCGlobalSearchComponentController()');
        
        try{
            htmlError = '';
            countProposalEmpty = true;
            countProposal='0';
            
            userCFC = UserDAO.getInstance().getUserById(UserInfo.getUserId());
            system.debug('CFCGlobalSearchComponentController.userCFC >>> ' + userCFC);
            
            Proposal__c propCFC = ProposalDao.getInstance().getOpenProposalByContactAndAccount(userCFC.Contact.id, userCFC.Contact.Account.id);
            System.debug('CFCGlobalSearchComponentController.propCFC >>> ' + propCFC);
            if (propCFC != null){
                List<Proposal_Items__c> lstProposalItems = ProposalItemsDao.getInstance().listItemsByProposal(propCFC.id);
                System.debug('lstProposalItems: ' + lstProposalItems);
                if (lstProposalItems != null && lstProposalItems.size() != 0){
                    countProposal = String.valueOf(lstProposalItems.size());
                    countProposalEmpty = false;
                }
            }
        } catch(Exception ex){
            system.debug('ERROR: ' + ex);
        }        
    }
    
    @remoteAction
    public static List<Product2> autoCompleteList(String searchTerm){
        listProduct = [SELECT a.Id, a.Name
                           FROM
                           Product2 a
                           WHERE Publication_Status__c = 'Published'
                           AND (
                               Name LIKE : '%'+searchTerm+'%') 
                           LIMIT 10];
        System.debug('listProduct: ' + listProduct);
        return listProduct;
    }
    
    public pageReference SearchProduct(){
        VerifyPardot();
        system.debug('productSearch >>> '  + ProductName);
        if(!String.isBlank(ProductName) && ProductName != null){
            PageReference page;
            if(isPardotActive){
                page = new PageReference(customLinkPardot + '?q=' + EncodingUtil.urlEncode(ProductName, 'UTF-8'));
            } else {
                page = new PageReference('/apex/CFCSearchResult?q=' + EncodingUtil.urlEncode(ProductName, 'UTF-8'));
            }  
            page.setRedirect(true);
            return page;  
        }else{
            htmlError = 'Type the product name in the search field';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Type the product name in the search field'));
            return null;
        }        
    }
    
    public void VerifyPardot(){
        system.debug('CFCGlobalSearchComponentController.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
            customLinkPardot = cs.Search_Link__c;
        }        
        system.debug('CFCGlobalSearchComponentController.isPardotActive >>> ' + isPardotActive);
    }
    
}