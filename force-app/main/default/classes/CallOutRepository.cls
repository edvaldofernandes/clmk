public class CallOutRepository implements Visitator {
    
    public CallOutRepository(){}
    
    public void visit(List<AccountInformation> accountInformationList, List<AccountHistoryInformation> accountHistoryInformationList, String className) {
        saveCallOut((List<AccountInformation>)accountInformationList, (List<AccountHistoryInformation>) accountHistoryInformationList, className);
    }
    
    public void visit(List<AircraftInformation> aircraftInformationList, List<AircraftHistoryInformation> aircraftHistoryList, String className) {
        saveCallOut((List<AircraftInformation>)aircraftInformationList, (List<AircraftHistoryInformation>)aircraftHistoryList, className);
    }
    
    public static List<Call_Out__c> findPayloadByClassName(String className){
        
        List<Call_Out__c> calloutList;
        
        try{
            
            calloutList = [SELECT id, payload__c, payloadHistory__c, Type_of_Object__c 
                           FROM Call_Out__c 
                           WHERE Type_of_Object__c = :className AND status__c = 'RCP_SENT'];
            
        }catch(QueryException e){}
        
        return calloutList;
    }
    
    public static List<Call_Out__c> findPayloadByClassNameAndStatus(String className){
        
        List<Call_Out__c> calloutList;
        
        try{
            
            calloutList = [SELECT id, payload__c, payloadHistory__c, Type_of_Object__c 
                           FROM Call_Out__c 
                           WHERE Type_of_Object__c = :className AND status__c = NULL];
            
        }catch(QueryException e){}
        
        return calloutList;
    }
    
    public void saveCallOut(List<Object> objList, List<Object> objListHistory, String className){
        
        Call_Out__c callout = new Call_Out__c();
        callout.payload__c = JSON.serialize(objList);
        callout.payloadHistory__c = JSON.serialize(objListHistory);
        callout.Type_of_Object__c = className;
        insert callout;
    }
    
    /*public void deleteCallOut(String className, String status){
        
        try{
            
            if(status.toUpperCase().contains(Constants.STATUS_RESPONSE)){
                
                List<Call_Out__c> calloutList = [SELECT id, payload__c, Type_of_Object__c 
                                                 FROM Call_Out__c 
                                                 WHERE Type_of_Object__c = :className];
                
                if(calloutList != null && calloutList.size() > 0){
                    
                    for(Call_Out__c callout : calloutList)
                        delete callout;
                    
                }
                
            }
            
        }catch(DmlException ex){
            
            throw new CalloutException('..... Error while trying to find callout ... ' + ex.getMessage());
        }
    }*/
    
    public static void deleCalloutSent(List<Call_Out__c> calloutDeleteList){
        
        try{
            
            Database.delete(calloutDeleteList);
            
        }catch(DmlException e){
            
            //throw new CalloutException('..... Error while trying to delete ... ' + e.getMessage());
        }
    }
    
    public static void updateCalloutSent(List<Call_Out__c> calloutDeleteList){
        
        try{
            
            Database.update(calloutDeleteList);
            
        }catch(DmlException e){
            
            //throw new CalloutException('..... Error while trying to update ... ' + e.getMessage());
        }
    }
}