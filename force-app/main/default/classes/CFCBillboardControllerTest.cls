@isTest
public class CFCBillboardControllerTest {
    
    static testMethod void myUnitTest() 
    {
        RecordType recordTypeBillboard = [Select Name, Id From RecordType Where SobjectType = 'Attachments__c' And DeveloperName = 'Billboards' limit 1 ];
        
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        system.debug('CFCBillboardControllerTest.myUnitTest.bGroup >>> ' + bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Published';
        database.insert(billboard);
        system.debug('CFCBillboardControllerTest.myUnitTest.billboard >>> ' + billboard);
        
        CFC_Configurations__c cs = new CFC_Configurations__c();
        cs.Mobile_Limits_KB__c = 1;
        cs.Name = 'Test CS';
        cs.Tablet_Limits_KB__c = 1;
        cs.Desktop_Limit_KB__c = 1;
        
        cs.Billboard_max_published_number__c = 5;
        cs.URL_Communities__c = 'wwww.teste.com.br';
        cs.Billboard_Time__c = 5;
        cs.Email_to_send_error_job__c = 'teste@teste.com.br';
        cs.User_Name__c = 'teste@teste.com';
        
        database.insert(cs);
        system.debug('CFCBillboardControllerTest.myUnitTest.cs >>> ' + cs);
        
        Blob b = Blob.valueOf('Test Data'); 
        Attachment attachment = new Attachment();  
        attachment.ParentId = billboard.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;        
        database.insert(attachment); 
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachment);
        
        //create attachment__c device Tablet to billboard
        Attachments__c attBillboardTabletTest = new Attachments__c();
        attBillboardTabletTest.Billboard__c = billboard.Id;
        attBillboardTabletTest.Active__c = true;
        attBillboardTabletTest.Description__c = 'teste';
        attBillboardTabletTest.Status__c = 'Published';
        attBillboardTabletTest.RecordTypeId = recordTypeBillboard.id;
        attBillboardTabletTest.Device_Type__c = 'Tablet';
        database.insert(attBillboardTabletTest);
        system.debug('CFCHomeControllerTest.view.attBillboardTabletTest >>> ' + attBillboardTabletTest);
        
        //create attachment__c device Mobile to billboard
        Attachments__c attBillboardMobileTest = new Attachments__c();
        attBillboardMobileTest.Billboard__c = billboard.Id;
        attBillboardMobileTest.Active__c = true;
        attBillboardMobileTest.Description__c = 'teste';
        attBillboardMobileTest.Status__c = 'Published';
        attBillboardMobileTest.RecordTypeId = recordTypeBillboard.id;
        attBillboardMobileTest.Device_Type__c = 'Mobile';
        database.insert(attBillboardMobileTest);
        system.debug('CFCHomeControllerTest.view.attBillboardMobileTest >>> ' + attBillboardMobileTest);
        
        //create attachment__c device Desktop to billboard 
        Attachments__c attBillboardDestkopTest = new Attachments__c();
        attBillboardDestkopTest.Billboard__c = billboard.Id;
        attBillboardDestkopTest.Active__c = true;
        attBillboardDestkopTest.Description__c = 'teste';
        attBillboardDestkopTest.Status__c = 'Published';
        attBillboardDestkopTest.RecordTypeId = recordTypeBillboard.id;
        attBillboardDestkopTest.Device_Type__c = 'Desktop';
        database.insert(attBillboardDestkopTest);
        system.debug('CFCHomeControllerTest.view.attBillboardDestkopTest >>> ' + attBillboardDestkopTest);
        
        //create attachment to billboard tablet
        Attachment attachmentTabletTest = new Attachment();  
        attachmentTabletTest.ParentId = attBillboardTabletTest.Id;  
        attachmentTabletTest.Name = 'Test Attachment for Parent';  
        attachmentTabletTest.Body = b;        
        database.insert(attachmentTabletTest); 
        system.debug('CFCHomeControllerTest.view.attachmentTabletTest >>> ' + attachmentTabletTest);
        
        //create attachment to billboard mobile
        Attachment attachmentMobileTest = new Attachment();  
        attachmentMobileTest.ParentId = attBillboardMobileTest.Id;  
        attachmentMobileTest.Name = 'Test Attachment for Parent';  
        attachmentMobileTest.Body = b;        
        database.insert(attachmentMobileTest); 
        system.debug('CFCHomeControllerTest.view.attachmentMobileTest >>> ' + attachmentMobileTest);
        
        ApexPages.currentPage().getParameters().put('id', billboard.Id);
        CFCBillboardController controller = new CFCBillboardController();
        
        system.debug('CFCBillboardControllerTest.myUnitTest.controller.mapBillboardImages >>> ' + controller.mapBillboardImages.values());
        system.assert(controller.mapBillboardImages.values() != null);
    }
    
}