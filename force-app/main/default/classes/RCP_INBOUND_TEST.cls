@isTest(SeeAllData=true)
global class RCP_INBOUND_TEST {
    
    @isTest global static void rcpInboundTest(){
        Test.startTest();
        try{
        
        List<RCP> rcpList = new List<RCP>();
        
        RCP_fhfc rCP_fhfc = new RCP_fhfc();
        rCP_fhfc.codigoProjeto = '175';
        rCP_fhfc.dataReferencia = date.newInstance(2015, 12, 5);
        rCP_fhfc.numeroSerieAeronave = 232;
        rCP_fhfc.numeroRegistroAeronave = '4bg67';
        rCP_fhfc.codigoOperador = 6787;
        rCP_fhfc.horaVooMes = 10.2;
        rCP_fhfc.ciclosDeVoo = 3;
        rCP_fhfc.diasOperacionais = 300;
        rCP_fhfc.horasVooAcumuladas = 52.87;
        rCP_fhfc.ciclosVooAcumulados = 17;
        
        RCP_HistAeronave rcpHistAeronave = new RCP_HistAeronave();
        rcpHistAeronave.codigoProjeto = '175';
        rcpHistAeronave.numeroSerieAeronave = 232;
        rcpHistAeronave.numeroRegistroAeronave = '4bg67';
        rcpHistAeronave.apu = '1xduyyuu';
        rcpHistAeronave.dataEntregaOperador = date.newInstance(2015, 12, 5);
        rcpHistAeronave.dataUltimaAtualizacao = date.newInstance(2015, 12, 5);
        rcpHistAeronave.motorUm = 'pn480xxxxxxx';
        rcpHistAeronave.motorDois = 'pn481xxxxxxx';
        
        RCP_Mtbur rcpMtbur = new RCP_Mtbur();
        rcpMtbur.ataChapter = 321123;
		rcpMtbur.familia = '190';
   	 	rcpMtbur.CodigoOperador = 6787;
		rcpMtbur.partNumber = 'pn-123-456';
		rcpMtbur.operadorGroupDesc = 'oper desc';
    	rcpMtbur.partNumberDesc = 'part number desc';
    	rcpMtbur.removalsL12M = 0001001;
        
        Account account = new Account();
        account.Name = 'test rcp_inboud';
        account.Company_Nickname__c = 'test rcp_inboud';
        account.COD_ORGz__c = 6787;
        insert account;    
        
        RCP_Problem rcp_Problem = new RCP_Problem();
        rcp_Problem.ataChapter = 32112;
        rcp_Problem.failCode = 3123;
        rcp_Problem.aircraftSerialNumber = '232';
        rcp_Problem.codeProj = '175';  
        rcp_Problem.problem = 77777;
        rcp_Problem.ataSubChapter = 98765;
        rcp_Problem.problemType = '1n2wert4455';
        rcp_Problem.eventDate = date.newInstance(2015, 12, 5);
        rcp_Problem.problemCorrection = 'Problem Correction';
        rcp_Problem.destiny = 'destiny';
        rcp_Problem.origin = 'origin';
        rcp_Problem.failCodeDescription = 'failCodeDescription';
        rcp_Problem.techicalIncident = 'techicalIncident';
        rcp_Problem.interruptionType = 'interruptionType';
        rcp_Problem.ifsd = 'ifsd';
        rcp_Problem.flightNumber = 'flightNumber';
        rcp_Problem.delayTime = 01001001;
        rcp_Problem.register = 'register';  
		
        RCP_Remocoes rcp_Remocoes = new RCP_Remocoes();
        rcp_Remocoes.numeroSerieAeronave = 232;
        rcp_Remocoes.partNumberNoSerieOn = 'ccbBr1910';    
        rcp_Remocoes.partNumberNoSerieOff = 'quDequi';
        rcp_Remocoes.partNumber  = 'z123hjhs';
        rcp_Remocoes.codPnSubtt  =  'hs321z1';   
        rcp_Remocoes.codigoProjeto = '175';
        rcp_Remocoes.dataEventRemocoes = date.newInstance(2015, 12, 5);
        rcp_Remocoes.razaoRemocao = 'unvailable';
        rcp_Remocoes.classificacao = '1Br2017';
        
        List<RCP_fhfc> rCP_fhfcList = new List<RCP_fhfc>();
        List<RCP_HistAeronave> rcpHistAeronaveList = new List<RCP_HistAeronave>();
        List<RCP_Mtbur> rcpMtburList = new List<RCP_Mtbur>();
        List<RCP_Problem> rcp_ProblemList = new List<RCP_Problem>();    
        List<RCP_Remocoes> rcp_RemocoesList = new List<RCP_Remocoes>();
        
        rCP_fhfcList.add(rCP_fhfc);    
        rcpHistAeronaveList.add(rcpHistAeronave);
        rcpMtburList.add(rcpMtbur);
        rcp_ProblemList.add(rcp_Problem);
        rcp_RemocoesList.add(rcp_Remocoes);
            
        rcpList.add(new RCP(rCP_fhfcList, rcpHistAeronaveList, rcpMtburList, rcp_ProblemList, rcp_RemocoesList));
        
        RCP_INBOUND.rcpInbound(rcpList);     
        
        //RCPService rCPService = new RCPService(rcpList);
        //rCPService.parseToObject();
        
        RCP_FH_FC__c rCPFhFcObj = [SELECT id,
                                          codigoProjeto__c,
                                          dataReferencia__c,
                                          numeroSerieAeronave__c,
                                          numeroRegistroAeronave__c,
                                          codigoOperador__c,
                                          horaVooMes__c,
                                          ciclosDeVoo__c,
                                          diasOperacionais__c,
                                          horasVooAcumuladas__c,
                                          ciclosVooAcumulados__c
                                     FROM RCP_FH_FC__c
                                     WHERE codigoProjeto__c = '175'];
        
        System.assertEquals(rCPFhFcObj.codigoProjeto__c,  rCP_fhfc.codigoProjeto);
        
        Historico_ANV__c historico_ANVObj = [SELECT id,
                                            		codigoProjeto__c,
                                            		numeroSerieAeronave__c,
                                                    numeroRegistroAeronave__c,
                                                    apu__c,
                                                    dataEntregaOperador__c,
                                            		dataUltimaAtualizacao__c,
                                                    motorUm__c,
                                                    motorDois__c
                                              FROM Historico_ANV__c
                                              WHERE codigoProjeto__c = '175'];
        
        System.assertEquals(historico_ANVObj.codigoProjeto__c,  rcpHistAeronave.codigoProjeto);
        
        String idOperator = (String.valueOf(123));

        RCP_MTBUR__c rcp_MTBURObj = [SELECT id,
                                		    AtaChapter__c,
                                    		family__c,
                                    		Operador__c,
                                    		partNumber__c,
                                    		GroupDescription__c,
                                    		PartNumberDescription__c,
                                            RemovalsL12M__c 
                                      FROM RCP_MTBUR__c
                                      WHERE AtaChapter__c = 321123];
       
        System.assertEquals(rcp_MTBURObj.AtaChapter__c, rcpMtbur.AtaChapter);
        
        RCP_problem__c RCP_ProblemObj = [SELECT id,
                                        		ataChapter__c,
                                        		failCode__c,
                                        		aircraftSerialNumber__c,
                                        		codeProj__c,
                                        		problem__c,
                                        		ataSubChapter__c,
                                        		problemType__c,
                                        		eventDate__c
                                         FROM RCP_problem__c
                                         WHERE codeProj__c = '175'];
        
       System.assertEquals(RCP_ProblemObj.codeProj__c, rcp_Problem.codeProj);
        
        RCP_Rem__c rcp_RemObj = [SELECT id,
                                		numeroSerieAeronave__c,
                                		partNumberNoSerieOn__c,
                                		partNumberNoSerieOff__c,
                                		partNumber__c,
                                		codPnSubtt__c,
                                		codigoProjeto__c,
                                		dataEventRemocoes__c,
                                		
                                		classificacao__c
                                  FROM RCP_Rem__c
                                  WHERE codigoProjeto__c = '175'];
		       
        System.assertEquals(rcp_RemObj.codigoProjeto__c, rcp_Remocoes.codigoProjeto);
		
        }catch(TypeException e){}
		
        Test.stopTest();

    }
    
    @isTest static void buildSerialNumberTest(){
        
        String model175 = '175';
        String model195 = '195';
        
        String serialNumber = '232';
        
        RcpService  rcpService = new RcpService();
        
        String serialNumberResult175 = rcpService.buildSerialNumber(model175, serialNumber);
        
        String serialNumberResult195 = rcpService.buildSerialNumber(model195, serialNumber);
        
        System.assertEquals(serialNumberResult175, '17000232');
        System.assertEquals(serialNumberResult195, '19000232');
    }
}