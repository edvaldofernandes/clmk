public class RcpOutBoundAccountService {
    
    private List<AccountInformation> accountInformationList;
    private List<AccountHistoryInformation> accountHistoryInformationList;
    private List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount> rcpAccountList;
    private List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist> rcpAccountHistList;
    private List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember> rcpAccountTeamMemberList;
    private List<Call_Out__c> calloutUpdateList;
    
    public RcpOutBoundAccountService(){
        
        initNativeObject();
        initWSElement();
    }
    
    private void initNativeObject(){
        
        calloutUpdateList = new List<Call_Out__c>();
        accountInformationList = deserializeAccountInformation();
        accountHistoryInformationList = deserializeAccountHistoryInformation();
    }
    
    private void initWSElement(){
        
        rcpAccountList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount>();
        rcpAccountHistList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist>();
        rcpAccountTeamMemberList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember>();
    }
    
    public List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount> buildRcpAccount(){
        
        for(AccountInformation accountInformation : accountInformationList){
            
            Account account = accountInformation.getAccount();
            
            RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount SfAccount = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount();
            
            SfAccount.accountType = account.Account_Type__c;
            SfAccount.companyName = account.name;
            SfAccount.companyNickname = account.Company_Nickname__c;
            SfAccount.icaoCode = account.ICAO_Code__c;
            SfAccount.geographicalArea = account.Geographical_Area__c;
            SfAccount.flyEmbraerID = account.FlyEmbraerId__c;
            SfAccount.flyEmbraerName = account.FlyEmbraer_Name__c;
            
            rcpAccountList.add(SfAccount);
        }
        
        return rcpAccountList;
    }
    
    public List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist> buildRcpAccountHistory(){
        
        for(AccountHistoryInformation accountHistoryInformation : accountHistoryInformationList){
            
            RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist SfAccountHist = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist();
            System.debug('accountHistoryInformation.getCreatedDate()   ' + accountHistoryInformation.getCreatedDate());
            //SfAccountHist.createdDate = accountHistoryInformation.getCreatedDate();
            SfAccountHist.createdById = accountHistoryInformation.getCreatedBy();
            SfAccountHist.field = accountHistoryInformation.getField();
            SfAccountHist.oldValue = accountHistoryInformation.getOldValue();
            SfAccountHist.newValue = accountHistoryInformation.getNewValue();
            
            rcpAccountHistList.add(SfAccountHist);
        }
        
        return rcpAccountHistList;
    }
    
    public static List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember> buildRcpTeamMember(List<AccountTeamMember> accountTeamMemberList){
        
        List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember> teamMemberList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember>();
        List<String> userIdList = new List<String>();
        
        if(accountTeamMemberList == null)
            return null;
        
        for(AccountTeamMember accountTeamMember : accountTeamMemberList)
            userIdList.add(accountTeamMember.userId);
        
        
        List<user> userList = rcpRepository.searchUserByUserId(userIdList);
        
        for(AccountTeamMember accountTeamMember : accountTeamMemberList){
            
            for(User user : userList){
                
                if(user.id == accountTeamMember.userId){
                    
                    RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember SfTeamMember = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember();
                    
                    SfTeamMember.teamMemberRole = accountTeamMember.TeamMemberRole;
                    SfTeamMember.user_x = '[Phone: '+user.phone+' Email: '+user.E_mail__c+' Name: '+user.name+']';
                    
                    teamMemberList.add(SfTeamMember);
                }
            }
        }
        
        return teamMemberList;
    }
    
    private List<AccountInformation> deserializeAccountInformation(){
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassNameAndStatus(String.valueOf(AccountInformation.class));
        
        List<AccountInformation>  accountInformationList = new List<AccountInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AccountInformation>  accountInfoList = (List<AccountInformation>)JSON.deserialize(callout.payload__c, List<AccountInformation>.class);
            
            for(AccountInformation accountInformation : accountInfoList)
                accountInformationList.add(accountInformation); 
        } 
        
        return accountInformationList;
    }
    
    private List<AccountHistoryInformation> deserializeAccountHistoryInformation(){ 
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassNameAndStatus(String.valueOf(AccountInformation.class));
        
        List<AccountHistoryInformation>  accountHistoryInformationList = new List<AccountHistoryInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AccountHistoryInformation>  accountHistoryInfoList = (List<AccountHistoryInformation>)JSON.deserialize(callout.payload__c, List<AccountHistoryInformation>.class);
            
            for(AccountHistoryInformation accountHistoryInformation : accountHistoryInformationList)
                accountHistoryInfoList.add(accountHistoryInformation);
            
            callout.status__c = 'RCP_SENT';
            calloutUpdateList.add(callout);
        }
        
        return accountHistoryInformationList;
    }
    
    public List<Call_Out__c> getCalloutUpdateList(){
        
        return calloutUpdateList;
    }
}