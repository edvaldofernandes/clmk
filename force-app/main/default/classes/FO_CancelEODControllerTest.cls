/**
* @description: FO_CancelEODController test class.
**/
@isTest
public class FO_CancelEODControllerTest {
    /**
    * @description Test if redirect() method returns the cancelled EOD record detail page.
    **/
    @isTest static void testIsGettingCancelRedirectPage(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        
		ApexPages.currentPage().getParameters().put('id',eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
        
	    FO_CancelEODController controller  = new FO_CancelEODController(stdController);
        
        PageReference cancelledEODPage = controller.redirect();
        system.assertEquals(cancelledEODPage.getUrl(), '/' + eod.Id);
    }
}