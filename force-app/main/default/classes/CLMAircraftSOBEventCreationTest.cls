@isTest
public class CLMAircraftSOBEventCreationTest {
    
    static testMethod void testProposalSingleAndExpire(){
		
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        database.insert(productTest);
        
        id priceBook2Id = SObjectInstanceTest.getPricebook2Std2();
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(priceBook2Id, productTest.Id); 
        database.insert(priceBookEntryTest);

        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Name = 'Valid Proposal';
        Agreement1.Account__c = accountTest.Id;
        Agreement1.Buyer__c = accountTest.Id;
        Agreement1.Proposal_Validity__c = Date.today();
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        Agreement1.Agreement_Color__c = '0000FF';
        Agreement1.DF_Color__c = 'Blue';
        Agreement1.Admin_Mode__c = 'true';
        Agreement1.DOCCON_Number__c = '123';
        Agreement1.Status_Category__c = 'Proposal Request';
        //Agreement1.Bypass_Approval__c = true;
        //Agreement1.Bypass_Comments__c = ' ok ';
        Agreement1.Country__c = 'Brazil';
        Agreement1.Signature_Due_Date__c = Date.today();
        Agreement1.Signature_Date__c = Date.today();
        Agreement1.Nickname__c = 'Valid Proposal';
        Agreement1.Change_History__c = 'Valid Proposal signature';
        Agreement1.Signature_Date__c = date.today();
        //Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId();
        database.insert(new List<Agreement__c>{Agreement1});
        
        //create aircraft price
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2});
        
        //create line item
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft2.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft3.Aircraft_Price__c = aircraftPrice1.Id;
        
        List<Agreement_Aircraft__c> createdACList = new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3};
        database.insert(createdACList);
        
        //create initial SOB Event
        //SOB_Event__c sobEvent1 = SObjectInstanceTest.createSOBEvent(agreementAircraft1.Id, 'Firm', date.today(), 'Firm Included');
        //SOB_Event__c sobEvent2 = SObjectInstanceTest.createSOBEvent(agreementAircraft2.Id, 'Option', date.today(), 'Option Included');
        //SOB_Event__c sobEvent3 = SObjectInstanceTest.createSOBEvent(agreementAircraft3.Id, 'Purchase Right', date.today(), 'PRA Included');
        //database.insert(new List<SOB_Event__c>{sobEvent1, sobEvent2, sobEvent3});
        
        agreementAircraft1.Order_Type__c = 'Firm';
        agreementAircraft2.Order_Type__c = 'Option';
        agreementAircraft3.Order_Type__c = 'Purchase Right';
        database.update(createdACList);
        
        List<SOB_Event__c> createdSOBList = [SELECT Id FROM SOB_Event__c WHERE Contract_Aircraft__c IN: createdACList];
        System.assertEquals(3,createdSOBList.size());
        createdSOBList.Clear();
        
        Test.startTest();
   		ApexPages.currentPage().getParameters().put('id',Agreement1.Id);
        //ApexPages.StandardController sc = new ApexPages.StandardController(listAgg[0]);
        //CLMEndAgreementController eac = new CLMRevertToRequestController(sc);
        CLMEndAgreementController.expire();
        Test.stopTest();
        
        createdSOBList = [SELECT Id FROM SOB_Event__c WHERE Date_Type__c = 'Expired'];
        //System.assertEquals(3,createdSOBList.size());

    }
    
    
    static testMethod void testPADeliveryAndTerminate(){
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        database.insert(productTest);
        
        id priceBook2Id = SObjectInstanceTest.getPricebook2Std2();
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(priceBook2Id, productTest.Id); 
        database.insert(priceBookEntryTest);

        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Name = 'Valid PA';
        Agreement1.Account__c = accountTest.Id;
        Agreement1.Buyer__c = accountTest.Id;
        Agreement1.Proposal_Validity__c = Date.today();
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        Agreement1.Agreement_Color__c = '0000FF';
        Agreement1.DF_Color__c = 'Blue';
        Agreement1.Admin_Mode__c = 'true';
        Agreement1.DOCCON_Number__c = '123';
        Agreement1.Status_Category__c = 'PA Signed';
        Agreement1.Country__c = 'Brazil';
        Agreement1.Signature_Due_Date__c = Date.today();
        Agreement1.Signature_Date__c = Date.today();
        Agreement1.Nickname__c = 'Valid PA';
        Agreement1.Change_History__c = 'Valid PA signature';
        Agreement1.Signature_Date__c = date.today();
        //Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(new List<Agreement__c>{Agreement1});
        
        //create aircraft price
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2});
        
        //create line item
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft2.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft3.Aircraft_Price__c = aircraftPrice1.Id;
        
        List<Agreement_Aircraft__c> createdACList = new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3};
        database.insert(createdACList);
        
        //create initial SOB Event
        //SOB_Event__c sobEvent1 = SObjectInstanceTest.createSOBEvent(agreementAircraft1.Id, 'Firm', date.today(), 'Firm Included');
        //SOB_Event__c sobEvent2 = SObjectInstanceTest.createSOBEvent(agreementAircraft2.Id, 'Option', date.today(), 'Option Included');
        //SOB_Event__c sobEvent3 = SObjectInstanceTest.createSOBEvent(agreementAircraft3.Id, 'Purchase Right', date.today(), 'PRA Included');
        //database.insert(new List<SOB_Event__c>{sobEvent1, sobEvent2, sobEvent3});
        
        agreementAircraft1.Order_Type__c = 'Firm';
        agreementAircraft2.Order_Type__c = 'Option';
        agreementAircraft3.Order_Type__c = 'Purchase Right';
        database.update(createdACList);
        
        List<SOB_Event__c> createdSOBList = [SELECT Id FROM SOB_Event__c WHERE Contract_Aircraft__c IN: createdACList];
        System.assertEquals(3,createdSOBList.size());
        createdSOBList.Clear();
        
        //Agreement1.Bypass_Approval__c = true;
        //Agreement1.Bypass_Comments__c = ' ok ';
        //Agreement1.Status_Category__c = 'PA Approved';
        //database.update(Agreement1);
        
        Test.startTest();
   		ApexPages.currentPage().getParameters().put('id',Agreement1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMSignAgreementController ac = new CLMSignAgreementController(sc);
        ac.sign();
        
        agreementAircraft1.Status__c = 'Delivered';
        agreementAircraft1.Actual_Delivery_Date__c = Date.today();
        database.update(agreementAircraft1);
        
        createdSOBList = [SELECT Id FROM SOB_Event__c WHERE Book_Status__c = 'Delivered'];
        //System.assertEquals(1,createdSOBList.size());        
        
        Agreement1.Status_Category__c = 'Terminated';
        database.update(Agreement1);
        
        agreementAircraft2.Status__c = 'Terminated';
        database.update(agreementAircraft2);
        
        agreementAircraft3.Status__c = 'Terminated';
        database.update(agreementAircraft3);
        
        Test.stopTest();
        
        createdSOBList = [SELECT Id FROM SOB_Event__c 
                          WHERE Contract_Aircraft__c IN: createdACList 
                          AND Date_Type__c = 'Terminated'];
        System.assertEquals(2,createdSOBList.size());
    }
    
    static testMethod void testPAAmendAircraft(){
		
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        database.insert(productTest);
        
        id priceBook2Id = SObjectInstanceTest.getPricebook2Std2();
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(priceBook2Id, productTest.Id); 
        database.insert(priceBookEntryTest);

        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Name = 'Valid PA';
        Agreement1.Account__c = accountTest.Id;
        Agreement1.Buyer__c = accountTest.Id;
        Agreement1.Proposal_Validity__c = Date.today();
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        Agreement1.Agreement_Color__c = '0000FF';
        Agreement1.DF_Color__c = 'Blue';
        Agreement1.Admin_Mode__c = 'true';
        Agreement1.DOCCON_Number__c = '123';
        Agreement1.Status_Category__c = 'PA Request';
        Agreement1.Country__c = 'Brazil';
        Agreement1.Signature_Due_Date__c = Date.today();
        Agreement1.Signature_Date__c = Date.today();
        Agreement1.Nickname__c = 'Valid PA';
        Agreement1.Change_History__c = 'Valid PA signature';
        Agreement1.Signature_Date__c = date.today();
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(new List<Agreement__c>{Agreement1});
        
        //create aircraft price
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2});
        
        //create line item
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Purchase Right');
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft2.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft3.Aircraft_Price__c = aircraftPrice1.Id;
        
        List<Agreement_Aircraft__c> createdACList = new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3};
        database.insert(createdACList);
        
        Agreement1.Status_Category__c = 'PA Signed';
        database.update(Agreement1);
        
        Test.setCurrentPage(Page.CLMAmendment);
        ApexPages.currentPage().getParameters().put('id',Agreement1.Id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMAmendmentController pc = new CLMAmendmentController(sc);
        pc.amend();
        test.stopTest();
    
        Agreement__c amendedAgreement = new Agreement__c();
        amendedAgreement = [SELECT Id FROM Agreement__c WHERE Name = 'Valid PA Amendment'];
        
        List<Agreement_Aircraft__c> amendedAC = new List<Agreement_Aircraft__c>();
        amendedAC = [SELECT Id, Actual_Delivery_Date__c, Order_Type__c, Status__c FROM Agreement_Aircraft__c WHERE Agreement__c =: amendedAgreement.Id];
        
        amendedAC[0].Status__c = 'Delivered';
        amendedAC[0].Actual_Delivery_Date__c = Date.today();
        
        amendedAC[1].Order_Type__c = 'Firm';
        amendedAC[1].Option_Execution_Date__c = Date.today();
        
        amendedAC[2].Order_Type__c = 'Option';
        amendedAC[2].Pra_Execution_Date__c = Date.today();
        Database.update(amendedAC);
        
        amendedAC[2].Order_Type__c = 'Purchase Right';
        amendedAC[2].Option_Execution_Date__c = Date.today();
        Database.update(amendedAC);
        
        
    }

}