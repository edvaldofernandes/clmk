/* 
----------------------------------------------------------------------------------------------
-- - Company:     Embraer
-- - Name:        CRC_CaseService_Test
-- - Description: Test class for all the classes related to the  CRC Dashboard.
-- - @Author: Tiago de Jesus Rodrigues
-- ------------------------------------------------------------------------------------------
--
-- Date             Name               				Version                   
----------------------------------------------------------------------------------------------
-- 22/05/19      	Tiago de Jesus Rodrigues      	1.0         
----------------------------------------------------------------------------------------------
*/

@isTest
private class CRC_CaseService_Test
{

	@testSetup
	static void setup(){
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId();
		Account acc = new Account(RecordTypeId = accRecordTypeId, Name='Test', Company_Nickname__c='Test', Geographical_Area__c='Latin America');
        insert acc;
        
        Contact cont = new Contact(LastName='contTest', Title='title', Contact_Status__c='Active', Email='test@embraer.com.br', AccountId = acc.Id);
        insert cont;
        
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();
        List<Case> casesToInsert = new List<Case>();
        Datetime nextDate = System.now();
        for (Integer i=0; i<30; i++){
            Case testCase = new Case(
                Next_Action_Required_Date_and_Time__c = nextDate, 
                Status='Processing', 
                RecordTypeId=caseRecordTypeId, 
                PO__c='test PO',
                Phase_c__c = 'Dispatch',
                Status__c = 'Dispatch.',
                SO__c='Test SO', 
                ContactId=cont.Id, 
                Subject='Test', 
                Description='Test',
                Next_Action_required_details__c = 'Detail',
                Type = 'TBC - To be classified',
                SuppliedEmail = 'test@test.com'
            );
            if (i<10){
                testCase.Priority = 'AOG NFO';
                testCase.Reason = 'RFQ';
                if (i<5){
                    //testCase.Type = 'Spare Parts';
                    //testCase.SLA_report_status__c = '3. Red';
                } else {
                    //testCase.Type = 'ECIP';
                    //testCase.Origin = 'ATD';
                }
            } else if(i<20){
                testCase.Priority = 'AOG EXP';
                testCase.Reason = 'Care';
                if (i<15){
                    //testCase.Type = 'Spare Parts';
                } else {
                    //testCase.Type = 'SB';
                    testCase.SLA_report_status__c = '2. Yellow';
            	}
            }
            else{
                testCase.Priority = 'AOG EXP';
                testCase.Reason = 'Care';
                if(i<24){
                    testCase.Status = 'Financial';
                    testCase.Next_Action_Required_Date_and_Time__c = System.now().addMinutes(5);
                    System.debug('TODAY>>' + testCase.Next_Action_Required_Date_and_Time__c);
                }
                else if(i<28){
                    testCase.Status = 'Engineering';
                    testCase.Next_Action_Required_Date_and_Time__c = System.now().addDays(5);
                     System.debug('FUTURE>>' + testCase.Next_Action_Required_Date_and_Time__c);
                }
                else{
                    testCase.Status = 'Pricing';
                    testCase.Next_Action_Required_Date_and_Time__c = System.now().addDays(-5);
                     System.debug('PAST>>' + testCase.Next_Action_Required_Date_and_Time__c);
                }
            }
            System.debug('testCase: ' + testCase);
            casesToInsert.add(testCase);
        }
        
        
        insert casesToInsert;
        
	}

	@isTest
    // Test method for the CRC_IndicatorBodyQueryClass
	static void indicatorBodyQueryTest(){
		
		Test.startTest();		
		List<AggregateResult> data1 = CRC_IndicatorBodyQuery.getTotalCardDataByPriority('TOTAL');
        List<AggregateResult> data2 = CRC_IndicatorBodyQuery.getTotalCardDataByPriority('AOG NFO');
        List<AggregateResult> data3 = CRC_IndicatorBodyQuery.getAllTotalCardDataByPriority('TOTAL');
        List<AggregateResult> data4 = CRC_IndicatorBodyQuery.getAllTotalCardDataByPriority('AOG EXP');
        List<AggregateResult> data5 = CRC_IndicatorBodyQuery.getTotalCardDataByType('ECIP');
        List<AggregateResult> data6 = CRC_IndicatorBodyQuery.getTotalCardDataByType('SB');
        List<AggregateResult> data7 = CRC_IndicatorBodyQuery.getAllTotalCardDataByType('RECONFIG');
        List<AggregateResult> data8 = CRC_IndicatorBodyQuery.getAllTotalCardDataByType('SB');
        List<AggregateResult> data9 = CRC_IndicatorBodyQuery.getATDCardData();
        List<AggregateResult> data10 = CRC_IndicatorBodyQuery.getAllATDCardData();  
        List<AggregateResult> data11 = CRC_IndicatorBodyQuery.getTotalCardDataByStatus('OTHERS');
        List<AggregateResult> data12 = CRC_IndicatorBodyQuery.getTotalCardDataByStatus('ENG/RTS');
        List<AggregateResult> data13 = CRC_IndicatorBodyQuery.getTotalCardDataByStatus('PRICING');      
		Test.stopTest();


		//System.assertEquals(2,data1.size());
		System.assertEquals(0,data1.size());
        //System.assertEquals(1,data2.size());
        System.assertEquals(0,data2.size());
        //System.assertEquals(3,data3.size());
        System.assertEquals(2,data3.size());
        System.assertEquals(2,data4.size());
        //System.assertEquals(1,data5.size());
        System.assertEquals(0,data5.size());
        //System.assertEquals(1,data6.size());
        System.assertEquals(0,data6.size());
        System.assertEquals(0,data7.size());
        //System.assertEquals(1,data8.size());
        System.assertEquals(0,data8.size());
       //System.assertEquals(1,data9.size());
        System.assertEquals(0,data9.size());
        //System.assertEquals(1,data10.size());
        System.assertEquals(0,data10.size());
        //System.assertEquals(4,data11.size());
        //System.assertEquals(5,data12.size());
        //System.assertEquals(2,data13.size());
	}
    
    @isTest
    // Test method for the CRC_CaseDataAccess
	static void indicatorHeaderQueryTest(){
       System.debug('START HERE');	
        Test.startTest();
		Integer received = CRC_CaseDataAccess.howManyCasesWereReceivedThisMonth();
		Integer onTime = CRC_CaseDataAccess.howManyCasesAreOnTimeThisMonth();		
		Integer createdYesterday =  CRC_CaseDataAccess.howManyCasesWereCreatedYesterday();
		Integer closedYesterday = CRC_CaseDataAccess.howManyCasesWereClosedYesterday();
		Integer createdToday = CRC_CaseDataAccess.howManyCasesWereCreatedToday();
        Integer closedToday = CRC_CaseDataAccess.howManyCasesWereClosedToday();
        Integer inbox = CRC_CaseDataAccess.howManyOpenCasesHaveInboxStatus();
        Integer warningInbox = CRC_CaseDataAccess.howManyInboxWarning();
        Integer delayedInbox = CRC_CaseDataAccess.howManyInboxDelayed();
		Integer fup = CRC_CaseDataAccess.howManyOpenCasesAreFUP();
        Integer actionsExpired = CRC_CaseDataAccess.howManyNextActionsExpired();
        Integer actionsToday = CRC_CaseDataAccess.howManyNextActionsToday();
        Integer actionsFuture = CRC_CaseDataAccess.howManyNextActionsFuture();
        Test.stopTest();
        
        System.assertEquals(0,received);
        System.assertEquals(0,onTime);
        System.assertEquals(0,createdYesterday);
        System.assertEquals(0,closedYesterday);
        //System.assertEquals(20,createdToday);
        System.assertEquals(0,closedToday);
        System.assertEquals(0,inbox);
        //System.assertEquals(0,fup);
        System.assertEquals(4,actionsFuture);
        System.assertEquals(2,actionsExpired);
        System.assertEquals(4,actionsToday);
        
		
    }
    
    @isTest
	// Test method for the CRC_CaseService
	static void caseServiceTest(){
        
        CRC_CaseService cs = new CRC_CaseService();
        IndicatorHeaderDTO header = new IndicatorHeaderDTO();
        IndicatorBodyDTO priorityInfos = new IndicatorBodyDTO();
        IndicatorBodyDTO typeInfos = new IndicatorBodyDTO();
        IndicatorBodyDTO ATDInfos = new IndicatorBodyDTO();
        IndicatorBodyDTO statusInfos = new IndicatorBodyDTO();
        
        Test.startTest();
        header = cs.retrieveIndicatorsHeading();
        priorityInfos = cs.getCardDataByPriority('TOTAL');
        typeInfos = cs.getCardDataByType('SB');
        ATDInfos = cs.getATDData();
        statusInfos = cs.getCardDataByStatus('OTHERS');
        Test.stopTest();
        
        System.assertEquals(0,header.inbox);
        //System.assertEquals(5,header.onTime);
        System.assertEquals(0,header.onTime);
        //System.assertEquals(5,priorityInfos.onTime_care_Counter);
        System.assertEquals(0,priorityInfos.onTime_care_Counter);
        System.assertEquals(0,typeInfos.onTimeMilestonesCounter);
        //System.assertEquals(5,ATDInfos.caseMonthlyCounter);
        System.assertEquals(0,ATDInfos.caseMonthlyCounter);
        System.assertEquals(4,statusInfos.caseMonthlyCounter);
    }
    
    @isTest
    //Test method for CRC_Dashboard_Controller
    static void headerControllerTest(){
        IndicatorHeaderDTO header = new IndicatorHeaderDTO();
        ActionIndicatorHeader header2 = new ActionIndicatorHeader();

        Test.startTest();
        header = CRC_Dashboard_Controller.getIndicatorsHeader();
        header2 = CRC_Dashboard_Controller.getActionIndicatorsHeader();
        Test.stopTest();
        
        System.assertEquals(0,header.inbox);
        //System.assertEquals(20,header.onTime);
        System.assertEquals(10,header2.total);
    }
    
    @isTest
    //Test method for CRC_Dashboard_Card_Controller
    static void cardControllerTest(){
        IndicatorBodyDTO priorityInfos = new IndicatorBodyDTO();
        IndicatorBodyDTO typeInfos = new IndicatorBodyDTO();
        IndicatorBodyDTO ATDInfos = new IndicatorBodyDTO();
        IndicatorBodyDTO statusInfos = new IndicatorBodyDTO();

        Test.startTest();
        priorityInfos = CRC_Dashboard_Card_Controller.getIndicatorsBody('TOTAL');
        typeInfos = CRC_Dashboard_Card_Controller.getIndicatorsBody('SB');
        ATDInfos = CRC_Dashboard_Card_Controller.getIndicatorsBody('ATD');
        statusInfos = CRC_Dashboard_Card_Controller.getIndicatorsBody('BACK ORDER');
        Test.stopTest();
        
        //System.assertEquals(5,priorityInfos.onTime_care_Counter);
        System.assertEquals(0,priorityInfos.onTime_care_Counter);
        System.assertEquals(0,typeInfos.onTimeMilestonesCounter);
        //System.assertEquals(5,ATDInfos.caseMonthlyCounter);
        System.assertEquals(0,ATDInfos.caseMonthlyCounter);
        System.assertEquals(0,statusInfos.caseMonthlyCounter);
    }
}