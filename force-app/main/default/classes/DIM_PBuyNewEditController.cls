// Used by DIM - Market Intelligence.
public with sharing class DIM_PBuyNewEditController {

    public PBuy__c newPBuy {get; set;}
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public DIM_PBuyNewEditController(ApexPages.StandardController stdController) {
        newPBuy = (PBuy__c)stdController.getRecord();
        
        if (String.isEmpty(newPBuy.Id)) {
            updateNewPBuyAccordingToTheLastPBuy();
        }
        updateCriteriaDetails();
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public void updateNewPBuyAccordingToTheLastPBuy() {
    
        List<PBuy__c> pBuys = [SELECT Customer_Needs__c, Airline_Health__c, Time_to_Close_Deal__c FROM PBuy__c WHERE Account__c =: newPBuy.Account__c ORDER BY Date__c DESC, Name DESC LIMIT 1];
        if (pBuys.size() > 0) {
            PBuy__c lastPBuy = pBuys.get(0);
            
            newPBuy.Customer_Needs__c = lastPBuy.Customer_Needs__c;
            newPBuy.Airline_Health__c = lastPBuy.Airline_Health__c;
            newPBuy.Time_to_Close_Deal__c = lastPBuy.Time_to_Close_Deal__c;
        }
        else {
            newPBuy.Customer_Needs__c = (String) PBuy__c.Customer_Needs__c.getDescribe().getDefaultValue();
            newPBuy.Airline_Health__c = (String) PBuy__c.Airline_Health__c.getDescribe().getDefaultValue();
            newPBuy.Time_to_Close_Deal__c = (String) PBuy__c.Time_to_Close_Deal__c.getDescribe().getDefaultValue();
        }
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private void updateCriteriaDetails() {
        newPBuy.Customer_Needs_Definition__c = ((String) PBuy__c.Customer_Needs_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPBuy.Airline_Health_Definition__c = ((String) PBuy__c.Airline_Health_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPBuy.Time_To_Close_Deal_Definition__c = ((String) PBuy__c.Time_To_Close_Deal_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

}