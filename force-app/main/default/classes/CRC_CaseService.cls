/*
----------------------------------------------------------------------------------------------
-- - Company:     Embraer
-- - Name:        CRC_Dashboard_Card_Controller
-- - Description: Class responsible for threating the information gathered from the server and
-- 				  sending them to CRC Dashboard controller.
-- - @Author: Tiago de Jesus Rodrigues
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version
----------------------------------------------------------------------------------------------
-- 06/05/2019		Tiago de Jesus Rodrigues		1.0
-- 06/10/2019		André Vitor Leinio Graça		1.1
----------------------------------------------------------------------------------------------
*/

public class CRC_CaseService {

	public static final string FUP_STATUS = 'FUP';	// FUP status => to be modified.

	// Method that creates and fills the structure with the header information.
	public IndicatorHeaderDTO retrieveIndicatorsHeading(){
		IndicatorHeaderDTO header = new IndicatorHeaderDTO();

		header.received = (decimal) nullChecker(CRC_CaseDataAccess.howManyCasesWereReceivedThisMonth());
		header.onTime = (decimal) nullChecker(CRC_CaseDataAccess.howManyCasesAreOnTimeThisMonth());
		header.goal = '97%';
        if (header.received==0){
            header.reached = '100%';
        } else {
            decimal percentage = 100*header.onTime/header.received;
            header.reached = string.valueOf(math.round(percentage)) + '%';
        }


		header.createdYesterday =  nullChecker(CRC_CaseDataAccess.howManyCasesWereCreatedYesterday());
		header.closedYesterday = nullChecker(CRC_CaseDataAccess.howManyCasesWereClosedYesterday());
		header.createdToday = nullChecker(CRC_CaseDataAccess.howManyCasesWereCreatedToday());
		header.closedToday = nullChecker(CRC_CaseDataAccess.howManyCasesWereClosedToday());

		header.inbox = nullChecker(CRC_CaseDataAccess.howManyOpenCasesHaveInboxStatus());
        header.warningInbox = nullChecker(CRC_CaseDataAccess.howManyInboxWarning());
        header.delayedInbox = nullChecker(CRC_CaseDataAccess.howManyInboxDelayed());
		header.fup = nullChecker(CRC_CaseDataAccess.howManyOpenCasesAreFUP());

		return header;
	}
	public ActionIndicatorHeader getActionIndicatorsHeader(){
		ActionIndicatorHeader header = new ActionIndicatorHeader();

		header.total = nullChecker(CRC_CaseDataAccess.howManyOpenCasesAreFUP());
		header.nextDay = nullChecker(CRC_CaseDataAccess.howManyNextActionsFuture());
		header.today = nullChecker(CRC_CaseDataAccess.howManyNextActionsToday());
		header.expired = nullChecker(CRC_CaseDataAccess.howManyNextActionsExpired());
		header.incomingEmail = nullChecker(CRC_CaseDataAccess.howManyCasesIncomingEmail());
		header.awaitingCustomerResponse = nullChecker(CRC_CaseDataAccess.howManyCasesAwaitingCustomerResponse());

		return header;
	}

    // Method that creates and fills the structure with the TOTAL, AOG NFO, AOG EXP and SCHEDULE cards information.
	public IndicatorBodyDTO getCardDataByPriority(String priority){
        String reason;
        String status;
        Integer quantity;
		Decimal Total = 0;
		Decimal onTime = 0;
        Map<String,Integer> structuredData = new Map<String,Integer>();
        List<AggregateResult> cardData = CRC_IndicatorBodyQuery.getTotalCardDataByPriority(priority);
        for (AggregateResult data : cardData){
            reason = (String) data.get('Reason');
            status = (String) data.get('SLA_report_status__c');
            quantity = (Integer) data.get('expr0');
            structuredData.put(Reason+' '+status,quantity);
        }
        List<AggregateResult> cardTopData = CRC_IndicatorBodyQuery.getAllTotalCardDataByPriority(priority);
        for (AggregateResult data : cardTopData){
            Total += (Integer) data.get('expr0');
            status = (String) data.get('SLA_report_status__c');
            if ((status == '1. Green')||(status == '2. Yellow')){
                onTime += (decimal) data.get('expr0');
            }
        }
        IndicatorBodyDTO infos = fillCardInformations(structuredData);
        infos.allCaseMonthlyCounter = (integer) Total;
        if (Total==0){
            infos.onTime = '100%';
        } else{
            infos.onTime = String.valueOf(math.round(100*onTime/Total))+'%';
        }
		return infos;
    }

    // Method that creates and fills the structure with the ECIP, SB and RECONFIG cards information.
    public IndicatorBodyDTO getCardDataByType(String type){
        String reason;
        String status;
        Integer quantity;
		Decimal Total = 0;
		Decimal onTime = 0;
        Map<String,Integer> structuredData = new Map<String,Integer>();
        List<AggregateResult> cardData = CRC_IndicatorBodyQuery.getTotalCardDataByType(type);
        for (AggregateResult data : cardData){
            reason = (String) data.get('Reason');
            status = (String) data.get('SLA_report_status__c');
            quantity = (Integer) data.get('expr0');
            structuredData.put(Reason+' '+status,quantity);
        }
        List<AggregateResult> cardTopData = CRC_IndicatorBodyQuery.getAllTotalCardDataByType(type);
        for (AggregateResult data : cardTopData){
            Total += (Decimal) data.get('expr0');
            status = (String) data.get('SLA_report_status__c');
            if ((status == '1. Green')||(status == '2. Yellow')){
                onTime += (Decimal) data.get('expr0');
            }
        }
        IndicatorBodyDTO infos = fillCardInformations(structuredData);
        infos.allCaseMonthlyCounter = (Integer) Total;
        if (Total==0){
            infos.onTime = '100%';
        } else{
            infos.onTime = String.valueOf(math.round(100*onTime/Total))+'%';
        }
		return infos;
    }
		// Method that creates and fills the structure with the cases sorted by Status.
    public IndicatorBodyDTO getCardDataByStatus(String status){
        String reason;
        String slaStatus;
        Integer quantity;
		Decimal Total = 0;
		Decimal onTime = 0;
        Map<String,Integer> structuredData = new Map<String,Integer>();
        List<AggregateResult> cardData = CRC_IndicatorBodyQuery.getTotalCardDataByStatus(status);
        for (AggregateResult data : cardData){
            reason = (String) data.get('Reason');
            slaStatus = (String) data.get('SLA_report_status__c');
            quantity = (Integer) data.get('expr0');
            structuredData.put(Reason+' '+slaStatus,quantity);
        }

        IndicatorBodyDTO infos = fillCardInformations(structuredData);


		return infos;
    }

    // Method that creates and fills the structure with the ATD card information.
    public IndicatorBodyDTO getATDData(){
        String reason;
        String status;
        Integer quantity;
		Decimal Total = 0;
		Decimal onTime = 0;
        Map<String,Integer> structuredData = new Map<String,Integer>();
        List<AggregateResult> cardData = CRC_IndicatorBodyQuery.getATDCardData();
        for (AggregateResult data : cardData){
            reason = (String) data.get('Reason');
            status = (String) data.get('SLA_report_status__c');
            quantity = (Integer) data.get('expr0');
            structuredData.put(Reason+' '+status,quantity);
        }
        List<AggregateResult> cardTopData = CRC_IndicatorBodyQuery.getAllATDCardData();
        for (AggregateResult data : cardTopData){
            Total += (Decimal) data.get('expr0');
            status = (String) data.get('SLA_report_status__c');
            if ((status == '1. Green')||(status == '2. Yellow')){
                onTime += (Decimal) data.get('expr0');
            }
        }
        IndicatorBodyDTO infos = fillCardInformations(structuredData);
        infos.allCaseMonthlyCounter = (Integer) Total;
        if (Total==0){
            infos.onTime = '100%';
        } else{
            infos.onTime = String.valueOf(math.round(100*onTime/Total))+'%';
        }
		return infos;
    }

    // Auxiliary method that set NULL values to zero.
    public integer nullChecker(Integer value){
        if (value == NULL){
            value = 0;
        }
        return value;
    }

    // Auxiliary method that set groups the query results into the categories RFQ-PO and CARE and calculates the total.
    public IndicatorBodyDTO fillCardInformations(Map<String,Integer> data){
        IndicatorBodyDTO infos = new IndicatorBodyDTO();
        infos.onTime_RFQ_PO_Counter = nullChecker(data.get('RFQ 1. Green')) + nullChecker(data.get('PO 1. Green')) + nullChecker(data.get('Invoice 1. Green'));
        infos.warning_RFQ_PO_Counter = nullChecker(data.get('RFQ 2. Yellow')) + nullChecker(data.get('PO 2. Yellow')) + nullChecker(data.get('Invoice 2. Yellow'));
        infos.delayed_RFQ_PO_Counter = nullChecker(data.get('RFQ 3. Red')) + nullChecker(data.get('PO 3. Red')) + nullChecker(data.get('Invoice 3. Red'));
        infos.RFQ_PO_Counter = infos.onTime_RFQ_PO_Counter + infos.warning_RFQ_PO_Counter + infos.delayed_RFQ_PO_Counter;
        infos.onTime_care_Counter = nullChecker(data.get('Care 1. Green')) + nullChecker(data.get('RMA 1. Green')) + nullChecker(data.get('Follow Up 1. Green'));
        infos.warning_care_Counter = nullChecker(data.get('Care 2. Yellow')) + nullChecker(data.get('RMA 2. Yellow')) + nullChecker(data.get('Follow Up 2. Yellow'));
        infos.delayed_care_Counter = nullChecker(data.get('Care 3. Red')) + nullChecker(data.get('RMA 3. Red')) + nullChecker(data.get('Follow Up 3. Red'));
        infos.care_Counter = infos.onTime_care_Counter + infos.warning_care_Counter + infos.delayed_care_Counter;
        infos.onTimeMilestonesCounter = infos.onTime_RFQ_PO_Counter + infos.onTime_care_Counter;
        infos.warningMilestonesCounter = infos.warning_RFQ_PO_Counter + infos.warning_care_Counter;
        infos.delayedMilestonesCounter = infos.delayed_RFQ_PO_Counter + infos.delayed_care_Counter;
        infos.caseMonthlyCounter = infos.onTimeMilestonesCounter + infos.warningMilestonesCounter + infos.delayedMilestonesCounter;
        return infos;
    }
}