/**
* @description: FO_FupRepository test class.
**/
@isTest
public class FO_FupRepositoryTest {
    @isTest static void testCanGetFupById(){
        Fup__c expectedFup = new FO_FupTestDataBuilder().build();
        
        
        Fup__c actualFup = FO_FupRepository.getById(expectedFup.Id);
        
        
        System.assertEquals(expectedFup.Id, actualFup.Id);
    }
    
    @isTest static void testCanGetPublishedUpdates(){
        FUP_Update__c expectedFupUpdate = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .build();
                
        
        List<FUP_Update__c> fupUpdates = FO_FupRepository.getPublicUpdatesByFup(
            expectedFupUpdate.FUP__c
        );	
        FUP_Update__c actualFupUpdate = fupUpdates.get(0);

        System.assertEquals(expectedFupUpdate.Id, actualFupUpdate.Id);
    }
    
    @isTest static void testCanGetPublishedUpdatesWithFupList(){
        FUP_Update__c expectedFupUpdate1 = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .build();
        
        FUP_Update__c expectedFupUpdate2 = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .build();
                
        List<Id> fupIds = new List<Id>{
            expectedFupUpdate1.FUP__c, 
            expectedFupUpdate2.FUP__c
        };
        
        List<FUP_Update__c> updates = FO_FupRepository.getPublicUpdatesByFup(
            fupIds
        );
        System.assert(!updates.isEmpty());
        
        
        List<Id> updateIds = new List<Id>();
        for(FUP_Update__c u : updates){
            updateIds.add(u.Id);
        }
                
        System.debug('expectedFupUpdate1 = ' + expectedFupUpdate1);
        System.debug('expectedFupUpdate2 = ' + expectedFupUpdate2);
		System.debug('updateIds = ' + updateIds);
        
        System.assert(
            updateIds.contains(expectedFupUpdate1.Id)
        );
        System.assert(
            updateIds.contains(expectedFupUpdate2.Id)
        );
    }
    
    /**
    * @description Test if it does not return unpublished updates on 
    * getPublicUpdatesByFup method.
    **/
    @isTest static void testIfNoUnpublishedUpdatesAreReturned(){
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
        FUP_Update__c expectedFupUpdate = new FO_FupUpdateTestDataBuilder()
            .withParentFup(fup)
            .withStatus('Unpublished')
            .build();
                
        
        List<FUP_Update__c> fupUpdates = FO_FupRepository.getPublicUpdatesByFup(
            expectedFupUpdate.FUP__c
        );	
        
        
        System.assertEquals(fupUpdates.size(), 0);
    }
    
    /**
    * @description Test if it does not return published updates from unpublished
    * fups on getPublicUpdatesByFup method.
    **/
    @isTest static void testIfCannotGetPublishedUpdateFromAnUnpublishedFup(){
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Unpublished')
            .build();
        FUP_Update__c fupUpdate = new FO_FupUpdateTestDataBuilder()
            .withStatus('published')
            .withParentFup(fup)
            .build();
                
        
        List<FUP_Update__c> fupUpdates = FO_FupRepository.getPublicUpdatesByFup(
            fup.Id
        );	
        
        
        System.assertEquals(fupUpdates.size(), 0);
    }
   
    @isTest static void testIfOnlyPublishedFupsAreReturned(){
        FUP__c unpublishedFup = new FO_FupTestDataBuilder()
            .withPublishStatus('Unpublished')
            .build();
        FUP__c publishedFup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
        
        List<FUP__c> fups = FO_FupRepository.getPublishedFups();
        
        
        for(FUP__c fup : fups){
            Boolean isPublished = fup.Publish_Status__c == 'Published';
            System.assert(isPublished);
        }
    }
   
    /**
    * @description Test if Can filter E-jet Models by Its Family.
    **/
    @isTest static void testCanGetModelsByFromEjet(){
        String aircraftFamily = 'EJet';
        Set<String> expectedModels = FO_FupRepository.EJET_MODELS;
        Set<String> actualModels = FO_FupRepository.getModelsNameByFleetType(
            new List<String>{aircraftFamily}
        );
        System.assert(actualModels.equals(expectedModels));
    }
    
    /**
    * @description Test If Can filter ERJ Models by Its Family.
    **/
    @isTest static void testCanGetModelsByFromErj(){
        String aircraftFamily = 'ERJ';
        Set<String> expectedModels = FO_FupRepository.ERJ_MODELS;
        Set<String> actualModels = FO_FupRepository.getModelsNameByFleetType(
            new List<String>{aircraftFamily}
        );
        System.assert(actualModels.equals(expectedModels));
    }
    
    /**
    * @description Test If Can filter E2 Models by Its Family.
    **/
    @isTest static void testCanGetModelsByFromE2(){
        String aircraftFamily = 'E2';
        Set<String> expectedModels = FO_FupRepository.E2_MODELS;
        Set<String> actualModels = FO_FupRepository.getModelsNameByFleetType(
            new List<String>{aircraftFamily}
        );
        System.assert(actualModels.equals(expectedModels));
    }

    /**
    * @description Test If Can filter All Models by Its Family.
    **/
    @isTest static void testCanGetAllModelsByFamily(){
        String aircraftFamily = 'E2';
        
        Set<String> models = FO_FupRepository.getModelsNameByFleetType(
            new List<String>{
                'EJet',
                'E2',
                'ERJ'
            }
        );
        System.assert(models.containsAll(FO_FupRepository.E2_MODELS));
        System.assert(models.containsAll(FO_FupRepository.EJET_MODELS));
        System.assert(models.containsAll(FO_FupRepository.ERJ_MODELS));
    }
    
    /**
    * @description Test If It is possible to get on N last days Updates.
    **/
    @isTest static void testCanFilterPublicUpdatesByNMonths(){
        Integer N = 1;
        List<String> aircraftFamilies = new List<String>{'EJet'};
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        FUP_Update__c expectedUpdate = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .withParentFup(fup)
            .withPublishDate(System.today().addMonths(-N))
            .build();
        

        List<Fup_Update__c> actualUpdates = FO_FupRepository.getLastNMonthsPublicUpdatesByFleetFamily(N, aircraftFamilies);
        List<Id> updatesIds = new List<Id>();
        for(Fup_Update__c c : actualUpdates){
            updatesIds.add(c.Id);
        }
                
        System.assert( updatesIds.contains(expectedUpdate.Id) );
    }
    
    /**
    * @description Test If Only updates published in N last days interval are
    * returned.
    **/
    @isTest static void testIfNoUpdateOlderThanNMonthsAreReturned(){
        Integer N = 1;
        List<String> aircraftFamilies = new List<String>{'EJet'};
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        FUP_Update__c unexpectedUpdate = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .withParentFup(fup)
            .withPublishDate(System.today())
            .build();
        
        
        List<Fup_Update__c> actualUpdates = FO_FupRepository.getLastNMonthsPublicUpdatesByFleetFamily(N, aircraftFamilies);
                
        System.assert(actualUpdates.isEmpty());
    }
    
    
    /**
    * @description Test if only updates published in N last days interval are
    * returned.
    **/
    @isTest static void testIfUnpublishedUpdatesAreNotReturned(){
        Integer N = 1;
        List<String> aircraftFamilies = new List<String>{'EJet'};
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
        FUP_Update__c unexpectedUpdate = new FO_FupUpdateTestDataBuilder()
            .withParentFup(fup)
            .withPublishDate(System.today().addMonths(-N))
            .build();
        
        
        List<Fup_Update__c> actualUpdates = FO_FupRepository.getLastNMonthsPublicUpdatesByFleetFamily(N, aircraftFamilies);
        
        
        System.assert(actualUpdates.isEmpty());
    }
    
	/**
    * @description Test if the method can format the string list inputted to be used
    * as a filter in a SOQL query.
    **/
    @isTest static void testCanBuildAircraftModelFilterStringProperly(){
        Set<String> aircraftFamilies = new Set<String>{'a', 'b', 'c'};
        String expectedString = ' \'a\' , \'b\' , \'c\' ';
        
        String actualString = FO_FupRepository.buildAircraftFamilyFilter(aircraftFamilies);
        
        System.assertEquals(expectedString, actualString);
    }   
}