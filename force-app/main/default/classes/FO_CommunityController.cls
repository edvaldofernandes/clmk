// Upon adding new tabs, check listAppTabs() instructions

public class FO_CommunityController {    
    public List<PublicComment> casePublicComment {get;set;}
    public List<EmailMessage> caseEmails {get;set;}
    public List<Attachment> caseAttachment {get;set;}
    public List<TabLinks> appTabs {get;set;}
    public List<Case> cases {get;set;}
    public List<Case> casesDefault {get;set;}
    public List<Case> resultSearch {get;set;}
    public List<Case> caseDetailsComments {get;set;}
    public List<Case> caseDetailsEmails {get;set;}
    public List<Case> caseDetailsAttachments {get;set;}
    public List<Case> casesToShow{get;set;}
    public Case caseDetails {get;set;}
    public Contact[] contact;
    public Account account{get;set;}
    public Boolean DisplayCaseDetails{get;set;}
    public Boolean DisplayCasesTable{get;set;}
    public Boolean DisplaySearchBar{get;set;}
    public Boolean DisplayFilterBar{get;set;}
    public Boolean DisplaySearchResults{get;set;}
    public Boolean DisplayCompanyFilter{get;set;}
    public Boolean hasComments{get;set;} 
    public Boolean hasEmails{get;set;}
    public Boolean hasSearchResults{get;set;}
    public Boolean hasAttachments{get;set;}
    public Boolean emailSenderIsEmbraer{get;set;}
    public Integer totalCasesCount{get;set;}
    public Integer parsedCasesCount{get;set;}
    public Integer displayCasesLimitSize = 20; //max cases to be shown at a time
    public Integer currentPaginationNumber{get;set;}
    public Integer totalPaginationNumber{get;set;}
    public String caseID{get;set;}
    public String searchString{get;set;}
    public String selectedStatusFilter {get;set;}
    public String selectedCompanyFilter {get;set;}
    public String newissuelink {get;set;}
    public String accountDomainsDefault {get;set;}
    public String accountDomains {get;set;}
    
    //Wrappers for Tabs, Public Comments within a Case
    public class TabLinks {
		public String linkname {
			get {return linkname; }
			set {linkname = value; }						
		}
		public String linkurl {
			get {return linkurl; }
			set {linkurl = value; }						
		}	
        public String pagename {
			get {return pagename; }
			set {pagename = value; }						
		}
	}
    
    public class PublicComment {
		public String commentText {
			get {return commentText; }
			set {commentText = value; }						
		}	
	}
    
    //Constructor
    public FO_CommunityController(){
        resetDisplayState();
        //Get tabs on myOpereng App and fill List<TabLinks> appTabs
        listAppTabs();
        //Get list of cases
        listCases();
    }
    
    //Resets display blocks for Case Page to initial state
    public void resetDisplayState(){
        toggleDisplayPageBlock(true, true, false, false, true);      
    }
    
    //Toggle display blocks for Case Page
    public void toggleDisplayPageBlock(Boolean searchBar, Boolean casesTable, Boolean caseDetails, Boolean SearchResults, Boolean filterBar){
		DisplaySearchBar = searchBar;
        DisplayFilterBar = filterBar;
        DisplayCasesTable = casesTable;
		DisplayCaseDetails = caseDetails; 
        DisplaySearchResults = searchResults;
    }
    
    //List cases based on the Company assigned to User
    public void listCases(){
        contact = [SELECT AccountId FROM Contact WHERE Email =: UserInfo.getUserEmail()];
        if(contact.size()>0){           
            account = [SELECT Name,Id,Geographical_Area__c, FlightOps_Account_Domains__c FROM Account WHERE Account.Id =: contact[0].AccountId];
            accountDomainsDefault = account.FlightOps_Account_Domains__c;
            accountDomains = accountDomainsDefault;
            cases = [SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description, FlightOps_Last_Email_Received__c FROM Case WHERE (Case.AccountId =: contact[0].AccountId AND Customer_Visible__c = true AND Case.Status != 'Archived' AND RecordType.Name = 'FlightOps Case Record Type') ORDER BY Status ASC, FlightOps_Last_Email_Received__c DESC, CreatedDate DESC];
            casesDefault = cases;
            setupPagination(cases);
            if(account.Name.toLowerCase().normalizeSpace().contains('embraer')){
                DisplayCompanyFilter = Boolean.valueOf('true');  
            } else {
                DisplayCompanyFilter = Boolean.valueOf('false');
            }
        }
    }
    
    //Get details for a selected case
    //and Toggle Page Blocks (toggleDisplayPageBlock) to show information instead of the case list
    public void getCaseDetails(String caseID){
        	caseDetails = [SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description FROM Case WHERE Case.Id =: caseID  AND RecordType.Name = 'FlightOps Case Record Type'];            
            getCaseComments(caseID);
            getCaseEmails(caseID);
        	getCaseAttachments(caseID);
            toggleDisplayPageBlock(false, false, true, false, false);
    }
    
    //Get Comments for a selected case
    public void getCaseComments(String caseID){
            //Getting Comments for Case
            caseDetailsComments = [SELECT (SELECT Id, ParentId, IsPublished, CommentBody, CreatedById, CreatedDate, SystemModstamp, LastModifiedDate, LastModifiedById, IsDeleted FROM CaseComments WHERE isPublished = true AND isDeleted = false) FROM Case WHERE Case.Id =: caseID  AND RecordType.Name = 'FlightOps Case Record Type'];
            casePublicComment = new List<PublicComment>();        
            for(CaseComment comm : caseDetailsComments[0].CaseComments){
                PublicComment p = new PublicComment();
                p.commentText = comm.CommentBody;
                casePublicComment.add(p);
            }
            if(casePublicComment.size() == 0){
                hasComments = Boolean.valueOf('false');  
            } else {
                hasComments = Boolean.valueOf('true');  
            }
    }
    
    //Get Emails for a selected case
    public void getCaseEmails(String caseID){
        //Getting Emails for Case
        caseDetailsEmails = [SELECT (SELECT Id, ParentId, HtmlBody, TextBody, Subject, ToAddress, FromName, FromAddress, MessageDate, IsDeleted, BccAddress, CcAddress FROM EmailMessages WHERE isDeleted = false ORDER BY MessageDate DESC) FROM Case WHERE Case.Id =: caseID  AND RecordType.Name = 'FlightOps Case Record Type' ];
        caseEmails = new List<EmailMessage>();
        List<String> domains;
        if(accountDomains != NULL){
        	domains = accountDomains.split('@');
        }
        Boolean containsAddress;
        for(EmailMessage eMsg : caseDetailsEmails[0].EmailMessages){
            if(account != NULL){
                if(account.Name.toLowerCase().normalizeSpace().contains('embraer')){
                    containsAddress = Boolean.valueOf('true');
                } else {
                    containsAddress = Boolean.valueOf('false');
                    if(domains != NULL){
                        for (String eDomain: domains){
                            if(eDomain != ''){
                                if((eMsg.BccAddress!= NULL) && (eMsg.BccAddress.contains(eDomain))){
                                    containsAddress = Boolean.valueOf('true');
                                } else {
                                    if((eMsg.CcAddress!= NULL) && (eMsg.CcAddress.contains(eDomain))){
                                        containsAddress = Boolean.valueOf('true');
                                    } else {
                                        if((eMsg.FromAddress!= NULL) && (eMsg.FromAddress.contains(eDomain))){
                                            containsAddress = Boolean.valueOf('true');
                                        } else {
                                            if((eMsg.ToAddress!= NULL) && (eMsg.ToAddress.contains(eDomain))){
                                            	containsAddress = Boolean.valueOf('true');
                                        	}
                                        }
                                    }
                                }  
                            }       
                        }
                    }
                }
                
                if(containsAddress){
                    if((eMsg.FromAddress!= NULL) && (eMsg.FromAddress.contains('embraer'))){
                    	emailSenderIsEmbraer = Boolean.valueOf('true');
                    } else {
                        emailSenderIsEmbraer = Boolean.valueOf('false');
                    }
                    EmailMessage e = new EmailMessage();
                    e = eMsg;
                    caseEmails.add(e);
                }                
       		}
        }
        
        if(caseEmails.size() == 0){
            hasEmails = Boolean.valueOf('false');  
        } else {
            hasEmails = Boolean.valueOf('true');  
        }
    }
    
    //Get Attachments for a selected case
    public void getCaseAttachments(String caseID){
        //Getting Attachments for Case
            caseDetailsAttachments = [SELECT (SELECT Id, Name FROM Attachments WHERE isDeleted = false) FROM Case WHERE Case.Id =: caseID  AND RecordType.Name = 'FlightOps Case Record Type' ];
            caseAttachment = new List<Attachment>();
            for(Attachment cAttach : caseDetailsAttachments[0].Attachments){
                Attachment at = new Attachment();
                at = cAttach;
                caseAttachment.add(at);
            }
            if(caseAttachment.size() == 0){
                hasAttachments = Boolean.valueOf('false');  
            } else {
                hasAttachments = Boolean.valueOf('true');  
            }
    }
    
    //Get Case ID selected by the user on the case list
    //With Case ID get additional details on selected case
    public PageReference setupCaseDetails(){
        String caseIDChosen = caseID;
        getCaseDetails(caseIDChosen);
        return null;
    }
/*   
    public void searchTest(String searchString){
        List<List<sObject>> searchList = Search.query('FIND \''+ String.escapeSingleQuotes(searchString) + '*\' IN ALL FIELDS RETURNING Case(Id, Status, CreatedDate, Subject WHERE (RecordType.Name = \'FlightOps Case Record Type\'))');
    	List<Case> resultSearchTemp = new List<Case>();
        resultSearch = new List<Case>();
        resultSearchTemp = ((List<Case>)searchList[0]);
        hasSearchResults = Boolean.valueOf('true');
        //Remove Duplicate Results
        Set<Case> duplicateSet = new Set<Case>();
        for (Case r : resultSearchTemp) {
            if (duplicateSet.add(r)) {
                resultSearch.add(r);
            }
        }
        toggleDisplayPageBlock(true, false, false, true, false);
    }
  */  
    //Perform search for cases based on search string (subject, description, comments)
    public void searchCases(String searchString){
        List<Case> resultSearchTemp = new List<Case>();
        resultSearch = new List<Case>();
        searchString = searchString.toLowerCase().normalizeSpace();
        hasSearchResults = Boolean.valueOf('false');
        
        //Iterate through cases
        for(Case c : cases){
            Case temp = new Case();
            //Search for Subject
            if(c.Subject.toLowerCase().normalizeSpace().contains(searchString)){
                System.debug('Match found on subject.');
                temp = c;
                hasSearchResults = Boolean.valueOf('true');
                resultSearchTemp.add(temp);
            }  
            
            //Search for Description
            if(string.isNotBlank(c.Description)){
                if(c.Description.toLowerCase().normalizeSpace().contains(searchString)){
                    System.debug('Match found on description.');
                    temp = c;
                    hasSearchResults = Boolean.valueOf('true');
                    resultSearchTemp.add(temp);
                }
            }
            
            //Search for Comment
            caseDetailsComments = [SELECT (SELECT Id, ParentId, IsPublished, CommentBody, CreatedById, CreatedDate, SystemModstamp, LastModifiedDate, LastModifiedById, IsDeleted FROM CaseComments WHERE isPublished = true AND isDeleted = false) FROM Case WHERE Case.Id =: c.Id  AND RecordType.Name = 'FlightOps Case Record Type'];
            for(CaseComment comm : caseDetailsComments[0].CaseComments){
                PublicComment p = new PublicComment();
                p.commentText = comm.CommentBody;
                if(comm.CommentBody.toLowerCase().normalizeSpace().contains(searchString)){
                    System.debug('Match found on comment.');
                    temp = c;
                    hasSearchResults = Boolean.valueOf('true');
                    resultSearchTemp.add(temp);
                }
            }
            
            //Search for Email
            caseDetailsEmails = [SELECT (SELECT Id, ParentId, HtmlBody, TextBody, Subject, FromName, FromAddress, MessageDate, IsDeleted, BccAddress, CcAddress FROM EmailMessages WHERE isDeleted = false ORDER BY MessageDate DESC) FROM Case WHERE Case.Id =: c.Id  AND RecordType.Name = 'FlightOps Case Record Type'];
            for(EmailMessage e : caseDetailsEmails[0].EmailMessages){
                EmailMessage emailTemp = new EmailMessage();
                emailTemp.TextBody = e.TextBody;
                if(emailTemp.TextBody.toLowerCase().normalizeSpace().contains(searchString)){
					System.debug('Match found on email.');
                    temp = c;
                    hasSearchResults = Boolean.valueOf('true');
                    resultSearchTemp.add(temp);                	
            	}
            }  
        }
        
        //Remove Duplicate Results
        Set<Case> duplicateSet = new Set<Case>();
        for (Case r : resultSearchTemp) {
            if (duplicateSet.add(r)) {
                resultSearch.add(r);
            }
        }
        toggleDisplayPageBlock(true, false, false, true, false);
    }
    
    //Get parameter typed by user on page
    //perform a search based on the string provided
    public PageReference startCaseSearch(){
        if(string.isNotBlank(searchString)){
         searchCases(searchString);
        }
        return null;
    }
    
    //PageReference to redirect to New Issues Tab
    public PageReference goToNewIssues() {
        PageReference pageRef = new PageReference(newissuelink);
        pageRef.setRedirect(true);
        return pageRef;
    }  
    
    //Get parameters to filter list
    public PageReference startCaseFilter(){
        String queryString = 'SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description, FlightOps_Last_Email_Received__c FROM Case WHERE Customer_Visible__c = true AND Case.Status != \'Archived\' AND RecordType.Name = \'FlightOps Case Record Type\'';
        if(selectedStatusFilter != 'Status'){
            if(selectedStatusFilter == 'Open'){
                queryString = queryString + ' AND ((Case.Status = \'New\') OR (Case.Status = \'In Progress\'))';
            } else {
            	queryString = queryString + ' AND Case.Status = \''+ selectedStatusFilter +'\'';
            }
        }
        if(selectedCompanyFilter != NULL){
            if(selectedCompanyFilter != 'Company'){
            System.debug(selectedCompanyFilter);
            Account accountTemp = [SELECT Name,Id,Geographical_Area__c, FlightOps_Account_Domains__c FROM Account WHERE Account.Id =: selectedCompanyFilter];
            if(accountTemp.FlightOps_Account_Domains__c != NULL){
                accountDomains = accountTemp.FlightOps_Account_Domains__c;
            } else {
                accountDomains = '';
            }
            queryString = queryString + ' AND Case.AccountId = \''+ selectedCompanyFilter +'\'';
            } else { 
                accountDomains = accountDomainsDefault;
                contact = [SELECT AccountId FROM Contact WHERE Email =: UserInfo.getUserEmail()];
                if(contact.size() > 0){
                	selectedCompanyFilter = contact[0].AccountId;
                	queryString = queryString + ' AND Case.AccountId = \''+ selectedCompanyFilter +'\'';
                }
            }
        } else {
            accountDomains = accountDomainsDefault;
            contact = [SELECT AccountId FROM Contact WHERE Email =: UserInfo.getUserEmail()];
            if(contact.size() > 0){
            	selectedCompanyFilter = contact[0].AccountId;
            	queryString = queryString + ' AND Case.AccountId = \''+ selectedCompanyFilter +'\'';
            }
        }
		queryString = queryString + 'ORDER BY Status ASC, FlightOps_Last_Email_Received__c DESC, CreatedDate DESC';
        cases = Database.query(queryString);
        setupPagination(cases);
        return null;
    }
    
    //Select list for status
    public List<SelectOption> getListOfStatusFilter(){
        List<SelectOption> StatusFilterList = new List<SelectOption>();
        StatusFilterList.add(new SelectOption('Status' ,'Status'));
        StatusFilterList.add(new SelectOption('Open' , 'Open'));
        StatusFilterList.add(new SelectOption('Closed' , 'Answered'));
        
        return StatusFilterList;
    }
       
    //Select list for company
    public List<SelectOption> getListOfCompanyFilter(){
        List<Account> acc = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Airline' AND (Company_Status__c = 'In EIS Process' OR Company_Status__c = 'Active') ORDER BY Name LIMIT 1000] ;
        List<SelectOption> CompanyFilterList = new List<SelectOption>();
        CompanyFilterList.add(new SelectOption('Company' ,'Company'));
        for(Account a: acc){
            CompanyFilterList.add(new SelectOption(a.Id , a.Name));
        }
        return CompanyFilterList;
    }
    
    //PageReference to redirect to New Issues Tab
    public PageReference resetFilters() {
        selectedCompanyFilter = 'Company';
        selectedStatusFilter = 'Status';
        if(casesDefault != NULL){
            cases = casesDefault;
        	setupPagination(cases);
        }
        if(accountDomainsDefault != NULL){
            accountDomains = accountDomainsDefault;
        }
        return null;
    }
    
    //Pagination for cases
    public void setupPagination(List<Case> c){
        casesToShow = new List<Case>();
        parsedCasesCount = 0;
        //Check number of cases for pagination
        //setup pagination
            totalCasesCount = c.size();
        	totalPaginationNumber = (Integer) Math.ceil(Decimal.valueOf(totalCasesCount).divide(displayCasesLimitSize, 1));
        	currentPaginationNumber = 1;
            if((parsedCasesCount+displayCasesLimitSize) <= totalCasesCount){
                for(Integer i=0;i<displayCasesLimitSize;i++){
                    casesToShow.add(c.get(i));
                }
            }else{
                for(Integer i=0;i<totalCasesCount;i++){
                    casesToShow.add(c.get(i));
                }
            }
    }
    
    public void beginning(){ 
        casesToShow.clear();
        parsedCasesCount = 0;
        currentPaginationNumber = 1;
        if((parsedCasesCount + displayCasesLimitSize) <= totalCasesCount){
            for(Integer i=0;i<displayCasesLimitSize;i++){
                casesToShow.add(cases.get(i));
            }     
        } else{
            for(Integer i=0;i<totalCasesCount;i++){
                casesToShow.add(cases.get(i));
            }         
        }      
    }
    
    public void next(){
        casesToShow.clear();
        currentPaginationNumber++;
        parsedCasesCount = parsedCasesCount + displayCasesLimitSize;
        
        if((parsedCasesCount + displayCasesLimitSize) <= totalCasesCount){
            for(Integer i = parsedCasesCount;i<(parsedCasesCount + displayCasesLimitSize);i++){
                casesToShow.add(cases.get(i));
            }
        } else{
            for(Integer i=parsedCasesCount;i<totalCasesCount;i++){
                casesToShow.add(cases.get(i));
            }
        }
    }
    
    public void previous(){
        casesToShow.clear();
        currentPaginationNumber--;
        parsedCasesCount = parsedCasesCount - displayCasesLimitSize;  
        for(Integer i=parsedCasesCount;i<(parsedCasesCount + displayCasesLimitSize); i++){
            casesToShow.add(cases.get(i));
        }
    }
    
    public void last (){
        casesToShow.clear();
        currentPaginationNumber = totalPaginationNumber;
        if(math.mod(totalCasesCount , displayCasesLimitSize) == 0){
            parsedCasesCount = displayCasesLimitSize * ((totalCasesCount/displayCasesLimitSize)-1);
        } else if (math.mod(totalCasesCount , displayCasesLimitSize) != 0){
            parsedCasesCount = displayCasesLimitSize * ((totalCasesCount/displayCasesLimitSize));
        }
        
        for(Integer i=parsedCasesCount-1;i<totalCasesCount-1;i++){
            casesToShow.add(cases.get(i));
        }  
    }
    
    public Boolean getEnableNext(){
        if((parsedCasesCount + displayCasesLimitSize) >= totalCasesCount )
            return false ;
        else
            return true ;
    }
    
    public Boolean getEnablePrevious(){
        if(parsedCasesCount == 0)
			return false ;
        else
            return true ;
    } 
    
    
    //Instructions when adding new tabs so it looks good on view 
    //Steps 1 and 2 should be done on the indicated area below (*)
    //1 - Add a case for the linkname with the desired name to show on menu (Apex currently doesn't support SWITCH CASE, which is why it currently uses IFs)
    //2 - Add a pagename equals to your Visualforce Page name, ex.: Page for displaying cases is FO_Community_View.vfp, so pagename is going to be FO_Community_View
    public void listAppTabs(){
            appTabs = new List<TabLinks>();
            // Get tab set describes for each app
            List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
            
            // Iterate through each tab set describe for each app and display the info
            for(DescribeTabSetResult tsr : tabSetDesc) {
                String appLabel = tsr.getLabel();
                System.debug('Label: ' + appLabel);
                System.debug('Logo URL: ' + tsr.getLogoUrl());
                System.debug('isSelected: ' + tsr.isSelected());
                String ns = tsr.getNamespace();
                if (ns == '') {
                    System.debug('The ' + appLabel + ' app has no namespace defined.');
                }
                else {
                    System.debug('Namespace: ' + ns);
                }
                
                // Display tab info for the myOpereng app
                // (*) Changes on LINKNAME and PAGENAME should be done on this area
                if (appLabel == 'myOpereng') {
                    List<Schema.DescribeTabResult> tabDesc = tsr.getTabs();
                    System.debug('-- Tab information for the myOpereng app --');
                    for(Schema.DescribeTabResult tr : tabDesc) {
                        TabLinks t = new TabLinks();
                        t.linkname = tr.getLabel();
                        t.linkurl = tr.getUrl();
                        if(t.linkname == 'myOpereng Cases'){
                            t.linkname = 'Cases';
                            t.pagename = 'FO_Community_View';
                        } else {
                            if(t.linkname == 'myOpereng New Issue'){
                                t.linkname = 'New Issue';
                                t.pagename = 'FO_MyOP_NewCases_View';
                                newissuelink = t.linkurl;
                            }
                        }
                        if(t.linkname != 'Home'){
                        	appTabs.add(t);
                        }
                    }
                }
            }
         
    }
    
}