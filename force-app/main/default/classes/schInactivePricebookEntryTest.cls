/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class schInactivePricebookEntry
* NAME: schInactivePricebookEntryTest.cls
* AUTHOR: LRSA                                                DATE: 02/10/2014
*
*******************************************************************************/

@isTest
private class schInactivePricebookEntryTest {

    static testMethod void myUnitTest() 
    {
      PriceBook2 lPB = SObjectInstanceTest.createPricebook2();
      lPB.Expiration_date__c = System.today().addDays( -1 );
      insert lPB;
      
      schInactivePricebookEntry sch = new schInactivePricebookEntry();
      
      Test.startTest();
        System.schedule( 'Inactivate PricebookEntry test', '0 0 1 * * ?', sch );
      Test.stopTest();
    }
}