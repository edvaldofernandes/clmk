public class RCPService {
    
    private List<RCP> rcpList;
    private List<RCP_FH_FC__c> rCPFhFcObjList;
    private List<Historico_ANV__c > historicoANVList;
    private List<RCP_MTBUR__c> rCPMTBURList;
    private List<RCP_problem__c> rcpProblemList;
    private List<RCP_Rem__c > rcp_RemList;
    private Map<String,String> accountIdMap;
    private Map<String,String> aircraftIdMap;
    private ServicesUtils serviceUtils;
    
    public RCPService(){
        
        serviceUtils  = new ServicesUtils();
    }
    
    public RCPService(List<RCP> rcpList){
        
        this.rcpList = rcpList;
        
        initListObject();
        initCommonsServices();
        
        parseToObject();
    }
    
    private void initListObject(){
        
        rCPFhFcObjList   = new List<RCP_FH_FC__c>();
        historicoANVList = new List<Historico_ANV__c >();
        rCPMTBURList     = new List<RCP_MTBUR__c>();
        rcpProblemList   = new List<RCP_problem__c>();
        rcp_RemList      = new List<RCP_Rem__c >();
    }
    
    private void initCommonsServices(){
        
        accountIdMap  = RcpRepository.buildMapWithAllAccount();
        aircraftIdMap = RcpRepository.buildMapWithAllAircraft();
        serviceUtils  = new ServicesUtils();
    }
    
    public void parseToObject(){
        
        for(RCP rcp : rcpList){
            
            parseToObjectFhFc(rcp);
            parseToObjectAircraftHistory(rcp);
            parseToObjectMTBUR(rcp);
            parseToObjectProblem(rcp);
            parseToObjectRemovels(rcp);
        }
        
        RcpTemplate.save(rCPFhFcObjList);
        RcpTemplate.save(historicoANVList);
        RcpTemplate.save(rCPMTBURList);
        RcpTemplate.save(rcpProblemList);
        RcpTemplate.save(rcp_RemList);

    }
    
    private void parseToObjectFhFc(RCP rcp){
        
        if(rcp.rCP_fhfc == null) return;
        
        for(RCP_fhfc rCP_fhfc : rcp.rCP_fhfc){
            
            RCP_FH_FC__c rCPFhFcObj = new RCP_FH_FC__c();
        	rCPFhFcObj.codigoProjeto__c          = rCP_fhfc.codigoProjeto;
        	rCPFhFcObj.dataReferencia__c         = rCP_fhfc.dataReferencia;
        	rCPFhFcObj.numeroSerieAeronave__c    = rCP_fhfc.numeroSerieAeronave;
        	rCPFhFcObj.numeroRegistroAeronave__c = rCP_fhfc.numeroRegistroAeronave;
        	rCPFhFcObj.codigoOperador__c         = rCP_fhfc.codigoOperador;
        	rCPFhFcObj.horaVooMes__c 			 = rCP_fhfc.horaVooMes;
        	rCPFhFcObj.ciclosDeVoo__c            = rCP_fhfc.ciclosDeVoo;
        	rCPFhFcObj.diasOperacionais__c       = rCP_fhfc.diasOperacionais;
        	rCPFhFcObj.horasVooAcumuladas__c     = rCP_fhfc.horasVooAcumuladas;
        	rCPFhFcObj.ciclosVooAcumulados__c    = rCP_fhfc.ciclosVooAcumulados;
        
        	String relatedId = aircraftIdMap.get(buildSerialNumber(rCPFhFcObj.codigoProjeto__c,String.valueOf(rCPFhFcObj.numeroSerieAeronave__c)));
        
        	if(relatedId != null)
            	rCPFhFcObj.Aircraft__c = relatedId;
        
        	rCPFhFcObjList.add(rCPFhFcObj);
        }
    }
    
    private void parseToObjectAircraftHistory(RCP rcp){
        
        if(rcp.rCP_HistAeronave == null) return;
        
        for(RCP_HistAeronave rCP_HistAeronave : rcp.rCP_HistAeronave){
            
            Historico_ANV__c historico_ANV = new Historico_ANV__c();
        	historico_ANV.codigoProjeto__c           = rCP_HistAeronave.codigoProjeto;
        	historico_ANV.numeroSerieAeronave__c	 = rCP_HistAeronave.numeroSerieAeronave;
        	historico_ANV.numeroRegistroAeronave__c  = rCP_HistAeronave.numeroRegistroAeronave;
        	historico_ANV.apu__c					 = rCP_HistAeronave.apu;
        	historico_ANV.dataEntregaOperador__c	 = rCP_HistAeronave.dataEntregaOperador;	
        	historico_ANV.dataUltimaAtualizacao__c   = rCP_HistAeronave.dataUltimaAtualizacao;
        	historico_ANV.motorUm__c				 = rCP_HistAeronave.motorUm;
        	historico_ANV.motorDois__c    			 = rCP_HistAeronave.motorDois;
        
        	String relatedId 						 = aircraftIdMap.get(buildSerialNumber(historico_ANV.codigoProjeto__c,String.valueOf(historico_ANV.numeroSerieAeronave__c)));
        
        	if(relatedId != null)
            	historico_ANV.Aircraft__c = relatedId;
                
        	historicoANVList.add(historico_ANV);
    	}
    }
    
    private void parseToObjectMTBUR(RCP rcp){
        System.debug(' MTBUR SIZE ...... ' + rcp.rcp_Mtbur);
        if(rcp.rcp_Mtbur == null) return;
        
        for(RCP_Mtbur rcp_Mtbur : rcp.rcp_Mtbur){
        	
            RCP_MTBUR__c rcpMTBUR = new RCP_MTBUR__c();
        	rcpMTBUR.AtaChapter__c            = rcp_Mtbur.ataChapter;
        	rcpMTBUR.family__c                = rcp_Mtbur.familia;
        	rcpMTBUR.Operador__c              = Decimal.valueOf(rcp_Mtbur.CodigoOperador);
        	rcpMTBUR.partNumber__c            = rcp_Mtbur.partNumber;
        	rcpMTBUR.GroupDescription__c 	  = rcp_Mtbur.operadorGroupDesc;
        	rcpMTBUR.PartNumberDescription__c = rcp_Mtbur.partNumberDesc;
        	rcpMTBUR.RemovalsL12M__c          = rcp_Mtbur.removalsL12M;
        	rcpMTBUR.createdDate__c           = Date.today();
        	String relatedId 				  = accountIdMap.get(String.valueOf(rcpMTBUR.Operador__c));
        
        	if(relatedId != null)
            	rcpMTBUR.Account__c = relatedId;
        
        	rCPMTBURList.add(rcpMTBUR);
        }
    }
    
    private void parseToObjectProblem(RCP rcp){
        
        if(rcp.rcp_Problem == null) return;
        
        for(RCP_Problem rcp_Problem : rcp.rcp_Problem){
        
            RCP_problem__c rcp_problem_c = new RCP_problem__c();
    		rcp_problem_c.ataChapter__c 			= rcp_Problem.ataChapter;
			rcp_problem_c.failCode__c 				= rcp_Problem.failCode;
			rcp_problem_c.aircraftSerialNumber__c 	= rcp_Problem.aircraftSerialNumber;
    		rcp_problem_c.codeProj__c 				= rcp_Problem.codeProj;
    		rcp_problem_c.problem__c 				= rcp_Problem.problem; 
			rcp_problem_c.ataSubChapter__c 			= rcp_Problem.ataSubChapter;
			rcp_problem_c.problemType__c 			= rcp_Problem.problemType;
			rcp_problem_c.eventDate__c 				= rcp_Problem.eventDate;
			rcp_problem_c.ProblemAndCorrection__c 	= rcp_Problem.problemCorrection;
    		rcp_problem_c.destiny__c 				= rcp_Problem.destiny;	
			rcp_problem_c.origin__c 				= rcp_Problem.origin;
			rcp_problem_c.failCodeDescription__c  	= rcp_Problem.failCodeDescription;
			rcp_problem_c.techicalIncident__c 		= rcp_Problem.techicalIncident;
    		rcp_problem_c.interruptionType__c 		= rcp_Problem.interruptionType;
    		rcp_problem_c.ifsd__c 					= rcp_Problem.ifsd;
    		rcp_problem_c.flightNumber__c 			= rcp_Problem.flightNumber;
    		rcp_problem_c.delayTime__c 				= rcp_Problem.delayTime; 
    		rcp_problem_c.register__c 				= rcp_Problem.register;
        	
            String relatedId						= aircraftIdMap.get(buildSerialNumber(rcp_problem_c.codeProj__c,String.valueOf(rcp_problem_c.aircraftSerialNumber__c)));
        
        	if(relatedId != null)
            	rcp_problem_c.Aircraft__c = relatedId;           
        
        	rcpProblemList.add(rcp_problem_c);
        
        }
    }
    
    private void parseToObjectRemovels(RCP rcp){
        
        if(rcp.rcp_Remocoes == null) return;
        
        for(RCP_Remocoes rcp_Remocoes : rcp.rcp_Remocoes){
            
            RCP_Rem__c rcp_Rem = new RCP_Rem__c();
        	rcp_Rem.numeroSerieAeronave__c 	= rcp_Remocoes.numeroSerieAeronave;
    		rcp_Rem.partNumberNoSerieOn__c 	= rcp_Remocoes.partNumberNoSerieOn;
    		rcp_Rem.partNumberNoSerieOff__c = rcp_Remocoes.partNumberNoSerieOff;
    		rcp_Rem.partNumber__c  			= rcp_Remocoes.partNumber;
    		rcp_Rem.codPnSubtt__c 			= rcp_Remocoes.codPnSubtt;
    		rcp_Rem.codigoProjeto__c 		= rcp_Remocoes.codigoProjeto;
    		rcp_Rem.dataEventRemocoes__c 	= rcp_Remocoes.dataEventRemocoes;
			rcp_Rem.razaoRemocao__c 		= rcp_Remocoes.razaoRemocao;
    		rcp_Rem.classificacao__c 		= rcp_Remocoes.classificacao;
        
        	String relatedId                = aircraftIdMap.get(buildSerialNumber(rcp_Rem.codigoProjeto__c,String.valueOf(rcp_Rem.numeroSerieAeronave__c)));
        
        	if(relatedId != null)
            	rcp_Rem.Aircraft__c = relatedId;
        
        	rcp_RemList.add(rcp_Rem);
        }
    }
    
    public String buildSerialNumber(String model, String serialNumber){
        System.debug('.... model ..... ' + model);
        String serialNum = serviceUtils.compleTeWithZero(serviceUtils.changeToAircrfatModelInSalesforce(model), serialNumber);
        
        return serialNum;
    }
}