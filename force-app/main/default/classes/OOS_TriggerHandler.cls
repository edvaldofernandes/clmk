public with sharing class OOS_TriggerHandler {
  public List<Out_of_service__c> newRecords = new List<Out_of_service__c>();
  public List<Out_of_service__c> oldRecords = new List<Out_of_service__c>();
  public boolean isInsert = false;
  public boolean isUpdate = false;
  public boolean isBefore = false;
  public boolean isAfter = false;
  public Boolean isTestClass = false;

  public final String email_footer =
    '<p>Kind Regards,</p>' +
    '<div><b>Fleet Performance, Reliability & Economics</b><br />' +
    'Services & Support<br />' +
    'Embraer Commercial Aviation<br />' +
    'P: +55 12 3313 0416 / 3294 / 1009 / 3145 / 7926 / 2835<br />' +
    'fleet.reliability@embraer.net.br</div>';
  public final String email_sender = 'fleet.reliability@embraer.net.br';
  public final String email_name_sender = 'Fleet Reliability';
  public final String email_subject = 'New OOS Data - SalesForce';

  public void onBeforeInsert() {
    Timezone tz = UserInfo.getTimeZone();
    Integer shiftedTime = Integer.valueOf(tz.getDisplayName().substring(4, 7));
    for (Out_of_Service__c oos : newRecords) {
      if (oos.OOS_Total_Time__c > 0) {
        oos.OOS_Total_Time__c = oos.OOS_Total_Time__c.setScale(2);
      }
      if (!String.isBlank(oos.Flight_Number__c)) {
        oos.Flight_Number__c = oos.Flight_Number__c.replace('.0', '');
      }
      if (!String.isBlank(oos.Log_Number__c)) {
        oos.Log_Number__c = oos.Log_Number__c.replace('.0', '');
      }
      if (!String.isBlank(String.valueOf(oos.Start_Time__c))) {
        oos.Start_Time__c = oos.Start_Time__c.addHours(shiftedTime);
      }
      if (!String.isBlank(String.valueOf(oos.Release_Time__c))) {
        oos.Release_Time__c = oos.Release_Time__c.addHours(shiftedTime);
      }
      if (
        (!String.isBlank(oos.EMIT_Comments__c) &&
        !String.isBlank(oos.RTS_Comments__c)) || oos.OOS_Total_Time__c < 4
      ) {
        oos.EMIT_RTS_Classified__c = true;
      }

      oos.Id_SN_Date__c =
        String.valueOf(oos.Serial_Number__c) +
        '&' +
        String.valueOf(oos.Start_Date__c) +
        String.valueOf(oos.Start_Time__c);
    }
  }

  public void onBeforeUpdate() {
    for (Out_of_service__c oos : newRecords) {
      if (
        ((!String.isBlank(oos.EMIT_Comments__c) &&
        !String.isBlank(oos.RTS_Comments__c)) || oos.OOS_Total_Time__c < 4) || isTestClass == true
      ) {
        oos.EMIT_RTS_Classified__c = true;
      } else {
        oos.EMIT_RTS_Classified__c = false;
      }
      if (String.valueOf(oos.Quality_Item_Classification__c) == 'N/A') {
        oos.Quality_Investigation_Status__c = 'Closed';
      }
    }
  }

  public void onAfterInsert() {
    Set<Id> acIds = new Set<Id>();
    Set<Id> oosIdsTechRep = new Set<Id>();
    Set<Id> oosIdsEMITRTS = new Set<Id>();
    Set<Id> oosIdsQuality = new Set<Id>();
    for (Out_of_Service__c oos : newRecords) {
      if (oos.Disregarded_Classification__c == false) {
        if (oos.TechRep_Classified__c == false) {
          acIds.add(oos.Serial_Number__c);
          oosIdsTechRep.add(oos.Id);
        }
        if (oos.EMIT_RTS_Classified__c == false) {
          oosIdsEMITRTS.add(oos.Id);
        }
        if (oos.Quality_Classified__c == false) {
          oosIdsQuality.add(oos.Id);
        }
      }
    }
    /*List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
    List<Messaging.SendEmailResult> results;
    if (oosIdsTechRep.size() > 0) {
      messages = this.sendEmailToTechRep(oosIdsTechRep, acIds);
    }
    if (oosIdsEMITRTS.size() > 0) {
      messages.add(
        this.sendEmailToProfiles(
          oosIdsEMITRTS,
          new List<String>{ 'Customer Data Reader Chatter Only' }
        )
      );
    }
    if (oosIdsQuality.size() > 0) {
      messages.add(
        this.sendEmailToProfiles(
          oosIdsQuality,
          new List<String>{ 'Quality for Chatter Only', 'Quality Full' }
        )
      );
    }

    results = Messaging.sendEmail(messages);*/
  }

  /*public Map<Id, Map<String, Object>> getMapBetweenAccountAndOOS(
    Set<Id> oosIds
  ) {
    Map<Id, Map<String, Object>> mapAccountAndOOS = new Map<Id, Map<String, Object>>();
    for (Out_of_Service__c oos : [
      SELECT Id, Serial_Number__r.Operator__c, Serial_Number__r.Operator__r.Name
      FROM Out_of_Service__c
      WHERE Id IN :oosIds
      ORDER BY Start_Date__c DESC
    ]) {
      Map<String, Object> mapValues = new Map<String, Object>();
      mapValues.put('oosId', oos.Id);
      mapValues.put('operatorName', oos.Serial_Number__r.Operator__r.Name);

      mapAccountAndOOS.put(oos.Serial_Number__r.Operator__c, mapValues);
    }
    return mapAccountAndOOS;
  }*/

  /*public List<Messaging.SingleEmailMessage> sendEmailToTechRep(
    Set<Id> oosIds,
    Set<Id> acIds
  ) {
    Map<Id, Map<String, Object>> mapAccountAndOOS = this.getMapBetweenAccountAndOOS(
      oosIds
    );
    Messaging.SingleEmailMessage message;
    List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

    for (AccountTeamMember member : [
      SELECT UserId, AccountId
      FROM AccountTeamMember
      WHERE
        AccountId IN (SELECT Operator__c FROM Aircraft__c WHERE Id IN :acIds)
        AND TeamMemberRole = 'Tech Rep'
    ]) {
      message = new Messaging.SingleEmailMessage();
      message.toAddresses = new List<Id>{ member.userId };
      message.subject = email_subject;

      message.setReplyTo(email_sender);
      message.setSenderDisplayName(email_name_sender);

      String dataUrl =
        Url.getSalesforceBaseUrl().toExternalForm() +
        '/lightning/r/Out_of_Service__c/' +
        mapAccountAndOOS.get(member.AccountId).get('oosId') +
        '/view';

      message.htmlBody =
        '<p>Dear TechRep,</p>' +
        '<p>New Out-of-Service Data from ' +
        mapAccountAndOOS.get(member.AccountId).get('operatorName') +
        ' is available in Salesforce.<br />' +
        'Please <a href="' +
        dataUrl +
        '">click here</a> to classify them.</p>' +
        email_footer;

      messages.add(message);
    }
    return messages;
  }*/

  /*public Messaging.SingleEmailMessage sendEmailToProfiles(
    Set<Id> oosIds,
    List<String> profiles
  ) {
    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
    List<Id> userIds = new List<Id>();
    for (User user : [SELECT Id FROM User WHERE profile.name IN :profiles]) {
      userIds.add(user.Id);
    }
    String dataUrl =
      Url.getSalesforceBaseUrl().toExternalForm() +
      '/lightning/r/Out_of_Service__c/' +
      String.valueOf(oosIds.iterator().next()) +
      '/view';

    message.setSubject(email_subject);
    message.setToAddresses(userIds);
    message.setReplyTo(email_sender);
    message.setSenderDisplayName(email_name_sender);
    message.setHtmlBody(
      '<p>Dears,</p>' +
      '<p>New Out-of-Service Data is available in Salesforce.<br />' +
      'Please <a href="' +
      dataUrl +
      '">click here</a> to classify them.</p>' +
      email_footer
    );

    return message;
  }*/
}