/**
* @author Marcos Vinicius de Oliveira Duque
* @date 28/11/2018
* @description: This class is the controller for cloning an EOD for review.
**/
public class FO_EODCloneController {
    private Id originalId;
    
    /**
    * @description The class constructor. It gets the current record Id.
    * 
    * @param ApexPages.StandardController stdController : The standard controller.
    **/
    public FO_EODCloneController(ApexPages.StandardController stdController) {
        originalId = stdController.getId();
    }
    
    /**
    * @description Generates the cloned EOD and redirects to its record page.
    * 
    * @return PageReference : The cloned record page reference.
    **/
    public PageReference redirect(){
        EOD__c review = FO_EODService.cloneForRevision(originalId);
        
		PageReference pageRef = new PageReference('/' + review.Id);
        pageRef.setRedirect(true);    
        return pageRef;
    }
    
}