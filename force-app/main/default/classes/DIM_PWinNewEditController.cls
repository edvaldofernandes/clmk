// Used by DIM - Market Intelligence.
public with sharing class DIM_PWinNewEditController {

    public PWin__c newPWin {get; set;}
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public DIM_PWinNewEditController(ApexPages.StandardController stdController) {
        newPWin = (PWin__c)stdController.getRecord();
        
        if (String.isEmpty(newPWin.Id)) {
            updateNewPWinAccordingToTheLastPWin();
        }
        updateCriteriaDetails();
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public void updateNewPWinAccordingToTheLastPWin() {
    
        List<PWin__c> pWins = [SELECT Business_Support__c, Business_Support_Weight__c, Customer_Experience__c, Customer_Experience_Weight__c, 
                                        Financing__c, Financing_Weight__c, Perceived_Value__c, Perceived_Value_Weight__c, 
                                        Product_Positioning__c, Product_Positioning_Weight__c, Relationship__c, Relationship_Weight__c, 
                                        Time_to_Market__c, Time_to_Market_Weight__c, Deal_Positioning__c FROM PWin__c WHERE Opportunity__c =: newPWin.Opportunity__c ORDER BY Date__c DESC, Name DESC LIMIT 1];
        if (pWins.size() > 0) {
            PWin__c lastPWin = pWins.get(0);
            
            newPWin.Business_Support__c = lastPWin.Business_Support__c;
            newPWin.Business_Support_Weight__c = lastPWin.Business_Support_Weight__c;
            newPWin.Customer_Experience__c = lastPWin.Customer_Experience__c;
            newPWin.Customer_Experience_Weight__c = lastPWin.Customer_Experience_Weight__c;
            newPWin.Financing__c = lastPWin.Financing__c;
            newPWin.Financing_Weight__c = lastPWin.Financing_Weight__c;
            newPWin.Perceived_Value__c = lastPWin.Perceived_Value__c;
            newPWin.Perceived_Value_Weight__c = lastPWin.Perceived_Value_Weight__c;
            newPWin.Product_Positioning__c = lastPWin.Product_Positioning__c;
            newPWin.Product_Positioning_Weight__c = lastPWin.Product_Positioning_Weight__c;
            newPWin.Relationship__c = lastPWin.Relationship__c;
            newPWin.Relationship_Weight__c = lastPWin.Relationship_Weight__c;
            newPWin.Time_to_Market__c = lastPWin.Time_to_Market__c;
            newPWin.Time_to_Market_Weight__c = lastPWin.Time_to_Market_Weight__c;
            newPWin.Deal_Positioning__c = lastPWin.Deal_Positioning__c;
        }
        else {
            newPWin.Business_Support__c = (String) PWin__c.Business_Support__c.getDescribe().getDefaultValue();
            newPWin.Business_Support_Weight__c = (String) PWin__c.Business_Support_Weight__c.getDescribe().getDefaultValue();
            newPWin.Customer_Experience__c = (String) PWin__c.Customer_Experience__c.getDescribe().getDefaultValue();
            newPWin.Customer_Experience_Weight__c = (String) PWin__c.Customer_Experience_Weight__c.getDescribe().getDefaultValue();
            newPWin.Financing__c = (String) PWin__c.Financing__c.getDescribe().getDefaultValue();
            newPWin.Financing_Weight__c = (String) PWin__c.Financing_Weight__c.getDescribe().getDefaultValue();
            newPWin.Perceived_Value__c = (String) PWin__c.Perceived_Value__c.getDescribe().getDefaultValue();
            newPWin.Perceived_Value_Weight__c = (String) PWin__c.Perceived_Value_Weight__c.getDescribe().getDefaultValue();
            newPWin.Product_Positioning__c = (String) PWin__c.Product_Positioning__c.getDescribe().getDefaultValue();
            newPWin.Product_Positioning_Weight__c = (String) PWin__c.Product_Positioning_Weight__c.getDescribe().getDefaultValue();
            newPWin.Relationship__c = (String) PWin__c.Relationship__c.getDescribe().getDefaultValue();
            newPWin.Relationship_Weight__c = (String) PWin__c.Relationship_Weight__c.getDescribe().getDefaultValue();
            newPWin.Time_to_Market__c = (String) PWin__c.Time_to_Market__c.getDescribe().getDefaultValue();
            newPWin.Time_to_Market_Weight__c = (String) PWin__c.Time_to_Market_Weight__c.getDescribe().getDefaultValue();
            newPWin.Deal_Positioning__c = (String) PWin__c.Deal_Positioning__c.getDescribe().getDefaultValue();
        }
    }
    
    @TestVisible
    private void updateCriteriaDetails() {
        newPWin.Business_Support_Definition__c = ((String) PWin__c.Business_Support_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Customer_Experience_Definition__c = ((String) PWin__c.Customer_Experience_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Financing_Definition__c = ((String) PWin__c.Financing_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Perceived_Value_Definition__c = ((String) PWin__c.Perceived_Value_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Product_Positioning_Definition__c = ((String) PWin__c.Product_Positioning_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Relationship_Definition__c = ((String) PWin__c.Relationship_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Time_to_Market_Definition__c = ((String) PWin__c.Time_to_Market_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
        newPWin.Deal_Positioning_Definition__c = ((String) PWin__c.Deal_Positioning_Definition__c.getDescribe().getDefaultValueFormula()).replace('\"', '');
    }
    
    public Boolean hasSumOfWeightsEqualTo100() {
        return
        (Integer.valueOf(newPWin.Business_Support_Weight__c) +
        Integer.valueOf(newPWin.Customer_Experience_Weight__c) +
        Integer.valueOf(newPWin.Financing_Weight__c) +
        Integer.valueOf(newPWin.Perceived_Value_Weight__c) +
        Integer.valueOf(newPWin.Product_Positioning_Weight__c) +
        Integer.valueOf(newPWin.Relationship_Weight__c) +
        Integer.valueOf(newPWin.Time_to_Market_Weight__c)) == 100;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

}