/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ChatterUtils
*
* NAME: ChatterUtilsTest.cls
* AUTHOR: RLdO                                                DATE: 04/12/2014
*******************************************************************************/
@isTest(seeAllData=true)
private class ChatterUtilsTest
{
  static testMethod void myUnitTest()
  {
    Opportunity lOpp = SObjectInstanceTest.createOpportunity();
    Database.insert(lOpp);

    list<Id> usrMention = new list<Id>{ UserInfo.getUserId() };

    test.startTest();    
    ChatterUtils.mentionTextPost(UserInfo.getUserId(), usrMention, lOpp.Id, 'Test');
    test.stopTest();
  }
}