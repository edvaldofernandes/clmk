public with sharing class CLMProposalOrderSizeApexController {
    @AuraEnabled
    public static List<OrderSizeItem__c> getOrderSizeItems(Id recordId){
        List<OrderSizeItem__c> OSKAList = new List<OrderSizeItem__c>();
        try{
			OSKAList = [SELECT Id, Aircraft_Model__c, Model_Name__c, Firm__c, Option__c, Purchase_Right__c
                                                  FROM OrderSizeItem__c
                                                  WHERE Commercial_Agreement__c =: recordId AND Info_Type__c = 'Order Size']; 
            return OSKAList;
        } catch(Exception e){
            String errorMessage = 'Error: ' + e.getMessage() + ' ' + e.getStackTraceString();
            return OSKAList;
        }
    }
        
    @AuraEnabled
    public static List<OrderSizeItem__c> saveEdits(List<OrderSizeItem__c> acs){
        try{
            update acs;
            return acs;
        } catch(Exception e){
            String errorMessage = 'Error: ' + e.getMessage() + ' ' + e.getStackTraceString();
            system.debug(errorMessage);
            return acs;
        }
    }

}