/* Classe implementadora de SOBjectDAO para operações DML no objeto Attachment, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 15/01/2016
*/
public without sharing class AttachmentDao {
    private static final AttachmentDao instance = new AttachmentDao();    
    
    private AttachmentDao(){
    }    
    
    public static AttachmentDao getInstance(){
        return instance;
    }
    
    public Attachment getByParentId (String idParent){       
        List<Attachment> listAttach = [SELECT ParentId, OwnerId, Name, Id, Description, ContentType, BodyLength 
                                       FROM Attachment
                                       WHERE ParentId = :idParent
                                       LIMIT 1];
        
        if(listAttach.size() > 0){
            return listAttach[0];
        }
        
        return null;   
    }
    
    public List<Attachment> listByParentId (String idParent){       
        return [SELECT ParentId, OwnerId, Name, Id, Description, ContentType, BodyLength 
                FROM Attachment
                WHERE ParentId = :idParent];
    }
    
    public List<Attachment> listBySetParentId(Set<String> setIdParent){       
        return [SELECT ParentId, OwnerId, Name, Id, Description, ContentType, BodyLength, Body 
                                       FROM Attachment
                                       WHERE ParentId = :setIdParent];
    }
}