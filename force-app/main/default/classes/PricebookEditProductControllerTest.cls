/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the class EdicaoProdutosPricebook
*
* NAME: PricebookEditProductController.cls
* AUTHOR:JFS                                                DATE: 08/10/2014
*
*******************************************************************************/

@isTest(seeAllData=true)
private class PricebookEditProductControllerTest {

    private static PricebookEditProductController EditarController;
    private static final integer LOTE = 200; 
 
    static testMethod void EdicaodePagina(){
          
      Product2 lProduct = SObjectInstanceTest.createProduct2();
      Database.insert(lProduct);
      
      Id stdPricebook2Id = SObjectInstanceTest.getPricebook2Std2();
      
      PricebookEntry pbeStd = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct.id);
      Database.insert(pbeStd);
        
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
      lPricebook.Name = 'pricebook teste 1';
      lPricebook.Annual_Escalation__c = 10;
      Database.insert(lPricebook);
                
      PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lProduct.Id);
      lPricebookentry.UnitPrice = 20;
      lPricebookentry.RECPrice__c = 100; 
      Database.insert(lPricebookentry);
      
      Map<String,String> teste = new Map<String,String> ();
      teste.put(lPricebookentry.Id,lPricebookentry.Id);
      
      PageReference pageRef = Page.PricebookEditProduct;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('lstaPb', lPricebookentry.Id);
      
      
        
      EditarController = new PricebookEditProductController(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      EditarController.annualEscalation = 10;
      EditarController.mysave();
      Test.stopTest();
        
    }
    
     static testMethod void TesteUnitario(){
          
      Product2 lProduct = SObjectInstanceTest.createProduct2();
      Database.insert(lProduct);
      
      Id stdPricebook2Id = SObjectInstanceTest.getPricebook2Std2();
      
      PricebookEntry pbeStd = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct.id);
      Database.insert(pbeStd);
        
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
      lPricebook.Name = 'pricebook teste 1';
      lPricebook.Annual_Escalation__c = 10;
      Database.insert(lPricebook);
                
      PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lProduct.Id);
      lPricebookentry.UnitPrice = 20;
      lPricebookentry.RECPrice__c = 100; 
      lPricebookentry.useStandardPrice =false;
      Database.insert(lPricebookentry);
      
      Map<String,String> teste = new Map<String,String> ();
      teste.put(lPricebookentry.Id,lPricebookentry.Id);
      
      PageReference pageRef = Page.PricebookEditProduct;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('lstaPb', lProduct.Id);
      
      
        
      EditarController = new PricebookEditProductController(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      id idTeste = EditarController.RecProductTailored;
      idTeste = EditarController.RecProductPilots; 
      idTeste = EditarController.RecProductTraining;
      EditarController.annualEscalation = 10;
      EditarController.Pricebookprodutos[0].UnitPrice = '0';
      EditarController.Pricebookprodutos[0].useStandardPrice = true;
      EditarController.mysave();
      Test.stopTest();
        
    }
    
    static testMethod void UnitTest(){
          
      Product2 lProduct = SObjectInstanceTest.createProduct2();
      Database.insert(lProduct);
      
      Id stdPricebook2Id = SObjectInstanceTest.getPricebook2Std2();
      
      PricebookEntry pbeStd = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct.id);
      Database.insert(pbeStd);
        
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
      lPricebook.Name = 'pricebook teste 1';
      lPricebook.Annual_Escalation__c = 10;
      Database.insert(lPricebook);
                
      PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lProduct.Id);
      lPricebookentry.UnitPrice = 20;
      lPricebookentry.RECPrice__c = 100; 
      lPricebookentry.useStandardPrice =false;
      Database.insert(lPricebookentry);
      
      Map<String,String> teste = new Map<String,String> ();
      teste.put(lPricebookentry.Id,lPricebookentry.Id);
      
      PageReference pageRef = Page.PricebookEditProduct;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('lstaPb', lProduct.Id);
      
      
        
      EditarController = new PricebookEditProductController(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      id idTeste = EditarController.RecProductTailored;
      idTeste = EditarController.RecProductPilots; 
      idTeste = EditarController.RecProductTraining;
      EditarController.annualEscalation = 10;
      EditarController.Pricebookprodutos[0].Id = null;
      EditarController.Pricebookprodutos[0].UnitPrice = '0';
      EditarController.Pricebookprodutos[0].useStandardPrice = false;

      EditarController.mysave();
      Test.stopTest();
        
    }
    
     static testMethod void EdicaodePaginaLote(){
      
      list<Product2> lstProduct = new list<Product2>();
      list<PricebookEntry> listPricebookEntry = new list<PricebookEntry>();
      
      for(integer i =0; i<LOTE; i++){
        
        Product2 lProduct = SObjectInstanceTest.createProduct2();
        lProduct.Name = 'teste produto' + i;
        lProduct.ProductCode = 'testx' + i;
        lstProduct.add(lProduct);
      }
      
      Database.insert(lstProduct);
        
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
      lPricebook.Name = 'pricebook teste 1';
      lPricebook.Annual_Escalation__c = 10;
      Database.insert(lPricebook);
     
      Id Pricebookstd = SObjectInstanceTest.getPricebook2Std2();
      
      lPricebook.Id = Pricebookstd;
        
      for(integer i =0; i<LOTE; i++){
        
        PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lstProduct.get(i).id);
        lPricebookentry.UnitPrice = 20;
        lPricebookentry.RECPrice__c = 100;
        listPricebookEntry.add(lPricebookentry);
      }  
            
      Database.insert(listPricebookEntry);
        
      EditarController = new PricebookEditProductController(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      EditarController.mysave();
      Test.stopTest();
        
    }
}