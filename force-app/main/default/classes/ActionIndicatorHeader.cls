/*
----------------------------------------------------------------------------------------------
-- - Company:     GSW
-- - Name:        ActionIndicatorHeader
-- - Description: Data Transfer Object to carry second CRC Dashboard header data from apex to
-- 				  lightning component.
-- - @Author: André Leinio
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version
----------------------------------------------------------------------------------------------
-- 12/06/2019		André Leinio		1.0
----------------------------------------------------------------------------------------------
*/

public class ActionIndicatorHeader {
		@AuraEnabled
		public decimal total {get; set;} // Total of CRC cases with Next Action.

		@AuraEnabled
		public decimal nextDay {get; set;} // Number of CRC cases with Next Action due date tomorrow or after.

		@AuraEnabled
		public decimal today {get; set;} // Number of CRC cases with Next Action due date today.

		@AuraEnabled
		public decimal expired {get; set;} // Number of CRC cases with Next Action due date expired.

		@AuraEnabled
		public integer incomingEmail 	{get; set;} // Number of CRC cases with Incoming Email.

		@AuraEnabled
		public integer awaitingCustomerResponse {get; set;} // Number of CRC cases Awaiting Customer Response.


    // Class constructor method.
	public ActionIndicatorHeader(){
		total = 0;
		nextDay = 0;
		today = 0;
		expired = 0;
		incomingEmail = 0;
		awaitingCustomerResponse = 0;
	}
}