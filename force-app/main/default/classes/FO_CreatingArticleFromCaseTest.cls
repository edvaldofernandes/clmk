@isTest
public class FO_CreatingArticleFromCaseTest {
    
     static testMethod void Test_FO_CreatingArticleFromCase() { 
         
        Test.startTest();
         
        RecordType rt1 = [SELECT id from RecordType where SobjectType='Account' AND Name = 'Airline'];
        //Site Emb Latin America : 001i000000sX5Mc
        Account a = new Account(RecordTypeId=rt1.Id,Name='Embraer Airline',Company_Nickname__c='Emb Airline',
                                Company_Status__c='Active',Training_Status__c='Active',Geographical_Area__c='Latin America',
                                Embraer_Fleet_Type__c='EJet,ERJ,Turboprop',
                               FlightOps_Account_Domains__c='abc@gmail.com');
        Database.insert(a);
         
        RecordType rt2 = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type']; 
        Case caseToCreate = new Case(RecordTypeId=rt2.Id, Description='Eitchaaa',Status='New',
                                     Subject='Case Test',AccountId=a.Id,SuppliedEmail='abc@gmail.com');
        Database.insert(caseToCreate);
        
        EmailMessage emToCreate = new EmailMessage(FromAddress='abc@gmail.com',
                                               FromName='Vinicius',HtmlBody='Eitchaa',
                                               Incoming=True,ParentId=caseToCreate.Id,
                                               Status='0',Subject='Eitchaa',TextBody='Eitchaaa',
                                               ToAddress='abc@gmail.com');       

        Database.insert(emToCreate);
         
         EmailMessage emToCreate2 = new EmailMessage(FromAddress='abc@gmail.com',
                                               FromName='Vinicius',HtmlBody='Eitchaa',
                                               Incoming=False,ParentId=caseToCreate.Id,
                                               Status='2',Subject='Eitchaa',TextBody='Eitchaaa',
                                               ToAddress='abc@gmail.com');       

        Database.insert(emToCreate2);
      	         
         ApexPages.currentPage().getParameters().put('sourceId', caseToCreate.Id);
         
         ApexPages.KnowledgeArticleVersionStandardController ctl = new ApexPages.KnowledgeArticleVersionStandardController(new FlightOpsCaseAsArticle__kav());
         new FO_CreatingArticleFromCase(ctl);
         
         Test.stopTest();
         
   }

}