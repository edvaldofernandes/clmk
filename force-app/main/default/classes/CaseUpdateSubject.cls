public without sharing class CaseUpdateSubject {
    
    private Set<Id> recordTypes = new Set<Id>{
        Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId(),
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Backorder').getRecordTypeId(),
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Spare Parts Price').getRecordTypeId()
            };    
                
                private List<Case> cases;
    
    public CaseUpdateSubject(List<Case> cases) {
        this.cases = cases;
    }
    public void updateSubject(){
        System.debug('cases: ' + cases);
        
        for(Case c : cases){
            if (c.subject == null) { 
                c.Subject = c.SuppliedEmail; // SondaIT jul2020 - Correção para Subject == NULL
                } 
            
            if(String.isNotBlank(c.RecordTypeId) && recordTypes.contains(c.RecordTypeId) && !c.Subject.contains(c.CaseNumber)){
                c.Subject = c.CaseNumber + ' - ' + c.Subject;
            }
        }
        
    }
    
    public void updateSubjectToInsert(){
        
        Set<Id> caseIds = new Set<Id>();
        
        try{
            
            for(Case c : cases){
                caseIds.add(c.Id);
                
            }
            
            
            List<Case> cas = CaseDAO.getInstance().getCasesBySetId(caseIds);
            
            for(Case c : cas){
                
                if(String.isNotBlank(c.RecordTypeId) && recordTypes.contains(c.RecordTypeId) && !c.Subject.contains(c.CaseNumber)){
                    c.Subject = c.CaseNumber + ' - ' + c.Subject;
                }
               
            }
            
            update cas;
            
        }catch(Exception ex){
            System.debug('Erro ao atualizar o Subject do Case: ' + ex.getMessage());
        }
        
    }
    
}