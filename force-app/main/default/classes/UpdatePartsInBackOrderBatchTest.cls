@isTest
private class UpdatePartsInBackOrderBatchTest {
	@testSetup static void setup() {
        List<Account> accounts = new List<Account>();
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        List<Case> cases = new List<Case>();
        
        //create accounts
        accounts.add(new Account(Name='test acct fll', Company_Nickname__c='test acct fll', PRC_Support_Plant__c='FLL'));
        accounts.add(new Account(Name='test acct lbg', Company_Nickname__c='test acct lbg', PRC_Support_Plant__c='LBG'));
        insert accounts;
        
        //create parts
        parts.add(new LLPDatabase__c(Name='104003-2', Description__c='test', Ecode__c='3883442', Top_Most__c='3883442', Is_Part_Serialized__c='Serialized'));
        parts.add(new LLPDatabase__c(Name='104003-1', Description__c='test', Ecode__c='3883441', Top_Most__c='3883442', Is_Part_Serialized__c='Serialized'));
        parts.add(new LLPDatabase__c(Name='1116-42-1116 MODS 604', Description__c='test', Ecode__c='2074610', Top_Most__c='9408215', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
        cases.add(new Case(RecordTypeId=rTypeId, Subject='test case fll', Description='is back order', AccountId=accounts[0].Id, Part_Number__c=parts[1].Id));
        cases.add(new Case(RecordTypeId=rTypeId, Subject='test case lbg', Description='is not back order', AccountId=accounts[1].Id, Part_Number__c=parts[2].Id));
    	insert cases;
        
        //update phase to back order for the test cases
        List<Case> casesToUpdate = [SELECT Id, Phase__c FROM Case];
        for(Case c : casesToUpdate){
            c.Phase__c = 'Back Order';
        }
        update casesToUpdate;
    }
    @isTest static void testBatch(){
        Test.startTest();
        	UpdatePartsInBackOrderBatch backOrderBatch = new UpdatePartsInBackOrderBatch();
        	Database.executeBatch(backOrderBatch);
        Test.stopTest();
        // after the testing stops, assert records were updated properly
        System.assertEquals(2, [SELECT count() FROM LLPDatabase__c WHERE Is_On_Back_Order_FLL__c = true]);	//should be two parts with FLL back orders
        System.assertEquals(1, [SELECT count() FROM LLPDatabase__c WHERE Is_On_Back_Order_LBG__c = true]);	//should be one part with LBG back orders
    }
    @isTest static void testSchedule(){
        Test.startTest();
        	/*UpdatePartsInBackOrderBatch backOrderBatch = new UpdatePartsInBackOrderBatch();
        	Database.executeBatch(backOrderBatch);*/
			String jobId = System.schedule('Test Job Name', '0 0 0 15 3 ? 2022', new ScheduleUpdatePartsInBackOrder());
        Test.stopTest();
        // after the testing stops, assert was scheduled
        System.assert([SELECT count() FROM CronTrigger WHERE Id = :jobID] > 0);
    }
}