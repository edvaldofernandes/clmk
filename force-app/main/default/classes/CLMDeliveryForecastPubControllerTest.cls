@isTest
public class CLMDeliveryForecastPubControllerTest 
{
    static testMethod void myUnitTest()
    {  
        //RecordType recordTypeProposal = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Proposal' limit 1];
        //RecordType recordType2Purchase = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Purchase_Agreement' limit 1];
        //RecordType recordTypeProduct2 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
        
        
        Id rtProposal = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId();
        Id rtPurchase = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        Id rtProduct = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId();
        
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        insert acc;
        
        DF_Filter__c dfVersion = new DF_Filter__c();
        dfVersion.Aircraft_Family__c = 'E1';
        dfVersion.Start_Date__c = system.today();
        dfVersion.End_Date__c = system.today();
        insert dfVersion;
        
        List<Product2> lstAircraft = new List<Product2>();
        for (Integer i = 0; i < 10; i++)
        {
            Product2 aircraft = new Product2();
            aircraft.RecordTypeId = rtProduct;
            if (i < 5)
            {
                aircraft.Name = 'E1_10001';
                aircraft.Aircraft_Family__c = 'E1';
                aircraft.DF_Grouping__c = '170/175';
            } 
            else 
            {
                aircraft.Name = 'E2_10002';
                aircraft.Aircraft_Family__c = 'E2';
                aircraft.DF_Grouping__c = '190/195';
            }
            lstAircraft.add(aircraft); 
        }
        Database.insert(lstAircraft);
        
        Snapshot_Agreement_Line_Item__c lineItem1 = new Snapshot_Agreement_Line_Item__c();
        lineItem1.Aircraft__c = lstAircraft.get(0).id;
        lineItem1.Aircraft_name__c = lstAircraft.get(0).Name;
        lineItem1.Delivery_Month__c = Date.today().addMonths(0);
        lineItem1.Current_Snapshot_Version__c = 0;
        lineItem1.TREND_Delivery_Date__c = Date.today().addMonths(5);
        lineItem1.Show_Aircraft_As__c = 'Firm';
        lineItem1.Order_Type__c = 'Firm';
        lineItem1.DF_Version__c = dfVersion.Id;
        lineItem1.Aircraft_Family__c = 'E1';
        lineItem1.Status_Category__c = 'PA Signed';
        lineItem1.DF_Grouping__c = lstAircraft.get(0).DF_Grouping__c;
        
        Database.insert(lineItem1);
        
        Snapshot_Agreement_Line_Item__c lineItem2 = new Snapshot_Agreement_Line_Item__c();
        lineItem2.Aircraft__c = lstAircraft.get(1).id;
        lineItem2.Aircraft_name__c = lstAircraft.get(1).Name;
        lineItem2.Delivery_Month__c = Date.today().addMonths(1);
        lineItem2.Current_Snapshot_Version__c = 0;
        lineItem2.TREND_Delivery_Date__c = Date.today().addMonths(6);
        lineItem2.Show_Aircraft_As__c = 'Option';
        lineItem2.Order_Type__c = 'Firm';
        lineItem2.DF_Version__c = dfVersion.Id;
        lineItem2.Aircraft_Family__c = 'E1';
        lineItem2.Status_Category__c = 'PA Signed';
        lineItem2.DF_Grouping__c = lstAircraft.get(1).DF_Grouping__c;
        
        Database.insert(lineItem2);
        
        Snapshot_Agreement_Line_Item__c lineItem3 = new Snapshot_Agreement_Line_Item__c();
        lineItem3.Aircraft__c = lstAircraft.get(2).id;
        lineItem3.Aircraft_name__c = lstAircraft.get(2).Name;
        lineItem3.Delivery_Month__c = Date.today().addMonths(2);
        lineItem3.Current_Snapshot_Version__c = 0;
        lineItem3.TREND_Delivery_Date__c = Date.today().addMonths(7);
        lineItem3.Show_Aircraft_As__c = 'Proposal';
        lineItem3.Order_Type__c = 'Firm';
        lineItem3.DF_Version__c = dfVersion.Id;
        lineItem3.Aircraft_Family__c = 'E1';
        lineItem3.Status_Category__c = 'PA Signed';
        lineItem3.DF_Grouping__c = lstAircraft.get(2).DF_Grouping__c;
        
        Database.insert(lineItem3);
        
        Snapshot_Agreement_Line_Item__c lineItem4 = new Snapshot_Agreement_Line_Item__c();
        lineItem4.Aircraft__c = lstAircraft.get(3).id;
        lineItem4.Aircraft_name__c = lstAircraft.get(3).Name;
        lineItem4.Delivery_Month__c = Date.today().addMonths(3);
        lineItem4.Current_Snapshot_Version__c = 0;
        lineItem4.TREND_Delivery_Date__c = Date.today().addMonths(8);
        lineItem4.Show_Aircraft_As__c = 'Hide';
        lineItem4.Order_Type__c = 'Firm';
        lineItem4.DF_Version__c = dfVersion.Id;
        lineItem4.Aircraft_Family__c = 'E1';
        lineItem4.Status_Category__c = 'PA Signed';
        lineItem4.DF_Grouping__c = lstAircraft.get(3).DF_Grouping__c;
        
        Database.insert(lineItem4);
        
        Snapshot_Agreement_Line_Item__c lineItem5 = new Snapshot_Agreement_Line_Item__c();
        lineItem5.Aircraft__c = lstAircraft.get(4).id;
        lineItem5.Aircraft_name__c = lstAircraft.get(4).Name;
        lineItem5.Delivery_Month__c = Date.today().addMonths(4);
        lineItem5.Current_Snapshot_Version__c = 0;
        lineItem5.TREND_Delivery_Date__c = Date.today().addMonths(9);
        lineItem5.Show_Aircraft_As__c = null;
        lineItem5.Order_Type__c = 'Firm';
        lineItem5.DF_Version__c = dfVersion.Id;
        lineItem5.Aircraft_Family__c = 'E1';
        lineItem5.Status_Category__c = 'PA Signed';
        lineItem5.DF_Grouping__c = lstAircraft.get(4).DF_Grouping__c;
        
        Database.insert(lineItem5);
        
        Snapshot_Agreement_Line_Item__c lineItem6 = new Snapshot_Agreement_Line_Item__c();
        lineItem6.Aircraft__c = lstAircraft.get(5).id;
        lineItem6.Aircraft_name__c = lstAircraft.get(5).Name;
        lineItem6.Delivery_Month__c = Date.today().addMonths(5);
        lineItem6.Current_Snapshot_Version__c = 0;
        lineItem6.TREND_Delivery_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem6.Show_Aircraft_As__c = null;
        lineItem6.Order_Type__c = 'Option';
        lineItem6.DF_Version__c = dfVersion.Id;
        lineItem6.Aircraft_Family__c = 'E1';
        lineItem6.Status_Category__c = 'PA Signed';
        lineItem6.DF_Grouping__c = lstAircraft.get(5).DF_Grouping__c;
        
        Database.insert(lineItem6);
        
        Snapshot_Agreement_Line_Item__c lineItem7 = new Snapshot_Agreement_Line_Item__c();
        lineItem7.Aircraft__c = lstAircraft.get(6).id;
        lineItem7.Aircraft_name__c = lstAircraft.get(6).Name;
        lineItem7.Delivery_Month__c = Date.today().addMonths(6);
        lineItem7.Current_Snapshot_Version__c = 0;
        lineItem7.TREND_Delivery_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem7.Show_Aircraft_As__c = null;
        lineItem7.Order_Type__c = 'Option';
        lineItem7.DF_Version__c = dfVersion.Id;
        lineItem7.Aircraft_Family__c = 'E2';
        lineItem7.Status_Category__c = 'PA Signed';
        lineItem7.DF_Grouping__c = lstAircraft.get(6).DF_Grouping__c;
        
        Database.insert(lineItem7);
        
        Snapshot_Agreement_Line_Item__c lineItem8 = new Snapshot_Agreement_Line_Item__c();
        lineItem8.Aircraft__c = lstAircraft.get(7).id;
        lineItem8.Aircraft_name__c = lstAircraft.get(7).Name;
        lineItem8.Delivery_Month__c = Date.today().addMonths(7);
        lineItem8.Current_Snapshot_Version__c = 0;
        lineItem8.TREND_Delivery_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem8.Show_Aircraft_As__c = null;
        lineItem8.Order_Type__c = 'Option';
        lineItem8.DF_Version__c = dfVersion.Id;
        lineItem8.Aircraft_Family__c = 'E2';
        lineItem8.Status_Category__c = 'PA Signed';
        lineItem8.DF_Grouping__c = lstAircraft.get(7).DF_Grouping__c;
        
        Database.insert(lineItem8);
        
        Snapshot_Agreement_Line_Item__c lineItem9 = new Snapshot_Agreement_Line_Item__c();
        lineItem9.Aircraft__c = lstAircraft.get(8).id;
        lineItem9.Aircraft_name__c = lstAircraft.get(8).Name;
        lineItem9.Delivery_Month__c = Date.today().addMonths(8);
        lineItem9.Current_Snapshot_Version__c = 0;
        lineItem9.TREND_Delivery_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem9.Show_Aircraft_As__c = null;
        lineItem9.Order_Type__c = 'Option';
        lineItem9.DF_Version__c = dfVersion.Id;
        lineItem9.Aircraft_Family__c = 'E2';
        lineItem9.Status_Category__c = 'PA Signed';
        lineItem9.DF_Grouping__c = lstAircraft.get(8).DF_Grouping__c;
        
        Database.insert(lineItem9);
        
        Snapshot_Agreement_Line_Item__c lineItem10 = new Snapshot_Agreement_Line_Item__c();
        lineItem10.Aircraft__c = lstAircraft.get(9).id;
        lineItem10.Aircraft_name__c = lstAircraft.get(9).Name;
        lineItem10.Delivery_Month__c = Date.today().addMonths(9);
        lineItem10.Current_Snapshot_Version__c = 0;
        lineItem10.TREND_Delivery_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        lineItem10.Show_Aircraft_As__c = null;
        lineItem10.Order_Type__c = 'Option';
        lineItem10.DF_Version__c = dfVersion.Id;
        lineItem10.Aircraft_Family__c = 'E2';
        lineItem10.Status_Category__c = 'PA Signed';
        lineItem10.DF_Grouping__c = lstAircraft.get(9).DF_Grouping__c;
        
        Database.insert(lineItem10);
        
        String lineItensId = '';
        List<Snapshot_Agreement_Line_Item__c> lineItensList = new List<Snapshot_Agreement_Line_Item__c>();
        lineItensList.add(lineItem1);
        lineItensList.add(lineItem2);
        lineItensList.add(lineItem3);
        lineItensList.add(lineItem4);
        lineItensList.add(lineItem5);
        lineItensList.add(lineItem6);
        lineItensList.add(lineItem7);
        lineItensList.add(lineItem8);
        lineItensList.add(lineItem9);
        lineItensList.add(lineItem10);
        
        For(Snapshot_Agreement_Line_Item__c lineItem : lineItensList){
            lineItensId += lineItem.id + ';';
        }
        
        List<DF_Production_Capability__c> pcs = new List<DF_Production_Capability__c>();        
        for(Integer i = 0; i < 10; i++){
            DF_Production_Capability__c pc = new DF_Production_Capability__c();
            pc.Aircraft_Model__c = lstAircraft.get(i).id;
            //pc.Aircraft_Model__r = lstAircraft.get(i);
            //pc.Aircraft_Model__r.Name = lstAircraft.get(i).Name;
            //pc.Aircraft_Model__r.DF_Grouping__c = lstAircraft.get(i).DF_Grouping__c;
            pc.Production_Capability__c = 10.0;
            //pc.Aircraft_Model__r.Aircraft_Family__c = lstAircraft.get(i).Aircraft_Family__c;
            pc.DF_Version__c = dfVersion.Id;
            pc.Delivery_s_Date__c = Date.today().addMonths(i);
            pcs.add(pc);
        }
        
        Database.insert(pcs);
        
        CLMDeliveryForecastPubController clmTable = new CLMDeliveryForecastPubController();
        CLMDeliveryForecastPubController.InformationByMonthTotals info = new CLMDeliveryForecastPubController.InformationByMonthTotals(12, 2);
        CLMDeliveryForecastPubController.InformationByMonth info2 = new CLMDeliveryForecastPubController.InformationByMonth();
        clmTable.checkButtonPermissions();
        clmTable.checkDFPermissions();
        clmTable.idDfVersion = dfVersion.Id;
        Test.setCurrentPageReference(new PageReference('Page.CLMDeliveryForecastPubController'));
        System.currentPageReference().getParameters().put('id', dfVersion.Id);
        clmTable.filteredYear();
        clmTable.getNewFilter();
        clmTable.lineItensId = lineItensId;
        clmTable.selectedValueFamily = 'E1';
        clmTable.aircraftFamily = 'E1';
        clmTable.currentYear = date.today().year();
        clmTable.getAgreementLineItens(); 
        clmTable.mapModeloCapability = new Map<String, DF_Production_Capability__c>();
        clmTable.getCapability(lineItensList);
        clmTable.fillThePage();
        clmTable.getOptionsYears();
        clmTable.aircraftId = lstAircraft.get(0).id;
        for (Snapshot_Agreement_Line_Item__c lineItem : lineItensList){
            CLMDeliveryForecastPubController.LineItemView lineItemView = clmTable.fillLineItemView(lineItem);
            clmTable.mapLineItemDetails.put(lineItem.Aircraft_name__c, lineItemView);
            clmTable.fillMapSubTotal(lineItem);
            CLMDeliveryForecastPubController.LineItemView view =  clmTable.mapLineItemDetails.get(lineItem.Aircraft_name__c);
        } 
        clmTable.showPopUp();
        clmTable.closePopUp();
        clmTable.closePopUpYes();
        clmTable.next();
        clmTable.previous();
        clmTable.printPdf();
        clmTable.publicRevision();
        
        clmTable = new CLMDeliveryForecastPubController();
        info = new CLMDeliveryForecastPubController.InformationByMonthTotals(12, 2);
        info2 = new CLMDeliveryForecastPubController.InformationByMonth();
        clmTable.lineItensId = lineItensId;
        clmTable.getAgreementLineItens();
        clmTable.mapModeloCapability = new Map<String, DF_Production_Capability__c>();
        clmTable.getCapability(lineItensList);
        clmTable.aircraftFamily = 'E2';
        Integer year = 2016;
        Set<String> setStr = new Set<String>();
        setStr.add('E1');
        setStr.add('E2');
        clmTable.getProductionByYear(year, setStr);
        clmTable.fillThePage();
        clmTable.selectedValueFamily = 'E2';
        clmTable.aircraftId = lstAircraft.get(0).id;
        for (Snapshot_Agreement_Line_Item__c lineItem : lineItensList){
            CLMDeliveryForecastPubController.LineItemView lineItemView = clmTable.fillLineItemView(lineItem);
            clmTable.mapLineItemDetails.put(lineItem.Aircraft_name__c, lineItemView);
            clmTable.fillMapSubTotal(lineItem);
            CLMDeliveryForecastPubController.LineItemView view =  clmTable.mapLineItemDetails.get(lineItem.Aircraft_name__c);
        } 
        clmTable.showPopUp();
        clmTable.closePopUp();
        clmTable.closePopUpYes();
        clmTable.next();
        clmTable.previous();
        clmTable.printPdf();
        clmTable.publicRevision();
        
        System.assert(clmTable.filteredData);
    }
}