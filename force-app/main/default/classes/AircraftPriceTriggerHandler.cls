public without sharing class AircraftPriceTriggerHandler
{
    public Map<Id,Aircraft_Price__c> newRecordsMap = new Map<Id,Aircraft_Price__c>();
    public Map<Id,Aircraft_Price__c> oldRecordsMap = new Map<Id,Aircraft_Price__c>(); 
    public List<Aircraft_Price__c> newRecords = new List<Aircraft_Price__c>();
    public List<Aircraft_Price__c> oldRecords = new List<Aircraft_Price__c>();
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public AircraftPriceTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

    public void OnBeforeInsert(){}

    public void OnAfterInsert(){}

    public void OnBeforeUpdate(){}

    public void OnAfterUpdate()
    {
        updateRelatedAircraftPrices();
    }

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}

    public boolean IsTriggerContext
    {
        get{return isExecuting;}
    }
    
    
    private void updateRelatedAircraftPrices(){
        
        for(List<Agreement_Aircraft__c> acList : [SELECT Id, Aircraft_Price__c, Basic_Price__c, Aircraft_Basic_Price_EC__c 
                                              	  FROM Agreement_Aircraft__c
                                              	  WHERE Aircraft_Price__c IN :newRecordsMap.keySet()
                                                  ORDER BY Aircraft_Price__c]){
            
            	for(Agreement_Aircraft__c ac : acList){
                    Aircraft_Price__c price = newRecordsMap.get(ac.Aircraft_Price__c);
                    
                    ac.Basic_Price__c = price.Aircraft_Basic_Price__c;
                   	ac.Aircraft_Basic_Price_EC__c = price.Economic_Condition__c;
            }
            Database.update(acList);
        }
        
    }
}