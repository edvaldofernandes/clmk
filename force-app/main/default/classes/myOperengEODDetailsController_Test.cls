@isTest
public class myOperengEODDetailsController_Test {
  
    @isTest static void testCanGetEodById(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
        Database.insert(testUser);
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 1);
        	Database.insert(testCase);
            //List<EOD__c> results = new List<EOD__c>();
            EOD__c[] testEod = FO_MyOP_TestDataFactory.createEods(testCase, 2);
            Database.insert(testEod[0]);
            myOperengEODDetailsController.getEod(testEod[0].Id); 
              
        }
        Test.stopTest(); 
    }
    
    @isTest static void testgetCaseNumber(){
        Test.startTest();
        
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
        Database.insert(testUser);
        
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 1);
        	Database.insert(testCase);
			
            Case[] cases = [SELECT Id, CaseNumber FROM Case];
            String caseNumber = myOperengEODDetailsController.getCaseNumber(cases[0].Id);
            System.assertEquals(cases[0].CaseNumber, caseNumber);  
        }
        
        Test.stopTest();
    }
    
    @isTest static void testgetEod(){
        Test.startTest();
        
        EOD__c[] eods = [SELECT Id, Name FROM EOD__c];
        
        for(Integer i = 0; i < eods.size(); i++){
            EOD__c obj = myOperengEODDetailsController.getEod(eods[i].Id);
            System.assertEquals(eods[i].Id, obj.Id);
        }
        
        Test.stopTest();
    }
    
	private static Case createCase(){
        
        Case c = new Case(
            Status = 'New',
            Due_Date__c = Date.today().addMonths(1),
            Subject = '.',
            Study_Type__c = '.',
            Type = '.',
            Phase_c__c = '.',
            Status__c = '.',
            Next_Action_required_details__c = '.',
            Next_Action_Required_Date_and_Time__c = System.now(),
            Requested_By__c = UserInfo.getUserId(),
            Description = '.'
        );
        
        return c;
    }
  
}