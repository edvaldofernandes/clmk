public class CLMSetAircraftEscalation {
	/*public Map<Id,Escalation__c> newRecordsMap = new Map<Id,Escalation__c>();
    public Map<Id,Escalation__c> oldRecordsMap = new Map<Id,Escalation__c>(); 
    public List<Escalation__c> newRecords = new List<Escalation__c>();
    public List<Escalation__c> oldRecords = new List<Escalation__c>();*/
    
    public static void newEscalation(List<Escalation__c> newRecords){
        
        Set<Id> agreementIdList = new Set<Id>();
        map<String, Escalation__c> escalationMap = new map<String, Escalation__c>();
        
        for(Escalation__c esc : newRecords){
            if (esc.Commercial_Agreement__c !=null){
            	agreementIdList.add(esc.Commercial_Agreement__c);
 				escalationMap.put('' + esc.Commercial_Agreement__c + esc.Rate_Effective_Date__c.month() + esc.Rate_Effective_Date__c.year(), esc);
            }
        }
        
        if(agreementIdList.isEmpty()) return;
        
        List<Agreement_Aircraft__c> aircraftList = [SELECT Id, Agreement__c, Escalation_Code__c, Contractual_Delivery_Month__c FROM Agreement_Aircraft__c WHERE Agreement__c IN :agreementIdList ORDER BY Agreement__c];
        
        if(aircraftList.isEmpty()) return;
        
        addEscalationToAircraft(aircraftList, escalationMap);
        
    }
    
    public static void updatedEscalation(Map<Id,Escalation__c> oldRecordsMap, List<Escalation__c> newRecords){
        List<Escalation__c> escalationToBeUpdated = new List<Escalation__c>();
        Set<Id> escalationIdSet = new Set<Id>();
        for (Escalation__c esc : newRecords){
            if(esc.Rate_Effective_Date__c != oldRecordsMap.get(esc.Id).Rate_Effective_Date__c){
                escalationToBeUpdated.add(esc);
                escalationIdSet.add(esc.Id);
            }
        }
        if (escalationToBeUpdated.isEmpty()) return;
        
        List<Agreement_Aircraft__c> acList = [SELECT Id, Escalation_Code__c From Agreement_Aircraft__c WHERE Escalation_Code__c IN :escalationIdSet];
        system.debug('QUANTIDADE PRA ZERAR' + acList.size() );
        
        for (Agreement_Aircraft__c ac : acList) ac.Escalation_Code__c = null;
        database.update(acList);
        
        newEscalation(escalationToBeUpdated);
    }
    
    public static void newAircraft(List<Agreement_Aircraft__c> newRecords){
        Set<Id> agreementIdList = new Set<Id>();
        map<String, Escalation__c> escalationMap = new map<String, Escalation__c>();

        for(Agreement_Aircraft__c ac : newRecords) agreementIdList.add(ac.Agreement__c);
        
        if(agreementIdList.isEmpty()) return;
        
        List<Escalation__c> agreementEscalationList = [SELECT Id, Commercial_Agreement__c, Rate_Effective_Date__c FROM Escalation__c WHERE Commercial_Agreement__c in :agreementIdList];
        
        for(Escalation__c esc : agreementEscalationList)
            if (esc.Commercial_Agreement__c !=null)
 				escalationMap.put('' + esc.Commercial_Agreement__c + esc.Rate_Effective_Date__c.month() + esc.Rate_Effective_Date__c.year(), esc);
        
        addEscalationToAircraft(newRecords, escalationMap);
    }
    
    public static void updatedAircraft(Map<Id,Agreement_Aircraft__c> oldRecordsMap, List<Agreement_Aircraft__c> newRecords){
        List<Agreement_Aircraft__c> aircraftToBeUpdated = new List<Agreement_Aircraft__c>();
        for (Agreement_Aircraft__c ac : newRecords){
            if(ac.Contractual_Delivery_Month__c != oldRecordsMap.get(ac.Id).Contractual_Delivery_Month__c){
                aircraftToBeUpdated.add(ac);
            }
        }
        if (!aircraftToBeUpdated.isEmpty()){
            newAircraft(aircraftToBeUpdated);
        }
    }
    
    private static void addEscalationToAircraft(List<Agreement_Aircraft__c> aircraftList, map<String, Escalation__c> escalationMap){
        Escalation__c escalationToBeAdded;
        List<Agreement_Aircraft__c> aircraftToBeUpdated = new List<Agreement_Aircraft__c>();
            
        for(Agreement_Aircraft__c ac : aircraftList){
            if (ac.Contractual_Delivery_Month__c == null) continue;
            escalationToBeAdded = escalationMap.get('' + ac.Agreement__c + ac.Contractual_Delivery_Month__c.month() + ac.Contractual_Delivery_Month__c.year());
            if (escalationToBeAdded != null){
                Agreement_Aircraft__c newAc = new Agreement_Aircraft__c();
                newAc.Id = ac.Id;
                newAc.Escalation_Code__c = escalationToBeAdded.Id;
                aircraftToBeUpdated.add(newAc);
            }
        }
        if(!aircraftToBeUpdated.isEmpty()){
            database.update(aircraftToBeUpdated);
        }
    }
}