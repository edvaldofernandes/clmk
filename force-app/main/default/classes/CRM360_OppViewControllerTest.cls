@isTest
public class CRM360_OppViewControllerTest {
    
    private static String accountName = 'Test Account';
    private static String opp1Name = 'Opp1';
    private static String opp2Name = 'Opp2';
    
    @testSetup
    static void setup(){

        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);

        insert accountsToInsert;
        
        List<Opportunity> oppsToInsert = new List<Opportunity>();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = opp1Name;
        opp1.AccountId = account.Id;
        opp1.StageName = 'Prospecting';
        opp1.Fleet_Type__c = '';
        opp1.Amount = 2000;
        opp1.CloseDate = Date.today();

        oppsToInsert.add(opp1);
        
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = opp2Name;
        opp2.AccountId = account.Id;
        opp2.StageName = 'Closed Won';
        opp2.Fleet_Type__c = 'ERJ';
        opp2.Amount = 2000;
        opp2.CloseDate = Date.today();

        oppsToInsert.add(opp2);
        
        insert oppsToInsert;
    }

    
    @isTest
    public static void testCaseAll(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        //List<Case> Cases = [SELECT Id FROM Case WHERE Subject = :case1subject];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_OppViewPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));

        Test.setCurrentPage(pageRef);
        
        CRM360_OppViewController testAccPlan = new CRM360_OppViewController();
            
        testAccPlan.loadOpportunities();
        testAccPlan.getAccount();
        testAccPlan.getLastYear();
        testAccPlan.getOpps();
        
        Test.stopTest();
    }
}