/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the classes 
* ContratoAddProductAirCraftOppBefore.cls and ContratoAddProductAirCraftOppAfter.cls
*
* NAME: ContractAddProductAircraftTest.cls
* AUTHOR:JFS                                                DATE: 02/11/2014
*******************************************************************************/

@isTest
private class ContractAddProductAircraftTest {

    private static final id Rectype = RecordTypeMemory.getRecType('Account', 'Operator');
   private static final integer LOTE = 200;
     
   static testMethod void TestFuncional() {
        
        Account acc = SObjectInstanceTest.createAccount(Rectype);
        database.insert(acc);
        
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        database.insert(opp);
        
        Product2 Prd = SObjectInstanceTest.createProduct2();
        database.insert(Prd);
        
        id pb2 = SObjectInstanceTest.getPricebook2Std2();
        
        PricebookEntry Pbe = SObjectInstanceTest.createPricebookEntry(pb2, Prd.id);
        database.insert(Pbe);
        
        OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(opp.Id, Pbe.Id);
        Oli.Sales_Price__c = Oli.TotalPrice;
        database.insert(Oli);
        
        Aircraft__c AirCraft = SObjectInstanceTest.createAircraft();
        database.insert(AirCraft);
        
        Related_Aircraft__c RelatedAirCraft = SObjectInstanceTest.createRelatedAirCraft(AirCraft.Id);
        RelatedAirCraft.Opportunity__c = opp.id;
        database.insert(RelatedAirCraft);
        
        Contact contato = SObjectInstanceTest.createContact(acc.Id);
        database.insert(contato);
        
        OpportunityContactRole OppCtRole = new OpportunityContactRole();
        OppCtRole.ContactId = contato.Id;
        OppCtRole.OpportunityId = opp.Id;
        OppCtRole.IsPrimary = true;      
        database.insert(OppCtRole);
        
        ServiceContract contrato = new ServiceContract();
        contrato.Name = 'teste';
        contrato.AccountId = acc.id;
        contrato.Opportunity__c = opp.id;
        
        test.startTest();
       
        database.insert(contrato);
                
        test.stopTest();
        
        list < ContractLineItem > lstContract = [SELECT id, ServiceContractId  FROM ContractLineItem WHERE ServiceContractId  =: contrato.id];
        list < Related_Aircraft__c > lstRelated = [SELECT id, Service_Contract__c FROM Related_Aircraft__c WHERE Service_Contract__c =: contrato.id];
        
        system.assert(!lstContract.isEmpty(), 'Deveria ter inserido os produtos da opportunidade no contrato');
                
   }
    
   static testMethod void TestErro() {
        
        Account acc = SObjectInstanceTest.createAccount(Rectype);
        database.insert(acc);
        
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        database.insert(opp);
        
        Product2 Prd = SObjectInstanceTest.createProduct2();
        database.insert(Prd);
        
        id pb2 = SObjectInstanceTest.getPricebook2Std2();
        
        PricebookEntry Pbe = SObjectInstanceTest.createPricebookEntry(pb2, Prd.id);
        database.insert(Pbe);
        
        OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(opp.Id, Pbe.Id);
        Oli.Sales_Price__c = Oli.TotalPrice;
        database.insert(Oli);
        
        Aircraft__c AirCraft = SObjectInstanceTest.createAircraft();
        database.insert(AirCraft);
        
        Related_Aircraft__c RelatedAirCraft = SObjectInstanceTest.createRelatedAirCraft(AirCraft.Id);
        RelatedAirCraft.Opportunity__c = opp.id;
        database.insert(RelatedAirCraft);
        
        Contact contato = SObjectInstanceTest.createContact(acc.Id);
        database.insert(contato);
        
        OpportunityContactRole OppCtRole = new OpportunityContactRole();
        OppCtRole.ContactId = contato.Id;
        OppCtRole.OpportunityId = opp.Id;
        OppCtRole.IsPrimary = true;      
        database.insert(OppCtRole);
        
        ServiceContract contrato = new ServiceContract();
        contrato.Name = 'teste';
        contrato.AccountId = acc.id;
        
        test.startTest();
       
        database.insert(contrato);
        
        test.stopTest();
        
        list < ContractLineItem > lstContract = [SELECT id, ServiceContractId  FROM ContractLineItem WHERE ServiceContractId  =: contrato.id];
        list < Related_Aircraft__c > lstRelated = [SELECT id, Service_Contract__c FROM Related_Aircraft__c WHERE Service_Contract__c =: contrato.id];
        
        system.assert(lstContract.isEmpty(), 'Deveria ter inserido os produtos da opportunidade no contrato');
        system.assert(lstRelated.isEmpty(), ' Deveria ter inserido os RelatedAircrafts da opportunidade no contrato');
   }
    
   static testMethod void TestLote() {
        
        Account acc = SObjectInstanceTest.createAccount(Rectype);
        database.insert(acc);
        
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        database.insert(opp);
        
        Product2 Prd = SObjectInstanceTest.createProduct2();
        database.insert(Prd);
        
        id pb2 = SObjectInstanceTest.getPricebook2Std2();
        
        PricebookEntry Pbe = SObjectInstanceTest.createPricebookEntry(pb2, Prd.id);
        database.insert(Pbe);
        
        OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(opp.Id, Pbe.Id);
        Oli.Sales_Price__c = Oli.TotalPrice;
        database.insert(Oli);
        
        Aircraft__c AirCraft = SObjectInstanceTest.createAircraft();
        database.insert(AirCraft);
        
        Related_Aircraft__c RelatedAirCraft = SObjectInstanceTest.createRelatedAirCraft(AirCraft.Id);
        RelatedAirCraft.Opportunity__c = opp.id;
        database.insert(RelatedAirCraft);
        
        Contact contato = SObjectInstanceTest.createContact(acc.Id);
        database.insert(contato);
        
        OpportunityContactRole OppCtRole = new OpportunityContactRole();
        OppCtRole.ContactId = contato.Id;
        OppCtRole.OpportunityId = opp.Id;
        OppCtRole.IsPrimary = true;      
        database.insert(OppCtRole);
        
        list < ServiceContract > lstContrato = new list < ServiceContract >();
        
        for( integer i = 0; i < LOTE; i++)
        {
           ServiceContract lcontrato = new ServiceContract();
           lcontrato.Name = 'teste contrato' + i;
           lcontrato.AccountId = acc.id;
           lcontrato.Opportunity__c = opp.id;
           lstContrato.add(lcontrato);
        }
        
        test.startTest();
       
        database.insert(lstContrato);
        
        test.stopTest();
        
        map <id, ContractLineItem > mapContract = new map <id, ContractLineItem >([SELECT ServiceContractId FROM ContractLineItem WHERE ServiceContractId  =: lstContrato]);
        map <id, Related_Aircraft__c > mapRelated = new map <id, Related_Aircraft__c > ([SELECT Service_Contract__c FROM Related_Aircraft__c WHERE Service_Contract__c =: lstContrato]);  
        
        for(ServiceContract lContrato: lstContrato)
        {
          system.assert(!mapContract.containsKey(lContrato.id), 'Deveria ter inserido os produtos da opportunidade no contrato');
          system.assert(!mapRelated.containsKey(lContrato.id), ' Deveria ter inserido os RelatedAircrafts da opportunidade no contrato');
          
          
        }
        
    }
}