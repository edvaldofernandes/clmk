global with sharing class CPSMockServer implements HTTPCalloutMock {
 
	global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('Your body');
        res.setStatusCode(201);
        return res;
    }   
}