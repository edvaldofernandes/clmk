@isTest
public class FO_FupQueryBuilderTest {
    @isTest static void testCanSetAValidStatus(){
        String status = 'Investigation';
        String expectedCondition  = 'Status__c = \'' + status + '\'';
        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withStatus(status);

        System.assert(
            builder.conditions.contains(expectedCondition)
        );
    }

    @isTest static void testIfStatusIsBlankWhenInputIsDefault(){
        String status = 'All';
        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withStatus(status);
        for(String condition : builder.conditions){
            System.assert(
                !condition.startsWith('Status__c')
            );
        }
    }

    @isTest static void testCanSetAValidAuthority(){
        String authority = 'EASA';
        String expectedCondition  = 'Authority__c INCLUDES ( \'' + authority + '\' )';
        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withAuthority(authority);
		        
        System.assert(
            builder.conditions.contains(expectedCondition)
        );
    }

    @isTest static void testIfAuthorityIsBlankWhenInputIsDefault(){
        String authority = 'All';
        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withAuthority(authority);
        for(String condition : builder.conditions){
            System.assert(
                !condition.startsWith('Authority__c')
            );
        }
    }
    @isTest static void testCanSetAValidFamily(){
        String family = 'E2';
        String expectedCondition  = 'Aircraft_Family__c INCLUDES ( \'' + family + '\' )';
        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withFamily(family);
        
        System.assert(
            builder.conditions.contains(expectedCondition)
        );
    }
    @isTest static void testIfFamilyIsBlankWhenInputIsDefault(){
        String family = 'All';
        
        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withFamily(family);
        
        
        for(String condition : builder.conditions){
            System.assert(
                !condition.startsWith(' Aircraft_Family__c')
            );
        }
    }
    
    @isTest static void testCanBuildConditionStringProperly(){
        String status = 'Investigation';
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withStatus(status); 
        
             
		String expectedConditionString = ' AND Publish_Status__c = \'Published\'  AND Status__c = \'' + status + '\' ';
        String actualConditionString =  builder.buildConditionsString();

        
        System.assertEquals(expectedConditionString, actualConditionString);
    }
    @isTest static void testCanBuildFieldsStringProperly(){
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder();
        
        
        List<String> fields = builder.fields;
        String expectedFieldString = '';
        for(String field : fields){
            expectedFieldString += ', ' + field;
        }      
        String actualFieldString =  builder.buildFieldsString();

        
        System.assertEquals(expectedFieldString, actualFieldString);
    }

    @isTest static void testIfCanBuildQueryProperly(){
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder();
        
        
        String conditionsString = builder.buildConditionsString();
        String fieldsString = builder.buildFieldsString();
        String expectedQuery = String.format(
            FO_FupQueryBuilder.QUERY_TEMPLATE, 
            new List<String>{
                fieldsString,
                conditionsString
            }
        );       
        String actualQuery = builder.buildQuery();
        
        
        System.assertEquals(expectedQuery, actualQuery);       
    }
    
    @isTest static void testIfQueryIsExecutedProperly(){
        Fup__c fup1 = new FO_FupTestDataBuilder()
            .withAuthorities(new List<String>{'EASA'})
            .withPublishStatus('Published')
            .withStatus('Investigation')
            .build();
        Fup__c fup2  = new FO_FupTestDataBuilder()
            .withAuthorities(new List<String>{'ANAC'})
            .withPublishStatus('Unpublished')
            .withStatus('Closed')
            .build();

        
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withAuthority('EASA')
            .withStatus('Investigation');
        String query = builder.buildQuery();
        List<Fup__c> expectedFups = Database.query(query);
        List<Fup__c> actualFups = builder.get();
        
        
        System.assertEquals(expectedFups, actualFups);
    }
}