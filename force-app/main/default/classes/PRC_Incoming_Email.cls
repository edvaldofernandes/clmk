public class PRC_Incoming_Email{
    
    public PageReference save() {
        return null;
    }
    
    public List<Case> cases;
       
    public Integer total {get; set;}
    
    public Integer cases_green {get; set;}
    public Integer cases_yellow {get; set;}
    public Integer cases_red {get; set;}
    
    public Integer aognfoTotal {get; set;}
    public Integer aognfoGreen {get; set;}
    public Integer aognfoYellow {get; set;}
    public Integer aognfoRed {get; set;}
    public Datetime aognfoMessage {get; set;}
    public Decimal aognfoElapsed {get; set;}
    
    public Integer aogTotal {get; set;}
    public Integer aogGreen {get; set;}
    public Integer aogYellow {get; set;}
    public Integer aogRed {get; set;}
    public Datetime aogMessage {get; set;}
    public Decimal aogElapsed {get; set;}
    
    public Integer crtTotal {get; set;}
    public Integer crtGreen {get; set;}
    public Integer crtYellow {get; set;}
    public Integer crtRed {get; set;}
    public Datetime crtMessage {get; set;}
    public Decimal crtElapsed {get; set;}
    
    public Integer rtnTotal {get; set;}
    public Integer rtnGreen {get; set;}
    public Integer rtnYellow {get; set;}
    public Integer rtnRed {get; set;}
    public Datetime rtnMessage {get; set;}
    public Decimal rtnElapsed {get; set;}

    
    public Decimal avgIncoming {get; set;}
    public Decimal avgIncomingGreen {get; set;}
    public Decimal avgIncomingYellow {get; set;}
    public Decimal avgIncomingRed {get; set;}
    
    public PRC_Incoming_Email(){
    
    //Query to get the average of all Incoming Emails
    
    List<AggregateResult> resultsAvg = [ SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                          FROM Case 
                                          WHERE 
                                          PRC_SJK__c = true
                                          AND Incoming_email_i__c = true
                                          AND Oldest_Case_Message_Date__c != NULL
                                          GROUP BY PRC_SJK__c];
    
    if(resultsAvg.size() > 0 ){   
        avgIncoming = ((decimal)resultsAvg[0].get('avgDec'));
        avgIncoming = avgIncoming.setScale(2);
    }
    
    //Query to get the average of Incoming Email with Status Green
    
    List<AggregateResult> resultsAvgGreen = [ SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                              FROM Case 
                                              WHERE 
                                              PRC_SJK__c = true
                                              AND Incoming_email_i__c = true 
                                              AND Incoming_Email_Status__c = 'Green'
                                              AND Oldest_Case_Message_Date__c != NULL
                                              GROUP BY PRC_SJK__c];
          
    if(resultsAvgGreen.size() > 0 ){   
        avgIncomingGreen = ((decimal)resultsAvgGreen[0].get('avgDec'));
        avgIncomingGreen = avgIncomingGreen.setScale(2);
    }
    
    //Query to get the average of Incoming Email with Status Yellow
    
    List<AggregateResult> resultsAvgYellow = [SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                              FROM Case 
                                              WHERE 
                                              PRC_SJK__c = true
                                              AND Incoming_email_i__c = true 
                                              AND Incoming_Email_Status__c = 'Yellow'
                                              AND Oldest_Case_Message_Date__c != NULL
                                              GROUP BY PRC_SJK__c];       
 
    if(resultsAvgYellow.size() > 0 ){   
        avgIncomingYellow = ((decimal)resultsAvgYellow[0].get('avgDec'));
        avgIncomingYellow = avgIncomingYellow.setScale(2);
    }
    
    //Query to get the average of Incoming Email with Status Red
    
    List<AggregateResult> resultsAvgRed = [SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                              FROM Case 
                                              WHERE 
                                              PRC_SJK__c = true
                                              AND Incoming_email_i__c = true 
                                              AND Incoming_Email_Status__c = 'Red'
                                              GROUP BY PRC_SJK__c];
          
   
    if(resultsAvgRed.size() > 0 ){ 
        avgIncomingRed = ((decimal)resultsAvgRed[0].get('avgDec'));
        avgIncomingRed = avgIncomingRed.setScale(2);
    }           
    //Initialize the variables in zero 
    
    total = 0;
   
    cases_green = 0;
    cases_yellow = 0;
    cases_red = 0;

    aognfoTotal = 0;
    aognfoGreen = 0;
    aognfoYellow = 0;
    aognfoRed = 0;
    
    aogTotal = 0;
    aogGreen = 0;
    aogYellow = 0;
    aogRed = 0;
    
    crtTotal = 0;
    crtGreen = 0;
    crtYellow = 0;
    crtRed = 0;
    
    rtnTotal = 0;
    rtnGreen = 0;
    rtnYellow = 0;
    rtnRed = 0;
 
    cases = getIncomingCases();
    AnalyseCases(); 
         
    }
    
    //Average values
    
    Public Decimal returnAvg() {
        return avgIncoming;
    } 
       
    Public Decimal returnAvgGreen() {
        return avgIncomingGreen;
    } 
      
    Public Decimal returnAvgYellow() {
        return avgIncomingYellow;
    }
   
    Public Decimal returnAvgRed() {
        return avgIncomingRed;
    }
    
    // Query to get all Cases with Incoming Email
    
    public List<Case> getIncomingCases()
    {
        return [SELECT RecordType.ID, CaseNumber, Incoming_Email_Status__c, Priority, Oldest_Case_Message_Date__c, Elapsed_Time_Oldest_Message__c 
                FROM Case
                WHERE
                PRC_SJK__c = true
                AND Incoming_email_i__c = true
                ORDER BY Oldest_Case_Message_Date__c DESC ];
    }
    
    //Analyse the data
    
    public void AnalyseCases()
    {
        for (Case casesIncoming : cases)
        {           
            total++;
            if(casesIncoming.Incoming_Email_Status__c == 'Green')
            {
                cases_green++;
            }
            if (casesIncoming.Incoming_Email_Status__c == 'Yellow')
            {
                 cases_yellow++;
            }
            if (casesIncoming.Incoming_Email_Status__c == 'Red')
            {    
                 cases_red++;
            }
            if(casesIncoming.Priority == 'AOG NFO')
            { 
               
                aognfoTotal++;
                
                aognfoMessage = casesIncoming.Oldest_Case_Message_Date__c;
                aognfoElapsed = casesIncoming.Elapsed_Time_Oldest_Message__c;
                       
                               
                if(casesIncoming.Incoming_Email_Status__c == 'Green')
                {
                   
                    aognfoGreen++;
                }
                else if(casesIncoming.Incoming_Email_Status__c == 'Yellow')
                {
                  
                    aognfoYellow++;
                }
                else aognfoRed++;
                    
             
            }
            if(casesIncoming.Priority == 'AOG')
            { 
               
                aogTotal++;
                
                aogMessage = casesIncoming.Oldest_Case_Message_Date__c;
                aogElapsed = casesIncoming.Elapsed_Time_Oldest_Message__c;
                
                               
                if(casesIncoming.Incoming_Email_Status__c == 'Green')
                {
                    aogGreen++;
                    
                }
                else if(casesIncoming.Incoming_Email_Status__c == 'Yellow')
                {
                    aogYellow++;
                   
                }
                else aogRed++;
                    
                
              
            }
            if(casesIncoming.Priority == 'Critical')
            { 
               
                crtTotal++;
                
                crtMessage = casesIncoming.Oldest_Case_Message_Date__c;
                crtElapsed = casesIncoming.Elapsed_Time_Oldest_Message__c;
                
                               
                if(casesIncoming.Incoming_Email_Status__c == 'Green')
                {
                    crtGreen++;
                    
                }
                else if(casesIncoming.Incoming_Email_Status__c == 'Yellow')
                {
                    crtYellow++;
                   
                }
                else crtRed++;
              
            }
            if(casesIncoming.Priority == 'Routine' || casesIncoming.Priority == 'OSS')
            { 
              
                rtnTotal++;
                rtnMessage = casesIncoming.Oldest_Case_Message_Date__c;
                rtnElapsed = casesIncoming.Elapsed_Time_Oldest_Message__c;  
                               
                if(casesIncoming.Incoming_Email_Status__c == 'Green')
                {
                   
                    rtnGreen++;
                }
                else if(casesIncoming.Incoming_Email_Status__c == 'Yellow')
                {
                    rtnYellow++;
                  
                }
                else rtnRed++;
            }
           
        }
    } 
         
}