@isTest
public class FO_CommunityController_Test {
    
    //Unit Test Case for Constructor
    @isTest static void testConstructor(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        	Database.insert(testCase[0]);
            comControl.listCases();
        }
        	
        Test.stopTest();  
	}
    
    //Unit Test Case for setupCaseDetails, getCaseDetails
    @isTest static void testSetupCaseDetails(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        //Executing test
        PageReference result;
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        	Database.insert(testCase); 
            comControl.listCases();
        
            for(Integer i=0;i<testCase.size();i++){
                comControl.caseID = testCase[0].Id;
                result = comControl.setupCaseDetails();
            }
        }
        Test.stopTest();  
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for getCases with Comments
    @isTest static void testGetCasesWithComments(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        CaseComment[] testComment = FO_MyOP_TestDataFactory.createCaseComment(testCase, 1);
        Database.insert(testComment);       
        //Executing test
        PageReference result;
        for(Integer i=0;i<testCase.size();i++){
            comControl.getCaseComments(testCase[i].Id);
        }
        Test.stopTest();  
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for getCaseEmails Account = Not Embraer
    @isTest static void testGetCaseEmailsAccountNotEmbraer(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(2);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        //Executing test
        PageReference result;
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 2);
        Database.insert(testEmail);
        comControl.accountDomains = 'teste@teste.com';
        comControl.account = testAccount[1];
        for(Integer i=0;i<testCase.size();i++){
            comControl.getCaseEmails(testCase[i].Id);
        }
        
        Test.stopTest();  
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for getCaseEmails Account = Embraer
    @isTest static void testGetCaseEmailsAccountEmbraer(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        
        //Executing test
        PageReference result;
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 2);
        Database.insert(testEmail);
        comControl.account = testAccount[0];
        for(Integer i=0;i<testCase.size();i++){
            comControl.getCaseEmails(testCase[i].Id);
        }
        
        Test.stopTest();  
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for searchCases
    @isTest static void testSearchCasesBlank(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
		Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 1);
        Database.insert(testEmail); 
        CaseComment[] testComment = FO_MyOP_TestDataFactory.createCaseComment(testCase, 4);
        Database.insert(testComment);
        //Executing test
        PageReference result;
        comControl.searchString = '';
        result = comControl.startCaseSearch();
        Test.stopTest();
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for searchCases
    @isTest static void testSearchCasesNotBlank(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
		Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        Database.insert(testCase);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 2);
        Database.insert(testEmail); 
        CaseComment[] testComment = FO_MyOP_TestDataFactory.createCaseComment(testCase, 2);
        Database.insert(testComment);
        //Executing test
        comControl.cases = testCase;
        PageReference result;
        comControl.searchString = 'Test';
        result = comControl.startCaseSearch();
        Test.stopTest();
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for New Issue page link
    //@isTest static void testNewIssuePageReference(){
     //   Test.startTest();
        //Instanciating class and databases
     //   FO_CommunityController comControl = new FO_CommunityController();
        //Executing test
    //    PageReference result;
    //    result = comControl.goToNewIssues();
    //    Test.stopTest();
    //    System.assertNotEquals(NULL, result);
	//}
    
    //Unit Test Case for filters on cases - Status not blank
    @isTest static void testFilterStatusNotBlank(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        Database.insert(testCase);
        //Executing test
        PageReference result;
        comControl.selectedStatusFilter = 'New';
        result = comControl.startCaseFilter();
        comControl.selectedStatusFilter = 'Open';
        result = comControl.startCaseFilter();
        Test.stopTest();
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for filters on cases - selectedCompanyFilter not blank
    @isTest static void testFilterCompanyNotBlank(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        Database.insert(testCase);
        //Executing test
        PageReference result;
        Account [] selectCompany = Database.query('SELECT Id FROM Account LIMIT 1');
        comControl.selectedCompanyFilter = selectCompany[0].Id;
        result = comControl.startCaseFilter();
        Test.stopTest();
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for filters on cases - selectedCompanyFilter = Company
    @isTest static void testFilterCompanyEqualsCompany(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        
        //Executing test
        PageReference result;
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        	Database.insert(testCase);
        	comControl.listCases();
            comControl.selectedCompanyFilter = 'Company';
            result = comControl.startCaseFilter();
        }
        Test.stopTest();
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for filters on cases - selectedCompanyFilter is blank
    @isTest static void testFilterCompanyBlank(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        //Executing test
        PageReference result;
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        	Database.insert(testCase);
        	comControl.listCases();
            comControl.selectedStatusFilter = '';
            result = comControl.startCaseFilter();
        }
        Test.stopTest();
        System.assertEquals(NULL, result);
	}
    
    //Unit Test Case for selection list on status
    @isTest static void testFilterSelectionListStatus(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        //Executing test
        List<SelectOption> selectOptionStatus = comControl.getListOfStatusFilter();
        Integer result = selectOptionStatus.size();
        Test.stopTest();
	}
    
    //Unit Test Case for selection list on company
    @isTest static void testFilterSelectionListCompany(){
        Test.startTest();
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        	Database.insert(testCase);
        	comControl.listCases();
        }
        List<SelectOption> selectOptionCompany = comControl.getListOfCompanyFilter();
        Integer result = selectOptionCompany.size();
        Test.stopTest();
	}
    
    //Unit Test Case for selection list on reset
    @isTest static void testFilterSelectionListReset(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 2);
        	Database.insert(testCase);
        	comControl.listCases();
        }
        List<SelectOption> selectOptionStatus = comControl.getListOfStatusFilter();
        List<SelectOption> selectOptionCompany = comControl.getListOfCompanyFilter();
        comControl.resetFilters();
        Test.stopTest();
        System.assertEquals('Company', comControl.selectedCompanyFilter);
        System.assertEquals('Status', comControl.selectedStatusFilter);
	}
    
    //Unit Test Case for pagination
    @isTest static void testSetupPagination(){
        Test.startTest();
        //Instanciating class and databases
        FO_CommunityController comControl = new FO_CommunityController();
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 6);
        Database.insert(testCase);
        //Executing test
        comControl.cases = testCase;
        comControl.setupPagination(testCase);
        comControl.last();
        comControl.beginning();
        comControl.next();
        comControl.previous();
        Boolean enableNext = comControl.getEnableNext();
        Boolean enablePrevious = comControl.getEnablePrevious();
        Test.stopTest();
	}
    
    
    
    
    

    
}