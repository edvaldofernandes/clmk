@IsTest 
public with sharing class CLMSkylineReportControllerTest
{
            
        static testMethod void unitTest1()
        {
            product2 aircraft = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
            aircraft.Aircraft_Family__c = 'E1';
            insert aircraft;
            product2 aircraft2 = [SELECT id FROM product2 LIMIT 1];
       
            PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(Test.getStandardPricebookId(), aircraft.Id);
            insert pbe;  
        
            Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
            insert acc;
            
            Agreement__c agreement = new Agreement__c();
            agreement.Name = 'Test 1';
            agreement.Account__c = acc.Id;
            agreement.Country__c = 'Iceland';
            agreement.Agreement_Code__c = 'ABCDEFG';
            agreement.Region_Code__c = 'RegionCODE';
            agreement.Status_Category__c = 'PA Signed';
            agreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
            agreement.Company_Signed_Date__c = Date.today(); 
            agreement.Kickoff_trigger_already_executed__c = false; 
            agreement.Admin_Mode__c = 'true';
            agreement.Agreement_Color__c = '0000FF';
            insert agreement;
            Agreement__c agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        
            Aircraft_Price__c aircraftPrice = new Aircraft_Price__c();
            aircraftPrice.Model__c = aircraft2.Id;
            aircraftPrice.Aircraft_Version__c = 'IGW';
            aircraftPrice.Economic_Condition__c = '132131';
            aircraftPrice.Commercial_Agreement__c = agreement2.Id;
            aircraftPrice.Aircraft_List_Price__c = 32131231.00;
            aircraftPrice.Aircraft_Basic_Price__c = 32141212321.00;
            insert aircraftPrice;
            Aircraft_Price__c aircraftPrice2 = [SELECT id FROM Aircraft_Price__c LIMIT 1];
            
                
            Agreement_Aircraft__c contractAircraft = new Agreement_Aircraft__c();
            contractAircraft.Aircraft__c  = aircraft2.Id;
            contractAircraft.Delivery_Date__c = Date.today();
            contractAircraft.Contract_Aircraft_Number__c = 5;
            contractAircraft.Order_Type__c = 'Firm';
            contractAircraft.Status__c = 'Planned';
            contractAircraft.Basic_Price__c = 200;
            contractAircraft.Aircraft_Configuration__c = 'AK';
            contractAircraft.Agreement__c = agreement2.Id;
            contractAircraft.TREND_Delivery_Date__c = Date.today();
            contractAircraft.Skyline_Summary_Calculated__c = 'Firms E1';
            contractAircraft.Contractual_Delivery_Month__c = Date.today();
            contractAircraft.Aircraft_Price__c = aircraftPrice2.id;
            /*system.debug(
                'contractAircraft.Agreement__c' + contractAircraft.Agreement__c + 
                'agreement2.Id' + agreement2.Id + 
                
                Agreement__r.Id != Aircraft_Price__r.Commercial_Agreement__r.Id,
Aircraft__r.Id != Aircraft_Price__r.Model__r.Id
            
            
            );*/
            insert contractAircraft;
            
            Agreement_Aircraft__c contractAircraft2 = new Agreement_Aircraft__c();
            contractAircraft2.Aircraft__c  = aircraft2.Id;
            contractAircraft2.Delivery_Date__c = Date.today();
            contractAircraft2.Contract_Aircraft_Number__c = 5;
            contractAircraft2.Order_Type__c = 'Firm';
            contractAircraft2.Status__c = 'Planned';
            contractAircraft2.Basic_Price__c = 200;
            contractAircraft2.Aircraft_Configuration__c = 'AK';
            contractAircraft2.Agreement__c = agreement2.Id;
            contractAircraft2.TREND_Delivery_Date__c = Date.today().addYears(-1);
            contractAircraft2.Skyline_Summary_Calculated__c = 'Firms E2';
            contractAircraft2.Contractual_Delivery_Month__c = Date.today();
            contractAircraft2.Aircraft_Price__c = aircraftPrice2.id;
            insert contractAircraft2;
            
            Agreement_Aircraft__c contractAircraft3 = new Agreement_Aircraft__c();
            contractAircraft3.Aircraft__c  = aircraft2.Id;
            contractAircraft3.Delivery_Date__c = Date.today();
            contractAircraft3.Contract_Aircraft_Number__c = 7;
            contractAircraft3.Order_Type__c = 'Firm';
            contractAircraft3.Status__c = 'Planned';
            contractAircraft3.Basic_Price__c = 200;
            contractAircraft3.Aircraft_Configuration__c = 'AK';
            contractAircraft3.Agreement__c = agreement2.Id;
            contractAircraft3.TREND_Delivery_Date__c = Date.today().addYears(-1);
            contractAircraft3.Skyline_Summary_Calculated__c = 'High Risk E1';
            contractAircraft3.Contractual_Delivery_Month__c = Date.today();
            contractAircraft3.Aircraft_Price__c = aircraftPrice2.id;
            insert contractAircraft3;
            
            List<DCTReportsSettings__c> goals = new List<DCTReportsSettings__c>();
            goals.add(new DCTReportsSettings__c(name = string.valueOf(Date.today().addYears(-1).Year()),YearGoal__c = 100));
            goals.add(new DCTReportsSettings__c(name = string.valueOf(Date.today().Year()),YearGoal__c = 200));
            insert goals;
                      
            agreement.Status_Category__c = 'PA Signed';
            update agreement;

            Test.startTest();
            string initialYearTest = string.valueOf(Date.today().Year()-7);
            string finalYearTest = string.valueOf(Date.today().Year()); 
            List<CLMSkylineReportController.ChartDataWrapper> loadChartDataRemoting = CLMSkylineReportController.loadChartData(initialYearTest,finalYearTest);
            Test.stopTest();
            
            PageReference pageRef = Page.CLMSkylineReport;
            
            CLMSkylineReportController controller = new CLMSkylineReportController(); 
        
            Test.setCurrentPage(pageRef);
            
            controller.changeSettings();
            controller.updateParameters();
            controller.loadChart();
            List<SelectOption> yearsI = controller.getFilterInitialYears();
            List<SelectOption> yearsF = controller.getFilterFinalYears();
            String filterInitialYear = CLMSkylineReportController.filterInitialYear;
            String filterFinalYear = CLMSkylineReportController.filterFinalYear;
            list<string> seriesNames = controller.seriesNames;
            
            System.Debug('Séries ' + controller.seriesNames);
            
            CLMSkylineReportController.ChartDataWrapper innerClass = new CLMSkylineReportController.ChartDataWrapper('Teste',100,new List<Integer>{150},1,new List<String>{'teste Rodrigo'} ) ;
        
        }   
}