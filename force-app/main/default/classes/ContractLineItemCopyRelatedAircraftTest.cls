/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ContractLineItemCopyRelatedAircraft
*
* NAME: ContractLineItemCopyRelatedAircraftTest.cls
* AUTHOR: RLdO                                                DATE: 20/12/2014
*******************************************************************************/
@isTest
private class ContractLineItemCopyRelatedAircraftTest
{
  static testMethod void myUnitTest()
  {
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();

    Product2 prod = SObjectInstanceTest.createProduct2();
    Database.insert(prod);

    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
    Database.insert(pbe);

    Aircraft__c air = SObjectInstanceTest.createAircraft();
    Database.insert(air);

    Opportunity opp = SObjectInstanceTest.createOpportunity();
    opp.Pricebook2Id = stdPB;
    Database.insert(opp);

    OpportunityLineItem oli = SObjectInstanceTest.createOppItem(opp.Id, pbe.Id);
    Database.insert(oli);

    Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
    rel.Opportunity__c = opp.Id;
    rel.Opportunity_product_id__c = oli.Id;
    //rel.Contract_Line_Item__c = contractLI.Id;
    Database.insert(rel);

    ServiceContract svcContract = SObjectInstanceTest.createServiceContract();
    svcContract.Pricebook2Id = stdPB;
    svcContract.Opportunity__c = opp.Id;
    Database.insert(svcContract);

    ContractLineItem contractLI = SObjectInstanceTest.createContractLineItem(svcContract.Id, pbe.Id);

    Test.startTest();
    Database.insert(contractLI);
    Test.stopTest();

    list<Related_Aircraft__c> lstRel = [SELECT Id from Related_Aircraft__c WHERE Service_Contract__c = :svcContract.Id];
    system.assert(!lstRel.isEmpty());
  }
}