/**
* @author Marcilio Leite de Souza
* @date 20/08/2018
* @description: Test Class for Trigger Helper DML methods (to be improved)
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           20 AGO 2018             Original Version
**/
@isTest
public class GEN_TriggerHelper_Test {

    @isTest
    public static void testTriggerHelper() {
        GEN_TriggerHelper.enableTrigger();
               
        if(GEN_TriggerHelper.isTriggerEnabled()){

            List<Account> listAcc = new List<Account>(); 
            List<Account> listAcc2 = new List<Account>(); 
            
            Account acc = new Account();
            new GEN_TestDataFactory(acc).populateRequiredFields();
           
            system.debug('acc_BEFORE: ' + acc);
            
            acc.Company_Nickname__c = 'NICKNAMENOVO';
              system.debug('acc_AFTER: ' + acc);
            
            
             Account acc2 = new Account();
            new GEN_TestDataFactory(acc2).populateRequiredFields();
            
             Account acc3 = new Account();
            new GEN_TestDataFactory(acc3).populateRequiredFields();
            
             Account acc4 = new Account();
            new GEN_TestDataFactory(acc4).populateRequiredFields();
            
            //Account acc2 = new Account(Name='Account Test 2'); 
            //Account acc3 = new Account(Name='Account Test 3'); 
            //Account acc4 = new Account(Name='Account Test 4');
            
            listAcc.add(acc3);
            listAcc2.add(acc4);
            
            //GEN_TriggerHelper.insertObject(acc);
           // GEN_TriggerHelper.updateObject(acc);
           // GEN_TriggerHelper.deleteObject(acc);
            
            Account acc5 = new Account(name='TestUpsert');
            acc5.Company_Nickname__c = 'TestUpsert';
            acc5.Primary_Key__c = 'PrimaryKeyUpsert';
            
            GEN_TriggerHelper.upsertObjectTriggerDisabled(acc5, Account.Primary_Key__c);
            
            Account acc6 = new Account(name='TestUpsert6');
            acc6.Company_Nickname__c = 'TestUpsert6';
            acc6.Primary_Key__c = 'PrimaryKeyUpsert6';
            List<Account> listAcc3 = new List<Account>(); 
            listAcc3.add(acc6);
            GEN_TriggerHelper.upsertObjectListTriggerDisabled(listAcc3, Account.Primary_Key__c);
            
            
            GEN_TriggerHelper.insertObjectTriggerDisabled(acc2);
            GEN_TriggerHelper.updateObjectTriggerDisabled(acc2);
            GEN_TriggerHelper.deleteObjectTriggerDisabled(acc2);
            
            //GEN_TriggerHelper.insertObjectList(listAcc);
            //GEN_TriggerHelper.updateObjectList(listAcc);
            //GEN_TriggerHelper.deleteObjectList(listAcc);
            
            GEN_TriggerHelper.insertObjectListTriggerDisabled(listAcc2);
            GEN_TriggerHelper.updateObjectListTriggerDisabled(listAcc2);
            GEN_TriggerHelper.deleteObjectListTriggerDisabled(listAcc2);
        }
    }
}