/**
* @author Felipe Gouvea
* @date 11/09/2018
* @description: 
* @comments: 
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
**/
public with sharing class TriggerHelperServiceContractMgmt {

	public static Set<Id> recalculateItemsQuantityAvailableRecordTypes = new Set<Id>{
		Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId(),
		Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable eSolutions').getRecordTypeId(),
		Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable Embraer').getRecordTypeId(),
		Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable (Third Party)').getRecordTypeId(),
		Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable').getRecordTypeId()
	};

	

	public static Set<Id> getEligibleRecordTypes(){
		return recalculateItemsQuantityAvailableRecordTypes;
	}

	public static void recalculateSCMQuantities(Set<Id> pIdsContractLineItems)
    {
        if(pIdsContractLineItems.isEmpty()) return;
        Map<Id,Double> mapDelivered = new Map<Id,Double>();
        Map<Id,Double> mapConverted = new Map<Id,Double>();
        String recordTypeName = '';
        Double quantity = 0;
        string quantityString = '';
        List<ContractLineItem> itemsToUpdate = new List<ContractLineItem>();
        boolean updatedRecord = false;
        
        
        for(AggregateResult result : [SELECT Contract_Line_Item_Master__c,RecordType.Name,SUM(Quantity__c) totalQuantity 
        	FROM Service_Contract_Management__c Where Contract_Line_Item_Master__c in : pIdsContractLineItems And Contract_Line_Item_Master__r.ServiceContract.RecordTypeId = : Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Entitlement').getRecordTypeId() Group BY Contract_Line_Item_Master__c,RecordType.Name Order By Contract_Line_Item_Master__c])
        {
            recordTypeName = string.valueOf(result.get('Name'));
            quantity = 0;
            quantityString = string.valueOf(result.get('totalQuantity'));
            if(recordTypeName.contains('Deliverable'))
            {
                if(mapDelivered.containsKey((id)result.get('Contract_Line_Item_Master__c')))
                    quantity =  mapDelivered.get((id)result.get('Contract_Line_Item_Master__c'));
                
                if(String.IsNotEmpty(quantityString))
                    quantity += double.ValueOf(quantityString);
                
                
                mapDelivered.put((id)result.get('Contract_Line_Item_Master__c'),quantity);
            }
            else if(recordTypeName.contains('Conversion'))
            {
                if(mapConverted.containsKey((id)result.get('Contract_Line_Item_Master__c')))
                    quantity =  mapConverted.get((id)result.get('Contract_Line_Item_Master__c'));
                
                 if(string.IsNotEmpty(quantityString))
                    quantity += double.ValueOf(quantityString);
                
                mapConverted.put((id)result.get('Contract_Line_Item_Master__c'),quantity);
            }
        }
        
        for(ContractLineItem cli : [Select id,delivered__c,Converted__c From ContractLineItem Where Id in : pIdsContractLineItems])
        {
            updatedRecord = false;
            
            cli.delivered__c = 0;
            cli.Converted__c = 0;
            
            if(mapDelivered.containsKey(cli.Id))
            {
               cli.delivered__c = mapDelivered.get(cli.Id);
                updatedRecord = true;
            }
            
            if(mapConverted.containsKey(cli.Id))
            {
                cli.Converted__c = mapConverted.get(cli.Id);
                updatedRecord = true;
            }
            
            if(updatedRecord)
                itemsToUpdate.add(cli);
        }
    
        if(!itemsToUpdate.isEmpty())
            update itemsToUpdate;
    
    
    }

}