@isTest
public class CFCHomeControllerTest 
{
    static testMethod void view()
    {    
        Id recordTypeProductId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId();     
        system.debug('CFCHomeControllerTest.view.recordTypeProduct >>> ' + recordTypeProductId);
        Id recordTypeBillboardId = Schema.SObjectType.Attachments__c.getRecordTypeInfosByName().get('Billboards').getRecordTypeId();     
        system.debug('CFCHomeControllerTest.view.recordTypeBillboard >>> ' + recordTypeBillboardId);
        Id recordTypeAircraftId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Embraer').getRecordTypeId();     
        system.debug('CFCHomeControllerTest.view.recordTypeAircraft >>> ' + recordTypeAircraftId);
        Id recordTypeAccountOEMId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Aircraft OEM').getRecordTypeId();     
        system.debug('CFCHomeControllerTest.view.recordTypeAccountOEM >>> ' + recordTypeAccountOEMId);
        Id recordTypeAccountCAAAId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Civil Aviation Airworthiness Authority').getRecordTypeId();     
        system.debug('CFCHomeControllerTest.view.recordTypeAccountCAAA >>> ' + recordTypeAccountCAAAId);
                
        User userTest = CFCTestSetup.getUser();
        
        Account accOEMTest = [SELECT id, name FROM Account where RecordTypeId =: recordTypeAccountOEMId];
        system.debug('CFCHomeControllerTest.view.accOEMTest >>> ' + accOEMTest);
        
        Account accCAAATest = [SELECT id, name FROM Account where RecordTypeId =: recordTypeAccountCAAAId];
        system.debug('CFCHomeControllerTest.view.accCAAATest >>> ' + accCAAATest);
        
        System.runAs ( userTest ) 
        {            
            CFC_Configurations__c cs = new CFC_Configurations__c();
            cs.Mobile_Limits_KB__c = 50;
            cs.Name = 'Test CS';
            cs.Tablet_Limits_KB__c = 50;
            cs.Desktop_Limit_KB__c = 50;
            cs.Billboard_Time__c = 1000;
            
            cs.Billboard_max_published_number__c = 5;
            cs.URL_Communities__c = 'wwww.teste.com.br';
            cs.Billboard_Time__c = 5;
            cs.Email_to_send_error_job__c = 'teste@teste.com.br';
            cs.User_Name__c = 'teste@teste.com';
            
            database.insert(cs);
            system.debug('CFCHomeControllerTest.view.cs >>> ' + cs);
            
            system.debug('CFCHomeControllerTest.view.userTest >>> ' + userTest);
            
            Contact conTest = [select id,AccountId from Contact where id =:userTest.ContactId];
            system.debug('CFCHomeControllerTest.view.conTest >>> ' + conTest);
            
            //create billboard group
            Billboard_Group__c bGroupTest = new Billboard_Group__c();
            database.insert(bGroupTest);
            system.debug('CFCHomeControllerTest.view.bGroupTest >>> ' + bGroupTest);
            
            //create billboard
            Billboard__c billboardTest = new Billboard__c();
            billboardTest.Billboard_Group__c = bGroupTest.id;
            billboardTest.External_Link__c = '';
            billboardTest.Publish_Status__c = 'Published';
            database.insert(billboardTest);
            system.debug('CFCHomeControllerTest.view.billboardTest >>> ' + billboardTest);
            
            //create attachment__c to billboard but without attachment
            Attachments__c attBillboardTest = new Attachments__c();
            attBillboardTest.Billboard__c = billboardTest.Id;
            attBillboardTest.Active__c = true;
            attBillboardTest.Description__c = 'teste';
            attBillboardTest.Status__c = 'Published';
            attBillboardTest.RecordTypeId = recordTypeBillboardid;
            attBillboardTest.Device_Type__c = 'Desktop';
            database.insert(attBillboardTest);
            system.debug('CFCHomeControllerTest.view.attBillboardTest >>> ' + attBillboardTest);
            
            //create attachment__c device Tablet to billboard
            Attachments__c attBillboardTabletTest = new Attachments__c();
            attBillboardTabletTest.Billboard__c = billboardTest.Id;
            attBillboardTabletTest.Active__c = true;
            attBillboardTabletTest.Description__c = 'teste';
            attBillboardTabletTest.Status__c = 'Published';
            attBillboardTabletTest.RecordTypeId = recordTypeBillboardid;
            attBillboardTabletTest.Device_Type__c = 'Tablet';
            database.insert(attBillboardTabletTest);
            system.debug('CFCHomeControllerTest.view.attBillboardTabletTest >>> ' + attBillboardTabletTest);
            
            //create attachment__c device Mobile to billboard
            Attachments__c attBillboardMobileTest = new Attachments__c();
            attBillboardMobileTest.Billboard__c = billboardTest.Id;
            attBillboardMobileTest.Active__c = true;
            attBillboardMobileTest.Description__c = 'teste';
            attBillboardMobileTest.Status__c = 'Published';
            attBillboardMobileTest.RecordTypeId = recordTypeBillboardid;
            attBillboardMobileTest.Device_Type__c = 'Mobile';
            database.insert(attBillboardMobileTest);
            system.debug('CFCHomeControllerTest.view.attBillboardMobileTest >>> ' + attBillboardMobileTest);
            
            //create attachment__c device Desktop to billboard 
            Attachments__c attBillboardDestkopTest = new Attachments__c();
            attBillboardDestkopTest.Billboard__c = billboardTest.Id;
            attBillboardDestkopTest.Active__c = true;
            attBillboardDestkopTest.Description__c = 'teste';
            attBillboardDestkopTest.Status__c = 'Published';
            attBillboardDestkopTest.RecordTypeId = recordTypeBillboardid;
            attBillboardDestkopTest.Device_Type__c = 'Desktop';
            database.insert(attBillboardDestkopTest);
            system.debug('CFCHomeControllerTest.view.attBillboardDestkopTest >>> ' + attBillboardDestkopTest);
            
            //create attachment to billboard destkop
            Blob b = Blob.valueOf('Test Data'); 
            Attachment attachmentDesktopTest = new Attachment();  
            attachmentDesktopTest.ParentId = attBillboardDestkopTest.Id;  
            attachmentDesktopTest.Name = 'Test Attachment for Parent';  
            attachmentDesktopTest.Body = b;        
            database.insert(attachmentDesktopTest); 
            system.debug('CFCHomeControllerTest.view.attachmentDesktopTest >>> ' + attachmentDesktopTest);
            
            //create attachment to billboard tablet
            Attachment attachmentTabletTest = new Attachment();  
            attachmentTabletTest.ParentId = attBillboardTabletTest.Id;  
            attachmentTabletTest.Name = 'Test Attachment for Parent';  
            attachmentTabletTest.Body = b;        
            database.insert(attachmentTabletTest); 
            system.debug('CFCHomeControllerTest.view.attachmentTabletTest >>> ' + attachmentTabletTest);
            
            //create attachment to billboard mobile
            Attachment attachmentMobileTest = new Attachment();  
            attachmentMobileTest.ParentId = attBillboardMobileTest.Id;  
            attachmentMobileTest.Name = 'Test Attachment for Parent';  
            attachmentMobileTest.Body = b;        
            database.insert(attachmentMobileTest); 
            system.debug('CFCHomeControllerTest.view.attachmentMobileTest >>> ' + attachmentMobileTest);
            
            //create product to related category
            Product2 prodTestRelated = new Product2();
            prodTestRelated.Name = 'Teste Produto';
            prodTestRelated.Applicability__c = 'Turboprop';
            prodTestRelated.RecordTypeId = recordTypeProductId;
            prodTestRelated.Publication_Status__c = 'Published';
            prodTestRelated.Display_on_home_page__c = true;
            insert prodTestRelated;
            System.debug('CFCHomeControllerTest.view.prodTestRelated >>> ' + prodTestRelated);
            
            //create product to others category
            Product2 prodTestOther = new Product2();
            prodTestOther.Name = 'Teste Produto 2';
            prodTestOther.Applicability__c = 'EJET';
            prodTestOther.RecordTypeId = recordTypeProductId;
            prodTestOther.Publication_Status__c = 'Published';
            prodTestOther.Display_on_home_page__c = true;
            insert prodTestOther;
            System.debug('CFCHomeControllerTest.view.prodTestOther >>> ' + prodTestOther);
            
            //create aircraft with account owner
            Aircraft__c aircraftTest = new Aircraft__c();
            aircraftTest.Name = '12345678';
            aircraftTest.Fleet_Type__c = 'Turboprop';
            aircraftTest.Owner__c = conTest.AccountId;
            aircraftTest.Operator__c = conTest.AccountId;
            aircraftTest.RecordTypeId = recordTypeAircraftid;
            aircraftTest.Aircraft_Certification__c = accCAAATest.Id;
            aircraftTest.Manufacturer_Name__c = accOEMTest.Id;
            aircraftTest.Primary_Key__c = '123';
            aircraftTest.Registration__c ='1234';
            aircraftTest.Commercial_Name__c = 'EMB-110P1';
        	aircraftTest.Aircraft_Status__c = 'Other';
            insert aircraftTest;
            System.debug('CFCHomeControllerTest.view.aircraftTest >>> ' + aircraftTest); 
            
            system.debug('TESTE2 >>> ' + [SELECT id FROM Product2] );
            
            CFCHomeController controller = new CFCHomeController();
            controller.showOthers = true;
            
            List<Product2> lstProducts = [SELECT id FROM Product2];
            
            controller.GenerateThumbnailRelatedProducts(lstProducts);
            controller.GenerateThumbnailOthersProducts(lstProducts);
            
            system.assert(controller != null); 
       }      
    }   
}