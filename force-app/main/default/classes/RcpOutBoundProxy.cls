public class RcpOutBoundProxy {
    
    @future(callout=true)
    public static void executeAccount(){

        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element();
        RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort rcpSFRequest = new RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort();
        
        buildBasicAuthentication(rcpSFRequest);
        
        RcpOutBoundAccountService rcpOutBoundAccountService  = new RcpOutBoundAccountService();
        
        SfInfoElement.account = rcpOutBoundAccountService.buildRcpAccount();
        SfInfoElement.accountHistory = rcpOutBoundAccountService.buildRcpAccountHistory();
        
        String rcpResponse = rcpSFRequest.updateFromSalesForce(SfInfoElement);
                
        RecordResponse.recordResponse(rcpResponse, Constants.RCP_ACCOUNT);
        
        CallOutRepository.updateCalloutSent(rcpOutBoundAccountService.getCalloutUpdateList());
    }
    
    @future(callout=true)
    public static void executeAircraft(){

        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element();
        RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort rcpSFRequest = new RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort();
        
        buildBasicAuthentication(rcpSFRequest);
        
        RcpOutBoundAircraftService rcpOutBoundAircraftService = new RcpOutBoundAircraftService();
        
        SfInfoElement.aircraft = rcpOutBoundAircraftService.buildRcpAircraft();
        SfInfoElement.aircraftHistory = rcpOutBoundAircraftService.buildRcpAircratHistory();
        
        String rcpResponse = rcpSFRequest.updateFromSalesForce(SfInfoElement);
        
        RecordResponse.recordResponse(rcpResponse, Constants.RCP_AIRCRAFT);
        
        CallOutRepository.updateCalloutSent(rcpOutBoundAircraftService.getCalloutUpdateList());
    }
    
    @future(callout=true)
    public static void executeTeamMemberUpdated(){
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element();
        RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort rcpSFRequest = new RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort();
        
        buildBasicAuthentication(rcpSFRequest);
        
        SfInfoElement.accountTeamMember = RcpOutBoundAccountService.buildRcpTeamMember(RcpRepository.searchAccountTeamMemberUpdated());
        
        if(SfInfoElement.accountTeamMember != null)
            RecordResponse.recordResponse(rcpSFRequest.updateFromSalesForce(SfInfoElement), Constants.RCP_TEAM_MEMBER);
    }
    
    @future(callout=true)
    public static void executeTeamMemberInserted(){
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element();
        RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort rcpSFRequest = new RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort();
        
        buildBasicAuthentication(rcpSFRequest);
        
        SfInfoElement.accountTeamMember = RcpOutBoundAccountService.buildRcpTeamMember(RcpRepository.searchAccountTeamMemberInserted());
        
        if(SfInfoElement.accountTeamMember != null)
            RecordResponse.recordResponse(rcpSFRequest.updateFromSalesForce(SfInfoElement), Constants.RCP_TEAM_MEMBER);
    }
    
    private static void buildBasicAuthentication(RCP_OUT_BOUND_WEB_SERVICE.SFDCtoRCPWSPort rcpSFRequest){
        
        Blob headerValue = Blob.valueOf(WebServiceConfiguration.getAuthentication(Constants.RCP_OUT_BOUND_PROXY));
        String authorizationHeader = Constants.BASIC_AUTHORIZATION + EncodingUtil.base64Encode(headerValue);
		
        rcpSFRequest.inputHttpHeaders_x = new Map<String, String>();
        rcpSFRequest.inputHttpHeaders_x.put(Constants.AUTHORIZATION,authorizationHeader);
    }
}