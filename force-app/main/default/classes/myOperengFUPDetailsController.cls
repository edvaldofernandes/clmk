/**
* @description: This class is the controller of the Follow-up data for myOpereng.
**/
public class myOperengFUPDetailsController {
    
    @AuraEnabled
    public static FUP__c getFup(Id fupId) {
        try {
            return FO_FUPService.getPublishedFupById(fupId);
        } catch(FlightOpsException e) {
            throw new AuraHandledException('Permission denied to access this Follow-up.');
        }
    }
}