@isTest
public class CLMAmendmentControllerTest {
/*
    static testMethod void alreadyPromoted(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        Agreement1.PA_Already_set__c = true;
        Agreement1.Related_Proposal__c = Agreement1.Id;
        database.insert(new List<Agreement__c>{Agreement1});
        Agreement1.Status_Category__c = 'Proposal Signed';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id', Agreement2.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMPromoteToPAController ptpa = new CLMPromoteToPAController(sc);
        
        ptpa.promote();
        
        system.assert([SELECT id FROM Agreement__c].size() == 1);
        
    }
    
    static testMethod void notApprovedOrSignedYet(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(Agreement1);
        Agreement1.Status_Category__c = 'Proposal Request';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement2.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMPromoteToPAController ptpa = new CLMPromoteToPAController(sc);
        
        ptpa.promote();
        
        system.assert([SELECT id FROM Agreement__c].size() == 1);
        
    }*/
    
    static testMethod void amend(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(Agreement1);
        Agreement1.Status_Category__c = 'PA Signed';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement2.Id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMAmendmentController clone = new CLMAmendmentController(sc);        
        clone.amend();
        
        clone.amendCompleted();
        clone.goToNewAmend();
        test.stopTest();
        
        system.assert([SELECT id FROM Agreement__c].size() == 2);
    }
    
    static testMethod void nullOption(){
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id', null);
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp 1';
        opp.Fleet_Type__c = 'Turboprop';
        opp.StageName = 'Validation_of_approvals';
        opp.CloseDate = system.today();
        insert opp;
        Kickoff__c ko = new Kickoff__c();
        ko.Name = 'Checklist 1';
        ko.Opportunity__c = opp.Id;
        ko.Customer_Type__c = 'New Customer';
        ko.submitted__c = true;
        ko.Kick_Off_Meeting_Date__c = system.today();
        insert ko;       
        
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Kickoff__c = ko.Id;
        database.insert(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement2);
        CLMAmendmentController clone = new CLMAmendmentController(sc);
        clone.amend();
        test.stopTest();
        
        system.assert([SELECT id FROM Agreement__c].size() == 1);
    }
    
    static testMethod void NotApproved(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(Agreement1);
        Agreement1.Status_Category__c = 'PA Request';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement2.Id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMAmendmentController clone = new CLMAmendmentController(sc);
        clone.amend();
        
        clone.amendCompleted();
        clone.goToNewAmend();
        test.stopTest();
        
        system.assert([SELECT id FROM Agreement__c].size() == 1);
    }

}