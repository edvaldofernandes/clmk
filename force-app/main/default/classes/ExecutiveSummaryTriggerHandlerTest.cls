/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public class ExecutiveSummaryTriggerHandlerTest  {
    static testMethod void myUnitTest() 
    {
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert acc;
        Id recordTypeSitesMonthlyId = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('Sites Monthly Overview').getRecordTypeId();
        Id recordTypeAirlineCustomerId = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('Airline Customers').getRecordTypeId();

        
        ExecutiveSummaryTriggerHandler handler = new ExecutiveSummaryTriggerHandler(true);
        Account_Cockpit__c exSum = new Account_Cockpit__c( Month__c = 'September', Account_Name__c = acc.Id, RecordTypeId = recordTypeSitesMonthlyId);
        insert exSum;
        update exSum;
        

        System.AssertEquals(handler.IsTriggerContext,true);
        exSum.recordTypeId = recordTypeAirlineCustomerId;
 		exSum.Month__c = 'November';
        update exSum;

        exsum = exSum.clone(false, true, false, false);

        exSum.recordTypeId = recordTypeAirlineCustomerId;
        insert exSum;
        delete exSum;
        

        
    }

}