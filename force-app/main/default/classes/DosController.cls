public class DosController{ 
    
    //public members used in the view and controller
    //lists of all the main data
    public List<LLPDatabase__c> SelectedPartNumbers{get;set;}
    public DosTopMost SelectedTopMost {get;set;}
    public List<Case> SelectedCases {get;set;}
    public List<Case> SelectedContractTerms {get;set;}
    public List<Core_Information__c> SelectedCores {get;set;}
    public List<Repair_Information__c> SelectedRepairInfo {get;set;}
    public List<RESS_Info__c> SelectedResses {get;set;}
    public List<QM__c> SelectedQMs {get;set;}
    
    //various public members
    public List<SelectOption> Categories {get;set;}
    public integer SelectedRank {get;set;} {SelectedRank = 1;}
    public integer topMostTotal {get;set;}
    public integer orderTotal {get;set;}
    public string MultiSearchInput {get;set;}
    public string SelectedRegion {get;set;} {SelectedRegion = 'ALL';}
    public string ErrorMessage {get;set;}
    public Boolean IsInvalidRank {get;set;} {IsInvalidRank = false;}
    public Boolean IsNoMatch {get;set;} {IsNoMatch = false;}
    
    //toggle panels
    public Boolean IsExpandedRE {get;set;} {IsExpandedRE = false;}
    public Boolean IsExpandedCO {get;set;} {IsExpandedCO = false;}
    public Boolean IsExpandedPOH {get;set;} {IsExpandedPOH = false;}
    public Boolean IsExpandedRI {get;set;} {IsExpandedRI = false;}
    public Boolean IsExpandedCI {get;set;} {IsExpandedCI = false;}
    public Boolean IsExpandedQM {get;set;} {IsExpandedQM = false;}
    
    //variables for comments section
    public List<Comments__c> PRCComments{get;set;}
    Public List<Comments__c> POSComments{get;set;}
    Public List<Comments__c> RMComments{get;set;}
    public string NewComment{get;set;}
    public string SelectedDepartment{get;set;}
    public string selectedCaseNumber{get;set;}
    public Boolean NotifyPRC{get;set;} {NotifyPRC = false;}
    public Case currentCase{get;set;}
    public string SelectedCategory{get;set;}
    
    //private members only used in the controller
    private List<DosTopMost> AllTopMosts {get;set;}
    @TestVisible private integer currentRank{get;set;} {currentRank = 1;}
    
    //get DosService class instance
    DosService dService = new DosService();
    //get DosRepository class instance
    DosRepository dRepo = new DosRepository();
    
    //CONSTRUCTOR
    public DosController() {
        ShowCasesByRegion();
    }
    
    public void ShowCasesByRegion(){
        //intialize variables to be used in view
        SelectedCases = new List<Case>();
        SelectedContractTerms = new List<Case>();
        SelectedTopMost = new DosTopMost();
        SelectedPartNumbers = new List<LLPDatabase__c>();
        SelectedCores = new List<Core_Information__c>();
        SelectedRepairInfo = new List<Repair_Information__c>();
        SelectedResses = new List<RESS_Info__c>();
        SelectedQMs = new List<QM__c>();
        
        PRCComments = new List<Comments__c>();
        POSComments = new List<Comments__c>();
        RMComments = new List<Comments__c>();
        Categories = DosService.getPicklistMainCategory();
        
        //Get cases depending on region
        List<Case> DosCases = new List<Case>();
        if(SelectedRegion=='ALL')
        	DosCases = dRepo.GetAllDosCases();	//get all dos cases
        ELSE if(SelectedRegion=='AOG')
        	DosCases = dRepo.GetDosCasesByPriority(SelectedRegion);	//get all AOG cases
      /*  else if(SelectedRegion.left(3)=='REP')
          DosCases = dRepo.GetRepairAdminDosCases(SelectedRegion); Nancy Removed*/	//get repair admin dos cases
        else
            DosCases = dRepo.GetDosCasesByRegion(SelectedRegion); 		//get dos cases for the selected region
        
        //Group Order Data By top most
        List<DosTopMost> groupedTMs = dService.GroupByTopmost(DosCases);  		//group the cases by topMost
        AllTopMosts = dService.RankTopMosts(groupedTMs); 						//rank the list of TMs
      	List<String> TopMostStrings = dService.getTopMostStrings(groupedTMs);	//set the TopMostStrings List
        
        //Get other DOS info based on region
        List<RESS_Info__c> resses = new List<RESS_Info__c>();
        List<Repair_Information__c> repairs = new List<Repair_Information__c>();
        List<Core_Information__c> cores = new List<Core_Information__c>();
        List<QM__c> QMs = new List<QM__c>();
        if(SelectedRegion=='ALL'){
            repairs = dRepo.GetRepairInfoByTopmost(TopMostStrings);
      //      resses = dRepo.GetRessInfoByTopmost(TopMostStrings);
            cores = dRepo.GetCoresByTopmost(TopMostStrings);
            QMs = dRepo.GetQMsByTopmost(TopMostStrings);
            //GET info by priority AOG
        } ELSE if(SelectedRegion=='AOG'){
            repairs = dRepo.GetRepairInfoByTopmost(TopMostStrings);
        //    resses = dRepo.GetRessInfoByTopmost(TopMostStrings);
            cores = dRepo.GetCoresByTopmost(TopMostStrings);
            QMs = dRepo.GetQMsByTopmost(TopMostStrings);
        }
        //else if(SelectedRegion.left(3)=='REP'){
           // if(SelectedRegion=='REP AZUL')
             //   repairs = dRepo.GetRepairInfoByRegion(TopMostStrings, 'FLL COM');
           // else
            	//repairs = dRepo.GetRepairInfoByRegion(TopMostStrings, SelectedRegion.right(3) + ' COM');
      //  }
    else{
            repairs = dRepo.GetRepairInfoByRegion(TopMostStrings, SelectedRegion + ' COM');
        //    resses = dRepo.GetRessInfoByRegion(TopMostStrings, SelectedRegion);
            cores = dRepo.GetCoresByRegion(TopMostStrings, SelectedRegion);
            QMs = dRepo.GetQMsByRegion(TopMostStrings, SelectedRegion);
        }
        
        //Set other DOS info 
        dService.SetPartsOnHand(AllTopMosts, dRepo.GetPartNumbersByTopmost(TopMostStrings)); 	//Set Part Number-On Hand Info for each top Most
        dService.SetCores(AllTopMosts, cores);													//set core info for each top most
        dService.SetRepairInfo(AllTopMosts, repairs); 											//Set repair info for each top most
        dService.SetResses(AllTopMosts, resses);												//set RESSes for each top most
        dService.SetQMs(AllTopMosts, QMs);														//set QMs for each top most
        
        //set totals for the view
        topMostTotal = AllTopMosts.size();
        orderTotal = DosCases.size();
        
        //set selected Rank
        SelectedRank = 1;
		IsInvalidRank = false;
        
        //set the selectedTopMost
        if(!AllTopMosts.IsEmpty())
        	SetSelectedTopMost(AllTopMosts[0]);
    }
    
    //sets the selected topmost by rank
    public void SetSelectedTopMost(DosTopMost topMost){
        SelectedTopMost = topMost;
        SelectedCases = topMost.Cases;
        SelectedPartNumbers = topMost.PartsOnHand;
        SelectedCores = topMost.Cores;
        SelectedRepairInfo = topMost.RepairInfo;
        SelectedResses = topMost.Resses;
        SelectedQMs = topMost.QMs;
        SelectedContractTerms = dService.setCTs(topMost.Cases);
    }
    
    public void SelectByRank(){
        //set top most using the selected rank if the new rank is valid
        if(SelectedRank > 0 && SelectedRank <= topMostTotal){
            SetSelectedTopMost(AllTopMosts[SelectedRank-1]);
            currentRank = SelectedRank;
            IsInvalidRank = false;
            return;
        }
        SelectedRank = currentRank;  //reset invalid selected Rank to the current rank
        IsInvalidRank = true;
    }
    
    //searches for a topmost by tm, ecode, pn, so#
    //gets a tm from the dos service and sets the Dos using that tm
    public void SearchTopMost(){
        DosTopMost tm = dService.SearchTopMosts(MultiSearchInput.trim(), AllTopMosts);
        if(!string.isBlank(tm.TopMost)){
            IsNoMatch = false;
            SelectedRank = tm.Rank;
            SetSelectedTopMost(tm);   
        }
        else{
            IsNoMatch = true;
        }
    }
    
    public void toggleREPanel(){
        IsExpandedRE = !IsExpandedRE;
    }
    
    public void toggleCOPanel(){
        IsExpandedCO = !IsExpandedCO;
    }
    
    public void togglePOHPanel(){
        IsExpandedPOH = !IsExpandedPOH;
    }
    
    public void toggleRIPanel(){
        IsExpandedRI = !IsExpandedRI;
    }

    public void toggleCIPanel(){
        IsExpandedCI = !IsExpandedCI;
    } 
    
    public void toggleQMPanel(){
        IsExpandedQM = !IsExpandedQM;
    }
                                           
    public void GotoFirst(){
        SelectedRank = 1;
        SelectByRank();
    }
    
    public void GotoLast(){
        SelectedRank = topMostTotal;
        SelectByRank();
    }
    
    public void GotoNext(){
        SelectedRank = SelectedRank + 1;
        SelectByRank();
    }
    
    public void GotoPrev(){
        SelectedRank = SelectedRank - 1;
        SelectByRank();
    }
    
    //get comments for the selected case
    public void getCaseComments(){
        POSComments.clear();
        PRCComments.clear();
        RMComments.clear();
        //get all comments for the selected case from DB
        currentCase = dRepo.GetCommentsByCase(SelectedCaseNumber);
        List<Comments__c> AllComments = currentCase.Comments__r;	//assign the comments from a case into a list 
        //sort the comments by department into their respective lists
        for(Comments__c com : AllComments){
            if(com.Department__c == 'PRC'){
                PRCComments.add(com);
            } else if(com.Department__c == 'POS'){
                POSComments.add(com);
            } else{
                RMComments.add(com);
            }
        }    
    }
    
    //insert a comment into DB
    public void AddComment(){
        try{
            dService.AddCommentToDb(currentCase, NewComment, SelectedDepartment, NotifyPRC);
            //clear the comments and checkbox
            NotifyPRC=false;
            NewComment='';
            ErrorMessage='';
            refresh();
        }
        catch(Exception e){
            System.Debug('The following exception occured: ' + e.getMessage());
            ErrorMessage = 'DOS Comments have a max length of 255 characters. Please try again.';
        }
    }
    
    //clear the latest POS comment
    public void ClearPOSComment(){
        dService.ClearLatestComment(currentCase, 'POS');
        //refresh everything after comment is cleared
        refresh();
    }
    
    //clear latest PRC comment
    public void ClearPRCComment(){
        dService.ClearLatestComment(currentCase, 'PRC');
        //refresh everything after comment is cleared
        refresh();
    }
    
    //refresh DOS data but stay on the same page
    private void refresh(){
        integer currentRnk = SelectedTopMost.Rank;
        ShowCasesByRegion();
        SelectedRank = currentRnk;
        SelectByRank();
    }
    
    //updates case in DB
    public void ChangeCategory(){
        dRepo.UpdateCase(currentCase);
        refresh();
    }
    
    //end of controller
}