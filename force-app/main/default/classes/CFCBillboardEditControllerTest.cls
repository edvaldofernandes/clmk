@isTest
public class CFCBillboardEditControllerTest {
    
    static testMethod void myUnitTest() 
    {
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        system.debug('CFCBillboardEditControllerTest.myUnitTest.bGroup >>> ' + bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Published';
        database.insert(billboard);
        system.debug('CFCBillboardEditControllerTest.myUnitTest.billboard >>> ' + billboard);
        
        CFCBillboardEditController controller = new CFCBillboardEditController(new ApexPages.StandardController(billboard));
        controller.validatePublication();
        
        system.assert(controller.fieldBillboard != null);
    }
    
    static testMethod void myUnitTest2() 
    {
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        system.debug('CFCBillboardEditControllerTest.myUnitTest.bGroup >>> ' + bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Not published';
        database.insert(billboard);
        system.debug('CFCBillboardEditControllerTest.myUnitTest.billboard >>> ' + billboard);
        
        CFCBillboardEditController controller = new CFCBillboardEditController(new ApexPages.StandardController(billboard));
        controller.validatePublication();
        
        system.assert(controller.fieldBillboard != null);
    }
    
}