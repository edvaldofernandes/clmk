/* Classe implementadora de SOBjectDAO para operações DML no objeto PricebookEntry, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 13/01/2016
*/
public without sharing class PricebookEntryDao {
    
    private static final PricebookEntryDao instance = new PricebookEntryDao();    
    
    private PricebookEntryDao(){
    }    
    
    public static PricebookEntryDao getInstance(){
        return instance;
    }
    
    public List<PricebookEntry> listByPricebookIdAndSetProductId(String idPricebook, Set<String> setIdProduct){        
        string setToString = '';
        for(String includeValue : setIdProduct){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');

        List<PricebookEntry> lst;
        try {
        	lst = database.query(' Select ' + utils.getAllFields('PricebookEntry') + 
                                 ' FROM PricebookEntry WHERE Pricebook2Id =\''+String.escapeSingleQuotes(idPricebook)+'\''+
                                 ' AND Product2Id IN ('+setToString+')'+
                                 ' AND CurrencyIsoCode = \'USD\'');        
        } catch (Exception e) {
            lst = new List<PricebookEntry>();
        }
                
        return lst;
    }
    
    public List<PricebookEntry> getByPricebookId(SET<String> idPricebook){
        string setToString = '';
        for(String includeValue : idPricebook){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');

        List<PricebookEntry> lst = [SELECT id, Product2Id FROM PricebookEntry WHERE Id IN :idPricebook];
        return lst;
    }    
    
    
}