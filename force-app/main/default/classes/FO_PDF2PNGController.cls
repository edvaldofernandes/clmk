global class FO_PDF2PNGController{
    //public static final Integer FILE_SIZE_LIMIT = Math.round( Limits.getLimitHeapSize()*0.565810 );
    public static final Integer FILE_SIZE_LIMIT = 4500000;
    public Id recordId {get; set;}
    
    @RemoteAction
    global static void saveImage(String base64Image, Id documentId, Integer pageNumber){
		Blob image = EncodingUtil.base64Decode(base64Image);

        FO_Document.insertTemporaryFile(image, documentId, pageNumber);
    }
    
    public String getAttachedPdfList(){
        List<Id> attachedFilesIds = FO_EODService.getEodRelatedDocumentsIds(this.recordId);
        List<ContentVersion> attachedPdfs = [
        	SELECT
            	Id,
            	Title,
            	ContentDocumentId,
            	VersionData
            FROM
            	ContentVersion
            WHERE
            	ContentDocumentId IN :attachedFilesIds
        ];
        if(Limits.getHeapSize() < FILE_SIZE_LIMIT){
            String jsonResponse = buildJsonResponse(attachedPdfs);
            return jsonResponse;
        }else{
        	throw new FlightOpsException('The sum size of the attached files is larger than the limit (4.5MB). please reduce the file size to less than 4.5MB.');
        }
    }
    public static String buildJsonResponse(List<ContentVersion> attachedPdfs){
        Map<Id, String> attachmentsData = new Map<Id, String>();
        for(ContentVersion att : attachedPdfs){
            String attId = att.ContentDocumentId;
            String data = EncodingUtil.base64Encode(att.VersionData);
            attachmentsData.put(attId, data);
        }
        String jsonResponse = JSON.serialize(attachmentsData);
        return jsonResponse;
    }
    @RemoteAction
    global static void doAction(Id recordId){
        FO_EODService.publish(recordId);
    }
    
    /**
    * @description Do the action and returns to its record page.
    * 
    * @return PageReference : An page reference.
    **/
    public PageReference redirect(){
        //doAction();
		PageReference pageRef = new PageReference(this.getPageReferenceUrl());
        pageRef.setRedirect(true);    
        return pageRef;
    }
    
    /**
    * @description Creates the URL to which the user will be redirected.
    * 
    * @param ApexPages.StandardController stdController : The standard controller.
    **/    
    public virtual String getPageReferenceUrl(){
        return '/' + recordId;
    }
}