/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for deleting related aircraft when a ServiceContract is deleted.
*
* NAME: ServiceContractDeleteRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*******************************************************************************/
public with sharing class ServiceContractDeleteRelatedAircraft {

  public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    List<Id> lstOppId = new List<Id>();
    for ( ServiceContract opp: (List<ServiceContract>) trigger.old)
    {
      lstOppId.add(opp.Id);
    }
    if ( lstOppId.isEmpty() ) return;
    
    List<String> lstProdutoOppId = new List<String>();
    for ( ContractLineItem oli : [SELECT Id FROM ContractLineItem WHERE ServiceContractId =: lstOppId])
    {
      lstProdutoOppId.add(oli.Id);
    }
    if ( lstProdutoOppId.isEmpty() ) return;
    
    List<Related_Aircraft__c> lstRelAircrafts = [ SELECT Id FROM Related_Aircraft__c 
      WHERE Contract_Line_Item__c =: lstProdutoOppId ];
      
    if ( !lstRelAircrafts.isEmpty() ) delete lstRelAircrafts;
  }
  
}