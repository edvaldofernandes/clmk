public with sharing class CFCSidebarComponentController {
    public String fleetType {get;set;}
    public String lblApplicability {get;set;}
    public String lblFleetType {get;set;}
    public String lblOperationContext {get;set;}
    public String lblProductFamily {get;set;}
    public String lblAtaChapter {get;set;}
    
    /*public String operationContext {get;set;}
public String[] productFamily {get;set;}
public String[] motivationForSales {get;set;}
public String[] applicabilityCompany {get;set;}
public String[] productType {get;set;}
public String[] ataChapter {get;set;}
public String queryString {get;set;}*/
    
    public String operationContext = '';
    public String[] productFamily = new String[]{};
        public String[] motivationForSales = new String[]{};
            public String[] applicabilityCompany = new String[]{};
                public String[] productType = new String[]{};
                    public String[] ataChapter = new String[]{};
                        public String queryString = '';
    //public String labelApplicability = '';
    
    public CFCSidebarComponentController() {
        
        CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults();
        lblApplicability = cs.Applicability_Company__c;
        lblFleetType = cs.Fleet_Type__c;
        lblOperationContext = cs.Customer_Phase__c;
        lblProductFamily = cs.Family__c;
        lblAtaChapter = cs.ATA_Chapter__c;
        
        System.debug('CFCSidebarComponentController construtor Inicio');            
        /*operationContext = '';
productFamily = new String[]{};
motivationForSales = new String[]{};
applicabilityCompany = new String[]{};
productType = new String[]{};
ataChapter = new String[]{};
queryString = '';*/
    }
    
    
    public PageReference filter() {  
        system.debug('CFCSidebarComponentController.Filter()');
        
        /*String qsProductCategory = ApexPages.currentPage().getParameters().get('productCategory');
String qsFleetType = ApexPages.currentPage().getParameters().get('fleetType');
String qsOperationContext = ApexPages.currentPage().getParameters().get('operationContext');
String qsProductFamily = ApexPages.currentPage().getParameters().get('productFamily');
String qsMotivationForSales = ApexPages.currentPage().getParameters().get('motivationForSales');
String qsApplicabilityCompany = ApexPages.currentPage().getParameters().get('applicabilityCompany');
String qsProductType = ApexPages.currentPage().getParameters().get('productType');
String qsAtaChapter = ApexPages.currentPage().getParameters().get('ataChapter');*/
        
        system.debug('CFCSidebarComponentController.filter.fleetType >>> ' + fleetType);
        String queryResult = '';
        if (fleetType != null && !fleetType.equalsIgnoreCase('')){
            fleetType = generateParamFleetType(fleetType);
            queryResult += '&fleetType=' +  fleetType;
        }    
        if(operationContext != null && operationContext != '') {
            system.debug('CFCSidebarComponentController.pageReference.operationContext.queryResult1 ' + queryResult);
            queryResult += '&operationContext=' +  operationContext;
        }
        if(productFamily != null && productFamily.size() > 0){
            system.debug('CFCSidebarComponentController.pageReference.productFamily.queryResult1 ' + queryResult);
            queryResult += '&productFamily=';
            String productFamilyResult = '';
            for(String item : productFamily){
                system.debug('CFCSidebarComponentController.pageReference.productFamily.queryResult2 antes do tratamento ' + queryResult);
                productFamilyResult += item + ';';
            }
            queryResult += productFamilyResult.replaceAll('[&]', '%26');
            queryResult = queryResult.substring(0, queryResult.length()-1);
        }
        if(motivationForSales != null && motivationForSales.size() > 0){
            system.debug('CFCSidebarComponentController.pageReference.motivationForSales.queryResult ' + queryResult);
            queryResult += '&motivationForSales=';
            for(String item : motivationForSales){
                queryResult += item + ';';
            }
            queryResult = queryResult.substring(0, queryResult.length()-1);
        }
        if(applicabilityCompany != null && applicabilityCompany.size() > 0 ){
            system.debug('CFCSidebarComponentController.pageReference.applicabilityCompany.queryResult ' + queryResult);
            queryResult += '&applicabilityCompany=';
            for(String item : applicabilityCompany){
                queryResult += item + ';';
            }
            queryResult = queryResult.substring(0, queryResult.length()-1);
        } 
        if(productType != null && productType.size() > 0 ){
            system.debug('CFCSidebarComponentController.pageReference.productType.queryResult ' + queryResult);
            queryResult += '&productType=';
            String productTypeResult = '';
            
            for(String item : productType){
                productTypeResult += item + ';';
            }
            queryResult += productTypeResult.replaceAll('[&]', '%26');
            queryResult = queryResult.substring(0, queryResult.length()-1);
        }
        if(ataChapter != null && ataChapter.size() > 0 ){
            system.debug('CFCSidebarComponentController.pageReference.ataChapter ' + ataChapter);
            system.debug('CFCSidebarComponentController.pageReference.ataChapter.queryResult ' + queryResult);
            queryResult += '&ataChapter=';
            
            for(String item : ataChapter){
                system.debug('CFCSidebarComponentController.pageReference.ataChapter.item' + item);
                queryResult += item + ';';
            }
            queryResult = queryResult.substring(0, queryResult.length()-1);
        }
        
        string searchFilter = '&p=' + ApexPages.currentPage().getParameters().get('q');
        if(searchFilter != '&p=' && searchFilter != '&p=null'){
            queryResult += searchFilter;
        }
        
        string productCategory = '&productCategory=' + ApexPages.currentPage().getParameters().get('productCategory');
        if(productCategory != '&productCategory=' && productCategory != '&productCategory=null'){
            queryResult += productCategory;
        }
        
        system.debug('CFCSidebarComponentController.pageReference.queryResult ' + queryResult);
        PageReference page;
        page = new PageReference('/apex/CFCSearchResult?' + queryResult );
        page.setRedirect(true);
        
        system.debug('CFCSidebarComponentController.pageReference.filter fim');
        return page;  
        
    }    
    
    public List<SelectOption> getOperationItems() {
        System.debug('CFCSidebarComponenetController.getOperationItems inicio');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();
        
        for(Schema.Picklistentry item : Product2.fields.Customer_Phase__c.getDescribe().getpicklistvalues()) {
            //options.add(new SelectOption(item.getLabel().toLowerCase(),item.getValue().toLowerCase()));
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        System.debug('CFCSidebarComponenetController.getOperationItems Fim');             
        return options;         
    }    
    
    public List<SelectOption> getProductFamilyItems() {
        System.debug('CFCSidebarComponenetController.getProductFamilyItems inicio');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();
        
        for(Schema.Picklistentry item : Product2.fields.Family.getDescribe().getpicklistvalues()) {
            //options.add(new SelectOption(item.getLabel().toLowerCase(),item.getValue().toLowerCase()));
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        System.debug('CFCSidebarComponenetController.getProductFamilyItems Fim');             
        return options;         
    }
    
    public List<SelectOption> getApplicabilityCompanyItems() {
        System.debug('CFCSidebarComponenetController.getApplicabilityCompanyItems inicio');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();
        
        for(Schema.Picklistentry item : Product2.fields.Applicability_company__c.getDescribe().getpicklistvalues()) {
            //options.add(new SelectOption(item.getLabel().toLowerCase(),item.getValue().toLowerCase()));
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        System.debug('CFCSidebarComponenetController.getApplicabilityCompanyItem Fim');             
        return options;         
    } 
    
    public List<SelectOption> getProductTypeItems() {
        System.debug('CFCSidebarComponenetController.getProductTypeItems inicio');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();
        
        for(Schema.Picklistentry item : Product2.fields.Product_Type__c.getDescribe().getpicklistvalues()) {
            //options.add(new SelectOption(item.getLabel().toLowerCase(),item.getValue().toLowerCase()));
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        System.debug('CFCSidebarComponenetController.getProductTypeItems Fim');             
        return options;         
    }  
    
    public List<SelectOption> getAtaChapterItems() {
        System.debug('CFCSidebarComponenetController.getAtaChapterItems inicio');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();
        
        for(Schema.Picklistentry item : Product2.fields.ATA_Chapter__c.getDescribe().getpicklistvalues()) {
            //options.add(new SelectOption(item.getLabel().toLowerCase(),item.getValue().toLowerCase()));
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        System.debug('CFCSidebarComponenetController.getAtaChapterItems Fim');             
        return options;         
    }      
    
    public String getOperationContext() {
        return operationContext;
    }
    
    public void setOperationContext(String operationContext) {
        this.operationContext = operationContext;     
    }
    
    public String[] getProductFamily() {
        return productFamily;
    }
    
    public void setProductFamily(String[] productFamily) {
        this.productFamily = productFamily;     
    }   
    
    public String[] getApplicabilityCompany() {
        return applicabilityCompany;
    }
    
    public void setApplicabilityCompany(String[] applicabilityCompany) {
        this.applicabilityCompany = applicabilityCompany;     
    }
    
    public String[] getProductType() {
        return productType;
    }
    
    public void setProductType(String[] productType) {
        this.productType = productType;     
    }   
    
    public String[] getAtaChapter() {
        return ataChapter;
    }
    
    public void setAtaChapter(String[] ataChapter) {
        this.ataChapter = ataChapter;     
    }   
    
    public String generateParamFleetType(string param){
        if(param == 'ErjFamily'){return 'ERJ';}
        else if(param == 'EjetsFamily'){ return 'EJET';}
        else if(param == 'E-JETSE2FAMILY'){ return 'E-JET E2';}
        else if(param == 'TURBOPROP'){return 'TURBOPROP';}
        else { return null;}
    }    
}