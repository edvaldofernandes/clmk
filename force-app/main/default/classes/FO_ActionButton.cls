/**
* @description This class represents an generic action button on a record detail page.
**/
public abstract class FO_ActionButton {
    
    public Id recordId{
        public get;
        public set;
    }
    
    /**
    * @description The class constructor. It gets the current record Id.
    * 
    * @param ApexPages.StandardController stdController : The standard controller.
    **/
    public FO_ActionButton(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
    } 
    
    public abstract void doAction();
    
    /**
    * @description Do the action and returns to its record page.
    * 
    * @return PageReference : An page reference.
    **/
    public PageReference redirect(){
        doAction();
		PageReference pageRef = new PageReference(this.getPageReferenceUrl());
        pageRef.setRedirect(true);    
        return pageRef;
    }
    
    /**
    * @description Creates the URL to which the user will be redirected.
    * 
    * @param ApexPages.StandardController stdController : The standard controller.
    **/    
    public virtual String getPageReferenceUrl(){
        return '/' + recordId;
    }
}