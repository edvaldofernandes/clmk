/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class OpportunityDeleteRelatedAircraft
*
* NAME: OpportunityDeleteRelatedAircraftTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*******************************************************************************/
@isTest
private class OpportunityDeleteRelatedAircraftTest {

 private static final Integer LOTE = 200;

    static testMethod void myUnitTest() {
       Opportunity opp = SObjectInstanceTest.createOpportunity();
       Database.insert(opp);
       
       Product2 prod = SObjectInstanceTest.createProduct2();
       Database.insert(prod);
       
       PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(SObjectInstanceTest.catalogoDePrecoPadrao(), prod.Id);
       Database.insert(pbe);
       
       OpportunityLineItem oli = SObjectInstanceTest.createOppItem(opp.Id, pbe.Id);
       Database.insert(oli);
       
       Aircraft__c air = SObjectInstanceTest.createAircraft();
       Database.insert(air);
       
       Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
       rel.Opportunity_product_id__c = oli.Id;
       rel.Opportunity__c = opp.Id;
       Database.insert(rel);
       
       Test.startTest();
       Database.delete(opp);
       Test.stopTest();
       
       List<Related_Aircraft__c> lstRel = [SELECT Id from Related_Aircraft__c WHERE Opportunity__c =: opp.Id];
       system.assert(lstRel.isEmpty());
    }
    
    static testMethod void lote() {
      
      List<Opportunity> lstOpp = new List<Opportunity>();
      for (Integer i = 0; i < LOTE ; i++ )
      {
        lstOpp.add(SObjectInstanceTest.createOpportunity());
      }
      Database.insert(lstOpp);
       
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
       
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(SObjectInstanceTest.catalogoDePrecoPadrao(), prod.Id);
      Database.insert(pbe);
     
      List<OpportunityLineItem> lstOli = new List<OpportunityLineItem>();  
      for (Integer i = 0; i < LOTE ; i++ )
      {
        lstOli.add(SObjectInstanceTest.createOppItem(lstOpp.get(i).Id, pbe.Id));
      }
      Database.insert(lstOli);
       
      Aircraft__c air = SObjectInstanceTest.createAircraft();
      Database.insert(air);
      
      List<String> lstIds = new List<String>(); 
      List<Related_Aircraft__c> lstRel = new List<Related_Aircraft__c>();  
      for (Integer i = 0; i < LOTE ; i++ )
      {
        Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
        rel.Opportunity_product_id__c = lstOli.get(i).Id;
        rel.Opportunity__c = lstOpp.get(i).Id;
        lstIds.add(lstOli.get(i).Id);
        lstRel.add(rel);
      }
      Database.insert(lstRel);
       
      Test.startTest();
      Database.delete(lstOpp);
      Test.stopTest();
       
      List<Related_Aircraft__c> lstRelResult = [SELECT Id from Related_Aircraft__c WHERE Opportunity__c =: lstOpp];
      system.assert(lstRelResult.isEmpty());
    }
}