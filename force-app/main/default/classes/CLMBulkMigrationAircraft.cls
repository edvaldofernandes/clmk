global class CLMBulkMigrationAircraft implements Database.Batchable<sObject>, Database.Stateful {
    
    
    global List<CLMMigration.CLMMigrationErroModel> listMigration = new List<CLMMigration.CLMMigrationErroModel>();
    
    global Database.Querylocator start( Database.BatchableContext BC ){
        
        return Database.getQueryLocator([SELECT Id,IsDeleted,Name,CurrencyIsoCode,CreatedDate,CreatedById,
                                         LastModifiedDate,LastModifiedById,SystemModstamp,LastActivityDate,
                                         Apttus__AgreementId__c,Apttus__Description__c,Apttus__ExtendedPrice__c,
                                         Apttus__ListPrice__c,Apttus__NetPrice__c,Apttus__ProductId__c,
                                         Apttus__Quantity__c,AFA__c,APU__c,Actual_Delivery_Date__c,
                                         Actual_Delivery_Start_Date__c,Aircraft_Basic_Price_EC__c,
                                         Aircraft_Certification_Designation__c,Aircraft_Configuration_Code__c,
                                         Aircraft_Configuration__c,Aircraft_Delivery__c,Aircraft_Family__c,
                                         Aircraft_ID__c,Aircraft_Model__c,Aircraft_Name__c,Aircraft_Price__c,
                                         Aircraft_Purchase_Price_EC__c,Aircraft_Purchase_Price__c,
                                         Aircraft_Version__c,Aircraft__c,Airport_of_Destination__c,
                                         Ato_Concessorio__c,Auto_Update_Trend_Date__c,Balance__c,Basic_Price__c,
                                         Bill_to_Aircraft__c,Bill_to_Kit__c,Book_Status__c,ChaveDF__c,
                                         Comments_Delivery_Details__c,Comments_Price__c,Comments__c,
                                         Contract_Administrator__c,Contract_Aircraft_Number__c,
                                         Contract_Engineer__c,Contractual_Delivery_First_Month_Day__c,
                                         Counter__c,Current_Snapshot_Version__c,Customs_Clearance_At__c,
                                         DF_Code__c,DF_Country_Code__c,Agr_Status__c,Date_to_Exercise_Option__c,
                                         Delivered__c,Delivery_Coordinator__c,Delivery_Date__c,Delivery_Pilot__c,
                                         Delivery_SEC_Score__c,Dellivery_Month__c,Discount_or_Increase__c,
                                         Escalated_Price__c,Escalation_Factor__c,Escalation__c,
                                         Estimated_Closing_Date__c,Exercised_Date__c,Expiration_Date__c,
                                         Ferry_Flight_Pilot_1__c,Ferry_Flight_Pilot_2__c,Final_Price__c,
                                         Firm_Cancelled__c,Firm_Cancelled_and_Transfered__c,Firm_Included__c,
                                         Firm_Terminated__c,Firm_Transfered__c,First_Notification__c,
                                         Foreign_Registration1__c,Foreign_Registration__c,/*Goal__c,*/
                                         Included_Event_Confirmed__c,Initial_Airworthiness_Team__c,
                                         Inspection_Week_First_Day__c,Interest_Amount__c,Invoice_Interest_Amount__c,
                                         Invoice_No_Charge__c,Invoice_PDP_Return__c,Invoice__c,Last_Notification__c,
                                         Left_Engine__c,Lessee_Defined__c,List_Price__c,MFA_Score__c,
                                         MFIR_Bill_to_Aircraft__c,MFIR_Bill_to_Kit__c,MFIR_Kit_Bill_To__c,
                                         MFIR_Kit_Ship_To__c,MFIR_Ship_to_Aircraft__c,MFIR_Ship_to_Kit__c,MSN__c,
                                         Material_Code__c,No_Charge_Amount__c,Operator_Aircraft_Number__c,
                                         /*Option_Cancelled__c,*/Option_Execution_Date__c,Option_Expiration_Date__c,
                                         Option_In_Execution__c,/*Option_Included__c,Option_Terminated__c,*/
                                         Option_Under_negotiation__c,/*Option_to_Firm__c,Option_to_PRA__c,*/
                                         Order_Type_Original__c,Order_Type__c,Original_Order_Type__c,
                                         Outbound_Delivery__c,PDP_Already_set__c,PDP_Return__c,PEP_Aircraft__c,
                                         PEP_Delivery__c,PEP_Preservation__c,PEP_Warranty__c,/*PRA_Cancelled__c,
                                         PRA_Execution_Date__c,PRA_Expiration_Date__c,PRA_Included__c,
                                         PRA_Terminated__c,PRA_Under_negotiation__c,PRA_to_Firm__c,PRA_to_Option__c,*/
                                         Production_SEC_Score__c,/*Production_Support_Engineer__c,*/Progress_Bar__c,
                                         Quality_Documentation__c,Quality_Focal_Point_1st_Shift__c,
                                         Quality_Focal_Point_2nd_Shift__c,RC__c,RE__c,Registration_Mark__c,
                                         Related_Agreement_Line_Item__c,Right_Engine__c,Risk__c,SAP_Batch__c,
                                         SAP_Contract__c,SAP_Item__c,Scheduled_Inspection_Date__c,Send_Email__c,
                                         Ship_to_Aircraft__c,Ship_to_Kit__c,Show_Aircraft_As__c,/*Skyline_Goal__c,*/
                                         Sold_to__c,Status__c,Stop_Escalation__c,Summary_SKYLINE__c,
                                         TREND_Country_Code__c,TREND_DF_Code__c,TREND_Delivery_Date__c,
                                         Take_off_Date_Time__c,Technical_Leader_1st_Shift__c,
                                         Technical_Leader_2nd_Shift__c,Temporary_Brazilian_Registration__c,
                                         Undisclosed_Aircraft__c,Agreement_RT_Name__c,IDAircraftSAP__c,
                                         Data_do_Ato_Concessorio__c,AC_Version__c,CompanyType__c,
                                         SkylineSummaryCalculated__c,DeliveryMonthYearReference__c,
                                         Final_SEC_Delivery__c,AC_Order__c,Final_SEC_Production__c,
                                         Calc_Expir_date__c,Contract_Buyer__c,Customer_Nickname__c 
                                         FROM Apttus__AgreementLineItem__c 
                                         ORDER BY CreatedDate]);    
    }
    
    global void execute( Database.BatchableContext BC, List<sObject> scope ){
        List<Apttus__AgreementLineItem__c> listApptAircrft = (List<Apttus__AgreementLineItem__c>) scope;
        
        List<Agreement_Aircraft__c> listAircraft = new List<Agreement_Aircraft__c>();
        
        Set<String> existingAC = new Set<String>();

        List<Agreement_Aircraft__c> acList = new List<Agreement_Aircraft__c>([SELECT Apptus_Aircraft_ID__c FROM Agreement_Aircraft__c WHERE Apptus_Aircraft_ID__c <> NULL OR Apptus_Aircraft_ID__c <> '']);
        
        for(Agreement_Aircraft__c ac : acList){
            existingAC.add(ac.Apptus_Aircraft_ID__c);
        }
        
        for(Apttus__AgreementLineItem__c obj : listApptAircrft){
            Agreement_Aircraft__c aggAir = CLMMigration.apttusToCommercialAircraft(obj);
            
            if(!existingAC.contains(aggAir.Apptus_Aircraft_ID__c)){
               listAircraft.add(aggAir); 
            }
            	
        }
        
        Schema.DescribeFieldResult objField = Agreement_Aircraft__c.Apptus_Aircraft_ID__c.getDescribe();
        Schema.sObjectField extField = objField.getSObjectField();
        
        Database.UpsertResult[] srList = Database.upsert(listAircraft,extField,false);
        
        for (Database.UpsertResult sr : srList) {
            
            CLMMigration.CLMMigrationErroModel migErro = new CLMMigration.CLMMigrationErroModel();
            migErro.recordId = sr.getId();
            
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                //System.debug('Successfully inserted . Aircraft Id: ' + sr.getId());
                migErro.erroMsg = 'Successfully Inserted';
            }else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    
                    migErro.recordId = sr.getId();
                    migErro.erroCode = err.getStatusCode();
                    migErro.erroMsg = err.getMessage();
                    migErro.listFields = err.getFields();
                    
                    //System.debug('The following error has occurred.');                    
                    //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    //System.debug('Agreement fields that affected this error: ' + err.getFields());
                }
            }
            this.listMigration.add(migErro);
        }
    }
    
    global void finish( Database.BatchableContext bcMain ){
        
        Messaging.EmailFileAttachment csv = new Messaging.EmailFileAttachment();
        //system.debug('listMigration: ' + listMigration);
        blob xlsxBlob = Blob.valueOf(JSON.serialize(this.listMigration));     
        csv.setFileName('AgreementAircraftMigration.json');
        csv.setBody(xlsxBlob);
        csv.setContentType('text/json');  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'felipe.inacio@embraer.com.br'};
        mail.setToAddresses(toAddresses);        
        mail.setSubject('Agreement Aircraft Migration');     
        mail.setPlainTextBody('Anexo resultado ');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csv});            
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
    
}