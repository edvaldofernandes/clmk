/*
----------------------------------------------------------------------------------------------
-- - Company:     Stefanini
-- - Name:        CRC_CaseDataAccess
-- - Description: Class responsible for retrieving data regarding CRC Dashboard header
-- - @Author: Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version
----------------------------------------------------------------------------------------------
-- 29/03/2019       Felipe Gouvea      				1.0
-- 06/05/2019		Tiago de Jesus Rodrigues		1.1
-- 20/05/2019		Tiago de Jesus Rodrigues		1.2
-- 19/11/2019		Fabiano Albino Ferreira			1.3
----------------------------------------------------------------------------------------------
*/

public class CRC_CaseDataAccess {


	public static Date firstDayOfMonth = System.Date.today().toStartOfMonth();
    public static string CRCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();
	public static string SPAREPARTS_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Spare Parts Price').getRecordTypeId();
	public static string BACKORDER_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Backorder').getRecordTypeId();


	// Method that queries the number of CRC cases created yesterday
	public static integer howManyCasesWereCreatedYesterday(){
		AggregateResult query = [SELECT count(Id) FROM Case
								 WHERE CreatedDate = YESTERDAY
            					 AND RecordTypeId =: CRCRecordType];
		Integer result = (Integer) query.get('expr0');
		return result;
	}

	// Method that queries the number of CRC cases closed yesterday
	public static integer howManyCasesWereClosedYesterday(){
		AggregateResult query = [SELECT count(Id) FROM Case
								 WHERE IsClosed = TRUE
                                 AND ClosedDate = YESTERDAY
                                 AND RecordTypeId =: CRCRecordType];
		Integer result = (Integer) query.get('expr0');
		return result;
	}

	// Method that queries the number of CRC cases created today
	public static integer howManyCasesWereCreatedToday(){
		AggregateResult query = [SELECT count(Id) FROM Case
                                 WHERE CreatedDate = TODAY
                                 AND RecordTypeId =: CRCRecordType];
		Integer result = (Integer) query.get('expr0');
		return result;
	}

	// Method that queries the number of CRC cases closed today
	public static integer howManyCasesWereClosedToday(){
		AggregateResult query = [SELECT count(Id) FROM Case
                                 WHERE IsClosed = TRUE
                                 AND ClosedDate = TODAY
                                 AND RecordTypeId =: CRCRecordType];
		Integer result = (Integer) query.get('expr0');
		return result;
	}

	// Method that queries the number of CRC cases with a specific status
	public static integer howManyOpenCasesHaveInboxStatus(){
		AggregateResult query = [SELECT count(Id) FROM Case
								 WHERE IsClosed = FALSE
                                 AND Status = 'Dispatch'
            					 AND RecordTypeId =: CRCRecordType
		];
		Integer result = (Integer) query.get('expr0');
		return result;
	}

    public static integer howManyOpenCasesAreFUP(){
		
        List<string> caseStatus = new List<string>{
			'Back order','Contract Creation','Engineering','Financial',
			'Follow Up','New Customer','Pricing','RTS','Warehouse'
		};

        AggregateResult query = [SELECT count(Id) FROM Case
								 WHERE IsClosed = FALSE
                                 AND Status In :caseStatus
            					 AND RecordTypeId =: CRCRecordType
		];

		Integer result = (Integer) query.get('expr0');

		return result;
    }

	public static integer howManyNextActionsToday(){

        List<string> caseStatus = new List<string>{
			'Back order','Contract Creation','Engineering','Financial',
			'Follow Up','New Customer','Pricing','RTS','Warehouse'
		};

        DateTime currentTime = System.now();

		AggregateResult query = [SELECT count(Id) FROM Case
								 WHERE IsClosed = FALSE
								 AND Status In :caseStatus
								 AND RecordTypeId =: CRCRecordType
								 AND Next_Action_Required_Date_and_Time__c >= :currentTime
								 AND Next_Action_Required_Date_and_Time__c = TODAY
		];

		Integer result = (Integer) query.get('expr0');

		return result;
    }

	//Withdraw condition LastModifiedDate  = THIS_MONTH
	public static integer howManyNextActionsExpired(){

        List<string> caseStatus = new List<string>{
			'Back order','Contract Creation','Engineering','Financial',
			'Follow Up','New Customer','Pricing','RTS','Warehouse'
		};

		DateTime currentTime = System.now();

		AggregateResult query = [SELECT count(Id) 
								 FROM Case
								 WHERE IsClosed = FALSE
								 AND Status In :caseStatus
								 AND RecordTypeId =: CRCRecordType
								 AND Next_Action_Required_Date_and_Time__c < :currentTime
		];

		Integer result = (Integer) query.get('expr0');

		return result;
    }

	// Withdraw condition LastModifiedDate  = THIS_MONTH
	public static integer howManyNextActionsFuture(){

        List<string> caseStatus = new List<string>{
			'Back order','Contract Creation','Engineering','Financial','Follow Up',
			'New Customer','Pricing','RTS','Warehouse'
		};

		AggregateResult query = [SELECT count(Id) 
								 FROM Case
								 WHERE IsClosed = FALSE
								 AND Status In :caseStatus
								 AND RecordTypeId =: CRCRecordType
								 AND Next_Action_Required_Date_and_Time__c > TODAY
		];

		Integer result = (Integer) query.get('expr0');

		return result;
    }

	// Withdraw condition LastModifiedDate  = THIS_MONTH
	public static integer howManyCasesIncomingEmail(){

		Set<String> recordTypes = new Set<String>{SPAREPARTS_RECORDTYPE, BACKORDER_RECORDTYPE};

		System.debug('##recordTypes: ' + recordTypes);

		AggregateResult query = [SELECT count(Id) 
								FROM Case
								WHERE IsClosed = FALSE
								AND Incoming_email_i__c = true
								AND (
									(
										(NOT Status IN ('Closed', 'Processing', 'Pricing', 'Dispatch', 'Back order', 'Awaiting Cust Response'))  
										AND RecordTypeId = :CRCRecordType
									)
									OR
									(
										Status <> 'Closed' 
										AND RecordTypeId IN :recordTypes
									)
									OR
									(
										RecordTypeId = :CRCRecordType
										AND Status IN ('Pricing', 'Back order')
									)
								)
								
		];

		Integer result = (Integer) query.get('expr0');

		return result;
    }

	// Withdraw condition LastModifiedDate  = THIS_MONTH
	public static integer howManyCasesAwaitingCustomerResponse(){

		AggregateResult query = [SELECT count(Id) 
								FROM Case
								WHERE IsClosed = FALSE
								AND Status = 'Awaiting Cust Response'
								AND RecordTypeId =: CRCRecordType
		];

		Integer result = (Integer) query.get('expr0');

		return result;
    }


    // Method that queries the number of CRC cases with status dispatch and yellow SLA
	// Withdraw condition LastModifiedDate  = THIS_MONTH
    public static integer howManyInboxWarning(){

		AggregateResult query = [SELECT count(Id) 
								 FROM Case
                                 WHERE IsClosed = FALSE
                                 AND Status = 'Dispatch'
                                 AND SLA_report_status__c = '2. Yellow'
                                 AND RecordTypeId =: CRCRecordType
		];

		Integer result = (Integer) query.get('expr0');

		return result;
	}

    // Method that queries the number of CRC cases with status dispatch and red SLA
	// Withdraw condition LastModifiedDate  = THIS_MONTH
    public static integer howManyInboxDelayed(){

		AggregateResult query = [SELECT count(Id) FROM Case
                                 WHERE IsClosed = FALSE
                                 AND Status = 'Dispatch'
                                 AND SLA_report_status__c = '3. Red'
                                 AND RecordTypeId =: CRCRecordType
		];

		Integer result = (Integer) query.get('expr0');

		return result;
	}

    // Method that queries the number of CRC case milestones created this month
	public static integer howManyCasesWereReceivedThisMonth(){
		
        List<MilestoneType> MilestoneTypeIds = [SELECT Id 
												FROM MilestoneType 
												WHERE Name = 'CRC 02 RFQ' 
												OR Name = 'CRC 03 PO'
                                              	OR Name = 'CRC 04 Invoice' 
												OR Name = 'CRC 05 Care' 
												OR Name = 'CRC 06 RMA'
                                              	OR Name = 'CRC 07 Follow up' 
												OR Name = 'CRC 08 eOutlet'
		];

		AggregateResult query = [SELECT Count(Id) FROM CaseMilestone
            					 WHERE MilestoneTypeId 
								 IN :MilestoneTypeIds
								 AND StartDate  >= :firstDayOfMonth
		];

		Integer result = (Integer) query.get('expr0');

		return result;
	}

    // Method that queries the number of CRC case milestones created this month that were not violated
	public static integer howManyCasesAreOnTimeThisMonth(){
		List<MilestoneType> MilestoneTypeIds = [SELECT Id 
												FROM MilestoneType 
												WHERE Name = 'CRC 02 RFQ' 
												OR Name = 'CRC 03 PO'
                                              	OR Name = 'CRC 04 Invoice' 
												OR Name = 'CRC 05 Care' 
												OR Name = 'CRC 06 RMA'
                                              	OR Name = 'CRC 07 Follow up' 
												OR Name = 'CRC 08 eOutlet'
		];

		AggregateResult query = [SELECT Count(Id) 
								 FROM CaseMilestone
            					 WHERE MilestoneTypeId IN :MilestoneTypeIds
                                 AND IsViolated = False
								 AND StartDate  >= :firstDayOfMonth
		];

		Integer result = (Integer) query.get('expr0');
		return result;
	}
}