/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* This class is responsible for calculating and updating the field Total_of_REC_and_NREC__c. 
* This field works like a Roll-Up Summary, summarising Percentage_of_proposal_REC__c 
* and Percentage_of_proposal_NREC__c from Service_Contract_Management__c records related 
* to a ContractLineItem.
* When a Service_Contract_Management__c record is created, updated or deleted, the class 
* updates the Total_of_REC_and_NREC__c value of the corresponding ContractLineItem.
*
* NAME: AccReceivablesUpdateContractLineItem.cls
* AUTHOR: DPF                                                DATE: 19/12/2014
*
*******************************************************************************/
public with sharing class AccReceivablesUpdateContractLineItem
{
    
  public static void newServiceContractManagement(List<Service_Contract_Management__c> listNewServiceContractMgmt)
  {
    TriggerUtils.assertTrigger();

    map<Id, list<Service_Contract_Management__c>> mapAccReceivables = new map<Id, list<Service_Contract_Management__c>>();
    for ( Service_Contract_Management__c acsReceivables : listNewServiceContractMgmt )
    {
      if ( ( TriggerUtils.wasChanged(acsReceivables, Service_Contract_Management__c.Percentage_of_proposal_REC__c) || TriggerUtils.wasChanged(acsReceivables, Service_Contract_Management__c.Percentage_of_proposal_NREC__c) )
       && acsReceivables.Service_Contract_line_item__c != null )
      {
        list<Service_Contract_Management__c> lstAccReceivables = mapAccReceivables.get(acsReceivables.Service_Contract_line_item__c);
        if ( lstAccReceivables == null )
        {
          lstAccReceivables = new list<Service_Contract_Management__c>();
          mapAccReceivables.put(acsReceivables.Service_Contract_line_item__c, lstAccReceivables);
        }
        lstAccReceivables.add(acsReceivables);
      }
    }

    if ( mapAccReceivables.isEmpty() ) return;

    list<ContractLineItem> lstCli = new list<ContractLineItem>();
    for ( ContractLineItem cli : [SELECT Total_of_REC_and_NREC__c FROM ContractLineItem
      WHERE Id =: mapAccReceivables.keySet() ])
    {
      Decimal total = cli.Total_of_REC_and_NREC__c != null ? cli.Total_of_REC_and_NREC__c : 0.00;
      list<Service_Contract_Management__c> lstAccReceivables = mapAccReceivables.get(cli.Id);
      if ( lstAccReceivables == null ) continue;
      for ( Service_Contract_Management__c acc : lstAccReceivables )
      {
        total += acc.Percentage_of_proposal_REC__c != null ? acc.Percentage_of_proposal_REC__c : 0.00;
        total += acc.Percentage_of_proposal_NREC__c != null ? acc.Percentage_of_proposal_NREC__c : 0.00;
      }
      cli.Total_of_REC_and_NREC__c = total;
      lstCli.add(cli);
    }
    if ( !lstCli.isEmpty() ) update lstCli;

    if(test.IsRunningTest()) SCMUpdateQuantityContractLineItem.execute();
  }

  public static void deleteServiceContractManagement(List<Service_Contract_Management__c> listOldServiceContractMgmt)
  {
    
    TriggerUtils.assertTrigger();

    map<Id, list<Service_Contract_Management__c>> mapAccReceivables = new map<Id, list<Service_Contract_Management__c>>();
    for ( Service_Contract_Management__c acsReceivables : listOldServiceContractMgmt )
    {
      if ( acsReceivables.Service_Contract_line_item__c == null  ) continue;

      list<Service_Contract_Management__c> lstAccReceivables = mapAccReceivables.get(acsReceivables.Service_Contract_line_item__c);
      if ( lstAccReceivables == null )
      {
        lstAccReceivables = new list<Service_Contract_Management__c>();
        mapAccReceivables.put(acsReceivables.Service_Contract_line_item__c, lstAccReceivables);
      }
      lstAccReceivables.add(acsReceivables);
    }
    if ( mapAccReceivables.isEmpty() ) return;

    list<ContractLineItem> lstCli = new list<ContractLineItem>();
    for ( ContractLineItem cli : [SELECT Total_of_REC_and_NREC__c FROM ContractLineItem
      WHERE Id =: mapAccReceivables.keySet() ])
    {
      Decimal total = cli.Total_of_REC_and_NREC__c != null ? cli.Total_of_REC_and_NREC__c : 0.00;
      list<Service_Contract_Management__c> lstAccReceivables = mapAccReceivables.get(cli.Id);
      if ( lstAccReceivables == null ) continue;
      for ( Service_Contract_Management__c acc : lstAccReceivables )
      {
        total -= acc.Percentage_of_proposal_REC__c != null ? acc.Percentage_of_proposal_REC__c : 0.00;
        total -= acc.Percentage_of_proposal_NREC__c != null ? acc.Percentage_of_proposal_NREC__c : 0.00;
      }
      cli.Total_of_REC_and_NREC__c = total;
      lstCli.add(cli);
    }
    if ( !lstCli.isEmpty() ) update lstCli;

    if(test.IsRunningTest()) SCMUpdateQuantityContractLineItem.execute();
  }

  public static void updateServiceContractManagement(List<Service_Contract_Management__c> listNewServiceContractMgmt, Map<Id, Service_Contract_Management__c> oldMapServiceContractMgmt)
  {
    TriggerUtils.assertTrigger(); 

    map<Id, list<Service_Contract_Management__c>> mapAccReceivables = new map<Id, list<Service_Contract_Management__c>>();
    for ( Service_Contract_Management__c acsReceivables : listNewServiceContractMgmt )
    {
      if ( ( TriggerUtils.wasChanged(acsReceivables, Service_Contract_Management__c.Percentage_of_proposal_REC__c)
       || TriggerUtils.wasChanged(acsReceivables, Service_Contract_Management__c.Percentage_of_proposal_NREC__c) )
       && acsReceivables.Service_Contract_line_item__c != null )
      {
        list<Service_Contract_Management__c> lstAccReceivables = mapAccReceivables.get(acsReceivables.Service_Contract_line_item__c);
        if ( lstAccReceivables == null )
        {
          lstAccReceivables = new list<Service_Contract_Management__c>();
          mapAccReceivables.put(acsReceivables.Service_Contract_line_item__c, lstAccReceivables);
        }
        lstAccReceivables.add(oldMapServiceContractMgmt.get( acsReceivables.id ));
      }
    }
    if ( mapAccReceivables.isEmpty() ) return;

    list<ContractLineItem> lstCli = new list<ContractLineItem>();
    for ( ContractLineItem cli : [SELECT Total_of_REC_and_NREC__c FROM ContractLineItem
      WHERE Id =: mapAccReceivables.keySet() ])
    {
      Decimal total = cli.Total_of_REC_and_NREC__c != null ? cli.Total_of_REC_and_NREC__c : 0.00;
      list<Service_Contract_Management__c> lstAccReceivables = mapAccReceivables.get(cli.Id);
      if ( lstAccReceivables == null ) continue;
      for ( Service_Contract_Management__c acc : lstAccReceivables )
      {
        total -= acc.Percentage_of_proposal_REC__c != null ? acc.Percentage_of_proposal_REC__c : 0.00;
        total -= acc.Percentage_of_proposal_NREC__c != null ? acc.Percentage_of_proposal_NREC__c : 0.00;
      }
      cli.Total_of_REC_and_NREC__c = total;
      lstCli.add(cli);
    }
    if ( !lstCli.isEmpty() ) update lstCli;
  }
}