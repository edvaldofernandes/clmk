/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class for testing and covering the code of the class JSLinkAircraftsToContractLineItem
*
* NAME: JSLinkAircraftsToContractLineItemTest.cls
* AUTHOR: RLdO                                                 DATE: 19/05/2014
*******************************************************************************/
@isTest(seeAllData=true)
private class JSLinkAircraftsToContractLineItemTest 
{
    private static Account acc;
    private static Pricebook2 catalogo;
    private static Product2 produto;
    private static PricebookEntry pbEntry;
    
    static
    {
        catalogo = SObjectInstanceTest.getPricebook2Std();
        
        produto = SObjectInstanceTest.createProduct2();
        database.insert(produto);
        
        pbEntry = SObjectInstanceTest.createPricebookEntry(catalogo.Id, produto.Id);
        database.insert(pbEntry);
        
        acc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Operator'));
        database.insert(acc);
    }
    
    static testMethod void myUnitTest() 
    {
        ServiceContract contrato = new ServiceContract();
        contrato.AccountId = acc.Id;
        contrato.Pricebook2Id = catalogo.Id;
        contrato.Name = acc.Name;
        contrato.DOCCON__c = '123456';
        database.insert(contrato);
        
        ContractLineItem itemContrato = new ContractLineItem();
        itemContrato.ServiceContractId = contrato.Id;
        itemContrato.PricebookEntryId = pbEntry.Id;
        itemContrato.Quantity = 1;
        itemContrato.UnitPrice = 10;
        itemContrato.Catalogue_Price__c = 10;
        itemContrato.Sales_Price__c = 10;
        database.insert(itemContrato);
        
        Aircraft__c aeronave = new Aircraft__c();
        aeronave.RecordTypeId = RecordTypeMemory.getRecType('Aircraft__c', 'Embraer');
        aeronave.Owner__c = acc.Id;
        aeronave.Operator__c = acc.Id;
        aeronave.Name = 'Embraer0';
        aeronave.Aircraft_Status__c = 'In Service';
        aeronave.Commercial_Name__c = 'EMB-110P1';
        database.insert(aeronave);
        
        Test.startTest();
        String sResult = JSLinkAircraftsToContractLineItem.processar(itemContrato.Id);
        Test.stopTest();
        system.assert(![select id from Aircraft_with_this_service__c where Contract_Line_Item__c = :itemContrato.Id].isEmpty());
    }
}