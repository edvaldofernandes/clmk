public class CRM360_OperationalPerformanceController {
  
    public CRM360_OperationalPerformanceController(){
        E2Info = false;
        loadFPData();
	    loadInterruptionData();
    }
    // Gethering Account Info
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private static Account acc = [SELECT Name, Id, COD_ORGz__c FROM Account WHERE Id = :accId];
    
    private Map<String, CRM360_FP_Data__c> FPDataMap = new Map<String, CRM360_FP_Data__c>();
    public String Family {get; set;}
	public String [] keySearch = new String [] {'EMBRAER 190/195 E2 FAMILY'};

    public boolean E2Info {get; set;}
    public boolean HasData {get; set;}
    
    
    private Map<String, List<CRM360_Interruption_Data__c>> interruptionsDataMap = new Map<String, List<CRM360_Interruption_Data__c>>();
    private Map<String, Date> interruptionsDateMap = new Map<String, Date>();

    public Account getAccount (){
        return acc;
    }
    
    public Date getTopIssueDate () {
        return interruptionsDateMap.get(Family);
    }
    public void loadFPData(){
        
        Schema.DescribeFieldResult fieldResult = CRM360_FP_Data__c.FAMILY__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            List<CRM360_FP_Data__c> data = [select 	Name, Account__c, FAMILY__c, AIRCRAFT_IN_SERVICE__c, FH_ACC__c, FC_ACC__c, FH_DAY__c, FC_DAY__c, FH_MONTH__c, FC_MONTH__c,
                                            AIRCRAFT_FLEET_LEADER_FH__c, HOURS_FLEET_LEADER__c, AIRCRAFT_FLEET_LEADER_FC__c,REF_DATE_HOUR__c,REGION_REF_DATE_SR__c,REF_DATE_INTER__c,AVG_FC_MONTH__c, AVG_FH_MONTH__c,
                                            CYCLES_FLEET_LEADER__c, SR_L1M__c, SR_L3M__c, SR_L12M__c, SR_L12M_REGION__c, SR_L12M_WWF__c, CR_L1M__c, CR_L3M__c, CR_L12M__c, CR_L12M_REGION__c, CR_L12M_WWF__c,
                                            WWF_ACFT_IN_SERVICE__c, WWF_AVG_FH_MONTH__c, WWF_AVG_FC_MONTH__c, WWF_FC_DAY__c, WWF_FH_DAY__c
                                            from 		CRM360_FP_Data__c 
                                            where 	FAMILY__c = :f.getValue() AND Account__c = :acc.Id order by CreatedDate DESC limit 1];
            //System.debug('Data ' + f + ' : ' + data);
            if(data.size() > 0){
                FPDataMap.put( f.getValue(), data[0] );
                If(this.Family == null){
                    this.Family = Data[0].FAMILY__c;
                } 

            }
        }
        HasData = !FPDataMap.isEmpty();
        
    }
    
    public void loadInterruptionData(){
        Schema.DescribeFieldResult fieldResult = CRM360_Interruption_Data__c.FAMILY__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                            System.debug('Entrou aqui'+ ple);
        
        

        for( Schema.PicklistEntry f : ple)
        {
            
            List<CRM360_Interruption_Data__c> dateList = new List<CRM360_Interruption_Data__c>();
            dateList= [select 	Account__c, FAMILY__c, DATA_REF__c
                       from 		CRM360_Interruption_Data__c 
                       where 	FAMILY__c = :f.getValue() AND Account__c = :acc.Id order by DATA_REF__c DESC limit 1];
            System.debug('Temp List ' + dateList);
            if(dateList.size() != 0){
                interruptionsDateMap.put( f.getValue(), dateList[0].DATA_REF__c );
            }
            
            
        }
        
        for( Schema.PicklistEntry f : ple)
        {
            List<CRM360_Interruption_Data__c> tempList = new List<CRM360_Interruption_Data__c>();
            tempList= [select 	Account__c, FAMILY__c, RANK__c, FAIL_CODE_DESCRIPTION__c, QTY__c, CONTRIBUTION__c, MONTH__c, DATA_REF__c
                       from 		CRM360_Interruption_Data__c 
                       where 	FAMILY__c = :f.getValue() AND DATA_REF__c =:interruptionsDateMap.get(f.getValue()) AND Account__c = :acc.Id order by CONTRIBUTION__c DESC limit 10];
            System.debug('Temp List ' + tempList);
            if(tempList != null){
                interruptionsDataMap.put( f.getValue(), tempList );
            }
        }
    }
    
    public void checkForE2Family(){
        //E2Info = this.keySearch.contains(this.Family);
        E2Info = false;
    }
    
    public List<CRM360_Interruption_Data__c> getInterruptions(){
        System.debug('return'+ interruptionsDataMap.get(this.Family));
        return interruptionsDataMap.get(this.Family);
    }
    
    public CRM360_FP_Data__c getFPData (){
        checkForE2Family();
        return FPDataMap.get(this.Family);
    }
    
    /*public PageReference updateFamily(){
        if(EvType == 'All'){
            Schema.DescribeFieldResult fieldResult = CalendarofEvents__c.Type_of_Event__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            EvTypes = new List<String>();
            for( Schema.PicklistEntry f : ple)
            {
                EvTypes.add(f.getValue());
            }
            EvTypes.add('');
            
        } else {
            EvTypes.clear();
            EvTypes.add(EvType);
        }
        return null;
    }*/
    
    // Data for Select List 
    /*public List<SelectOption> getFamilies() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('All', 'All'));
        Schema.DescribeFieldResult fieldResult = CRM360_FP_Data__c.FAMILY__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        //System.debug('options' + options);
        return options;
    } */
    
    
    public List<SelectOption> getFamilies() {
        List<SelectOption> options = new List<SelectOption>();
        for( string stg : FPDataMap.keySet())
        {
            options.add(new SelectOption(stg, stg));
        }
        //System.debug('options' + options);
        return options;
    } 
    
}