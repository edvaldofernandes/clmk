public class CRM360_CasesManagementController {

    public CRM360_CasesManagementController(){
        Category = ApexPages.currentPage().getParameters().get('category');
        loadCategories();
        loadCases();
        //this.Category = 'Init';
        //(String) ApexPages.currentPage().getParameters().get('category');
    }
    
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    public String Category {get;set;}
    private Account acc = [SELECT Name, Id, COD_ORGz__c FROM Account WHERE Id = :accId];
    
    private Map<String, List<Case>> casesListMap = new Map<String, List<Case>>();
    private list<String> allCategories = new list<String>();
    
    public Integer caseCount {get; set;}
    public Integer delayedCaseCount {get; set;}
    public Account getAccount (){
        return acc;
    }
    
    public void loadCategories(){
        Schema.DescribeFieldResult fieldResult = Case.CRM360_Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            this.allCategories.add(f.getValue());
        }
    }
    
    public List<SelectOption> getCategories() {
        List<SelectOption> options = new List<SelectOption>();
        for( string stg : allCategories)
        {
            options.add(new SelectOption(stg, stg));
        }
        return options;
    }
    
    public List<Case> getCases() {
        List<Case> caseList = new List<Case>();
        this.caseCount = 0;
        if(this.Category != 'ALL'){
            for(string cat : this.allCategories){
                if(This.Category.contains(cat)){
                    System.debug(cat);
                    System.debug('Cases to add' + casesListMap.get(cat));
                    this.caseCount += casesListMap.get(cat).size();
                    caseList.addAll(casesListMap.get(cat));
                    System.debug('temp Cases' + caseList);
                    
                }
            }    
        }else {
            for(string cat : this.allCategories){
                this.caseCount += casesListMap.get(cat).size();
                caseList.addAll(casesListMap.get(cat));
            }
        }
        delayedCaseCount = 0;
        for(Case c : caseList){
            if(c.IsEscalated == true){
                delayedCaseCount ++;
            }
        }
        return caseList;
    }


    public void loadCases(){
        //system.debug('All Cats' + allCategories);

        for(string category : this.allCategories){
            List<Case> caseList = new List<Case>();
            caseList = [SELECT CaseNumber, Subject, Description, CRM360_view__c, IsEscalated,
                        CRM360_Category__c, CRM_Customer_Satisfaction_Impact__c,
                        CRM_Case_Actual_Commitment_Date__c, OwnerId, Customer_Expectation__c,External_Comments__c, Owner_name__c
                        FROM case
                        WHERE CRM360_view__c = true 
                        AND CRM360_Category__c = : category 
                        AND AccountId = :this.acc.Id 
                        AND Status = 'OPEN'];
            //system.debug('Cases List' +caseList);
            casesListMap.put(category, caseList);
        }
       	System.debug(casesListMap);
    }
}