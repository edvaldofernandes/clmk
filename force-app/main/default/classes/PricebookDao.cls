/* Classe implementadora de SOBjectDAO para operações DML no objeto Pricebook, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 13/01/2016
*/
public without sharing class PricebookDao {
    
    private static final PricebookDao instance = new PricebookDao();    
    
    private PricebookDao(){
    }    
    
    public static PricebookDao getInstance(){
        return instance;
    }
    
    public Pricebook2 getByName(String pricebookName){
        List<Pricebook2> listPricebook = database.query(' Select ' + utils.getAllFields('Pricebook2') + ' FROM Pricebook2 WHERE Name =\''+String.escapeSingleQuotes(pricebookName)+'\' LIMIT 1');
        
        if(listPricebook.size() > 0){
            return listPricebook[0];
        }        
        return null;
                                                                    
    }
    
}