/* Author: Fabiano Albino Ferreira - TCS
 * 
 * Description: Test class for AssignContact
 * 
 * CreatedDate: 06/12/2019.
 * 
 * LastModifiedDate: 06/12/2019
 */

@IsTest
public class AssignContactTest {
    
    @TestSetup
    private static void testSetup(){
        
        Account acc = createAccount();
        Database.insert(acc);
        
        Contact contact = createContact(acc.Id);
        Database.insert(contact);
        
    }
    
    @IsTest
    private static void shouldAssignContact2(){
        Case c = createCase();
        insert c;
        
        Test.startTest();
        
        new AssignContact(new List<Case>{c}).assignContact();
        Test.stopTest();
    }
    
    @IsTest
    private static void shouldAssignContact(){
        
        Case c = createCase();
        
        Contact contact = [SELECT Id FROM Contact WHERE Email = 'aogdesk@aircanada.ca'];
        
        Test.startTest();
        
        Database.insert(c);
        
        Test.stopTest();
        
        Case caseWithContactId = [SELECT Id, ContactId, AccountId FROM Case WHERE Id = :c.Id];
        
        System.assertEquals(contact.Id, caseWithContactId.ContactId);
        System.assert(String.isNotBlank(caseWithContactId.AccountId));
    }
    
    private static Account createAccount(){
        
        Account acc = new Account(
            BillingCountry = 'Brazil',
            Name = 'Test',
            Company_Nickname__c = 'testNickname',
            FlyEmbraerId__c = '99999'
        );
        
        return acc;
    }
    
    private static Contact createContact(Id accountId){
        
        Contact contact = new Contact(
            FirstName = 'Test-First',
            LastName = 'Test-Second',
            AccountId = accountId,
            Email = 'aogdesk@aircanada.ca'
        );
        
        return contact;
    }
    
    private static Case createCase(){
        
        Case c = new Case(
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRC').getRecordTypeId(),
            Status = 'New',
            Due_Date__c = Date.today().addMonths(1),
            Subject = 'NEW SO:SCH -AIR CANADA - 161861',
            Study_Type__c = 'Performance',
            Type = 'TBC - To be classified',
            Phase_c__c = 'Dispatch',
            Status__c = 'Dispatch.',
            Next_Action_required_details__c = 'Required Details',
            Next_Action_Required_Date_and_Time__c = System.now(),
            Requested_By__c = UserInfo.getUserId(),
            Description = 'Customer Number:0000335353 * Customer Contact Mail: aog@aa.com ='
        );
        
        return c;
    }
    
}