@isTest
private class PlanningReportLightningTest {
	@isTest static void testGetCompiledSearchResults_FLL(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string ecode = '7430';
        string regionAndSegment = 'FLL COM';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledSearchResults(ecode, regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 1;
        System.assertEquals(expectedResult, results.size());
    }
    
    @isTest static void testGetCompiledSearchResults_LBG(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string ecode = '7430';
        string regionAndSegment = 'LBG COM';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledSearchResults(ecode, regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 1;
        System.assertEquals(expectedResult, results.size());
    }
    
    @isTest static void testGetCompiledSearchResults_BlankInput(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string ecode = '';
        string regionAndSegment = 'LBG COM';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledSearchResults(ecode, regionAndSegment);
        Test.stopTest();
        
        System.assertEquals(null, results);
    }
    
    @isTest static void testGetCompiledSearchResults_NoMatch(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string ecode = '123456789';
        string regionAndSegment = 'LBG COM';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledSearchResults(ecode, regionAndSegment);
        Test.stopTest();
        
        System.assertEquals(null, results);
    }
    
    @isTest static void testGetCompiledSearchResults_BlankRegion(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string ecode = '7430';
        string regionAndSegment = '';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledSearchResults(ecode, regionAndSegment);
        Test.stopTest();
        
        System.assertEquals(null, results);
    }
    
    @isTest static void testGetStockInfosByRegionAndSegment_BlankRegion(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        List<String> ecodes = new List<String>{'7430'};
        string regionAndSegment = '';
        List<Stock_Info_FLL__c> results = PlanningReportController.getStockInfosByRegionAndSegment(ecodes, regionAndSegment);
        Test.stopTest();
        
        System.assertEquals(null, results);
    }
    
    @isTest static void testGetCompiledDataByRegion_FLL(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = 'FLL COM';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledDataByRegion(regionAndSegment);
        Test.stopTest();
        
        integer expectedResults = 1;
        System.assertEquals(expectedResults, results.size());
    }
    
    @isTest static void testGetCompiledDataByRegion_LBG(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = 'LBG COM';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledDataByRegion(regionAndSegment);
        Test.stopTest();
        
        integer expectedResults = 1;
        System.assertEquals(expectedResults, results.size());
    }
    
    @isTest static void testGetCompiledDataByRegion_BlankRegion(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = '';
        List<PlanningReportController.StockCoresAndRepairsWrapper> results = PlanningReportController.getCompiledDataByRegion(regionAndSegment);
        Test.stopTest();
        
        System.assertEquals(null, results);
    }
}