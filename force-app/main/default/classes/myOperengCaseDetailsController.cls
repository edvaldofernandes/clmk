public with sharing class myOperengCaseDetailsController {
    public static String EMAIL_DOMAIN_REGEX = '@[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9-]+)*';

    @AuraEnabled
    public static List<Case> getCaseDetails(Id caseId) {
        //First, check if user has access
        User[] u = [SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        Contact[] contact = [SELECT AccountId FROM Contact WHERE Id =: u[0].ContactId LIMIT 1];
        if(contact.size()>0){ 
            Account account = [SELECT Name,Id,Geographical_Area__c, FlightOps_Account_Domains__c FROM Account WHERE Account.Id =: contact[0].AccountId];
        	return [SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description FROM Case WHERE Case.Id =: caseId  AND RecordType.Name = 'FlightOps Case Record Type' AND Case.AccountId =: account.Id LIMIT 1];        	
        } else {
            List<Case> noCases = new List<Case>();
        	return noCases;
        } 
    }

    @AuraEnabled
    public static String getUserName() {
        Contact userContact = [
            SELECT
                Id,
                Name
            FROM
                Contact
            WHERE
                Id IN (
                    SELECT
                        ContactId
                    FROM
                        User
                    WHERE
                        Id =: UserInfo.getUserId()
                )
            LIMIT 1
        ];
        return userContact.Name;
    }
    
    //Get Comments for a selected case
    @AuraEnabled
    public static List<CaseComment> getCaseComments(Id caseId){
        //Getting Comments for Case
        return [SELECT Id, ParentId, IsPublished, CommentBody, CreatedById, CreatedDate, SystemModstamp, LastModifiedDate, LastModifiedById, IsDeleted FROM CaseComment WHERE isPublished = true AND isDeleted = false AND ParentId=: caseId ORDER BY CreatedDate DESC];
    }
    
    @AuraEnabled
    public static Boolean getContainEmbraer(List<String> emailList) {
        for(String e : emailList){
            if(e.toLowerCase().normalizeSpace().contains('embraer')){
                return true;        	
            } else {
                return false;
            }
        }
        return false;
    }
    /* GET CASE EMAILS */
    @AuraEnabled
    public static List<EmailMessage> getCaseEmails(Id caseId){
        /*
         * 	Returns the e-mails related to the case's inputted Id
         */
        Account companyAccount = getUserAccount();
        Set<String> emailDomains = getAccountEmailDomains(companyAccount);
        List<EmailMessage> caseEmails = getCaseEmailMessagesByDomain(caseId, emailDomains);
        return caseEmails;
    }
    public static Set<String> getAccountEmailDomains(Account a){
        /*
         * 	Returns the e-mail domains from an account
         */
        Set<String> domains = new Set<String>();
        
        if(!String.isBlank(a.FlightOps_Account_Domains__c)){
            domains = splitEmailDomains(a.FlightOps_Account_Domains__c);
        }else{
            String contactEmailAddress = [
                SELECT
            		Email
                FROM
                	Contact
                WHERE
                	Id IN (
                    	SELECT
                        	ContactId
                        FROM
                        	User
                        WHERE
                        	Id =: UserInfo.getUserId()
                    )
                LIMIT 1
            ].Email; 
            if(!String.isBlank(contactEmailAddress)){
            	String domain = '@' + contactEmailAddress.split('@')[1];
                domains.add(domain);
            }else{
                system.debug('Can\'t find any valid E-mail for the user : ' + UserInfo.getUserId());
                return null; 
//            throw new Exception('Can\'t find any valid E-mail for the user : ' + UserInfo.getUserId());
            }
        }
        return domains;
    } 
    public static Set<String>splitEmailDomains(String emailDomains){
         /*
         * 	Gets only the domain from an e-mail address
         */
        Set<String> domains = new Set<String>();
        Pattern emailDomainPattern = Pattern.compile(EMAIL_DOMAIN_REGEX);
        matcher domainMatcher = emailDomainPattern.matcher(emailDomains);
        while(domainMatcher.find()){
        	domains.add(domainMatcher.group());
        }
        return domains;
    }
    public static Account getUserAccount(){
         /*
         * 	Returns the user's account
         */
        return [
            SELECT 
                contact.account.Name,
                contact.account.Id,
            	contact.Email,
                contact.account.FlightOps_Account_Domains__c
            FROM
                User
            WHERE
                Id =: UserInfo.getUserId()
                OR contact.Email =: UserInfo.getUserEmail()
        	LIMIT 1
    	].contact.account;
    }
    public static List<EmailMessage> getCaseEmailMessagesByDomain(Id caseId, Set<String> emailDomains){
		 /*
         * 	Returns the e-mail messages where the e-mail domain is present
         */
        Set<String> queryArguments = new Set<String>();
        for(String domain : emailDomains)
            queryArguments.add('%'+ domain);
        return [
            SELECT
                Id,
                ParentId,
                HtmlBody,
                TextBody,
                Subject,
                ToAddress,
                FromName,
                FromAddress,
                MessageDate,
                IsDeleted,
                BccAddress,
                CcAddress,
                FlightOps_SenderIsEmbraer__c,
                HasAttachment
            FROM
            	EmailMessage
            WHERE
            	ParentId =: caseId
            	AND (
                	FromAddress LIKE :queryArguments
                    OR ToAddress LIKE :queryArguments
                    OR BccAddress LIKE :queryArguments
                    OR CcAddress LIKE :queryArguments
                )
            ORDER BY MessageDate DESC
        ];        
    }
    /*END  GET CASE EMAILS  END*/
     @AuraEnabled
    public static List<Attachment> getEmailAttachments(List<EmailMessage> caseEmails) {
        List<Attachment> caseAttachments = new List<Attachment>();
        for(EmailMessage eMsg : caseEmails){
            if(eMsg.HasAttachment){
                List<Attachment> at = new  List<Attachment>();
                at = [SELECT Id, Name, ParentId, ContentType FROM Attachment WHERE isDeleted = false AND ParentId =: eMsg.Id];
                for(Attachment attach : at){
                	caseAttachments.add(attach);
                    System.debug('ATTACHMENT CONTENT TYPE = '+attach.ContentType);
                }
            }
        }
        return caseAttachments;
    }
}