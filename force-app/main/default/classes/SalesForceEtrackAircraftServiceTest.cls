@isTest(seeAllData=true)
public class SalesForceEtrackAircraftServiceTest {
    
    @isTest static void aircraftInformationAndHistory(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test aircraftInformation';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = 'TEST 123456';
        insert account;
        
        Aircraft__c aircraft = new Aircraft__c ();
        aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '19000111';
        aircraft.Owner__c = account.id;
        aircraft.Aircraft_Status__c = 'In Service';
        aircraft.Commercial_Name__c = 'EMB-110P1';
        insert aircraft;  
        
        AircraftInformation aircraftINformationConstrutor = new AircraftInformation();
        
        AircraftInformation aircraftINformation = new AircraftInformation(aircraft,account,
                                                                          Constants.NEW_OBJECT_CREATED, 
                                                                          Constants.STATUS_ACTIVE);
        
        AircraftHistoryInformation aircraftHistoryInformation = new AircraftHistoryInformation(DateTime.now(), 
                                                                                               'test field', 
                                                                                               'test OldValue', 
                                                                                               'test newValue',
                                                                                               'test createdBy');
        
        System.assertEquals(aircraftINformation.getAccount(), account);
        System.assertEquals(aircraftINformation.getAircraft(), aircraft);
        System.assertEquals(aircraftINformation.getObjectType(), Constants.NEW_OBJECT_CREATED);
        System.assertEquals(aircraftINformation.getStatus(), Constants.STATUS_ACTIVE);
        
        
        List<AircraftInformation> aircraftINformationList = new List<AircraftInformation>();
        aircraftINformationList.add(aircraftINformation);
        
        List<AircraftHistoryInformation> aircraftHistoryInformationList = new List<AircraftHistoryInformation>();
        aircraftHistoryInformationList.add(aircraftHistoryInformation);
        
        Visitator visitor = new CallOutRepository();
        visitor.visit(aircraftINformationList, aircraftHistoryInformationList, 'AircraftInformation');
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassNameAndStatus('AircraftInformation');
        
        List<AircraftHistoryInformation>  aircraftInformationUpdateList = new List<AircraftHistoryInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AircraftHistoryInformation>  aircraftInfoUpdateList = (List<AircraftHistoryInformation>)JSON.deserialize(callout.payloadHistory__c, List<AircraftHistoryInformation>.class);
            
            for(AircraftHistoryInformation aircraftHistoryInformationUpdate : aircraftInfoUpdateList)
                aircraftInformationUpdateList.add(aircraftHistoryInformationUpdate);
            
            callout.status__c = 'RCP_SENT';
            update callout; 
        } 
        
        SalesForceEtrackAircraftService sfEtrackAircraftService = new SalesForceEtrackAircraftService();
        List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft> sfInfoAircraftList = sfEtrackAircraftService.buildAircraft();
        
        for(ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft sfAircraft : sfInfoAircraftList){
            System.assert(sfAircraft.modelType != null);
            System.assert(sfAircraft.flyEmbraerID != null);
        }
        
        Test.setMock(WebServiceMock.class, new SalesForceToEtrackProxyTest());
        
        SalesForceToEtrackProxy.executeAircraft();
        
        Test.stopTest();
    }
}