/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class  ProductDetail
*
* NAME: ProductDetailController.cls
* AUTHOR: RLdO                                                DATE: 23/12/2014
*******************************************************************************/
@isTest
private class ProductDetailControllerTest
{
  static testMethod void myUnitTest()
  {
    Product2 produto = SObjectInstanceTest.createProduct2();
    produto.Name = 'Test Embraer';
    Database.insert(produto);

    test.startTest();
    Test.setCurrentPage(system.Page.ProductDetail);
    ApexPages.currentPage().getParameters().put('Id',produto.Id);
    ProductDetailController controlador = new ProductDetailController();
    test.stoptest();

    system.assert(controlador.lProduct != null, 'Product not found!');
  }
}