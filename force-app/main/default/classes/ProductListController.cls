/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Controller class for the visualforce page ProductList
*
* NAME: ProductListController.cls
* AUTHOR: RLdO                                                DATE: 23/12/2014
*******************************************************************************/
public with sharing class ProductListController
{
  public list<Product2> LstProducts {get; set;}
  public String searchKey {get; set;}

  public ProductListController()
  {
    this.searchKey = '';
    this.fetchProducts();
  }

  public void fetchProducts()
  {
    String lQuery = 'select Id, ProductCode, Name, Product_Status__c, RecordType.Name from Product2 ';

    if (String.isNotBlank(this.searchKey))
      lQuery += ' where Name like \'%' + this.searchKey + '%\' ';

    lQuery += ' order by Name limit 100';
    this.LstProducts = database.query(lQuery);
  }
}