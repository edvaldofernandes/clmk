public interface ObjectTypeVisitor {
    
    String accept(ObjectType objectType);
}