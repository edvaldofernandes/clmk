/**
 * @author Marcilio Leite de Souza
 * @date 21/02/2018
 * @description: Default Handler Class methods for Communications__c trigger
 *
 * Modification Log    :
 * ------------------------------------------------------------------------------------------------
 * Developer                          Date                    Description
 * ---------------                   -----------             ----------------------------------------------
 * Marcilio Leite de Souza           21 FEV 2018             Original Version
 * Marcilio Leite de Souza           13 ABR 2018             Include daily summary change value to send the e-mail
 * Luis Gustavo de Souza             15 JUL 2020             Include REC Handler
 **/
public without sharing class CommunicationsTriggerHandler {
  public Map<Id, Communications__c> newRecordsMap = new Map<Id, Communications__c>();
  public Map<Id, Communications__c> oldRecordsMap = new Map<Id, Communications__c>();
  public List<Communications__c> newRecords = new List<Communications__c>();
  public List<Communications__c> oldRecords = new List<Communications__c>();
  public boolean isInsert = false;
  public boolean isUpdate = false;
  public boolean isUndelete = false;
  public boolean isDelete = false;
  public boolean isBefore = false;
  public boolean isAfter = false;
  private boolean isExecuting = false;

  private Id idEISRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
    .get('EIS Daily Report')
    .getRecordTypeId();

  public CommunicationsTriggerHandler(boolean isExecuting) {
    this.isExecuting = isExecuting;
  }

  public void OnBeforeInsert() {
  }

  public void OnAfterInsert() {
    if (getNewEISDailyReport().size() > 0) {
      Map<Id, List<Aircraft__c>> mapIdOperadorByAircraft = getMapIdOperadorByAircraft(
        getNewEISDailyReport(),
        CommunicationsTriggerHandler.modelType()
      );

      EISDailySupportBusinessRules.populateRelatedAircraft(
        mapIdOperadorByAircraft,
        getMapIdOperByIdComm(getNewEISDailyReport())
      );

      //If there is a Aircraft is possible to have a MEL
      if (mapIdOperadorByAircraft.size() > 0) {
        EISDailySupportBusinessRules.populateRelatedMEL(
          getNewEISDailyReport(),
          mapIdOperadorByAircraft
        );
      }
    }

    RECTriggerHandler recHandler = new RECTriggerHandler(this.newRecords);
    Map<Id, Communications__c> newRec = recHandler.getNewRec();

    if (newRec.size() == 1) {
      recHandler.changeTextAreaImagePrivateToPublic();
    }
  }

  public void OnBeforeUpdate() {
    /*
		if(getNewEISDailyReport().size() > 0){
			EISDailySupportBusinessRules.updateRemarks(getNewEISDailyReport());                                                       
		}
		*/
  }

  public void OnAfterUpdate() {
    //Send email restrict by 1 record and communication equal Issued and daily summary changed the value
    if (getNewEISDailyReport().size() == 1) {
      Id idKey = getNewEISDailyReport().values().get(0).Id;
      if (
        (getNewEISDailyReport().values().get(0).Communication_Status__c ==
        'Issued' &
        getNewEISDailyReport().values().get(0).Other_relevant_information__c !=
        oldRecordsMap.get(idKey).Other_relevant_information__c) ||
        (oldRecordsMap.get(idKey).Communication_Status__c == 'Draft' &
        getNewEISDailyReport().values().get(0).Communication_Status__c ==
        'Issued')
      ) {
        Map<Id, List<Aircraft__c>> mapIdOperadorByAircraft = getMapIdOperadorByAircraft(
          getNewEISDailyReport(),
          CommunicationsTriggerHandler.modelType()
        );
        Map<Id, List<Events__c>> mapIdCommuByInterRemol = getMapIdCommuByInterRemol(
          getNewEISDailyReport()
        );
        Map<Id, List<Minimum_Equipment_List__c>> mapIdCommuByMEL = getMapIdCommuByMEL(
          getNewEISDailyReport()
        );

        if (!Test.isRunningTest()) {
          EISDailySupportBusinessRules.sendEmailEIS(
            getNewEISDailyReport(),
            mapIdOperadorByAircraft,
            mapIdCommuByInterRemol,
            mapIdCommuByMEL
          );
        }
      }
    }

    RECTriggerHandler recHandler = new RECTriggerHandler(this.newRecords);
    Map<Id, Communications__c> newRec = recHandler.getNewRec();

    if (newRec.size() == 1) {
      recHandler.changeTextAreaImagePrivateToPublic();
      Communications__c rec = newRec.values().get(0);
      Id idKey = rec.Id;

      Communications__c writeableRec = [
        SELECT id, Resubmit__c, Email_Preview__c
        FROM Communications__c
        WHERE id = :rec.Id
        LIMIT 1
      ];
      if (
        (rec.Communication_Status__c == 'Issued' & rec.Resubmit__c == true) ||
        (oldRecordsMap.get(idKey).Communication_Status__c != 'Issued' &
        rec.Communication_Status__c == 'Issued')
      ) {
        if (REC_DOCController.getNewImages(rec, false).keySet().size() == 0) {
          recHandler.sendEmailToRECInterests();
        } else {
          throw new AsyncException(
            'You need save changes before send an email!'
          );
        }

        if (rec.Resubmit__c == true) {
          writeableRec.Resubmit__c = false;
          Database.update(writeableRec);
        }
      }
      if (!String.isBlank(rec.Email_Preview__c)) {
        recHandler.sendEmailPreview();
        writeableRec.Email_Preview__c = null;
        Database.update(writeableRec);
      }
    }
  }

  public void OnBeforeDelete() {
  }

  public void OnAfterDelete() {
  }

  public void OnUndelete() {
  }

  public boolean IsTriggerContext {
    get {
      return isExecuting;
    }
  }

  /*
   * @Return new map for Record type EIS Daily Report
   */
  private Map<Id, Communications__c> getNewEISDailyReport() {
    Map<Id, Communications__c> mapAux = new Map<Id, Communications__c>();
    for (Communications__c obj : this.newRecords) {
      if (obj.RecordTypeId == idEISRecordType) {
        mapAux.put(obj.Id, obj);
      }
    }
    return mapAux;
  }

  /*
   * @Return Set aircraft model type
   */
  public static Set<String> modelType() {
    Set<String> setModelType = new Set<String>();
    if (Test.isRunningTest()) {
      setModelType.add('E2 190');
    } else {
      setModelType.addAll(
        EIS_Daily_Report_Settings__c.getInstance().Model_Type__c.split(';')
      );
    }
    return setModelType;
  }

  /*
   * @Return Map Id Operador with related Id Communication
   */
  public static Map<String, String> getMapIdOperByIdComm(
    Map<Id, Communications__c> mapNewRecords
  ) {
    Map<String, String> mapIdOperByIdComm = new Map<String, String>();
    for (Communications__c obj : mapNewRecords.values()) {
      mapIdOperByIdComm.put(obj.Operator__c, obj.Id);
    }
    return mapIdOperByIdComm;
  }

  /*
   * @Return Map Id Operador with related List Aircraft
   */
  public static Map<Id, List<Aircraft__c>> getMapIdOperadorByAircraft(
    Map<Id, Communications__c> mapNewRecords,
    Set<String> setModelType
  ) {
    List<Aircraft__c> listAircraft = [
      SELECT
        Operator__c,
        Name,
        Model_Type__c,
        Aircraft_Status__c,
        Fleet_Type__c,
        Registration__c,
        FC_accumulated__c,
        FH_accumulated__c,
        EIS_AOG_Action__c,
        EIS_AOG_Status__c,
        EIS_Daily_Scheduled_Flights__c
      FROM Aircraft__c
      WHERE
        Operator__c IN :getMapIdOperByIdComm(mapNewRecords).keySet()
        AND Model_Type__c IN :setModelType
    ];

    system.debug(
      'CommunicationsTriggerHandler.getMapIdOperadorByAircraft.listAircraft: ' +
      listAircraft
    );

    Map<Id, List<Aircraft__c>> mapIdOperatorByAircraft = new Map<Id, List<Aircraft__c>>();

    for (Aircraft__c obj : listAircraft) {
      if (mapIdOperatorByAircraft.containsKey(obj.Operator__c)) {
        mapIdOperatorByAircraft.get(obj.Operator__c).add(obj);
      } else {
        List<Aircraft__c> lstTemp = new List<Aircraft__c>();
        lstTemp.add(obj);
        mapIdOperatorByAircraft.put(obj.Operator__c, lstTemp);
      }
    }
    return mapIdOperatorByAircraft;
  }

  /*
   * @Return Map Id Communicator with related Interruptions / Removals object Events__c
   */
  public static Map<Id, List<Events__c>> getMapIdCommuByInterRemol(
    Map<Id, Communications__c> mapNewRecords
  ) {
    String strSetIds = Utils.getStringFromSet(mapNewRecords.keySet());

    List<Events__c> listInterRemol = database.query(
      ' SELECT ' +
      utils.getAllFields('Events__c') +
      ',' +
      'Aircraft__r.Name FROM Events__c' +
      '  WHERE Daily_Summary_Interruption__c IN (' +
      strSetIds +
      ') ' +
      '     OR Daily_Summary_Removals__c     IN (' +
      strSetIds +
      ') ' +
      '     OR Daily_Summary_Pirep__c        IN (' +
      strSetIds +
      ') '
    );

    system.debug(
      'CommunicationsTriggerHandler.getMapIdCommuByInterRemol.listInterRemol: ' +
      listInterRemol
    );
    system.debug(
      'CommunicationsTriggerHandler.getMapIdCommuByInterRemol.listInterRemol.size(): ' +
      listInterRemol.size()
    );

    Map<Id, List<Events__c>> mapIdCommuByInterRemol = new Map<Id, List<Events__c>>();

    for (Events__c obj : listInterRemol) {
      String strIdCommu = '';
      if (obj.Daily_Summary_Interruption__c != null) {
        strIdCommu = obj.Daily_Summary_Interruption__c;
      } else if (obj.Daily_Summary_Removals__c != null) {
        strIdCommu = obj.Daily_Summary_Removals__c;
      } else if (obj.Daily_Summary_Pirep__c != null) {
        strIdCommu = obj.Daily_Summary_Pirep__c;
      }

      if (mapIdCommuByInterRemol.containsKey(strIdCommu)) {
        mapIdCommuByInterRemol.get(strIdCommu).add(obj);
      } else {
        List<Events__c> lstTemp = new List<Events__c>();
        lstTemp.add(obj);
        mapIdCommuByInterRemol.put(strIdCommu, lstTemp);
      }
    }

    system.debug(
      'CommunicationsTriggerHandler.getMapIdCommuByInterRemol.mapIdCommuByInterRemol: ' +
      mapIdCommuByInterRemol
    );
    return mapIdCommuByInterRemol;
  }

  /*
   * @Return Map Id Communicator with related MELs
   */
  public static Map<Id, List<Minimum_Equipment_List__c>> getMapIdCommuByMEL(
    Map<Id, Communications__c> mapNewRecords
  ) {
    String strSetIds = Utils.getStringFromSet(mapNewRecords.keySet());

    List<Minimum_Equipment_List__c> listMEL = database.query(
      ' SELECT ' +
      utils.getAllFields('Minimum_Equipment_List__c') +
      ',' +
      ' Aircraft__r.Name FROM Minimum_Equipment_List__c' +
      ' WHERE EIS_Daily_Report__c IN (' +
      strSetIds +
      ') '
    );

    system.debug(
      'CommunicationsTriggerHandler.getMapIdCommuByMEL.listMEL: ' + listMEL
    );
    system.debug(
      'CommunicationsTriggerHandler.getMapIdCommuByMEL.listMEL.size(): ' +
      listMEL.size()
    );

    Map<Id, List<Minimum_Equipment_List__c>> mapIdCommuByMEL = new Map<Id, List<Minimum_Equipment_List__c>>();

    for (Minimum_Equipment_List__c obj : listMEL) {
      if (mapIdCommuByMEL.containsKey(obj.EIS_Daily_Report__c)) {
        mapIdCommuByMEL.get(obj.EIS_Daily_Report__c).add(obj);
      } else {
        List<Minimum_Equipment_List__c> lstTemp = new List<Minimum_Equipment_List__c>();
        lstTemp.add(obj);
        mapIdCommuByMEL.put(obj.EIS_Daily_Report__c, lstTemp);
      }
    }
    system.debug(
      'CommunicationsTriggerHandler.getMapIdCommuByMEL.mapIdCommuByMEL: ' +
      mapIdCommuByMEL
    );

    return mapIdCommuByMEL;
  }
}