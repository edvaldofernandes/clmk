public class RcpOutBoundAircraftService {
    
    private List<AircraftInformation> aircraftInformationList;
    private List<AircraftHistoryInformation> aircraftHistoryInformationList;
    private List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft> rcpAircraftList;
    private List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist> rcpAircraftHistList;
    private List<Call_Out__c> calloutUpdateList;
    
    public RcpOutBoundAircraftService(){
        
        initNativeObject();
        initWSElement();
    }
    
    private void initNativeObject(){
        
        calloutUpdateList = new List<Call_Out__c>();
        aircraftInformationList = deserializeAircraftInformation();
        aircraftHistoryInformationList = deserializeAircraftHistoryInformation();
    }
    
    private void initWSElement(){
        
        rcpAircraftList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft>();
        rcpAircraftHistList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist>();
    }
    
    public List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft> buildRcpAircraft(){
        
        for(AircraftInformation aircraftInformation : aircraftInformationList){
            
            Aircraft__c aircraft = aircraftInformation.getAircraft();
            Account account = aircraftInformation.getAccount();
            
            RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft SfAircraft = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft();
            SfAircraft.serialNumber = aircraft.name;
            SfAircraft.registration = aircraft.Registration__c;
            SfAircraft.aircraftStatus = aircraft.Aircraft_Status__c;
            SfAircraft.aircraftCertification = aircraft.Aircraft_Certification__c;
            SfAircraft.modelType = aircraft.Model_Type__c;
            SfAircraft.operator = aircraft.Operator__c;
            SfAircraft.operatorCountry = aircraft.Operator_Country__c;
            SfAircraft.geograficRegion = aircraft.Geografic_Region__c;
            SfAircraft.operatorCertification = aircraft.Operator_Certification__c;
            SfAircraft.owner = aircraft.Owner__c;
            SfAircraft.ownerCountry = aircraft.Owner_Country__c;
            SfAircraft.manufacturingDate = aircraft.Manufacturing_Date__c;
            SfAircraft.oemDeliveryDate = aircraft.OEM_Delivery_Date__c;
            SfAircraft.deliveryDateCurrentOperator = aircraft.Delivery_Date_to_Current_Operator__c;
            SfAircraft.fhAccumulated = aircraft.FH_accumulated__c;
            SfAircraft.fcAccumulated = aircraft.FC_accumulated__c;
            SfAircraft.relevantInformation = aircraft.Relevant_information__c;
            SfAircraft.oldOperator = aircraft.Old_Operator__c;
            SfAircraft.oldOwner = aircraft.Old_Owner__c;
            SfAircraft.leasingStartDate = aircraft.Leasing_Start_Date__c;
            
            rcpAircraftList.add(SfAircraft);   
        }
        
        return rcpAircraftList;
    }
    
    public List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist> buildRcpAircratHistory(){
        
        for(AircraftHistoryInformation  aircraftHistoryInformation : aircraftHistoryInformationList){
            
            RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist SfAircraftHist = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist();
            
            SfAircraftHist.createdDate = aircraftHistoryInformation.getCreatedDate();
            SfAircraftHist.createdById = aircraftHistoryInformation.getCreatedBy();
            SfAircraftHist.field = aircraftHistoryInformation.getField();
            SfAircraftHist.oldValue = aircraftHistoryInformation.getOldValue();
            SfAircraftHist.newValue = aircraftHistoryInformation.getNewValue();
            
            rcpAircraftHistList.add(SfAircraftHist);
        }
        
        return rcpAircraftHistList;
    }
    
    
    private List<AircraftInformation> deserializeAircraftInformation(){ 
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassNameAndStatus(String.valueOf(AircraftInformation.class));
        
        List<AircraftInformation>  aircraftInformationList = new List<AircraftInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AircraftInformation>  aircraftInfoList = (List<AircraftInformation>)JSON.deserialize(callout.payload__c, List<AircraftInformation>.class);
            
            for(AircraftInformation aircraftInformation : aircraftInfoList)
                aircraftInformationList.add(aircraftInformation);    
        }
        
        return aircraftInformationList;
    }
    
    private List<AircraftHistoryInformation> deserializeAircraftHistoryInformation(){
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassNameAndStatus(String.valueOf(AircraftInformation.class));
        
        List<AircraftHistoryInformation>  aircraftInformationList = new List<AircraftHistoryInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AircraftHistoryInformation>  aircraftInfoList = (List<AircraftHistoryInformation>)JSON.deserialize(callout.payloadHistory__c, List<AircraftHistoryInformation>.class);
            
            for(AircraftHistoryInformation aircraftHistoryInformation : aircraftInfoList)
                aircraftInformationList.add(aircraftHistoryInformation);
            
            callout.status__c = 'RCP_SENT';
            
            calloutUpdateList.add(callout);
        }
        
        return aircraftInformationList;
    }
    
    public List<Call_Out__c> getCalloutUpdateList(){ 
        
        return calloutUpdateList;
    }
}