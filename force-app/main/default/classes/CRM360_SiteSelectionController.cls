public class CRM360_SiteSelectionController {
        
    public CRM360_SiteSelectionController(ApexPages.StandardController controller){Id standardId = controller.getId();}
    //Gets all sites
    //Use list name: siteList
    //Fields: Name, Geographical_Area__c    
    public static List<Account> getSiteList(){
        List<Account> siteList = new List<Account>();
        siteList = [SELECT Name, Geographical_Area__c 
                    FROM Account 
                    WHERE (Account_Type__c = 'Embraer Site' 
                           AND CRM360_Site__c = true) ORDER BY Name ASC];
        Account acc = new Account(Name = 'All Sites',Geographical_Area__c = 'All Areas');
        siteList.add(acc);
        return siteList;
    }
}