//Classe aproveitada
@isTest 
public class FO_MergeCaseControllerTest {
    
    static testMethod void TesteUnitario() {
        
      RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
                         
      Case casoToInsert1 = SObjectInstanceTest.createCase();
      casoToInsert1.RecordTypeId = rt.Id; 
      casoToInsert1.Status = 'New';
      casoToInsert1.Subject = 'RE';  
      list<Case> lstcaso = new list<Case>();
      lstcaso.add(casoToInsert1);
      database.insert(lstcaso);   
          
      EmailMessage email = SObjectInstanceTest.email( casoToInsert1.Id );
      database.insert(email);
      
      Attachment anexo = SObjectInstanceTest.anexo( email.id );
      database.insert( anexo );
      
      Task tt =SObjectInstanceTest.createTask();
      tt.WhatId = casoToInsert1.Id;
      database.insert(tt);
        
      Case casoToInsert2 = SObjectInstanceTest.createCase();
      casoToInsert2.RecordTypeId = rt.Id; 
      casoToInsert2.Subject = 'RE';
      database.insert(casoToInsert2);   
        
      Task t =SObjectInstanceTest.createTask();
      t.WhatId = casoToInsert2.Id;
      database.insert(t);
         
      test.startTest();
      
       PageReference pageRef = Page.FO_CaseMergeFlightOps;
       pageRef.getParameters().put('Refid',String.valueOf(casoToInsert1.id));
       Test.setCurrentPageReference(pageRef);
       FO_MergeCaseController controller = new FO_MergeCaseController (new ApexPages.StandardsetController(lstcaso));    
       controller.localizarid=casoToInsert1.Subject;
       controller.LocalizarCase();
       controller.casoController = new Case(ParentId=casoToInsert1.Id);
       controller.msg = 'teste';
        
       controller.save();
        
       for( FO_MergeCaseController.wrapperCase wCaso : controller.lstCase )
            wCaso.atribuir = true;
            
        
       controller.save();  
        
      test.stopTest();
           
    }    
    
    static testMethod void TesteUnitario2() {
        
      RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
             
      Case casoToInsert1 = SObjectInstanceTest.createCase();
      casoToInsert1.RecordTypeId = rt.Id;
      casoToInsert1.Status='New';
      casoToInsert1.Subject = 'RE';
      list<Case> lstcaso = new list<Case>();
      lstcaso.add(casoToInsert1);
      database.insert(lstcaso);   
                    
      Case casoToInsert2 = SObjectInstanceTest.createCase();
      casoToInsert2.RecordTypeId = rt.Id;  
      casoToInsert2.Subject = 'RE';  
      database.insert(casoToInsert2);      
         
      test.startTest();
      
      ApexPages.StandardsetController stdController = new ApexPages.StandardsetController(lstcaso);
      stdController.setSelected(lstcaso);
       FO_MergeCaseController controller = new FO_MergeCaseController (stdController);    
       
       controller.LocalizarCase();
       controller.casoController = new Case();

       controller.save();
        test.stopTest();
           
    }    
}