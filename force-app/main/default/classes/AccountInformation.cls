public class AccountInformation {
    
    private Account account {set; get;}
    private String objectType {set; get;}
    private String status {set; get;}
    
    public AccountInformation(){}
    
    public AccountInformation(Account account,String objectType, String status){
      this.account = account;
      this.objectType = objectType;
      this.status = status;  
    }
    
    public Account getAccount(){
        return this.account;
    }
    
    public String getObjectType(){
        return this.objectType;
    }
    
    public String getStatus(){
        return this.status;
    }
}