@isTest(seeAllData=true)
public class SalesForceToEtrackAccountServiceTest {
    
    @isTest static void sfToEtrackAccountInformationAndHistory(){
        
        Test.startTest();
        Account account = new Account();
        account.Name = 'Test trigger account deleted';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
        
        AccountInformation accountInformationConstrutor = new AccountInformation();
        
        AccountInformation accountInformation = new AccountInformation(account, 'NEW OBJECT', 'NEW');
        
        System.assertEquals(accountInformation.getAccount(), account);
        System.assertEquals(accountInformation.getObjectType(), 'NEW OBJECT');
        System.assertEquals(accountInformation.getStatus(), 'NEW');
        
        AccountHistoryInformation accountHistoryInformation = new AccountHistoryInformation(Datetime.now(), 'field', 'OldValue', 'newValue', 'createdBy');
        
        List<AccountInformation> accountInformationList = new List<AccountInformation>();
        accountInformationList.add(accountInformation);
        
        List<AccountHistoryInformation> accountHistoryInformationList = new List<AccountHistoryInformation>();
        accountHistoryInformationList.add(accountHistoryInformation);
        
        Visitator visitor = new CallOutRepository();
        visitor.visit(accountInformationList, accountHistoryInformationList, 'AccountInformation');
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassNameAndStatus('AccountInformation');
        
        List<AccountHistoryInformation>  accountHistoryInformationUpdateList = new List<AccountHistoryInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AccountHistoryInformation>  accountHistoryInfoList = (List<AccountHistoryInformation>)JSON.deserialize(callout.payload__c, List<AccountHistoryInformation>.class);
            
            for(AccountHistoryInformation accountHistoryInformationUpdate : accountHistoryInformationUpdateList)
                accountHistoryInfoList.add(accountHistoryInformationUpdate);
            
            callout.status__c = 'RCP_SENT';
            update callout;
        }
        
        SalesForceToEtrackAccountService sForceService = new SalesForceToEtrackAccountService();
        List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount> accountList = sForceService.buildAccount();
        
        for(ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount sfAccount : accountList)
            System.assert(sfAccount.companyName != null);
        
        Test.setMock(WebServiceMock.class, new SalesForceToEtrackProxyTest());
        SalesForceToEtrackProxy.executeAccount();
        
        Test.stopTest();
    }
    
    @isTest static void deleteAccountTriggerTest(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test trigger account deleted';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
        
        Account accountDelete = [SELECT ID, NAME FROM Account WHERE ID = :account.ID];
        
        delete accountDelete;
        
        Test.setMock(WebServiceMock.class, new SalesForceToEtrackProxyTest());
        
        Test.stopTest();
    }
    
    @isTest static void accountTeamMemberTest(){
        
        try{
            
            Test.startTest();
            
            Test.setMock(WebServiceMock.class, new SalesForceToEtrackProxyTest()); 
            SalesForceToEtrackProxy.executeTeamMemberInserted();    
            
            Account account = new Account();
            account.Name = 'accountTeamMemberTest';
            account.Company_Nickname__c = 'tt account';
            account.Company_Status__c = 'Active';
            insert account;
            
            Profile p = [select id from Profile where name='System Administrator'];
            
            User user = new User(alias = 'utest8', email='Unit8.Test@unittest.com',emailencodingkey='UTF-8', 
                                 firstName='First8', lastname='Last', languagelocalekey='en_US', localesidkey='en_US', 
                                 profileid = p.id, timezonesidkey='Europe/London', username='Unit8.Test@unittest.com');
            insert user;
            
            AccountTeamMember accountTeamMember = new AccountTeamMember();
            accountTeamMember.UserId = user.Id;
            accountTeamMember.AccountId = account.Id;
            accountTeamMember.teamMemberRole = 'Role_test';
            
            insert accountTeamMember;
            
            List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
            accountTeamMemberList.add(accountTeamMember);
            
            SalesForceToEtrackAccountService.buildTeamMemberUpdatedOrInserted(accountTeamMemberList, Constants.NEW_OBJECT_CREATED);
            
            for(AccountTeamMember teamMember : accountTeamMemberList)
                System.assertEquals(teamMember.UserId, user.Id);
            
            Test.stopTest();
            
        }catch(TypeException error){}  
    }
    
    @isTest static void accountTeamMemberTestUpdate(){
        
        try{
            
            Test.startTest();
            
            Test.setMock(WebServiceMock.class, new SalesForceToEtrackProxyTest());
            SalesForceToEtrackProxy.executeTeamMemberUpdated();    
            
            Account account = new Account();
            account.Name = 'accountTeamMemberTest';
            account.Company_Nickname__c = 'tt account';
            account.Company_Status__c = 'Active';
            insert account;
            
            Profile p = [select id from Profile where name='System Administrator'];
            
            User user = new User(alias = 'utest9', email='Unit9.Test@unittest.com',emailencodingkey='UTF-8', 
                                 firstName='First9', lastname='Last9', languagelocalekey='en_US', localesidkey='en_US', 
                                 profileid = p.id, timezonesidkey='Europe/London', username='Unit9.Test@unittest.com');
            insert user;
            
            AccountTeamMember accountTeamMember = new AccountTeamMember();
            accountTeamMember.UserId = user.Id;
            accountTeamMember.AccountId = account.Id;
            accountTeamMember.teamMemberRole = 'Role_test';
            
            insert accountTeamMember;
            
            List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
            accountTeamMemberList.add(accountTeamMember);
            
            SalesForceToEtrackAccountService.buildTeamMemberUpdatedOrInserted(accountTeamMemberList, Constants.NEW_OBJECT_CREATED);
            
            for(AccountTeamMember teamMember : accountTeamMemberList)
                System.assertEquals(teamMember.UserId, user.Id);
            
            accountTeamMember.teamMemberRole = 'Role_test_update';
            update accountTeamMember;
            
            List<AccountTeamMember> accountTeamMemberListUpdate = new List<AccountTeamMember>();
            accountTeamMemberListUpdate.add(accountTeamMember);
            
            SalesForceToEtrackAccountService.buildTeamMemberUpdatedOrInserted(accountTeamMemberListUpdate, Constants.OLD_OBJECT_UPDATED_OR_DELETED); 
            
            
            Test.stopTest();
            
        }catch(TypeException error){}    
    }
}