/**
* @author Marcilio Leite de Souza
* @date 01/04/2019
* @description: Test class for CLMUpdateAgreement
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           21 FEV 2018             Original Version
**/
@IsTest 
public with sharing class CLMUpdateAgreementTest {
    
    static testMethod void is_agreement_set_to_expire(){
        List<Agreement__c> listAgg = [Select Status__c, Status_Category__c FROM Agreement__c];
        System.debug('is_agreement_set_to_expire.listAgg: ' + listAgg);

        List<Agreement_Aircraft__c> listAggAir = [Select Status__c FROM Agreement_Aircraft__c];
        System.debug('is_agreement_set_to_expire.listAggAir: ' + listAggAir);

        List<Aircraft_PDP__c> listAirPDP = [Select Contract_Aircraft__c, Status_L__c FROM Aircraft_PDP__c];
        System.debug('is_agreement_set_to_expire.listAirPDP: ' + listAirPDP);

        List<SOB_Event__c> listSOBEvt = [Select Commercial_Agreement__c, Contract_Aircraft__c, SOB_Status__c FROM SOB_Event__c];
        System.debug('is_agreement_set_to_expire.listSOBEvt: ' + listSOBEvt);

        Test.startTest();
          CLMUpdateAgreement clmUpdate = new CLMUpdateAgreement(listAgg);
          clmUpdate.updateAgreementStatus('Expired', 'Expired');
          //System.assertEquals([Select Status__c, Status_Category__c FROM Agreement__c LIMIT 1 ].Status__c, 'Expired');

          clmUpdate.updateAgreementAircraftStatus('Expired',new Set<String>{'Expired'});

          clmUpdate.updateAgreementAircraftPDPStatus('Cancelled','Cancelled');

          clmUpdate.updateSOBEventStatus('Expired');

          clmUpdate.deleteAgreementTasks();

        Test.stopTest();
    }
    

   @TestSetup
   static void  insertAgreement(){
        product2 aircraft = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
        insert aircraft;
       
        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(Test.getStandardPricebookId(), aircraft.Id);
        insert pbe;  
        
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        insert acc;
            
        Agreement__c agreement = new Agreement__c();
        agreement.Name = 'Test 1';
        agreement.Account__c = acc.Id;
        agreement.Country__c = 'Iceland';
        agreement.Agreement_Code__c = 'ABCDEFG';
        agreement.Region_Code__c = 'RegionCODE';
        
        agreement.Status__c = 'Request';
        agreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        agreement.Company_Signed_Date__c = Date.today(); 
        agreement.Kickoff_trigger_already_executed__c = false; 
        agreement.Admin_Mode__c = 'true';
        agreement.Agreement_Color__c = '0000FF';
        insert agreement;
       	
       
       Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(agreement.Id, aircraft.id);
       insert aircraftPrice1;

        Agreement_Aircraft__c contractAircraft = new Agreement_Aircraft__c();
        contractAircraft.Aircraft__c  = aircraft.Id;
        contractAircraft.Delivery_Date__c = Date.today();
        contractAircraft.Contract_Aircraft_Number__c = 5;
        contractAircraft.Order_Type__c = 'Option';
        contractAircraft.Basic_Price__c = 200;
        contractAircraft.Aircraft_Configuration__c = 'AK';
        contractAircraft.Agreement__c = agreement.Id;
        contractAircraft.Actual_Delivery_Date__c = Date.today();
        contractAircraft.Skyline_Summary_Calculated__c = 'Firms E1';
        contractAircraft.MFA_SCORE__c = 100;
        contractAircraft.Contractual_Delivery_Month__c = Date.today();
        contractAircraft.Status__c = 'Planned';
       contractAircraft.Aircraft_Price__c = aircraftPrice1.Id;
        insert contractAircraft;

        Aircraft_PDP__c aircraftPDP = new Aircraft_PDP__c();
        aircraftPDP.Contract_Aircraft__c = contractAircraft.Id;
        aircraftPDP.Status_L__c = 'Pending';
        insert aircraftPDP;

        SOB_Event__c sobEvent = new SOB_Event__c();
        sobEvent.Commercial_Agreement__c = agreement.Id;
        sobEvent.Contract_Aircraft__c = contractAircraft.Id;
        sobEvent.SOB_Status__c = 'Firm Orders Included';
        insert sobEvent;

        Task task = new Task();
        task.Description = 'test';
        task.Subject = 'test';
        insert task;

        TaskRelation taskRelation = new TaskRelation();
        taskRelation.TaskId = task.Id;
        taskRelation.RelationId = agreement.Id;
        taskRelation.IsWhat = true;
        insert taskRelation;

    }
}