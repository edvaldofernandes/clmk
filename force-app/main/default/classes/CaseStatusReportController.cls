public class CaseStatusReportController
{
    //variables used in the view
    public list<CasoWrapper> lstCasoWrapper;
  	public map<String, CasoWrapper> mapCasoWrapper;
  	//public integer reloadTime {get; set;}
    
    //constructor
    public CaseStatusReportController(ApexPages.StandardController std){
    	preencheCaixas();
    }
    
    //Initializes all data, ultimately storing it in the variable mapCasoWrapper
    private void preencheCaixas(){
    	//Preenche nomes das colunas
    	this.mapCasoWrapper = new map<String, CasoWrapper>();
    	CasoWrapper casoW0 = new CasoWrapper(DashboardUtils.INBOX);
    	this.mapCasoWrapper.put(DashboardUtils.INBOX, casoW0);		
    	CasoWrapper casoW1 = new CasoWrapper(DashboardUtils.AOG_NFO);
    	this.mapCasoWrapper.put(DashboardUtils.AOG_NFO, casoW1);
    	CasoWrapper casoW2 = new CasoWrapper(DashboardUtils.AOG);
    	this.mapCasoWrapper.put(DashboardUtils.AOG, casoW2);
    	CasoWrapper casoW3 = new CasoWrapper(DashboardUtils.CRI);
    	this.mapCasoWrapper.put(DashboardUtils.CRI, casoW3);
    	CasoWrapper casoW4 = new CasoWrapper(DashboardUtils.RTN);
    	this.mapCasoWrapper.put(DashboardUtils.RTN, casoW4);
    	//CasoWrapper casoW5 = new CasoWrapper(DashboardUtils.OSS);
    	//this.mapCasoWrapper.put(DashboardUtils.OSS, casoW5);
    	//CasoWrapper casoW6 = new CasoWrapper(EHSSHIP);
    	//this.mapCasoWrapper.put(EHSSHIP, casoW6);
    	
    	//Preenche quantidade total para cada caixa
    	DashboardUtils.calculaTotais(mapCasoWrapper);
        
        //calculate trend
        datetime hoje = DashboardUtils.getHojeAndMes()[0];	//beginning of day
        datetime mes = DashboardUtils.getHojeAndMes()[1];	//beginning of month
        map<String, map<Boolean, Integer>> mapCasesPassadosInbox = DashboardUtils.retornaTotaisCasosFechadosInbox(mes, system.now());
    	map<String, map<Boolean, Integer>> mapCasesHojeInobx = DashboardUtils.retornaTotaisCasosFechadosInbox(hoje, system.now());
        map<String, map<Boolean, Integer>> mapCasesPassados = DashboardUtils.retornaTotaisCasosFechadosResponse(mes, system.now());
    	map<String, map<Boolean, Integer>> mapCasesHoje = DashboardUtils.retornaTotaisCasosFechadosResponse(hoje, system.now());

        //calcula semaforo
    	map<String, map<String, Integer>> mapCasosPorMotivo = DashboardUtils.calculaSemaforo();
		
        //preenche informações
        for (String motivo : mapCasoWrapper.keySet()){
      		CasoWrapper casoW = mapCasoWrapper.get(motivo);

      		//preenche tendencia
      		//month
      		map<Boolean, Integer> mapQtd = (motivo == DashboardUtils.INBOX) ? mapCasesPassadosInbox.get(motivo) : mapCasesPassados.get(motivo);
      		if(mapQtd != null){        
                Integer qtdFechados = mapQtd.get(false);
        		Integer qtdNFechados = mapQtd.get(true);
        		if(qtdNFechados == null) qtdNFechados = 0;
                if(qtdFechados == null) qtdFechados = 0;
                casoW.month = ((qtdFechados + qtdNFechados) > 0) ? DashboardUtils.fmtPercent(qtdFechados, (qtdFechados + qtdNFechados)) : null;
            }
            
            //current day
            map<Boolean, Integer> mapQtdHoje = (motivo == DashboardUtils.INBOX) ? mapCasesHojeInobx.get(motivo) : mapCasesHoje.get(motivo);
            if(mapQtdHoje != null){
                Integer qtdFechadosHoje = mapQtdHoje.get(false);
        		Integer qtdNFechadosHoje = mapQtdHoje.get(true);
        		if(qtdNFechadosHoje == null) qtdNFechadosHoje = 0;
        		if(qtdFechadosHoje == null) qtdFechadosHoje = 0;
                casoW.lastDay = ((qtdFechadosHoje + qtdNFechadosHoje) > 0) ? DashboardUtils.fmtPercent(qtdFechadosHoje, (qtdFechadosHoje + qtdNFechadosHoje)) : null;
            }
            
            //preenche semáforo
      		if(mapCasosPorMotivo != null){
                map<String, Integer> mapTemp = mapCasosPorMotivo.get(motivo);
                if(mapTemp != null){
          			for(String status : mapTemp.keySet()){
                        Integer qtdStatus = mapTemp.get(status);
                        if(qtdStatus == null) continue;
                        if(status.contains(DashboardUtils.VERMELHO)) casoW.quantidadeVermelho = String.valueOf(qtdStatus);
                        else if(status.contains(DashboardUtils.AMARELO)) casoW.quantidadeAmarelo = String.valueOf(qtdStatus);
                        else casoW.quantidadeVerde = String.valueOf(qtdStatus);
                    }
                }
            }
            //preenche as informações de status (carinha e seta)
            DashboardUtils.verificaStatus(casoW);
            //preenche dados do caso mais velho
            //calcularTempoRestante(motivo);
        }
    }
    
    //getter method for lstCasoWrapper
    public list<CasoWrapper> getlstCasoWrapper(){
    	lstCasoWrapper = new list<CasoWrapper>();
    	lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.INBOX));
    	lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.AOG_NFO));
    	lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.AOG));
    	lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.CRI));
    	lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.RTN));
    	//lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.EHS));
    	//lstCasoWrapper.add(mapCasoWrapper.remove(DashboardUtils.EHSSHIP));
        return lstCasoWrapper;
    }
}