/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class OpportunityUpdateQuote
* NAME: OpportunityUpdateQuoteTest.cls
* AUTHOR: DPF                                                DATE: 02/02/2015
*
*******************************************************************************/

@isTest
private class OpportunityUpdateQuoteTest {

    private static final integer QTD_LOTE = 200;
    private static final id RecTypeAcc = RecordTypeMemory.getRecType( 'Account', 'Operator' );
    
    static testMethod void funcional() {
      Id lPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      Account acc = SObjectInstanceTest.createAccount(RecTypeAcc);
      Database.insert(acc);
      
      Opportunity lOpportunity = SObjectInstanceTest.createOpportunity();
      lOpportunity.AccountId = acc.Id;
      insert lOpportunity;
      
      Quote cotacao = SObjectInstanceTest.createQuote(lOpportunity.Id);
      cotacao.Pricebook2Id = lPB;
      cotacao.Status = 'Presented to Customer';
      insert cotacao;
      
      Account acc2 = SObjectInstanceTest.createAccount(RecTypeAcc);
      Database.insert(acc2);
      
      Test.startTest();
      lOpportunity.AccountId = acc2.Id;
      database.update(lOpportunity);
      Test.stopTest();
    }
    
    static testMethod void lote() {
      Id lPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      Account acc = SObjectInstanceTest.createAccount(RecTypeAcc);
      Database.insert(acc);
      
      List<Opportunity> lstOpp = new List<Opportunity>();
      for ( Integer i = 0; i < QTD_LOTE; i++ )
      {
	      Opportunity lOpportunity = SObjectInstanceTest.createOpportunity();
	      lOpportunity.AccountId = acc.Id;
	      lstOpp.add(lOpportunity);
      }
      insert lstOpp;
      
      List<Quote> lstquote = new List<Quote>();
      for ( Integer i = 0; i < QTD_LOTE; i++ )
      {
        Quote cotacao = SObjectInstanceTest.createQuote(lstOpp.get(i).Id);
	      cotacao.Pricebook2Id = lPB;
	      cotacao.Status = 'Presented to Customer';
	      lstquote.add(cotacao);
      }
      insert lstquote;
      
      Account acc2 = SObjectInstanceTest.createAccount(RecTypeAcc);
      Database.insert(acc2);
      
      Test.startTest();
      for ( Opportunity lOpportunity : lstOpp )
        lOpportunity.AccountId = acc2.Id;
      database.update(lstOpp);
      Test.stopTest();
    }
}