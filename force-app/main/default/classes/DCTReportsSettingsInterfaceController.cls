public without sharing class DCTReportsSettingsInterfaceController
{


    private map<string,DCTReportsSettings__c> mapReportSettings;
    public List<SelectOption> avaliableYearsSettings;
    public String selectedYear {get;set;}
    public DCTReportsSettings__c selectedSetting {get;set;}
    public boolean isNewRecord {get;set;}
    private string originReport = '';
    
    
    
    public DCTReportsSettingsInterfaceController(ApexPages.StandardController stdController)
    {
        originReport  = ApexPages.currentPage().getParameters().get('origin');
        selectedYear = ApexPages.currentPage().getParameters().get('year');
        isNewRecord = false;
        loadSettings();
        updateYear();
    
    }
    
    
    public Pagereference updateYear()
    {
        selectedSetting = mapReportSettings.get(selectedYear);
        isNewRecord = false;
        return null;
    }
    
    public Pagereference saveRecord()
    {
        try
        {
            if(isNewRecord)
            {
                insert selectedSetting;
            }
            else
            {
                update selectedSetting;
            }
            
            selectedYear = selectedSetting.Name.trim();
            loadSettings();
            updateYear();

        }
        catch(exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));        
        }
        isNewRecord = false;
        return null;
    }
    public Pagereference backToPrevious()
    {
        
        return new PageReference('/apex/' + originReport);
    }
    
    public Pagereference newRecord()
    {
        selectedSetting = new DCTReportsSettings__c();
        isNewRecord = true;
        return null;
    }
    
        private void loadSettings()
        {
            mapReportSettings = DCTReportsSettings__c.getAll();
        }
        public List<SelectOption> getAvaliableYearsSettings()
        {
            avaliableYearsSettings = new List<SelectOption>();    
            
            for(String yearKey : mapReportSettings.keySet())
                avaliableYearsSettings.add(new SelectOption(yearKey,yearKey));
                        
            avaliableYearsSettings.sort();
            
            return avaliableYearsSettings;     
        }
    
    }