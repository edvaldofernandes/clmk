/*******************************************************************************
*                              Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class SvcContractUpdateStatusSvcContract
*
* NAME: SvcContractUpdateStatusSvcContractTest.cls
* AUTHOR: KHPS                                                 DATE: 02/01/2015
*******************************************************************************/

@isTest(seeAllData=true)
private class SvcContractUpdateStatusSvcContractTest {
    
    private static final id recTypeAcc = RecordTypeMemory.getRecType('Account', 'Operator');
    private static final id recTypeSvc = RecordTypeMemory.getRecType('ServiceContract', 'Aircraft_Modification');   

    static testMethod void testeFuncional() {
        
            Id stdPbk = SObjectInstanceTest.catalogoDePrecoPadrao();        
        
        Product2 prod = SObjectInstanceTest.createProduct2();
        Database.insert(prod);

        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPbk, prod.Id);
        Database.insert(pbe);       
        
        Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
      Database.insert(acc);
        
        Contact ctt = SObjectInstanceTest.createContact(acc.Id);
        Database.insert(ctt);
        
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        opp.AccountId = acc.Id;
        opp.Pricebook2Id = stdPbk;
        Database.insert(opp);
        
        MFIR__c mfir = new MFIR__c();
        mfir.Name = 'Teste mfir';
        mfir.MFIR_number__c = '1234567890';
        mfir.Account__c = acc.Id;
        Database.insert(mfir);
      
      ServiceContract svcPai = SObjectInstanceTest.createServiceContract(acc.Id, recTypeSvc);
      Database.insert(svcPai);
      
      ServiceContract svcFilho = SObjectInstanceTest.createServiceContract(acc.Id, recTypeSvc);
      svcFilho.Contract_Status__c = 'Draft';
      svcFilho.Contract_hierarchy__c = svcPai.Id;
      svcFilho.Pricebook2Id = stdPbk;
      svcFilho.Opportunity__c = opp.Id;
      svcFilho.ApprovalStatus = 'Approved';
      svcFilho.Comments_for_Signature__c = 'Teste';
      svcFilho.StartDate = Date.today();
      svcFilho.EndDate = Date.today();
      svcFilho.Payment_Terms__c = 'Teste';
      svcFilho.SpecialTerms = 'Teste';
      svcFilho.ContactId = ctt.Id;
      svcFilho.Submission_Date__c = Date.today();
      svcFilho.Validity_Date__c = Date.today();
      svcFilho.MFIR__c = mfir.Id;
      Database.insert(svcFilho);
      
      ContractLineItem conli = SObjectInstanceTest.createContractLineItem(svcFilho.Id, pbe.Id);
      Database.insert(conli);      
      
      svcFilho.Contract_Status__c = 'Internal approval';
      Database.update(svcFilho);
      
      svcFilho.Contract_Status__c = 'Submited to customer';
      Database.update(svcFilho);      
      
      svcFilho.Contract_Status__c = 'Signed';
        svcFilho.Reason_of_Signature__c = 'Price';      
      svcFilho.Signature_Date__c = Date.today();  
        opp.StageName = 'Archived';
      Database.update(opp);     
      
      Test.startTest();
      Database.update(svcFilho);
      Test.stopTest();
      
      ServiceContract svcPaiResult = [SELECT Id, Contract_Status__c FROM ServiceContract WHERE Id = :svcPai.Id];
      //System.assertEquals('Amended', svcPaiResult.Contract_Status__c);              
        
    }
}