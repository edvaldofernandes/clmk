global class CLMBulkMigrationDocument implements Database.Batchable<sObject>, Database.Stateful {
        
    global List<CLMMigration.CLMMigrationErroModel> listMigration = new List<CLMMigration.CLMMigrationErroModel>();

    global Database.Querylocator start( Database.BatchableContext BC ){
                             
        return Database.getQueryLocator([SELECT Id,Name,Apttus__Agreement__c,Apttus__Comments__c
                                                ,Apttus__Path__c,Apttus__Type__c,Apttus__URL__c
                                                ,Apttus__URL_Link__c,Apttus__Version__c
                                             FROM Apttus__Agreement_Document__c ]);    
    }
    
    global void execute( Database.BatchableContext BC, List<sObject> scope ){
       List<Apttus__Agreement_Document__c> listApttDoc = (List<Apttus__Agreement_Document__c>) scope;

       //List to insert content version (Salesforce File)
       List<ContentVersion> listContentVersionInsert = new List<ContentVersion>();

       //List to insert content document link (Salesforce File x New Agreement)
       Set<ContentDocumentLink> listContentDocLink = new Set<ContentDocumentLink>();
    
       //Hold the file name and related Agg to be insert in Content Document Link
       Map<String,String> mapFileNameByAggId = new Map<String,String>();

       //Set String hold the File Id from Apttus Document substring URL
       Set<String> setFileIdFromApttDoc = new Set<String>();

       for(Apttus__Agreement_Document__c obj : listApttDoc){
           
           mapFileNameByAggId.put(obj.Name, obj.Apttus__Agreement__c);
           String strFileId = obj.Apttus__URL__c.substringAfterLast('file=');
           setFileIdFromApttDoc.add(strFileId);

       }

       List<Document> listDocument = [SELECT Name, Description,Body, AuthorId 
                                        FROM Document WHERE Id IN :setFileIdFromApttDoc];

        for(Document objDoc : listDocument){
            ContentVersion cv = new ContentVersion();
            cv.ContentLocation = 'S';
            cv.Description = objDoc.Description;
            //cv.OwnerId = objDoc.AuthorId;
            cv.PathOnClient = objDoc.Name;
            cv.Title = objDoc.Name;
            cv.VersionData = objDoc.Body;
            listContentVersionInsert.add(cv);
        }

        System.debug('listContentVersionInsert.size(): ' + listContentVersionInsert.size());
        System.debug('listContentVersionInsert: ' + listContentVersionInsert);
        if (listContentVersionInsert.size() > 0) {
            Database.insert(listContentVersionInsert);
        }
        
        System.debug('mapFileNameByAggId.keySet(): ' + mapFileNameByAggId.keySet());

        Map<String,String> mapAggApptusByAggId = new Map<String,String>();
        for (Agreement__c obj : [SELECT Id, Apptus_Agreement_ID__c FROM Agreement__c 
                                 WHERE Apptus_Agreement_ID__c IN :mapFileNameByAggId.values()]) {
           
            mapAggApptusByAggId.put(obj.Apptus_Agreement_ID__c, obj.Id);

        }
        for (ContentVersion objCV : [SELECT Id, Title, ContentDocumentId FROM ContentVersion 
                                      WHERE Title =: mapFileNameByAggId.keySet()]) {
             ContentDocumentLink cdl = new ContentDocumentLink();

             System.debug('apttus agreement id: ' + mapFileNameByAggId.get(objCV.Title));
             String apttusAggId = mapFileNameByAggId.get(objCV.Title);
             String newAggId = mapAggApptusByAggId.get(apttusAggId);

             System.debug('newAggId: ' + newAggId);

             cdl.LinkedEntityId = newAggId;
             cdl.ShareType = 'I';
             cdl.ContentDocumentId = objCV.ContentDocumentId;

             listContentDocLink.add(cdl);
        }
        System.debug('listContentDocLink.size(): ' + listContentDocLink.size());
        System.debug('listContentDocLink: ' + listContentDocLink);
        if (listContentDocLink.size() > 0) {
            Database.insert(new List<ContentDocumentLink>(listContentDocLink),false);
        }
    }

    global void finish( Database.BatchableContext bcMain ){
       
        System.debug('finish');

    }
      
}