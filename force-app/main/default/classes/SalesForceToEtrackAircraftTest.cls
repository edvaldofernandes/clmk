@isTest(SeeAllData=true)
public class SalesForceToEtrackAircraftTest {
    
    @isTest static void sfToEtrackAircraftTest(){
        
        try{
            
            Test.startTest();
        
        	Account account = new Account(Name='Test test test test ');
        	account.Company_Nickname__c = 'test tt';
        	account.FlyEmbraerId__c = '1234550000000000';
        	insert account;
        
        	Aircraft__c aircraft = new Aircraft__c();
        	aircraft.Model_Type__c = '190' ;
        	aircraft.Operator__c = account.id;
        	aircraft.Owner__c = account.id;
        	aircraft.Registration__c = '4568';
        	aircraft.name = '09876123';
        	insert aircraft;
        
        	aircraft.Model_Type__c = '195';
        	aircraft.Operator__c = account.id;
        	aircraft.Owner__c = account.id;
        	aircraft.Registration__c = '4321';
        	aircraft.name = '12345678';
        
        	update aircraft;
        
        	delete aircraft;
        
        	Test.stopTest();
            
        }catch(Exception error){}    
    }
}