@isTest public class CustomButtonActionsTest {
    @isTest public static void ConstructorTest(){
        //Create test data
        Case c = new Case();
        insert c;
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        
        //Execute Test
        Test.startTest();
        CustomButtonActions cba = new CustomButtonActions(stdController);	//testing the constructor
        Test.stopTest();
        
        //Verify Results
        System.assertEquals(stdController.view().getURL(), cba.caseView.getURL());	//the page references should match
        System.assertEquals(c.Id,cba.c.Id);	//the IDs should match
    }
    
    @isTest public static void SetEmailReadTest(){
        //Create test data
        Case c = new Case(Incoming_email_i__c=true);
        insert c;
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        CustomButtonActions cba = new CustomButtonActions(stdController);	//get instance of CustomButtonActions class
        
        //Execute test
        Test.startTest();
        cba.SetEmailRead();	
        Test.stopTest();
        
        //Verify results
        Case result = [SELECT Incoming_email_i__c FROM Case WHERE Id = :c.id LIMIT 1];	//get result
		System.assert(!result.Incoming_email_i__c);    //Incoming email should be false after the function runs    
    }
    
    @isTest public static void DismissPRCAlertTest(){
        //Create test data
        Case c = new Case(Notify_POS__c=true);
        insert c;
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        CustomButtonActions cba = new CustomButtonActions(stdController);	//get instance of CustomButtonActions class
        
        //Execute test
        Test.startTest();
        cba.DismissPRCAlert();	
        Test.stopTest();
        
        //Verify results
        Case result = [SELECT Notify_POS__c FROM Case WHERE Id = :c.id LIMIT 1];	//get result
		System.assert(!result.Notify_POS__c);    //Notify POS should be false after the function runs    
    }
    
    @isTest public static void DismissPOSAlertTest(){
        //Create test data
        Case c = new Case(POS_Dispo__c=true);
        insert c;
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        CustomButtonActions cba = new CustomButtonActions(stdController);	//get instance of CustomButtonActions class
        
        //Execute test
        Test.startTest();
        cba.DismissPOSAlert();	
        Test.stopTest();
        
        //Verify results
        Case result = [SELECT POS_Dispo__c FROM Case WHERE Id = :c.id LIMIT 1];	//get result
		System.assert(!result.POS_Dispo__c);    //POS_Dispo should be false after the function runs    
    }
    
    @isTest public static void updateOwnerByApexTest_UserIsOwner(){
        //Create test data
        //create two new users
        Profile p = [SELECT Id FROM Profile WHERE Name='PRC'];
        User u1 = new User(Alias = 'standt1', Email='standarduser1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1029384756@testorg102938475601.com');
        User u2 = new User(Alias = 'standt2', Email='standarduser2@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser6574839201@testorg657483920102.com');
        
        List<User> usersList = new List<User>();
        usersList.add(u1);
        usersList.add(u2);
        insert usersList;
        //set case owner as user #1
        Case c = new Case(OwnerId=usersList[0].Id, RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId(), Subject='Test');
        insert c;
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        CustomButtonActions cba = new CustomButtonActions(stdController);	//get instance of CustomButtonActions class
        
        //Execute test
        Test.startTest();
        //run as user #1
        System.runAs(usersList[0]){
            cba.UpdateOwnerByApex();
        }
        Test.stopTest();
        
        //Verify results
        Case result = [SELECT OwnerId FROM Case WHERE Id = :c.id LIMIT 1];	//get result
		System.assertEquals(usersList[0].Id, result.OwnerId);    			//ownerId should match User #1's Id
    }
    
    @isTest public static void updateOwnerByApexTest_UserIsNotOwner(){
        //Create test data
        //create two new users
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u1 = new User(Alias = 'standt3', Email='standarduser3@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Test User', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1029384756@testorg102938475603.com');
        User u2 = new User(Alias = 'standt4', Email='standarduser4@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Current Case Owner', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser6574839201@testorg657483920104.com');
        
        List<User> usersList = new List<User>();
        usersList.add(u1);
        usersList.add(u2);
        insert usersList;
        //set case owner as user #2
        Case c = new Case(OwnerId=usersList[1].Id, RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId(), Subject='Test');
        insert c;
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        CustomButtonActions cba = new CustomButtonActions(stdController);	//get instance of CustomButtonActions class
        
        //Execute test
        
        //run as user #1
        System.runAs(usersList[0]){
            cba.UpdateOwnerByApex();
        }
        
        //Verify results
        Case result = [SELECT OwnerId FROM Case WHERE Id = :c.id LIMIT 1];	//get result
		System.assertEquals(usersList[0].Id, result.OwnerId);    			//ownerId should match User #1's Id   
    }
    
    @isTest public static void testFillEpoolCase(){
        //create part
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='1116-42-1116 MODS 604', Description__c='test', Ecode__c='2074610', Top_Most__c='9408215', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create case
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
        List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='NOT VALID YET: CRI - SATENA - 503341 - 24828417',
                           Description='*B2B - Customer Reference: 078/19'
                           +'\n*R3 - R3 Sales Order Exchange: 0024828417'
                           +'\n*R3 - R3 Notification: 000300695184'
                           +'\n*R3 - R3 Service Order: 10938245'
                           +'\n*B2B - Sales Force Number: 0002445551'
                           +'\n\n*B2B - STANDARD: Sales Force'
                           +'\n*R3 - R3 Customer Name: SERVICIO AEREO A TERRITORIOS'
                           +'\n*R3 - Customer Number:0000503341'
                           +'\n\n*B2B - Contract based on: Priority'
                           +'\n*B2B - DocumentType: EXCHANGE'
                           +'\n*B2B - Invalid Ship To [&1]'
                           +'\n*B2B - PERMANENT: Create new ShipTo, TEMPORARY: Update Order'
                           +'\n*B2B - Address: PERMANENT'
                           +'\n*B2B - Name: SATENA ATT: DEPÓSITO PRIVADO SATENA'
                           +'\n*B2B - SORT1: BOGOTA'
                           +'\n*B2B - STREET: AV ELDORADO ENTRADA 1, INTERIOR 1 -'
                           +'\n*B2B - LOCATION: EX'
                           +'\n*B2B - PO_BOX: 99999'
                           +'\n*B2B - City: BOGOTA'
                           +'\n*B2B - COUNTRY: CO'
                           +'\nUS EXPRESS PICK UP'
                           +'\nWarning: by changing agreed contractual Ship to address you are assuming all charges related to this specific shipment. Feel free to'
                           +'\n\n*R3 - Line Number: 010100'
                           +'\n*R3 - Emb.Code: 000000000002074610'
                           +'\n*R3 - Part Number: 1116-42-1116 MODS 604'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 00'
                           +'\n*B2B - Lead time to deliver this item (hours): 96.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n\n*R3 - Line Number: 010200'
                           +'\n*R3 - Emb.Code: 000000000002074610'
                           +'\n*R3 - Part Number: 1116-42-1116 MODS 604'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 02'
                           +'\n*R3 - Need by date (yyyy/mm/dd): 20190602'
                           +'\n*B2B - Lead time to deliver this item (hours): 24.00'
                           +'\n*B2B - PoolClass: [&1]',
                           RecordTypeId=rTypeId,
                           Is_Unit_EC_Blocked__c='No'));
        insert cases;
        
        cases[0].Subject = 'NEW SO: CRI - SATENA - 503341 - 24828417';
        update cases;

        ApexPages.StandardController stdController = new ApexPages.StandardController(cases[0]);
        CustomButtonActions cba = new CustomButtonActions(stdController);	//get instance of CustomButtonActions class
        
        Test.startTest();
        cba.FillEpoolCase();
        Test.stopTest();
        
        List<Case> result = [SELECT PO__c, SO__c, Notification__c, Part_Number__r.Name, Need_by_Date__c, Priority FROM Case WHERE Id = :cases[0].Id];
        
        /*System.assertEquals('078/19', result[0].PO__c);									//verify PO was populated
     	System.assertEquals('24828417', result[0].SO__c);   							//verify SO
        System.assertEquals('300695184', result[0].Notification__c); 					//verify notif
        System.assertEquals('1116-42-1116 MODS 604', result[0].Part_Number__r.Name);*/	//verify part number
        System.assertEquals(null, result[0].Need_by_Date__c);							//verify need by date is null
        //System.assertEquals('Critical', result[0].Priority);							//verify priority
    }
    
    private static MFIR__C createMFIR(Id accountId){
        
        MFIR__c mfir = new MFIR__c(
        	MFIR_number__c = '335353',
            SAP_Name__c = 'nome_sap_test',
            Account__c = accountId
        );
       
        return mfir;
    }
    
    private static Account createAccount(){
        
        Account acc = new Account(
        	BillingCountry = 'Brazil',
        	Name = 'Test',
        	Company_Nickname__c = 'testNickname',
        	FlyEmbraerId__c = '99999'
        );
       
        return acc;
    }
}