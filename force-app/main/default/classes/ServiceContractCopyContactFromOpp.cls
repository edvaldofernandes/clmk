/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for copying the opportunity contact role to the contact field
* of a ServiceContract. If there is a primary contact role, this one will be assigned to
* the ServiceContract, if there is no a primary contact role, the first record
* returned by the query will be assigned to the ServiceContract.
*
* NAME: ServiceContractCopyContactFromOpp.cls
* AUTHOR: DPF                                                DATE: 16/12/2014
*******************************************************************************/
public with sharing class ServiceContractCopyContactFromOpp {

  public static void execute()
  {
  	Map<Id, List<ServiceContract>> mapIdOppContract = new Map<Id, List<ServiceContract>>();
  	for ( ServiceContract contrato : (List<ServiceContract>) trigger.new )
  	{
  		if ( contrato.Opportunity__c == null ) continue;
  		List<ServiceContract> lstContratos = mapIdOppContract.get(contrato.Opportunity__c);
  		if ( lstContratos == null )
  		{
  			lstContratos = new List<ServiceContract>();
  			mapIdOppContract.put(contrato.Opportunity__c, lstContratos);
  		}
  		lstContratos.add(contrato);
  	}
  	if ( mapIdOppContract.isEmpty() ) return;
  	
  	Map<Id, List<OpportunityContactRole>> mapOppContactRole = new Map<Id, List<OpportunityContactRole>>();
  	for ( OpportunityContactRole ocr : [SELECT OpportunityId, IsPrimary, ContactId 
  	  FROM OpportunityContactRole  
      WHERE OpportunityId =: mapIdOppContract.keySet()] )
    {
    	List<OpportunityContactRole> lstOcr = mapOppContactRole.get(ocr.OpportunityId);
    	if ( lstOcr == null )
    	{
    		lstOcr = new List<OpportunityContactRole>();
    		mapOppContactRole.put(ocr.OpportunityId, lstOcr);
    	}
    	lstOcr.add(ocr);
    }
    if ( mapOppContactRole.isEmpty() ) return;
    
    for ( Id oppId : mapIdOppContract.keySet() )
    {
    	List<ServiceContract> lstContratos = mapIdOppContract.get(oppId);
    	if ( lstContratos == null ) continue;
    	List<OpportunityContactRole> lstOcr = mapOppContactRole.get(oppId);
    	if ( lstOcr == null ) continue;
    	
    	OpportunityContactRole contato = lstOcr[0];
    	for ( OpportunityContactRole ocr : lstOcr )
    	{
    		if ( ocr.IsPrimary )
    		{
    			contato = ocr;
    			break;
    		}
    	}
     	for ( ServiceContract contrato : lstContratos )
      {
        contrato.ContactId = contato.ContactId;
      }     	
    }
  }
  
}