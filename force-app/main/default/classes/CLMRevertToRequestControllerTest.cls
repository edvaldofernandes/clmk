@isTest
private class CLMRevertToRequestControllerTest {
    
    static testMethod void RevertProposal(){
        Account testAc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(testAc);
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp 1';
        opp.Fleet_Type__c = 'Turboprop';
        opp.StageName = 'Validation_of_approvals';
        opp.CloseDate = system.today();
        insert opp;
        Kickoff__c ko = new Kickoff__c();
        ko.Name = 'Checklist 1';
        ko.Opportunity__c = opp.Id;
        ko.Customer_Type__c = 'New Customer';
        ko.submitted__c = true;
        ko.Kick_Off_Meeting_Date__c = system.today();
        insert ko;       
        
        Agreement__c agreementPropo = SObjectInstanceTest.createApttusAgreement(testAc.Id);
    	agreementPropo.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        agreementPropo.Status_Category__c = 'Proposal Approved';
        agreementPropo.Bypass_Approval__c = false;
        agreementPropo.Kickoff__c = ko.Id;
        agreementPropo.Comments__c = 'ok'; 
        database.insert(agreementPropo);
        //agreementPropo.Status_Category__c =false'Proposal Approved';
        agreementPropo.Bypass_Approval__c = true;
        agreementPropo.Comments__c = 'ok'; 
        database.update(agreementPropo);
                
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',agreementPropo.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(agreementPropo);
        CLMRevertToRequestController rr = new CLMRevertToRequestController(sc);
        rr.revert();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Agreement__c.Id = :agreementPropo.Id AND Agreement__c.Status_Category__c='Proposal Request'];
        System.assertEquals(1,testAgreements.size());
    }
    
    static testMethod void RevertPA(){
        Account testAcc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(testAcc);
        
        Agreement__c agreementPA = SObjectInstanceTest.createApttusAgreement(testAcc.Id);
    	agreementPA.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        agreementPA.Status_Category__c = 'PA Approved';
        agreementPA.Bypass_Approval__c = false;
        database.insert(agreementPA);
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',agreementPA.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(agreementPA);
        CLMRevertToRequestController rr = new CLMRevertToRequestController(sc);
        rr.revert();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Agreement__c.Id = :agreementPA.Id AND Agreement__c.Status_Category__c='PA Request'];
        System.assertEquals(1,testAgreements.size());
    }
    
    static testMethod void RequirementsNotMet(){
        Account testAcc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(testAcc);
        
        Agreement__c agreementNotMet = SObjectInstanceTest.createApttusAgreement(testAcc.Id);
    	agreementNotMet.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        
        agreementNotMet.Bypass_Approval__c = false;
        database.insert(agreementNotMet);
        agreementNotMet.Status_Category__c = 'Amended';
        database.update(agreementNotMet);

        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',agreementNotMet.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(agreementNotMet);
        CLMRevertToRequestController rr = new CLMRevertToRequestController(sc);
        rr.revert();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Agreement__c.Id = :agreementNotMet.Id AND Agreement__c.Status_Category__c='PA Request'];
        System.assertEquals(0,testAgreements.size());
    }
    
    static testMethod void RevertPASigned(){
        Account testAcc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(testAcc);
        
        Agreement__c agreementPA = SObjectInstanceTest.createApttusAgreement(testAcc.Id);
    	agreementPA.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        agreementPA.Status_Category__c = 'PA Signed';
        agreementPA.Status__c = 'Activated';
        agreementPA.Bypass_Approval__c = false;
        database.insert(agreementPA);
        agreementPA.Status_Category__c = 'PA Signed';
        agreementPA.Status__c = 'Activated';
        database.update(agreementPA);
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',agreementPA.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(agreementPA);
        CLMRevertToRequestController rr = new CLMRevertToRequestController(sc);
        rr.revert();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Agreement__c.Id = :agreementPA.Id AND Agreement__c.Status_Category__c='PA Request'];
        System.assertEquals(1,testAgreements.size());
    }
    
    /*
    static testMethod void FailRevert(){
        try{
            CLMRevertToRequestController rr = new CLMRevertToRequestController();
        	rr.revert();
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('ID not found') ? true : false;
            system.assertEquals(true,expectedExceptionThrown);      
        }
    }
	*/    

}