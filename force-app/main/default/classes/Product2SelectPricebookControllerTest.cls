/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the class Product2SelectPricebookController.
* 
*
* NAME: Product2SelectPricebookController.cls
* AUTHOR:LMdO                                                DATE: 19/12/2014
*******************************************************************************/

@isTest
private class Product2SelectPricebookControllerTest {

    static testMethod void myUnitTest() {
        
        Product2 produto = SObjectInstanceTest.createProduct2();
        Database.insert(produto);
        
        Id idStandard = SObjectInstanceTest.catalogoDePrecoPadrao();
        
        Pricebook2 pricebook = SObjectInstanceTest.createPricebook2();
        Database.insert(pricebook);
        
        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(idStandard, produto.Id);
        Database.insert(pbe);
        
        PageReference pageRef = Page.Product2SelectPricebook;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('addTo', produto.Id);
        
        Product2SelectPricebookController controller = new Product2SelectPricebookController (new ApexPages.Standardcontroller(produto));
        
        Product2SelectPricebookController.pbwrapper pdw = new Product2SelectPricebookController.pbwrapper( pricebook, true );
        pdw.isSelected = true;
        
        controller.Idprod = String.valueOf(produto.Id);
        controller.CarregarPricebooks();
        controller.Selected(); 
        
        
    }
}