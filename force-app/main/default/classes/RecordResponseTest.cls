@isTest
public class RecordResponseTest {

    @isTest static void recordResponseTest(){
        
        Test.startTest();
        
        BaseOnEvents__c baseEsightFos = new BaseOnEvents__c();
		baseEsightFos.sender__c = 'ESIGHT';
		baseEsightFos.receiver__c = 'SALES_FORCE_COMERCIAL';
		baseEsightFos.application__c = 'ESightFos__c';
		baseEsightFos.CreateFor__c = 'ESIGHT_FOS_INBOUND';
		baseEsightFos.BaseOn__c = 'ESIGHT_FOS_INBOUND';
		insert baseEsightFos;
        
        RecordResponse.recordResponse('SUCCESS', baseEsightFos.application__c);
        
        Test.stopTest();
    }
}