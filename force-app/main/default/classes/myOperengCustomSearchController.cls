public without sharing class myOperengCustomSearchController {
/*
 * This class is the controller of the Case's search bar.
 */
    @AuraEnabled
    public static List<Case> searchForIds(String searchText){
        /*
         * 	This method takes the argument and search through the cases and e-mails on
         *  database.
         */

        if(getCurrentUserAccountId() == NULL) // returns an empty set
            return new List<Case>();
        
        String query = myOperengCustomSearchController.buildQuery(searchText);
        List<List<sObject>> searchResult = searchInCasesAndEmails(query);
        
        Set<Case> cases = new Set<Case>( (List<Case>) searchResult[0] );

        List<EmailMessage> emails = searchResult[1];
        List<Case> casesFoundbyEmails = getCasesByEmails(emails);
        
        cases.addAll(casesFoundbyEmails);
       

        return New List<Case>(cases);
    }    
    public static  String buildQuery(String argument){
        /*
         * Splits the argument into tokens to be used as individual arguments for the
         * search.
         */ 
        if(argument.length() < 2)
             throw new AuraHandledException('Query must have two or more characters.');
        
        List<String> tokens = argument.split(' ');
        Iterator<String> iter = tokens.iterator();
        String query = '';
        String token = iter.next();
        if(token.length() >= 2)
                query += '*' + token + '*';
        while(iter.hasNext()){
        	token = iter.next();
            if(token.length() < 2)
                    continue;           
            query += ' OR ';

            query += '*' + token + '*';
        }
        if(query.length() < 2)
             throw new AuraHandledException('Please, try words bigger than two characters');
        return query;
    }
    public static List<Case> getCasesByEmails(List<EmailMessage> emails){
        /*
         * Do the SOSL the cases related to the e-mails inputted.
         * 
         */
        // get Cases Ids from the EmailMessage object
        List<Id> casesIds = New List<Id>();
        for(EmailMessage m : emails)
            casesIds.add(m.parentId);   
        // return the cases related to the email list
		return [
            SELECT
                Id,
                Subject,
                CreatedDate,
                ClosedDate,
                Status,
                Incoming_email_i__c
            FROM
                Case 
            WHERE
                AccountId =: getCurrentUserAccountId()
                AND Customer_Visible__c = TRUE
                AND Case.Status != 'Archived'
                AND RecordType.Name = 'FlightOps Case Record Type'
                AND Id IN :casesIds
        ];
    }
    public static Id getCurrentUserAccountId(){
        /* 
         * Gets the account Id from the logged user.
         */
        return [
            SELECT contact.account.Id
            FROM
            	User
            WHERE
            	Id =: UserInfo.getUserId()
            LIMIT 1
        ].contact.account.Id;
    }
    public static List<List<sObject>> searchInCasesAndEmails(String query){
        /*
         * Do search on the cases and e-mails based on the inputted argument.
         */
        return [
            FIND :query IN ALL FIELDS
            RETURNING
                Case(
                    Id,
                    Subject,
                    CreatedDate,
                    ClosedDate,
                    Status,
                    Incoming_email_i__c
                WHERE
                    AccountId =: getCurrentUserAccountId()
                    AND Customer_Visible__c = TRUE
                    AND Case.Status != 'Archived'
                    AND RecordType.Name = 'FlightOps Case Record Type'
                ),
                EmailMessage(
                        ParentId
                    WHERE
                        IsDeleted = false
                )
        ];
    }
}