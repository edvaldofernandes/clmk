/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Test Class for TriggerRollupAircraft cover TriggerHandlerAircraft     
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
@isTest
public class TriggerRollupAircraft_Test {

	@isTest
	public static void itShouldTestAllRollUp(){

        insert create_account_test_data();
    
		insert create_aircraft_test_data(create_account_test_data());
		
	}
    
    @isTest
	public static void itShouldTestTriggerDisable(){

        insert create_account_test_data();
        
    	GEN_TriggerHelper.disableTrigger();
		insert create_aircraft_test_data(create_account_test_data());
		
	}
    
    static List<Account> create_account_test_data(){
        Account acc1 = new Account();
        acc1.BillingCountry = 'Brazil';
        acc1.Name = 'Name';
        acc1.Company_Nickname__c = 'Nickname';
        acc1.ICAO_Code__c = 'ICAO';
        
        Account acc2 = new Account();
        acc2.BillingCountry = 'Brazil';
        acc2.Name = 'Name2';
        acc2.Company_Nickname__c = 'Nickname2';
        acc2.ICAO_Code__c = 'ICAO2';
        
        return new List<Account>{acc1, acc2};
    }
    
    static List<Aircraft__c> create_aircraft_test_data(List<Account> listAcc){
        
        List<Aircraft__c> listAir = new  List<Aircraft__c>();
        
        for(integer i=0;i<=100;i++){
            
            Aircraft__c aircraft = new Aircraft__c();
            String strName =  string.valueOf(i) + 'Embraer';
            aircraft.Name = strName.left(8);
            aircraft.Commercial_Name__c = 'Aircraft';
            if(i <= 50){
                aircraft.Owner__c = listAcc.get(0).Id;
                aircraft.Operator__c = listAcc.get(0).Id;
                aircraft.Aircraft_Status__c = 'In Service';
                aircraft.Model_Type__c = 'E170';
                aircraft.RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Embraer').getRecordTypeId();
            }else{
                aircraft.Owner__c = listAcc.get(1).Id;
               aircraft.Operator__c = listAcc.get(1).Id;
                aircraft.Aircraft_Status__c = 'Parked';
                aircraft.Model_Type__c = 'E175';
                aircraft.RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Non Embraer').getRecordTypeId();
            }
            listAir.add(aircraft);
        }
        return listAir;
    }
}