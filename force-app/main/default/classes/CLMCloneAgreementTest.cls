@isTest
/*
 * Test Class for CLMCloneAgreement, AutoNamingRules, CLMChangeShowAircraftAs and CLMSyncAmendment
*/
public class CLMCloneAgreementTest {
    private static final String PROMOTE_ACTION = 'Promote';
    private static final String AMEND_ACTION = 'Amend';
    
	static testMethod void cloneTest(){
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        database.insert(productTest);
        
        id priceBook2Id = SObjectInstanceTest.getPricebook2Std2();
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(priceBook2Id, productTest.Id); 
        database.insert(priceBookEntryTest);
        
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        //Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(new List<Agreement__c>{Agreement1});
        

        //create users to assign at agreement team
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userTest1 = SObjectInstanceTest.createUser(p.Id);
        userTest1.Username = 'usertest1@mail.com.br'; userTest1.CommunityNickname = 'Community Nickname1';
        User userTest2 = SObjectInstanceTest.createUser(p.Id);
        userTest2.Username = 'usertest2@mail.com.br'; userTest2.CommunityNickname = 'Community Nickname2';
        User userTest3 = SObjectInstanceTest.createUser(p.Id);
        userTest3.Username = 'usertest3@mail.com.br'; userTest3.CommunityNickname = 'Community Nickname3';
        database.insert(new List<User>{userTest1,userTest2,userTest3});
        
        //create a agreement team related list to be cloned
        Agreements_Team__c team1 = new Agreements_Team__c(Commercial_Agreement__c = Agreement1.Id, User__c = userTest1.id);
        Agreements_Team__c team2 = new Agreements_Team__c(Commercial_Agreement__c = Agreement1.Id, User__c = userTest2.id);
        Agreements_Team__c team3 = new Agreements_Team__c(Commercial_Agreement__c = Agreement1.Id, User__c = userTest3.id);
        database.insert(new List<Agreements_Team__c>{team1,team2,team3});
        
        //create contracts pdp to be cloned
        Contract_PDP__c contract1 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = 'Initial Deposit', Payment_ammount__c = 1, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract2 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '1 Progress Payment', Payment_ammount__c = 2, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract3 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '2 Progress Payment', Payment_ammount__c = 3, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract4 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '3 Progress Payment', Payment_ammount__c = 4, Payment_Date__c = Date.today().addDays(1));
        database.insert(new List<Contract_PDP__c>{contract1,contract2,contract3,contract4});
        
        //create aircraft prices to be cloned
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2});
        
        //create escalation
        Escalation__c escalation1 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 50);
        Escalation__c escalation2 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 35);
        database.insert(new List<Escalation__c>{escalation1,escalation2});
        
        //create line item to be cloned
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft2.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft3.Aircraft_Price__c = aircraftPrice1.Id;
        
        database.insert(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3});
        agreementAircraft1.Escalation_Code__c = escalation1.Id;
        agreementAircraft2.Escalation_Code__c = escalation2.Id;
        database.update(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2});
        
        Agreement1.Status_Category__c = 'PA Signed';
        database.update(new List<Agreement__c>{Agreement1});
		//system.debug('CLMCloneAgreement.cloneAgreement >>> ' + Agreement1);
		system.debug('escalations before amendment ' + [SELECT id, Escalation_Rate__c, Related_Escalation__c, Commercial_Agreement__c FROM Escalation__c]);

        Task task1 = SObjectInstanceTest.createTask();
        Task task2 = SObjectInstanceTest.createTask();
        Agreement__c NewAgreement = [SELECT id from Agreement__c LIMIT 1];       
        task1.WhatId = NewAgreement.Id;
        task2.WhatId = NewAgreement.Id;
        task2.Status = 'Completed';
        task2.Completion_date__c = system.today();
        database.insert(new List<Task>{task1,task2});
        
        test.startTest();
       		//CLMCloneAgreement clone = new CLMCloneAgreement(AMEND_ACTION);
        	//system.debug('CLMCloneAgreement.cloneAgreement >>> ' + Agreement1);
            String clonedAgreement = CLMCloneAgreement.cloneAgreement(String.valueOf(Agreement1.Id),'Amend');
            //system.debug('CLMCloneAgreement.cloneAgreement >>> ' + Agreement1);
        test.stopTest();
        
        //get the cloned agreement to asserts bellow
        //
        Agreement__c agrTest = [SELECT name FROM Agreement__c WHERE id = :NewAgreement.Id LIMIT 1];
        Agreement__c agreementCloned = [SELECT id FROM Agreement__c WHERE name = :(agrTest.name + ' Amendment') LIMIT 1]; 
        
        //Agreement__c agreementCloned = [SELECT id, RecordTypeId FROM Agreement__c WHERE id = :Id.valueOf(clonedAgreement) LIMIT 1];
        
        //assert to ensure that we have two news agreements
        system.assert([SELECT id FROM Agreement__c].size() == 2);

        //assert to ensure that the original proposal has the field PA_Already_set__c equals TRUE
                //system.debug('Proposal: ' + RecordTypeMemory.getRecType('Agreement__c', 'Proposal'));
        //system.debug('Purchase_Agreement: ' + RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement'));
        //system.debug('Agreement IDs: ' + [SELECT id, RecordTypeId, PA_Already_set__c FROM Agreement__c]); 
       	//system.assert([SELECT id, PA_Already_set__c FROM Agreement__c WHERE RecordTypeId = :RecordTypeMemory.getRecType('Agreement__c', 'Purchase Agreement')].PA_Already_set__c == true);
        
        //assert to ensure that one of them has record type equals "Purchase Agreement"
        system.assert([SELECT id, Is_Amendment__c FROM Agreement__c WHERE Is_Amendment__c = true].size() == 1);
        
        //assert to ensure that agreement team's list is the same that original agreement
        system.assert([SELECT id FROM Agreements_Team__c WHERE Commercial_Agreement__c =: agreementCloned.Id].size() == 3);
        
        //assert to ensure that contract pdp list is the same that original agreement
        system.assert([SELECT id FROM Contract_PDP__c WHERE Commercial_Agreement__c =: agreementCloned.Id].size() == 4);
        
        //assert to ensure that aircraft price list is the same that original agreement
        //System.debug('aircraft>>>' + [SELECT id FROM Aircraft_Price__c WHERE Commercial_Agreement__c =: agreementCloned.Id].size());
        system.assert([SELECT id FROM Aircraft_Price__c WHERE Commercial_Agreement__c =: agreementCloned.Id].size() == 2);
        
        //assert to ensure that contract pdp list is the same that original agreement
        system.assert([SELECT id FROM Agreement_Aircraft__c WHERE Agreement__c =: agreementCloned.Id].size() == 3);
        
        //assert to ensure that there is relationship beetwen original agreementlineitem and cloned agreementlineitem
        system.assert([SELECT id, Related_Agreement_Aircraft__c FROM Agreement_Aircraft__c WHERE Related_Agreement_Aircraft__c =: agreementAircraft2.Id].size() == 1);
        
        //assert to ensure that there is relationship beetwen cloned agreementlineitem and origianl agreementlineitem
        //system.debug('Related agreement aircraft ' + [SELECT Related_Agreement_Aircraft__c, Id FROM Agreement_Aircraft__c WHERE Id =: agreementAircraft2.Id LIMIT 1]);
        //system.assert([SELECT id, Related_Agreement_Aircraft__c FROM Agreement_Aircraft__c WHERE Id =: agreementAircraft2.Id LIMIT 1].Related_Agreement_Aircraft__c != null);
        
        //assert to ensure the relationship beetwen line item clone and aircraft price clone
        system.assert([SELECT id, Aircraft_Price__c FROM Agreement_Aircraft__c WHERE Related_Agreement_Aircraft__c =: agreementAircraft1.Id].Aircraft_Price__c != null);
    	
        agreementAircraft1.Airport_of_Destination__c = 'syncTest';
        escalation2.Escalation_Rate__c = 2.1;
        
        database.update(new List<Agreement_Aircraft__c>{agreementAircraft1});
        database.update(new List<Escalation__c>{escalation2});
        
        
        
        //system.debug('SOBCOUNT' + [SELECT Id, Contract_Aircraft__c FROM SOB_Event__c].size());
        CLMSyncAmendment.syncAmendment(agreementCloned.Id+'');
        
        system.assert([SELECT id, Airport_of_Destination__c FROM Agreement_Aircraft__c WHERE Airport_of_Destination__c = 'syncTest'].size() == 2);
        
        system.assert([SELECT id, Escalation_Rate__c FROM Escalation__c WHERE Escalation_Rate__c = 2.1].size() == 2);
        
        System.debug([SELECT id, whatId FROM Task WHERE whatId = :agreementCloned.id]);
        system.assert([SELECT id, whatId FROM Task WHERE whatId = :agreementCloned.id].size() == 1);
    	
        system.assert([SELECT id, whatId FROM Task].size() == 2);
        
        agreementCloned.Show_All_Aircraft_As__c = 'Firm';
        database.update(new List<Agreement__c>{agreementCloned});
        //system.debug('CLMChangeShowAircraftAs TEST ' + [SELECT id, Show_Aircraft_As__c, Agreement__c FROM Agreement_Aircraft__c WHERE Agreement__c = :agreementCloned.id]);
        system.assert([SELECT id, Show_Aircraft_As__c, Agreement__c FROM Agreement_Aircraft__c WHERE Agreement__c = :agreementCloned.id AND Show_Aircraft_As__c = 'Firm'].size() == 3);
        
        agreementCloned.Show_All_Aircraft_As__c = 'Option';
        database.update(new List<Agreement__c>{agreementCloned});
        system.assert([SELECT id, Show_Aircraft_As__c, Agreement__c FROM Agreement_Aircraft__c WHERE Agreement__c = :agreementCloned.id AND Show_Aircraft_As__c = 'Option'].size() == 3);
    
    	//CLMCloneAgreement clone2 = new CLMCloneAgreement(AMEND_ACTION);
        String clonedAgreement2 = CLMCloneAgreement.cloneAgreement(String.valueOf(Agreement1.Id),'Amend');
        
    }
    
    	static testMethod void numberingTest(){
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        database.insert(productTest);
        
        id priceBook2Id = SObjectInstanceTest.getPricebook2Std2();
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(priceBook2Id, productTest.Id); 
        database.insert(priceBookEntryTest);
        
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        //Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
        database.insert(new List<Agreement__c>{Agreement1});
        

        //create users to assign at agreement team
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userTest1 = SObjectInstanceTest.createUser(p.Id);
        userTest1.Username = 'usertest1@mail.com.br'; userTest1.CommunityNickname = 'Community Nickname1';
        
        //create contracts pdp to be cloned
        Contract_PDP__c contract1 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = 'Initial Deposit', Payment_ammount__c = 1, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract2 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '1 Progress Payment', Payment_ammount__c = 2, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract3 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '2 Progress Payment', Payment_ammount__c = 3, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract4 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '3 Progress Payment', Payment_ammount__c = 4, Payment_Date__c = Date.today().addDays(1));
        database.insert(new List<Contract_PDP__c>{contract1,contract2,contract3,contract4});
        
        //create aircraft prices to be cloned
        Aircraft_Price__c aircraftPrice1 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = SObjectInstanceTest.createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2});
        
        //create escalation
        Escalation__c escalation1 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 50);
        Escalation__c escalation2 = SObjectInstanceTest.createDefaultEscalation(Agreement1.Id, 35);
        database.insert(new List<Escalation__c>{escalation1,escalation2});
        
        //create line item to be cloned
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Option');
        
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft2.Aircraft_Price__c = aircraftPrice1.Id;
        agreementAircraft3.Aircraft_Price__c = aircraftPrice1.Id;
        
        database.insert(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3});
        agreementAircraft1.Escalation_Code__c = escalation1.Id;
        agreementAircraft2.Escalation_Code__c = escalation2.Id;
        agreementAircraft1.TREND_Delivery_Date__c = system.today();
        database.update(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2});
        
        test.startTest();
        Agreement1.Agreement_Code__c = 'TEST';
        database.update(new List<SObject>{Agreement1});
        test.stopTest();
        
        List<Agreement_Aircraft__c> acList = [SELECT Id, DF_Code__c, Agreement__c FROM Agreement_Aircraft__c WHERE Agreement__c = :Agreement1.Id ORDER BY DF_Code__c];
        system.debug('cloned aircrafts' + acList);
        system.assert(acList[0].DF_Code__c == 'TEST-01(AK-01) OP');
        system.assert(acList[1].DF_Code__c == 'TEST-02(AK-02) OP');
        system.assert(acList[2].DF_Code__c == 'TEST-03(AK-03) OP');
               
    }
    
}