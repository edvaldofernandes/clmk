/* Classe implementadora de SOBjectDAO para operações DML no objeto Task_Setup__c, utilizando pattern de Singleton.
* @author - Elvia Serpa
*			elvsantos@deloitte.com
* @version 1.0 14/01/2016
*/
public with sharing class TaskSetupDAO 
{
    
    private static final TaskSetupDAO instance = new TaskSetupDAO();    
    
    private TaskSetupDAO(){
    }    
    
    public static TaskSetupDAO getInstance(){
        return instance;
    }
    
    public List<Task_Setup__c> getTaskSetup()
    {
        List<Task_Setup__c> lstTaskSetup = [ Select Name , 
        										   Question__c , 
        									       Answer__c , 
        									       Description__c , 
        									       Program__c , 
        										   Selected_Date__c , 
        										   Before_Selected_date__c , 
        										   Sequence__c ,
        										   Leap_Time__c, 
        										   Question_API__c ,
        										   Refers_To__c,
                                            	   RecordType.Name,
                                            	   Date_API__c
        									 From 
        									 	   Task_Setup__c ];
        return lstTaskSetup;        
    }
    
    public List<Task_Setup__c> getTaskSetupByRecordType(String recordTypeName)
    {
        List<Task_Setup__c> lstTaskSetup = [ Select Name , 
        										   Question__c , 
        									       Answer__c , 
        									       Description__c , 
        									       Program__c , 
        										   Selected_Date__c , 
        										   Before_Selected_date__c , 
        										   Sequence__c ,
        										   Leap_Time__c, 
        										   Question_API__c ,
        										   Refers_To__c,
                                            	   RecordType.Name,
                                            	   Date_API__c
        									 From 
        									 	   Task_Setup__c
                                            Where RecordType.Name = :recordTypeName];
        return lstTaskSetup;        
    }
    
    public Task_Setup__c getSObjectTaskSetup(String deletedFields)
    {
        Task_Setup__c lstTaskSetup = new Task_Setup__c();
        try {
            lstTaskSetup = [ Select 
                        	Deleted_Fields__c 
                        From 
                        	Task_Setup__c 
                        Where 
                        	RecordType.DeveloperName =  :deletedFields ];
        } Catch (Exception e){
            System.debug('There is not Tasks Setup with record type ' + deletedFields);
            return null;
        }
        return lstTaskSetup;        
    } 
    
    public List<Task_Setup__c> getTaskSetupBySequence(Set<Integer> setTask)
    {
		List<Task_Setup__c> lstTaskSetup = [Select 
												Selected_Date__c, 
												Before_Selected_date__c, 
												Leap_Time__c, 
												Sequence__c 
											From 
												Task_Setup__c  
											Where 
												Sequence__c IN :setTask ];
        
        return lstTaskSetup;        
    }             
}