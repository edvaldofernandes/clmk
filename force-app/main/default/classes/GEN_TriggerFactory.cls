/**
* @author Marcilio Leite de Souza
* @date 20/08/2018
* @description: Trigger Factory register  the handlers
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           20 AGO 2018             Original Version
**/
public without sharing class GEN_TriggerFactory{

    /**
    * @description : Create and execute a trigger handler
    * @param Schema.sObjectType soType : Object type to process (SObject.sObjectType)
    * @Throws : A TriggerException if no handler has been coded.
	* @return void
    **/
    public static void createHandler(Schema.sObjectType soType) {
        
        // Get a handler appropriate to the object being processed
        GEN_ITrigger handler = getHandler(soType);
        
        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler == null) {
            throw new GEN_TriggerException('No Trigger Handler registered for Object Type: ' + soType);
        }
        
        // Execute the handler to fulfil the trigger
       execute(handler);
    }
     
    /**
    * @description : Control the execution of the handler
    * @param GEN_ITrigger handler : A Trigger Handler to execute
	* @return void
    **/
    @TestVisible 
    private static void execute(GEN_ITrigger handler) {
        
        // Before Trigger
        if (Trigger.isBefore) {
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkBefore();
            
        } else if(Trigger.isAfter) {
            // Call the bulk after to handle any caching of data and enable bulkification
            handler.bulkAfter();
        }
        
        // Perform any post processing
        handler.andFinally();
    }
    
    /**
    * @description : Get the appropriate handler for the object type.
    * @param Schema.sObjectType soType : Object type tolocate (SObject.sObjectType)
	* @return GEN_ITrigger : A trigger handler if one exists or null.
    **/
    @TestVisible
    private static GEN_ITrigger getHandler(Schema.sObjectType soType){
        
        if (soType == Aircraft__c.sObjectType) {
            return new TriggerHandlerAircraft();
        }
        
        if (soType == Service_Contract_Management__c.sObjectType) {
            return new TriggerHandlerServiceContractMgmt();
        }
                
        return null;
    }
}