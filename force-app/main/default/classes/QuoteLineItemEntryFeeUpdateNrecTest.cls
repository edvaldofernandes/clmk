/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class 
* QuoteLineItemEntryFeeUpdateNrec
*
* NAME: QuoteLineItemEntryFeeUpdateNrecTest.cls
* AUTHOR: AFC                                                  DATE: 24/03/2015
*******************************************************************************/
@isTest
public with sharing class QuoteLineItemEntryFeeUpdateNrecTest 
{
	private static final integer LOTE = 200;
	
	private static id accRecTypeId = RecordTypeMemory.getRecType( 'Account', 'Aircraft_OEM' );
  private static id oppRecTypeId = RecordTypeMemory.getRecType( 'Opportunity', 'Aircraft_Modification' );
	  
  static testMethod void myUnitTest()
  {
    Account acc = SObjectInstanceTest.createAccount( accRecTypeId );
    database.insert( acc );
    
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
	  pbAirMod.Name = 'Aircraft Modification';
	  pbAirMod.Type__c = 'Aircraft Modification / Technical Services';
    database.insert( pbAirMod );
    
    Opportunity opp = SObjectInstanceTest.createOpportunity();
    opp.RecordTypeId = oppRecTypeId;
    database.insert( opp );
    
    Quote quote = SObjectInstanceTest.createQuote( opp.Id );
    quote.Pricebook2Id = pbAirMod.Id;
    database.insert( quote );   
    
    Product2 prod = SObjectInstanceTest.createProduct2();
	  prod.Unit__c = 'Aricraft';
	  prod.Product_Type__c = 'Aircraft Modification';
	  prod.Family = 'Aircraft Modification';
	  prod.Applicability__c = 'Turboprop';
    database.insert( prod );
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    database.insert( pbe );
    
    PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    database.insert( pbeAirMod );
      
    list< QuoteLineItem > lstQItem = new list< QuoteLineItem >();
    
    QuoteLineItem qItemEntryFee = SObjectInstanceTest.createQuoteLineItem(quote.Id, pbeAirMod.Id);
    qItemEntryFee.Princing__c = 'Entry fee';
    lstQItem.add( qItemEntryFee );
    
    QuoteLineItem qItemNrec = SObjectInstanceTest.createQuoteLineItem(quote.Id, pbeAirMod.Id);      
	  qItemNrec.Princing__c = 'NREC';
	  qItemNrec.Entry_fee__c = 200;
    lstQItem.add( qItemNrec );
    
    database.insert( lstQItem );

    test.startTest();
    database.delete( qItemEntryFee );    
    test.stopTest();
    
    list< QuoteLineItem > lstQItemNrecUp = new list< QuoteLineItem >( 
      [ SELECT id, Entry_fee__c FROM QuoteLineItem WHERE id =: qItemNrec.Id ] );    
    
    for( QuoteLineItem qItemNrecUp : lstQItemNrecUp )
      system.assertEquals(0, qItemNrecUp.Entry_fee__c );
  }
  
  static testMethod void testLOTE()
  {
    Account acc = SObjectInstanceTest.createAccount( accRecTypeId );
    database.insert( acc );
    
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
    pbAirMod.Name = 'Aircraft Modification';
    pbAirMod.Type__c = 'Aircraft Modification / Technical Services';
    database.insert( pbAirMod );
    
    Opportunity opp = SObjectInstanceTest.createOpportunity();
    opp.RecordTypeId = oppRecTypeId;
    database.insert( opp );
    
	  Quote quote = SObjectInstanceTest.createQuote( opp.Id );
	  quote.Pricebook2Id = pbAirMod.Id;	   
    database.insert( quote );
     
    Product2 prod = SObjectInstanceTest.createProduct2();
    prod.Unit__c = 'Aricraft';
    prod.Product_Type__c = 'Aircraft Modification';
    prod.Family = 'Aircraft Modification';
    prod.Applicability__c = 'Turboprop';
    database.insert( prod );
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    database.insert( pbe );
    
    PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    database.insert( pbeAirMod );
      
    list< QuoteLineItem > lstQItem = new list< QuoteLineItem >();
    list< QuoteLineItem > lstLineItemDelete = new list< QuoteLineItem >();    
    for(integer i = 0; i < LOTE; i++)
    {
      QuoteLineItem qItemEntryFee = SObjectInstanceTest.createQuoteLineItem( quote.Id, pbeAirMod.Id );
	    qItemEntryFee.Princing__c = 'Entry fee';
	    lstQItem.add( qItemEntryFee );
	    lstLineItemDelete.add( qItemEntryFee );
	    
	    QuoteLineItem qItemNrec = SObjectInstanceTest.createQuoteLineItem( quote.Id, pbeAirMod.Id );      
	    qItemNrec.Princing__c = 'NREC';
	    qItemNrec.Entry_fee__c = 200;
	    lstQItem.add( qItemNrec );
    }    
    database.insert( lstQItem );
    
    test.startTest();
    database.delete( lstLineItemDelete );    
    test.stopTest();
    
    list< QuoteLineItem > lstQItemNrecUp = new list< QuoteLineItem >( [ SELECT id, Entry_fee__c FROM QuoteLineItem WHERE Princing__c =: 'NREC' ] );   
    
    for( QuoteLineItem qItemNrecUp : lstQItemNrecUp )
      system.assertEquals(0, qItemNrecUp.Entry_fee__c );
  }
}