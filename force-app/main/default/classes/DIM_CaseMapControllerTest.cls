// Used by DIM - Market Intelligence.
@isTest
public class DIM_CaseMapControllerTest {

    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateIconsByStatus() {
        System.assertEquals('standard:address', DIM_CaseMapController.getIcon('green'));
        System.assertEquals('standard:answer_public', DIM_CaseMapController.getIcon('yellow'));
        System.assertEquals('standard:dashboard', DIM_CaseMapController.getIcon('red'));
        System.assertEquals('standard:generic_loading', DIM_CaseMapController.getIcon(''));
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateCasesListWithAccountWithLatLong() {
    
        Account defaultAccount = new Account();
        defaultAccount.Name = 'Embraer São José dos Campos';
        defaultAccount.Company_Nickname__c = 'Embraer';
        defaultAccount.BillingCity = 'Sao Paulo';
        defaultAccount.BillingCountry = 'Brazil';
        defaultAccount.BillingLatitude = 2.2;
        defaultAccount.BillingLongitude = 2.2;
        defaultAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert defaultAccount;
        
        Account airline = new Account();
        airline.Name = 'Airline';
        airline.Company_Nickname__c = 'Airline';
        airline.BillingCity = 'Sao Paulo';
        airline.BillingCountry = 'Brazil';
        airline.BillingLatitude = 1.1;
        airline.BillingLongitude = 1.1;
        airline.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Operator').getRecordTypeId();
        airline.Embraer_Site__c = defaultAccount.Id;
        insert airline;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        caso.AccountId = airline.Id;
        insert caso;
        
        caso.Status = 'In Progress';
        update caso;

        System.assertEquals(1.1, DIM_CaseMapController.getCases('Sales_Support', 'In Progress', '', 'Embraer São José dos Campos').get(0).Account.BillingLatitude);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateCasesListWithAccountWithoutLatLongAndWithCountry() {
    
        Account defaultAccount = new Account();
        defaultAccount.Name = 'Embraer São José dos Campos';
        defaultAccount.Company_Nickname__c = 'Embraer';
        defaultAccount.BillingCity = 'Sao Paulo';
        defaultAccount.BillingCountry = 'Brazil';
        defaultAccount.BillingLatitude = 2.2;
        defaultAccount.BillingLongitude = 2.2;
        defaultAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert defaultAccount;
        
        Account airline = new Account();
        airline.Name = 'Airline';
        airline.Company_Nickname__c = 'Airline';
        airline.BillingCity = 'Sao Paulo';
        airline.BillingCountry = 'Brazil';
        airline.BillingLatitude = null;
        airline.BillingLongitude = null;
        airline.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Operator').getRecordTypeId();
        airline.Embraer_Site__c = defaultAccount.Id;
        insert airline;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        caso.AccountId = airline.Id;
        insert caso;
        
        caso.Status = 'In Progress';
        update caso;

        System.assertEquals(null, DIM_CaseMapController.getCases('Sales_Support', 'In Progress', '', 'Embraer São José dos Campos').get(0).Account.BillingLatitude);
        System.assertEquals('Brazil', DIM_CaseMapController.getCases('Sales_Support', 'In Progress', '', 'Embraer São José dos Campos').get(0).Account.BillingCountry);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateCasesListWithAccountWithoutLatLongAndWithoutCityCountry() {
    
        Account defaultAccount = new Account();
        defaultAccount.Name = 'Embraer São José dos Campos';
        defaultAccount.Company_Nickname__c = 'Embraer';
        defaultAccount.BillingCity = 'Sao Paulo';
        defaultAccount.BillingCountry = 'Brazil';
        defaultAccount.BillingLatitude = 2.2;
        defaultAccount.BillingLongitude = 2.2;
        defaultAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert defaultAccount;
        
        Account airline = new Account();
        airline.Name = 'Airline';
        airline.Company_Nickname__c = 'Airline';
        airline.BillingCity = null;
        airline.BillingCountry = null;
        airline.BillingLatitude = null;
        airline.BillingLongitude = null;
        airline.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Operator').getRecordTypeId();
        airline.Embraer_Site__c = defaultAccount.Id;
        insert airline;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        caso.AccountId = airline.Id;
        insert caso;
        
        caso.Status = 'In Progress';
        update caso;

        System.assertEquals(2.2, DIM_CaseMapController.getCases('Sales_Support', 'In Progress', '', 'Embraer São José dos Campos').get(0).Account.BillingLatitude);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateCasesListWithAccountWithoutLatLongAndWithoutCityCountryAndWithoutAnyInfoInEmbraerSite() {
    
        Account defaultAccount = new Account();
        defaultAccount.Name = 'Embraer São José dos Campos';
        defaultAccount.Company_Nickname__c = 'Embraer';
        defaultAccount.BillingCity = 'Sao Paulo';
        defaultAccount.BillingCountry = 'Brazil';
        defaultAccount.BillingLatitude = 2.2;
        defaultAccount.BillingLongitude = 2.2;
        defaultAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert defaultAccount;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        caso.Status = 'In Progress';
        update caso;

        System.assertEquals(2.2, DIM_CaseMapController.getCases('Sales_Support', 'In Progress', '', 'Embraer São José dos Campos').get(0).Account.BillingLatitude);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateStudyTypeShortened() {
        System.assertEquals('A, B', DIM_CaseMapController.getShortenedStudyType('A;B'));
        System.assertEquals('A, B, +2', DIM_CaseMapController.getShortenedStudyType('A;B;C;D'));
    }
    
   //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateSalesCasesListWithAccountWithLatLong() {
    
        Account defaultAccount = new Account();
        defaultAccount.Name = 'Embraer Europe';
        defaultAccount.Company_Nickname__c = 'Embraer';
        defaultAccount.BillingCity = 'Amsterdam';
        defaultAccount.BillingCountry = 'Netherlands';
        defaultAccount.BillingLatitude = 2.2;
        defaultAccount.BillingLongitude = 2.2;
        defaultAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert defaultAccount;
        
        Account airline = new Account();
        airline.Name = 'Airline';
        airline.Company_Nickname__c = 'Airline';
        airline.Geographical_Area__c = 'Europe';
        airline.BillingCity = 'Amsterdam';
        airline.BillingCountry = 'Netherlands';
        airline.BillingLatitude = 1.1;
        airline.BillingLongitude = 1.1;
        airline.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Operator').getRecordTypeId();
        airline.Embraer_Site__c = defaultAccount.Id;
        insert airline;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
        caso.AccountId = airline.Id;
        insert caso;

        System.assertEquals(1.1, DIM_CaseMapController.getCases('Sales', 'Open', 'EMEA', 'Embraer Europe').get(0).Account.BillingLatitude);
    }
    
   //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateSalesCasesListWithAccountWithoutAnyInformation() {
    
        Account defaultAccount = new Account();
        defaultAccount.Name = 'Embraer Latin America & Caribbean';
        defaultAccount.Company_Nickname__c = defaultAccount.Name;
        defaultAccount.BillingCity = 'Sao Paulo';
        defaultAccount.BillingCountry = 'Brazil';
        defaultAccount.BillingLatitude = 2.2;
        defaultAccount.BillingLongitude = 2.2;
        defaultAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert defaultAccount;
        
        Account latamAccount = new Account();
        latamAccount.Name = 'Embraer LATAM';
        latamAccount.Company_Nickname__c = latamAccount.Name;
        latamAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Embraer_Site').getRecordTypeId();
        insert latamAccount;
        
        Account airline = new Account();
        airline.Name = 'Airline';
        airline.Company_Nickname__c = 'Airline';
        airline.Geographical_Area__c = 'Latin America';
        airline.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Operator').getRecordTypeId();
        airline.Embraer_Site__c = latamAccount.Id;
        insert airline;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
        caso.AccountId = airline.Id;
        insert caso;

        System.assertEquals(2.2, DIM_CaseMapController.getCases('Sales', 'Open', 'Latin America', 'Embraer Latin America').get(0).Account.BillingLatitude);
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

}