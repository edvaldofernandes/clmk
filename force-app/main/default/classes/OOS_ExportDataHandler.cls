public with sharing class OOS_ExportDataHandler {
  public List<Out_of_service__c> oosData;

  public OOS_ExportDataHandler() {
  }

  public OOS_ExportDataHandler(ApexPages.StandardSetController controller) {
    controller.setPageSize(controller.getResultSize());
    oosData = controller.getRecords();
  }

  public Map<String, Object> fillInitialValues(
    Map<String, Object> recordFields,
    String fieldString
  ) {
    for (String field : fieldString.split(',')) {
      recordFields.put(field, '');
    }
    return recordFields;
  }

  public Map<Id, Id> getOOSIdMap(List<sObject> objs, String targetKey) {
    Map<Id, Id> oosIdMap = new Map<Id, Id>();
    for (sObject obj : objs) {
      Map<String, Object> mapObj = obj.getPopulatedFieldsAsMap();
      oosIdMap.put(
        (Id) mapObj.get('Out_of_service__c'),
        (Id) mapObj.get(targetKey)
      );
    }
    return oosIdMap;
  }

  public Map<Id, Map<String, Object>> getRecordIdMap(List<sObject> objs) {
    Map<Id, Map<String, Object>> recordMap = new Map<Id, Map<String, Object>>();
    for (sObject obj : objs) {
      Map<String, Object> mapObj = obj.getPopulatedFieldsAsMap();
      recordMap.put(obj.Id, mapObj);
    }
    return recordMap;
  }

  public Map<Id, Map<String, Object>> getRecordIdMap(
    List<sObject> objs,
    String targetId
  ) {
    Map<Id, Map<String, Object>> recordMap = new Map<Id, Map<String, Object>>();
    for (sObject obj : objs) {
      Map<String, Object> mapObj = obj.getPopulatedFieldsAsMap();
      system.debug(obj);
      system.debug(mapObj.get(targetId));
      recordMap.put((Id) mapObj.get(targetId), mapObj);
    }
    return recordMap;
  }

  public Map<Id, Map<String, Object>> getRecords() {
    String oosFields = Utils.getAllFields('Out_of_service__c');
    String fcFields = Utils.getAllFields('Fail_Code__c');
    String rcFields = Utils.getAllFields('Root_Code__c');
    String removalFields = Utils.getAllFields('PN_Removal__c');
    Map<String, Schema.SObjectField> mapTypeField = this.oosData[0]
      .getSObjectType()
      .getDescribe()
      .fields.getMap();

    String oosIds = '';
    for (Out_of_service__c oos : this.oosData) {
      oosIds += '\'' + oos.Id + '\'' + ',';
    }
    // remove last comma
    oosIds = oosIds.substring(0, oosIds.length() - 1);

    this.oosData = Database.query(
      'SELECT ' +
      oosFields +
      ' FROM Out_of_Service__c WHERE Id IN (' +
      oosIds +
      ')'
    );

    Map<Id, Map<String, Object>> records = new Map<Id, Map<String, Object>>();
    Map<Id, Id> oosFcAssociations = getOOSIdMap(
      Database.query(
        'SELECT Out_of_Service__c, Fail_Code__c FROM FC_OOS_Association__c WHERE Out_of_Service__c IN (' +
        oosIds +
        ')'
      ),
      'Fail_Code__c'
    );

    Map<Id, Id> oosRcAssociations = getOOSIdMap(
      Database.query(
        'SELECT Out_of_Service__c, Root_Code__c FROM RC_OOS_Association__c WHERE Out_of_Service__c IN (' +
        oosIds +
        ')'
      ),
      'Root_Code__c'
    );

    Map<Id, Map<String, Object>> failCodes = getRecordIdMap(
      [SELECT Id, Name, ATA__c, Sub_ATA__c, Technology_Name__c FROM Fail_Code__c]
    );

    Map<Id, Map<String, Object>> rootCodes = getRecordIdMap(
      [SELECT Id, Name, ATA__c, Supplier__c FROM Root_Code__c]
    );

    Map<Id, Map<String, Object>> suppliers = getRecordIdMap(
      [SELECT Id, Name FROM Supplier__c]
    );

    Map<Id, Map<String, Object>> pnRemovals = getRecordIdMap(
      Database.query(
        'SELECT Out_of_service__c, Name, PN_ON__c, SN_ON__c, SN_OFF__c, Pool__c, Rspl__c FROM PN_Removal__c WHERE Out_of_Service__c IN (' +
        oosIds +
        ')'
      ),
      'Out_of_service__c'
    );

    for (Out_of_service__c oos : this.oosData) {
      Map<String, Object> recordFields = new Map<String, Object>();
      Map<String, Object> mapOOS = oos.getPopulatedFieldsAsMap();
      Map<String, Object> failCode = failCodes.get(
        oosFcAssociations.get(oos.Id)
      );

      Map<String, Object> rootCode = rootCodes.get(
        oosRcAssociations.get(oos.Id)
      );

      Map<String, Object> pnRemoval = pnRemovals.get(oos.Id);
      //   system.debug(failCode);
      for (String key : oosFields.split(',')) {
        String type = String.valueOf(
          mapTypeField.get(key).getDescribe().getType()
        );

        if (mapOOS.get(key) != null) {
          recordFields.put(key, mapOOS.get(key));
        } else {
          recordFields.put(key, '');
          continue;
        }
        if (type == 'TEXTAREA') {
          recordFields.put(
            key,
            ((String) recordFields.get(key)).replaceAll('<br>', ' // ').replaceAll('</p><p>', ' // ').replaceAll('</li><li>', ' // ').replaceAll('</ol><ol>', ' // ')
          );
        } else if (type == 'DATE') {
          recordFields.put(
            key,
            ((Datetime) Date.valueOf(recordFields.get(key)))
              .formatGmt('dd/MM/yyyy')
          );
        } else if (type == 'TIME') {
          recordFields.put(
            key,
            ((Time) recordFields.get(key)).hour() +
            ':' +
            ((Time) recordFields.get(key)).minute()
          );
        }
      }

      for (String key : fcFields.split(',')) {
        if (failCode != null && failCode.get(key) != null) {
          recordFields.put('Fail_Code__c_' + key, failCode.get(key));
        } else {
          recordFields.put('Fail_Code__c_' + key, '');
        }
      }

      for (String key : rcFields.split(',')) {
        if (rootCode != null && rootCode.get(key) != null) {
          if (key == 'Supplier__c') {
            recordFields.put(
              'Root_Code__c_Supplier__c',
              suppliers.get((Id) rootCode.get(key)).get('Name')
            );
          } else {
            recordFields.put('Root_Code__c_' + key, rootCode.get(key));
          }
        } else {
          recordFields.put('Root_Code__c_' + key, '');
        }
      }

      for (String key : removalFields.split(',')) {
        if (pnRemoval != null && pnRemoval.get(key) != null) {
          recordFields.put('PN_Removal__c_' + key, pnRemoval.get(key));
        } else {
          recordFields.put('PN_Removal__c_' + key, '');
        }
      }

      for (String key : recordFields.keySet()) {
        recordFields.put(
          key,
          String.valueOf(recordFields.get(key)).toUpperCase()
        );
      }

      records.put(oos.Id, recordFields);
    }

    return records;
  }
}