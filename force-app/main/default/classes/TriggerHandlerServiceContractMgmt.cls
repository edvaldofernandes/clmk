/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Trigger Handler for Service Contract Management  Object       
* @comments: XXXXXX to cover test
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
public class TriggerHandlerServiceContractMgmt implements GEN_ITrigger {

    public List<Service_Contract_Management__c> newList;
    public List<Service_Contract_Management__c> oldList;
    public Map<Id, Service_Contract_Management__c> newMapRecords;
    public Map<Id, Service_Contract_Management__c> oldMapRecords;
   
    private static Boolean recursive = false;

    private TriggerActionServiceContractMgmt triggerAction;
    
    /**
    * @description : Constructor setting all Trigger attributes
    **/
    public TriggerHandlerServiceContractMgmt() {
        this.newList = (List<Service_Contract_Management__c>) Trigger.New;
        this.oldList = (List<Service_Contract_Management__c>) Trigger.Old;
        this.oldMapRecords = (Map<Id, Service_Contract_Management__c>) Trigger.OldMap;
        this.newMapRecords = (Map<Id, Service_Contract_Management__c>) Trigger.NewMap;

        triggerAction = new TriggerActionServiceContractMgmt(this);
    }
    
    /**
    * @description : Bulk before execute all before methods
    **/
    public void bulkBefore() { 
            switch on Trigger.operationType {
                
                when BEFORE_DELETE {
                }
                when BEFORE_INSERT {
                    triggerAction.updateContractLineItemMaster();
                    triggerAction.updateOppReason();

                } 
                when BEFORE_UPDATE {
                    triggerAction.updateContractLineItemMaster();
       
                }
                when else {
                    system.debug('TriggerHandlerAircraft.bulkBefore.Trigger.operationType: ' + Trigger.operationType);
                }
            }
    }
    
    /**
    * @description : Bulk after execute all after methods
    **/
    public void bulkAfter() {
        switch on Trigger.operationType {
            
            when AFTER_DELETE {
                new TriggerRollupServiceContractMgmt(oldList);

                triggerAction.deleteServiceContractManagement();
                triggerAction.recalculateItemsQuantityOnDelete();
                triggerAction.updateEntitlementConversion();
            }
            when AFTER_INSERT {
                new TriggerRollupServiceContractMgmt(newList);
                TriggerHandlerServiceContractMgmt.recursive = true;
                triggerAction.updateProductRecordTypeOnAfterInsert();
                triggerAction.recalculateItemsQuantityOnInsert();
                triggerAction.newServiceContractManagement(); 
                triggerAction.updateEntitlementConversion();
        
            } 
            when AFTER_UNDELETE {
                new TriggerRollupServiceContractMgmt(newList);

                triggerAction.recalculateItemsQuantityOnUnDelete();
            }
            when AFTER_UPDATE {
                if(!TriggerHandlerServiceContractMgmt.recursive){
                    new TriggerRollupServiceContractMgmt(newList);
                } 
                triggerAction.updateProductRecordTypeOnAfterUpdate();
                triggerAction.recalculateItemsQuantityOnUpdate();
                triggerAction.updateServiceContractManagement();
                triggerAction.newServiceContractManagement(); 
                triggerAction.updateEntitlementConversion();
                TriggerHandlerServiceContractMgmt.recursive = true;
            }
            when else {
                system.debug('TriggerHandlerServiceContractMgmt.bulkAfter.Trigger.operationType: ' + Trigger.operationType);
            }
        }
    }    
    
    public void andFinally() {
        system.debug('TriggerHandlerAircraft.andFinally');
        system.debug('TriggerHandlerAircraft.andFinally.newList: ' + newList);
    }
}