public class FO_NewFUPDigestController {
    public Contact contact {get; set;}
    /**
    * @description gets all recent fup updates and formats in a map for better
    * iterativity.  
    **/
    public Map<FUP__c, List<FUP__c>> getResponse(){
        Map<FUP__c, List<FUP__c>> response =
            new Map<FUP__c, List<FUP__c>>();
        
        if (contact != null){
            List<FUP__c> updates = FO_FupService.getNewsForDigestByContact(
                contact.Id
            );
            response = FO_FupService.buildFupNewMap(updates);
        }
		return response;
    }
    public Boolean getResponseIsEmpty(){
		return getResponse().isEmpty();
    }
}