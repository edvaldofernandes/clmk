/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Sep-2015.
**/

global class UpdateSCMProductRecordTypeBatch implements Database.Batchable<sObject>, Database.Stateful 
{
    
    global Database.Querylocator start( Database.BatchableContext BC )
    {
              
        string query = '';
        
        query = 'Select Id,Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name from Service_Contract_Management__c';  
        
        return Database.getQueryLocator( query );
    }
    
    
    global void execute( Database.BatchableContext BC, List<sObject> scope )
    {
        List<Service_Contract_Management__c> recordList = new List<Service_Contract_Management__c>();
    	Service_Contract_Management__c scm = new Service_Contract_Management__c();
    	
    	for(sObject objeto : scope )
    	{
    		scm = (Service_Contract_Management__c)objeto;
    		scm.productRecordType__c = scm.Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name;
        	recordList.add(scm);
    	}
    	
    	if(!recordList.isEmpty())
    		database.update(recordList,false);
    
        
    }
    
    global void finish( Database.BatchableContext bcMain )
    {
        
    }   

}