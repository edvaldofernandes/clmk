@isTest
private class SurveyManagerController_Test {
    @isTest(seeAllData=True)
    private static void doTest(){
        PageReference pageRef = Page.SurveyManagerPage;
        Test.setCurrentPage(pageRef);

        SurveyForce__c s = new SurveyForce__c();
        s.Name = 'test survey';
        insert s;
        ApexPages.StandardController sc = new ApexPages.StandardController(s);

        SurveyManagerController con = new SurveyManagerController(sc);
        System.assertEquals(con.surveyId, s.Id);

        Message msg = con.pageMessage;
        
        PageReference pf = con.save();
    }
}