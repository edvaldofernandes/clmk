/**
* @description: This class is the controller for the cancel button from an EOD object.
**/
public class FO_CancelEODController extends FO_ActionButton{
    public FO_CancelEODController(ApexPages.StandardController stdController) {
        super(stdController);
    } 
    /**
    * @description: Calls the service layer to do the job.
    **/
    public override void doAction(){
        FO_EODService.cancel(this.recordId);
    }
}