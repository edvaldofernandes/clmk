/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ServiceContractChatter
* NAME: ServiceContractChatterTest.cls
* AUTHOR: LRSA                                                DATE: 04/12/2014
*
*******************************************************************************/

@isTest(SeeAllData=true)
private class ServiceContractChatterTest {
    
    private static final integer LOTE = 2;
    
    private static final id RecTypeAcc = RecordTypeMemory.getRecType( 'Account', 'Operator' );
    private static final id RecTypeSC = RecordTypeMemory.getRecType( 'ServiceContract', 'Aircraft_Modification' );

    static testMethod void addParent() 
    {
        Account lAcc = SObjectInstanceTest.createAccount( RecTypeAcc );
      insert lAcc;
      
      ServiceContract lServC = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
      insert lServC;
      
      ServiceContract lServC2 = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
      lServC2.Contract_hierarchy__c = lServC.id;
      
      Test.StartTest();
      insert lServC2;
      Test.StopTest();
      
      list< FeedItem > lListFd = [ select id from FeedItem Limit 50];
      
      system.assert( !lListFd.isEmpty() ); 
    }
    
    static testMethod void updateParent() 
    {
      Account lAcc = SObjectInstanceTest.createAccount( RecTypeAcc );
      insert lAcc;
      
      ServiceContract lServC = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
      insert lServC;
      
      ServiceContract lServC2 = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
      insert lServC2;
      
      lServC2.Contract_hierarchy__c = lServC.id;
      
      Test.StartTest();
      update lServC2;
      Test.StopTest();
      
      list< FeedItem > lListFd = [ select id from FeedItem Limit 50];
      
      system.assert( !lListFd.isEmpty() ); 
    }
    
    static testMethod void loteParent() 
    {
        Account lAcc = SObjectInstanceTest.createAccount( RecTypeAcc );
      insert lAcc;
      
      ServiceContract lServC = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
      insert lServC;
      
      list< ServiceContract > lListSC = new list< ServiceContract >();
      
      for ( integer i=0; i<Lote; i++ )
      {
        ServiceContract lServC2 = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
        lServC2.Contract_hierarchy__c = lServC.id;
        lListSC.add( lServC2 );
      }
      
      Test.StartTest();
      insert lListSC;
      Test.StopTest();
      
      list< FeedItem > lListFd = [ select id from FeedItem Limit 50];
      
      system.assert( !lListFd.isEmpty() ); 
    }
}