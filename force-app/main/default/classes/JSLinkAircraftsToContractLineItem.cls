/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class which provides a service to relate aircraft to a ContractLineItem.
* The service relates all records of Aircraft__c which have the account of the 
* contract line item as Owner or Operator.
*
* NAME: JSLinkAircraftsToContractLineItem.cls
* AUTHOR: RLdO                                                 DATE: 19/05/2014
*******************************************************************************/
global with sharing class JSLinkAircraftsToContractLineItem 
{
  webservice static String processar(String pSvcContractLItemId)
  {
  	DMLExecution.deleteCMD([select Id from Aircraft_with_this_service__c
      where Contract_Line_Item__c = :pSvcContractLItemId]);

    list<ContractLineItem> lstContractLI = [select Id, ServiceContract.AccountId
      from ContractLineItem
      where Id = :pSvcContractLItemId]; 

    if (lstContractLI.isEmpty()) return 'Contract line item not found';

    Id lIdAccount = lstContractLI[0].ServiceContract.AccountId;
    list<Aircraft_with_this_service__c> lstRelAircraft = new list<Aircraft_with_this_service__c>();

    for (Aircraft__c aircraft: [SELECT Id FROM Aircraft__c
      where (Owner__c = :lIdAccount or Operator__c = :lIdAccount)
      and RecordType.DeveloperName = 'Embraer'
      ])
    {
    	Aircraft_with_this_service__c relAircraft = new Aircraft_with_this_service__c();
    	relAircraft.Aircraft__c = aircraft.Id;
    	relAircraft.Contract_Line_Item__c = (Id)pSvcContractLItemId;
    	lstRelAircraft.add(relAircraft);
    }
    
    return (DMLExecution.insertCMD(lstRelAircraft)) ? '' : 'Could not relate aircraft to this contract line item';
  }
}