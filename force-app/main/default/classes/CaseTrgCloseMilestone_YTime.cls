/*******************************************************************************
*                     Copyright (C) 2015 - Felipe
*-------------------------------------------------------------------------------
*******************************************************************************/

public with sharing class CaseTrgCloseMilestone_YTime {

    // Variaveis RecordType.

    @TestVisible private static final Id TYPE_INBOX = RecordTypeMemory.getRecType('Case', 'PRC');
    @TestVisible private static final Id TYPE_ECIP = RecordTypeMemory.getRecType('Case', 'ECIP_Sales');
    @TestVisible private static final Id TYPE_POOL = RecordTypeMemory.getRecType('Case', 'Pool_Exchanges');
    @TestVisible private static final Id TYPE_EPEP = RecordTypeMemory.getRecType('Case', 'EPEP_Loans_Rentals'); 
    @TestVisible private static final Id TYPE_WARRANTY = RecordTypeMemory.getRecType('Case', 'Warranty');         
    @TestVisible private static final Id TYPE_CUST = RecordTypeMemory.getRecType('Case', 'Customer_Issues');         
    @TestVisible private static final Id TYPE_DHL_MOVS = RecordTypeMemory.getRecType('Case', 'DHL_Fedex_movs'); 
    @TestVisible private static final Id TYPE_NOTIF_DISC = RecordTypeMemory.getRecType('Case', 'Notification_Discrepancy');
    @TestVisible private static final Id TYPE_QUOTE = RecordTypeMemory.getRecType('Case', 'Quote');
    @TestVisible private static final Id TYPE_REPAIR = RecordTypeMemory.getRecType('Case', 'Repair_Administ');    
    @TestVisible private static final Id TYPE_WARRANTY_REQ = RecordTypeMemory.getRecType('Case', 'Warrant_Request');               

    // Variaveis Reason
    @TestVisible private static final String ORDERS = 'Orders';
    @TestVisible private static final String EXTERNAL = 'External Request';
    @TestVisible private static final String INTERNAL = 'Internal Request';

    // Variaveis Phase.
    @TestVisible private static final String BACK_ORDER = 'Back Order';
    @TestVisible private static final String EXCEPTI = 'Exception';
    @TestVisible private static final String PROCESSING = 'Processing';
    @TestVisible private static final String TRACK_TRACE = 'Track & Trace';

    // Variaveis Status.
    @TestVisible private static final String INBOX='Inbox';
    @TestVisible private static final String DISPATCH='Dispatch';
    @TestVisible private static final String ANALYZE='Analyze';
    @TestVisible private static final String AWAITING_CREDIT='Awaiting Credit';
    @TestVisible private static final String AWAITING_CUSTOMER_SIGNATURE='Awaiting Customer Signature';
    @TestVisible private static final String AWAITING_FEDEX_DELIVERY='Awaiting Fedex Delivery';
    @TestVisible private static final String AWAITING_MANAGER_RELEASE='Awaiting Manager Release';
    @TestVisible private static final String AWAITING_SPARE_PARTS_PRICING='Awaiting Spare Parts Pricing';
    @TestVisible private static final String CLOSE='Closed';
    @TestVisible private static final String EHS_SHIPPING='EHS/Shiping';
    @TestVisible private static final String MOVEMENTS_ERROR='Movements Error';
    @TestVisible private static final String PART_AT_REPAIR='Part at Repair';
    @TestVisible private static final String PART_PENDING_SHIPMENT_TO_CUSTOMER='Part Pending Shipment to Customer';
    @TestVisible private static final String PENDING_ARRIVAL_AT_FLL='Pending Arrival at FLL';
    @TestVisible private static final String PENDING_CUSTOMER_INFO='Pending Customer Info';
    @TestVisible private static final String PENDING_INTERNAL_INFORMATION='Pending Internal Information';
    @TestVisible private static final String PENDING_PMG_ANALYSIS='Pending PMG Analysis';
    @TestVisible private static final String READY_FOR_ACK='Ready for ACK';
    @TestVisible private static final String AWA_CUSTOMER_RESPONSE='Awaiting Cust Response';
    @TestVisible private static final String AWA_POS_SUPPORT='Awaiting POS Support';
    @TestVisible private static final String PART_DOCK='Part on Dock';
    @TestVisible private static final String POS_DISP='POS Disposition';
    @TestVisible private static final String EHSSOTHER = 'EHS/Shipping Site Other';
    @TestVisible private static final String CRC = 'CRC Support';
    @TestVisible private static final String POS_SUPPORT = 'Pending POS support';
    @TestVisible private static final String EHS_DOCK = 'EHS/Dock';    
    @TestVisible private static final String PENDING_V_RMA = 'Pending Vendor RMA';

    
    //Variaveis Milestones
    
     private static List<String> MILE_USER = new List<String> {'001_YOUR PROCESS TIME - ECIP','002_YOUR PROCESS TIME - POOL','003_YOUR PROCESS TIME - WARRANTY','005_YOUR PROCESS TIME - BO ANALYZE',
     '007_YOUR PROCESS TIME - EPEP CONTRACT','008_YOUR PROCESS TIME - EPEP ORDER'};
    
     private static List<String> MILE_PROC = new List<String> {'001_YOUR PROCESS TIME - ECIP','002_YOUR PROCESS TIME - POOL','008_YOUR PROCESS TIME - EPEP ORDER','007_YOUR PROCESS TIME - EPEP CONTRACT',
     '003_YOUR PROCESS TIME - WARRANTY'};

    

    
     @TestVisible private static final String MILE1='001_YOUR PROCESS TIME - ECIP';
     @TestVisible private static final String MILE2='002_YOUR PROCESS TIME - POOL';
     @TestVisible private static final String MILE3='003_YOUR PROCESS TIME - WARRANTY';
     @TestVisible private static final String MILE4='004_TIME TO CLOSE NOTIF - WARRANTY';
     @TestVisible private static final String MILE5='005_YOUR PROCESS TIME - BO ANALYZE';
     @TestVisible private static final String MILE6='006_WARRANTY TAT';
     @TestVisible private static final String MILE7='007_YOUR PROCESS TIME - EPEP CONTRACT';
     @TestVisible private static final String MILE8='008_YOUR PROCESS TIME - EPEP ORDER';
     @TestVisible private static final String MILE9='009_CONTRACTED TAT';
     @TestVisible private static final String MILE10='010_AOG NFO ORDER';
     @TestVisible private static final String MILE11='011_AOG NFO ACK';
     @TestVisible private static final String MILE12='012_AOG NFO POS';
     @TestVisible private static final String MILE13='013_AOG NFO EXT REQUEST';
     @TestVisible private static final String MILE14='014_AOG NFO INT REQUEST';
     @TestVisible private static final String MILE15='015_AOG ORDER';
     @TestVisible private static final String MILE16='016_AOG ACK';
     @TestVisible private static final String MILE17='017_AOG POS';
     @TestVisible private static final String MILE18='018_AOG EXT REQUEST';
     @TestVisible private static final String MILE19='019_AOG INT REQUEST';
     @TestVisible private static final String MILE20='020_CRITICAL ORDER';
     @TestVisible private static final String MILE21='021_CRITICAL POS';
     @TestVisible private static final String MILE22='022_CRITICAL EXT REQUEST';
     @TestVisible private static final String MILE23='023_CRITICAL INT REQUEST';
     @TestVisible private static final String MILE24='024_ROUTINE ORDER';
     @TestVisible private static final String MILE25='025_ROUTINE POS';
     @TestVisible private static final String MILE26='026_ROUTINE EXT REQUEST';
     @TestVisible private static final String MILE27='027_ROUTINE INT REQUEST';
     @TestVisible private static final String MILE28='028_AOG NFO EHS SHIPPING';
     @TestVisible private static final String MILE29='029_AOG EHS SHIPPING';
     @TestVisible private static final String MILE30='030_CRITICAL EHS SHIPPING';
     @TestVisible private static final String MILE31='031_ROUTINE EHS SHIPPING';
     @TestVisible private static final String MILE32='032_AOG NFO EHS DOCK';
     @TestVisible private static final String MILE33='033_AOG EHS DOCK';
     @TestVisible private static final String MILE34='034_CRITICAL EHS DOCK';
     @TestVisible private static final String MILE35='035_ROUTINE EHS DOCK';
     @TestVisible private static final String MILE36='INBOX time';
     @TestVisible private static final String MILE37='037_AOG NFO Pending Request';
     @TestVisible private static final String MILE38='038_AOG NFO Part at Repair';
     @TestVisible private static final String MILE39='039_AOG Pending Request';
     @TestVisible private static final String MILE40='040_AOG Part at Repair';
     @TestVisible private static final String MILE41='041_CRI Pending Request';     
     @TestVisible private static final String MILE42='042_CRI Part at Repair';     
     @TestVisible private static final String MILE43='043_RTN Pending Request';     
     @TestVisible private static final String MILE44='044_RTN Part at Repair';
     @TestVisible private static final String MILE45='045_Warranty Request to PMG';     

    //Variaveis Priority
     @TestVisible private static final String AOGNFO='AOG NFO';
     @TestVisible private static final String AOG='AOG';
     @TestVisible private static final String CRI='Critical';
     @TestVisible private static final String RTN='ROUTINE';
     private static  integer i;
     
     
     
     private static Datetime closedDate = Datetime.now();

  public static void processar()
  {
    TriggerUtils.assertTrigger();

    // Declaração de variaveis.
    map < id, Case > lListCase = new map< id, Case >();
    map < Id, Case > mapOldCases = (map<Id, Case>)Trigger.oldMap;
    map < id, map < String, CaseMilestone > > mapMilestone = new map< id, map<String, CaseMilestone>>();
    map < String, CaseMilestone > mapMilestonesNames = new map< String, CaseMilestone >();
    list < CaseMilestone > lUpdateMileStone = new list <CaseMilestone>();
    Set< ID > lUpdateCase = new Set< ID >();

    // Pega os Casos alterados.
    for( Case updateCase : (List<Case>)Trigger.new ) {
      //if(((TriggerUtils.wasChanged(updateCase, Case.Status) || TriggerUtils.wasChanged(updateCase,Case.Phase__c) || TriggerUtils.wasChanged(updateCase,Case.OwnerId))))
      if(TriggerUtils.wasChanged(updateCase, Case.Status) || TriggerUtils.wasChanged(updateCase,Case.Phase__c) || TriggerUtils.wasChanged(updateCase,Case.OwnerId) || TriggerUtils.wasChanged(updateCase,Case.RecordtypeId))  
      {
        lListCase.put(updateCase.Id, updateCase);
      }
    }
    if( lListCase.isEmpty() ) return;

    //popula o mapMilestone map < id, map< string, CaseMilestone > >
    for( CaseMilestone cm : [select Id,completionDate, MilestoneType.Name, caseId, StartDate from CaseMilestone
        where caseID = :lListCase.keySet() order by StartDate asc] )
    {
      mapMilestonesNames = mapMilestone.get(cm.caseID);
      if(mapMilestonesNames == null) {
        mapMilestonesNames = new map< string, CaseMilestone >();
        mapMilestone.put(cm.caseID, mapMilestonesNames);
      }
      mapMilestonesNames.put(cm.MilestoneType.Name, cm);
    }

    // Define qual Milestone será fechado.
    for ( Case c : lListCase.values()) {
      CaseMilestone testMil,testmil2;
      map< String, CaseMilestone > lMapa = mapMilestone.get(c.id);
      System.debug('>>>> Lucas map ' + lMapa);
      //c.ACK_sent__c =  true;

      if ( lMapa == Null ) continue;

      lUpdateCase.add( c.id );
      
      
        // If from other user     
   
        if(TriggerUtils.wasChanged(c, Case.OwnerId)) {
        
            for(i=0;i<6;i++){  
          testMil = SetCompletionDate( MILE_USER[i], lMapa );
          if(testMil != NULL) {
          testmil2 = testMil;
           }
          }
          if(testMil2 != NULL){
          lUpdateMileStone.add(testMil2);
          }
          testMil2 = NULL;
        }
      
      // If from Phase BackOrder      
   
        if(TriggerUtils.wasChangedto(c, Case.Status, CLOSE) || TriggerUtils.wasChangedto(c, Case.Status, AWA_CUSTOMER_RESPONSE) || TriggerUtils.wasChangedto(c, Case.Status, AWA_POS_SUPPORT) || 
        TriggerUtils.wasChangedto(c, Case.Status, CRC) || TriggerUtils.wasChangedto(c, Case.Status, PART_AT_REPAIR) || TriggerUtils.wasChangedto(c, Case.Status, PART_DOCK)) {
        
        
          testMil = SetCompletionDate( MILE5, lMapa );
          if(testMil != NULL) {
          testmil2 = testMil;
           }
          
          if(testMil2 != NULL){
          lUpdateMileStone.add(testMil2);
          }
          testMil2 = NULL;
        }
        
        
    
     // IF from Phase Processing  
        if( (TriggerUtils.wasChangedto(c, Case.Phase__c, BACK_ORDER) && TriggerUtils.wasChangedto(c, Case.Status, ANALYZE)) || TriggerUtils.wasChangedto(c, Case.Status, AWAITING_CUSTOMER_SIGNATURE) ||
        TriggerUtils.wasChangedto(c, Case.Status, AWAITING_FEDEX_DELIVERY) || TriggerUtils.wasChangedto(c, Case.Status, AWAITING_MANAGER_RELEASE) || TriggerUtils.wasChangedto(c, Case.Status, CLOSE) ||
        TriggerUtils.wasChangedto(c, Case.Status, PENDING_ARRIVAL_AT_FLL) || TriggerUtils.wasChangedto(c, Case.Status, PENDING_V_RMA) ||
        (TriggerUtils.wasChangedto(c, Case.Phase__c, TRACK_TRACE) && (TriggerUtils.wasChangedto(c, Case.Status, EHS_SHIPPING)) || TriggerUtils.wasChangedto(c, Case.Status, CLOSE) || TriggerUtils.wasChangedto(c, Case.Status, EHSSOTHER))
        ) {
        
         for(i=0;i<5;i++){  
          testMil = SetCompletionDate( MILE_PROC[i], lMapa );
          if(testMil != NULL) {
          testmil2 = testMil;
           }
          }
          if(testMil2 != NULL){
          lUpdateMileStone.add(testMil2);
          }
          testMil2 = NULL;
        }   
      
 }

    if ( !lUpdateMileStone.isEmpty() ) update (lUpdateMileStone);
    if ( !lUpdateCase.isEmpty() ) CaseUpdateMilestone.processar( lUpdateCase );
  }

  private static CaseMilestone SetCompletionDate( String aMilestoneName, map< String, CaseMilestone > aCaseMilestone )
  {
    CaseMilestone testMil = aCaseMilestone.get( aMilestoneName );
    if(testMil == NULL) return null;
    if(testMil.CompletionDate != NULL) return null;
    testMil.CompletionDate = closedDate;
    return testMil;
  }

}