public class ESightFosInfoService {
    
    private List<ESightFosInfo> eSightFosInfoList;
    private List<ESightFos__c> eSightFosObjectList;
    private Map<String,String> accountIdMap;
    
    public ESightFosInfoService(List<ESightFosInfo> eSightFosInfoList){
        this.eSightFosInfoList = eSightFosInfoList;
        
        if(eSightFosObjectList == null)
            eSightFosObjectList = new List<ESightFos__c>();
		
		initCommonsServices();        
    }
    
    private void initCommonsServices(){
        
        accountIdMap  = eSightRepository.buildMapWithAllAccount();
    }
    
    public void parseToObject(){
        
        for(ESightFosInfo eSightFosInfo : eSightFosInfoList){
            
            ESightFos__c eSightFosObj = new ESightFos__c();
            eSightFosObj.aircraftsDelivered__c      = eSightFosInfo.aircraftsDelivered;
            eSightFosObj.aircraftsInService__c      = eSightFosInfo.aircraftsInService;
            eSightFosObj.averageFlightTime__c       = eSightFosInfo.averageFlightTime;
            eSightFosObj.dailyUtilizationCycles__c  = eSightFosInfo.dailyUtilizationCycles;
            eSightFosObj.dailyUtilizationHours__c   = eSightFosInfo.dailyUtilizationHours;
            eSightFosObj.family__c                  = eSightFosInfo.family;
            eSightFosObj.fleetLeaderCycles__c 		= eSightFosInfo.fleetLeaderCycles;
            eSightFosObj.fleetLeaderHours__c 		= eSightFosInfo.fleetLeaderHours;
            eSightFosObj.lastUpdate__c  			= eSightFosInfo.lastUpdate;
            eSightFosObj.leaderCycles__c 			= eSightFosInfo.leaderCycles;
            eSightFosObj.leaderHours__c 			= eSightFosInfo.leaderHours;
            eSightFosObj.operatorId__c 				= eSightFosInfo.operatorId;
            eSightFosObj.operatorName__c 			= eSightFosInfo.operatorName;
            eSightFosObj.referenceDate__c 			= eSightFosInfo.referenceDate;
            eSightFosObj.totalFlightCycles__c 		= eSightFosInfo.totalFlightCycles;
            eSightFosObj.totalFlightHours__c 		= eSightFosInfo.totalFlightHours;
            
            buildAccountId(eSightFosObj);
            
            eSightFosObjectList.add(eSightFosObj);
        }
        
        RepositoryTemplate.save(eSightFosObjectList);        
    }
    
    private void buildAccountId(ESightFos__c eSightFosObj){
        
        if(accountIdMap.get(String.valueOf(eSightFosObj.operatorId__c)) != null)
            eSightFosObj.Account__c  = accountIdMap.get(String.valueOf(eSightFosObj.operatorId__c));
    }
}