/**
* @author Marcilio Leite de Souza
* @date 20/08/2018
* @description: Trigger Exception
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           20 AGO 2018             Original Version
**/
public class GEN_TriggerException extends Exception {
    @TestVisible
    String testCover;
}