public with sharing class CLMEndAgreementController {
    
    public static final String EXPIRED = 'Expired';
    public static final String TERMINATED = 'Terminated';
    
    public CLMEndAgreementController(ApexPages.StandardController controller){
        /*
        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return;
        }
		*/
    }
    
    public static PageReference terminate(){
        
        String agreementId = ApexPages.currentPage().getParameters().get('id');
        
        if (agreementId==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return null;
        }
        
		String agreementList =  'SELECT ' + Utils.getAllFields('Agreement__c')  +
            ' FROM Agreement__c' +
            ' WHERE id = :agreementId';

        Agreement__c agreementSelected = database.query(agreementList);
        
        if(agreementSelected.Status__c == TERMINATED || agreementSelected.Status_Category__c == TERMINATED){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'You can only terminate once.'));
            return null;
        }

        if(agreementSelected.Status__c == EXPIRED || agreementSelected.Status_Category__c == EXPIRED){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'You can not terminate expired agreement.'));
            return null;
        }
        
        if(agreementSelected.Status__c=='Amended' || agreementSelected.Status_Category__c =='Superseded'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'You can only terminate the active Agreement, this one is Superseded.'));
            return null;
        }
        if(agreementSelected.Status_Category__c!='PA Signed' && agreementSelected.Status_Category__c!='Proposal Signed'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Agreement must be signed.'));
            return null;
        }
        
        List<Agreement__c> allAgreementsToTerminate = new List<Agreement__c>();
        
        allAgreementsToTerminate.add(agreementSelected);
        
        allAgreementsToTerminate.addAll(getAllRelatedAmendments(String.ValueOf(agreementSelected.Id)));

		Savepoint sp = Database.setSavepoint();

        try {
            CLMEndAgreement.terminateAgreement(allAgreementsToTerminate);
        } catch (Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessages(ex);
            return null;
        }
        
        PageReference returnPage = new PageReference('/'+ agreementId);  
        returnPage.setRedirect(true);
        return returnPage;
		
    }
    
    public static PageReference expire(){
        
        String agreementId = ApexPages.currentPage().getParameters().get('id');
        
        if (agreementId==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return null;
        }

		String agreementList =  'SELECT ' + Utils.getAllFields('Agreement__c')  +
            ' FROM Agreement__c' +
            ' WHERE id = :agreementId';

        Agreement__c agreementSelected = database.query(agreementList);
        
        if(agreementSelected.Status__c == EXPIRED || agreementSelected.Status_Category__c == EXPIRED){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Agreements can not expire twice.'));
            return null;
        }

         if(agreementSelected.Status__c == TERMINATED || agreementSelected.Status_Category__c == TERMINATED){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Agreements can not expire with status terminated.'));
            return null;
        }
        
        if(agreementSelected.Status__c=='Activated' || agreementSelected.Status_Category__c=='PA Signed' || agreementSelected.Status_Category__c=='Proposal Signed'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Agreement must not be Signed'));
            return null;
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            CLMEndAgreement.expiredAgreement(agreementSelected);
        } catch (Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessages(ex);
            return null;
        }
        

        PageReference returnPage = new PageReference('/'+ agreementId);  
        returnPage.setRedirect(true);
        return returnPage;
		
    }
    
    private static List<Agreement__c> getAllRelatedAmendments(String agreementId){
        String agreementList =  'SELECT ' + Utils.getAllFields('Agreement__c')  +
            ' FROM Agreement__c' +
            ' WHERE Child_Agreement__c = :agreementId';
        List<Agreement__c> agreementSelected = database.query(agreementList);
        return agreementSelected;
    }
}