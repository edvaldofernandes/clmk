global class RCP_fhfc {  
    
    webservice Integer numeroSerieAeronave;
    webservice Integer codigoOperador;
    webservice String  codigoProjeto;
    webservice Date    dataReferencia;
    webservice Double  horaVooMes;
    webservice Integer ciclosVooAcumulados;
    webservice Integer ciclosDeVoo;
    webservice Integer diasOperacionais;
    webservice Double  horasVooAcumuladas;
    webservice String  numeroRegistroAeronave;
        
    public RCP_fhfc(){}

}