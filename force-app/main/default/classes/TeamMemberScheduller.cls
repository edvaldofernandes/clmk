global class TeamMemberScheduller  implements Schedulable, Database.AllowsCallouts {
    
    global void execute(SchedulableContext SC) {

        RcpOutBoundProxy.executeTeamMemberUpdated();
        RcpOutBoundProxy.executeTeamMemberInserted();
    }    
}