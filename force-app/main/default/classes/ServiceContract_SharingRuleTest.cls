/*******************************************************************************
*                               Embraer - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ServiceContract_SharingRule
* NAME: ServiceContract_SharingRuleTest.cls
* AUTHOR: Bruno Severino                                       DATE: 28/04/2015
*
*******************************************************************************/
@isTest
public class ServiceContract_SharingRuleTest {
    /***
     * all SC created by SJK ACC MANAGER with MRO Srvs.. will share with SJK_ACC_MANAGER / SJK_BDM_MRO_SVCS / [*SITE*]_BDM / [*SITE*]_ACC_MANAGER 
     * */            
   /***
     * all opp created by SJK ACC MANAGER with MRO Srvs.. will share with SJK_ACC_MANAGER / SJK_BDM_MRO_SVCS / [*SITE*]_BDM / [*SITE*]_ACC_MANAGER 
     * */      
     
    static Map<String,Id> embraerSitesMap = new  Map<String,Id>();     
    static testMethod void Test1(){     
    	if(embraerSitesMap.isEmpty())
        	createEmbraerSites();
               
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEi' And isActive  = true Limit 1]; //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEi'; //SJK_ACC_MANAGER
        //insert(user1);
        
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Maintenance Services').getRecordTypeId();
            Test.startTest();
            insert(sc);
            Test.stopTest();
                    
            //verify if the shares where created
            boolean hasBDM = false, hasACCM = false, hasSJKACCM = false, hasSJKBDM = false;
            List<ServiceContractShare> listUserOrGroupId = [SELECT UserOrGroupId FROM ServiceContractShare WHERE ParentId = :sc.Id AND RowCause = 'Manual'];
            Set<Id> idGroups = new Set<Id>();
            
            for( ServiceContractShare scShare : listUserOrGroupID)
                idgroups.add(scShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( ServiceContractShare scShare : listUserOrGroupID){
                string groupD = mapGroup.get(scShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('LATAM_ACC_MANAGER')) hasACCM = true;
                if(groupD.equals('LATAM_BDM')) hasBDM = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_MRO_SVCS')) hasSJKBDM = true;
            }        
            //system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_MRO_SVCS, Deveria estar inserido no OpportunityShare');                   
            //system.assert(hasBDM, 'O Grupo: LATAM_BDM, Deveria estar inserido no OpportunityShare');
            //system.assert(hasACCM, 'O Grupo: LATAM_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
        }
    } 
    
    /***
     * all opp created by ERCA_ACC MANAGER with Tech Srvs.. will share with SJK_BDM_TECH_SVCS / SJK_ACC_MANAGER / SJK_FLIGHT_OPS_LIASION / SJK_SS_CONTRACT_ADMIN 
     * */  
    static testMethod void Test2(){
    	//if(embraerSitesMap.isEmpty())
        //	createEmbraerSites();
        
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEHEA0'; //ERCA_ACC
        insert(user1);
        //User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEHEA0' and isActive = true]; //System Administrator
            
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'ERCA_ACC_MANAGER';
            //sc.AccountId = embraerSitesMap.get('teste');
            
            Test.startTest();
            insert(sc);
            Test.stopTest();
            
            //verify if the shares where created
            boolean hasFLOP = false, hasSSCA = false, hasSJKACCM = false, hasSJKBDM = false;
            List<ServiceContractShare> listUserOrGroupId = [SELECT UserOrGroupId FROM ServiceContractShare WHERE ParentId = :sc.Id AND RowCause = 'Manual'];
            Set<Id> idGroups = new Set<Id>();
            
            for( ServiceContractShare scShare : listUserOrGroupID)
                idgroups.add(scShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( ServiceContractShare scShare : listUserOrGroupID){
                string groupD = mapGroup.get(scShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('SJK_FLIGHT_OPS_LIASION')) hasFLOP = true;
                if(groupD.equals('SJK_SS_CONTRACT_ADMIN')) hasSSCA = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_TECH_SVCS')) hasSJKBDM = true;
            }
            //system.assert(hasFLOP, 'O Grupo: SJK_FLIGHT_OPS_LIASION, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSSCA, 'O Grupo: SJK_SS_CONTRACT_ADMIN, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_TECH_SVCS, Deveria estar inserido no OpportunityShare');        
        }
    }
    
    /***
     * all opp created by MEA_BDM with Tech Srvs.. will share with SJK_BDM_TECH_SVCS / SJK_ACC_MANAGER / MEA_ACC_MANAGER 
     * */  
    static testMethod void Test3(){    
    	if(embraerSitesMap.isEmpty())
        	createEmbraerSites();    
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvESEA0'; // MEA_BDM
        //insert(user1);
        
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And isActive  = true Limit 1];//And UserRoleId = '00Ei0000000GvESEA0']; //System Administrator
            
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'CHI_BDM';
            
            Test.StartTest();
            insert(sc);
            
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services';
            update(sc);
            sc.Owner_Role__c = 'ERCA_BDM';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            sc.Service_Program_Type__c = 'Technical Services';
            update(sc);
            
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services';
            update(sc);
            sc.Owner_Role__c = 'NA_BDM';            
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            sc.Service_Program_Type__c = 'Technical Services';
            update(sc);
            Test.StopTest();
            
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services';
            update(sc);
            sc.Owner_Role__c = 'AP_BDM';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            sc.Service_Program_Type__c = 'Technical Services';
            update(sc);
            
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services';
            update(sc);
            sc.Owner_Role__c = 'LATAM_BDM';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            sc.Service_Program_Type__c = 'Technical Services';
            update(sc);
            
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services';
            update(sc);
            sc.Owner_Role__c = 'MEA_BDM';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            sc.Service_Program_Type__c = 'Technical Services';
            update(sc);
            
            
            //verify if the shares where created
            boolean hasACCM = false, hasSJKACCM = false, hasSJKBDM = false;
            
            List<ServiceContractShare> listUserOrGroupId = [SELECT UserOrGroupId FROM ServiceContractShare WHERE ParentId = :sc.Id AND RowCause = 'Manual' LIMIT 30];
            Set<Id> idGroups = new Set<Id>();
            
            for( ServiceContractShare scShare : listUserOrGroupID)
                idgroups.add(scShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( ServiceContractShare scShare : listUserOrGroupID){
                string groupD = mapGroup.get(scShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('MEA_ACC_MANAGER')) hasACCM = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_TECH_SVCS')) hasSJKBDM = true;
            }
            
            //system.assert(hasACCM, 'O Grupo: MEA_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_TECH_SVCS, Deveria estar inserido no OpportunityShare');  
                
        }
    }
    
    /***
     * all opp created by SJK ACC MANAGER with MAterial Srvs.. will share with SJK_ACC_MANAGER / SJK_BDM_MAT_SVCS / SJK_FLIGHT_OPS_LIASION / SJK_SS_CONTRACT_ADMIN 
     * */   
    static testMethod void Test4(){              
        //Creating Users to opportunity Owner
        if(embraerSitesMap.isEmpty())
        	createEmbraerSites();
        
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        //insert(user1);
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEiEAK' And isActive  = true Limit 1]; //System Administrator
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services'; 
            sc.AccountId = embraerSitesMap.get('Europe and Central Asia');
            
            Test.startTest();
            insert(sc);
            
            sc.AccountId = embraerSitesMap.get('China');
            update(sc);
            
            sc.AccountId = embraerSitesMap.get('North America');
            update(sc);
            
            sc.AccountId = embraerSitesMap.get('Middle East & Africa');
            update(sc);
            
            sc.AccountId = embraerSitesMap.get('Asia Pacific');
            update(sc);
            
            sc.AccountId = embraerSitesMap.get('Latin America');
            update(sc);
            
            
            Test.stopTest();
            
            //verify if the shares where created
            boolean hasBDM = false, hasACCM = false, hasSJKACCM = false, hasSJKBDM = false;
            List<ServiceContractShare> listUserOrGroupId = [SELECT UserOrGroupId FROM ServiceContractShare WHERE ParentId = :sc.Id AND RowCause = 'Manual'];
            Set<Id> idGroups = new Set<Id>();
            
            for( ServiceContractShare scShare : listUserOrGroupID)
                idgroups.add(scShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( ServiceContractShare scShare : listUserOrGroupID){
                string groupD = mapGroup.get(scShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('LATAM_ACC_MANAGER')) hasACCM = true;
                if(groupD.equals('LATAM_BDM')) hasBDM = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_MAT_SVCS')) hasSJKBDM = true;
            }
            //system.assert(hasACCM, 'O Grupo: LATAM_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasBDM, 'O Grupo: LATAM_BDM, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_TECH_SVCS, Deveria estar inserido no OpportunityShare');           
        }  
    } 
    
     static testMethod void Test5()
     {           
     	if(embraerSitesMap.isEmpty())
        	createEmbraerSites();   
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        //insert(user1);
        
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEiEAK' And isActive  = true Limit 1]; //System Administrator
        
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services'; 
            sc.AccountId = embraerSitesMap.get('China');
            
            Test.startTest();
            insert(sc);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test6()
     {         
     	if(embraerSitesMap.isEmpty())
        	createEmbraerSites();     
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        //insert(user1);
        
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEiEAK' And isActive  = true Limit 1]; //System Administrator
        
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services'; 
            sc.AccountId = embraerSitesMap.get('North America');
            
            
            Test.startTest();
            insert(sc);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test7()
     {              
     	 if(embraerSitesMap.isEmpty())
        	createEmbraerSites();
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        //insert(user1);
        
       
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEiEAK' And isActive  = true Limit 1]; //System Administrator
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services'; 
            sc.AccountId = embraerSitesMap.get('Middle East & Africa');
            
            Test.startTest();
            insert(sc);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test8()
     {           
     	if(embraerSitesMap.isEmpty())
        	createEmbraerSites();   
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        //insert(user1);
        
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEiEAK' And isActive  = true Limit 1]; //System Administrator
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            sc.Service_Program_Type__c = 'Material Services'; 
            sc.AccountId = embraerSitesMap.get('Asia Pacific');
            
            
            Test.startTest();
            insert(sc);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test9()
     {   
     	if(embraerSitesMap.isEmpty())
        	createEmbraerSites();           
        //Creating Users to opportunity Owner
        //User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        //user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        //insert(user1);
        
        
        User user1 = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And UserRoleId = '00Ei0000000GvEiEAK' And isActive  = true Limit 1]; //System Administrator
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            ServiceContract sc = createServiceContract();
            sc.Owner_Role__c = 'SJK_ACC_MANAGER';
            sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            sc.AccountId = embraerSitesMap.get('Latin America');
            
            
            Test.startTest();
            insert(sc);
            Test.stopTest();
        }
      
     }      
             
    static void createEmbraerSites()
    {
    	List<Account> listAccounts = new List<Account>();
    	
    	List<Account> listAccountsAirlines = new List<Account>();
    	
    	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Embraer Europe and Central Asia',Company_Nickname__c = 'Embraer Europe and Central Asia',ICAO_Code__c = '1',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId()));
       	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Embraer China',Company_Nickname__c = 'Embraer China',ICAO_Code__c = '2',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId()));
		listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Embraer North America',Company_Nickname__c = 'Embraer North America',ICAO_Code__c = '3',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId()));	    	
		listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Embraer Middle East & Africa',Company_Nickname__c = 'Embraer Middle East & Africa',ICAO_Code__c = '4',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId()));
    	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Embraer Asia Pacific',Company_Nickname__c = 'Embraer Asia Pacific',ICAO_Code__c = '5',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId()));
    	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Embraer Latin America',Company_Nickname__c = 'Embraer Latin America',ICAO_Code__c = '6',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId()));
    	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Europe and Central Asia',Company_Nickname__c = 'Europe and Central Asia',ICAO_Code__c = '7',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()));
       	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'China',Company_Nickname__c = 'China',ICAO_Code__c = '8',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()));
		listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'North America',Company_Nickname__c = 'North America',ICAO_Code__c = '9',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()));	    	
		listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Middle East & Africa',Company_Nickname__c = 'Middle East & Africa',ICAO_Code__c = '10',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()));
    	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Asia Pacific',Company_Nickname__c = 'Asia Pacific',ICAO_Code__c = '11',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()));
    	listAccounts.add(new Account(BillingCountry = 'Brazil',Name = 'Latin America',Company_Nickname__c = 'Latin America',ICAO_Code__c = '12',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId()));
       	insert listAccounts;
       	
    	for(integer i=11;i>=6;i--)
    	{
    		Account acc = listAccounts.get(i); 
    		acc.Embraer_Site__c = listAccounts.get(i-6).Id;
    		listAccountsAirlines.add(acc);	
    	}
    	update listAccountsAirlines;
		
		for(account acc : listAccountsAirlines)
			embraerSitesMap.put(acc.Name,acc.Id);
    	
    }
    
    static ServiceContract createServiceContract(){
        ServiceContract sc = new ServiceContract();
        //sc.Service_Program_Type__c = 'Technical Services';
        sc.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();   
        sc.Contract_Type__c = 'PSB';
        sc.Fleet_Type__c = 'EJET';
        sc.Name = 'Teste - ServiceContract Sharing Rule';
        sc.Contract_Status__c = 'Draft';
        //sc.AccountId = '001i000000uhklIAAQ';
        return sc;
    }
}