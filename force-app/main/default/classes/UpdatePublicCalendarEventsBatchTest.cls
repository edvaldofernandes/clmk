/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Sep-2015.
** o SeeAllData nesse caso é indispensável pois não há ainda como inserir calendário público via apex
**/
@isTest(seeAllData=true)
private class UpdatePublicCalendarEventsBatchTest
{

    static testmethod void test() 
    {
       
       Test.startTest();
       UpdatePublicCalendarEventsBatch updtPCB = new UpdatePublicCalendarEventsBatch();
       Database.executeBatch(updtPCB);
       Test.stopTest();

    }
}