/**
 * 
 **/
public without sharing class EscalationTriggerHandler 
{
    public Map<Id,Escalation__c> newRecordsMap = new Map<Id,Escalation__c>();
    public Map<Id,Escalation__c> oldRecordsMap = new Map<Id,Escalation__c>(); 
    public List<Escalation__c> newRecords = new List<Escalation__c>();
    public List<Escalation__c> oldRecords = new List<Escalation__c>();
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public EscalationTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

    public void OnBeforeInsert(){}

    public void OnAfterInsert()
    {
        //updateRelatedAircrafts();
        CLMSetAircraftEscalation.newEscalation(newRecords);
    }

    public void OnBeforeUpdate(){}

    public void OnAfterUpdate()
    {
        //updateRelatedAircrafts();
        CLMSetAircraftEscalation.updatedEscalation(oldRecordsMap, newRecords);
    }

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}

    public boolean IsTriggerContext
    {
        get{return isExecuting;}
    }

    /*
    private void updateRelatedAircrafts()
    {
         
        Map<id,Map<string,id>> mapAgreementEscalationMap = new Map<id,Map<string,id>>();
        Map<string,id> mapDateReferenceEscalationId = new Map<string,id>();
        Set<string> monthYearSet = new Set<string>();
        list<Agreement_Aircraft__c> listLineItemsToUpdate = new list<Agreement_Aircraft__c>();
        boolean eligibleRecord = false;


        
        for(Escalation__c escalation : this.newRecords)
        {            
            
            if(escalation.Rate_Effective_Date__c != null)
            {
                eligibleRecord = this.isInsert || (this.isUpdate && escalation.Rate_Effective_Date__c != this.oldRecordsMap.get(escalation.Id).Rate_Effective_Date__c);
                
                if(eligibleRecord)
                {
                    mapDateReferenceEscalationId = new Map<string,id>();
            
                    if(mapAgreementEscalationMap.containsKey(escalation.Commercial_Agreement__c))
                        mapDateReferenceEscalationId = mapAgreementEscalationMap.get(escalation.Commercial_Agreement__c);    
            
                    mapDateReferenceEscalationId.put(escalation.RateMonthYearReference__c,escalation.Id);
            
                    monthYearSet.add(escalation.RateMonthYearReference__c);
            
                    mapAgreementEscalationMap.put(escalation.Commercial_Agreement__c,mapDateReferenceEscalationId);
                }
            }
        }        
        
        for(Agreement_Aircraft__c contractItem : [SELECT Aircraft_ID__c, Aircraft_Name__c, Agreement__c,Contractual_Delivery_Month__c FROM Agreement_Aircraft__c WHERE Contractual_Delivery_Month__c != NULL AND Agreement__c IN :mapAgreementEscalationMap.keySet()])
        {
            if(monthYearSet.contains(contractItem.Contractual_Delivery_Month__c.month() + '/' + contractItem.Contractual_Delivery_Month__c.year())){
                //contractItem.Escalation__c = mapAgreementEscalationMap.get(contractItem.Agreement__c).get(contractItem.DeliveryMonthYearReference__c); 
                contractItem.Escalation__c = mapAgreementEscalationMap.get(contractItem.Agreement__c).get(contractItem.Contractual_Delivery_Month__c.month()+'/'+contractItem.Contractual_Delivery_Month__c.year()); 
                listLineItemsToUpdate.add(contractItem);
            }
        }
        
        if(!listLineItemsToUpdate.isEmpty())
            update listLineItemsToUpdate;        
        
    }*/
}