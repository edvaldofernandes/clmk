/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Method checkUpdateStatus() - when the field StageName of an opportunity is 
* updated to 'Proposal/Price Quote', this method checks if there is at least
* one contact related to the opportunity. If there is no contact related, the
* class will return an error message.
* Method checkUpdateAccount() -when the account of an opportunity is updated 
* this method will return an error message if there is a contact role related to 
* the opportunity.
*
* NAME: OpportunityCheckContactRole.cls
* AUTHOR: DPF                                                DATE: 16/12/2014
*
*******************************************************************************/
public with sharing class OpportunityCheckContactRole {

  public static void checkUpdateStatus()
  {
    TriggerUtils.assertTrigger();

    map<Id, Opportunity> mapOpp = new map<Id, Opportunity>();
    for ( Opportunity opp : (list<Opportunity>) trigger.new )
    {
      if ( TriggerUtils.wasChangedTo(opp, Opportunity.StageName, 'Proposal/Price Quote'))
      {
        mapOpp.put(opp.Id, opp);
      }
    }
    if ( mapOpp.isEmpty() ) return;

    for ( OpportunityContactRole ocr : [SELECT OpportunityId FROM OpportunityContactRole
      WHERE OpportunityId =: mapOpp.keyset() ])
    {
      mapOpp.remove(ocr.OpportunityId);
    }
    if ( mapOpp.isEmpty() ) return;

    for ( Opportunity opp : mapOpp.values() )
    {
      opp.addError('There must be at least one contact related to the opportunity');
    }
  }

  public static void checkUpdateAccount()
  {
  	TriggerUtils.assertTrigger();

    map<Id, Opportunity> mapOpp = new map<Id, Opportunity>();
    for ( Opportunity opp : (list<Opportunity>) trigger.new )
    {
      if ( TriggerUtils.wasChanged(opp, Opportunity.AccountId))
      {
        mapOpp.put(opp.Id, opp);
      }
    }
    if ( mapOpp.isEmpty() ) return;
    
    for ( OpportunityContactRole ocr : [SELECT OpportunityId FROM OpportunityContactRole
      WHERE OpportunityId =: mapOpp.keyset() ])
    {
    	Opportunity opp = mapOpp.get(ocr.OpportunityId);
    	if ( opp == null ) continue;
    	opp.addError(' It is not possible to change the account when the opportunity has contacts.');
    }
    
  }
}