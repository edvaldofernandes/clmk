/**
* Controller class for the visualforce page CFCRequestProposalFinish.
* Name: CFCRequestProposalFinishController
* @version 1.0 - 14/02/2015
**/

public without sharing class CFCRequestProposalFinishController {
    
    public Contact contactCFC {get; set;}
    public Account accountCFC {get; set;}
    public User userCFC {get; set;}
    
    public Proposal__c proposalCFC {get; set;}
    
    public String NameOpportunity {get; set;}
    public String emailOwner {get; set;}
    public String phoneOwner {get; set;}
    
    public CFCRequestProposalFinishController(){  
        
        NameOpportunity = 'no information';
        emailOwner = 'no information';
        phoneOwner = 'no information';
        
        userCFC = UserDAO.getInstance().getUserById(UserInfo.getUserId());
        system.debug('CFCRequestProposalFinishController.userCFC >>> ' + userCFC);
        
        if(userCFC.Contact.Account.Cam__c != null){
            User userOwnerId = UserDao.getInstance().getUserById(userCFC.Contact.Account.Cam__c);
            if(userOwnerId != null){
                emailOwner = userOwnerId.E_mail__c;
                phoneOwner = userOwnerId.Phone;
            }
        } else {
            CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults();            
            system.debug('CFCRequestProposalController.GenerateOwnerId.cs >>> ' + cs);
            User userOwnerId = UserDao.getInstance().getByUserName(cs.User_Name__c);
            if(userOwnerId!=null){
                emailOwner=userOwnerId.E_mail__c;
                phoneOwner=userOwnerId.Phone;
            }
        }       
    }

}