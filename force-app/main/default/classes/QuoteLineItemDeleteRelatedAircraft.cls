/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for deleting related aircraft when a quote line item
* is deleted.
*
* NAME: QuoteLineItemDeleteRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*******************************************************************************/

public with sharing class QuoteLineItemDeleteRelatedAircraft {

  public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    List<Id> lstProdutoOppId = new List<Id>();
    for ( QuoteLineItem oli: (List<QuoteLineItem>) trigger.old)
    {
      lstProdutoOppId.add(oli.Id);
    }
    if ( lstProdutoOppId.isEmpty() ) return;
    
    List<Related_Aircraft__c> lstRelAircrafts = [ SELECT Id FROM Related_Aircraft__c 
      WHERE Quote_Line_Item__c =: lstProdutoOppId ];
      
    if ( !lstRelAircrafts.isEmpty() ) delete lstRelAircrafts;
  }
  
}