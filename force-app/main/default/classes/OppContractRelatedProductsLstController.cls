public with sharing class OppContractRelatedProductsLstController 
{
    public List<OpportunityLineItem> opportunitiesItemsList{get;set;}
    public List<ContractLineItem> contractsItemsList{get;set;}
    public Id productId;
    private String sortDirectionOpp = 'ASC';
    private String sortExpOpp = 'OpportunityId';
    private String sortDirection = 'ASC';
    private String sortExp = 'ServiceContractId';
    public String section{get;set;}
    public String sortExpressionOpp{get{return sortExpOpp;}set{if(value == sortExpOpp)sortDirectionOpp = (sortDirectionOpp == 'ASC')? 'DESC' : 'ASC';else sortDirectionOpp = 'ASC';sortExpOpp = value;}}
    public String sortExpression{get{return sortExp;}set{if(value == sortExp)sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';else sortDirection = 'ASC';sortExp = value;}}
    public boolean haveOpportunities{get;set;}
    public boolean haveContracts{get;set;}
    
    
    public OppContractRelatedProductsLstController(ApexPages.StandardController stdController) 
    {
        opportunitiesItemsList = new List<OpportunityLineItem>();
        contractsItemsList = new List<ContractLineItem>();
        productId = stdController.getId();
        haveOpportunities = false;
        haveContracts=false;
                
        loadRecords();  
    
    }
    
    public String getSortDirection()
    {
        string value = sortDirection;
        
        if (String.IsEmpty(sortExpression))
            value = 'ASC';
            
        return value;
    }
        
    
    public void setSortDirection(String pValue)
    {
        sortDirection = pValue;    
    }
    
    public String getSortDirectionOpp()
    {
        string value = sortDirectionOpp;
        
        if (String.IsEmpty(sortExpressionOpp))
            value = 'ASC';
            
        return value;
    }
        
    
    public void setSortDirectionOpp(String pValue)
    {
        sortDirectionOpp = pValue;    
    }
    
    public PageReference viewData()
    {
        loadRecords();
        return null;
    }
    
    private void loadRecords()
    {
        string sortFullExpOpp = sortExpressionOpp  + ' ' + sortDirectionOpp;
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        Set<Id> setIdProduct = new Set<Id>();
        String filter = 'NREC';
        
        setIdProduct.add(productId);
        
        if(String.isEmpty(section) || section == 'Opportunity')
            opportunitiesItemsList = Database.query('Select OpportunityId, Quantity, OpportunityRecordType__c,AccountName__c,Stage_of_opportunity__c From OpportunityLineItem Where PricebookEntry.Product2Id in  : setIdProduct Order By ' + sortFullExpOpp);
        
        if(String.isEmpty(section) || section == 'Contract')
            contractsItemsList = Database.query('Select ServiceContractId, Quantity, Status,Account_ID__c,Account_name__c,ContractRecordType__c From ContractLineItem Where PricebookEntry.Product2Id in  : setIdProduct Order By ' + sortFullExp );
    
        haveOpportunities = opportunitiesItemsList.size()>0;
        haveContracts = contractsItemsList.size()>0;
    
    }
    
        
}