/**
* @author Marcilio Leite de Souza
* @date 03/07/2018
* @description: TEST CLASS FOR FOR_ControllerRequestFlightOperation
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           03 JUL 2018             Original Version
**/
@isTest
private class FOR_ControllerRequFlightOper_Test {

    @isTest
    static void it_validate_controller_constructor() {

        FOR_ControllerRequestFlightOperation controller = new FOR_ControllerRequestFlightOperation();
        
        controller.objLeg = new FOR_Flight_Leg__c();
        
        Account acc = create_account_test_data();    
        
        controller.objCase = new Case();
        controller.objCase.FOR_Activity__c = 'Line Captain';
        controller.objCase.SuppliedEmail = 'teste@test.com';
        controller.objCase.FOR_Requester_Customer__c = 'test';
  
        controller.strStartTime = '10:10';
        controller.strStartDate = '10/10/2019';
        controller.strEndDate = '10/10/2020';
        controller.objFile.body = 'data:image/gif;base64,R0lGODlhQABAAPf/AA2OwQy';
        controller.objFile.typeFile = 'teste';
        controller.objFile.name = 'teste';
        controller.objFile.description = 'teste';
        
        controller.saveCase();

    }
    
     @isTest
    static void it_validate_controller_constructor_exception() {

        FOR_ControllerRequestFlightOperation controller = new FOR_ControllerRequestFlightOperation();
        
        controller.objLeg = new FOR_Flight_Leg__c();
        
        Account acc = create_account_test_data();        
        controller.objCase.FOR_Activity__c = 'Line Captain';
        controller.objCase.SuppliedEmail = 'teste@test.com';
        controller.objCase.FOR_Requester_Customer__c = 'test';
 
        controller.saveCase();
    }
    
    @isTest
    static void it_select_options() {
        
        FOR_ControllerRequestFlightOperation controller = new FOR_ControllerRequestFlightOperation();
        
        controller.getCase();
        controller.getCaseActivities();
        controller.getCaseAircraftTypes();
        controller.getCasePriorities();
        controller.getCaseServiceRequest();
    }
   
    static Account create_account_test_data(){
        Account a = new Account();
        a.BillingCountry = 'Brazil';
        a.Name = 'Test';
        a.Company_Nickname__c = 'testNickname';
        a.FlyEmbraerId__c = '99999';
        insert a;
        return a;
    }
}