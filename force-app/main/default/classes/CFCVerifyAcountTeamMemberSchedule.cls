/**
* Controller class for verify the account team member equal CAM
* Name: CFCVerifiyAcountTeamMember
* @author - gjesus@deloitte.com
* @version 1.0 - 03/02/2016
**/ 
global class CFCVerifyAcountTeamMemberSchedule implements Schedulable{
    
    public static String sched = '0 0 19 * * ?';  //Every Day at 19 
    
    global static String scheduleMe() {
        CFCVerifyAcountTeamMemberSchedule SC = new CFCVerifyAcountTeamMemberSchedule(); 
        return System.schedule('My batch Job', sched, SC);
    }
    
    global void execute(SchedulableContext SC){
        system.debug('Start the execution of the CFCVerifiyAcountTeamMemberSchedule job');        
        
        CFCVerifyAcountTeamMemberBatch b1 = new CFCVerifyAcountTeamMemberBatch();
        Database.executeBatch(b1,50);
    }
}

/*
 * CFCVerifyAcountTeamMemberSchedule sb = new CFCVerifyAcountTeamMemberSchedule();
 * String t = '0 0 19 * * ?';
 * system.schedule('VerifyAccount', t, sb);
 * 
*/