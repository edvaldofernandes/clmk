public with sharing class CaseEmailMerge {
 //new cases default to record type repair quote and FLLPRC DEFAULT CASE OWNER ID.
    static final Id RTYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair Quote').getRecordTypeId();
    
    public Static Boolean TRACE = true;

protected Case theCase = null;
protected Messaging.InboundEmail inboundEmail;
protected String defaultCaseOwnerId;


    
public CaseEmailMerge() {
    //pool , EPEP, REPAIR ADMIN, REPAIR QUOTE,WARRANTY, STOCK REPAIR
case[] queues = [select Id, Notification__c ,QM__c,SAP_PO__c from case where (Notification__c != '' OR QM__c !='') and (RecordTypeId= '012i0000000xxbSAAQ' or RecordTypeId='012i0000000xxbOAAQ' or RecordTypeId='012i0000000xxbUAAQ' or RecordTypeId='0120H000001YdYhQAK' or RecordTypeId='012i0000000xxbWAAQ' or RecordTypeId='0120H000001Yda4QAC') and CreatedDate = LAST_N_DAYS:180];
this.defaultCaseOwnerId = '005i0000006F6NzAAK'; //FLLPRC DEFAULT 
}

public Messaging.InboundEmailResult processInboundEmail(Messaging.InboundEmail email)
{
 Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
 result.success = true;
 this.inboundEmail = email;
 
// EXTRACT FROM THE SUBJECT OF THE EMAIL
 String Notification = extractRef(email.subject);
    //EXTRACT FROM THE BODY OF THE EMAIL IF THE SUBJECT IS EMPTY
    if (Notification == null)
    {
     string emailbody = email.plainTextBody;
    
    //string notifstart = '';
    if(Notification == null || Notification.length() <5)
    {    
        string notifstart = '';
        notification = emailbody.substringBetween('300',' ');
        notifstart = '300';
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('300',',');
            notifstart = '300';
        }
         if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('300','/');
            notifstart = '300';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('350',' ');
            notifstart = '350';
            
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('350',',');
            notifstart = '350';
        }
         if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('350','/');
            notifstart = '350';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('200',' ');
            notifstart = '200';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('200',',');
            notifstart = '200';
        }
         if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('200','/');
            notifstart = '200';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('202',' ');
            notifstart = '202';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('202',',');
            notifstart = '202';
        }
         if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('202','/');
            notifstart = '202';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('250',',');
            notifstart = '250';
        }
        if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('250',' ');
            notifstart = '250';
        }
         if (Notification == null || Notification.length() < 6)
        {
            notification = emailbody.substringBetween('250','/');
            notifstart = '250';
        }
       if (Notification != null){
       notification = notifstart + notification;
       notification=notification.removeEnd(',');
       notification =notification.left(9);
       }
    }
    }
 if(Notification != null)
 {  
  if(TRACE)system.debug(Logginglevel.ERROR,'CaseEmailMerge.  extracted case number: "' + Notification + '"');
  this.theCase = locateByNotificationAsString(Notification);
  if(this.theCase == null) {
   // TODO email error message to SysAdmin
   system.debug(Logginglevel.ERROR,'CaseEmailMerge.  Create a new case.  Could not find case number: "' + Notification + '"');
  }
 } else {
  // try to match subject
  String mainSubject = extractMainSubject(email.subject);
  Case[] matchingCases = [Select Id, Notification__c, QM__c,Repair_Quote_Status__c, Phase_c__c,Status__c,SAP_PO__c,CaseNumber, Subject, Description from Case where (RecordTypeId= '012i0000000xxbSAAQ' or RecordTypeId='012i0000000xxbOAAQ' or RecordTypeId='012i0000000xxbUAAQ' or RecordTypeId='0120H000001YdYhQAK' or RecordTypeId='012i0000000xxbWAAQ' or RecordTypeId='0120H000001Yda4QAC') AND Subject = :mainSubject
   and CreatedDate = LAST_N_DAYS:180];
  if(matchingCases.size() == 1) {
   this.theCase = matchingCases[0];
  } else 
  {
   system.debug(Logginglevel.ERROR,'CaseEmailMerge.  Create a new case because we found '+matchingCases.size() + ' cases with the subject: "' + mainSubject + '"');
  }
 }
 if(this.theCase == null) {
  // else create a new Case
  this.theCase = new Case();
  thecase.RecordTypeID = RTYPE_ID;
  theCase.SuppliedEmail = email.fromAddress;
  theCase.SuppliedName = email.fromName;
  theCase.Status = 'New';
  theCase.Priority = 'Low';
     IF (Notification != Null){
         string NotifOrQm = Notification.left(3);
  // two possible match QMs and notification.. notification and QM will never be filled at the same time
 IF (NotifOrQm == '300' || NotifOrQm == '350')
 {
            theCase.notification__c = Notification; 

 }else{theCase.qm__c = Notification;}}
  
  theCase.OwnerId = '005i0000006F6NzAAK';
  theCase.Origin = 'Email';
  theCase.Subject = email.Subject;
  theCase.Status = 'New';
  theCase.Description = email.plainTextBody;
  
  insertSObject(this.theCase);
 }

 createEmailMessage(theCase,email);
 handleAttachments(theCase, email);

 return result;
}

// Save attachments, if any
public void handleAttachments(Case theCase, Messaging.InboundEmail email) {
 if(email.textAttachments!=null && email.textAttachments.size() >0) {
  for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
    Attachment attachment = new Attachment();  
    attachment.Name = tAttachment.fileName;
    attachment.Body = Blob.valueOf(tAttachment.body);
    attachment.ParentId = theCase.Id;
    insertSObject(attachment);
  }
 }
 
 if(email.binaryAttachments!=null && email.binaryAttachments.size() >0) {
  for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
    Attachment attachment = new Attachment();
    attachment.Name = bAttachment.fileName;
    attachment.Body = bAttachment.body;
    attachment.ParentId = theCase.Id;
    insertSObject(attachment);
  }
 } 
}

public void insertSObject(sObject obj) {
 try {insert obj;} catch (System.DmlException e) {handleError(e, 'Could not insert obj '+ obj);}
}
//salesforce limits
public String limitLength(String input, Integer maxLength)
{
 String results;
 if(input != null && input.length() > maxLength)
  results = input.substring(0,maxLength);
 else 
  results = input;
 return results;
}

public void createEmailMessage(Case theCase, Messaging.InboundEmail email) {
 String value;
 Integer maxlength;
 EmailMessage theEmail = new EmailMessage();
 theEmail.ParentId = theCase.Id;
 theEmail.Incoming = true;
 Schema.DescribeFieldResult F = EmailMessage.HtmlBody.getDescribe();
 //.HtmlBody.getDescribe();
 maxlength = F.getLength();
 theEmail.Subject = limitLength(email.Subject, EmailMessage.Subject.getDescribe().getLength());
 theEmail.MessageDate = datetime.now();
 theEmail.HtmlBody = limitLength(email.htmlBody,EmailMessage.HtmlBody.getDescribe().getLength());  
 theEmail.TextBody = limitLength(email.plainTextBody,EmailMessage.TextBody.getDescribe().getLength());

 /* **** To */
 value = '';
 if(email.toAddresses != null) {
  Boolean seenOne= false;
  for(String to : email.toAddresses) {
   if(seenOne) {
    value += ';\n';
   }
   to  = extractAddress(to);
   system.debug('ToAddress: ' + to);
   value += to;
   seenOne = true;
  }
 }
 theEmail.ToAddress = limitLength(value,EmailMessage.ToAddress.getDescribe().getLength());
 
 /* **** From */
 theEmail.FromName = email.fromName;
 theEmail.FromAddress = email.fromAddress;
 
 /* **** CC */
 value = '';
 if(email.ccAddresses != null) {
  Boolean seenOne= false;
  for(String cc : email.ccAddresses) {
   if(seenOne) {
    value += ';\n';
   }
   cc  = extractAddress(cc);
   system.debug('CcAddress: ' + cc);
   value += cc;
   seenOne = true;
  }
 }
 theEmail.CcAddress = limitLength(value,EmailMessage.CcAddress.getDescribe().getLength()); 
 insertSObject(theEmail);
}



public void handleError(System.DmlException e,  String message){
 String baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
 if(TRACE)system.debug(baseURL);
 String caseURL;  
 String msg = message + '\n';
 if(this.theCase != null)
 {
  caseURL = baseURL + theCase.Id;
  msg += '\n';
  msg += 'Originating Notification Number: ' + theCase.Notification__c + '  '+ caseURL+'\n';   
 }
 if(this.inboundEmail != null) {
  msg += '\nEmail:';
  msg += '  subject: ' + inboundEmail.Subject + '\n'; 
  msg += '  from: ' + inboundEmail.FromName + '\n'; 
  msg += '  address: ' + inboundEmail.FromAddress + '\n'; 
 }
 if(e != null) { // compose the DmlException message on one line to minimize the number of untested lines.  AFAIK easy to instantiate a DmlException in a unit test. 
  msg += '\n';
  msg += 'EXCEPTION:\n  Error: ' + e.getMessage() + '\n  Type: ' + e.getTypeName() + '\n  Line Number: ' + e.getLineNumber() + '\n  Trace:\n' + e.getStackTraceString() + '\n(end stack trace)\n';
 }

 Case errCase = new Case();
 errCase.OwnerId = '005i0000006F6NzAAK';
 errCase.Status = 'New';
 errCase.Priority = 'Low';
 errCase.Origin = 'Email';
 errCase.RecordTypeID = RTYPE_ID;
 
    ///errCase.Notification__c = theCase.Notification__c;
 errCase.Subject = 'Error processing incoming email';
 errCase.Description = limitLength(msg,Case.Description.getDescribe().getLength());
 insert errCase;
 errCase = [Select Id, notification__c,Phase_c__c,Status__c,CaseNumber from Case where Id = :errCase.Id limit 1];  

 caseURL = baseURL + errCase.Id;
 msg += '\n\n';
 msg += 'Created new Notification number ' + errCase.Notification__c + ' for this error.  See: ' + caseURL +'\n'; 

 //TriggerErrorNotification.reportError('CaseEmailMerge', msg); 

}


public Case locateByNotificationAsString(String caseNotifStr){
 string target = caseNotifStr;
 Case theResult = null;
 String Notification = '%' + String.valueOf(target);
 Case[] matchingCases = [Select Id, Notification__c,Repair_Quote_Status__c, SAP_PO__c,QM__c, Quote_Date__c, CaseNumber, Subject, Description from Case where (RecordTypeId= '012i0000000xxbSAAQ' or RecordTypeId='012i0000000xxbOAAQ' or RecordTypeId='012i0000000xxbUAAQ' or RecordTypeId='0120H000001YdYhQAK' or RecordTypeId='012i0000000xxbWAAQ' or RecordTypeId='0120H000001Yda4QAC') and (Notification__c like :Notification or QM__C LIKE :Notification)];
 for(Case aCase: matchingCases) {
      // two possible match QMs and notification.. notification and QM will never be filled at the same time
      //  String cnum = aCase.qm__c;
     //if(aCase.qm__c == null)
    // {
      //  cnum = aCase.Notification__c; 
    // }
 
     String cnum = aCase.Notification__c;
     if(aCase.Notification__c != target)
     {
        cnum = aCase.QM__c; 
     }
 
     
  if(cnum == target) {
  theResult = aCase;
      // after merge change the status/ only if status is pending quote and add quote date
      if (aCase.Repair_Quote_Status__c == 'Pending Quote' ||string.isEmpty(aCase.Repair_Quote_Status__c)==true || aCase.Repair_Quote_Status__c == 'No Quote Required')
          
      {
       		aCase.Repair_Quote_Status__c = 'Quote Received';
            aCase.Quote_Date__c = Date.today(); 
            update aCase;
        }
      //change status to PO approved.
      if (aCase.Repair_Quote_Status__c == 'PO Created' || aCase.Repair_Quote_Status__c == 'PO Approval not sent' )
        {
       		aCase.Repair_Quote_Status__c = 'PO Approved';
            aCase.PO_Approval_Date__c = system.today();
            update aCase;
        }
        
  break;
  }
 }
 return theResult;
}


public String extractRef(String emailSubject)
{
 String itemRef = null;
   if(itemRef == null )
    {    
        string notifstart = '';
        itemRef = emailSubject.substringBetween('300',' ');
        notifstart = '300';
        if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('300',',');
            notifstart = '300';
        }
         if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('300','/');
            notifstart = '300';
        }
        if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('350',' ');
            notifstart = '350';
            
        }
        if (itemRef == null)
        {
            itemRef = emailSubject.substringBetween('350',',');
            notifstart = '350';
        }
         if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('350','/');
            notifstart = '350';
        }
       
        if (itemRef == null)
        {
            itemRef = emailSubject.substringBetween('200',' ');
            notifstart = '200';
        }
        if (itemRef == null)
        {
            itemRef = emailSubject.substringBetween('200',',');
            notifstart = '200';
        }
         if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('200','/');
            notifstart = '200';
        }
       
        if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('202',' ');
            notifstart = '202';
        }
        if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('202',',');
            notifstart = '202';
        }
         if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('202','/');
            notifstart = '202';
        }
       
        if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('250',' ');
            notifstart = '250';
        }
        if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('250',',');
            notifstart = '250';
        }
         if (itemRef == null )
        {
            itemRef = emailSubject.substringBetween('250','/');
            notifstart = '250';
        }
       
        
        if (itemRef != null){
        itemRef = notifstart + itemRef;
       itemRef=itemRef.removeEnd(',');
       itemRef =itemRef.left(9);
       }
    }
    
 return itemRef; 
}

public String extractMainSubject(String emailSubject)
{
 if(emailSubject == null || emailSubject.length() < 3)
  return emailSubject;
 String[] prefixes = new String[] {'fw:','re:', 'automatic reply:', 'out of office autoreply:', 'out of office'};  
 String target = emailSubject.toLowerCase();
 for(String prefix: prefixes) {
  Integer index = target.indexOf(prefix); 
  if(index == 0 ){
   String mainSubject = emailSubject.substring(prefix.length(),emailSubject.length());
   return mainSubject.trim();
  }  
 }
 return emailSubject; 
}

public String extractAddress(String inAddress)
{
 String address;
 String patternString;
 Pattern thePattern;
 Matcher matcher;
 patternString = '.*<(.*)>.*';
 thePattern = Pattern.compile(patternString);
 matcher = thePattern.matcher(inAddress);
 if (matcher.matches()) {
  address = matcher.group(1);
  system.debug('Extracted address ' + address); 
 }
 else
 {
  address = inAddress;
  system.debug('Did not match angle-address ' + address);   
 }
 return address;
 
}
    }