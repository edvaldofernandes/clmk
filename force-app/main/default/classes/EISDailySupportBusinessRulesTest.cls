@isTest
public class EISDailySupportBusinessRulesTest {

    static testMethod void method1(){
        EIS_Daily_Report_Settings__c cs = new EIS_Daily_Report_Settings__c();
        cs.Email_Footer__c = 'footer';
        cs.Email_Header__c = 'header';
        cs.Email_Subject__c = 'subject';
        cs.Model_Type__c = 'E190;E195;';
        cs.Email_to__c = 'teste@teste.com;teste1@teste.com';
        insert cs;
        
        Account account = new Account();
        account.Name = 'Test account name';
        account.Company_Nickname__c = 'Test c nickename ';
        
        insert account;
            
        Aircraft__c aircraft = new Aircraft__c();
        aircraft.Owner__c = account.Id;
        aircraft.Operator__c = account.Id;
        aircraft.Flying_For__c = account.Id;
        aircraft.RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Embraer').getRecordTypeId();
        aircraft.Name = '09876478';
        aircraft.Aircraft_Status__c = 'In Service';
        aircraft.Model_Type__c = 'E190';
        aircraft.Commercial_Name__c = 'EMB-110P1';
            
        Aircraft__c aircraft2 = new Aircraft__c();
        aircraft2.Owner__c = account.Id;
        aircraft2.Operator__c = account.Id;
        aircraft2.Flying_For__c = account.Id;
        aircraft2.RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Embraer').getRecordTypeId();
        aircraft2.Name = '09876499';
        aircraft2.Aircraft_Status__c = 'In Service';
        aircraft2.Model_Type__c = 'E190';
        aircraft2.Commercial_Name__c = 'EMB-110P1';
        
        List<Aircraft__c> listAir = new List<Aircraft__c>();
        listAir.add(aircraft);
        listAir.add(aircraft2);
        insert listAir;

        Id idEISRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName().get('EIS Daily Report').getRecordTypeId();    
        
        Communications__c com = new Communications__c();
        com.RecordTypeId = idEISRecordType;
        com.Operator__c = aircraft.Operator__c;
        com.Reference_Date__c = Date.newInstance(2017, 1, 1);
        
        Test.startTest();
        insert com;
        
        Id idEISInt = Schema.SObjectType.Events__c.getRecordTypeInfosByName().get('Interruption').getRecordTypeId(); 
        Id idEISRem = Schema.SObjectType.Events__c.getRecordTypeInfosByName().get('Removal').getRecordTypeId(); 
        Id idEISPirep = Schema.SObjectType.Events__c.getRecordTypeInfosByName().get('Pirep').getRecordTypeId(); 
        
        Minimum_Equipment_List__c mel = new Minimum_Equipment_List__c();
        mel.Aircraft__c = aircraft.Id;
        mel.EIS_Daily_Report__c = com.Id;
        mel.Status__c = 'OPEN';
        
        Minimum_Equipment_List__c mel2 = new Minimum_Equipment_List__c();
        mel2.Aircraft__c = aircraft.Id;
        mel2.EIS_Daily_Report__c = com.Id;
        mel2.Status__c = 'Close';
        mel2.Open_Date__c = Date.newInstance(2017, 1, 1);
        mel2.Close_Date__c = Date.newInstance(2018, 5, 5);
        
        List<Minimum_Equipment_List__c> listMel = new List<Minimum_Equipment_List__c>();
        listMel.add(mel);
        listMel.add(mel2);
        insert listMel;
        
        Events__c evtInt = new Events__c();
        evtInt.Aircraft__c = aircraft.Id;
        evtInt.RecordTypeId = idEISInt;
        evtInt.Daily_Summary_Interruption__c = com.Id;
       
        insert evtInt;
        
        Events__c evtRem= new Events__c();
        evtRem.Aircraft__c = aircraft.Id;
        evtRem.RecordTypeId = idEISRem;
        evtRem.Daily_Summary_Removals__c = com.Id;
       
        insert evtRem;
        
        Events__c evtPirep = new Events__c();
        evtPirep.Aircraft__c = aircraft.Id;
        evtPirep.RecordTypeId = idEISRem;
        evtPirep.Daily_Summary_Pirep__c = com.Id;
       
        insert evtPirep;

        com.Communication_Status__c = 'Issued';
        com.Daily_Summary__c = 'daily';
        
        update com;
        Test.stopTest();
    }
}