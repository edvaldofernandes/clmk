/* Classe implementadora de SOBjectDAO para operações DML no objeto  Aircraft__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 11/01/2016
*/
public without sharing class AircraftDao {
    
    private static final AircraftDao instance = new AircraftDao();    
    
    private AircraftDao(){
    }    
    
    public static AircraftDao getInstance(){
        return instance;
    }
    
    public List<Aircraft__c> listByOwnerCId(string idOwner){
        List<Aircraft__c> listAircraft = database.query(' Select ' + utils.getAllFields('Aircraft__c') + ' FROM Aircraft__c WHERE Owner__c =\'' +String.escapeSingleQuotes(idOwner)+'\'');
        return listAircraft;
    }
    
}