@isTest
public class TestEmbDispo {

//Testing constructor case 1
    @isTest static void TestcreateEmbDispo_m1_c0(){
        //generate test data
        ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c0();
        
        Test.startTest();
        createEmbTechDispo x = new createEmbTechDispo(sc);
        Test.stopTest();
        
        //assert 
        //there shopfinding should be retrieved from db query, should equal Comments2
        System.assertEquals('ShopFinding1', x.c.Shop_Findings__c);
    }
    
    //Testing constructor case 2
    @isTest static void TestcreateEmbDispo_m1_c1(){
       // generate test data
        ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c1();
        
        Test.startTest();
        createEmbTechDispo x = new createEmbTechDispo(sc);
        Test.stopTest();
        
        //assert 
        //there shopfinding should be retrieved from db query, should equal Dispo2
       System.assertEquals('Dispo2', x.c.Disposition__c);
      //  System.assert(x.ReasonForRemovalIsBlank);
    }
    
    //Testing constructor case 3
   // @isTest static void TestcreateEmbDispo_m1_c2(){
        //generate test data
   //     ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c2();
        
    //    Test.startTest();
    //    createEmbTechDispo x = new createEmbTechDispo(sc);
     //   Test.stopTest();
        
        //assert 
        //there should be retrieved from db query, should equal PN3
    //    System.assertEquals('Dispo3', x.c.Disposition__c);
       // System.assert(x.ShopFindingIsBlank);
  //  }
    
    
    //Testing GetXMLString
    @isTest static void testgetXmlString(){
        //generate test data
        ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c1();
        string notif = '';
        string serial = '';
        string Doc = '';
        date DcDate = system.today();
        string Comments ='';
        string Disposition ='Dispo2';
        string custname='X';
        
        
        Test.startTest();
        //initialize CreateEmbTechDispo class
        createEmbTechDispo x = new createEmbTechDispo(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.DocDate = DcDate;
       // x.CSEAnalysis = Comments;
       x.FinalDispo = Disposition;
        x.CustomerName = custname;
        
        //this is the function being tested
        string xmlString = x.getXmlString();
        Test.stopTest();
        
        //assert 
                //creating xml string for the full document
            string s = '<?xml version="1.0" encoding="UTF-8"?>' +
                '<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' +
                '<f href="S:\\Material Services\\06 - OPERATIONAL SUPPORT\\Developments\\EmbEngDispo\\EmbEngDispoBlank.pdf"/>' +
                '<fields>' +
                '<field name="Date"><value>'+system.today().format()+'</value></field>'+
                '<field name="Notification"><value>'+x.c.Notification__c+'</value></field>'+
               '<field name="DocDate"><value>'+ system.today().format() +'</value></field>'+
                '<field name="FinalDispo"><value>'+x.c.Disposition__c+'</value></field>'+
                '<field name="PartNumber"><value>'+x.c.Part_Number__r.Name+'</value></field>'+
                '<field name="PartDescription"><value>'+x.c.Part_Number__r.Description__c+'</value></field>'+
                '<field name="EngineeringRecomendation"><value>'+x.c.Engineering_Recommendation__c+'</value></field>'+
                '<field name="CSEcomments"><value>'+x.c.CSE_Comments__c+'</value></field>'+
                '<field name="ShopFindings"><value>'+x.c.Shop_Findings__c+'</value></field>'+
                '<field name="RepairStation"><value>'+x.c.Repair_Station__r.Name+'</value></field>'+
                '<field name="CustName"><value>'+x.c.Customer_Name__c+'</value></field>'+
                '<field name="CaseNumber"><value>'+x.c.CaseNumber+'</value></field>'+
                '<field name="ReasonForRemoval"><value>'+x.c.Reason_for_Removal__c+'</value></field>'+
                '<field name="ACType"><value>'+x.c.A_C_TypeSJK__c+'</value></field>'+
                '<field name="AircraftSerialNumber"><value>'+x.c.Aircraft_Serial_Number__r.Name+'</value></field>'+
                '<field name="SerialNumber"><value>'+x.c.Serial_Number__c+'</value></field>'+
                '<field name="DamageType"><value>'+x.c.Damage_Type__c+'</value></field>'+
                '</fields>' +
                '</xfdf>';
        System.assertEquals(s, xmlString);
    }
    
   
    //Testing XDPInit base case
    @isTest static void testXDPInit_m1_c1(){
        //generate test data
        ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c1();
        string notif = 'notif';
        string serial = 'serial';
        string Doc = 'Doc';
        date DcDate = system.today();
     //   string Comments ='comments2';
        
        
        Test.startTest();
        //initialize createEmbTechDispo class
        createEmbTechDispo x = new createEmbTechDispo(sc);
        x.notification = notif;
        x.serialNumber = serial;

        x.DocDate = DcDate;
       // x.CSEAnalysis = Comments;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        System.assert(true);
    }
    
    //Testing XDPInit shopfinding is blank
    @isTest static void testXDPInit_m0_c1(){
        //generate test data
        ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c2();
        string notif = 'notif';
        string serial = 'serial';
        string Doc = 'Doc';
        date DcDate = system.today();
        string Comments = null;
        string ShopFin = 'ShopFin';
        
        Test.startTest();
        //initialize createEmbTechDispo class
        createEmbTechDispo x = new createEmbTechDispo(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.DocDate = DcDate;
      //  x.CSEAnalysis = Comments;
        x.ShopFinding = ShopFin;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        //shop finding is empty
        case c = [select Shop_Findings__c FROM case WHERE Disposition__c='Dispo3'];
        System.assertEquals(ShopFin, c.Shop_Findings__c);
    }
    
    //Testing XDPInit CSE_Comments__c is blank
    @isTest static void testXDPInit_m1_c0(){
        //generate test data
        ApexPages.StandardController sc = testDataEmbDispo.createEmbDispo_m1_c0();
        string notif = 'notif';
        string serial = 'serial';
        string Doc = 'Doc';
        date DcDate = system.today();
        string Comments ='Comments2';
        string ShopFin = 'ShopFinding1';
        
        
        Test.startTest();
        //initialize createEmbTechDispo class
        createEmbTechDispo x = new createEmbTechDispo(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.DocDate = DcDate;
      //  x.CSEAnalysis = Comments;
        x.ShopFinding = ShopFin;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        case c = [select Shop_Findings__c FROM case WHERE Reason_for_Removal__c='reason1'];
        System.assertEquals(ShopFin, c.Shop_Findings__c);
    }
    
  
}