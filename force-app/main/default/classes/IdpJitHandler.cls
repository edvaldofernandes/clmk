/**
* @author 
* @date 01/02/2021
* @description: Receive information from AD and handle with Users
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Renan de Castan e Carvalho        01 FEB 2021             Original Version
* Renan de Castan e Carvalho        15 APR 2021             Removed User Request interation
**/

global class IdpJitHandler implements Auth.SamlJitHandler {
    private class JitException extends Exception{}
    private final String ORG_WEB_GROUP = 'web-salesforce-defense';
    
    
    //* @description hasWebGroup: Verify if the user has the right Embraer WebGroup.
    //* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    //* @return Boolean : If the user has the right Embraer WebGroup
    
    //Waiting for ITWF CONFIG
    //private Boolean hasWebGroup(Map<String, String> attributes){
        //String environment = 'qas';
        //if(attributes.containsKey('WebGroupList')) {
        //    if (!Boolean.valueOf([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox))
        //    	environment = 'prd';
        //    String webGroupList = attributes.get('WebGroupList').toLowerCase();
        //    if (webGroupList.contains(ORG_WEB_GROUP+'-'+environment))
        //        return true;
        //}
        //return false;
    //}
    
    // DEPRECATED - ACTIVE DIRECTORY DOES NOT SEND DEACTIVATION CONFIRMATION
    //* @description isEmbraerActiveUser: Verify if the user is active in Embraers AD.
    //* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    //* @return Boolean : If the user is active
    
    //private Boolean isEmbraerActiveUser(Map<String, String> attributes){
	//	if(attributes.containsKey('Active'))
    //        return Boolean.valueOf(attributes.get('Active'));
    //    return false;
    //    return true;
    //}
    
    
    //* @description NoWebgroupNotification: Send a confirmation email when a new user tries to login in salesforce.
    //* @param User user : user information.
    //* @return void.
    
    //Waiting for ITWF CONFIG
    /*private void NoWebgroupNotification (User user) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] {String.valueOf(user.Email)};
        message.optOutPolicy = 'FILTER';
        message.subject = 'Please request Salesforce WEBGROUP via ITWF';
        message.plainTextBody = 'Dear User,\n';
		message.plainTextBody += 'To login into Salesforce you must have the ' + ORG_WEB_GROUP + '-prd WebGroup. Make the request via IT Workflow.';
        message.plainTextBody += '\n\nEmbraer Salesforce Team';
        if(user.Email==null||user.Email=='') return;
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }*/
   
    //* @description nameFormat: Make first letter capital.
    //* @param String name : String to be formatted.
    //* @return String : Result.
    
    private String nameFormat (String name) {
        system.debug('name'+name);
        List<String> names = name.toLowerCase().split(' ');
        for (Integer i = 0; i < names.size(); i++)
            names[i] = names[i].capitalize();
        name = String.join(names, ' ');
        system.debug('name'+name);
        return name;
    } 
   
    //* @description handleUser: to set or update users main attributes.
    //* @param Boolean create : Flag to select if a new user must be created.
    //* @param User u : User record to be created or updated in the database.
    //* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    //* @param String federationIdentifier : The ID Salesforce expects to be used for this user.
    //* @param boolean isStandard : Flag to differentiate community/portal users from standard users.
    //* @return User : The updated user.
    
    private User handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) {
        system.debug('IdpJitHandler.handleUser');
        String userEmail = '';//'renan.castan@embraer.net.br';
        Boolean hasChanges = false;
        //Boolean userHasWebGroup = hasWebGroup(attributes);

		for (String key : attributes.keySet()) {
            system.debug('DEBUG ATTRIBUTES: ' + key + ': ' + attributes.get(key));
        }
        if(attributes.containsKey('EmployeeNumber')) {
            String chapa = attributes.get('EmployeeNumber');
            if (chapa != u.EmployeeNumber) {
                u.EmployeeNumber = chapa;
                hasChanges = true;  
            }
        }
        if(attributes.containsKey('Alias')) {
            String embraerUsername = attributes.get('Alias').toLowerCase();
            if (embraerUsername != u.Alias) {
                u.Alias = embraerUsername;
                hasChanges = true;    
            }
        }
        if(attributes.containsKey('Department')) {
            String area = attributes.get('Department');
            if (area != u.Department) {
                u.Department = area;
                hasChanges = true;  
            }
        }
        if(attributes.containsKey('LastName')) {
            String ln = nameFormat(attributes.get('LastName'));
            if (ln != u.LastName) {
                u.LastName = ln;
                hasChanges = true;  
            }
        }
        if(attributes.containsKey('FirstName')) {
            String fn = nameFormat(attributes.get('FirstName'));
            if (fn != u.FirstName) {
                u.FirstName = fn;
                hasChanges = true;  
            }
        }
        if(attributes.containsKey('Company')) {
            String company = attributes.get('Company');
            if (company != u.CompanyName) {
                u.CompanyName = company;
                hasChanges = true;  
            }
        }
        //if(attributes.containsKey('Manager')) {
        //    String manager = attributes.get('Manager').split(',')[0].removeStart('CN=');
        //    if(createOrUpdateUserRequest && newUR.Immediate_Superior_Embraer_User__c != manager)
		//		newUR.Immediate_Superior_Embraer_User__c = manager;
        //}
        
        //Waiting for ITWF CONFIG
        //if (!userHasWebGroup && !create){
            
            //Option 1: Freeze user without webgroup
            /*UserLogin ul = [SELECT Id, UserId, IsFrozen FROM UserLogin WHERE UserId =: u.Id];
            ul.IsFrozen=true;
            update ul;
            NoWebgroupNotification(u);*/
            //Option 2: Deactivate user without webgroup
            //u.IsActive = false;
        	//hasChanges = true;
        //}
        if(!create) {
            if(hasChanges)
                update(u);
        }
        return u;
    }
    
   
    //* @description handleJit: Handle diferences between Community/Portal Users and Standard Salesforce User scenarios. 
    //* @param Boolean create : Flag to select if a new user must be created.
    //* @param User u : User record to be created or updated in the database.
    //* @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
    //* @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
    //* @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
    //* @param String federationIdentifier : The ID Salesforce expects to be used for this user.
    //* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    //* @param String assertion : The default SAML assertion, base-64 encoded.
    //* @return User : The new or updated User record.
    
    private User handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        if(communityId != null || portalId != null) {
            //String account = handleAccount(create, u, attributes);
            //handleContact(create, account, u, attributes);
            //handleUser(create, u, attributes, federationIdentifier, false);
            //We're not handling Community SSO Login
            system.debug('IdpJitHandler.handleJit.Community/Portal');
            return null;
        } else {
            system.debug('IdpJitHandler.handleJit');
            return handleUser(create, u, attributes, federationIdentifier, true);
        }
    }
    
    
    //* @description Implements Auth.SamlJitHandler method to create an user request
    //* @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
    //* @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
    //* @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
    //* @param String federationIdentifier : The ID Salesforce expects to be used for this user.
    //* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    //* @param String assertion : The default SAML assertion, base-64 encoded.
    //* @return User : Created User
        
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        //We're not handling new users
        system.debug('IdpJitHandler.createUser');
        User u = new User();
        //handleJit(true, u, samlSsoProviderId, communityId, portalId,
        //    federationIdentifier, attributes, assertion);
        //system.debug('IdpJitHandler.createUser.User: '+ u);
        return u;
    }
    
   
    //* @description Implements Auth.SamlJitHandler method to update user
    //* @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
    //* @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
    //* @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
    //* @param String federationIdentifier : The ID Salesforce expects to be used for this user.
    //* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    //* @param String assertion : The default SAML assertion, base-64 encoded.
    
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = [SELECT Id, LastName, FirstName, CompanyName, FederationIdentifier, EmployeeNumber, Alias, Department, IsActive FROM User WHERE Id=:userId];
        system.debug('IdpJitHandler.updateUser');
        handleJit(false, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
    }
}