@isTest
public class FO_MyOP_TestDataFactory {
    
    public static List<Account> createAccounts(Integer numAccts){
        RecordType accRT = [SELECT Id FROM RecordType WHERE SobjectType='Account' AND Name = 'Airline'];    	
        //Create accounts
		List<Account> testAccount = new List<Account>();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account();
            if(i==0){
                a.Name='Embraer testCompany' + i;
            }else{
                a.Name='testCompany' + i;
            }
            a.Geographical_Area__c = 'North America';
            a.Company_Nickname__c = 'testCompany' + i;
            a.FlightOps_Account_Domains__c = '@teste' +i;
            a.Embraer_Fleet_Type__c='Turboprop,ERJ,E2';
            a.RecordTypeId = accRT.Id;
            //For each account a add to testAccount
            testAccount.add(a);
        }
        
        return testAccount;
    }
    
    public static List<Contact> createContacts(List<Account> testAccount, Integer numAccts, Integer numContactsPerAcct){
    	//Create contacts and assign to accounts
        List<Contact> testContact = new List<Contact>();
        for(Integer j=0;j<numAccts;j++) {
            Account acc = testAccount[j];
            for (Integer k=numContactsPerAcct*j;k<numContactsPerAcct*(j+1);k++) {
                Contact c = new Contact();
                c.FirstName = 'TestFirstName'+k;
                c.LastName = 'Test.LastName'+k;
                c.AccountId = acc.Id;
                c.Email = 'TesteFirstName'+k+'@embraer.com.br';                
                // For each account just inserted, add contacts
            	testContact.add(c);
            }
        }
        return testContact;
    }
    
    public static List<Contact> createContacts2(List<Account> testAccount, Integer numAccts, Integer numContactsPerAcct){
    	//Create contacts and assign to accounts
        List<Contact> testContact = new List<Contact>();
        for(Integer j=0;j<numAccts;j++) {
            Account acc = testAccount[j];
            for (Integer k=numContactsPerAcct*j;k<numContactsPerAcct*(j+1);k++) {
                Contact c = new Contact();
                c.FirstName = 'TestFirstName'+k;
                c.LastName = 'Test.LastName'+k;
                c.AccountId = acc.Id;
                c.Email = 'TesteFirstName'+k+'@testmyopereng.com.br';                
                // For each account just inserted, add contacts
            	testContact.add(c);
            }
        }
        return testContact;
    }
    
    public static List<User> createUsers(List<Contact> testContact){
        //Create users and assign to contacts
		List<User> testUser = new List<User>();
        Integer numTotalContacts = testContact.size();
		for(Integer l=0;l<numTotalContacts;l++) {
            if( testContact[l]!= NULL){
                Contact con = testContact[l];
                User u = new User();
                u.ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id;
                u.Email = con.Email;
                u.FirstName = con.FirstName;
                u.LastName = con.LastName;
                u.ContactId = con.Id;
                u.Username = con.Email;
                u.EmailEncodingKey = 'ISO-8859-1';
                u.Alias = 'Alias'+l;
                u.TimeZoneSidKey = 'America/Los_Angeles';
                u.LocaleSidKey = 'en_US';
                u.LanguageLocaleKey = 'en_US';
                u.CommunityNickname = 'Nickname'+l; 
                testUser.add(u);
            }
		}
        
        return testUser;
    }
    
    public static List<Case> createCases(List<Contact> testContact, Integer numCasesPerUser){
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
    	//Create cases and assign to users
        List<Case> testCase = new List<Case>();
        Integer numTotalContacts = testContact.size();
        for(Integer y=0;y<numTotalContacts;y++) {
            Contact c = testContact[y];
            for(Integer x=numCasesPerUser*y;x<numCasesPerUser*(y+1);x++){
                Case cas = new Case();
                cas.Subject = 'TestSubject'+x;
                cas.Description = 'TestDescription'+x;
                cas.AccountId = c.AccountId;
                cas.RecordTypeId = caseRT.Id;
                cas.Customer_Visible__c = Boolean.valueOf('true');
                if(math.mod(x,2) == 0){	
                    cas.Status = 'New';
                } else {
                    cas.Status = 'Closed';
                }
                
                testCase.add(cas);
            }
        }
        return testCase;
    }
    
    public static List<CaseComment> createCaseComment(List<Case> testCase, Integer numComPerCase){
    	//Create comments and assign to cases
        List<CaseComment> testComment = new List<CaseComment>();
        Integer numTotalCases = testCase.size();
        for(Integer y=0;y<numTotalCases;y++) {
            Case c = testCase[y];
            for(Integer x=numComPerCase*y;x<numComPerCase*(y+1);x++){
                CaseComment comm = new CaseComment();
                comm.ParentId = c.Id;
                comm.CommentBody = 'TestComment'+x;
                comm.IsPublished = true;
                testComment.add(comm);
            }
        }
        return testComment;
    }
    
    public static List<EmailMessage> createCaseEmail(List<Case> testCase, Integer numEmailPerCase){
    	//Create emails and assign to cases
        List<EmailMessage> testEmail = new List<EmailMessage>();
        Integer numTotalCases = testCase.size();
        for(Integer y=0;y<numTotalCases;y++) {
            Case c = testCase[y];
            for(Integer x=numEmailPerCase*y;x<numEmailPerCase*(y+1);x++){
                EmailMessage em = new EmailMessage();
                em.ParentId = c.Id;
                em.TextBody = 'TestEmailBody'+x;
                em.Subject = 'TestSubject'+x;
                testEmail.add(em);
            }
        }
        return testEmail;
    }
    
	public static List<EOD__c> createEods(List <Case> testCase , Integer numEodPerCase ){
        //Eods Relacionadas aos Cases
        List<EOD__c> testEod= new List<EOD__c>();
        Integer numTotalCases=testCase.size();
        
        for (Integer y=0; y<numTotalCases; y++)
        {
          Case c =testCase[y];
          for( Integer x=numEodPerCase*y; x<numEodPerCase *(y+1); x++)
          {
            EOD__c eod = new EOD__c();
            eod.Subject__c = 'EodSubject'+x;
            eod.Related_Case__c = c.Id; 
            testEod.add(eod);
          }
        }
        return testEod;
      }
     

}