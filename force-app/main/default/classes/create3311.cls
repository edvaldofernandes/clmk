public class create3311 {
    //Author: John Paul Morrissee
    //These sites provided the example code which is used here
	//http://www.interactiveties.com/blog/2015/visualforce-button-pdf.php
    //https://developer.salesforce.com/page/Adobe_XFDF
    
    public boolean isInvoiceOnly{get;set;} {isInvoiceOnly=false;}
    public string address{get;set;}
    public string hts{get;set;}
    public string manufacturer{get;set;}
    public string cityState{get;set;}
    public string notification{get;set;}
    public string serialNumber{get;set;} 
    public decimal value{get;set;}
    public string declarant{get;set;}
    public string port{get;set;}
    public date portDate{get;set;}
    public string company{get;set;}
    public string madeIn{get;set;}
    public boolean manufacturerIsBlank{get;set;} {manufacturerIsBlank=false;}
    public boolean htsIsBlank{get;set;} {htsIsBlank=false;}
    public boolean addressIsBlank{get;set;} {addressIsBlank=false;}
    public boolean cityAndStateIsBlank{get;set;} {cityAndStateIsBlank=false;}
    //map containing company names and their related addresses
    @TestVisible private Map<String, String> companyAddress = new Map<String, String>{'Embraer Asia Pacific Pte. Ltd' => '391B Orchard Road# 24-02 Ngee Ann City- Tower B – Singapore 238874',
																		 	'Transpais Aereo S.A. De C.V.' => 'Av. Rio San Lorenzo, L-102 - San Pedro Garza Garcia, Mexico, 66220',
        																	'Aurora Jet Partner' => '3759 60 Avenue E, Edmonton, AB, Canada, T9E 0V4'};
    //map containing companies' tax id
    @TestVisible private Map<String, String> companyTaxID = new Map<String, String>{'Embraer Asia Pacific Pte. Ltd' => '200618286E',
																		 	'Transpais Aereo S.A. De C.V.' => 'TAE9108207Q7',
        																	'Aurora Jet Partner' => '88442 7865 RT001'};
    @TestVisible private LLPDatabase__C llp; //llp object
	
	//constructor
	public create3311(ApexPages.StandardController standardPageController) {
		llp = (LLPDatabase__c)standardPageController.getRecord(); //instantiate the LLPDatabase object for the current record
        llp = [SELECT Id, Name, Description__c, manufacturer__c, city_state_of_manufacture__c, ManufacturerAddress__c, Htsus__c
                FROM LLPDatabase__c
                WHERE id = :ApexPages.currentPage().getParameters().get('id')];	//gets the information needed from the LLPDatabase record
        
        if(String.Isblank(llp.manufacturer__c))				   //test whether manufacturer is blank
            manufacturerIsBlank=true;
        if(String.isBlank(llp.city_state_of_manufacture__c))  //test whether cityAndState field is blank
            cityAndStateIsBlank=true;
        if(String.Isblank(llp.Htsus__c))					 //test whether Htsus code is blank
            htsIsBlank=true;
        if(String.Isblank(llp.ManufacturerAddress__c))		//test whether manufacturer adress is blank
            addressIsBlank=true;
	}
    
    //create and return xml string, adding all the parameters needed
    @TestVisible private String getXmlString(){
        //creating xml string for the full document, else create only invoice xml string
        if(!isInvoiceOnly){
            string s = '<?xml version="1.0" encoding="UTF-8"?>' +
                '<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' +
                '<f href="S:\\Material Services\\06 - OPERATIONAL SUPPORT\\Developments\\3311\\finalFormWithInvoice.pdf"/>' +
                '<fields>' +
                '<field name="date"><value>'+system.today().format()+'</value></field>'+
                '<field name="nameOfManufacturer"><value>'+llp.manufacturer__c+'</value></field>'+
                '<field name="cityAndStateOfManufacture"><value>'+llp.city_state_of_manufacture__c+'</value></field>'+
                '<field name="reasonForReturn"><value>'+'EXCHANGE RETURN'+'</value></field>'+
                '<field name="unclaimed"><value>'+'X'+'</value></field>'+
                '<field name="no"><value>'+'X'+'</value></field>'+
                '<field name="marks1"><value>'+ llp.Name +'  '+ llp.Description__c +' (S/N: '+serialNumber+')'+'</value></field>'+
                '<field name="marks2"><value>'+'NOTIF: '+ notification +'</value></field>'+
                '<field name="value"><value>'+'$'+ value.setScale(2) +'</value></field>'+
                '<field name="were"><value>'+'X'+'</value></field>'+
                '<field name="have"><value>'+'X'+'</value></field>'+
                '<field name="nameOfDeclarant"><value>'+ declarant +'</value></field>'+
                '<field name="titleOfDeclarant"><value>'+'Regulatory Compliance'+'</value></field>'+
                '<field name="nameOfCorporation"><value>'+'EMBRAER AIRCRAFT CUSTOMER SERVICES, INC.'+'</value></field>'+
                '<field name="name"><value>'+ declarant +'</value></field>'+
                '<field name="address"><value>'+ '276 SW 34th Street Fort Lauderdale, FL 33315' +'</value></field>'+
                '<field name="port2"><value>'+ port +'</value></field>'+
                '<field name="date3"><value>'+ portDate.format() +'</value></field>'+
                '<field name="marks3"><value>'+'Notification Number: '+ notification +'</value></field>'+
                '<field name="number"><value>'+'Part Number:\n'+ llp.Name +'\n\nSerial Number:\n'+serialNumber+'</value></field>'+
                '<field name="quantity"><value>'+ '1' +'</value></field>'+
                '<field name="description"><value>'+ llp.Description__c +'</value></field>'+
                '<field name="address2"><value>'+ companyAddress.get(company) +'</value></field>'+
                '<field name="company"><value>'+ company +'</value></field>'+
                '<field name="notification"><value>'+ notification +'</value></field>'+
                '<field name="taxId"><value>'+'Company Registration No and GST No: '+ companyTaxID.get(company) +'</value></field>'+
                '<field name="addressOfManufacture"><value>'+ llp.ManufacturerAddress__c +'</value></field>'+
                '<field name="htsCode"><value>'+ llp.Htsus__c +'</value></field>'+
                '<field name="partNumber"><value>'+ llp.Name +'</value></field>'+
                '<field name="serialNumber"><value>'+ serialNumber +'</value></field>'+
                '<field name="um"><value>'+ 'EA' +'</value></field>'+
                '<field name="madeIn"><value>'+ madeIn +'</value></field>'+
                '<field name="agent"><value>'+ 'Material Services Agent' +'</value></field>'+
                '</fields>' +
                '</xfdf>';
            return s;
        }else{
            string s = '<?xml version="1.0" encoding="UTF-8"?>' +
     			'<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' +
            	'<f href="S:\\Material Services\\06 - OPERATIONAL SUPPORT\\Developments\\3311\\InvoiceForm.pdf"/>' +
                '<fields>' +
                '<field name="date"><value>'+system.today().format()+'</value></field>'+
                '<field name="nameOfManufacturer"><value>'+llp.manufacturer__c+'</value></field>'+
                '<field name="cityAndStateOfManufacture"><value>'+llp.city_state_of_manufacture__c+'</value></field>'+
                '<field name="reasonForReturn"><value>'+'EXCHANGE RETURN'+'</value></field>'+
                '<field name="unclaimed"><value>'+'X'+'</value></field>'+
                '<field name="no"><value>'+'X'+'</value></field>'+
                '<field name="marks1"><value>'+ llp.Name +'  '+ llp.Description__c +' (S/N: '+serialNumber+')'+'</value></field>'+
                '<field name="marks2"><value>'+'NOTIF: '+ notification +'</value></field>'+
                '<field name="value"><value>'+'$'+ value.setScale(2) +'</value></field>'+
                '<field name="were"><value>'+'X'+'</value></field>'+
                '<field name="have"><value>'+'X'+'</value></field>'+
                '<field name="nameOfDeclarant"><value>'+ declarant +'</value></field>'+
                '<field name="titleOfDeclarant"><value>'+'Regulatory Compliance'+'</value></field>'+
                '<field name="nameOfCorporation"><value>'+'EMBRAER AIRCRAFT CUSTOMER SERVICES, INC.'+'</value></field>'+
                '<field name="name"><value>'+ declarant +'</value></field>'+
                '<field name="address"><value>'+ '276 SW 34th Street Fort Lauderdale, FL 33315' +'</value></field>'+
                '<field name="marks3"><value>'+'Notification Number: '+ notification +'</value></field>'+
                '<field name="number"><value>'+'Part Number:\n'+ llp.Name +'\n\nSerial Number:\n'+serialNumber+'</value></field>'+
                '<field name="quantity"><value>'+ '1' +'</value></field>'+
                '<field name="description"><value>'+ llp.Description__c +'</value></field>'+
                '<field name="address2"><value>'+ companyAddress.get(company) +'</value></field>'+
                '<field name="company"><value>'+ company +'</value></field>'+
                '<field name="notification"><value>'+ notification +'</value></field>'+
                '<field name="taxId"><value>'+'Company Registration No and GST No: '+ companyTaxID.get(company) +'</value></field>'+
                '<field name="addressOfManufacture"><value>'+ llp.ManufacturerAddress__c +'</value></field>'+
                '<field name="htsCode"><value>'+ llp.Htsus__c +'</value></field>'+
                '<field name="partNumber"><value>'+ llp.Name +'</value></field>'+
                '<field name="serialNumber"><value>'+ serialNumber +'</value></field>'+
                '<field name="um"><value>'+ 'EA' +'</value></field>'+
                '<field name="madeIn"><value>'+ madeIn +'</value></field>'+
                '<field name="agent"><value>'+ 'Material Services Agent' +'</value></field>'+
                '</fields>' +
                '</xfdf>';
            return s;
        }
    }
    
    //the main function 
    //updates LLP record if manufacturer or city are blank
	//calculates end value
	//gets the xml string
	//saves the xdp file as an attachment
	//redirects back to the PNDatabase page
	public PageReference XDPInit() {
       	if(manufacturerIsBlank)
            llp.manufacturer__c=manufacturer;
        if(cityAndStateIsBlank)
            llp.city_state_of_manufacture__c=cityState;
        if(htsIsBlank)
            llp.Htsus__c=hts;
        if(addressIsBlank)
            llp.ManufacturerAddress__c=address;
    	
    	
        //remove 16 percent from the price
        value= value*0.84;
        value.setScale(2);
		
        //creates the string which will be the content of the XDP file
        String xmlContent = getXmlString();
    	
        //creates xdp file out of the xml string then attach it to the LLP record
		Attachment attachment = new Attachment();
       	attachment.Body = Blob.valueOf(xmlContent);
       	attachment.Name = 'Form3311_'+llp.Name.substring(0,3)+'.XDP';
       	attachment.ParentId = llp.Id;
        insert attachment;
        
        //when manufacturer info is needed, updates the llp with the new user input
        if(manufacturerIsBlank || cityAndStateIsBlank || htsIsBlank || addressIsBlank)
            update llp;
        
        //redirect to the original PNDatabase page
        PageReference pageWhereWeWantToGo = new ApexPages.StandardController(llp).view(); //we want to redirect the User back to the Account detail page
		pageWhereWeWantToGo.setRedirect(true); //indicate that the redirect should be performed on the client side
		return pageWhereWeWantToGo; //send the User on their way
    }
    
    public void toggleInvoiceOnly(){
        isInvoiceOnly = !isInvoiceOnly;
    }
}