public class CLMSetPDPController {

    public CLMSetPDPController(ApexPages.StandardController controller){
        /*
        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return;
        }
		*/
    }
    
    public PageReference setPDP(){
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Not implemented yet'));
        //return null;
        String agreementId = ApexPages.currentPage().getParameters().get('id');
        
        if (agreementId==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return null;
        }
                
        CLMSetPDP sPDP = new CLMSetPDP();
        agreementId = sPDP.setPDP(agreementId);
        
        if(agreementId=='There is no Payment Setup'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'There is no Payment Setup.'));
            return null;
        }
        
        if(agreementId=='None Elegible Aircraft'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'All Aircraft PDP have already been settled or there is no Aircraft in the agreement. No action will be taken.'));
            return null;
        }

        PageReference returnPage = new PageReference('/'+ agreementId);  
        returnPage.setRedirect(true);
        return returnPage;
    }
    
}