/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ServiceContractDeleteRelatedAircraft
*
* NAME: ServiceContractDeleteRelatedAircraftTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*******************************************************************************/
@isTest
private class ServiceContractDeleteRelatedAircraftTest {

  private static final Integer LOTE = 200;

    static testMethod void myUnitTest() {
       Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
       
       ServiceContract opp = SObjectInstanceTest.createServiceContract();
       opp.Pricebook2Id = stdPB;
       Database.insert(opp);
       
       Product2 prod = SObjectInstanceTest.createProduct2();
       Database.insert(prod);
       
       PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
       Database.insert(pbe);
       
       ContractLineItem oli = SObjectInstanceTest.createContractLineItem(opp.Id, pbe.Id);
       Database.insert(oli);
       
       Aircraft__c air = SObjectInstanceTest.createAircraft();
       Database.insert(air);
       
       Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
       rel.Contract_Line_Item__c = oli.Id;
       rel.Service_Contract__c = opp.Id;
       Database.insert(rel);
       
       Test.startTest();
       Database.delete(opp);
       Test.stopTest();
       
       List<Related_Aircraft__c> lstRel = [SELECT Id from Related_Aircraft__c WHERE Service_Contract__c =: opp.Id];
       system.assert(lstRel.isEmpty());
    }
    
    static testMethod void lote() {
      Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      List<ServiceContract> lstOpp = new List<ServiceContract>();
      for (Integer i = 0; i < LOTE ; i++ )
      {
        ServiceContract opp = SObjectInstanceTest.createServiceContract();
        opp.Pricebook2Id = stdPB;
        lstOpp.add(opp);
      }
      Database.insert(lstOpp);
       
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
       
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
      Database.insert(pbe);
     
      List<ContractLineItem> lstOli = new List<ContractLineItem>();  
      for (Integer i = 0; i < LOTE ; i++ )
      {
        lstOli.add(SObjectInstanceTest.createContractLineItem(lstOpp.get(i).Id, pbe.Id));
      }
      Database.insert(lstOli);
       
      Aircraft__c air = SObjectInstanceTest.createAircraft();
      Database.insert(air);
      
      List<String> lstIds = new List<String>(); 
      List<Related_Aircraft__c> lstRel = new List<Related_Aircraft__c>();  
      for (Integer i = 0; i < LOTE ; i++ )
      {
        Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
        rel.Contract_Line_Item__c = lstOli.get(i).Id;
        rel.Service_Contract__c = lstOpp.get(i).Id;
        lstIds.add(lstOli.get(i).Id);
        lstRel.add(rel);
      }
      Database.insert(lstRel);
       
      Test.startTest();
      Database.delete(lstOpp);
      Test.stopTest();
       
      List<Related_Aircraft__c> lstRelResult = [SELECT Id from Related_Aircraft__c WHERE Service_Contract__c =: lstOpp];
      system.assert(lstRelResult.isEmpty());
    }
}