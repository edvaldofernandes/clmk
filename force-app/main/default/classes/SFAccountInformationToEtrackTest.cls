@isTest
public class SFAccountInformationToEtrackTest {
    
    @isTest static void informationAfterInsertOrUpdateOrDeleteTest(){
        
        Test.startTest();
        
        DescribeFieldResult field = AccountHistory.Field.getDescribe();
        List<PicklistEntry> availableValues = field.getPicklistValues();
        
        Account account = new Account();
        account.Name = 'account insert trigger';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
        
        AccountHistory accountHistoryTest = new AccountHistory();
        accountHistoryTest.Field = availableValues.get(0).getValue();
        accountHistoryTest.AccountId = account.Id;
        insert accountHistoryTest;
        
        Account accountUpdate = [SELECT Name, Company_Nickname__c, Company_Status__c 
                                 FROM Account
                                 WHERE id = :account.id];
        
        accountUpdate.Name = 'account update trigger';
        accountUpdate.Company_Nickname__c = 'tt account';
        accountUpdate.Company_Status__c = 'BLOCKED';
        
        update accountUpdate;
        
        Account accountDelete = [SELECT Name, Company_Nickname__c, Company_Status__c 
                                 FROM Account
                                 WHERE id = :account.id];
        
        delete accountDelete;
        
        Test.stopTest();
    }
}