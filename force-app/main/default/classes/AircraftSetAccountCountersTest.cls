/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Classe de cobertura da classe AircraftSetAccountCounters.cls
*
* NAME: AircraftSetAccountCountersTest.cls
* AUTHOR: RLdO                                                 DATE: 27/05/2014
*******************************************************************************/
@isTest
private class AircraftSetAccountCountersTest
{
    static Account accOp1;
    static Account accOp2;
    static Aircraft__c aeronaveEMB;
    static Aircraft__c aeronaveOther;
    
    static
    {
        accOp1 = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Operator'));
        accOp1.Name = 'Operator-' + accOp1.Name;
        accOp2 = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Operator'));
        accOp2.Name = 'Operator-' + accOp2.Name;
        database.insert(new list<Account> { accOp1, accOp2 } );
        
        aeronaveEMB = new Aircraft__c();
        aeronaveEMB.Owner__c = accOp1.Id;
        aeronaveEMB.Operator__c = accOp1.Id;
        aeronaveEMB.RecordTypeId = RecordTypeMemory.getRecType('Aircraft__c', 'Embraer');
        aeronaveEMB.Name = 'Embraer0';
        aeronaveEMB.Commercial_Name__c = 'EMB-110P1';
        aeronaveEMB.Aircraft_Status__c = 'Other';
        
        aeronaveOther = aeronaveEMB.clone(false, true);
        aeronaveOther.RecordTypeId = RecordTypeMemory.getRecType('Aircraft__c', 'Non_Embraer');
        aeronaveOther.Name = 'Non_Embr';
        aeronaveOther.Commercial_Name__c = 'EMB-110P1';
        aeronaveOther.Aircraft_Status__c = 'Other';
        database.insert(new list<Aircraft__c>{ aeronaveEMB, aeronaveOther } );
    }
    
    static testMethod void myUnitTest1()
    {
        Test.startTest();
        aeronaveOther.Operator__c = accOp2.Id;
        aeronaveOther.Delivery_Date_to_Current_Operator__c = System.Today().addDays(10);
        database.update(aeronaveOther);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest2()
    {
        Test.startTest();
        database.delete(aeronaveOther);
        database.undelete(aeronaveOther);
        Test.stopTest();
    }
}