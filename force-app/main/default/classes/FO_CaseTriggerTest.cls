@isTest
public class FO_CaseTriggerTest {

    @isTest static void testInsertWithAccId(){
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 2);

        
        system.debug(testAccount);
        Test.startTest();
        List<Database.SaveResult> resultAccount = Database.insert(testAccount);
        List<Database.SaveResult> resultContact = Database.insert(testContact);
        List<Database.SaveResult> resultCase = Database.insert(testCase);
        Id parentId = [SELECT Id FROM Case LIMIT 1].Id;
        for(EmailMessage m : testEmail)
            m.parentId = parentId; 
        
        List<Database.SaveResult> resultEmail = Database.insert(testEmail);

        
        Test.stopTest();
	}
	/*
    @isTest static void testInsertWithoutAccId(){
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
        
        
        User liaison  = [SELECT Id FROM User LIMIT 1];
        Account companyAccount = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec',
            FlightOps_Account_Domains__c = '@test.com.br'
        );
        insert companyAccount;
        
        AccountTeamMember membership = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=liaison.Id,
            AccountId=companyAccount.Id
        );
        insert membership; 
        
        Case c1 = new Case(
            Subject = 'TestSubject0',
            Description = 'TestDescription0',
            RecordTypeId = caseRT.Id,
            Customer_Visible__c = Boolean.valueOf('true'),
            Status = 'New',
        	OwnerId = [SELECT Id FROM User LIMIT 1].Id,
        	SuppliedEmail = 'test@test.com.br'
        );
        insert c1;

        EmailMessage em = new EmailMessage(
                ParentId = c1.Id,
                TextBody = 'TestEmailBody0',
                Subject = 'TestSubject0',
                Incoming = true,
                FlightOps_Flag__c = true,
                Status = '0'
        );
        
        Test.startTest();
        
        Database.SaveResult resultCase = Database.insert(c1);
//      Database.SaveResult resultEmail = Database.insert(em);
        
        Test.stopTest();
        
	} original
	*/

    @isTest static void testInsertWithoutFleetType(){
        Test.startTest();
        //Instanciating class and databases
        RecordType accRT = [SELECT Id FROM RecordType WHERE SobjectType='Account' AND Name = 'Airline'];
        Account a = new Account();
            a.Name='Embraer testCompany0';
            a.Geographical_Area__c = 'North America';
            a.Company_Nickname__c = 'testCompany0';
            a.FlightOps_Account_Domains__c = '@teste0';
            a.Embraer_Fleet_Type__c='EJet';
            a.RecordTypeId = accRT.Id;
        Database.insert(a);
        
        Contact c = new Contact();
                c.FirstName = 'TestFirstName0';
                c.LastName = 'Test.LastName0';
                c.AccountId = a.Id;
                c.Email = 'TesteFirstName0@embraer.com.br';        
        Database.insert(c);
        
        User u1 = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName,
                ContactId = c.Id,
                Username = c.Email,
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0' 
        ); 
        Database.insert(u1);
        
        User u2 = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Flight Operations Liaison'].Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName,
                Username = c.Email + 'randomrandom',
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0hawaii'
        );
        Database.insert(u2);
        
        AccountTeamMember membership = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=u2.Id,
            AccountId=a.Id
        );
        insert membership; 
        
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
        
        Case cas = new Case();
                cas.Subject = 'TestSubject0';
                cas.Description = 'TestDescription0';
                cas.RecordTypeId = caseRT.Id;
                cas.Customer_Visible__c = Boolean.valueOf('true');
                cas.Status = 'New';
        		cas.ContactId = c.Id;
        		cas.OwnerId = u1.Id;
        		cas.SuppliedEmail = c.Email;
        Database.insert(cas);

        EmailMessage em = new EmailMessage();
                em.ParentId = cas.Id;
                em.TextBody = 'TestEmailBody0';
                em.Subject = 'TestSubject0';
                em.Incoming = true;
                em.FlightOps_Flag__c = true;
                em.Status = '0';
        Database.insert(em);	
        Test.stopTest();  
	}/*
    @isTest static void testCanGetCompanyLiaison(){
        User user = [SELECT Id FROM User Limit 1];
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert acc;
        AccountTeamMember member = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=user.Id,
            AccountId=acc.Id
        );
        insert member;    
        
        User testedUser = FO_CaseTriggerHandler.getCompanyLiaison(acc.Id);   	
        system.assertEquals(user, testedUser, 'Can not get company Liaison');   
    }*/
/*
    @isTest static void testDonotGetLiaisonFromWrongCompany(){
        User testedUser;
        List<User> user = [SELECT Id FROM User Limit 2];
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert acc;
        Account acc2 = new Account(
            Name = 'qwert',
            Company_Nickname__c = 'zxcvbnm'
        );
        insert acc2;
        AccountTeamMember member = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=user[0].Id,
            AccountId=acc.Id
        );
        insert member;    
        AccountTeamMember member2 = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=user[1].Id,
            AccountId=acc2.Id
        );
        insert member2;
        
               
        testedUser = FO_CaseTriggerHandler.getCompanyLiaison(acc.Id);
        

        system.assertEquals(testedUser.Id, user[0].Id, 'Can not get the right company Liaison');     
    }*/
    @isTest static void testSplitstheDomainFromEmail(){
        String emailAdress = 'test@embraer.com.br';
        String emailDomain = FO_CaseTriggerHandler.getEmailDomain(emailAdress);

        system.assertEquals(emailDomain, '@embraer.com.br', 'Can not get the E-mail domain correctly');
        
    }
    @isTest static void testGetCompanyFromEmailDomain(){
        String emailDomain = '@embraer.com.br';
        Account acc = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec',
            FlightOps_Account_Domains__c = '@embraer.com.br'
        );
        insert acc;
        
        
		Id [] fixedSearchResults = new Id[]{acc.Id};        
        Test.setFixedSearchResults(fixedSearchResults);
        
        
        
        Account testedAccount = FO_CaseTriggerHandler.getCompanyAccountByEmailDomain(emailDomain);
        
        system.assertEquals(acc.Id, testedAccount.Id, 'Can not get the right company by the e-mail domain');

    }/*
    @isTest static void testCanSetOwnerForTheCaseWithAccount(){
        User liaison  = [SELECT Id FROM User LIMIT 1];
        Account companyAccount = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec'
        );
        insert companyAccount;
        
        AccountTeamMember membership = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=liaison.Id,
            AccountId=companyAccount.Id
        );
        insert membership; 
        
        Case c1 = new Case(
        	AccountId=companyAccount.Id  
        );
        insert c1;
        List<Case> ownedCases = FO_CaseTriggerHandler.setOwner(new List<Case>{c1});
        
        
        system.assertEquals(c1.Id, ownedCases[0].Id, '');
        
    }*/
    /*
    @isTest static void testCanSetOwnerForTheCaseWithEmail(){
        User liaison  = [SELECT Id FROM User LIMIT 1];
        Account companyAccount = new Account(
            Name = '1234',
            Company_Nickname__c = 'qwec',
            FlightOps_Account_Domains__c = '@test.com.br'
        );
        insert companyAccount;
        
        AccountTeamMember membership = new AccountTeamMember(
            TeamMemberRole='Flight Ops Liaison',
        	UserId=liaison.Id,
            AccountId=companyAccount.Id
        );
        insert membership; 
        
        Case c1 = new Case(
        	AccountId=companyAccount.Id,
            SuppliedEmail = 'test@test.com.br'
        );
        insert c1;
        
        List<Case> ownedCases = FO_CaseTriggerHandler.setOwner(new List<Case>{c1});
              
        
        system.assertEquals(c1.Id, ownedCases[0].Id, '');
        
    }*/
    
}