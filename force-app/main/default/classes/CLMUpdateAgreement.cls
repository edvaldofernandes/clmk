/**
* @author Marcilio Leite de Souza
* @date 01/04/2019
* @description: Update objects related with agreement
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           21 FEV 2018             Original Version
**/
public with sharing class CLMUpdateAgreement {
    
    private List<Agreement__c> listAgreement {get;set;}
    private Set<Id> setAggId;
    private Set<Id> setAggAirId;
    public static final String OPEN = 'Open';
    
    /**
	* @description : Constructor with a list of agreements
	*/
    public CLMUpdateAgreement(List<Agreement__c> listAgreement) {
        this.listAgreement = listAgreement;
        setAggId = new Set<Id>();
        for (Agreement__c agg : this.listAgreement) {
            setAggId.add(agg.Id);
        }
    }

    /**
	* @description : Update the status of related agreements
    * @param String status : Status value to be set
    * @parem String statusCategory : Status Category to be set
	* @return void : 
	*/
    public void updateAgreementStatus(String status, String statusCategory){
        List<Agreement__c> listUpdateAgg = new List<Agreement__c>();

        for (Agreement__c agg : this.listAgreement) {
            Agreement__c newAgg = new Agreement__c();
            newAgg.Id = agg.Id;
            newAgg.Status__c = status;
            newAgg.Status_Category__c = statusCategory;
            listUpdateAgg.add(newAgg);
        }
      
        if(listUpdateAgg.size() > 0){update listUpdateAgg;}
    }

    /**
	* @description : Update the status of related Agreement Aircrafts
    * @param String status : Status value to be set
    * @param String notStatus : exclude agreement aircraft with the status
	* @return void 
	*/
    public void updateAgreementAircraftStatus(String status, Set<String> notStatus){
        List<Agreement_Aircraft__c> listUpdateAggAircraft = new List<Agreement_Aircraft__c>();
        setAggAirId = new Set<Id>();

        for (Agreement_Aircraft__c aggAir : [SELECT Status__c
                                               FROM Agreement_Aircraft__c
                                              WHERE Agreement__c IN :this.setAggId
                                                AND Status__c NOT IN :notStatus]) {
            Agreement_Aircraft__c newAggAir = new Agreement_Aircraft__c();
            newAggAir.Id = aggAir.Id;
            newAggAir.Status__c = status;
            newAggAir.Termination_Date__c = status == CLMEndAgreement.TERMINATED ? Date.today() : null;    
            newAggAir.Expiration_Date__c = status == CLMEndAgreement.EXPIRED ? Date.today() : null; 
                   
            listUpdateAggAircraft.add(newAggAir);

            setAggAirId.add(aggAir.Id);
        }

        if(listUpdateAggAircraft.size() > 0){update listUpdateAggAircraft;}
    }

    /**
	* @description : Update the status of related Agreement Aircrafts Payments
    * @param String status : Status value to be set
    * @param String notStatus : exclude agreement aircraft  payment with the status
	* @return void 
	*/
    public void updateAgreementAircraftPDPStatus(String status, String notStatus){
        List<Aircraft_PDP__c> listUpdateAggAirPDP = new List<Aircraft_PDP__c>();

        for (Aircraft_PDP__c aggAir : [SELECT Status_L__c
                                         FROM Aircraft_PDP__c
                                        WHERE Contract_Aircraft__c IN :this.setAggAirId
                                          AND Status_L__c != :notStatus]) {
            Aircraft_PDP__c newAggAirPDP = new Aircraft_PDP__c();
            newAggAirPDP.Id = aggAir.Id;
            newAggAirPDP.Status_L__c = status;
            listUpdateAggAirPDP.add(newAggAirPDP);
        }

        if(listUpdateAggAirPDP.size() > 0){update listUpdateAggAirPDP;}
    }

     /**
	* @description : Update the status of SOB Events Related
    * @param String status : Status value to be set
	* @return void 
	*/
    public void updateSOBEventStatus(String status){
        List<SOB_Event__c> listUpdateSOBEvent = new List<SOB_Event__c>();
        
        for (SOB_Event__c sobEvt : [SELECT SOB_Status__c
                                      FROM SOB_Event__c
                                     WHERE Commercial_Agreement__c IN :this.setAggId
                                       AND Contract_Aircraft__c IN :this.setAggAirId]){
            SOB_Event__c newSOBEvt = new SOB_Event__c();
            newSOBEvt.Id = sobEvt.Id;
            newSOBEvt.SOB_Status__c = status;
            listUpdateSOBEvent.add(newSOBEvt);
        }

        if(listUpdateSOBEvent.size() > 0){update listUpdateSOBEvent;}
    }

    /**
	* @description : Delete task related to the agreements
	* @return void 
	*/
    public void deleteAgreementTasks(){
        List<Task> listDeleteTask = new List<Task>();

        for (TaskRelation task : [SELECT TaskId
                                    FROM TaskRelation
                                   WHERE (RelationId IN :this.setAggId OR RelationId IN :this.setAggAirId)
                                     AND (Task.Status = :OPEN OR Task.Status = NULL)]) {
            Task delTask = new Task();
            delTask.Id = task.TaskId;
            listDeleteTask.add(delTask);
        }

         if(listDeleteTask.size() > 0){delete listDeleteTask;}
    }
}