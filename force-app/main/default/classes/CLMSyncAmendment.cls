/*
    * @description Synchronizes information from the last active agreement(parent agreement) to the new one. 
    * Update Aircraft and Escalation information. 
    * Get a copy of Checklists and SobEvents.
    * Move all open tasks.
    * Update Agreement Status.
 */

public class CLMSyncAmendment {
    
    private static List<String> requiredFields = new List<String>{
			'Actual_Delivery_Date__c','Status__c','Expiration_Date__c', 'Exercised_Date__c', 'Option_Execution_Date__c', 'Option_Expiration_Date__c', 'PRA_Execution_Date__c', 
            'PRA_Expiration_Date__c', 'Take_off_Date_Time__c', 'Discount_or_Increase__c', 'Escalation_Code__r.Escalation_Code__c', 'Escalation_Code__r.Id',
            'Escalation_Code__r.EscalationExternalID__c', 'Escalation_Code__r.Escalation_Rate__c', 'Escalation_Code__r.Rate_Effective_Date__c', 
            'Escalation_Code__r.RateMonthYearReference__c', 'Scheduled_Inspection_Date__c', 'MSN__c', 'Registration_Mark__c', 'Temporary_Brazilian_Registration__c', 
            'Left_Engine__c', 'Right_Engine__c', 'APU__c', 'Bill_to_Kit__c', 'Bill_to_Aircraft__c', 'MFIR_Bill_to_Aircraft__c', 
            'MFIR_Bill_to_Kit__c', 'MFIR_Ship_to_Aircraft__c', 'MFIR_Ship_to_Kit__c', 'Ship_to_Aircraft__c', 
            'Ship_to_Kit__c', 'RE__c', 'RC__c', 'Customs_Clearance_At__c', 'Airport_of_Destination__c', 'Comments_Delivery_Details__c', /*'SAP_Contract__c', */'SAP_Item__c', 
            'SAP_Batch__c', 'Estimated_Closing_Date__c', 'Material_Code__c', 'PEP_Aircraft__c', 'PEP_Delivery__c', 'PEP_Preservation__c', 'PEP_Warranty__c', 'Invoice__c', 
            'Invoice_Interest_Amount__c', 'Invoice_No_Charge__c', 'Invoice_PDP_Return__c', 'No_Charge_Amount__c', 'Ato_Concessorio__c', 'Data_do_Ato_Concessorio__c', 
            'Aircraft_Delivery__c', 'Outbound_Delivery__c', 'Interest_Amount__c', 'PDP_Return__c',  
            'Delivery_SEC_Score__c', 'Production_SEC_Score__c', 'MFA_Score__c'
 	};
	        
    private static List<String> escalationRequiredFields = new List<String>{
			'Escalation_Code__c', 'EscalationExternalID__c', 'Escalation_Rate__c', 'Rate_Effective_Date__c'
 	};  
    
    public static String syncAmendment(String agreementId){
        String agreementList;
        agreementList =  'SELECT ' + 'id, Parent_Agreement__c, Parent_Agreement__r.id' + /*Utils.getAllFields('Agreement__c')  +*/
            ' FROM Agreement__c' +
            ' WHERE id = :agreementId';
        Agreement__c actualAgreement = database.query(agreementList);
        
        List<Id> signatureNewObjects = new List<Id>();
        Map<String, SObjectField> aircraftFields = Schema.getGlobalDescribe().get('Agreement_Aircraft__c').getDescribe().fields.getMap();
        Map<String, SObjectField> escalationFields = Schema.getGlobalDescribe().get('Escalation__c').getDescribe().fields.getMap();
        
        List<Agreement_Aircraft__c> aircraftList = getAircraft(agreementId);
        
        List<Escalation__c> escalationList = getEscalations(agreementId);
        
        updateAmendedAircrafts(aircraftFields, aircraftList, escalationFields, escalationList);
        
        signatureNewObjects.addAll(processCloneChecklist(actualAgreement.Parent_Agreement__c,agreementId,aircraftList));
        
        updateSupersededAgreement(String.valueOf(actualAgreement.Parent_Agreement__r.Id), agreementId);
        
        moveTasks(actualAgreement.Parent_Agreement__c,agreementId, aircraftList);
        
        signatureNewObjects.addAll(processCloneSOB(actualAgreement.Parent_Agreement__c, actualAgreement.Id, aircraftList));
        
        return String.join(signatureNewObjects,',');
    }
    
    private static List<Agreement_Aircraft__c> getAircraft(String agreementId){
        String aircraftList =  'SELECT id,';
        String lastField;
        for(String acfield : requiredFields){
            aircraftList = aircraftList + acfield + ', ';
            lastField = acfield;
        }
        aircraftList = aircraftList + 'Related_Agreement_Aircraft__r.Id, ';
        for(String acfield : requiredFields){
            aircraftList = aircraftList + 'Related_Agreement_Aircraft__r.' + acfield;
            if(acfield!=lastField)
            	aircraftList = aircraftList + ', ';
        }
        aircraftList = aircraftList + ' FROM Agreement_Aircraft__c WHERE Agreement__c = :agreementId';
      
        List<Agreement_Aircraft__c> aircraftReturnList = database.query(aircraftList);
        return aircraftReturnList;
    }
    
    private static List<Escalation__c> getEscalations(String agreementId){
        String escalationList =  'SELECT id,';
        String lastField;
        for(String acfield : escalationRequiredFields){
            escalationList = escalationList + acfield + ', ';
            lastField = acfield;
        }
        for(String acfield : escalationRequiredFields){
            escalationList = escalationList + 'Related_Escalation__r.' + acfield;
            if(acfield!=lastField)
            	escalationList = escalationList + ', ';
        }
        escalationList = escalationList + ' FROM Escalation__c WHERE Commercial_Agreement__r.id = :agreementId' ;
        
        List<Escalation__c> escalationReturnList = database.query(escalationList);
        return escalationReturnList;
    }
    
    private static void updateAmendedAircrafts(Map<String, SObjectField> aircraftFields, List<Agreement_Aircraft__c> aircraftList, Map<String, SObjectField> escalationFields, List<Escalation__c> escalationList){
        for(Agreement_Aircraft__c agac : aircraftList){
            system.debug('Id' + agac.Id);
            if(agac.Related_Agreement_Aircraft__c==null)
            	continue;
            for(String acField : requiredFields){
                if(aircraftFields.get(acField)!=null){
                    if(agac.Related_Agreement_Aircraft__r.get(aircraftFields.get(acField))!=null){
                		system.debug('aircraftFields.get(acField) ' + aircraftFields.get(acField));
                		system.debug('agac.Related_Agreement_Aircraft__r.get(aircraftFields.get(acField)) ' + agac.Related_Agreement_Aircraft__r.get(aircraftFields.get(acField)));
                		agac.put(aircraftFields.get(acField), agac.Related_Agreement_Aircraft__r.get(aircraftFields.get(acField)));
                    }
                }
            }
        }
        if(!aircraftList.isEmpty())
        	update aircraftList;
        
        if (!escalationList.isEmpty()){
            Object relatedEscField;
            SObjectField relativeRelatedEscField;
            for(Escalation__c esc : escalationList){
                system.debug('esc.Escalation_Code__c' + esc.Escalation_Code__c + 'esc.Related_Escalation__r.' + esc.Related_Escalation__r.Id);
                for(String escField : escalationRequiredFields){
                    relativeRelatedEscField = escalationFields.get(escField);
                    if(relativeRelatedEscField!=null && esc.Related_Escalation__c != null){
                        //system.debug('aircraftFields.get(escField)' + escalationFields.get(escField));
                        //system.debug('aircraftFields.get(escField)' + esc.Related_Escalation__r.get(escalationFields.get(escField)));
                        system.debug('NULLOBJECT?>>' + escField);
                        relatedEscField = esc.Related_Escalation__r.get(relativeRelatedEscField);
                        if(relatedEscField!=null){
                            esc.put(relativeRelatedEscField, relatedEscField);
                            //system.debug('aircraftFields.get(escField)' + relativeRelatedEscField);
                        }
                    }
                }
            }
            update escalationList;
        }        
    }

    private static Set<Id> processCloneChecklist(Id originalAgreementId, Id clonedAgreementId, List<Agreement_Aircraft__c> aircraftList){
        Set<Id> allIds = new Set<Id>();
        Map<Id,Agreement_Aircraft__c> mapOldToNewAircraft = new Map<Id,Agreement_Aircraft__c>();
        Agreement_Aircraft__c actualAircraft;
        Id acId;
        for(Agreement_Aircraft__c ac : aircraftList){
            acId = ac.Related_Agreement_Aircraft__r.Id;
            if(acId!=null){
                mapOldToNewAircraft.put(acId, ac);
                allIds.add(acId);
            }
        }
        //get all aircraft price by agreement
        String checklistQuery =  'SELECT RecordType.Name, Contract_Aircraft__r.Agreement__r.Id, ' + Utils.getAllFields('Questionnaire__c') + 
            ' FROM Questionnaire__c' +
            ' WHERE Commercial_Agreement__c = :originalAgreementId OR Contract_Aircraft__c IN :allIds';
        List<Questionnaire__c> listChecklistToClone = database.query(checklistQuery);
        
        //process to clone all aircrafts prices selected
        List<Questionnaire__c> listClonedChecklist = new List<Questionnaire__c>();
        for(Questionnaire__c checklist : listChecklistToClone){
            Questionnaire__c checklistCloned = checklist.clone(false, false, false, false);            
            //but now we set the new aircraft pric to the cloned agreement
            if(checklist.Contract_Aircraft__c != null){
                actualAircraft = mapOldToNewAircraft.get(checklist.Contract_Aircraft__c);
                if(actualAircraft!=null)
                	checklistCloned.Contract_Aircraft__c = actualAircraft.Id;
            }
            if(checklist.Commercial_Agreement__c != null){
                checklistCloned.Commercial_Agreement__c = clonedAgreementId;
            }
            listClonedChecklist.add(checklistCloned);
        }        
        database.insert(listClonedChecklist);
        return new Map<Id,SObject>(listClonedChecklist).keySet();
    }
    
    private static void updateSupersededAgreement(String parentId, String agreementId){
        String agreementList;
        agreementList =  'SELECT ' + 'id, Status__c, Status_Category__c' + /*Utils.getAllFields('Agreement__c')  +*/
            ' FROM Agreement__c' +
            ' WHERE id = :parentId';
        Agreement__c actualAgreement = database.query(agreementList);
        
        actualAgreement.Status__c = 'Amended';
        actualAgreement.Status_Category__c = 'Superseded';
        actualAgreement.Child_Agreement__c = agreementId;
        
        database.update(new List<Agreement__c>{actualAgreement});
    }
    
    private static void moveTasks(Id originalAgreementId, Id clonedAgreementId, List<Agreement_Aircraft__c> aircraftList){
        Set<Id> allIds = new Set<Id>();
        Map<Id,Id> mapOldToNewAircraft = new Map<Id,Id>();
        Id acId;
        for(Agreement_Aircraft__c ac : aircraftList){
            acId = ac.Related_Agreement_Aircraft__r.Id;
            if(acId!=null){
                mapOldToNewAircraft.put(acId, ac.Id);
                //system.debug('ac.Related_Agreement_Aircraft__r.Id' + acId);
                allIds.add(acId);
            }
        }
        allIds.add(originalAgreementId);
        String taskList = 'SELECT ' + 'id, WhatId, Status' +
            ' FROM Task' +
            ' WHERE WhatId IN :allIds AND Status = \'Open\'';
        List<Task> taskToBeMoved = database.query(taskList);
        System.debug('taskToBeMoved.size() ' + taskToBeMoved.size());
        Map<Id,Id> aircraftIdMap = new Map<Id,Id>();
        if(taskToBeMoved.size()>0){
            for(Task tsk : taskToBeMoved){
                system.debug('tsk.id' + tsk.id);
                if(tsk.WhatId == Id.valueOf(originalAgreementId))
                    tsk.WhatId = Id.valueOf(clonedAgreementId);
                else{
                    tsk.WhatId = mapOldToNewAircraft.get(tsk.WhatId);
                }
            }
        }
        if(taskToBeMoved.size()>0)
        	database.update(taskToBeMoved);
        
    }
    
    private static Set<Id> processCloneSOB(Id originalAgreementId, Id clonedAgreementId, List<Agreement_Aircraft__c> aircraftList){
        
        Set<Id> aircraftIds = new Set<Id>();//(new Map<Id,Agreement_Aircraft__c>(aircraftList)).keySet();
        Map<Id,Id> mapOldToNewAircraft = new Map<Id,Id>();
        Id acId;
        for(Agreement_Aircraft__c ac : aircraftList){
            acId = ac.Related_Agreement_Aircraft__r.Id;
            if(acId!=null){
                mapOldToNewAircraft.put(acId, ac.Id);
            	aircraftIds.add(acId);
            }
        }
        //get all SOB Events
        String sobQuery =  'SELECT ' + Utils.getAllFields('SOB_Event__c') + 
            ' FROM SOB_Event__c' +
            ' WHERE Contract_Aircraft__c IN :aircraftIds';
        List<SOB_Event__c> listSOBToClone = database.query(sobQuery);
        //system.debug('SOBCOUNT' + listSOBToClone.size());
        //process to clone all SOB Events selected
        List<SOB_Event__c> listClonedSOB = new List<SOB_Event__c>();
        for(SOB_Event__c sobEvent : listSOBToClone){
            
            SOB_Event__c sobCloned = sobEvent.clone(false, false, false, false);            
            //but now we set the new aircraft pric to the cloned agreement
            sobCloned.Contract_Aircraft__c = mapOldToNewAircraft.get(sobEvent.Contract_Aircraft__c);
            sobCloned.Commercial_Agreement__c = clonedAgreementId;
            listClonedSOB.add(sobCloned);
        }        
        database.insert(listClonedSOB);
        return new Map<Id,SObject>(listClonedSOB).keySet();
    }

}