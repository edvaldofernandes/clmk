global class CRM360_MonthlyUpdateUnchecker implements Schedulable{
    
    global void execute(SchedulableContext context){
        List<Account_Cockpit__c> accountCockpits = [SELECT Id, Is_Updated__c, Year__c
                                                    FROM Account_Cockpit__c 
                                                    WHERE Record_Type_ID_ref__c = 'CMR360_Dashboard_Data'
                                                    AND Is_Record__c = false];
        for(Account_Cockpit__c accountCockpit : accountCockpits){
            accountCockpit.Is_Updated__c = false;
            accountCockpit.Year__c = String.valueOf(Date.today().year());
        }
        update accountCockpits;
    }

}