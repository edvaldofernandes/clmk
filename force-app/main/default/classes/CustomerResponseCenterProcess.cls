/* 
----------------------------------------------------------------------------------------------
-- - Name:        CustomerReponseCenterProcess
-- - Description: Execute the process regarding CRC, routing and configuring cases (Email to case)
-- - @Author: Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               Version                   
----------------------------------------------------------------------------------------------
-- 03/12/2019       Felipe Gouvea      1.0 
-- 07/11/2019       Andre Leinio       2.0
----------------------------------------------------------------------------------------------
*/

public with sharing class CustomerResponseCenterProcess {

	public List<Case> contextCases {get; set;}

    //public Map<Id,  EmailMessage> mapping {get; set;}
    
    public List<Email_to_Case_Setting__mdt> settings {get; set;}
    
    public CustomerResponseCenterProcess(List<Case> cases){
		contextCases = cases;
        //mapping = new Map<Id, EmailMessage>();
        settings = [SELECT MasterLabel, Case_Reason__c, Priority__c, Senders__c, Type__c, Case_Origin__c FROM Email_to_Case_Setting__mdt];
   		for(Email_to_Case_Setting__mdt setting : settings)
      		system.debug('inputed settings >> ' + setting );
    }

    
	public void start(){
		//retrieveRelatedEmail();
        configureCases();
        //system.debug('ContextCases after process >>> ' + contextCases);
	}

    /*private void retrieveRelatedEmail(){
        try{
        	List<EmailMessage> emails = [SELECT Subject, FromAddress, ParentId FROM EmailMessage WHERE ParentId IN : contextCases];
        	for(EmailMessage message : emails){
            	mapping.put(message.ParentId, message);
        	}
        }catch(Exception e){
            System.debug('Exception on class CustomerResponseCenterProcess, method  retrieveRelatedEmail >>>' + e.getMessage());
        }
    }*/

    private void configureCases(){
        for(Case c : contextCases){
            //if(mapping.containsKey(c.Id)){
                // mail = mapping.get(c.Id);
                //adjustWithRule(mail, c);
                adjustWithRule(c);
            //}
        }
    }
    
    private void adjustWithRule(Case contextCase){
        
        if(contextCase.SuppliedEmail != null){
            for(Email_to_Case_Setting__mdt setting : settings){
            
                if(!String.isEmpty(contextCase.Subject) && contextCase.Subject.containsIgnoreCase(setting.MasterLabel) && setting.Senders__c.containsIgnoreCase(contextCase.SuppliedEmail)){
                    //system.debug('rule applied to case >> ' + contextCase);
                    contextCase.Priority = setting.Priority__c;
                    contextCase.Type = setting.Type__c;
                    contextCase.Reason = setting.Case_Reason__c;   
                    contextCase.Origin = setting.Case_Origin__c;
                }
            }
        }
    }
    /*private void adjustWithRule(EmailMessage mail, Case contextCase){
        for(Email_to_Case_Setting__mdt setting : settings){
            system.debug('Email >> ' + mail);
            if(mail.Subject.containsIgnoreCase(setting.MasterLabel) && setting.Senders__c.containsIgnoreCase(mail.FromAddress)){
                system.debug('rule applied to case >> ' + contextCase);
                contextCase.Priority = setting.Priority__c;
                contextCase.Type = setting.Type__c;
                contextCase.Reason = setting.Case_Reason__c;   
  				contextCase.Origin = setting.Case_Origin__c;
            }
        }
    }*/

}