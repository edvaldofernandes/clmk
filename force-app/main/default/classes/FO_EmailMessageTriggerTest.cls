@isTest
public class FO_EmailMessageTriggerTest {
    
    @isTest static void testDML(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        EmailMessage[] testEmail = FO_MyOP_TestDataFactory.createCaseEmail(testCase, 2);
        Database.insert(testEmail);  
        Database.update(testEmail[0]);
        Database.delete(testEmail[0]);
        Database.undelete(testEmail[0]);
        Test.stopTest();  
    }
    
    @isTest static void testCaseLastEmailUpdate(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 4);
        Database.insert(testCase);
        
        //Creating email
        Integer numEmailPerCase = 2;
        List<EmailMessage> testEmail = new List<EmailMessage>();
        Integer numTotalCases = testCase.size();
        for(Integer y=0;y<numTotalCases;y++) {
            Case c = testCase[y];
            for(Integer x=numEmailPerCase*y;x<numEmailPerCase*(y+1);x++){
                EmailMessage em = new EmailMessage();
                em.ParentId = c.Id;
                em.TextBody = 'TestEmailBody'+x;
                em.Subject = 'TestSubject'+x;
                em.Incoming = true;
                em.FlightOps_Flag__c = true;
                testEmail.add(em);
            }
        }
        Database.insert(testEmail);
        Test.stopTest();  
    }
    
    @isTest static void testCaseLastEmailUpdateStatusZero(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
		Database.insert(testUser);
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
        Case cas = new Case();
                cas.Subject = 'TestSubject0';
                cas.Description = 'TestDescription0';
                cas.AccountId = testContact[0].AccountId;
                cas.RecordTypeId = caseRT.Id;
                cas.Customer_Visible__c = Boolean.valueOf('true');
                cas.Status = 'New';
        		cas.ContactId = testContact[0].Id;
        		cas.OwnerId = testUser[0].Id;
        Database.insert(cas);
        
        //Creating email
        EmailMessage em2 = new EmailMessage();
                em2.ParentId = cas.Id;
        		em2.ToAddress = testContact[0].Email;
                em2.TextBody = 'TestEmailBody0';
                em2.Subject = 'TestSubject0';
                em2.Incoming = False;
                em2.FlightOps_Flag__c = true;
                em2.Status = '3';
        Database.insert(em2);
        
       EmailMessage em = new EmailMessage();
                em.ParentId = cas.Id;
                em.TextBody = 'TestEmailBody0';
                em.Subject = 'TestSubject0';
                em.Incoming = true;
                em.FlightOps_Flag__c = true;
                em.Status = '0';
        Database.insert(em);
        
        Test.stopTest();
    }
    
    @isTest static void testCaseLastEmailUpdateStatusThree(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
        Case cas = new Case();
                cas.Subject = 'TestSubject0';
                cas.Description = 'TestDescription0';
                cas.AccountId = testContact[0].AccountId;
                cas.RecordTypeId = caseRT.Id;
                cas.Customer_Visible__c = Boolean.valueOf('true');
                cas.Status = 'New';
        		cas.ContactId = testContact[0].Id;
        		cas.SuppliedEmail = testContact[0].Email;
        Database.insert(cas);
        //Creating incoming email = true
        EmailMessage em = new EmailMessage();
                em.ParentId = cas.Id;
        		em.ToAddress = testContact[0].Email;
                em.TextBody = 'TestEmailBody0';
                em.Subject = 'TestSubject0';
                em.Incoming = true;
                em.FlightOps_Flag__c = true;
                em.Status = '3';
        Database.insert(em);
        
        Case cas2 = new Case();
                cas2.Subject = 'TestSubject1';
                cas2.Description = 'TestDescription1';
                cas2.AccountId = testContact[0].AccountId;
                cas2.RecordTypeId = caseRT.Id;
                cas2.Customer_Visible__c = Boolean.valueOf('true');
                cas2.Status = 'New';
        		cas2.ContactId = testContact[0].Id;
        		cas2.SuppliedEmail = testContact[0].Email;
        Database.insert(cas2);
        
        //Creating incoming email = true
        EmailMessage em2 = new EmailMessage();
                em2.ParentId = cas2.Id;
        		em2.ToAddress = testContact[0].Email;
                em2.TextBody = 'TestEmailBody0';
                em2.Subject = 'TestSubject0';
                em2.Incoming = False;
                em2.FlightOps_Flag__c = true;
                em2.Status = '3';
        Database.insert(em2);
        Test.stopTest();  
    }
    
    @isTest static void testStatusThreeCaseClosed(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
        Case cas = new Case();
                cas.Subject = 'TestSubject0';
                cas.Description = 'TestDescription0';
                cas.AccountId = testContact[0].AccountId;
                cas.RecordTypeId = caseRT.Id;
                cas.Customer_Visible__c = Boolean.valueOf('true');
                cas.Status = 'Closed';
        		cas.ContactId = testContact[0].Id;
        		cas.SuppliedEmail = testContact[0].Email;
        Database.insert(cas);
        //Creating incoming email = true
        EmailMessage em = new EmailMessage();
                em.ParentId = cas.Id;
        		em.ToAddress = testContact[0].Email;
                em.TextBody = 'TestEmailBody0';
                em.Subject = 'TestSubject0';
                em.Incoming = true;
                em.FlightOps_Flag__c = true;
                em.Status = '3';
        Database.insert(em);
        Test.stopTest();  
    }
    
    @isTest static void testStatusThreeNotContact(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type'];
        Case cas = new Case();
                cas.Subject = 'TestSubject0';
                cas.Description = 'TestDescription0';
                cas.AccountId = testContact[0].AccountId;
                cas.RecordTypeId = caseRT.Id;
                cas.Customer_Visible__c = Boolean.valueOf('true');
                cas.Status = 'New';
        		cas.SuppliedEmail = testContact[0].Email;
        Database.insert(cas);
        //Creating incoming email = true
        EmailMessage em = new EmailMessage();
                em.ParentId = cas.Id;
        		em.ToAddress = testContact[0].Email;
                em.TextBody = 'TestEmailBody0';
                em.Subject = 'TestSubject0';
                em.Incoming = false;
                em.FlightOps_Flag__c = true;
                em.Status = '3';
        Database.insert(em);
        Test.stopTest();  
    }
    
    
}