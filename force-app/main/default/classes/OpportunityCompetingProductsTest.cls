/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the class OpportunityCompetingProducts
*
* NAME: OpportunityCompetingProductsTest.cls
* AUTHOR:JFS                                                DATE: 04/12/2014
*******************************************************************************/

@isTest 
private class  OpportunityCompetingProductsTest {

  private static final id RectypeNoEmbraer = RecordTypeMemory.getRecType('Product2', 'Non_Embraer');
  private static final id RectypeProd = RecordTypeMemory.getRecType('Product2', 'Aircraft_Modification');
  private static final integer LOTE = 200; 
   
  static testMethod void TestFuncional()
  {
    
    Product2 Produto = SObjectInstanceTest.createProduct2();
    Produto.RecordTypeId = RectypeProd;
    Produto.IsActive = true;
    database.insert(Produto);
    
    Product2 ProdutoNoEmbraer = SObjectInstanceTest.createProduct2();
    ProdutoNoEmbraer.Name = 'teste produto nao Embraer';
    ProdutoNoEmbraer.RecordTypeId = RectypeNoEmbraer;
    ProdutoNoEmbraer.ProductCode = 'test12';
    ProdutoNoEmbraer.Embraer_Product__c = Produto.Id;
    ProdutoNoEmbraer.IsActive = true;
    database.insert(ProdutoNoEmbraer);    
    
    id pb2 = SObjectInstanceTest.getPricebook2Std2();
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2, Produto.Id);
    database.insert(pbe);
    
    Opportunity Opp = SObjectInstanceTest.createOpportunity();
    database.insert(Opp);
    
    OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(Opp.Id, pbe.Id);
    
    test.startTest();
    database.insert(Oli);
    test.stopTest();
    
    list < Competing_Product__c > lstComp = [SELECT id FROM Competing_Product__c WHERE Competing_Product__c =:ProdutoNoEmbraer.id and 
    Embraer_Product__c =:Produto.id and Opportunity__c =: Opp.id ];
    
    system.assert(!lstComp.isEmpty(), 'Deveria ter inserido o produto competidor na oportunidade');
    
  }
    
  static testMethod void TestErro()
  {
    
    Product2 Produto = SObjectInstanceTest.createProduct2();
    Produto.RecordTypeId = RectypeProd;
    Produto.IsActive = true;
    database.insert(Produto);
    
    id pb2 = SObjectInstanceTest.getPricebook2Std2();
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2, Produto.Id);
    database.insert(pbe);
    
    Opportunity Opp = SObjectInstanceTest.createOpportunity();
    database.insert(Opp);
    
    OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(Opp.Id, pbe.Id);
    
    test.startTest();
    database.insert(Oli);
    test.stopTest();
    
    list < Competing_Product__c > lstComp = [SELECT id FROM Competing_Product__c WHERE 
    Embraer_Product__c =:Produto.id and Opportunity__c =: Opp.id ];
    
    system.assert(lstComp.isEmpty(), 'Não Deveria ter inserido o produto competidor na oportunidade');
    
  }
    
  static testMethod void TestLote()
  {
    
    list < Product2 > lstProduto = new list < Product2 >();
    
    for(integer i = 0; i < LOTE; i++)
    {
      Product2 Produto = SObjectInstanceTest.createProduct2();
      Produto.RecordTypeId = RectypeProd;
      Produto.IsActive = true;
      Produto.Name = 'Teste produto' + i;
      Produto.ProductCode = 'testx' + i;
      lstProduto.add(Produto);
    }
    
    database.insert(lstProduto);
   
    list < Product2 > lstProdutoNoEmbraer = new list < Product2 >();
    
    for(integer i = 0; i < LOTE; i++)
    {
      Product2 ProdutoNoEmbraer = SObjectInstanceTest.createProduct2();
      ProdutoNoEmbraer.Name = 'teste produto nao Embraer' + i;
      ProdutoNoEmbraer.RecordTypeId = RectypeNoEmbraer;
      ProdutoNoEmbraer.Embraer_Product__c = lstProduto.get(i).id;
      ProdutoNoEmbraer.IsActive = true;
      ProdutoNoEmbraer.ProductCode = 'testz' + i;
      lstProdutoNoEmbraer.add(ProdutoNoEmbraer);
    }
    
    database.insert(lstProdutoNoEmbraer);
    
    id pb2 = SObjectInstanceTest.getPricebook2Std2();
    
    list < PricebookEntry > lstpbe = new list < PricebookEntry >();
    for(integer i = 0; i < LOTE; i++)
    {
    
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2, lstProduto.get(i).id);
      lstpbe.add(pbe);
    }
    
    database.insert(lstpbe);
    
    Opportunity Opp = SObjectInstanceTest.createOpportunity();
    database.insert(Opp);
    
    list < OpportunityLineItem > lstOli = new list < OpportunityLineItem >();
    
    for(integer i = 0; i < LOTE; i++)
    {
    
      OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(Opp.Id, lstpbe.get(i).id);
      lstOli.add(Oli);
    }    
    
    test.startTest();
    database.insert(lstOli);
    test.stopTest();
    
    map <id, Competing_Product__c > MapComp = new map < id, Competing_Product__c >([SELECT id FROM Competing_Product__c WHERE Embraer_Product__c =:lstProduto
    and Opportunity__c =: Opp.id]);
    
    for(Product2 prd: lstProdutoNoEmbraer)
    {
      system.assert(!MapComp.containsKey(prd.id), 'Não Deveria ter inserido o produto competidor na oportunidade');
    }
  }
  
	static testMethod void TesteUnitario()
  	{
    
    	Product2 Produto = SObjectInstanceTest.createProduct2();
    	Produto.RecordTypeId = RectypeProd;
    	Produto.IsActive = true;
    	database.insert(Produto);
    
    	Product2 ProdutoNoEmbraer = SObjectInstanceTest.createProduct2();
    	ProdutoNoEmbraer.Name = 'teste produto nao Embraer';
    	ProdutoNoEmbraer.RecordTypeId = RectypeNoEmbraer;
    	ProdutoNoEmbraer.ProductCode = 'test12';
    	ProdutoNoEmbraer.Embraer_Product__c = Produto.Id;
    	ProdutoNoEmbraer.IsActive = true;
    	database.insert(ProdutoNoEmbraer);    
    
    	id pb2 = SObjectInstanceTest.getPricebook2Std2();
    
    	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2, Produto.Id);
    	database.insert(pbe);
    	
    	PricebookEntry pbe2 = SObjectInstanceTest.createPricebookEntry(pb2, ProdutoNoEmbraer.Id);
    	database.insert(pbe2);
    
    	Opportunity Opp = SObjectInstanceTest.createOpportunity();
    	database.insert(Opp);
    
    	OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(Opp.Id, pbe.Id);
    	opportunityLineItem Oli2 = SObjectInstanceTest.createOppItem(Opp.Id, pbe2.Id);
    	
    	Competing_Product__c Prdcompt = new Competing_Product__c();
        Prdcompt.Competing_Product__c = ProdutoNoEmbraer.Id;
        Prdcompt.Embraer_Product__c = Produto.Id;
        Prdcompt.Opportunity__c = Opp.Id;
        insert Prdcompt;
    	
    	test.startTest();
    	database.insert(new List<OpportunityLineItem>{Oli,Oli2});
    	
    	
    	test.stopTest();
    
    	list < Competing_Product__c > lstComp = [SELECT id FROM Competing_Product__c WHERE Competing_Product__c =:ProdutoNoEmbraer.id and 
    	Embraer_Product__c =:Produto.id and Opportunity__c =: Opp.id ];
    
    	system.assert(!lstComp.isEmpty(), 'Deveria ter inserido o produto competidor na oportunidade');
    
  	}
  
}