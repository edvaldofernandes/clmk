public class DocumentNumberingGenerator {

    public static void setDate(List<Document_Numbering__c> newRecords){
        for (Document_Numbering__c dn: newRecords){
            if(dn.Creation_Date__c == null) dn.Creation_Date__c = system.today();
        }
    }
    
    public static void generate(List<Document_Numbering__c> newRecords){

        // ---- GENERATE DOCUMENT NUMBER, AUTO RUNNING NUMBER, RESET EVERY YEAR BASED ON DOCUMENT DATE
        // ---- INVOICE NUMBER FORMAT : {0000}-{YY}
        // ---- ASSUME THAT DOCUMENT DATE CAN BE BACKDATED

        Integer iCharLen = 4; // character length for number
        Integer iLastNo = 0; // information last running number this year
        String strZero = '0';
        
        // search type of invoice Year, because possible to have trigger for different Year
        String strYearCode = '';
        String strTemp;
        Map<String, Integer> mapYearLast = new Map<String, Integer>(); // map of last number for a Year
        for (Document_Numbering__c dn: newRecords){
        strYearCode = String.ValueOf(dn.Creation_Date__c.year());
        mapYearLast.put(strYearCode, 0); // add default last number
        }
        
        // search latest document number
        List<String> arrStr = new List<String>();
        Set<String> setYearCode = mapYearLast.keyset();
        for (Document_Numbering__c dnItem : [Select Number__c, Creation_Date__c
                                From Document_Numbering__c
                                WHERE IsDeleted = false AND Year__c IN :setYearCode AND Number__c != null
                                ])
        {
            strYearCode = String.valueOf(dnItem.Creation_Date__c.year());
            strTemp = dnItem.Number__c;
            arrStr = strTemp.split('-');
            //This case if area must appear in the document number
            //if (arrStr.size() > 2 && arrStr[1].isNumeric() && Integer.valueOf(arrStr[1])>mapYearLast.get(strYearCode))
            //	mapYearLast.put(strYearCode, Integer.valueOf(arrStr[1])); 
            if (arrStr.size() > 1 && arrStr[0].isNumeric() && Integer.valueOf(arrStr[0])>mapYearLast.get(strYearCode)) 
                mapYearLast.put(strYearCode, Integer.valueOf(arrStr[0]));
        }
        
        // start generate number
        String strNo = '';
        List<Document_Numbering__c> updateList = new List<Document_Numbering__c>();
        
        for (Document_Numbering__c dn: newRecords){
            iLastNo = 0; // init
            strYearCode = String.ValueOf(dn.Creation_Date__c.year());
            if (mapYearLast.containsKey(strYearCode)) iLastNo = mapYearLast.get(strYearCode);
            iLastNo++; // update to next number
            strTemp = String.valueOf(iLastNo);
            if (strTemp.length() < iCharLen) strTemp = strZero.repeat(iCharLen - strTemp.length()) + strTemp; // add 0 prefix
            strNo  = /*dn.Area__c + '-' +*/ strTemp + '-' + String.ValueOf(dn.Creation_Date__c.year()).right(2);
            Document_Numbering__c newDn = new Document_Numbering__c();
            newDn.Id = dn.Id;
            newDn.Number__c = strNo; // update field
            updateList.add(newDn);
            mapYearLast.put(strYearCode, iLastNo); // update map info      
        }
        database.update(updateList);
    }
}