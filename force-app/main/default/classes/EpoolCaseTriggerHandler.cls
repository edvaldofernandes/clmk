public class EpoolCaseTriggerHandler {
    //constants providing the pool and repair admin record type IDs
    private static final Id POOL_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
    private static final Id REPAIR_ADMIN_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair Administ.').getRecordTypeId();
    
    public static void handleNewEpoolCasesAsync(List<Case> newInserts){
        List<Id> newEpoolCases = new List<Id>();
        for(Case newInsert : newInserts){
            if(isEpoolCase(newInsert)){
                newEpoolCases.add(newInsert.Id);
            }
        }
        
        if(!newEpoolCases.isEmpty()){
            populateEpoolCasesAsync(newEpoolCases);	//pass IDs to asynchronous method which will parse these cases in the future
        }
    }
    
    @future
    private static void populateEpoolCasesAsync(List<Id> newEpoolCases){
        populateEpoolCases(newEpoolCases);
    }
    
    public static void populateEpoolCases(List<Id> newEpoolCases){
        Map<string, List<Case>> mfirMap = new Map<string, List<Case>>();	//map will hold all the cases related to a specific mfir
        Map<string, List<Case>> ecodeMap = new Map<string, List<Case>>();	//map will hold all the cases related to an ecode
        
        List<Case> newInserts = [SELECT Id, Subject, Description FROM Case WHERE Id IN :newEpoolCases FOR UPDATE];
        
        for(Case newInsert : newInserts){
            string priority = newInsert.subject.substringBetween('SO:', '-');	//get priority from subject
            if(!String.isBlank(priority)){
                priority = priority.trim();
                newInsert.Priority = (priority == 'AOG') ? 'AOG' : (priority == 'CRI') ? 'Critical' : (priority == 'OSS') ? 'OSS': 'Routine';
            }
            
            string mfir = parseEmailUpToCustomer(newInsert);				//parse first half of email and get back mfir value
            if(!mfirMap.containsKey(mfir)){									
                mfirMap.put(mfir, new List<Case>());
            }
            mfirMap.get(mfir).add(newInsert);								//group cases by their mfir in a map for later access
            
            string ecode = parseEmailToEnd(newInsert);						//parse last half of email and get back ecode value
            if(!ecodeMap.containsKey(ecode)){
                ecodeMap.put(ecode, new List<Case>());
            }
            ecodeMap.get(ecode).add(newInsert);								//group cases by their ecode in a map for later access
            
            newInsert.Bypass_Validation__c = true;							//bypass validation rules which may prevent case from updating
       
        }
        
        //get mfir records matching the mfir numbers we have
        List<MFIR__c> mfirRecords = [SELECT Name, Account__c FROM MFIR__c WHERE Name IN :mfirMap.keySet()];
        
        //assign MFIR to Cases
        //store mfir info in map where key is account Id and value is the account's mfir number, to connect account back to case later
        Map<Id, string> accountsToMfirMap = new Map<Id, string>();				
        for(MFIR__c mfirRecord : mfirRecords){
            for(Case c : mfirMap.get(mfirRecord.Name)){
                c.MFIR__c = mfirRecord.Id;
            }
            accountsToMfirMap.put(mfirRecord.Account__c, mfirRecord.Name);
        }
        
        //get accounts from the mfirs we found
        List<Account> acctsWithContacts = [SELECT Id, (SELECT Id FROM Contacts WHERE Email LIKE 'sepremote%') FROM Account WHERE Id IN :accountsToMfirMap.keySet()];
        
        //assign account and contact to each case
        for(Account acctWithContact : acctsWithContacts){
            for(Case c : mfirMap.get(accountsToMfirMap.get(acctWithContact.Id))){
                c.AccountId = acctWithContact.Id;
                c.ContactId = (!acctWithContact.Contacts.isEmpty()) ? acctWithContact.Contacts[0].Id : null;
            }
        }
        
        //assign part number to each case
        List<LLPDatabase__c> parts = [SELECT Id, Ecode__c FROM LLPDatabase__c WHERE Ecode__c IN :ecodeMap.keySet()];
        
        for(LLPDatabase__c part : parts){
            for(Case c : ecodeMap.get(part.Ecode__c)){
                c.Part_Number__c = part.Id;
            }
        }
        
        database.update(newInserts, false);
    }
    
    //parses a structured email from Epool to initialize some case fields
    private static string parseEmailUpToCustomer(Case caseToInit){
        //get the case description (this is the email body)
        string description = caseToInit.Description;
        
        //get PO number
        string poNumber = description.substringBetween('Reference:', '*');
        if(!String.isBlank(poNumber)){
            poNumber = poNumber.trim();
            description = description.substringAfter(poNumber);
            caseToInit.PO__c = poNumber;
        }
        
        //get sales order number
        string soNumber = description.substringBetween('Exchange:', '*');
        if(!String.isBlank(soNumber)){
            soNumber = string.valueOf(integer.valueOf(soNumber.trim()));	//remove leading zeroes
            description = description.substringAfter(soNumber);
            caseToInit.SO__c = soNumber;
        }
        String PartContracted ='Yes';
        if(description != '')
        {
            caseToInit.Part_Contracted__c = PartContracted;
        }
        
        //get notification
        string notification = description.substringBetween('Notification:', '*');
        if(!String.isBlank(notification)){
            notification = string.valueOf(integer.valueOf(notification.trim()));	//remove leading zeroes     
            description = description.remove(description.substringBefore('Customer Number:'));
            caseToInit.Notification__c = notification;
        }
        
        //get mfir
        string mfir = description.substringBetween('Customer Number:', '*');
        if(!String.isBlank(mfir)){
            return string.valueOf(integer.valueOf(mfir.trim()));
        }
        return '';	
    }
    
    private static string parseEmailToEnd(Case caseToInit){        
        //get ecode from line 010100 if line 010200 does not exist
        if(!caseToInit.Description.contains('Line Number: 010200')){
            string ecode = caseToInit.Description.substringBetween('Emb.Code:', '*');
            return (!String.isBlank(ecode)) ? string.valueOf(integer.valueOf(ecode.trim())) : '';
        }
        
        //get ecode from line 010200
        string description = caseToInit.Description.substringAfter('Line Number: 010200');
        string ecode = description.substringBetween('Emb.Code:', '*');
        ecode = (!String.isBlank(ecode)) ? string.valueOf(integer.valueOf(ecode.trim())) : '';
        description = description.substringAfter('(yyyy/mm/dd):');
        
        //get need by date for routine cases
        if(casetoInit.priority == 'Routine'){
            string needByDate = description.substringBefore('*');
            if(!String.isBlank(needByDate)){
                needByDate = needByDate.trim();
                integer year = integer.valueOf(needByDate.left(4));
                integer month = integer.valueOf(needByDate.right(4).left(2));
                integer day = integer.valueOf(needByDate.right(2));
                caseToInit.Need_by_Date__c = Date.newInstance(year, month, day);
            }
        }
        
        //get OSS consumed Serial Number... DEPRECATED
        /*if(description.contains('OSS')){
            string serialNum = description.substringBetween('SN', '.');
            if(!String.isBlank(serialNum)){
                serialNum = serialNum.trim();
                caseToInit.OSS_Consumed_Serial_Number__c = serialNum;
            }
        }*/
        return ecode;
    }
			  
    //returns true if case record type is Pool Exchange or Repair Admin and Subject contains "NEW SO:"
    public static boolean isEpoolCase(Case newInsert){
        return (newInsert.RecordTypeId == POOL_ID || newInsert.RecordTypeId == REPAIR_ADMIN_ID) 
            && !String.isBlank(newInsert.Subject)
            && newInsert.Subject.contains('NEW SO:')
            && !String.isBlank(newInsert.Description);
    }
}