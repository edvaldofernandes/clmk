@isTest(seeAllData=true)
public class RcpOutBoundAccountServiceTest {
    
     @isTest static void accountInformationAndHistory(){
        
        //try{
            
            Test.startTest();
        	Account account = new Account();
        	account.Name = 'Test account rcp';
        	account.Company_Nickname__c = 'tt account';
        	account.Company_Status__c = 'Active';
        	INSERT account;
        
        	AccountInformation accountInformation = new AccountInformation(account, 'NEW OBJECT', 'NEW');
        	AccountHistoryInformation accountHistoryInformation = new AccountHistoryInformation(Datetime.now(), 'field', 'OldValue', 'newValue', 'createdBy');
        
        	List<AccountInformation> accountInformationList = new List<AccountInformation>();
        	accountInformationList.add(accountInformation);
        
        	List<AccountHistoryInformation> accountHistoryInformationList = new List<AccountHistoryInformation>();
        	accountHistoryInformationList.add(accountHistoryInformation);
        
        	Visitator visitor = new CallOutRepository();
        	visitor.visit(accountInformationList, accountHistoryInformationList, 'AccountInformation');
        
        	RcpOutBoundAccountService rcpAccountService = new RcpOutBoundAccountService();
        	
            List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount> accountList = rcpAccountService.buildRcpAccount();
        
            Test.setMock(WebServiceMock.class, new RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(null));
            
        	for(RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount sfAccount : accountList)
            	System.assertEquals(sfAccount.companyName, 'Test account rcp');
            
            RcpOutBoundProxy.executeAccount();
            
            Test.stopTest();
        
        //}catch(TypeException e){}
    }
    
    @isTest static void aircraftInformationAndHistory(){
        
        
       // try{
            
            Test.startTest();
        
            Account account = new Account();
        	account.Name = 'Test aircraftInformation';
     		account.Company_Nickname__c = 'tt account';
        	account.Company_Status__c = 'Active';
        	account.FlyEmbraerId__c = 'TEST 123456';
        	insert account;
        
        	Aircraft__c aircraft = new Aircraft__c ();
     		aircraft.Model_Type__c = 'TEST 190';
        	aircraft.name = '19000111';
            aircraft.Owner__c = account.Id;
            aircraft.Aircraft_Status__c = 'In Service';
            aircraft.Commercial_Name__c = 'EMB-110P1';
        	insert aircraft;  
        
            AircraftInformation aircraftINformation = new AircraftInformation(aircraft,account,
                                                                              Constants.NEW_OBJECT_CREATED, 
                                                                			  Constants.STATUS_ACTIVE);
            
            AircraftHistoryInformation aircraftHistoryInformation = new AircraftHistoryInformation(DateTime.now(), 
                                                                                                   'test field', 
                                                                                                   'test OldValue', 
                                                                                                   'test newValue',
                                                                                                   'test createdBy');
            
            List<AircraftInformation> aircraftINformationList = new List<AircraftInformation>();
            aircraftINformationList.add(aircraftINformation);
            
            List<AircraftHistoryInformation> aircraftHistoryInformationList = new List<AircraftHistoryInformation>();
            aircraftHistoryInformationList.add(aircraftHistoryInformation);
            
            Visitator visitor = new CallOutRepository();
        	visitor.visit(aircraftINformationList, aircraftHistoryInformationList, 'AircraftInformation');
            
            RcpOutBoundAircraftService rcpAircraftService = new RcpOutBoundAircraftService();
            List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft> sfInfoAircraftList = rcpAircraftService.buildRcpAircraft();
            
            Test.setMock(WebServiceMock.class, new RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(null));
            
            for(RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft sfAircraft : sfInfoAircraftList)
                System.assertEquals(sfAircraft.modelType, 'TEST 190');
            
            RcpOutBoundProxy.executeAircraft();
            
            Test.stopTest();
                
        //}catch(TypeException e){}    
    }
    
    @isTest static void accountTeamMemberTest(){
        
        //try{
            
            Test.startTest();
        
        	Account account = new Account();
        	account.Name = 'accountTeamMemberTest';
        	account.Company_Nickname__c = 'tt account';
        	account.Company_Status__c = 'Active';
        	insert account;
        
        	Profile p = [select id from Profile where name='System Administrator'];
        
        	User user = new User(alias = 'utest1', email='Unit1.Test@unittest.com',emailencodingkey='UTF-8', 
                                 firstName='First1', lastname='Last1', languagelocalekey='en_US', localesidkey='en_US', 
                          		 profileid = p.id, timezonesidkey='Europe/London', username='Unit1.Test@unittest.com');
        	insert user;
        
        	AccountTeamMember accountTeamMember = new AccountTeamMember();
        	accountTeamMember.UserId = user.Id;
        	accountTeamMember.AccountId = account.Id;
       		accountTeamMember.teamMemberRole = 'Role_test';
        
        	insert accountTeamMember;
        
        	List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
        	accountTeamMemberList.add(accountTeamMember);
        
        	Test.setMock(WebServiceMock.class, new RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(null));
        	
        	RcpOutBoundAccountService.buildRcpTeamMember(accountTeamMemberList);
        
        	for(AccountTeamMember teamMember : accountTeamMemberList)
            	System.assertEquals(teamMember.UserId, user.Id);
        
            RcpOutBoundProxy.executeTeamMemberInserted();
            
        	Test.stopTest();
            
        //}catch(TypeException e){}
    }
    
     @isTest static void accountTeamMemberTestUpdate(){
        
        //try{
            
            Test.startTest();
        
        	Account account = new Account();
        	account.Name = 'accountTeamMemberTest';
        	account.Company_Nickname__c = 'tt account';
        	account.Company_Status__c = 'Active';
        	insert account;
        
        	Profile p = [select id from Profile where name='System Administrator'];
        
        	User user = new User(alias = 'utest2', email='Unit2.Test@unittest.com',emailencodingkey='UTF-8', 
                                 firstName='First2', lastname='Last2', languagelocalekey='en_US', localesidkey='en_US', 
                          		 profileid = p.id, timezonesidkey='Europe/London', username='Unit2.Test@unittest.com');
        	insert user;
        
        	AccountTeamMember accountTeamMember = new AccountTeamMember();
        	accountTeamMember.UserId = user.Id;
        	accountTeamMember.AccountId = account.Id;
       		accountTeamMember.teamMemberRole = 'Role_test';
        
        	insert accountTeamMember;
        
        	List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
        	accountTeamMemberList.add(accountTeamMember);
        
        	//SalesForceToEtrackAccountService.buildTeamMemberUpdatedOrInserted(accountTeamMemberList, Constants.NEW_OBJECT_CREATED);
        
        	for(AccountTeamMember teamMember : accountTeamMemberList)
            	System.assertEquals(teamMember.UserId, user.Id);
                    
            accountTeamMember.teamMemberRole = 'Role_test_update';
            update accountTeamMember;
            
            List<AccountTeamMember> accountTeamMemberListUpdate = new List<AccountTeamMember>();
            accountTeamMemberListUpdate.add(accountTeamMember);
            
            Test.setMock(WebServiceMock.class, new RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(null));
            
            RcpOutBoundAccountService.buildRcpTeamMember(accountTeamMemberList);
            
            RcpOutBoundProxy.executeTeamMemberUpdated();
            
        	Test.stopTest();
            
        //}catch(TypeException e){}
    }
}