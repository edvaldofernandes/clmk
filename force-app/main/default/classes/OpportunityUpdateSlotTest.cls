/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class OpportunityUpdateSlot
* NAME: OpportunityUpdateSlotTest.cls
* AUTHOR: KHPS                                                DATE: 20/12/2014
*
*******************************************************************************/

@isTest
private class OpportunityUpdateSlotTest {
	
		private static final id recTypeAcc = RecordTypeMemory.getRecType('Account', 'Operator');

    static testMethod void testeFuncional() {
    	
    	Id pbkId = SObjectInstanceTest.catalogoDePrecoPadrao();
    	
    	Product2 prod1 = SObjectInstanceTest.createProduct2(); 	
    	
    	Product2 prod2 = SObjectInstanceTest.createProduct2();
    	prod2.Name = 'Produto Teste 2';
      prod2.ProductCode = 'a555';
    	Database.insert(new list<Product2>{ prod1, prod2 });
    	
    	PricebookEntry pbe1 = SObjectInstanceTest.createPricebookEntry(pbkId, prod1.Id);
    	
    	PricebookEntry pbe2 = SObjectInstanceTest.createPricebookEntry(pbkId, prod2.Id);
    	Database.insert(new list<PricebookEntry>{ pbe1, pbe2 });

    	Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
    	Database.insert(acc);
    	
    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.AccountId = acc.Id;
    	opp.Fleet_Type__c = 'EJET';
    	Database.insert(opp);
    	
    	OpportunityLineItem oli1 = SObjectInstanceTest.createOppItem(opp.Id, pbe1.Id);
    	oli1.Start_date__c = System.today();
    	oli1.End_date__c = System.today().addDays(10);
    	oli1.Quantity = 100;
    	Database.insert(oli1);
    	
    	OpportunityLineItem oli2 = SObjectInstanceTest.createOppItem(opp.Id, pbe2.Id);
    	Database.insert(oli2);  	
    	
    	Test.startTest();
    	Database.delete(opp);
    	Test.stopTest();    	
    	
    }
    
		static testMethod void testeUpdate() {
			
    	Id pbkId = SObjectInstanceTest.catalogoDePrecoPadrao();
    	
    	Product2 prod1 = SObjectInstanceTest.createProduct2(); 	
    	
    	Product2 prod2 = SObjectInstanceTest.createProduct2();
    	prod2.Name = 'Produto Teste 2';
      prod2.ProductCode = 'a555';
    	Database.insert(new list<Product2>{ prod1, prod2 });
    	
    	PricebookEntry pbe1 = SObjectInstanceTest.createPricebookEntry(pbkId, prod1.Id);
    	
    	PricebookEntry pbe2 = SObjectInstanceTest.createPricebookEntry(pbkId, prod2.Id);
    	Database.insert(new list<PricebookEntry>{ pbe1, pbe2 });

    	Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
    	Database.insert(acc);
    	
    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.AccountId = acc.Id;
    	opp.StageName = 'Archived';
    	opp.Fleet_Type__c = 'EJET';
    	Database.insert(opp);
    	
    	OpportunityLineItem oli1 = SObjectInstanceTest.createOppItem(opp.Id, pbe1.Id);
    	oli1.Start_date__c = System.today();
    	oli1.End_date__c = System.today().addDays(10);
    	oli1.Quantity = 100;
    	Database.insert(oli1);
    	
    	OpportunityLineItem oli2 = SObjectInstanceTest.createOppItem(opp.Id, pbe2.Id);
    	Database.insert(oli2);  	
    	
    	opp.StageName = 'Closed Lost';
    	opp.Reason_for_Rejection__c = 'Price';
    	opp.Comments_for_Rejection__c = 'comments';
    	Test.startTest();
    	Database.update(opp);
    	Test.stopTest(); 			
			
		}    
    
}