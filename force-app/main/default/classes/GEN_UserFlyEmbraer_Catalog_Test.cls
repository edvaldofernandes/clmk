/**
* @author Marcilio Leite de Souza
* @date 30/05/2018
* @description: TEST CLASS FOR GEN_UserFlyEmbraer
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           30 MAY 2018             Original Version
**/
@isTest
private class GEN_UserFlyEmbraer_Catalog_Test {
    
    
    @isTest
    static void it_should_create_new_contact_and_user() {
        
        create_account_test_data();
        GEN_UserFlyEmbraer_Catalog gen_userFly = new GEN_UserFlyEmbraer_Catalog();
        User u = gen_userFly.handleUser('0DBi0000000TOMd', map_Attributes_test_data(),'testLogin',true);
        Test.startTest();
        System.assertNotEquals(u , null);
        Test.stopTest();
        
    }
    
    @isTest
    static void it_should_create_new_user_with_old_contact() {
        
        Test.startTest();
        create_contact_test_data();
        GEN_UserFlyEmbraer_Catalog gen_userFly = new GEN_UserFlyEmbraer_Catalog();
        User u = gen_userFly.handleUser('0DBi0000000TOMd', map_Attributes_test_data(),'testLogin',false);
        System.assertNotEquals(u , null);
        Test.stopTest();
        
    }
    
    @isTest
    static void it_should_update_old_user_with_permission_set() {
        
        Contact c = create_contact_test_data();
        create_user_test_data(c);
        GEN_UserFlyEmbraer_Catalog gen_userFly = new GEN_UserFlyEmbraer_Catalog();
        User u = gen_userFly.handleUser('0DBi0000000TOMd', map_Attributes_test_data(),'testLogin',false);
        
        Test.startTest();
        
        System.assertNotEquals(u , null);
        
        Test.stopTest();
        
    }
    
    @isTest
    static void it_test_inner_class_FlyEmbraerUser() {
        Test.startTest();
        
        GEN_UserFlyEmbraer_Catalog.FlyEmbraerUser flyUser = new GEN_UserFlyEmbraer_Catalog.FlyEmbraerUser();
        flyUser.embCompanyCode = 'testEmbCompanyCode';
        flyUser.embLogin = 'testEmbLogin';
        flyUser.federationIdentifier = 'testFederationId';
        flyUser.fullName = 'testFullName';
        flyUser.mail = 'test@test.com';
  
        Test.stopTest();
    }
    
    //Attributs test
    static Map<String, String> map_Attributes_test_data(){
        Map<String, String> attributes = new Map<String, String>();
        attributes.put('embCompanyCode','99999');
        attributes.put('embLogin','testLogin');
        attributes.put('fullName','Test Full Name');
        attributes.put('mail','test@test.com');
        attributes.put('federationIdentifier','testLogin');
        
        return attributes;
    } 
    
   
    // Create objets Account,Contatc,User and Attributs.
    static Account create_account_test_data(){
        Account a = new Account();
        a.BillingCountry = 'Brazil';
        a.Name = 'Test';
        a.Company_Nickname__c = 'testNickname';
        a.FlyEmbraerId__c = '99999';
        insert a;
        return a;
    }
    
    static Contact create_contact_test_data(){
        Contact c = new Contact();
        c.Title = 'testTitle';
        c.LastName = 'test LastName';
        c.Gender__c = 'Male';
        c.Contact_Status__c ='Active';
        c.Email = 'test@test.com.br';
        c.FlyEmbraerUserId__c = 'testLogin';
        c.AccountId = create_account_test_data().Id;
        
        insert c;
        return c;
    }
    
    static Contact create_Exist_contact_test_data(){
        Contact c = new Contact();
        c.Title = 'testTitle';
        c.LastName = 'test LastName';
        c.Gender__c = 'Male';
        c.Contact_Status__c ='Active';
        c.Email = 'test@test.com.br';
        c.FlyEmbraerUserId__c = 'testLogin';
        c.AccountId = create_account_test_data().Id;
        
        insert c;
        return c;
    }
    
    static void create_user_test_data(Contact c){
        Id idProfile = [Select Id From Profile Where Name = 'Services Catalog'].Id;
        
        User u = new User();
        u.Username = 'usernametest@mail.com.br';
        u.FirstName = 'Test ';
        u.LastName = 'User 1';
        u.Email = 'email1@mail.com.br';
        u.Alias = 'Alias1'; 
        u.CommunityNickname = 'Community Nickname1';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_Us'; 
        u.EmailEncodingKey = 'ISO-8859-1'; 
        u.LanguageLocaleKey = 'en_Us'; 
        u.FederationIdentifier = 'testLogin';
        u.FlyEmbraerUserId__c = 'teste1';
        u.IsActive = true;
        u.ProfileId = idProfile;
        u.ContactId = c.Id;
        u.CompanyName = 'teste';
        u.phone = '12992522570';
        u.Title = 'tes';
        u.City = 'texas';
        
        insert u;
    }
}