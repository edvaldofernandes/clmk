/* Author: Fabiano Albino Ferreira - TCS
* 
* Description: .
* 
* CreatedDate: 05/12/2019.
* 
* LastModifiedDate: 05/12/2019..
*/

public class AssignContact {
    
    private final Id CRC_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();
    
    private ContactDAO contactDAO;
    private List<Case> cases;
    private Map<String, ParamCaseSubject__mdt> mapParamBySubject;
    
    public AssignContact(List<Case> cases){
        this.cases = cases;
        this.contactDAO = new ContactDAO();
        
        buildParamCaseSubject();
    }
    
    public void assignContact(){
        
        Set<String> subjects = mapParamBySubject.keySet();
        Set<String> emails = new Set<String>();
        Map<String, Contact> emailContacts = new Map<String, Contact>();
        
        try{
            
            for(Case cas : cases){
                
                if(cas.RecordTypeId != CRC_ID)
                    continue;
                
                String prefix = cas.Subject.substringBeforeLast(' -');
                System.debug(prefix);
                
                if(String.isBlank(prefix))
                    continue;
                
                if(!subjects.contains(prefix)){
                    continue;
                }
                
                ParamCaseSubject__mdt param = mapParamBySubject.get(prefix);
                
                emails.add(param.Email__c);
            }
            
            if(emails == null || emails.isEmpty())
                return;
            
            List<Contact> contacts = contactDAO.getContactsByEmail(emails);
            
            if(contacts == null || contacts.isEmpty())
                return;
            
            for(Contact cont: contacts){
                emailContacts.put(cont.Email.toLowerCase(), cont);
            }
            
            for(Case cas : cases){
                
                System.debug('#Subject: ' + cas.Subject);
                
                if(cas.RecordTypeId != CRC_ID)
                    continue;
                
                String prefix = cas.Subject.substringBeforeLast(' -');
                String so = cas.Subject.substringAfterLast('-');
                
                if(!subjects.contains(prefix)){
                    continue;
                }
                
                ParamCaseSubject__mdt param = mapParamBySubject.get(prefix);
                                
                String email = param.Email__c.toLowerCase();
                                
                if(String.isBlank(email))
                    continue;
                                
                if(!emailContacts.containsKey(email)){
                    continue;
                }
                
                Contact contact = emailContacts.get(email);
                
                cas.AccountId = contact.AccountId;
                cas.ContactId = contact.Id;
                cas.SO__c = so;
                cas.Priority = param.Priority__c;
                
                System.debug('##cas: ' + cas);
            }
        }catch(Exception ex){
            System.debug('Erro ao criar o case com o Account e ContactId pré definidos no Subject: ' + ex.getMessage());
        }
    }
    
    private void buildParamCaseSubject(){
        
        mapParamBySubject = new Map<String, ParamCaseSubject__mdt>();
        
        List<ParamCaseSubject__mdt> params = [Select Id, Subject__c, Email__c, Priority__c FROM ParamCaseSubject__mdt];
        
        for(ParamCaseSubject__mdt param : params){
            mapParamBySubject.put(param.Subject__c, param);
        }
        
    }
}