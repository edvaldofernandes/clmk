public class CRM360_OppViewController {
    
    
    public CRM360_OppViewController(){
        last12Month = Date.today();
        last12Month = last12Month.addYears(-1);
        
        loadOpportunities();
        //this.Category = 'Init';
        //(String) ApexPages.currentPage().getParameters().get('category');
    }
    
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private Account acc = [SELECT Name, Id, COD_ORGz__c FROM Account WHERE Id = :accId];
    private static Id RecordTypeIdNewACSales = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Aircraft Sales').getRecordTypeId();
    private static Id RecordTypeIdUsedACSales = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Used Aircraft Sales').getRecordTypeId();

    
    private List<Opportunity> oppList = new List<Opportunity>();

    public Decimal totalOpenOppPrice {get; set;}
    public Decimal totalOpenOpp {get; set;}
    public Decimal totalClosedWonOpp {get; set;}
    public Decimal totalClosedWonOppPrice {get; set;}
    private Date last12Month;
    
    public Date getLastYear(){
        return last12Month;
    }
    
    public Account getAccount (){
        return acc;
    }
    
    public void loadOpportunities(){
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [SELECT RecordTypeId,Service_Program_Type__c, Name, Description, RFP_Date__c, CloseDate,Amount, StageName, Probability__c
                   FROM Opportunity
                   WHERE 
                   AccountId = :this.acc.Id 
                   AND StageName != 'Closed Won'
                   AND StageName != 'Closed Lost'
                   AND StageName != 'Archived'];
        //system.debug('Cases List' +caseList);
        if(oppList.size() != 0) {
            this.oppList = oppList;
            totalOpenOppPrice = 0;
            for(Opportunity o : oppList) {
                if(o.amount != null) {
                    totalOpenOppPrice += o.amount;
                } 
            }
            totalOpenOppPrice = totalOpenOppPrice /1000;
        }
        totalOpenOpp = oppList.size();

        List<Opportunity> oppList2= new List<Opportunity>();
        oppList2= [SELECT RecordTypeId,Service_Program_Type__c, Name, Description, RFP_Date__c, CloseDate,Amount, StageName, Probability__c
                   FROM Opportunity
                   WHERE 
                   AccountId = :this.acc.Id 
                   AND Close_Date__c >= :last12Month
                   AND StageName = 'Closed Won'];
        //system.debug('Cases List' +caseList);
        if(oppList2.size() != 0) {
            this.oppList.addAll(oppList2);

            totalClosedWonOppPrice = 0;
            for(Opportunity o : oppList2) {
                if(o.amount != null) {
                    totalClosedWonOppPrice += o.amount;
                } 
            }
            totalClosedWonOppPrice = totalClosedWonOppPrice /1000;
        }
        totalClosedWonOpp = oppList2.size();
    }
    
    public List<Opportunity> getOpps(){
        return oppList;
    }
}