@isTest
private class EamsComponentsCapTest {	
	
    static testMethod void accListTest(){
        Components_Capability__c compCap = new Components_Capability__c ();
        compCap.name='145-9087-098';
        compCap.Description__c = 'Component Cap List Test';
        compCap.ATA__c='57';
        compCap.Insp__c = 'YES';
        compCap.Repair__c  = 'YES';
        compCap.Manufacture__c  = 'Kawasaky';
        compCap.Aircraft_Type__c = 'Embraer ERJ-145';
        compCap.Shop__c = 'C-AIRFRAME';
        insert compCap;
        
        EamsComponentsCap controller = new EamsComponentsCap();
        System.assert(controller.cl.size() == 1);        
    }    
}