/* Classe implementadora de SOBjectDAO para operações DML no objeto Proposal_Items__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 04/01/2016
*/
public without sharing class ProposalItemsDao {
    
    private static final ProposalItemsDao instance = new ProposalItemsDao();    
    
    private ProposalItemsDao(){
    }    
    
    public static ProposalItemsDao getInstance(){
        return instance;
    }
    
    public List<Proposal_Items__c> listItemsByProposal(String idProposal){
		List<Proposal_Items__c> listItem = [SELECT Proposal__c, Product__c, Name, Id, Product__r.RecordTypeId, Product__r.Name, Product__r.Commercial_Description__c, Product__r.Family, Product__r.Custom_Redirect_Pardot__c, Product__r.Pardot_Status__c
                                            FROM Proposal_Items__c p 
                                            WHERE Proposal__c = :idProposal];
        return listItem;
    }
    
    public List<Proposal_Items__c> listBySetIdProposal(Set<ID> setIdproposal){        
        return [SELECT Proposal__c, Product__c, Name, Id, Product__r.RecordTypeId, Product__r.Name, Product__r.Commercial_Description__c, Product__r.Family, Product__r.Applicability__c
                                      FROM Proposal_Items__c p 
                                      WHERE Proposal__c IN :setIdproposal];
    }
    
    public Proposal_Items__c getItemByProposalAndProduct(String idProposal, String idProduct){
        List<Proposal_Items__c> listItem = [SELECT Proposal__c, Product__c, Name, Id, Product__r.RecordTypeId, Product__r.Name, Product__r.Commercial_Description__c, Product__r.Family
                                            FROM Proposal_Items__c p 
                                            WHERE Proposal__c = :idProposal
                                            AND Product__c = :idProduct];                
        if(listItem.size() > 0){
            return listItem[0];
        }
        
        return null;
    }
    
    public Proposal_Items__c createProposalItem(String idProduct, String idProposal){
        Proposal_Items__c item = new Proposal_Items__c();
        item.Product__c = idProduct;
        item.Proposal__c = idProposal;
        insert item;
        
        return item;
    }
    
    public void deleteProposalItem(Proposal_Items__c item){        
        delete item;
    }    
}