/* 
----------------------------------------------------------------------------------------------
-- - Company:     Stefanini
-- - Name:        BO_MFIR
-- - Description: Manage MFIR records also can be used to retrieve MFIRs
-- - @Author: Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               Version                   
----------------------------------------------------------------------------------------------
-- 03/12/2019       Felipe Gouvea      1.0         
----------------------------------------------------------------------------------------------
*/

public with sharing class BO_MFIR {


	public static List<MFIR__c> getMFIRsRelatedToCase(Case targetCase){
		try{
			List<MFIR__c> relatedMFIRs = [SELECT MFIR_number__c, SAP_Name__c, City__c, Country__c, Overdue_TOTAL__c, Blockage__c FROM MFIR__c WHERE Account__c =: targetCase.AccountId];
			system.debug('Result . relatedMFIRs.size() >>  ' + relatedMFIRs.size());
			return relatedMFIRs;
		}
		catch(Exception e){
			System.debug('Exception on class BO_MFIR at method getMFIRsRelatedToCase >>> ' + e.getMessage());
			return new List<MFIR__c>();
		}
		
	}
}