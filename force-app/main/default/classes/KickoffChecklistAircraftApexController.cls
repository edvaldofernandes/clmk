public with sharing class KickoffChecklistAircraftApexController {
    @AuraEnabled
    public static List<Kickoff_Aircraft__c> getOrderSizeItems(Id recordId){
        List<Kickoff_Aircraft__c> OSKAList = new List<Kickoff_Aircraft__c>();
        try{
			OSKAList = [SELECT Id, Aircraft_Model__c, Model_Name__c, Firm__c, Option__c, Purchase_Right__c 
                                                  FROM Kickoff_Aircraft__c
                                                  WHERE Kickoff__c =: recordId AND Info_Type__c = 'Order Size']; 
            return OSKAList;
        } catch(Exception e){
            String errorMessage = 'Error: ' + e.getMessage() + ' ' + e.getStackTraceString();
            system.debug(errorMessage);
            return OSKAList;
        }
    }
    
    @AuraEnabled
    public static List<Kickoff_Aircraft__c> getScheduleItems(Id recordId){
        List<Kickoff_Aircraft__c> OSList = new List<Kickoff_Aircraft__c>();
        try{
			OSList = [SELECT Id, Aircraft_Model__c, Model_Name__c, Year0Quantity__c, Year1Quantity__c, Year2Quantity__c, Year3Quantity__c, Year4Quantity__c, Year5Quantity__c, Year6Quantity__c, Year7Quantity__c 
                                                  FROM Kickoff_Aircraft__c
                                                  WHERE Kickoff__c =: recordId AND Info_Type__c = 'Delivery Schedule']; 
            return OSList;
        } catch(Exception e){
            String errorMessage = 'Error: ' + e.getMessage() + ' ' + e.getStackTraceString();
            system.debug(errorMessage);
            return OSList;
        }
    }
    
    @AuraEnabled
    public static Integer getYear(Id recordId){
        Kickoff__c ko;
        try{
			ko = [SELECT Id, Kick_Off_Meeting_Date__c FROM Kickoff__c WHERE Id =: recordId]; 
            return ko.Kick_Off_Meeting_Date__c.Year();
        } catch(Exception e){
            String errorMessage = 'Error: ' + e.getMessage() + ' ' + e.getStackTraceString();
            system.debug(errorMessage);
            return 0;
        }
    }
    
    @AuraEnabled
    public static List<Kickoff_Aircraft__c> saveEdits(List<Kickoff_Aircraft__c> acs){
        try{
            update acs;
            return acs;
        } catch(Exception e){
            String errorMessage = 'Error: ' + e.getMessage() + ' ' + e.getStackTraceString();
            system.debug(errorMessage);
            return acs;
        }
    }

}