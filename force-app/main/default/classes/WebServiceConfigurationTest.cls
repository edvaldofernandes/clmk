@isTest(SeeAllData=true)
public class WebServiceConfigurationTest {
    
    public static String ETRACK_GET_QUESTIONS_TEST = 'ETRACK_GET_QUESTIONS_TEST';
    
    @isTest static void getEnPointTest(){
        
        EventConfiguration__c eventConfiguration = new EventConfiguration__c();
        eventConfiguration.Name = 'ETRACK_GET_QUESTIONS_TEST';
        eventConfiguration.endPointUrl__c = 'http://ogwtest.com.br';
        
        Test.startTest();
        
        insert eventConfiguration;
        
        String endPoint = WebServiceConfiguration.getEnPoint(ETRACK_GET_QUESTIONS_TEST);
        
        System.assertEquals('http://ogwtest.com.br', endPoint);
        
        Test.stopTest();
    }
    
    @isTest static void getAuthenticationTest(){
        
        EventConfiguration__c eventConfiguration = new EventConfiguration__c();
        eventConfiguration.Name = 'ETRACK_GET_QUESTIONS_TEST';
        eventConfiguration.username__c = 'admin';
        eventConfiguration.password__c = 'admin';
        
        Test.startTest();

        insert eventConfiguration;
        
        String authentication = WebServiceConfiguration.getAuthentication(ETRACK_GET_QUESTIONS_TEST);
        
        System.assertEquals('admin:admin', authentication);
        
        Test.stopTest();
    }
}