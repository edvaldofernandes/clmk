/**
* @author Marcilio Leite de Souza
* @date 19/06/2018
* @description: Flight Operation business rules method
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           19 JUN 2018             Original Version
**/
public class FOR_FlightOperationsBusinessRules {
	
    public FOR_Flight_Operation_Request_Setting__mdt settings {get;set;}
    
    public static final String SINGLE_CONFIGURATION = 'Single_Configuration';
    public static final String QUEUE = 'Queue';
    
    /**
	* @description : Constructor with basic configuration
	*/
    public FOR_FlightOperationsBusinessRules(){
    	this.settings = [SELECT FOR_Account_Id__c, FOR_Queue_Label__c, FOR_Record_Type_Name__c
                         FROM FOR_Flight_Operation_Request_Setting__mdt
                         WHERE DeveloperName = :SINGLE_CONFIGURATION LIMIT 1];    
    }
    
    /**
	* @description : Select the reference record type based on Metadata Settings config
	* @return String : Id of record type related to the activity
	*/
    public String getRecordTypeIdToFOR(){       
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get(settings.FOR_Record_Type_Name__c).getRecordTypeId();
    }
    
    /**
	* @description : Select the reference Queue to owner the case
	* @return String : Id of record type related queue
	*/
    public String getQueueIdToFOR(){
        return [SELECT Id FROM Group WHERE Name = :settings.FOR_Queue_Label__c
                                     AND   Type = :QUEUE].Id;
    }
    
    /**
	* @description : Select the reference Account for the case
    * @param String customer : Customer name to be find
	* @return String : Id of the account
	*/
    public String getAccountIdToFOR(String customer){
        String idAcc = settings.FOR_Account_Id__c;
        List<Account> listAcc;

        if(customer != null){
            String str = customer+'%';
            listAcc = [SELECT Id FROM Account WHERE Name LIKE :str LIMIT 1];
        }
        
        if(listAcc != null && listAcc.size() > 0){
            idAcc = listAcc.get(0).Id;
        }

        return idAcc;
    }
}