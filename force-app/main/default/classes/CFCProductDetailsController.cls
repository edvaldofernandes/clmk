/**
* Controller class for the visualforce page CFCProductDetails.
* Name: CFCProductEditController
* @version 1.0 - 15/12/2015
**/

public with sharing class CFCProductDetailsController {
    
    public Product2 productDetail {get; set;}
    public Boolean showButtonProposal {get; set;}
    public Boolean isFavorite {get; set;}
    
    public Integer indexOfImages {get;set;}
    public Map<Integer,Attachment> mapAttachmentImages {get;set;}
    
    public Map<String, String> mapAttachmentDownloads{get;set;}
    public Map<String, String> mapContentTypeDownloads{get;set;}
    public List<Attachments__c> lstAttachmentDownload {get; set;}
    public Document documentReferenceGuideownload {get; set;}
    
    public Boolean showFieldProductCode {get;set;}
    public Boolean showFieldProductName {get;set;}
    public Boolean showFieldProductType {get;set;}
    public Boolean showFieldDescription {get;set;}
    public Boolean showFieldComercialDescription {get;set;}
    public Boolean showFieldIntendedAudience {get;set;}
    public Boolean showFieldeTrainingLink {get;set;}
        //Modification
    public Boolean showFieldCourseDuration {get;set;}
    public Boolean showFieldMaxNumbertrainees {get;set;}
    public Boolean showFieldMinNumbertrainees {get;set;}
    public Boolean showFieldMainFeatures {get;set;}
    public Boolean showFieldCertifications {get;set;}
    
    	//MRO Modification
    
    
    public Boolean showFieldTrialPeriod {get;set;}
    public Boolean showFieldTrialDescription {get;set;}
    public Boolean showFieldAircraftModel {get;set;}
    public Boolean showFieldBenefits {get;set;}
    public Boolean showTitlePhotograph {get;set;}
    
    public Boolean showFieldRecordType {get;set;}
    public Boolean showFieldApplicability {get;set;}
    public Boolean showFieldPrerequisites {get;set;}
    public Boolean showFieldLocation {get;set;}
    
    public Boolean showFieldReferences {get;set;}
    
    public Boolean showFieldMotivationsForSale {get;set;}
    public Boolean showFieldEstimatedLeadTime {get;set;}
    public Boolean showFieldEstimatedAircraftDowntime {get;set;}
    public Boolean showFieldEstimatedAircraftManHours {get;set;}
    public Boolean showFieldEstimatedWeightChange {get;set;}
        
    public Boolean showButtonTrainingCalendar {get;set;}
    public Boolean showButtonPreviewNext {get;set;}
    public Boolean showModelsNull {get;set;}
    public Boolean showServiceCenterNull {get;set;}
    
    public map<String, String> mapAuthorizedServiceCenter{get;set;}
    public List<Attachments__c> lstServiceCenter {get;set;}
    public List<Account> listAccountMRO {get;set;}
       
    public map<String, List<String>> mapAirCraftModel {get;set;}
    public map<String, String> mapAirCraftModelImageId {get;set;}
    public List<AuthorizedServiceCenterInnerClass> lstAuthorizedServiceCenterInnerClass {get;set;}
    public User user;
    
    public String aircraftModel {get;set;}
    public String varBenefits {get;set;}
    
    public Boolean isView {get;set;}    
	public Boolean isPardotActive {get;set;}
    
    public CFCProductDetailsController(ApexPages.StandardController stdController){
        try{
            isFavorite = false;
            showButtonProposal = false;
            
            productDetail = (Product2)stdController.getRecord();
            productDetail = Product2DAO.getInstance().getProductById(productDetail.Id);
            system.debug('CFCProductDetailsController.productDetail >>> ' + productDetail);
            
            aircraftModel = prepareStringAircraftModel(productDetail.Aircraft_Model__c);
            varBenefits = prapareStringBenefits (productDetail.Benefits__c);
            showButtonProposal = productDetail.Show_Proposal__c ? true : false;
            
            user = UserDAO.getInstance().getUserById(UserInfo.getUserId());
            system.debug('CFCProductDetailsController.user >>> ' + user); 
            system.debug('CFCProductDetailsController.user.Contact.AccountId >>> ' + user.Contact.AccountId);
            system.debug('CFCProductDetailsController.user.ContactId >>> ' + user.ContactId);
            
            VerifyPardot();
            
            VerifyFavorite();
            
            VerifyProductType();
            
            GenerateAttachmentImagesMap();
            
            GenerateAttachmentDownload();
            
            generateAuthorizedServiceCenter();
    
            loadListAirCraftModel();
            
            //if just a view the user cant do a proposal
            if(isView){
                showButtonProposal = false;
            }
            
            
        } catch(Exception ex){
            system.debug('ERROR >>> CFCProductDetailsController() >>> ' + ex);
        }        
	}
    
    private String prepareStringAircraftModel(String displayModel){        
        string newModel = '';        
        if(!String.isBlank(displayModel)) {
            displayModel = displayModel.replaceAll('([0-9^A-Z]+[ ]+[\\| ]*)', '');            
            if(String.isNotBlank(displayModel)){
                for(String txt : displayModel.split(';')){
                    newModel += txt + ';&nbsp;&nbsp;';
                }
            }                
            System.debug('CFCProductDetailsController.prepareStringAircraftModel.newModel >>> ' + newModel.unescapeHtml4());
        }        
        return newModel.unescapeHtml4();
    }
    
    private String prapareStringBenefits(string varStr){
        if(!string.isBlank(varStr)){
            varStr = varStr.replace('<br>', '\n');
            return varStr.unescapeHtml4();
        }
        return '';
        //return varStr.escapeHtml4();
    }
    
    public void VerifyPardot(){
        system.debug('CFCHomeController.GenerateViewProducts.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCHomeController.GenerateViewProducts.isPardotActive >>> ' + isPardotActive);
    }
    
    public void verifyIsView(){
        try{
            //determine if is just a productView
            String viewParam = ApexPages.currentPage().getParameters().get('View');
            system.debug('CFCProductDetailsController.verifyIsView.viewParam >>> ' + viewParam);
            if(!String.isBlank(viewParam)){
                isView = Boolean.valueOf(viewParam);
            } else {
                isView = false;
            }            
        }catch(Exception ex){
            system.debug('ERROR >>> verifyIsView() >>> ' + ex);
            isView = true;
        }
        
        system.debug('CFCProductDetailsController.verifyIsView.isView >>> ' + isView);
    }
    
    public void addRecentlyViewed(){
        try{
            System.debug('CFCProductDetailsController.addRecentlyViewed inicio');
            
            PageReference page;
            page = new PageReference(productDetail.Id);
            page.setRedirect(false);
            
            System.debug('CFCProductDetailsController.addRecentlyViewed fim');  
        } catch(Exception ex){
            system.debug('ERROR >>> addRecentlyViewed() >>> ' + ex);
        }
        
        
    }
    
    public Id getId(){
        return productDetail.Id;
    }
    
    public void loadListAirCraftModel() {
        try {
            showModelsNull = true;
            
            system.debug('start CFCProductDetailsController.loadListAirCraftModel');
            Product2 p = Product2DAO.getInstance().getAirCraftModelById(productDetail.Id);
            system.debug('PRODUCT ID: ' +p);
            System.debug('CFCProductDetailsController.loadListAirCraftModel.Product2: ' + p.Aircraft_Model__c);
            List<Attachments__c> attAircraftModel = AttachmentCustomDao.getInstance().listAttachmentAircraftModel();
            system.debug('attAircraftModel: ' + attAircraftModel);
            mapAirCraftModelImageId = new map<String, String>();
            for(Integer i=0; i < attAircraftModel.size(); i++) {                
                try{
                    mapAirCraftModelImageId.put(attAircraftModel.get(i).Aircraft_Model__c, attAircraftModel.get(i).Attachments[0].id);
                }catch(Exception ex){
                    mapAirCraftModelImageId.put(attAircraftModel.get(i).Aircraft_Model__c, 'no-image');
                }
            }
            System.debug('mapAirCraftModelImageId PUT: ' + mapAirCraftModelImageId);
            
            if(p.Aircraft_Model__c != null && p.Aircraft_Model__c != '') {
                String [] aircraftModel = p.Aircraft_Model__c.split(';');
                mapAirCraftModel = new map<String, List<String>>();
                showModelsNull = false;
                
                for(Integer i=0; i < aircraftModel.size(); i++) {
                    String [] aircraftPrefixModel = aircraftModel[i].split('\\|');                
                    String prefix = aircraftPrefixModel[0].trim();                
                    String nameModel = aircraftPrefixModel.size() > 1 ? aircraftPrefixModel[1].trim() : aircraftPrefixModel[0].trim();
                    system.debug('NAME MODEL: ' +nameModel);
					
                    if(!mapAirCraftModelImageId.containsKey(nameModel)){
                        putMapModel(mapAirCraftModel, prefix, 'no-image');
                        
                    } else {
                    	putMapModel(mapAirCraftModel, prefix, nameModel); 
                    }                    
                }
                
                Integer sizeTable = 0;
                for(String key : mapAirCraftModel.keySet()) {
                    if(mapAirCraftModel.get(key).size() > sizeTable) {
                        sizeTable = mapAirCraftModel.get(key).size();
                    }                
                }
                
                for(String key : mapAirCraftModel.keySet()) {
                    if(mapAirCraftModel.get(key).size() < sizeTable) {
                        for(String lst : mapAirCraftModel.get(key)) {
                            if(mapAirCraftModel.get(key).size() < sizeTable) {                                   
                                putMapModel(mapAirCraftModel, key, 'no-image');
                            }
                        }
                    }
                }
                System.debug('Finish CFCProductDetailsController.loadListAirCraftModel.mapAirCraftModel: ' + mapAirCraftModel);
            }
        } catch(Exception ex){
            system.debug('ERROR >>> loadListAirCraftModel() >>> ' + ex);
        }        
    }
    
    public Map<String, List<String>> putMapModel(Map<String, List<String>> mapModel, String prefixAirCraftModel, String nameAirCraftModel) {
        system.debug('CFCProductDetailsController.putMapModel.prefixAirCraftModel: ' + prefixAirCraftModel);
        if(mapModel.containsKey(prefixAirCraftModel)) {
            List<String> lstModel = mapModel.get(prefixAirCraftModel);            
            lstModel.add(nameAirCraftModel);            
            mapModel.remove(prefixAirCraftModel);
            mapModel.put(prefixAirCraftModel, lstModel); 
        } else {
            List<String> lstModel = new List<String>();
            lstModel.add(nameAirCraftModel);
            mapModel.put(prefixAirCraftModel, lstModel);
        }
        return mapModel;
    }
    
    
    
    public void generateAuthorizedServiceCenter(){
        system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter()');
        
        mapAuthorizedServiceCenter = new map<String, String>();   
        lstServiceCenter = new List<Attachments__c>();
        listAccountMRO = new List<Account>();
        
        showButtonPreviewNext = false;
        showServiceCenterNull = true;
        
        //produto tem campo preenchido e tem indicate MRO
        //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.productDetail.Maintance_Capability__c >>> ' + productDetail.Maintance_Capability__c);
        //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.productDetail.Indicates_MRO__c >>> ' + productDetail.Indicates_MRO__c);
        if(!String.isBlank(productDetail.Maintance_Capability__c) && productDetail.Indicates_MRO__c){
            
            //get all maintance capability
            set<String> setMaintanceCapabily = new Set<String>();
            for(String str : productDetail.Maintance_Capability__c.split(';')){
                setMaintanceCapabily.add(str);
            }
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.setMaintanceCapabily >>> ' + setMaintanceCapabily);
            
            //getrecord type MRO
            RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('MRO');
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.rType >>> ' + rType);
            
            //get all account of type MRO
            listAccountMRO = AccountDAO.getInstance().listByRecordTypeIdAndMaintanceCapability(rType.id, setMaintanceCapabily);
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.listAccountMRO >>> ' + listAccountMRO);
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.listAccountMRO.size() >>> ' + listAccountMRO.size());
            
            //get record type "Logo To MRO"
            RecordType rTypeLogoToMro = RecordTypeDao.getInstance().getByDeveloperName('Logo_To_MRO');
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.rTypeLogoToMro >>> ' + rTypeLogoToMro);
            
            //get all attachments by accounts
            lstServiceCenter = AttachmentCustomDao.getInstance().listByRecordTypeId(rTypeLogoToMro.Id);        
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.lstServiceCenter >>> ' + lstServiceCenter);
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.lstServiceCenter.size() >>> ' + lstServiceCenter.size());
            
            if(lstServiceCenter != null){
                showServiceCenterNull = false;
            } else {
                showServiceCenterNull = true;
            }
            
            //for just to fill the map with accounts and no-image
            for(Account acc : listAccountMRO){
                mapAuthorizedServiceCenter.put(acc.Id, 'no-image');
            }
            
            //for to fill the map to replace no-image to real image
            for(Account acc : listAccountMRO){
                for(Attachments__c att : lstServiceCenter){
                    try{
                        if(acc.Id == att.Account__c){
                            mapAuthorizedServiceCenter.put(acc.Id, att.Attachments[0].id);
                        }
                    } catch(Exception ex){
                        mapAuthorizedServiceCenter.put(acc.Id, 'no-image');
                    }                    
                }
            }
            
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.mapAuthorizedServiceCenter >>> ' + mapAuthorizedServiceCenter);
            //system.debug('CFCProductDetailsController.generateAuthorizedServiceCenter.mapAuthorizedServiceCenter.size() >>> ' + mapAuthorizedServiceCenter.size());
        }
    }       
    
    public pageReference RequestProposal(){
        try{
            system.debug('user.Contact.Id>>> ' +user.Contact.Id);
            system.debug('user.Contact.Account.id>>> ' +user.Contact.Account.id);
            system.debug('user.Contact.Email>>> ' +user.Contact.Email);
            system.debug('user.Contact.Phone>>>' +user.Contact.Phone);
            Proposal__c proposal;
            //get proposal        
            proposal = ProposalDao.getInstance().getOpenProposalByContactAndAccount(user.Contact.Id, user.Contact.Account.id);
            //system.debug('proposal>>> ' +proposal);
            if(proposal == null){
                //if proposal is null, so we create a new proposal
                proposal = ProposalDao.getInstance().createProposal(user.Contact.AccountId, user.ContactId, user.Contact.Email, user.Contact.Phone);
                //system.debug('proposal IF NULL>>> ' +proposal);
                //system.debug('CFCSearchResultController.RequestProposal.proposal >>> ' + proposal);
            }
            //system.debug('CFCSearchResultController.RequestProposal.proposal >>> ' + proposal);
            
            //add proposal items at proposal
            Proposal_Items__c item = ProposalItemsDao.getInstance().getItemByProposalAndProduct(proposal.Id, productDetail.Id);
            //system.debug('proposal item>>> ' +item);
            if(item == null){
                //create a new proposal item if he doesn't exist
                item = ProposalItemsDao.getInstance().createProposalItem(productDetail.Id, proposal.Id);
                //system.debug('proposal item IF NULL>>> ' +item);
            }
            
            PageReference page = new PageReference('/apex/CFCRequestProposal');
            //system.debug('proposal page reference>>> ' +page);
            return page;
        } catch(Exception ex){
            system.debug('ERROR >>> RequestProposal() >>> ' + ex);
            return null;
        }
        
        
    }
    
    public void VerifyFavorite(){        
        isFavorite = false;
        
        Favorite__c fav = FavoriteDao.getInstance().getByUserIdAndProductId(UserInfo.getUserId(), productDetail.Id);
        //system.debug('CFCProductDetailsController.VerifyFavorite.lstFavorites >>> ' + fav);
        
        if(fav != null){
            if(fav.Active__c){
                isFavorite = true;
            }
        }
        //system.debug('CFCProductDetailsController.VerifyFavorite.isFavorite >>> ' + isFavorite);        
    }    
    
    public void VerifyProductType(){
        RecordType rType = RecordTypeDao.getInstance().getById(productDetail.RecordTypeId);
        
        showFieldProductCode = false;
        showFieldProductName = false;
        showFieldProductType = false;
        showFieldDescription = false;
        showFieldComercialDescription = false;
        showFieldIntendedAudience = false;
        showFieldeTrainingLink = false;
        showFieldTrialPeriod = false;
        showFieldTrialDescription = false;
        showFieldAircraftModel = false;
        showFieldBenefits = false;
        showFieldRecordType = false;
        showFieldApplicability = false;
        showFieldPrerequisites = false;
        showFieldLocation = false;
        showFieldReferences = false;
        showFieldMotivationsForSale = false;
        showFieldEstimatedLeadTime = false;
        showFieldEstimatedAircraftDowntime = false;
        showFieldEstimatedAircraftManHours = false;
        showFieldEstimatedWeightChange = false;
        showButtonTrainingCalendar = false;
        showFieldCourseDuration = false;
        showFieldMaxNumbertrainees = false;
        showFieldMinNumbertrainees = false;
        showFieldMainFeatures = false;
        //showFieldCertifications = false;
        
        if(rType.DeveloperName.equalsIgnoreCase('Training')) {
            system.debug('Producy Type = Training');
            showFieldProductCode = true;
            showFieldProductName = true;
            showFieldProductType = true;
            showFieldDescription = true;
            showFieldComercialDescription = true;
            showFieldIntendedAudience = true;
            showFieldCourseDuration = true;
            showFieldMaxNumbertrainees = true;
            showFieldMinNumbertrainees = true;
            
            if(!String.isBlank(productDetail.eTraining_Link__c)) {
                showButtonTrainingCalendar = true;
            }
        } 
        
        if (rType.DeveloperName.equalsIgnoreCase('eSolutions')) {
            system.debug('Producy Type = eSolutions');
            showFieldProductCode = true;
            showFieldProductName = true;
            showFieldTrialPeriod = true;
            showFieldMainFeatures = true;
            
            showFieldTrialDescription = true;
            showFieldAircraftModel = true;
            //showFieldBenefits = true;
            showFieldDescription = true;
            showFieldComercialDescription = true;
            showFieldIntendedAudience = true;
        } 
        
        if (rType.DeveloperName.equalsIgnoreCase('Pilot_Services')) {
            system.debug('Producy Type = Pilot_Services');
            showFieldProductCode = true;
            showFieldProductName = true;
            showFieldRecordType = true;
            
            showFieldApplicability = true;
            showFieldPrerequisites = true;
            showFieldLocation = true;
            showFieldComercialDescription = true;
            showFieldAircraftModel = true;
            //showFieldBenefits = true;
            showFieldIntendedAudience = true;
        } 
        
        if (rType.DeveloperName.equalsIgnoreCase('TechnicalServices')) {
            system.debug('Producy Type = TechnicalServices');
            showFieldProductCode = true;
            showFieldProductName = true;
            showFieldDescription = true;
            showFieldMainFeatures = true;

            //showFieldBenefits = true;
            showFieldApplicability = true;
            showFieldPrerequisites = true;
            showFieldReferences = true;
            showFieldComercialDescription = true;
            showFieldIntendedAudience = true;
        } 
        
        if (rType.DeveloperName.equalsIgnoreCase('MaterialSolutions')) {
            system.debug('Producy Type = MaterialSolutions');
            //showFieldProductCode = true;
            showFieldProductName = true;
            showFieldDescription = true;
            //showFieldApplicability = true;
            showFieldMainFeatures = true;

            //showFieldPrerequisites = true;
            showFieldLocation = true;
            showFieldComercialDescription = true;
            showFieldAircraftModel = true;
            //showFieldBenefits = true;
            showFieldIntendedAudience = true;
        } 
        if (rType.DeveloperName.equalsIgnoreCase('MaintenanceServices')) {
            system.debug('Producy Type = MaintenanceServices');
            showFieldProductName = true;
            showFieldDescription = true;
            showFieldMainFeatures = true;
            showFieldCertifications = true;
            showFieldLocation = true;
            showFieldComercialDescription = true;
            showFieldAircraftModel = true;
            showFieldIntendedAudience = true;
        } 
        if (rType.DeveloperName.equalsIgnoreCase('Tailored')) {
            system.debug('Producy Type = Tailored');
            showFieldProductName = true;
            showFieldDescription = true;
            showFieldMainFeatures = true;
            showFieldCertifications = true;
            showFieldLocation = true;
            showFieldComercialDescription = true;
            showFieldAircraftModel = true;
            showFieldIntendedAudience = true;
        } 
        
        if (rType.DeveloperName.equalsIgnoreCase('Aircraft_Modification')) {
            system.debug('Producy Type = Aircraft_Modification');
            showFieldProductCode = true;
            showFieldProductName = true;
            showFieldDescription = true;
            //showFieldBenefits = true;
            showFieldMainFeatures = true;
            
            showFieldApplicability = true;
            showFieldPrerequisites = true;
            showFieldMotivationsForSale = true;
            showFieldEstimatedLeadTime = true;
            showFieldEstimatedAircraftDowntime = true;
            showFieldEstimatedAircraftManHours = true;
            showFieldEstimatedWeightChange = true;
            showFieldIntendedAudience = true;
        }
    }
    
    public pageReference pdfPrint(){
        try{
            System.debug('CFCProductDetailsController.pdfPrint inicio');
            
            PageReference page;
            
            //verify if link to pardot is active
            if(isPardotActive == true && !string.isBlank(productDetail.Print_Custom_Redirect_Pardot__c)){
                page = new PageReference(productDetail.Print_Custom_Redirect_Pardot__c);    
            } else {
                page = new PageReference('/apex/CFCProductDetailsPdf?id='+ productDetail.Id);
            }
            
            page.setRedirect(true);
            return page;
        } catch(Exception ex){
            system.debug('ERROR >>> addRecentlyViewed() >>> ' + ex);
            return null;
        }        
    }
    
    public pageReference AddFavorite(){
        try{
            system.debug('CFCProductDetailsController.AddFavorite()');  
            
            Favorite__c fav = FavoriteDao.getInstance().getByUserIdAndProductId(UserInfo.getUserId(), productDetail.Id);
            system.debug('CFCProductDetailsController.AddFavorite.fav >>> ' + fav);
            if(fav == null){
                //create a new favorite
                FavoriteDao.getInstance().createFavorite(UserInfo.getUserId(), productDetail.Id);
            } else {
                //update favorite
                FavoriteDao.getInstance().setActive(fav, True);
            }    
            
            PageReference page;
            
            //verify if link to pardot is active
            if(isPardotActive == true && !string.isBlank(productDetail.Favorite_Custom_Redirect_Pardot__c)){
                page = new PageReference(productDetail.Favorite_Custom_Redirect_Pardot__c);    
            } else {
                page = new PageReference('/apex/CFCProductDetails?id='+ productDetail.Id);    
            }
            
            page.setRedirect(true);
            return page;
        } catch(Exception ex){
            system.debug('ERROR >>> AddFavorite() >>> ' + ex);
            return null;
        }
        
        
    }
    
    public pageReference RemoveFavorite(){
        try{
            system.debug('CFCProductDetailsController.RemoveFavorite()');
            
            Favorite__c fav = FavoriteDao.getInstance().getByUserIdAndProductId(UserInfo.getUserId(), productDetail.Id);
            if(fav != null){
                FavoriteDao.getInstance().setActive(fav, False);
            }     
            
            PageReference page;
            
            //verify if link to pardot is active
            if(isPardotActive == true && !string.isBlank(productDetail.Favorite_Custom_Redirect_Pardot__c)){
                page = new PageReference(productDetail.Favorite_Custom_Redirect_Pardot__c);    
            } else {
                page = new PageReference('/apex/CFCProductDetails?id='+ productDetail.Id);    
            }  
            
            page.setRedirect(true);
            return page;
        } catch(Exception ex){
            system.debug('ERROR >>> RemoveFavorite() >>> ' + ex);
            return null;
        }
        
        
    }
    
    
    
    public void GenerateAttachmentImagesMap(){
        system.debug('CFCProductDetailsController.GenerateAttachmentImagesMap()'); 
        
        //get record type for videos to media center
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_Media_Center');
        
        //get attachment__c type of images to meida center
        List<Attachments__c> lstAttach = AttachmentCustomDao.getInstance().listByProductIdAndRecordTypeId(productDetail.Id, rType.Id);
        system.debug('CFCProductDetailsController.GenerateAttachmentImagesMap.lstAttach >>> ' + lstAttach);
        
        //set id items
        Set<String> setAttach = new Set<String>();
        for(Attachments__c att : lstAttach){
            setAttach.add(att.Id);
        }
        
        List<Attachment> lstImages = AttachmentDao.getInstance().listBySetParentId(setAttach);        
        system.debug('CFCProductDetailsController.GenerateAttachmentImagesMap.itemsDownload >>> ' + lstImages);
        
        //populate map images
        mapAttachmentImages = new Map<Integer,Attachment>();
        indexOfImages = 0;
        for(Attachment item :  lstImages){            
            mapAttachmentImages.put(indexOfImages,item);                
            indexOfImages = IndexOfImages + 1;
            showTitlePhotograph = true;
        }
        
        system.debug('CFCProductDetailsController.GenerateAttachmentImagesMap.mapAttachmentImages >>> ' + mapAttachmentImages);
    }
    
    public pageReference RedirectToHome(){
        try{ 
            system.debug('CFCProductDetailsController.RedirectToHome()');            
            addRecentlyViewed();
            
            verifyIsView();
            
            if(!isView){
                //if product doesn't published we redirect the user to home page
                if(productDetail.Publication_Status__c == null || !productDetail.Publication_Status__c.equalsIgnoreCase('Published')){
                    PageReference page = new PageReference('/apex/CFCHome');    
                    page.setRedirect(true);
                    return page;
                } 
            }
                   
            return null;
            
        } catch(Exception ex){
            system.debug('ERROR >>> RedirectToHome() >>> ' + ex);
            return null;
        }
        
       
    }
    
    public void GenerateAttachmentDownload(){
        system.debug(' Inicio CFCProductDetailsController.GenerateAttachmentDownload()'); 
        
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Documents_to_Media_Center');
        system.debug('CFCProductDetailsController.GenerateAttachmentDownload.rType >>> ' + rType); 
        
        //get list attchments__c images to home 
        lstAttachmentDownload = AttachmentCustomDao.getInstance().listByProductIdAndRecordTypeId(productDetail.Id, rType.Id);
        documentReferenceGuideownload = DocumentDAO.getInstance().getListDocumentFolderCFC();
        system.debug('Finish CFCProductDetailsController.GenerateAttachmentDownload()'); 
    }
    
    public String getUrlVideo(){ 
        try{
            system.debug('CFCProductDetailsController.getUrlVideo()');
            
            //get record type for videos to media center
            RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Videos_to_Media_Center');
            
            //get attachment__c type of videos to media center
            Attachments__c attach = AttachmentCustomDao.getInstance().getByProductIdAndRecordTypeId(productDetail.Id, rType.Id);
            system.debug('CFCProductDetailsController.getUrlVideo.attach >>> ' + attach);
            
            return attach == null? '' : attach.Video_Link__c;
        } catch(Exception ex){
            system.debug('ERROR >>> addRecentlyViewed() >>> ' + ex);
            return '';
        }
        
        
    }
    
    public String getImageProductDetail() {
        try{
            system.debug('CFCProductDetailsController.getImageProductDetail()');
            
            //get record type for images to product detail
            RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_product_detail');
            
            //get attachment__c type of image_for_product_details
            Attachments__c attach = AttachmentCustomDao.getInstance().getByProductIdAndRecordTypeId(productDetail.Id, rType.Id);
            system.debug('CFCProductDetailsController.getImageProductDetail.attach >>> ' + attach);
            
            if(attach != null){
                Attachment itemImage = AttachmentDao.getInstance().getByParentId(attach.Id); 
                system.debug('CFCProductDetailsController.getImageProductDetail.itemImage >>> ' + itemImage);            
                return itemImage.Id;
            } else{
                return 'no image';
            } 
        } catch(Exception ex){
            system.debug('ERROR >>> getImageProductDetail() >>> ' + ex);
            return 'no image';
        }                   
    }
}