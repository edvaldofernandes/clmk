// Used by DIM - Market Intelligence.
public class DIM_TestUtils {

  public static Boolean enableIsRunningTest = true;

  public static Boolean isRunningTest() {
    return Test.isRunningTest() && enableIsRunningTest;
  }
}