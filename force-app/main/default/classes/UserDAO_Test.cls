/**
* @author Marcilio Leite de Souza
* @date 30/05/2018
* @description: TEST CLASS FOR UserDAO
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           12 JUN 2018             Original Version
**/
@isTest
private class UserDAO_Test {
	
    @isTest
    static void it_should_return_user_by_id(){

        Contact c = create_contact_test_data();
        User uData = create_user_test_data(c);
        
        User u = UserDAO.getInstance().getUserById(uData.Id);
        
        Test.startTest();
        System.assertEquals(uData.Id, u.Id, 'id must be the same');
        Test.stopTest();
    }
    
    @isTest
    static void it_should_return_user_by_name(){

        Contact c = create_contact_test_data();
        User uData = create_user_test_data(c);
        
        User u = UserDAO.getInstance().getByUserName(uData.Username);
        
        Test.startTest();
        System.assertEquals(uData.Username, u.Username, 'Username must be the same');
        Test.stopTest();
    }
    
    @isTest
    static void it_should_return_user_by_federation_id(){

        Contact c = create_contact_test_data();
        User uData = create_user_test_data(c);
        
        User u = UserDAO.getUserByFederationIdentifier(uData.FederationIdentifier);
        
        Test.startTest();
        System.assertEquals(uData.FederationIdentifier, u.FederationIdentifier, 'FederationIdentifier must be the same');
        Test.stopTest();
    }
    
    @isTest
    static void it_should_return_user_by_contact_id(){

        Contact c = create_contact_test_data();
        User uData = create_user_test_data(c);
        
        User u = UserDAO.getPortalUserByContactId(c.Id);
        
        Test.startTest();
        System.assertEquals(uData.ContactId, u.ContactId, 'ContactId must be the same');
        Test.stopTest();
    }
    
    @isTest
    static void it_check_permission_set_assigned(){

        Contact c = create_contact_test_data();
        User u = create_user_test_data(c);
        
        Test.startTest();
        
        /** Hard code MyOpereng PermissionSet Id **/
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            create_permission_set_assign(u.Id, '0PS0H0000011M0W');
        }

        System.assertEquals(true, UserDAO.isPermissionSetAssingUser(u.Id, '0PS0H0000011M0W'), 'The assingment has to be true');
        Test.stopTest();
    }

    @isTest
    static void it_upsert_permission_set_to_user(){

        Contact c = create_contact_test_data();
        User u = create_user_test_data(c);
        
        Test.startTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            UserDAO.upsertUserPermission(u.Id, '0PS0H0000011M0W');
        }

        List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment
                                             WHERE AssigneeId = :u.Id
                                               AND PermissionSetId = '0PS0H0000011M0W' LIMIT 1];
        
        System.assertNotEquals(0, psa.size());
        Test.stopTest();
    }
    
    @isTest
    static void it_upsert_permission_set_to_user_future(){

        Contact c = create_contact_test_data();
        User u = create_user_test_data(c);
        
        Test.startTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            UserDAO.upsertUserPermissionFuture(u.Id, '0PS0H0000011M0W');
        }

        List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment
                                             WHERE AssigneeId = :u.Id
                                               AND PermissionSetId = '0PS0H0000011M0W' LIMIT 1];
        
        System.assertEquals(0, psa.size());
        Test.stopTest();
    }
    
    static void create_permission_set_assign(Id idUser, Id idPermission){
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = idUser;
        psa.PermissionSetId = idPermission;
        system.debug('create_permission_set_assign.idPermission: ' + idPermission);
        insert psa;
    }
    
    static User create_user_test_data(Contact c){
        Id idProfile = [Select Id From Profile Where Name = 'CRM Community Login'].Id;
        
        User u = new User();
        u.Username = 'usernametest@mail.com.br';
        u.FirstName = 'Test ';
        u.LastName = 'User 1';
        u.Email = 'email1@mail.com.br';
        u.Alias = 'Alias1'; 
        u.CommunityNickname = 'Community Nickname1';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_Us'; 
        u.EmailEncodingKey = 'ISO-8859-1'; 
        u.LanguageLocaleKey = 'en_Us'; 
        u.FederationIdentifier = 'teste1';
        u.FlyEmbraerUserId__c = 'teste1';
        u.IsActive = true;
        u.ProfileId = idProfile;
        u.ContactId = c.Id;
        
        insert u;
        return u;
    }
    
    static Account create_account_test_data(){
        Account a = new Account();
        a.BillingCountry = 'Brazil';
        a.Name = 'Test';
        a.Company_Nickname__c = 'testNickname';
        a.FlyEmbraerId__c = '99999';
        insert a;
        return a;
    }
    
    static Contact create_contact_test_data(){
        Contact c = new Contact();
        c.Title = 'testTitle';
        c.LastName = 'test LastName';
        c.Gender__c = 'Male';
        c.Contact_Status__c ='Active';
        c.Email = 'test@test.com.br';
        c.FlyEmbraerUserId__c = 'testFlyEmbraerId';
        c.AccountId = create_account_test_data().Id;
        
        insert c;
        return c;
    }
}