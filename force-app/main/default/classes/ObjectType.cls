public interface ObjectType {
    
    String objectType(AccountInformation accountInformation);
}