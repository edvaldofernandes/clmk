/*******************************************************************************
*                               Embraer - 2015
*-------------------------------------------------------------------------------
*
* NAME: ServiceContract_SharingRule
* AUTHOR: Bruno Severino                                        DATE: 28/04/2015
*
*******************************************************************************/
public class ServiceContract_SharingRule {
    //variables        
    public List<ServiceContractShare> sharestoCreate = new List<ServiceContractShare>();   
    public Map<string, string> groupList = new Map<String, String>();
    public Map<string, string> ownerRoleList = new Map<String, String>();
    public List<Group> groupList_temp = [SELECT Id,DeveloperName FROM Group WHERE Type = 'Role'];
    public static PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Opp_SC_Modify_All_Records'];
    
    public void newSharingRule()
    {
        TriggerUtils.assertTrigger();       
        
        //map all the Role Group        
        //List<Group> groupList_temp = [SELECT Id,DeveloperName FROM Group WHERE Type = 'Role'];
        for(Group groups : groupList_temp){
            groupList.put(groups.DeveloperName, groups.Id);
        }
        //map all the Opportunity
        List<ServiceContract> listSC= new List<ServiceContract>();
        for ( ServiceContract sc : (list<ServiceContract>) trigger.new )
            if ( TriggerUtils.wasChanged(sc, ServiceContract.RecordTypeId) || TriggerUtils.wasChanged(sc, ServiceContract.OwnerId)) //only if the owner or service program has changed
                listSC.add(sc);                  
        //if nothing has changed, then just stop and return.
        if ( listSC.isEmpty() ) return;  
        
        //get all sharerules
        for(ServiceContract serviceContract : listSC )
            createSharingRules(serviceContract);
        
        //insert the sharerules in database
        if (!sharestoCreate.isEmpty())  
        {
        	if(!system.isBatch())
        	{
            	insertPermissionSet(listSC[0].OwnerId);
            	insert sharestoCreate;
            	deletePermissionSet(listSC[0].OwnerId);
        	}
        }
    }
    
    @future
    public static void insertPermissionSet(Id OwnerId){
        Id ownerProfileID = OwnerId;
        Id profileID = [SELECT ProfileId FROM User WHERE Id = :ownerProfileID].Id;
        //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Opp_SC_Modify_All_Records'];
        system.debug('owner: '+ownerProfileID+' / profile: '+profileID);
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.Id, AssigneeId = profileID);
        if(!Test.isRunningTest())
            insert psa;       
    }
    
    @future
    public static void deletePermissionSet(Id OwnerId){
        //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Opp_SC_Modify_All_Records'];
        PermissionSetAssignment[] psa = [SELECT Id FROM PermissionSetAssignment WHERE PermissionSetId = :ps.Id];
        
        if(!Test.isRunningTest())
            delete psa;       
    }
    
    /**
     * Method to create the SharesRules
     */
    public void createSharingRules(ServiceContract serviceContract){  
        //variaveis
        String ownerUserRole = serviceContract.Owner_Role__c;
        //String serviceProgramType = serviceContract.Service_Program_type__c;
        Id idRecordType = serviceContract.RecordTypeId;
        Id technicalServicesId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
        Id materialSolutionsId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
        Id maintenanceServicesId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Maintenance Services').getRecordTypeId();
        String embraerSiteName = serviceContract.Embraer_Site__c;
        ServiceContractShare cs;
        ID groupId = null;          
        ID groupId_2 = null;
        ID groupId_3 = null;        
        //As Oportunidades criadas por Account Embraer Site "Embraer ****" e Owner Role "SJK_SS_CONTRACT_ADMIN" / "SJK_ACC_MANAGER" / "SJK_FLIGHT_OPS_LIASION" pode ser vista por ****_ACC_MANAGER e ****_BDM
        if((ownerUserRole == 'SJK_SS_CONTRACT_ADMIN') || (ownerUserRole == 'SJK_ACC_MANAGER') || (ownerUserRole == 'SJK_FLIGHT_OPS_LIASION')){
            cs = new ServiceContractShare();                            //****_ACC_MANAGER
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;               
            groupId = null;
            groupId_2 = null;
            if(embraerSiteName == 'Embraer Europe and Central Asia'){
                groupId = groupList.get('ERCA_ACC_MANAGER');
                groupId_2 = groupList.get('ERCA_BDM');
            }else if(embraerSiteName == 'Embraer China'){
                groupId = groupList.get('CHI_ACC_MANAGER');
                groupId_2 = groupList.get('CHI_BDM');
            }else if(embraerSiteName == 'Embraer North America'){
                groupId = groupList.get('NA_ACC_MANAGER');
                groupId_2 = groupList.get('NA_BDM');
            }else if(embraerSiteName == 'Embraer Middle East & Africa'){
                groupId = groupList.get('MEA_ACC_MANAGER');
                groupId_2 = groupList.get('MEA_BDM');
            }else if(embraerSiteName == 'Embraer Asia Pacific'){
                groupId = groupList.get('AP_ACC_MANAGER');
                groupId_2 = groupList.get('AP_BDM');
            }else if(embraerSiteName == 'Embraer Latin America'){
                groupId = groupList.get('LATAM_ACC_MANAGER');
                groupId_2 = groupList.get('LATAM_BDM');
            }        
            
            if (groupId != null){                
                cs.UserOrGroupId = groupId;
                sharesToCreate.add(cs);                 
            }
            
            cs = new ServiceContractShare();                            //****_BDM
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;
            if (groupId_2 != null){                
                cs.UserOrGroupId = groupId_2;
                sharesToCreate.add(cs);                 
            }
            
            cs = new ServiceContractShare();
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;  
            groupId = groupList.get('SJK_ACC_MANAGER');          
            if (groupId != null){     
                cs.UserOrGroupId =  groupId;
                sharesToCreate.add(cs);
            }

        }
        //------------ FIM ---------------
        
        //Service Contract criadas pelos SITES em "Material Services" serão Vistas por SJK_BDM_MAT_SVCS
        //Service Contract criadas pelos SITES em "Maintenance Services" serão Vistas por SJK_BDM_MRO_SVCS
        //Service Contract criadas pelos SITES em "Technical Services" serão Vistas por SJK_BDM_TECH_SVCS
        cs = new ServiceContractShare();
        cs.AccessLevel = 'Edit';
        cs.RowCause = 'Manual';
        cs.ParentId = serviceContract.Id;
        groupId = null;
        if(idRecordType == materialSolutionsId){
            groupId = groupList.get('SJK_BDM_MAT_SVCS');                
        } else if(idRecordType == maintenanceServicesId){
            groupId = groupList.get('SJK_BDM_MRO_SVCS'); 
        } else if (idRecordType == technicalServicesId){     
            groupId = groupList.get('SJK_BDM_TECH_SVCS');
        }
        
        if (groupId != null){     
            cs.UserOrGroupId =  groupId;
            sharesToCreate.add(cs);
        }
        //------------ FIM ---------------
         
                    
        if(idRecordType  == technicalServicesId){
            
            //As Service Contract criada pelo BDM / Sales Manager / SS contract admim  se for de Techical service o ACC manager do site.    
            cs = new ServiceContractShare();
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;         
            groupId = null;
            if((ownerUserRole == 'ERCA_BDM') || (ownerUserRole == 'ERCA_SALES_MANAGER') || (ownerUserRole == 'ERCA_SS_CONTRACT_ADMIN')){
                groupId = groupList.get('ERCA_ACC_MANAGER');
            }else if((ownerUserRole == 'CHI_BDM') || (ownerUserRole == 'CHI_SALES_MANAGER') || (ownerUserRole == 'CHI_SS_CONTRACT_ADMIN')){
                groupId = groupList.get('CHI_ACC_MANAGER');
            }else if((ownerUserRole == 'NA_BDM') || (ownerUserRole == 'NA_SALES_MANAGER') || (ownerUserRole == 'NA_SS_CONTRACT_ADMIN')){
                groupId = groupList.get('NA_ACC_MANAGER');
            }else if((ownerUserRole == 'MEA_BDM') || (ownerUserRole == 'MEA_SALES_MANAGER') || (ownerUserRole == 'MEA_SS_CONTRACT_ADMIN')){
                groupId = groupList.get('MEA_ACC_MANAGER');
            }else if((ownerUserRole == 'AP_BDM') || (ownerUserRole == 'AP_SALES_MANAGER') || (ownerUserRole == 'AP_SS_CONTRACT_ADMIN')){
                groupId = groupList.get('AP_ACC_MANAGER');
            }else if((ownerUserRole == 'LATAM_BDM') || (ownerUserRole == 'LATAM_SALES_MANAGER') || (ownerUserRole == 'LATAM_SS_CONTRACT_ADMIN')){
                groupId = groupList.get('LATAM_ACC_MANAGER');
            }        
            if (groupId != null){                
                cs.UserOrGroupId = groupId;
                sharesToCreate.add(cs);                 
            }
            //------------ FIM ---------------
           
            //Service Contract criadas pelos SITES em "Technical Services" serão Vistas por SJK_ACC_MANAGER
            cs = new ServiceContractShare();
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;  
            groupId = null;       
            groupId = groupList.get('SJK_ACC_MANAGER');          
            if (groupId != null){     
                cs.UserOrGroupId =  groupId;
                sharesToCreate.add(cs);
            }
            //------------ FIM ---------------          
                        
            //Service Contract criadas nos sites por "***_ACC MANAGER" em "Technical Services" serão Vistas por SJK_FLIGHT_OPS_LIASION / SJK_SS_CONTRACT_ADMIN
            cs = new ServiceContractShare();
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;         
            groupId = null;
            groupId_2 = null;
            if((ownerUserRole == 'ERCA_ACC_MANAGER') || (ownerUserRole == 'CHI_ACC_MANAGER') || (ownerUserRole == 'NA_ACC_MANAGER') || (ownerUserRole == 'MEA_ACC_MANAGER') || (ownerUserRole == 'AP_ACC_MANAGER') || (ownerUserRole == 'LATAM_ACC_MANAGER')){
                groupId = groupList.get('SJK_FLIGHT_OPS_LIASION');
                groupId_2 = groupList.get('SJK_SS_CONTRACT_ADMIN');
            }                      
            if (groupId != null){                   
                cs.UserOrGroupId =  groupId;
                sharesToCreate.add(cs);
            }
            cs = new ServiceContractShare();
            cs.AccessLevel = 'Edit';
            cs.RowCause = 'Manual';
            cs.ParentId = serviceContract.Id;                  
            if (groupId_2 != null){                 
                cs.UserOrGroupId =  groupId_2;
                sharesToCreate.add(cs);
            }
            //------------ FIM ---------------
        } 
    }
}