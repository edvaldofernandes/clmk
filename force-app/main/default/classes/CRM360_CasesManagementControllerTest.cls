@isTest
public class CRM360_CasesManagementControllerTest {
    
    private static String accountName = 'Test Account';
    private static String case1subject = 'Case1';
    
    @testSetup
    static void setup(){

        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);

        insert accountsToInsert;
        
        List<Case> casesToInsert = new List<Case>();
        
        Case case1 = new Case();
        case1.Subject = case1subject;
        case1.AccountId = account.Id;
        case1.Status = 'OPEN';
        case1.CRM360_view__c = true;
        case1.CRM360_Category__c = 'Finance';
        case1.CRM_Case_Actual_Commitment_Date__c = date.today().addDays(-1);
        casesToInsert.add(case1);
        
        insert casesToInsert;
    }

    
    @isTest
    public static void testCaseAll(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        //List<Case> Cases = [SELECT Id FROM Case WHERE Subject = :case1subject];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CasesManagementPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        pageRef.getParameters().put('category', 'ALL');

        Test.setCurrentPage(pageRef);
        
        CRM360_CasesManagementController testAccPlan = new CRM360_CasesManagementController();
            
        testAccPlan.loadCategories();
        testAccPlan.getAccount();
        System.Debug(testAccPlan.getCategories());
        System.Debug('Cases ' + testAccPlan.getCases());
        
        Test.stopTest();
    }
    
    @isTest
    public static void testCaseFinance(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        //List<Case> Cases = [SELECT Id FROM Case WHERE Subject = :case1subject];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_CasesManagementPage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));
        pageRef.getParameters().put('category', 'Finance');

        Test.setCurrentPage(pageRef);
        
        CRM360_CasesManagementController testAccPlan = new CRM360_CasesManagementController();
            
        testAccPlan.loadCategories();
        testAccPlan.getAccount();
        System.Debug(testAccPlan.getCategories());
        System.Debug('Cases ' + testAccPlan.getCases());
        
        Test.stopTest();
    }
}