@isTest
private class REC_DOCControllerTest {
  private testMethod static void getNewImagesTest() {
    Communications__c rec = createREC('newImagesTest');
    rec.summary__c = '<p style="text-align: center;"><img src="https://commercialaviation--fltperf--c.documentforce.com/servlet/rtaImage?refid=0EM0m0000005R6U" alt="test2808_01"></p><p>In Sept 21st 2017, Embraer received a report of RAT inoperative due to tangling of the detached <u>RAT door seal</u> in the RAT rotor. The event occurred during an annual in-flight RAT operational check required by local authority, in addition to the MRBR task.</p><p><br></p><p style="text-align: center;"><img src="https://commercialaviation--fltperf--c.documentforce.com/servlet/rtaImage?refid=0EM0m0000005B2U" alt="test2808_02"></p>';

    Map<String, List<Object>> matches = REC_DOCController.getNewImages(
      rec
    );
    system.assertEquals(
      2,
      matches.keySet().size(),
      '[ERROR] Failed to get new images from rich text fields!'
    );
  }

  private testMethod static void createNewPublicDocumentTest() {
    Id dummyId = [SELECT id FROM User LIMIT 1].Id;
    List<Id> docIds = new List<Id>{
      REC_DOCController.createNewPublicDocument(
        'test',
        Blob.valueOf('test'),
        'image/png',
        dummyId
      ),
      REC_DOCController.createNewPublicDocument(
        'this_is_a_test',
        Blob.valueOf('this_is_a_test'),
        'image/png',
        dummyId
      )
    };
    system.assertEquals(
      docIds.size(),
      2,
      '[EROR] Failed to create a new public document!'
    );
  }

  private testMethod static void changeImageUrlTest() {
    Communications__c rec = createREC('newImagesTest');
    rec.summary__c = 'test';
    Database.insert(rec);

    rec = [
      SELECT
        id,
        Summary__c,
        Investigation__c,
        References__c,
        Recommendation__c,
        Next_steps__c,
        Update__c
      FROM Communications__c
      LIMIT 1
    ];

    Id dummyId = [SELECT id FROM User LIMIT 1].Id;

    Id docId = REC_DOCController.createNewPublicDocument(
      'testt',
      Blob.valueOf('testt'),
      'image/png',
      dummyId
    );

    REC_DOCController.changeImageUrl('test', 'summary__c', rec, docId);

    rec = [
      SELECT
        id,
        Summary__c,
        Investigation__c,
        References__c,
        Recommendation__c,
        Next_steps__c,
        Update__c
      FROM Communications__c
      LIMIT 1
    ];

    system.assertEquals(
      REC_DOCController.getNewImages(rec).keySet().size(),
      0,
      rec.summary__c
    );
  }

  private static Communications__c createREC(String name) {
    Id idRECRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('REC')
      .getRecordTypeId();
    Communications__c rec = new Communications__c();
    rec.RecordTypeId = idRECRecordType;
    rec.communication_status__c = 'draft';
    rec.reference_date__c = system.today();
    rec.ata_chapter__c = '00 GENEREAL';
    rec.technology__c = 'AMS';
    rec.fleet_type__c = '195-E2';
    rec.Name = name;
    return rec;
  }
}