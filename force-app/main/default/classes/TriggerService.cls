public class TriggerService {
    
     public static List<String> createdByList(String accountId){
        
        List<String> createdIdList = new List<String>();
        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(accountId))
            createdIdList.add(accountHistory.CreatedById);
         
         return createdIdList;
    }
    
    public static Map<id,User> findUserByCreatedById(String accountId){
        
        Map<Id,User> userMap = new Map<Id,User>([SELECT id, phone, E_mail__c 
                                                 FROM User 
                                                 WHERE id IN :createdByList(accountId)]);
        
        return userMap;
    }
}