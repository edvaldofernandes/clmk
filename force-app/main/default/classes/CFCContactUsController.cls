/**
* Controller class for the visualforce page CFCContactUsController.
* Name: CFCContactUsController
* @author - Guilherme Nascimento - gjesus@deloitte.com
* @version 2.0 - 15/12/2015
**/

public without sharing class CFCContactUsController {

    public User userContact {get;set;}
    public Case newCase {get;set;}
    public Boolean hasAccountTeamMember {get;set;}
    public String error {get;set;}
    public String success {get;set;}
    public String caseNumber {get;set;}
    public String queryStringId {get;set;}
    public Attachment attachment{get; set;}
    public List<Attachment> listNameAtt {get;set;}
    public String removeAttachmentId{get; set;}
    
    public User userCAM{get; set;}
    
    Public String prodCaseName{get; set;}
    
    public CFCContactUsController(){
        system.debug('CFCContactUsController.version.2');
        userContact = new User();

        attachment = new Attachment();
        listNameAtt = new List<Attachment>();
        queryStringId = ApexPages.currentPage().getParameters().get('id');        
        newCase = new Case();
        
        //userContact = UserDAO.getInstance().getUserById('00556000000UuupAAC');
        userContact = UserDAO.getInstance().getUserById(UserInfo.getUserId());     
        system.debug('CFCContactUsController.userContact >>> ' + userContact);
		if(queryStringId != null) {            
        	//newCase.Product = Product2DAO.getInstance().getProductByCaseId(queryStringId);
        	Product2 prodCase2 = Product2DAO.getInstance().getProductByCaseId(queryStringId);
            system.debug('CFCContactUsController.prodCase >>> ' + prodCase2); 
            prodCaseName =  prodCase2.Name;          
            newCase.ProductId = prodCase2.id;
        }
        newCase.AccountId = userContact.Contact.AccountId; 
        newCase.ContactId = userContact.ContactId;
        newCase.Status = 'Open';        
        RecordType recordType = RecordTypeDao.getInstance().getByDeveloperName('CFC_Cases');
        newCase.RecordTypeId = recordType.id;
        newCase.OwnerId = GenerateOwnerId();       
    }
    
    public String GenerateOwnerId() {
        system.debug('CFCContactUsController.GenerateOwnerId()');
        hasAccountTeamMember = userContact.Contact.Account.CAM__c == null ? false : true;
        system.debug('CFCContactUsController.GenerateOwnerId.userContact.Contact.Account.CAM__r.id >>> ' + userContact.Contact.Account.CAM__r.id);
        if(hasAccountTeamMember) {
            return userContact.Contact.Account.CAM__r.id;
        } else {
            CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults();            
            userCAM = UserDao.getInstance().getByUserName(cs.User_Name__c);
            system.debug('CFCContactUsController.GenerateOwnerId.userCAM >> ' + userCAM);
            return userCAM.id;
        }
    }

    public pageReference Submit() {
        try {  
        	if(String.isBlank(newCase.Type_of_Service__c) || String.isBlank(newCase.Description)) {
        		error = 'All fields must be filled';
        		success = ''; 
        	} 
        	else 
        	{ 
	            Database.upsert(newCase);
	            caseNumber = CaseDAO.getInstance().getCaseNumber(newCase.id);
                system.debug('caseNumber >>>' + caseNumber);
                //success = 'success'; 
                PageReference page;
                if(String.isBlank(queryStringId)){
                    page = new PageReference('/apex/CFCContactUSFinish');
                } else {
                    page = new PageReference('/apex/CFCContactUSFinish?id='+queryStringId);
                }
		        page.setRedirect(true);
		        return page;				   	
        	}

        } catch(exception ex) {
            //error = 'an error occurred in the system, ask the administrator';
            //for (Integer i = 0; i < e.getNumDml(); i++) {
            //    system.debug('error: ' + e.getDmlMessage(i));                
            //}
			system.debug('ERROR >>> ' + ex);            
        }        
        return null;
    }
    
    public pageReference insertAttachment() {
        try{
            system.debug('CFCContactUsController.insertAttachment.attachment: ' + attachment);
            /*if(newCase == null){
                Database.insert(newCase);
            } else {
                Database.update(newCase);
            }*/		
            Database.upsert(newCase);
            attachment.ParentId = newCase.Id;
            Database.insert(attachment);
            //attachment.Body = null;
            listNameAtt.add(attachment);
            attachment = new Attachment();
        }catch(Exception ex){
            system.debug('ERROR >>> ' + ex);   
        }    
        return null;
    }
    public PageReference removeAtt() {        
        system.debug('CFCContactUsController.removeAtt.removeAttachmentId: ' + removeAttachmentId);
        for(Integer i=0; i<listNameAtt.size(); i++) {
            if(removeAttachmentId == listNameAtt.get(i).id) {
                Database.delete(listNameAtt.get(i).id);
                listNameAtt.remove(i);
            }
        }
        return null;
    }
}