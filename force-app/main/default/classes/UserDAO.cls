/* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 06/01/2016
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           29 MAY 2018             Add FederationIdentifier return Community User
*                                                           Methods: getUserByQuery , getUserByFederationIdentifier, getPortalUserByContactId
*                                                                    upsertUserPermission, isPermissionSetAssingUser
**/
public without sharing class UserDAO {
    
    private static final UserDAO instance = new UserDAO();    
    
    private UserDAO(){
    }    
    
    public static UserDAO getInstance(){
        return instance;
    }
    
    public User getUserById(String idUser) {        
        List<User> listUser = database.query(' Select ' + utils.getAllFields('User') + 
                                             ' ,Contact.AccountId, Contact.Account.CAM__c, Contact.Email, Contact.Phone, Contact.Name, Contact.Account.Name,'+
                                             ' Account.Name, Contact.Account.CAM__r.Name, Contact.Account.CAM__r.E_mail__c,'+
                                             ' Contact.Account.CAM__r.Phone, Contact.Account.CAM__r.lastName, Contact.Account.CAM__r.FirstName,Contact.Account.id'+
                                             ' FROM User WHERE Id = \'' + String.escapeSingleQuotes(idUser) + '\' LIMIT 1 ');
        if(listUser.size() > 0){
            return listUser[0];
        }
        return null;
    }
    
    public User getByUserName(String userName){        
        List<User> listUser = database.query(' Select ' + Utils.getAllFields('User') + ' FROM User WHERE UserName = \'' + String.escapeSingleQuotes(userName) + '\' ');
        
        if(listUser.size() > 0){
            return listUser[0];
        }
        
        return null;
    }
    
    /**
	* @description : User Active by federation Id 
    * @param String idFederation : FederationIdentifier filter
	* @return User or Null
	*/
    public static User getUserByFederationIdentifier(String idFederation){
        String str = 'SELECT ' + Utils.getAllFields('User') + ' FROM User ';
               str +='WHERE FederationIdentifier = \'' + String.escapeSingleQuotes(idFederation) + '\' ';
               str +='AND IsActive  = true ';
        return getUserByQuery(str);
    }
    
    /**
	* @description : User by a Contact Id
    * @param String isUser : User Id for the upsert
    * @param String idContact : Contact Id filter
	* @return User or Null
	*/
    public static User getPortalUserByContactId(String idContact){
        String str = 'SELECT ' + Utils.getAllFields('User') + ' FROM User ';
        str +='WHERE ContactId = \'' + String.escapeSingleQuotes(idContact) + '\' ';
        return getUserByQuery(str);
    }
    
    /**
	* @description : Check if a permission is assing to a user
    * @param String isUser : User Id for the check
    * @param String idPermission : Permission set Id for the check
	* @return Boolean
	*/
    public static Boolean isPermissionSetAssingUser(String idUser, String idPermission){
        System.debug('idUser '+idUser);
        System.debug('idPermission '+idPermission);
        List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment
                                             WHERE AssigneeId = :idUser
                                               AND PermissionSetId = :idPermission LIMIT 1];
        System.debug('permission '+psa);
        return psa.size() > 0 ? true : false;
    }
    
    /**
	* @description : Future upsert permission set for GEN_UserFlyEmbraer
    * @param String isUser : User Id for the upsert
    * @param String idPermission : Permission set Id for the upsert
	* @return void
	*/
    @future
    public static void upsertUserPermissionFuture(String idUser, String idPermission){
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = idUser;
        psa.PermissionSetId = idPermission;
        upsert psa;
    }
    
     /**
	* @description : Upsert permission set for GEN_UserFlyEmbraer
    * @param String isUser : User Id for the upsert
    * @param String idPermission : Permission set Id for the upsert
	* @return void
	*/
    public static void upsertUserPermission(String idUser, String idPermission){
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = idUser;
        psa.PermissionSetId = idPermission;
        upsert psa;
    }
    
    /**
	* @description : User by query string
    * @param String query : Query in string format
	* @return User : First User find
	*/
    private static User getUserByQuery(String query){
        List<User> listUser = database.query(query);
        
        if(listUser.size() > 0){ return listUser[0];}
        
        return null;
    }
}