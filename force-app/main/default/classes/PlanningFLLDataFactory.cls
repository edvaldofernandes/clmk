@isTest
public class PlanningFLLDataFactory {
    
    @isTest public static void createTestDataAndPageRef(){
        createTestData();
        setPage();
    }
    
    @isTest public static void createTestDataAndPageRefLBGCOM(){
        createTestData();
        setPageLBGCOM();
    }
    
    @isTest public static void createTestDataAndPageRefE2(){
        createTestData();
        setPageE2();
    }
    
    @isTest public static void createTestDataAndPageRefE2Mode(){
        createTestData();
        setPageE2Mode();
    }
    
    @isTest public static void createTestData(){
        //create 3 parts
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='HEA3610E145', Ecode__c='7063957', Top_Most__c='7430', Prime__c='0', Program__c='190', Is_Part_Serialized__c='Serialized', Top_Removal_Commercial__c=true, Top_Missed__c=true));
    	parts.add(new LLPDatabase__c(Name='170-55145-401', Ecode__c='1342430', Top_Most__c='7430', Prime__c='0', Program__c='190', Is_Part_Serialized__c='Serialized', Top_Removal_Commercial__c=true, Top_Missed__c=true));
        parts.add(new LLPDatabase__c(Name='170-55145-403', Ecode__c='7430', Top_Most__c='7430', Prime__c='X', Program__c='190', Is_Part_Serialized__c='Serialized', Top_Removal_Commercial__c=true, Top_Missed__c=true));
        insert parts;
        
        //create 3 stock info items with part numbers: HEA3610E145, 170-55145-401 and 170-55145-403
    	List<Stock_Info_FLL__c> stockInfos = new List<Stock_Info_FLL__c>();
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[0].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='FLL COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[1].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='FLL COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='FLL COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='OSS-LAX-AZU-CPZ-SAE-AZUE2-SKW-AZN', Is_Summary_Record__c=true, OH_Total__c=1, USAGE_12M__c=13, Region_and_Segment__c='FLL COM'));
        //LBG COM records
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='LBG COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=true, OH_Total__c=1, USAGE_12M__c=13, Region_and_Segment__c='LBG COM'));
        insert stockInfos;
        
        //create 4 Parts at repair with part numbers: HEA3610E145, HEA3610E145, 170-55145-401 and 170-55145-403, one has awb and deliv date
        List<Repair_Information__c> repairs = new List<Repair_Information__c>();
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[0].id, Site__c='FLL COM', AWB_From_Repair__c='AWB12345AWB', AWB_From_Repair_Deliv_Date__c=date.today()));
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[0].id, Site__c='FLL COM'));
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[1].id, Site__c='FLL COM'));
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[2].id, Site__c='FLL COM'));
        insert repairs;
        
        //create 2 cores with part numbers: 170-55145-401 and 170-55145-403
        List<Core_Information__c> cores = new List<Core_Information__c>();
        cores.add(new Core_Information__c(Part_Number__c=parts[1].id, Support_Plant__c='FLL', Segment__c='Commercial & Defense', Notification__c='300123456'));
        cores.add(new Core_Information__c(Part_Number__c=parts[2].id, Support_Plant__c='FLL', Segment__c='Commercial & Defense', Notification__c='300654321'));
        insert cores;
        
        //create 2 resses part numbers: 170-55145-401 and 170-55145-403
        List<RESS_Info__c> resses = new List<RESS_Info__c>();
        resses.add(new RESS_Info__c(RESS_ID__c='1111', Part_Number__c=parts[1].id, Site__c='FLL', Segment__c='Commercial', Customer_Name__c='EMBRAER SERVICES INC.'));
        resses.add(new RESS_Info__c(RESS_ID__c='2222', Part_Number__c=parts[2].id, Site__c='FLL', Segment__c='Commercial', Customer_Name__c='EMBRAER SERVICES INC.'));
       insert resses;
        
        //create 4 PO with part numbers: HEA3610E145, HEA3610E145, 170-55145-401 and 170-55145-403
        List<PO__c> PO = new List<PO__c>();
        PO.add(new PO__c(Notification__c='1238', Part_Number__c=parts[0].id, Segment__c='FLL'));
        PO.add(new PO__c(Notification__c='4561', Part_Number__c=parts[0].id, Segment__c='FLL'));
        PO.add(new PO__c(Notification__c='1589', Part_Number__c=parts[1].id, Segment__c='FLL'));
        PO.add(new PO__c(Notification__c='3371', Part_Number__c=parts[2].id, Segment__c='FLL'));
        insert PO;
    }
    
    @isTest public static void createTestDataWithCoreComment(){
        //create 3 parts
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='HEA3610E145', Ecode__c='7063957', Top_Most__c='7430', Prime__c='0', Program__c='190', Is_Part_Serialized__c='Serialized', Top_Removal_Commercial__c=true, Top_Missed__c=true));
    	parts.add(new LLPDatabase__c(Name='170-55145-401', Ecode__c='1342430', Top_Most__c='7430', Prime__c='0', Program__c='190', Is_Part_Serialized__c='Serialized', Top_Removal_Commercial__c=true, Top_Missed__c=true));
        parts.add(new LLPDatabase__c(Name='170-55145-403', Ecode__c='7430', Top_Most__c='7430', Prime__c='X', Program__c='190', Is_Part_Serialized__c='Serialized', Top_Removal_Commercial__c=true, Top_Missed__c=true));
        insert parts;
        
        //create usage info items
        Date lastMonth = Date.Today().addMonths(-25);
        List<Usage_Information__c> usageInfos = new List<Usage_Information__c>();
        usageInfos.add(new Usage_Information__c(Part_Number__c=parts[0].id, Customer__c='AZUL LINHAS AEREAS BRASILEIRAS S/A', Notification_Date__c=lastMonth, Exchange_Provided__c=TRUE, Region__c='FLL', Program__c='POOL'));
        usageInfos.add(new Usage_Information__c(Part_Number__c=parts[0].id, Customer__c='AZUL LINHAS AEREAS BRASILEIRAS S/A', Notification_Date__c=lastMonth, Exchange_Provided__c=FALSE, Region__c='FLL', Program__c='POOL'));
        usageInfos.add(new Usage_Information__c(Part_Number__c=parts[0].id, Customer__c='EMBRAER S.A.', Notification_Date__c=lastMonth, Exchange_Provided__c=FALSE, Region__c='FLL', Program__c='POOL'));
        usageInfos.add(new Usage_Information__c(Part_Number__c=parts[0].id, Customer__c='Pool Customer.', Notification_Date__c=lastMonth, Exchange_Provided__c=FALSE, Region__c='FLL', Program__c='POOL'));
        usageInfos.add(new Usage_Information__c(Part_Number__c=parts[0].id, Customer__c='EPEP Customer.', Notification_Date__c=lastMonth, Exchange_Provided__c=FALSE, Region__c='FLL', Program__c='EPEP'));
        usageInfos.add(new Usage_Information__c(Part_Number__c=parts[0].id, Customer__c='Other EPEP Customer.', Notification_Date__c=lastMonth, Exchange_Provided__c=FALSE, Region__c='FLL', Program__c='EPEP'));
        insert usageInfos;
        
        //create 3 stock info items with part numbers: HEA3610E145, 170-55145-401 and 170-55145-403
    	List<Stock_Info_FLL__c> stockInfos = new List<Stock_Info_FLL__c>();
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[0].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='FLL COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[1].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='FLL COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='FLL COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=true, OH_Total__c=1, USAGE_12M__c=13, Region_and_Segment__c='FLL COM'));
        //LBG COM records
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=false, Region_and_Segment__c='LBG COM'));
        stockInfos.add(new Stock_Info_FLL__c(Part_Number__c=parts[2].id, POOL_CUSTOMERS__c='AMC-AZU-TAE', Is_Summary_Record__c=true, OH_Total__c=1, USAGE_12M__c=13, Region_and_Segment__c='LBG COM'));
        insert stockInfos;
        
        //create 4 Parts at repair with part numbers: HEA3610E145, HEA3610E145, 170-55145-401 and 170-55145-403, one has awb and deliv date
        List<Repair_Information__c> repairs = new List<Repair_Information__c>();
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[0].id, Site__c='FLL COM', AWB_From_Repair__c='AWB12345AWB', AWB_From_Repair_Deliv_Date__c=date.today()));
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[0].id, Site__c='FLL COM'));
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[1].id, Site__c='FLL COM'));
        repairs.add(new Repair_Information__c(Name='1', Repair_Part_Number__c=parts[2].id, Site__c='FLL COM'));
        insert repairs;
        
         //create a PO comment object
        List<PO_Comment__c> POComments = new List<PO_Comment__c>();
        POComments.add(new PO_Comment__c(Body__c='this PO is new', Notification__c='902761720'));
        insert POComments;
        
        //create 4 PO with part numbers: HEA3610E145, HEA3610E145, 170-55145-401 and 170-55145-403
        List<PO__c> PO = new List<PO__c>();
        PO.add(new PO__c(Notification__c='1238', Part_Number__c=parts[0].id, Segment__c='FLL'));
        PO.add(new PO__c(Notification__c='4561', Part_Number__c=parts[0].id, Segment__c='FLL COM',PO_Comment__c=POComments[0].id));
        PO.add(new PO__c(Notification__c='1589', Part_Number__c=parts[1].id, Segment__c='FLL',PO_Comment__c=POComments[0].id));
        PO.add(new PO__c(Notification__c='902761720', Part_Number__c=parts[2].id, Segment__c='FLL3',PO_Comment__c=POComments[0].id));
        insert PO;
        
        //create a core comment object
        List<Core_Comment__c> coreComments = new List<Core_Comment__c>();
        coreComments.add(new Core_Comment__c(Body__c='this core has an awb 1234567890', Notification__c='300654321'));
        insert coreComments;
       
        //create 2 cores with part numbers: 170-55145-401 and 170-55145-403
        List<Core_Information__c> cores = new List<Core_Information__c>();
        cores.add(new Core_Information__c(Part_Number__c=parts[1].id, Support_Plant__c='FLL', Segment__c='Commercial & Defense', Notification__c='300123456', Epool_Status__c='Core Shipped'));
        cores.add(new Core_Information__c(Part_Number__c=parts[2].id, Support_Plant__c='FLL', Segment__c='Commercial & Defense', Notification__c='300654321', Core_Comment__c=coreComments[0].id));
        insert cores;
        
        //create 2 resses part numbers: 170-55145-401 and 170-55145-403
        List<RESS_Info__c> resses = new List<RESS_Info__c>();
        resses.add(new RESS_Info__c(RESS_ID__c='1111', Part_Number__c=parts[1].id, Site__c='FLL', Segment__c='Commercial', Customer_Name__c='EMBRAER SERVICES INC.'));
        resses.add(new RESS_Info__c(RESS_ID__c='2222', Part_Number__c=parts[2].id, Site__c='FLL', Segment__c='Commercial', Customer_Name__c='EMBRAER SERVICES INC.'));
        insert resses;
    }
    
    @isTest private static void setPage(){
        //set the current page to planning report fll
        PageReference pageRef = Page.Planning_Report_FLL;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('topmost', '7430');	    
    }
    
    @isTest private static void setPageLBGCOM(){
        //set the current page to planning report fll
        PageReference pageRef = Page.Planning_Report_FLL;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('topmost', '7430');	
        ApexPages.currentPage().getParameters().put('RegionAndSegment', 'LBG COM');	
    }
    
    @isTest private static void setPageE2(){
        //set the current page to planning report fll
        PageReference pageRef = Page.Planning_Report_FLL;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('topmost', '7430');	
        ApexPages.currentPage().getParameters().put('RegionAndSegment', 'E2');	
    }
    
    @isTest private static void setPageE2Mode(){
        //set the current page to planning report fll
        PageReference pageRef = Page.Planning_Report_FLL;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('topmost', '7430');	
        ApexPages.currentPage().getParameters().put('RegionAndSegment', 'E2Mode');	
    }
}