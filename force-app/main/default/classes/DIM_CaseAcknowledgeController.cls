// Used by DIM - Market Intelligence.
global class DIM_CaseAcknowledgeController { 
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------   
    @InvocableMethod(label='Send Email')
    global static void sendEmail(List<Id> caseIds) {
    
        Case caso = [SELECT Id, CaseNumber, Subject, Study_Type__c, Description, Owner.Name, Requested_By__c, Due_Date__c FROM Case WHERE Id IN :caseIds LIMIT 1];  
            
        OrgWideEmailAddress emailFrom = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'Sales Support Commercial'];
        String emailTo = [SELECT Email FROM User WHERE Id =: caso.Requested_By__c].Email;
        
        Datetime dueDateDateTime = Datetime.newInstance(caso.Due_Date__c.year(), caso.Due_Date__c.month(), caso.Due_Date__c.day());
        String dueDate = dueDateDateTime.format('MMM dd, YYYY');
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setOrgWideEmailAddressId(emailFrom.Id);
        email.setToAddresses(new String[] {emailTo});
        email.setSubject('[ACK] ' + caso.Subject);
        
        String description = '';
        if(caso.Description != null) {
            description = caso.Description;
        }
        
        String message = '';
        message = message + 'Dear,<br/>';
        message = message + '<br/>';
        message = message + 'Your request was received and will be handled by <b>' + caso.Owner.Name + '</b>.<br/>';
        message = message + 'The expected due date is <b>' + dueDate + '</b>.<br/>';
        message = message + '<br/>';
        message = message + 'You can follow the case and check its details at:<br/>';
        message = message + '<a href=\"' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + caso.Id + '\">Link to Salesforce case ' + caso.CaseNumber + '</a><br/>';
        message = message + '<br/>';
        message = message + 'Best Regards,<br/>';
        message = message + '<b>Sales Support Commercial</b><br/>';
        message = message + '<br/>';
        message = message + '--------------------------------------------------------------------------------<br/>';
        message = message + '<br/>';
        message = message + '<b>Subject: </b>' + caso.Subject + '<br/>';
        message = message + '<br/>';
        message = message + '<b>Study Type(s): </b>' + caso.Study_Type__c + '<br/>';
        message = message + '<br/>';
        message = message + '<b>Request Details:</b><br/>'; 
        message = message + description + '<br/>';
        message = message + '<br/>';
        message = message + '<b>Salesforce ID: </b>ref:_' + getShortId(UserInfo.getOrganizationId()) + '._' + getShortId(caso.Id) + ':ref';
        message = message + '<br/>';
        
        email.setHtmlBody(message);
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        emails.add(email);
        Messaging.sendEmail(emails, false);
        
        EmailMessage acknowledgeEmail = new EmailMessage();
        acknowledgeEmail.ToAddress = emailTo;
        acknowledgeEmail.FromAddress = emailFrom.Address;
        acknowledgeEmail.Subject = email.Subject;
        acknowledgeEmail.HtmlBody = email.HtmlBody;
        acknowledgeEmail.ParentId = caso.Id;
        insert acknowledgeEmail;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------   
    private static String getShortId(String id) {
       
        id = id.left(15);
        Integer i = 0;
        String shortId = id.right(10);
        
        do {
            if(shortId.mid(i, 1) != '0') {
                shortId = shortId.mid(i, shortId.length());
                break;
            }
            i = i+1;
        } while(i < shortId.length());
        
        return id.left(5) + shortId;
    }
}