// Used by DIM - Market Intelligence.
@isTest
public class DIM_CaseSplitControllerTest {

    //--------------------------------------------------------------------------
    static testMethod void validateCreate() {
         
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        EmailMessage email = new EmailMessage();
        email.ParentId = caso.Id;
        email.Subject = 'Email Request';
        insert email;
        
        Attachment emailAttachment = new Attachment();
        emailAttachment.Name = 'Presentation.ppt';
        emailAttachment.Body = Blob.valueOf('Slides');
        emailAttachment.ParentId = email.Id;
        insert emailAttachment;
        
        PageReference pageRef = Page.DIM_CaseSplitPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('emailId', email.Id);
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(caso); 
        DIM_CaseSplitController controller = new DIM_CaseSplitController(standardController);

        controller.email = email;
        controller.createCase();
        
        System.assertEquals(2, [SELECT count() FROM Case]);
    }
    
    //--------------------------------------------------------------------------
    static testMethod void validateTimezone() {
         
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'test', Email='test@companyTimezone.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='GMT', 
            UserName='test@companyTimezone.com');
        insert user;      
        
        PageReference pageRef = Page.DIM_CaseSplitPage;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(caso); 
        DIM_CaseSplitController controller = new DIM_CaseSplitController(standardController);
        
        System.runAs(user) {
            System.assertEquals(0, controller.offset);
        }
    }
}