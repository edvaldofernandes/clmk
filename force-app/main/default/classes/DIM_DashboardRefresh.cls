// Used by DIM - Market Intelligence.
global class DIM_DashboardRefresh { 
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------   
    @InvocableMethod(label='Refresh Dashboard')
    global static void callRefresh(List<Id> ids) {
        if(!DIM_TestUtils.isRunningTest()) {
            refresh(ids.get(0));
        }
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------   
    @future(callout=true)
    public static void refresh(Id dashboardId) {
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setMethod('PUT');
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v45.0/analytics/dashboards/' + dashboardId);

        Http http = new Http();
        HttpResponse res = http.send(req);
    }
}