/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for preventing a pricebook2 be created with the same name of
* another existing pricebook2.
*
* NAME: Pricebook2PreventsName.cls
* AUTHOR: KHPS                                               DATE: 18/12/2014
*******************************************************************************/

public with sharing class Pricebook2PreventsName {
	
	public static void execute() {
		
		TriggerUtils.assertTrigger();
		
		set<String> lstNamePricebookNew = new set<String>();
		set<String> lstNamePricebook = new set<String>();
		
		for(Pricebook2 pbk : (list<Pricebook2>)trigger.new) {
			lstNamePricebookNew.add(pbk.Name);
		}
		
		for(Pricebook2 pbk : [SELECT Id, Name 
		 FROM Pricebook2 WHERE Name = :lstNamePricebookNew]) {
		 	lstNamePricebook.add(pbk.Name);
		 }
		
		if(lstNamePricebook.isEmpty()) return;
		
		for(Pricebook2 pbk : (list<Pricebook2>)trigger.new) {
			if(lstNamePricebook.contains(pbk.Name)) {
				pbk.addError('There is already a pricebook with this name.');
			}
		}	
		
	}

}