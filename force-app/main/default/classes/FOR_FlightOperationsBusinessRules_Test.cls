/**
* @author Marcilio Leite de Souza
* @date 03/07/2018
* @description: TEST CLASS FOR FOR_FlightOperationsBusinessRules
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           03 JUL 2018             Original Version
**/
@isTest
private class FOR_FlightOperationsBusinessRules_Test {
	
   
    @isTest
    static void it_should_get_record_type_by_activity() {
       
        FOR_FlightOperationsBusinessRules obj = new FOR_FlightOperationsBusinessRules();
        
        Test.startTest();
       	System.assertNotEquals(null, obj.getRecordTypeIdToFOR());
        Test.stopTest();
        
    }
    
    @isTest
    static void it_should_get_queue() {
        
        String strNull = null;
        
        FOR_FlightOperationsBusinessRules obj = new FOR_FlightOperationsBusinessRules();
        
        Test.startTest();
        System.assertNotEquals(strNull,obj.getQueueIdToFOR());
        Test.stopTest();
        
    }
    
    @isTest
    static void it_should_get_account() {
        Account acc = create_account_test_data();
        
        FOR_FlightOperationsBusinessRules obj = new FOR_FlightOperationsBusinessRules();
        
        Test.startTest();
        System.assertEquals(acc.Id,obj.getAccountIdToFOR('Test'));
        Test.stopTest();
        
    }
    
    static Account create_account_test_data(){
        Account a = new Account();
        a.BillingCountry = 'Brazil';
        a.Name = 'Test';
        a.Company_Nickname__c = 'testNickname';
        a.FlyEmbraerId__c = '99999';
        insert a;
        return a;
    }
}