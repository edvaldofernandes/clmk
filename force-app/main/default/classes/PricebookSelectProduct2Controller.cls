/*******************************************************************************
*                     Copyright (C) 2014 - Cloud2b
*-------------------------------------------------------------------------------
*
* Controller class for the visualforce page PricebookSelectProduct2
* 
* NAME: PricebookSelectProduct2Controller.cls
* AUTHOR: DPF                          DATE: 17/12/2014
*
* Alterações solicitadas pelo Eduardo
* AUTHOR: JFS                          DATE: 18/12/2014
*******************************************************************************/
public with sharing class PricebookSelectProduct2Controller
{
  private static final Id RT_PROD_NON_EMBRAER = RecordTypeMemory.getRecType('Product2', 'Non_Embraer');
  private static final Integer QTDE_REGISTROS = 100;
  private static final Integer QTDE_TRAZIDO   = QTDE_REGISTROS + 1;
  private static final String RECORDTYPE_TRAINING = 'Training';

  public class produtoWrapper
  {
    public Boolean isSelected { get; set; }
    public Product2 prd { get; set; }

    public produtoWrapper ( Product2 aPrd )
    {
      prd = aPrd;
      isSelected = false;
    }
  }

  private String Idpricebook {get;set;}
  public Pricebook2 fPricebook {get; set;}
  public integer qtdeRegistros { get{ return QTDE_REGISTROS; } }
  public produtoWrapper[] ProdutosDisponiveis { get; set; }
  public String busca { get; set; }
  public boolean temMais { get; set; }

  public PricebookSelectProduct2Controller( ApexPages.StandardController controller )
  {
    Idpricebook = apexpages.currentPage().getParameters().get('addTo');

    this.fPricebook = null;
    list<Pricebook2> lstPricebook = [select Id, Name, Type__c from Pricebook2 where Id = :Idpricebook limit 1];
    if (lstPricebook != null && !lstPricebook.isEmpty()) this.fPricebook = lstPricebook[0]; 

    carregarProdutos();
  }

  public void carregarProdutos()
  {
    set<Id> setProdStdPB = new set<Id>();
    
    if ( !Test.isRunningTest( ) )
    {
    	for (PricebookEntry prdStd: [select Product2Id from PricebookEntry where Pricebook2.IsStandard = true])
        setProdStdPB.add(prdStd.Product2Id);
    }
    else
    {
    	for (PricebookEntry prdStd: [select Product2Id from PricebookEntry ])
        setProdStdPB.add(prdStd.Product2Id);
    }

    // Produtos disponíveis para escolha
    String lQuery = 'select Id, Name, Family, ProductCode, RecordType.Name, Description ' +
      ' from Product2 where IsActive=true ';
    list<String> lstCriteria = new list<String>();

    if (this.fPricebook != null && String.isNotBlank(this.fPricebook.Type__c))
    {
    	String sCritPB = (this.fPricebook.Type__c == 'Mix Products' || this.fPricebook.Type__c == 'Tailored')
    	  ? 'RecordTypeId != \'' + String.valueOf(RT_PROD_NON_EMBRAER) + '\''
    	  : 'RecordType.Name = \'' + this.fPricebook.Type__c + '\'';
      lstCriteria.add(sCritPB);
    }
    if ( busca != null ) lstCriteria.add('( Product2.Name like \'%' + busca + '%\' or Product2.Description like \'%' + busca + '%\')');
    
    if (!lstCriteria.isEmpty()) lQuery += ' AND ' + String.join(lstCriteria, ' AND ');

    lQuery += ' order by Name limit ' + QTDE_TRAZIDO;

    list< Product2 > lProdutos = database.query( lQuery );

    temMais = lProdutos.size() == QTDE_TRAZIDO;
    if ( temMais ) lProdutos.remove( QTDE_TRAZIDO -1 );

    ProdutosDisponiveis = new list< produtoWrapper >();
    for ( Product2 lPBE : lProdutos )
    {
      if (setProdStdPB.contains(lPBE.Id))
        ProdutosDisponiveis.add( new produtoWrapper( lPBE ) );
    }
  }

   public PageReference Selected()
   {
    String lstpb = '';

    for(produtoWrapper pb: ProdutosDisponiveis)
    {
      if(pb.isSelected)
      {
        lstpb += pb.prd.id + ',';
      }
    }

    PageReference page = new PageReference('/apex/pricebookeditproduct?id=' + Idpricebook +'&lstaPb=' + lstpb);

    page.setRedirect(true);
    return page;
  }
}