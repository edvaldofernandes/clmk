public class PublicCalendarsHomePageButtonController
{

    public string selectedCalendarId{get;set;}
    public string buttonCaption{get;set;}

   
    public PublicCalendarsHomePageButtonController() 
    {


    }

    
    public List<SelectOption> getSelectedCalendarNames()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Map<String, PublicCalendarSettings__c> calendars = PublicCalendarSettings__c.getAll();
        
        options.add(new SelectOption('', 'No Calendar Selected'));
        
        for (PublicCalendarSettings__c calendarTemp : calendars.values()) 
            options.add(new SelectOption(calendarTemp.calendarId__c, calendarTemp.Name));

        
        return options;
    }
    
    public pageReference openPublicCalendarPageView()
    {

    
        pageReference pageRef = new PageReference('/apex/PublicCalendarView?calendarId=' + selectedCalendarId);        
        pageRef.setRedirect(false);
        return pageRef;
    
    }


}