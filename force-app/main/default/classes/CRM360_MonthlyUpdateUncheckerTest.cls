@isTest
private class CRM360_MonthlyUpdateUncheckerTest {
    
    private static String CRON_EXP = '0 0 0 15 3 ? 2022';
    private static Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();

    @testSetup
    static void setup(){     
        
        //Create an Account
        Account testAccount 			= new Account();
        testAccount.Name				='Test Account record';
        testAccount.Company_Nickname__c ='TesterComp';
        insert testAccount;
        
        //Create an Executive Summary end associate it to the account
        Account_Cockpit__c ac = new Account_Cockpit__c();
        ac.Name = 'Test Meeting';
        ac.Customer_Expectation__c = 'Expects to test stuff';
        ac.Att_Level__c = 'High';
        ac.RecordTypeId = RecordTypeIdCRM360Data;
        ac.Related_EOC__c = 'EOC WW EJET 2019 WARSAW';
        ac.Account_Name__c = testAccount.Id;
        ac.Is_Updated__c = true;
        ac.Is_Record__c = false;
        insert ac;
    }
    
    static testmethod void testScheduledJob(){
        
        Test.startTest();
		String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new CRM360_MonthlyUpdateUnchecker());        
        Test.stopTest();
        
        List<Account_Cockpit__c> accountCockpitsTest = [SELECT Id, Is_Updated__c
                                                 	FROM Account_Cockpit__c 
                                                 	WHERE Record_Type_ID_ref__c = 'CMR360_Dashboard_Data'
                                                 	AND Is_Record__c = false];
        
        //Verify if status has been updated for all accounts
        for(Account_Cockpit__c accountCockpitTest : accountCockpitsTest){
            System.assertEquals(false, accountCockpitTest.Is_Updated__c);
        }
    }

}