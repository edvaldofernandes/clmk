/* Classe implementadora de SOBjectDAO para operações DML no objeto Contract_PDP__c, utilizando pattern de Singleton.
* @author - Elvia Serpa
*           elvsantos@deloitte.com
* @version 1.0 21/01/2016
*/
public with sharing class ContractPDPDAO 
{
    
    private static final ContractPDPDAO instance = new ContractPDPDAO();    
    
    private ContractPDPDAO(){
    }    
    
    public static ContractPDPDAO getInstance(){
        return instance;
    }
    
    public List<Contract_PDP__c> getContractPDP(String agreemenId)
    {
        List<Contract_PDP__c> lstContractPDP = [Select 
                                                    Id ,
                                                    Payment_Percentage__c ,
                                                    Payment_Number__c ,
                                                    Prior_payment_time__c
                                                From 
                                                    Contract_PDP__c 
                                                Where 
                                                    Commercial_Agreement__c = :agreemenId ];
        return lstContractPDP;        
    }     
}