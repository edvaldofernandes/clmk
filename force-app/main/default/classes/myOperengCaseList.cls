global class myOperengCaseList {
    @AuraEnabled global static List<Case> getCases(String filter) {
        User[] u = [SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        Contact[] contact = [SELECT AccountId FROM Contact WHERE Id =: u[0].ContactId LIMIT 1];
        
        if(contact.size()>0){           
            Account account = [SELECT Name,Id,Geographical_Area__c, FlightOps_Account_Domains__c FROM Account WHERE Account.Id =: contact[0].AccountId];
            if(filter == 'Open'){
                return [SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description, ClosedDate, FlightOps_Last_Email_Received__c, Incoming_email_i__c FROM Case WHERE (Case.AccountId =: contact[0].AccountId AND Customer_Visible__c = true AND Case.Status != 'Archived' AND Case.Status != 'Closed' AND RecordType.Name = 'FlightOps Case Record Type') ORDER BY Status ASC, FlightOps_Last_Email_Received__c DESC, ClosedDate DESC, CreatedDate DESC];
            }
            if(filter == 'Answered'){
                return [SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description, ClosedDate, FlightOps_Last_Email_Received__c, Incoming_email_i__c FROM Case WHERE (Case.AccountId =: contact[0].AccountId AND Customer_Visible__c = true AND Case.Status = 'Closed' AND RecordType.Name = 'FlightOps Case Record Type') ORDER BY Status ASC, FlightOps_Last_Email_Received__c DESC, ClosedDate DESC, CreatedDate DESC];
            }
            if(filter == 'All'){
                return [SELECT Id,Subject,Owner.Name,CaseNumber,CreatedDate,Status,Description, ClosedDate, FlightOps_Last_Email_Received__c, Incoming_email_i__c FROM Case WHERE (Case.AccountId =: contact[0].AccountId AND Customer_Visible__c = true AND Case.Status != 'Archived' AND RecordType.Name = 'FlightOps Case Record Type') ORDER BY Status ASC, FlightOps_Last_Email_Received__c DESC, ClosedDate DESC, CreatedDate DESC];
            }
        }
        List<Case> noCases = new List<Case>();
        return noCases;
    }
    
    @AuraEnabled global static List<Case> deserializeJson(String jsonString) {
        String s = jsonString;
        System.debug(s);
		List<Case> c = (List<Case>) JSON.deserialize(s, List<Case>.class);
        return c;
    }
}