/* Classe implementadora de SOBjectDAO para operações DML no objeto CaseDAO, utilizando pattern de Singleton.
* @author - Rodrigo Pereira
*			rodrpereira@deloitte.com
* @version 1.0 22/01/2016
*/
public without sharing class CaseDAO {
    
    private static final CaseDAO instance = new CaseDAO();    
    
    private CaseDAO(){
    }    
    
    public static CaseDAO getInstance() {
        return instance;
    }
    
    public String getCaseNumber(String caseID) {        
       return [select caseNumber from Case where id = :caseID].caseNumber;
	}
    
    public List<Case> getCasesBySetId(Set<Id> caseIds) {        
       return [
               SELECT Id, CaseNumber, Subject, RecordTypeId 
               FROM Case 
               WHERE Id IN :caseIds
       ];
	}
}