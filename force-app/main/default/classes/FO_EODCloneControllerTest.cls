/**
* @author Marcos Vinicius de Oliveira Duque
* @date 28/11/2018
* @description: FO_EODCloneController test class.
**/
@isTest
public class FO_EODCloneControllerTest {
    
    
    /**
    * @description Test if redirect() method returns the EOD's clone record detail page.
    **/
    @isTest static void testIsGettingReviewEODPage(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        
		ApexPages.currentPage().getParameters().put('id',eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
        
	    FO_EODCloneController controller  = new FO_EODCloneController(stdController);
        
        PageReference reviewPage = controller.redirect();
        EOD__c review = [SELECT Id FROM EOD__c WHERE Parent_EOD__c = :eod.Id];
        system.assertEquals(reviewPage.getUrl(), '/' + review.Id);
    }
}