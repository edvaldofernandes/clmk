@isTest(SeeAllData=true)
public with sharing class OOS_ClassificationControllerTest {
  private testMethod static void getCurrentTeamRoleMemberTest() {
    AccountTeamMember acc = getEligibleAccTeamMember();

    User u = [SELECT Id FROM User WHERE Id = :acc.UserId LIMIT 1];
    System.runAs(u) {
      List<AccountTeamMember> members = OOS_ClassificationController.getCurrentTeamRoleMember();
      System.assertEquals(
        members.size() >= 1,
        true,
        '[ERROR] It is not possible get the team roles of users.'
      );
    }
  }

  private testMethod static void getOOSRecordsTest() {
    AccountTeamMember acc = getEligibleAccTeamMember();

    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 10, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 8, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 6, 10))
    };
    Database.insert(newOOS);

    User u = [SELECT Id FROM User WHERE Id = :acc.UserId LIMIT 1];
    System.runAs(u) {
      List<Out_of_Service__c> oosDB = [
        SELECT Id
        FROM Out_of_Service__c
        WHERE
          Serial_Number__r.Operator__c = :acc.AccountId
          AND TechRep_Classified__c = FALSE
          AND Disregarded_Classification__c = FALSE
      ];

      List<Out_of_Service__c> oos = OOS_ClassificationController.getOOSRecords(
        oosDB[0].Id
      );

      system.assertEquals(
        oos.size() >= 1,
        true,
        '[ERROR] Problem to recovery any OOS record'
      );

      List<Id> ids = new List<Id>();
      for (Out_of_Service__c oosExample : oos) {
        ids.add(oosExample.Id);
      }

      system.assertEquals(
        ids.contains(oosDB[0].Id),
        true,
        '[ERROR] Problem to get OOS Records in Tech Rep accounts'
      );
    }
  }

  private testMethod static void getOOSRecordsToEMITRTSTest() {
    AccountTeamMember acc = getEligibleAccTeamMember();

    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 10, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 8, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 6, 10))
    };
    Database.insert(newOOS);

    List<User> users = [
      SELECT Id
      FROM User
      WHERE
        Profile.Name = 'Customer Data Reader Chatter Only'
        AND IsActive = TRUE
    ];
    if (users.size() > 0) {
      User u = users[0];
      System.runAs(u) {
        List<Out_of_Service__c> oosDB = [
          SELECT Id
          FROM Out_of_Service__c
          WHERE
            EMIT_RTS_Classified__c = FALSE
            AND Disregarded_Classification__c = FALSE
        ];

        List<Out_of_Service__c> oos = OOS_ClassificationController.getOOSRecords(
          oosDB[0].Id
        );

        system.assertEquals(
          oosDB.size(),
          oos.size(),
          '[ERROR] Problem to recovery any OOS record'
        );
      }
    }
  }

  private testMethod static void getOOSRecordsToQualityTest() {
    AccountTeamMember acc = getEligibleAccTeamMember();

    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 10, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 8, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 6, 10))
    };
    Database.insert(newOOS);

    List<User> users = [
      SELECT Id
      FROM User
      WHERE Profile.Name LIKE 'Quality for Chatter Only' AND IsActive = TRUE
    ];
    if (users.size() > 0) {
      User u = users[0];
      System.runAs(u) {
        List<Out_of_Service__c> oosDB = [
          SELECT Id
          FROM Out_of_Service__c
          WHERE
            Quality_Classified__c = FALSE
            AND Disregarded_Classification__c = FALSE
        ];

        List<Out_of_Service__c> oos = OOS_ClassificationController.getOOSRecords(
          oosDB[0].Id
        );

        system.assertEquals(
          oosDB.size(),
          oos.size(),
          '[ERROR] Problem to recovery any OOS record'
        );
      }
    }
  }

  private testMethod static void getOOSRecordsToOtherAreaTest() {
    AccountTeamMember acc = getEligibleAccTeamMember();

    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 10, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 8, 10)),
      createOOSRecord(acc.AccountId, Date.newInstance(2000, 6, 10))
    };
    Database.insert(newOOS);

    List<User> users = [
      SELECT Id
      FROM User
      WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE
    ];
    if (users.size() > 0) {
      User u = users[0];
      System.runAs(u) {
        List<Out_of_Service__c> oosDB = [
          SELECT Id
          FROM Out_of_Service__c
          WHERE Disregarded_Classification__c = FALSE
        ];

        List<Out_of_Service__c> oos = OOS_ClassificationController.getOOSRecords(
          oosDB[0].Id
        );

        system.assertEquals(
          oosDB.size(),
          oos.size(),
          '[ERROR] Problem to recovery any OOS record'
        );
      }
    }
  }

  private testMethod static void emailActionItemTest() {
    AccountTeamMember acc = getEligibleAccTeamMember();

    User u = [SELECT Id FROM User WHERE Id = :acc.UserId LIMIT 1];
    Id oosId = Database.insert(createOOSRecord()).getId();

    System.runAs(u) {
      Action_Item__c act = new Action_Item__c();
      act.Title__c = 'this is a test';
      act.Description__c = 'description';
      act.Responsible__c = u.Id;
      act.Deadline__c = Date.today();
      act.Status__c = 'PENDING FEEDBACK';
      act.Out_of_Service__c = oosId;
      insert act;

      System.assertEquals(
        [SELECT Id FROM Action_Item__c LIMIT 1].size(),
        1,
        '[ERROR] There is a problem saving a new Action Item!'
      );
    }

  }

  public static Out_of_Service__c createOOSRecord() {
    Out_of_Service__c oos = new Out_of_Service__c();
    oos.Serial_Number__c = [SELECT Id FROM Aircraft__c LIMIT 1].Id;
    oos.reference_date__c = Date.today();
    oos.event_description__c = 'This is a test';
    return oos;
  }

  public static Out_of_Service__c createOOSRecord(
    Id accountId,
    Date startDate
  ) {
    Out_of_Service__c oos = new Out_of_Service__c();
    oos.Serial_Number__c = [
      SELECT Id
      FROM Aircraft__c
      WHERE Operator__c = :accountId
      LIMIT 1
    ]
    .Id;
    oos.reference_date__c = Date.today();
    oos.event_description__c = 'This is a test';
    oos.Start_Date__c = startDate;
    oos.OOS_Total_Time__c = 8.00;
    return oos;
  }

  public static AccountTeamMember getEligibleAccTeamMember() {
    List<Id> accIds = new List<Id>();
    for (AccountTeamMember account : [
      SELECT AccountId
      FROM AccountTeamMember
      WHERE TeamMemberRole = 'Tech Rep'
    ]) {
      accIds.add(account.AccountId);
    }

    List<Id> operatorIds = new List<Id>();
    for (Aircraft__c ac : [
      SELECT Operator__c
      FROM Aircraft__c
      WHERE Operator__c IN :accIds
    ]) {
      operatorIds.add(ac.Operator__c);
    }

    AccountTeamMember acc = [
      SELECT UserId, AccountId
      FROM AccountTeamMember
      WHERE
        TeamMemberRole = 'Tech Rep'
        AND accountId IN :operatorIds
        AND UserId IN (
          SELECT Id
          FROM User
          WHERE
            IsActive = TRUE
            AND Profile.Name LIKE 'Service & Support Representative'
        )
      LIMIT 1
    ];

    return acc;
  }
}