/* 
----------------------------------------------------------------------------------------------
-- - Company:     Stefanini
-- - Name:        CustomRelatedListMFIR_Controller
-- - Description: Controller for Visualforce CustomRelatedListMFIR
-- - @Author: Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               Version                   
----------------------------------------------------------------------------------------------
-- 03/12/2019       Felipe Gouvea      1.0         
----------------------------------------------------------------------------------------------
*/

public with sharing class CustomRelatedListMFIR_Controller {
	private Case contextCase{ get; set; }
	public CustomRelatedListMFIR_Controller(ApexPages.StandardController controller) {
		contextCase = [SELECT AccountId FROM Case Where Id = : controller.getRecord().Id];
	}

	public List<MFIR_Wrapper> getRelatedMFIR(){
		List<MFIR__c> relatedMFIRs = BO_MFIR.getMFIRsRelatedToCase(contextCase);
		List<MFIR_Wrapper> MFIRWrapper = new List<MFIR_Wrapper>();
		for(MFIR__c mfirObject : relatedMFIRs){
			MFIRWrapper.add(new MFIR_Wrapper(mfirObject));
		}

		return MFIRWrapper;
	}

	public class MFIR_Wrapper{
		public MFIR__c mfir {get; set;}

		public MFIR_Wrapper(MFIR__c mfir){
			this.mfir = mfir;
		}
	}
}