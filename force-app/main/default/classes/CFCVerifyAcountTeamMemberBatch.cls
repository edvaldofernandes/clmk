global class CFCVerifyAcountTeamMemberBatch implements Database.Batchable<sObject>
{
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        //String query = 'SELECT Id,Name,CAM__c FROM Account WHERE Company_Status__c  = \'Active\'';
        String query = 'SELECT Id,Name,CAM__c FROM Account';
        return Database.getQueryLocator(query);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<String> listError = new List<String>();
        List<Account> accsToClear = new List<Account>();
        
        //cleaning the field CAM__c
        for(Account acc : scope){
            acc.CAM__c = null;
            accsToClear.add(acc);
        }
        system.debug('CFCVerifiyAcountTeamMemberSchedule.accsToClear >>> ' + accsToClear);             
        system.debug('CFCVerifiyAcountTeamMemberSchedule.accsToClear.size() >>> ' + accsToClear.size());
        Database.SaveResult[] listUpdateResultClean = Database.update(accsToClear,false);
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listUpdateResultClean >>> ' + listUpdateResultClean);
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listUpdateResultClean.size() >>> ' + listUpdateResultClean.size());
        
        //Add list error of list account to clear                                
        Integer recordId1 = 0;
        for (Database.SaveResult res : listUpdateResultClean) {
            if (!res.isSuccess()) {
                system.debug('accsToClear[recordId1].id >>> ' + accsToClear[recordId1].id);
                string errorMsg = '<br/>The Account Id:' + accsToClear[recordId1].id + ', ' + res.getErrors()[0].getMessage() + '. <br/>';
                listError.add(errorMsg);
            }
            recordId1++;
        }
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listError >>> ' + listError);
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listError.size() >>> ' + listError.size());
        
        //get list of account team member equals CAM
        List<AccountTeamMember> listteamMember = AccountTeamMemberDao.getInstance().listByTeamMemberRole('CAM - Customer Account Manager');        
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listteamMember >>> ' + listteamMember);
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listteamMember.size() >>> ' + listteamMember.size());
        
        //map account team member
        Map <string, string> mapAccountTeamMember = new Map<String, String>();
        for(AccountTeamMember teamMember : listteamMember){
            if(teamMember.AccountId != null && teamMember.UserId != null){
                mapAccountTeamMember.put(teamMember.AccountId, teamMember.UserId);
            }
        }
        system.debug('CFCVerifiyAcountTeamMemberSchedule.mapAccountTeamMember >>> ' + mapAccountTeamMember);
        system.debug('CFCVerifiyAcountTeamMemberSchedule.mapAccountTeamMember.size() >>> ' + mapAccountTeamMember.size());
        
        //populate accounts with the team member roles
        List<Account> listAccountToUpdate = new List<Account>();
        for(Account acc : accsToClear){
            //if the account exist in map so we need update the field CAM__c with the value of map
            if(mapAccountTeamMember.containsKey(acc.id)){
                acc.CAM__c = mapAccountTeamMember.get(acc.Id);
                listAccountToUpdate.add(acc);
            }
        }
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listAccountToUpdate >>> ' + listAccountToUpdate);
        system.debug('CFCVerifiyAcountTeamMemberSchedule.listAccountToUpdate.size() >>> ' + listAccountToUpdate.size());
        Database.SaveResult[] listUpdateResult = Database.update(listAccountToUpdate,false);   
        
        //send e-mail
        List<String> toAddresses = new List<String>();
        CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults();
        for(String item : cs.Email_to_send_error_job__c.split(';')){
            toAddresses.add(item);
        }
        system.debug('CFCVerifiyAcountTeamMemberSchedule.toAddresses >>> ' + toAddresses);
        if(listError.size() > 0 || test.isRunningTest()){ //the condition isRunningTest is used because is not possible replicate error in our test scenario
            sendmail(toAddresses,listError);
        }
    }   
    
    global void finish(Database.BatchableContext BC){
    }
    
    public void sendmail(List<String> emailsAddresses, List<String> accountErrors)
    {
        system.debug('CFCVerifiyAcountTeamMemberSchedule.sendmail()');
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject('Error Job CFCVerifyAcountTeamMemberBatch');
        string bodyMessage = 'Dear<br/><br/>The required fields from:<br/>';
        for(String str : accountErrors){
            bodyMessage += '\n'+str;
        }
        bodyMessage += '<br/><br/>Please fill the field for the job “CFCVerifyAccountTeamMemberBatch” execution occur correctly.<br/><br/>Thank you.';
        system.debug('CFCVerifiyAcountTeamMemberSchedule.sendmail.bodyMessage >>> ' + bodyMessage);
        email.setHtmlBody(bodyMessage);
        email.setToAddresses(emailsAddresses);
        Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});        
    } 
    
}