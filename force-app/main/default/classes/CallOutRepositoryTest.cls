@isTest
public class CallOutRepositoryTest {
    
    @isTest static void saveAccountInformation(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test account rcp';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        INSERT account;
        
        AccountInformation accountInformation = new AccountInformation(account, 'NEW OBJECT', 'NEW');
        AccountHistoryInformation accountHistoryInformation = new AccountHistoryInformation(Datetime.now(), 'field', 'OldValue', 'newValue', 'createdBy');
        
        List<AccountInformation> accountInformationList = new List<AccountInformation>();
        accountInformationList.add(accountInformation);
        
        List<AccountHistoryInformation> accountHistoryInformationList = new List<AccountHistoryInformation>();
        accountHistoryInformationList.add(accountHistoryInformation);
        
        Visitator visitor = new CallOutRepository();
        visitor.visit(accountInformationList, accountHistoryInformationList, 'AccountInformation');
        
        List<Call_Out__c> calloutList  = CallOutRepository.findPayloadByClassName('AccountInformation');
        
        List<Call_Out__c> calloutListDeleteUpdate = new List<Call_Out__c> ();
        
        for(Call_Out__c callout : calloutList){
            callout.status__c = 'RCP_SENT';
            calloutListDeleteUpdate.add(callout);
        }
        
        CallOutRepository.updateCalloutSent(calloutListDeleteUpdate);
        
        List<Call_Out__c> calloutAccountList = CallOutRepository.findPayloadByClassNameAndStatus('AccountInformation');
        List<Call_Out__c> calloutAccountListDeleteUpdate = new List<Call_Out__c> ();
        
        for(Call_Out__c callout : calloutAccountList)
            calloutAccountListDeleteUpdate.add(callout);
        
        CallOutRepository.deleCalloutSent(calloutAccountListDeleteUpdate);
        
    }
    
    
    
    @isTest static void saveAircraftInformation(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test aircraftInformation';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = 'TEST 123456';
        insert account;
        
        Aircraft__c aircraft = new Aircraft__c ();
        aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '19000111';
        aircraft.Owner__c = account.id;
        aircraft.Commercial_Name__c = 'EMB-110P1';
        aircraft.Aircraft_Status__c = 'Other';
        insert aircraft;  
        
        AircraftInformation aircraftINformation = new AircraftInformation(aircraft,account,
                                                                          Constants.NEW_OBJECT_CREATED, 
                                                                          Constants.STATUS_ACTIVE);
        
        AircraftHistoryInformation aircraftHistoryInformation = new AircraftHistoryInformation(DateTime.now(), 
                                                                                               'test field', 
                                                                                               'test OldValue', 
                                                                                               'test newValue',
                                                                                               'test createdBy');
        
        List<AircraftInformation> aircraftINformationList = new List<AircraftInformation>();
        aircraftINformationList.add(aircraftINformation);
        
        List<AircraftHistoryInformation> aircraftHistoryInformationList = new List<AircraftHistoryInformation>();
        aircraftHistoryInformationList.add(aircraftHistoryInformation);
        
        Visitator visitor = new CallOutRepository();
        visitor.visit(aircraftINformationList, aircraftHistoryInformationList, 'AircraftInformation');
        
    }
}