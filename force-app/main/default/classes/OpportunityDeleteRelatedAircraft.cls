/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for deleting related aircraft when an opportunity is deleted.
*
* NAME: OpportunityDeleteRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*******************************************************************************/
public with sharing class OpportunityDeleteRelatedAircraft {
	
	public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    List<Id> lstOppId = new List<Id>();
    for ( Opportunity opp: (List<Opportunity>) trigger.old)
    {
      lstOppId.add(opp.Id);
    }
    if ( lstOppId.isEmpty() ) return;
    
    List<String> lstProdutoOppId = new List<String>();
    for ( OpportunityLineItem oli : [SELECT Id FROM OpportunityLineItem WHERE OpportunityId =: lstOppId])
    {
    	lstProdutoOppId.add(oli.Id);
    }
    if ( lstProdutoOppId.isEmpty() ) return;
    
    List<Related_Aircraft__c> lstRelAircrafts = [ SELECT Id FROM Related_Aircraft__c 
      WHERE Opportunity_product_id__c =: lstProdutoOppId ];
      
    if ( !lstRelAircrafts.isEmpty() ) delete lstRelAircrafts;
  }

}