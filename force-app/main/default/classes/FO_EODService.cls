/**
* @description: This class represents the service layer for EODs(Embraer Operational Disposition).
**/
public class FO_EODService {
    public final static String VISUALFORCE_DOCUMENT_PATH = '/apex/FO_EODDocument';

    /**
    * @description Changes the EOD status to 'Issued' if it is publishable and
    * attachs an generated pdf to the EOD Sobject.
    **/
    public static void publish(Id eodId){
        EOD__c eod = FO_EodRepository.getById(eodId);
        FO_Document eodDocument = buildPdf(eodId);
        
        Boolean eodIsPublishable = FO_EODService.canIssue(eodId);
        if(eodIsPublishable){
            eodDocument.attachToRecord(eodId);
            
            eod.Status__c = 'Issued';
            eod.Issue_Date__c = System.today();
            update eod;
        }else{
            eodDocument.deleteAttachmentsTemporaryFiles();
            if( eod.Status__c == 'Issued'){
            	throw new FlightOpsException('This EOD is already issued.');
            }else{
            	throw new FlightOpsException('An EOD have to be approved before issued.');
            }
        }
    }
    
    /**
    * @description Builds an pdf document based on EOD's data.
    * 
    * @param Id eodId : The ID of an EOD.
	* @return Blob : A pdf file generated from the EOD.
    **/
    public static FO_Document buildPdf(Id eodId){
        String title = FO_EODService.getFilename(eodId);
        String filename = title + '.pdf';
        // Generating the pdf from a visualforce page
        
        PageReference htmlDocument = getPdfPageReference(eodId);
        htmlDocument.getParameters().put('id', eodId);
        
        List<Id> documentsIds = FO_EodRepository.getRelatedDocumentsIds(eodId);
        FO_Document document = new FO_Document(htmlDocument, documentsIds);
        document.setTitle(title);
        document.setFilename(filename);           
        document.build();
        return document;
    }
    
    /**
    * @description Generates an valid filename to an EOD.
    * 
    * @param Id eodId : The ID of the EOD.
	* @return String : A valid filename for an EOD.
    **/
    public static String getFilename(Id eodId){
        EOD__c eod = FO_EodRepository.getById(eodId);
        String EODNumber = eod.EOD_Number__c;
        String companyName = eod.Related_Case__r.account.name;
        Integer version = getCurrentVersion(eodId);
        String versionTag = '';
        if(version > 0){
			versionTag =  '_rev' + version;
        }
        String filename = EODNumber + '_' + companyName +'_' + getEscapedSubject(eodId) + versionTag;
        return filename;
    }
    
    /**
    * @description Gets the current version number of the inputted EOD.
    * If the EOD is the original one, the result will be 0.
    * 
    * @param Id eodId : The Id of the EOD.
	* @return Integer : The version of the inputted EOD.
    **/
    public static Integer getCurrentVersion(Id eodId){
        Integer version = 0;
        EOD__c currentEod = [
            SELECT
             Parent_EOD__c
            FROM
            	EOD__c
            WHERE
            	Id = :eodId
        ];
        while(currentEod.Parent_EOD__c != null){
            version++;
            currentEod = [
                SELECT
                	Parent_EOD__c
                FROM
                	EOD__c
                WHERE
                	Id = :currentEod.Parent_EOD__c
            ];
        }
        return version;
    }    
    
    /**
    * @description Remove all special characters on EOD's subject to be a valid
    * filename.
    * 
    * @param Id eodId : The ID of the EOD.
	* @return String : Escaped EOD subject.
    **/
    public static String getEscapedSubject(Id eodId){
        String regex = '[\\\\/:*?\"<>|]';
        String replacementCharacter = '_';
    	String subject = FO_EodRepository.getById(eodId).Subject__c;
        String escapedSubject = subject.replaceAll(regex, replacementCharacter);
        return escapedSubject;
    }
    
    /**
    * @description builds an PageReference object referring Eod document pdf.
    * 
	* @return PageReference : An object representing the document.
    **/
    public static PageReference getPdfPageReference(Id eodId){
        PageReference pageReference = new pageReference(VISUALFORCE_DOCUMENT_PATH);
        pageReference.getParameters().put('id', eodId);
        return pageReference;
    }   

	/**
    * @description Checks if an EOD can be issued.
    * 
    * @param Id eodId : The ID of the EOD.
    **/
    public static Boolean canIssue(Id eodId){
        String status = FO_EodRepository.getById(eodId).Status__c;
        return status == 'Approved';
    }

    /**
    * @description Copy all EOD fields to a new one and sets the original as parent
    * of it.
    * 
    * @param Id originalId : The ID of the original EOD.
	* @return EOD__c : the cloned EOD.
    **/
    public static EOD__c cloneForRevision(Id originalId){
        EOD__c original = FO_EodRepository.getById(originalId);
        EOD__c review = original.clone();
        
        
        review.Status__c = 'Draft';
        review.Parent_EOD__c = original.Id;
        original.Status__c = 'Deprecated';       
        
        SavePoint sp = Database.setSavepoint();
        try {
            insert review;
            update original;
        } catch (Exception e) {
            Database.rollback(sp);
            throw e;
        }
        return review;
    }
    
    /**
    * @description Builds an log mapping the revision version and its issue date.
    * 
    * @param Id eodId : The ID of the EOD.
	* @return Map<String, Date> : A Map representing the version log.
    **/
    public static Map<String, Date> getRevisionLog(Id eodId){
        Map<String, Date> reviews = new Map<String, Date>();
		EOD__c eod = FO_EodRepository.getById(eodId);
        Date issueDate =  eod.Issue_Date__c;
        if(issueDate == null){
            issueDate = System.today();
        }     
        if(eod.Parent_EOD__c == null){
            reviews.put('ORIGINAL', issueDate);
        } else{
            reviews = getRevisionLog(eod.Parent_EOD__c);
            Integer sumOfRevisions = reviews.size();
            reviews.put('REV ' + sumOfRevisions, issueDate);
        }
        return reviews;
    }
        
    /**
    * @description Return an URL for the pdf EOD document.
    * 
    * @param Id eodId : The ID of the EOD.
	* @return String : An URL String for the document.
    **/
    public static String getPdfUrl(Id eodId){
        return getPdfPageReference(eodId).getUrl();
    }

	/**
    * @description Change EOD status to "Cancelled".
    * 
    * @param Id eodId : The ID of an EOD.
    **/
    public static void cancel(Id eodId){
        EOD__c eod = FO_EodRepository.getById(eodId);
        if(eod.status__c != 'Issued'){
            eod.status__c = 'Cancelled';
            update eod;
        }else{
           throw new FlightOpsException('An EOD cannot be cancelled if it is issued');
        }
    }

	/**
    * @description Returns a list of aircraft on which the EOD is applicable.
    * 
	* @return List<Aircraft__c> : The related aircraft.
    **/
    public static List<Aircraft__c> getEodAssociatedAircraft(Id eodId){
        return FO_EodRepository.getAssociatedAircraft(eodId);
    }
    
	/**
    * @description Returns all files related to an Eod.
    * 
    * @param Id eodId : The ID of the EOD.
	* @return List<Id> : The files' ids related to the EOD.
    **/
    public static List<Id> getEodRelatedDocumentsIds(Id eodId){
        return FO_EodRepository.getRelatedDocumentsIds(eodId);
    }
    
}