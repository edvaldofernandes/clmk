/*
    * @description Controller for the CLMSignAgreement Visualforce Page. 
    * Confirms if the agreement can be signed and starts its process and calls CLMSyncAmendment and CLMSetNewActiveAgreement classes
 */


public class CLMSignAgreementController {
    public boolean isHide {get;set;}
    public boolean isSignatureReady {get;set;}
    public String newAmendmentId {get;set;}


	public Agreement__c agreement { get; set; }
    private Id parentId;
    private Id childId;
    
    public CLMSignAgreementController(ApexPages.StandardController controller){
        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            system.debug('NULL ID');
            return;
        }

        isHide = true;
        isSignatureReady = false;
        agreement = [SELECT Id, Edit_Mode__c, Error_Log__c, Parent_Agreement__r.Signature_SObject_Creation__c, Parent_Agreement__r.Id, Version_Number__c, Is_Amendment__c, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Date__c, Proposal_Validity__c, Nickname__c, Change_History__c 
                     FROM Agreement__c 
                     WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
        parentId = agreement.Parent_Agreement__r.Id;
        childId = agreement.Id;
        if(agreement == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Agreement not found','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
            system.debug('AGR NOT FOUND');
            return;
    	}
    }
    
    public PageReference sign(){
        if(agreement == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Agreement not found','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
            system.debug('AGR NOT FOUND');
            return null;
    	}
        if (agreement.Edit_Mode__c){
            agreement.Status__c = 'Activated';
            agreement.Status_Category__c = 'PA Signed'  ;
            update agreement; 
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Edit Mode is ON - Can\'t sign.'));
            system.debug('EDIT MODE');
            signCompleted();
            return null;
        }
        if(agreement.Parent_Agreement__r.Id != null){
            system.debug('agreement.Parent_Agreement__r.Id' + agreement.Parent_Agreement__r.Id);
            system.debug('agreement.Id' + agreement.Id);
            Integer parentNumber = GEN_Util.howManyContentDocumentLink(Id.valueOf(agreement.Parent_Agreement__r.Id));
            Integer childNumber = GEN_Util.howManyContentDocumentLink(Id.valueOf(agreement.Id));
            //if (GEN_Util.howManyContentDocumentLink(agreement.Parent_Agreement__r.Id) >= GEN_Util.howManyContentDocumentLink(agreement.id)){
            //if (GEN_Util.howManyContentDocumentLink(parentId) >= GEN_Util.howManyContentDocumentLink(childId)){
            if (parentNumber >= childNumber){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Agreement must have one more attached file.'));
                system.debug('NEED ONE MORE FILE AMEND');
                return null;
            }
        }
        else{
            Integer childNumber = GEN_Util.howManyContentDocumentLink(Id.valueOf(agreement.Id));
            system.debug('TEM ' + childNumber + ' ARQUIVOS');
            if (childNumber<1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Agreement must have one more attached file.'));
                system.debug('NEED ONE MORE FILE PROP/PA');
                return null;
            }
        }
        if (!isValidPA() && agreement.Is_Amendment__c==true){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue'));
            system.debug('Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue');
            return null;
        }
     
        if (agreement.recordtype.name == 'Proposal' && agreement.Status_Category__c == 'Proposal Approved'){
            
            if (!isValidProposal()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Make sure the fields: \'Country\' and \'Signature Date\' are filled to continue'));
            	system.debug('Make sure the fields: \'Country\' and \'Signature Date\' are filled to continue');
                return null;
            }
            agreement.Error_Log__c = '';
            database.update(agreement);
            futureSign('Proposal', agreement.id);
        }else{
            if (agreement.recordtype.name == 'Purchase Agreement' && agreement.Status_Category__c == 'PA Approved'){
                if (!isValidPA()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue'));
                    system.debug('Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue');
                    return null;
                }
                If(agreement.Parent_Agreement__r.Signature_SObject_Creation__c == 'Correction Mode Flag'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Parent Agreement is in Corretion Mode, please sign it again before signing this agreement.'));
                    return null;
                }
                else{
                    agreement.Error_Log__c = '';
            		database.update(agreement);
                	futureSign('PA', agreement.id);
                }
        	}else{
                if(agreement.Status_Category__c == 'PA Signed' || agreement.Status_Category__c == 'Proposal Signed')
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The ' + agreement.recordtype.name + ' is already signed.'));
                else
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The ' + agreement.recordtype.name + ' must be approved to be signed.'));
            	system.debug('ALREADY SIGNED OR NOT APPROVED');
                return null;
        	}
        } 
        /*system.debug('agreement.Status_Category__c '+ agreement.Status_Category__c);
        system.debug('agreement.Status__c' + agreement.Status__c);
        PageReference returnPage = new PageReference('/'+ agreement.id);  
        returnPage.setRedirect(true);
        return returnPage;
        */
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your request is being processed. It can take a few seconds.'));
        isHide = !isHide;
        return null;
    }

    @future
    public static void futureSign(String AgreementType, Id AgreementId){
        Savepoint sp = Database.setSavepoint();
        try{
            Agreement__c futureAgreement = [SELECT Id, Edit_Mode__c, Signature_SObject_Creation__c, Parent_Agreement__r.Id, Version_Number__c, Is_Amendment__c, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Date__c, Proposal_Validity__c, Nickname__c, Change_History__c, Future_Method__c 
                         FROM Agreement__c 
                         WHERE Id = :AgreementId];
            if (futureAgreement.Edit_Mode__c){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Edit Mode is ON - Can\'t sign.'));
            	system.debug('EDIT MODE');
            	return;
        	}
            String signatureNewObjects = '';
            futureAgreement.Future_Method__c = true;
            if(AgreementType == 'Proposal'){
                futureAgreement.Activated_By__c = UserInfo.getUserId();
                futureAgreement.Activated_Date__c = Date.today();
                futureAgreement.Status_Category__c = 'Proposal Signed';
                futureAgreement.Status__c = 'Activated';
                futureAgreement.Version_Number__c++;
                database.update(futureAgreement);
            }
            else if(AgreementType == 'PA'){
                updateAircraftBaseline(futureAgreement);
                updateSOBEvents(futureAgreement);
    
                futureAgreement.Activated_By__c = UserInfo.getUserId();
                futureAgreement.Activated_Date__c = Date.today();
                futureAgreement.Status_Category__c = 'PA Signed';
                futureAgreement.Status__c = 'Activated';
                futureAgreement.Version_Number__c++;
                database.update(futureAgreement);
                
                if (futureAgreement.Is_Amendment__c==true){
                    signatureNewObjects += CLMSyncAmendment.syncAmendment(String.valueOf(futureAgreement.Id));
                    signatureNewObjects += CLMSetNewActiveAgreement.setNewActiveAgreement(String.valueOf(futureAgreement.Id), String.valueOf(futureAgreement.Parent_Agreement__r.Id));
                }
            }
            system.debug('agreement.Status_Category__c '+ futureAgreement.Status_Category__c);
            system.debug('agreement.Status__c' + futureAgreement.Status__c);
    
            futureAgreement.Signature_SObject_Creation__c = signatureNewObjects;
            futureAgreement.Future_Method__c = false;
            database.update(futureAgreement);
        }catch(exception e){
            Database.rollback(sp);
            Agreement__c agrError = new Agreement__c();
            agrError.Id=Id.valueOf(agreementId);
            agrError.Error_Log__c = e.getMessage();
            database.update(agrError);
        }
    }
    
    private static void updateSOBEvents(Agreement__c futureAgreement){
        List<SOB_Event__c> SOBEventsSelected = [SELECT Id, Event_Date__c, Commercial_Agreement__c
            									FROM SOB_Event__c
            									WHERE Commercial_Agreement__c = :futureAgreement.Id];
        for(SOB_Event__c sobE : SOBEventsSelected){
            if(sobE.Event_Date__c==null)
                sobE.Event_Date__c = futureAgreement.Signature_Date__c;
        }
        if(SOBEventsSelected.size()>0)
        	database.update(SOBEventsSelected);
    }
    
    private static void updateAircraftBaseline(Agreement__c futureAgreement){
        List<Agreement_Aircraft__c> aircraftSelected = [SELECT Id, Order_Type__C, Aircraft_Model__c, Original_Order_Type__C, Original_Aircraft_Model__c 
                                                        FROM Agreement_Aircraft__c
                                                        WHERE Agreement__c = :futureAgreement.Id];
        for(Agreement_Aircraft__c aircraft : aircraftSelected){
            aircraft.Original_Order_Type__C = aircraft.Order_Type__C;
            aircraft.Original_Aircraft_Model__c = aircraft.Aircraft_Model__c;
        }
        if(aircraftSelected.size()>0)
        	database.update(aircraftSelected);
    }
    
    private Boolean isValidProposal(){
        if (agreement.Country__c == null || agreement.Signature_Date__c == null){
            return false;
        }
        
        return true;
    }
    
    private Boolean isValidPA(){
        if(Utils.getStringWhenNull(agreement.Nickname__c) == '' || Utils.getStringWhenNull(agreement.Change_History__c) == '' || Utils.getStringWhenNull(agreement.Country__c) == ''){
           return false; 
        }else{
        
        return true;   
        }
    }

    public PageReference signCompleted(){
        if(isSignatureReady){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'The Agreement is signed!'));
            return null;
        }
        Agreement__c agr = [SELECT name, Error_Log__c FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(agr == null) return null;
        if(agr.Error_Log__c!=null&&agr.Error_Log__c!=''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Amendment was not completed. '+ agr.Error_Log__c));
            return null;
        }
        List<Agreement__c> AgrSigned = [SELECT id FROM Agreement__c WHERE Future_Method__c = false AND Status__c = :'Activated' AND Id=:agreement.Id LIMIT 1];
        if(AgrSigned.isEmpty()){
            if(!isHide)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your request is being processed. It can take a few seconds.'));
            return null;
        }
        newAmendmentId = String.valueOf(AgrSigned.get(0).id);
        if(newAmendmentId!=''){
            isSignatureReady = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'The Agreement is signed!'));
        }
        return null;
    }

    public PageReference goToAgreement(){
        PageReference returnPage = new PageReference('/'+ String.valueOf(agreement.Id));  
        returnPage.setRedirect(true);
        return returnPage;
    }
}
/*
public class CLMSignAgreementController {
    
	public Agreement__c agreement { get; set; }
    private Id parentId;
    private Id childId;
    
    public CLMSignAgreementController(ApexPages.StandardController controller){
        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            system.debug('NULL ID');
            return;
        }
        
        agreement = [SELECT Id, Parent_Agreement__r.Signature_SObject_Creation__c, Parent_Agreement__r.Id, Version_Number__c, Is_Amendment__c, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Date__c, Proposal_Validity__c, Nickname__c, Change_History__c 
                     FROM Agreement__c 
                     WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
        parentId = agreement.Parent_Agreement__r.Id;
        childId = agreement.Id;
        if(agreement == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Agreement not found','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
            system.debug('AGR NOT FOUND');
            return;
    	}
    }
    
    public PageReference sign(){
        if(agreement == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Agreement not found','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
            system.debug('AGR NOT FOUND');
            return null;
    	}
        if(agreement.Parent_Agreement__r.Id != null){
            system.debug('agreement.Parent_Agreement__r.Id' + agreement.Parent_Agreement__r.Id);
            system.debug('agreement.Id' + agreement.Id);
            Integer parentNumber = GEN_Util.howManyContentDocumentLink(Id.valueOf(agreement.Parent_Agreement__r.Id));
            Integer childNumber = GEN_Util.howManyContentDocumentLink(Id.valueOf(agreement.Id));
            //if (GEN_Util.howManyContentDocumentLink(agreement.Parent_Agreement__r.Id) >= GEN_Util.howManyContentDocumentLink(agreement.id)){
            //if (GEN_Util.howManyContentDocumentLink(parentId) >= GEN_Util.howManyContentDocumentLink(childId)){
            if (parentNumber >= childNumber){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'The Agreement must have one more attached file.'));
                system.debug('NEED ONE MORE FILE AMEND');
                return null;
            }
        }
        else{
            Integer childNumber = GEN_Util.howManyContentDocumentLink(Id.valueOf(agreement.Id));
            system.debug('TEM ' + childNumber + ' ARQUIVOS');
            if (childNumber<1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'The Agreement must have one more attached file.'));
                system.debug('NEED ONE MORE FILE PROP/PA');
                return null;
            }
        }
        if (!isValidPA() && agreement.Is_Amendment__c==true){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                       'Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue'));
            system.debug('Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue');
            return null;
        }
     
        if (agreement.recordtype.name == 'Proposal' && agreement.Status_Category__c == 'Proposal Approved'){
            
            if (!isValidProposal()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'Make sure the fields: \'Country\' and \'Signature Date\' are filled to continue'));
            	system.debug('Make sure the fields: \'Country\' and \'Signature Date\' are filled to continue');
                return null;
            }
            futureSign('Proposal', agreement.id);
        }else{
            if (agreement.recordtype.name == 'Purchase Agreement' && agreement.Status_Category__c == 'PA Approved'){
                if (!isValidPA()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                               'Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue'));
                    system.debug('Make sure the fields: \'Country\', \'Signature Date\', \'Nickname\' and \'PA Main Changes\' are filled to continue');
                    return null;
                }
                If(agreement.Parent_Agreement__r.Signature_SObject_Creation__c == 'Correction Mode Flag'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                               'Parent Agreement is in Corretion Mode, please sign it again before signing this agreement.'));
                    return null;
                }
				else
                	futureSign('PA', agreement.id);
        	}else{
                if(agreement.Status_Category__c == 'PA Signed' || agreement.Status_Category__c == 'Proposal Signed')
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The ' + agreement.recordtype.name + ' is already signed.'));
                else
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The ' + agreement.recordtype.name + ' must be approved to be signed.'));
            	system.debug('ALREADY SIGNED OR NOT APPROVED');
                return null;
        	}
        } 
        system.debug('agreement.Status_Category__c '+ agreement.Status_Category__c);
        system.debug('agreement.Status__c' + agreement.Status__c);
        PageReference returnPage = new PageReference('/'+ agreement.id);  
        returnPage.setRedirect(true);
        return returnPage;
    }

    @future
    public static void futureSign(String AgreementType, Id AgreementId){
        Agreement__c futureAgreement = [SELECT Id, Signature_SObject_Creation__c, Parent_Agreement__r.Id, Version_Number__c, Is_Amendment__c, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Date__c, Proposal_Validity__c, Nickname__c, Change_History__c, Future_Method__c 
                     FROM Agreement__c 
                     WHERE Id = :AgreementId];
        String signatureNewObjects = '';
		futureAgreement.Future_Method__c = true;
        if(AgreementType == 'Proposal'){
            futureAgreement.Activated_By__c = UserInfo.getUserId();
            futureAgreement.Activated_Date__c = Date.today();
            futureAgreement.Status_Category__c = 'Proposal Signed';
            futureAgreement.Status__c = 'Activated';
            futureAgreement.Version_Number__c++;
            database.update(futureAgreement);
        }
        else if(AgreementType == 'PA'){
            updateAircraftBaseline(futureAgreement);
            updateSOBEvents(futureAgreement);

            futureAgreement.Activated_By__c = UserInfo.getUserId();
            futureAgreement.Activated_Date__c = Date.today();
            futureAgreement.Status_Category__c = 'PA Signed';
            futureAgreement.Status__c = 'Activated';
            futureAgreement.Version_Number__c++;
            database.update(futureAgreement);
            
            if (futureAgreement.Is_Amendment__c==true){
                signatureNewObjects += CLMSyncAmendment.syncAmendment(String.valueOf(futureAgreement.Id));
                signatureNewObjects += CLMSetNewActiveAgreement.setNewActiveAgreement(String.valueOf(futureAgreement.Id), String.valueOf(futureAgreement.Parent_Agreement__r.Id));
            }
        }
        system.debug('agreement.Status_Category__c '+ futureAgreement.Status_Category__c);
        system.debug('agreement.Status__c' + futureAgreement.Status__c);

        futureAgreement.Signature_SObject_Creation__c = signatureNewObjects;
        futureAgreement.Future_Method__c = false;
        database.update(futureAgreement);
    }
    
    private static void updateSOBEvents(Agreement__c futureAgreement){
        List<SOB_Event__c> SOBEventsSelected = [SELECT Id, Event_Date__c, Commercial_Agreement__c
            									FROM SOB_Event__c
            									WHERE Commercial_Agreement__c = :futureAgreement.Id];
        for(SOB_Event__c sobE : SOBEventsSelected){
            if(sobE.Event_Date__c==null)
                sobE.Event_Date__c = futureAgreement.Signature_Date__c;
        }
        if(SOBEventsSelected.size()>0)
        	database.update(SOBEventsSelected);
    }
    
    private static void updateAircraftBaseline(Agreement__c futureAgreement){
        List<Agreement_Aircraft__c> aircraftSelected = [SELECT Id, Order_Type__C, Aircraft_Model__c, Original_Order_Type__C, Original_Aircraft_Model__c 
                                                        FROM Agreement_Aircraft__c
                                                        WHERE Agreement__c = :futureAgreement.Id];
        for(Agreement_Aircraft__c aircraft : aircraftSelected){
            aircraft.Original_Order_Type__C = aircraft.Order_Type__C;
            aircraft.Original_Aircraft_Model__c = aircraft.Aircraft_Model__c;
        }
        if(aircraftSelected.size()>0)
        	database.update(aircraftSelected);
    }
    
    private Boolean isValidProposal(){
        if (agreement.Country__c == null || agreement.Signature_Date__c == null){
            return false;
        }
        
        return true;
    }
    
    private Boolean isValidPA(){
        if(Utils.getStringWhenNull(agreement.Nickname__c) == '' || Utils.getStringWhenNull(agreement.Change_History__c) == '' || Utils.getStringWhenNull(agreement.Country__c) == ''){
           return false; 
        }else{
        
        return true;   
        }
    }
}*/