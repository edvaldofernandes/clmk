/**
* @author Marcilio Leite de Souza
* @date 20/08/2018
* @description: Trigger Exception Test
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           20 AGO 2018             Original Version
**/
@isTest
public class GEN_TriggerException_Test {

    @isTest
    public static void testTriggerException() {
        
        Test.startTest();
        
        GEN_TriggerException ex = new GEN_TriggerException();
        ex.testCover = 'testCover';
        
        Test.stopTest();
    }
}