public class FO_CreatingArticleFromCase {

    public FO_CreatingArticleFromCase(ApexPages.KnowledgeArticleVersionStandardController ctl) {

        //SObject article = ctl.getRecord();
        String caseId = ctl.getSourceId();        
        Case c = [select Id,subject, description,RecordTypeId from Case where id=:caseId];
        
        RecordType rt = [SELECT Name FROM RecordType WHERE Id =: c.RecordTypeId];
        if (rt.Name == 'FlightOps Case Record Type'){
        
            EmailMessage em = [SELECT Id,HtmlBody,ParentId,Subject,MessageDate FROM EmailMessage WHERE ParentId =: c.Id ORDER BY MessageDate DESC LIMIT 1];
            
            if (em!=null){
            
                FlightOpsCaseAsArticle__kav article = (FlightOpsCaseAsArticle__kav)ctl.getRecord();
                article.put('title', em.Subject); 
                article.put('FlightOps_Content__c',em.HtmlBody);
                ctl.selectDataCategory('Support_Options','Product_Support'); 
                
            }
        
        }
        
    }
}