public with sharing class OpportunityLeadershipReportController
{

    public map<string,reportUtils.reportGroupWrapper> reportGroupMap{get;set;}
    public set<string> cellHeaderValues{get;set;}
    public string embraerSite{get;set;}    
    public boolean haveRowCount{get;set;}
    public boolean haveGroupings{get;set;}

    private Account_Cockpit__c currentRecord;


    public OpportunityLeadershipReportController(ApexPages.StandardController controller) 
    {
        currentRecord = [Select Id,Account_Name__r.Embraer_Site_Nickname__c,Account_Name__r.Company_Nickname__c,Account_Name__r.RecordTypeId, Year__c,Month__c From  Account_Cockpit__c where Id = : controller.getId() Limit 1];
        
        embraerSite = 'Embraer site not found in related Account';
        
        if(string.IsEmpty(currentRecord.Account_Name__r.Embraer_Site_Nickname__c))
        {
            if(currentRecord.Account_Name__r.RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId())
                embraerSite = currentRecord.Account_Name__r.Company_Nickname__c;   
        
        }
        else
        {
            embraerSite = currentRecord.Account_Name__r.Embraer_Site_Nickname__c;
        }
        
        getReportInformation();
    }
    
    
    private void getReportInformation()
    {
    
        String reportId = '';
        Reports.ReportFilter filter;
        List<Reports.ReportFilter> listFilters;
        
        listFilters = new List<Reports.ReportFilter>();
        
        filter = new Reports.ReportFilter();
        filter.setColumn('Account.Embraer_Site_Nickname__c');
        filter.setOperator('equals') ;
        filter.setValue(embraerSite);
        listFilters.add(filter);
        
        Report reportTemp = [Select Id From Report Where Name = 'Leadership Opportunities Report' Limit 1];
        
        reportId = string.valueOf(reportTemp.id);
        
        ReportUtils reportUtilsInstance = new ReportUtils();
        reportUtilsInstance.reportId = reportId;
        reportUtilsInstance.reportFiltersList = listFilters;
        reportUtilsInstance.runReport();
        
        this.cellHeaderValues = reportUtilsInstance.cellHeaderValues;
        this.reportGroupMap = reportUtilsInstance.reportGroupMap;
        this.haveGroupings =  (!this.reportGroupMap.isEmpty());
        this.haveRowCount = reportUtilsInstance.haveRowCount;
        
    }
  

}