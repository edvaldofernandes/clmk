/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for deleting related aircraft when a quote is deleted.
*
* NAME: QuoteDeleteRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*******************************************************************************/

public with sharing class QuoteDeleteRelatedAircraft {

  public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    List<Id> lstOppId = new List<Id>();
    for ( Quote opp: (List<Quote>) trigger.old)
    {
      lstOppId.add(opp.Id);
    }
    if ( lstOppId.isEmpty() ) return;
    
    List<Id> lstProdutoOppId = new List<Id>();
    for ( QuoteLineItem oli : [SELECT Id FROM QuoteLineItem WHERE QuoteId =: lstOppId])
    {
      lstProdutoOppId.add(oli.Id);
    }
    if ( lstProdutoOppId.isEmpty() ) return;
    
    List<Related_Aircraft__c> lstRelAircrafts = [ SELECT Id FROM Related_Aircraft__c 
      WHERE Quote_Line_Item__c =: lstProdutoOppId ];
      
    if ( !lstRelAircrafts.isEmpty() ) delete lstRelAircrafts;
  }
  
}