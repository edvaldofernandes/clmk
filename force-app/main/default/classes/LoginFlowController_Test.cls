/**
* @author Marcilio Leite de Souza
* @date 07/11/2018
* @description: Test Class for LoginFlowController            
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           07 NOV 2018             Original Version
* Renan de Castan e Carvalho		17 DEC 2019				Add possibility to show a second message
**/
@isTest
public class LoginFlowController_Test {
    
    static testMethod void should_redirect_to_login(){
        
        test.startTest();
        LoginFlowController lfc = new LoginFlowController();
        lfc.finishLoginFlowHome();
        test.stopTest();
    }
    
    static testMethod void should_refresh(){
        
        test.startTest();
        LoginFlowController lfc = new LoginFlowController();
        lfc.redirectToSecondPage();
        test.stopTest();
    }
    
}