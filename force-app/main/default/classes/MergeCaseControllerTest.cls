@isTest 
private class MergeCaseControllerTest {
   
    static testMethod void TesteUnitario() {
             
      Case caso = SObjectInstanceTest.createCase();
      caso.Status = 'open';
      
      list<Case> lstcaso = new list<Case>();
      lstcaso.add(caso);
      database.insert(lstcaso);   
          
      EmailMessage email = SObjectInstanceTest.email( caso.Id );
      database.insert(email);
      
      Attachment anexo = SObjectInstanceTest.anexo( email.id );
      database.insert( anexo );
           
      Case caso1 = SObjectInstanceTest.createCase();
      database.insert(caso1);      
         
      test.startTest();
      
       PageReference pageRef = Page.CaseMerge;
       pageRef.getParameters().put('Refid',String.valueOf(caso.id));
       Test.setCurrentPageReference(pageRef);
       MergeCaseController controller = new MergeCaseController(new ApexPages.StandardsetController(lstcaso));    
       controller.LocalizarCase();
       controller.casoController = new Case(ParentId=caso1.Id);
       controller.msg = 'teste';
     	
     	controller.save();
     	
     	
     	
       	for( MergeCaseController.wrapperCase wCaso : controller.lstCase )
       		wCaso.atribuir = true;
			
       	
       controller.save();
       controller.cancel();
      
      test.stopTest();
           
    }    
    
    static testMethod void TesteUnitario2() {
             
      Case caso = SObjectInstanceTest.createCase();
      caso.Status = 'open';
      
      list<Case> lstcaso = new list<Case>();
      lstcaso.add(caso);
      database.insert(lstcaso);   
                    
      Case caso1 = SObjectInstanceTest.createCase();
      database.insert(caso1);      
         
      test.startTest();
      
      ApexPages.StandardsetController stdController = new ApexPages.StandardsetController(lstcaso);
      stdController.setSelected(lstcaso);
       MergeCaseController controller = new MergeCaseController(stdController);    
       
       controller.LocalizarCase();
       controller.casoController = new Case();

       controller.save();
      
      test.stopTest();
           
    }    
    /*
    static testMethod void TesteUnitario3() {
             
      Case caso = SObjectInstanceTest.createCase();
      caso.Status = 'open';
      
      list<Case> lstcaso = new list<Case>();
      lstcaso.add(caso);
      database.insert(lstcaso);   
          
      EmailMessage email = SObjectInstanceTest.email( caso.Id );
      database.insert(email);
      
      Attachment anexo = SObjectInstanceTest.anexo( email.id );
      database.insert( anexo );
           
      Case caso1 = SObjectInstanceTest.createCase();
      database.insert(caso1);      
         
      test.startTest();
      
       PageReference pageRef = Page.CaseMerge;
       pageRef.getParameters().put('Refid',String.valueOf(caso.id));
       Test.setCurrentPageReference(pageRef);
       MergeCaseController controller = new MergeCaseController(new ApexPages.StandardsetController(lstcaso));    
       controller.LocalizarCase();
       controller.casoController = new Case(ParentId=caso1.Id);
       controller.msg = 'teste';
     	
     	controller.save();
     	
     	boolean valueBoolean = true;
     	
       	for( MergeCaseController.wrapperCase wCaso : controller.lstCase )
       	{
			wCaso.atribuir = valueBoolean;
			valueBoolean = !valueBoolean;
       	}
       controller.save();
       controller.cancel();
      
      test.stopTest();
           
    }    
    
    
    */
     
}