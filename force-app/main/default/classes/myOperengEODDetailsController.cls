public class myOperengEODDetailsController {
    @AuraEnabled
    public static EOD__c getEod(Id eodId) {
        return [
            SELECT
            	Description__c,
            	Expiry_Date__c,
            	Applicability__c,
                Related_Case__c,
            	Status__c,
            	Name,
            	Subject__c,
            	Operational_Disposition__c,
            	LastModifiedDate
            FROM 
            	EOD__c
            WHERE
            	Id =: eodId
        ];
    } 
    
    @AuraEnabled
    public static String getCaseNumber(String id){
        Case[] cases = [SELECT CaseNumber FROM Case WHERE Id =: id];
        return cases[0].CaseNumber;
    } 
    
}