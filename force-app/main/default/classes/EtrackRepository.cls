public class EtrackRepository {
    
    public static Map<String, String> findAllAircraft(){
                
        Map<String, String> aircraftMap = new Map<String, String>();
        
        for(Aircraft__c aircraft : [SELECT name, operator__c FROM Aircraft__c WHERE RecordTypeId = :Constants.RECORD_TYPE_ID]){
            if(aircraft.operator__c != null)
                aircraftMap.put(aircraft.name, aircraft.operator__c);
        }
        
        return aircraftMap;
    }
    
    public static Map<String, String> buildAccountMap(){
        
        Map<String, String> accountMap = new Map<String, String>();
        
        for(Account account : [SELECT id, FlyEmbraerId__c FROM Account])
            if(account.FlyEmbraerId__c != null)
            	accountMap.put(account.id, account.FlyEmbraerId__c);
        
        return accountMap;
    }
}