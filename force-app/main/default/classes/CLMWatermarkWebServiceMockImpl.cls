@isTest
global class CLMWatermarkWebServiceMockImpl implements WebServiceMock 
{
   global void doInvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType) 
   {
       CLMWatermarkWebService.serviceEndpoint = 'www.servic.com';
       CLMWatermarkWebService.getWatermarkedFileResponse respElement = new CLMWatermarkWebService.getWatermarkedFileResponse();
       respElement.return_x = 'Mock response';
       response.put('response_x', respElement); 
   }
}