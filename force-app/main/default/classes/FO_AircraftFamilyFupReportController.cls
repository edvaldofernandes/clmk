public class FO_AircraftFamilyFupReportController {
    public Boolean showEJet {get; set;}
    public Boolean showERJ {get; set;}
    public Boolean showE2 {get; set;}
    
    /**
    * @description gets all recent fup updates and formats in a map for better
    * iterativity. 
    **/
    public FO_AircraftFamilyFupReportController(){
        showEJet = false;
        showERJ = false;
        showE2 = false;
    }
    public List<FUP__c> getPublishedFups(){
        Map<Id, List<FUP_Update__c>> response =
            new Map<Id, List<FUP_Update__c>>();        
        List<String> aircraftFamilies = new List<String>();
        
        if(showEJet){
            aircraftFamilies.add('EJet');
        }
        if(showERJ){
            aircraftFamilies.add('ERJ');
        }
        if(showE2){
            aircraftFamilies.add('E2');
        }
        
        return FO_FupRepository.getPublishedFupsByAircraftFamily(aircraftFamilies);
        
    }
    
    public Map<Id, List<FUP_Update__c>> getResponse(){
        Map<Id, List<FUP_Update__c>> response =
            new Map<Id, List<FUP_Update__c>>();        
        List<String> aircraftFamilies = new List<String>();
        
        if(showEJet){
            aircraftFamilies.add('EJet');
        }
        if(showERJ){
            aircraftFamilies.add('ERJ');
        }
        if(showE2){
            aircraftFamilies.add('E2');
        }
        
		System.debug(aircraftFamilies); 

		
        response = FO_FupService.getPublishedFupsWithUpdatesByAircraftFamily(aircraftFamilies);     
        return response;
    }
}