/*******************************************************************************
*                     Copyright (C) 2015 - Cloud2b
*-------------------------------------------------------------------------------
* Controller class for the visualforce page EntitlementNotification.
* 
* NAME: EntitlementNotificationController.cls
* AUTHOR: DPF                                        DATE: 11/02/2015
*
*******************************************************************************/
public with sharing class EntitlementNotificationController {
    
    public ServiceContract contract { get; set; }
    public ProductTypeWrapper[] lineItems { get; set; }
    public Service_Contract_Management__c[] lstConversion { get; set; }
    public String dataGeracao { get; set; }
    public User usuario { get; set; }
    public String modelosAircraft { get; set; }
    public Boolean hasConversions { get; set; }
    
    public class ProductTypeWrapper {
        public String productType { get; set; }
        public List<ContractLineItemWrapper> lstContractLineItemWrapper { get; set; }
    }
    
    public class ContractLineItemWrapper {
        public ContractLineItem cli { get; set; }       
    public List<Service_Contract_Management__c> lstScm { get; set; }
    }
    
    public EntitlementNotificationController(ApexPages.StandardController std )
    {
        lineItems = new List<ProductTypeWrapper>();
        
        Id contractId = std.getId();
        
        List<Id> recordTypeDeliverables = new List<Id>(); 
        
        dataGeracao = system.now().format('MMMMM dd, yyyy');
        
        
        recordTypeDeliverables.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable').getRecordTypeId());
        recordTypeDeliverables.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable (Third Party)').getRecordTypeId());
        recordTypeDeliverables.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable Embraer').getRecordTypeId());
        recordTypeDeliverables.add(Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable eSolutions').getRecordTypeId());
        
        List<ServiceContract> lstContract = [ SELECT ContractNumber, Account.Name, DOCCON__c, SpecialTerms,To__c,Entitlement_Report_Date_Description__c, Account.Id, Number_of_A_C_in_this_contract__c,A_C_Model__c, Expiration_of_entitlement_credits__c FROM ServiceContract WHERE Id =: contractId ];
                        
        contract = lstContract.isEmpty() ? new ServiceContract() : lstContract[0];
        
        if ( contract.A_C_Model__c != null )
        {
            String[] modelos = contract.A_C_Model__c.split(';');
             modelosAircraft = String.join(modelos, '/');
        }
                
        Map<String, Map<Id, List<ContractLineItem>>> mapCli = new Map<String, Map<Id, List<ContractLineItem>>>();
        List<Id> lstLineItemId = new List<Id>();
        
        for ( ContractLineItem lineItem : [ SELECT Id, LineItemNumber, Pricebookentry.Product2.Product_Type__c, 
         PricebookEntry.Product2.Name, PricebookEntry.Product2Id, Quantity, Unit__c, Delivered__c, Converted__c, 
         Balance__c, Location__c, Start_date__c, End_date__c, Description, Ent_Image__c 
         FROM ContractLineItem
         WHERE ServiceContractId =: contract.Id
         ORDER BY PricebookEntry.Product2.ReportOrder__c,Pricebookentry.Product2.Product_Type__c,PricebookEntry.Product2.Name] )
        {
            lstLineItemId.add (lineItem.Id);
            
             Map<Id, List<ContractLineItem>> mapCliWrapper = mapCli.get(lineItem.Pricebookentry.Product2.Product_Type__c);
             if ( mapCliWrapper == null )
             {
                 mapCliWrapper = new Map<Id, List<ContractLineItem>>();
                 mapCli.put(lineItem.Pricebookentry.Product2.Product_Type__c, mapCliWrapper);
             }
             
             List<ContractLineItem> lstCliWrapper = mapCliWrapper.get(lineItem.PricebookEntry.Product2Id);
             if ( lstCliWrapper == null )
             {
                 lstCliWrapper = new List<ContractLineItem>();
                 mapCliWrapper.put(lineItem.PricebookEntry.Product2Id, lstCliWrapper);
             }           
             lstCliWrapper.add(lineItem);
        }
        
        Map<Id, List<Service_Contract_Management__c>> mapScm = new Map<Id, List<Service_Contract_Management__c>>();
        for ( Service_Contract_Management__c scm : [ SELECT Name, Product_reference__c, Status__c, 
          Quantity__c, Unit__c, Provider__r.Name,Start_date__c, End_Date__c, Names__c, 
          Comments__c, Contract_Line_Item_Master__c, Ent_SCM_Image__c, LocationNew__c, Delivered__c
          FROM Service_Contract_Management__c
          WHERE ( Service_Contract_line_item__c =: lstLineItemId OR Contract_Line_Item_Deliverable__c =: lstLineItemId ) And RecordTypeId in : recordTypeDeliverables Order By Start_date__c ASC])
        {
            List<Service_Contract_Management__c> lstScm = mapScm.get(scm.Contract_Line_Item_Master__c);
            if ( lstScm == null )
            {
                lstScm = new List<Service_Contract_Management__c>();
                mapScm.put(scm.Contract_Line_Item_Master__c, lstScm );
            }
            lstScm.add(scm);
        }
        
        lineItems = new List<ProductTypeWrapper>();
        for ( String productType : mapCli.keySet() )
        {
            Map<Id, List<ContractLineItem>> mapCliWrapper = mapCli.get(productType);
            if ( mapCliWrapper == null ) continue;
            
            List<ContractLineItemWrapper> lstCliWrapper = new List<ContractLineItemWrapper>();
            for ( Id productId : mapCliWrapper.keySet() )
            {
                List<ContractLineItem> lstCli = mapCliWrapper.get(productId);
                if ( lstCli == null ) continue;
                
                for ( ContractLineItem c : lstCli )
                {
                    ContractLineItemWrapper cliWrapper = new ContractLineItemWrapper();
                    cliWrapper.cli = c;
                    cliWrapper.lstScm = mapScm.get(c.Id);
                    lstCliWrapper.add(cliWrapper);
                }
            }
            
            ProductTypeWrapper p = new ProductTypeWrapper();
            p.productType = productType;
            p.lstContractLineItemWrapper = lstCliWrapper;
            
            lineItems.add(p);
        }
        
        List<User> lstUser = [SELECT Name, Department, Phone, Fax, Quote_Signature__c  
          FROM User 
          WHERE Id =: UserInfo.getUserId()];
        if ( !lstUser.isEmpty() ) usuario = lstUser[0];
                
        lstConversion = [ SELECT Name, Product_reference__c, 
      Quantity__c, Unit__c, Comments__c, Ent_SCM_Image__c, Date__c
      FROM Service_Contract_Management__c
      WHERE Contract_Line_Item_conversion__c =: lstLineItemId 
      ORDER BY Date__c,Name, Product_reference__c, Quantity__c, Unit__c, Comments__c ];
        
        hasConversions = !lstConversion.isEmpty();
    }

}