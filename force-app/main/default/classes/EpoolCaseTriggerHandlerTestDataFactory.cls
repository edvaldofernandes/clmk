@isTest
public class EpoolCaseTriggerHandlerTestDataFactory {
    @isTest public static void createEpoolCase_Pool(){ 
        //create parts
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='1116-42-1116 MODS 604', Description__c='test', Ecode__c='2074610', Top_Most__c='9408215', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
        List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='NEW SO: CRI - SATENA - 503341 - 24828417',
                           Description='*B2B - Customer Reference: 078/19'
                           +'\n*R3 - R3 Sales Order Exchange: 0024828417'
                           +'\n*R3 - R3 Notification: 000300695184'
                           +'\n*R3 - R3 Service Order: 10938245'
                           +'\n*B2B - Sales Force Number: 0002445551'
                           +'\n\n*B2B - STANDARD: Sales Force'
                           +'\n*R3 - R3 Customer Name: SERVICIO AEREO A TERRITORIOS'
                           +'\n*R3 - Customer Number:0000503341'
                           +'\n\n*B2B - Contract based on: Priority'
                           +'\n*B2B - DocumentType: EXCHANGE'
                           +'\n*B2B - Invalid Ship To [&1]'
                           +'\n*B2B - PERMANENT: Create new ShipTo, TEMPORARY: Update Order'
                           +'\n*B2B - Address: PERMANENT'
                           +'\n*B2B - Name: SATENA ATT: DEPÓSITO PRIVADO SATENA'
                           +'\n*B2B - SORT1: BOGOTA'
                           +'\n*B2B - STREET: AV ELDORADO ENTRADA 1, INTERIOR 1 -'
                           +'\n*B2B - LOCATION: EX'
                           +'\n*B2B - PO_BOX: 99999'
                           +'\n*B2B - City: BOGOTA'
                           +'\n*B2B - COUNTRY: CO'
                           +'\nUS EXPRESS PICK UP'
                           +'\nWarning: by changing agreed contractual Ship to address you are assuming all charges related to this specific shipment. Feel free to'
                           +'\n\n*R3 - Line Number: 010100'
                           +'\n*R3 - Emb.Code: 000000000002074610'
                           +'\n*R3 - Part Number: 1116-42-1116 MODS 604'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 00'
                           +'\n*B2B - Lead time to deliver this item (hours): 96.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n\n*R3 - Line Number: 010200'
                           +'\n*R3 - Emb.Code: 000000000002074610'
                           +'\n*R3 - Part Number: 1116-42-1116 MODS 604'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 02'
                           +'\n*R3 - Need by date (yyyy/mm/dd): 20190602'
                           +'\n*B2B - Lead time to deliver this item (hours): 24.00'
                           +'\n*B2B - PoolClass: [&1]',
                           RecordTypeId=rTypeId,
                           Is_Unit_EC_Blocked__c='No'));
        insert cases;
    }
    
    @isTest public static void createEpoolCase_OSS(){
        //create parts
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='104003-2', Description__c='test', Ecode__c='3883442', Top_Most__c='3883442', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
        List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='NEW SO: OSS - FLYBE - 506865 - 24829259',
                           Description='*B2B - Customer Reference: PX0369319'
                           +'\n*R3 - R3 Sales Order Exchange: 0024829259'
                           +'\n*R3 - R3 Notification: 000300695293'
                           +'\n*R3 - R3 Service Order: 000010938377'
                           +'\n*B2B - Sales Force Number: 0002446313'
                           +'\n\n*B2B - STANDARD: Sales Force'
                           +'\n*R3 - R3 Customer Name: FLYBE AVIATION SERVICES'
                           +'\n*R3 - Customer Number:0000506865'
                           +'\n\n*B2B - Contract based on: Need by Date'
                           +'\n*B2B - DocumentType: OSS_REPLENISHMENT'
                           +'\n*B2B - DocumentType: OSS'
                           +'\nK&N'
                           +'\nWarning: by changing agreed contractual Ship to address you are assuming all charges related to this specific shipment. Feel free to'
                           +'\n\n*R3 - Line Number: 010100'
                           +'\n*R3 - Emb.Code: 000000000003883442'
                           +'\n*R3 - Part Number: 104003-2'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 00'
                           +'\n*B2B - Attention! Dangerous Goods item must have a specific package'
                           +'\n*B2B - Lead time to deliver this item (hours): 144.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n\n*R3 - Line Number: 010200'
                           +'\n*R3 - Emb.Code: 000000000003883442'
                           +'\n*R3 - Part Number: 104003-2'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 02'
                           +'\n*R3 - Need by date (yyyy/mm/dd): 20190603'
                           +'\n*B2B - OSS consumed SN EF0658 . Create "OSS Adjustment" and "SENDING OSS"'
                           +'\n*B2B - Attention! Dangerous Goods item must have a specific package'
                           +'\n*B2B - Lead time to deliver this item (hours): 4.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n*B2B - Invalid Serial Number typed by user on the web, review Item Serial Number before create "OSS Adjustment.',
                           RecordTypeId=rTypeId,
                           Is_Unit_EC_Blocked__c='No'));
        insert cases;
    }
    
    @isTest public static void createMultiCases(){
        //create parts
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='104003-2', Description__c='test', Ecode__c='3883442', Top_Most__c='3883442', Is_Part_Serialized__c='Serialized'));
        parts.add(new LLPDatabase__c(Name='1116-42-1116 MODS 604', Description__c='test', Ecode__c='2074610', Top_Most__c='9408215', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create accounts
        List<Account> accts = new List<Account>();
        accts.add(new Account(Name='FLYBE', Company_Nickname__c='FLYBE'));
        insert accts;
        
        //create contacts
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(Email='sepremote@fly1be1.com', AccountId=accts[0].Id, LastName='Lname'));
        insert contacts;
        
        //create mfirs
        List<MFIR__c> mfirs = new List<MFIR__c>();
        mfirs.add(new MFIR__c(Name='506865', MFIR_number__c='506865', Blockage__c='Not Blocked', Account__c=accts[0].Id));
        insert mfirs;
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
        List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='NEW SO: OSS - FLYBE - 506865 - 24829259',
                           Description='*B2B - Customer Reference: PX0369319'
                           +'\n*R3 - R3 Sales Order Exchange: 0024829259'
                           +'\n*R3 - R3 Notification: 000300695293'
                           +'\n*R3 - R3 Service Order: 000010938377'
                           +'\n*B2B - Sales Force Number: 0002446313'
                           +'\n\n*B2B - STANDARD: Sales Force'
                           +'\n*R3 - R3 Customer Name: FLYBE AVIATION SERVICES'
                           +'\n*R3 - Customer Number:0000506865'
                           +'\n\n*B2B - Contract based on: Need by Date'
                           +'\n*B2B - DocumentType: OSS_REPLENISHMENT'
                           +'\n*B2B - DocumentType: OSS'
                           +'\nK&N'
                           +'\nWarning: by changing agreed contractual Ship to address you are assuming all charges related to this specific shipment. Feel free to'
                           +'\n\n*R3 - Line Number: 010100'
                           +'\n*R3 - Emb.Code: 000000000003883442'
                           +'\n*R3 - Part Number: 104003-2'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 00'
                           +'\n*B2B - Attention! Dangerous Goods item must have a specific package'
                           +'\n*B2B - Lead time to deliver this item (hours): 144.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n\n*R3 - Line Number: 010200'
                           +'\n*R3 - Emb.Code: 000000000003883442'
                           +'\n*R3 - Part Number: 104003-2'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 02'
                           +'\n*R3 - Need by date (yyyy/mm/dd): 20190603'
                           +'\n*B2B - OSS consumed SN EF0658 . Create "OSS Adjustment" and "SENDING OSS"'
                           +'\n*B2B - Attention! Dangerous Goods item must have a specific package'
                           +'\n*B2B - Lead time to deliver this item (hours): 4.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n*B2B - Invalid Serial Number typed by user on the web, review Item Serial Number before create "OSS Adjustment.',
                           RecordTypeId=rTypeId,
                           Is_Unit_EC_Blocked__c='No'));
        cases.add(new Case(Subject='NEW SO: CRI - SATENA - 503341 - 24828417',
                           Description='*B2B - Customer Reference: 078/19'
                           +'\n*R3 - R3 Sales Order Exchange: 0024828417'
                           +'\n*R3 - R3 Notification: 000300695184'
                           +'\n*R3 - R3 Service Order: 10938245'
                           +'\n*B2B - Sales Force Number: 0002445551'
                           +'\n\n*B2B - STANDARD: Sales Force'
                           +'\n*R3 - R3 Customer Name: SERVICIO AEREO A TERRITORIOS'
                           +'\n*R3 - Customer Number:0000503341'
                           +'\n\n*B2B - Contract based on: Priority'
                           +'\n*B2B - DocumentType: EXCHANGE'
                           +'\n*B2B - Invalid Ship To [&1]'
                           +'\n*B2B - PERMANENT: Create new ShipTo, TEMPORARY: Update Order'
                           +'\n*B2B - Address: PERMANENT'
                           +'\n*B2B - Name: SATENA ATT: DEPÓSITO PRIVADO SATENA'
                           +'\n*B2B - SORT1: BOGOTA'
                           +'\n*B2B - STREET: AV ELDORADO ENTRADA 1, INTERIOR 1 -'
                           +'\n*B2B - LOCATION: EX'
                           +'\n*B2B - PO_BOX: 99999'
                           +'\n*B2B - City: BOGOTA'
                           +'\n*B2B - COUNTRY: CO'
                           +'\nUS EXPRESS PICK UP'
                           +'\nWarning: by changing agreed contractual Ship to address you are assuming all charges related to this specific shipment. Feel free to'
                           +'\n\n*R3 - Line Number: 010100'
                           +'\n*R3 - Emb.Code: 000000000002074610'
                           +'\n*R3 - Part Number: 1116-42-1116 MODS 604'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 00'
                           +'\n*B2B - Lead time to deliver this item (hours): 96.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n\n*R3 - Line Number: 010200'
                           +'\n*R3 - Emb.Code: 000000000002074610'
                           +'\n*R3 - Part Number: 1116-42-1116 MODS 604'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 02'
                           +'\n*R3 - Need by date (yyyy/mm/dd): 20190602'
                           +'\n*B2B - Lead time to deliver this item (hours): 24.00'
                           +'\n*B2B - PoolClass: [&1]',
                           RecordTypeId=rTypeId,
                           Is_Unit_EC_Blocked__c='No'));
        cases.add(new Case(Subject='NEW SO: RTN - SKYWESTBOISE - 97192 - 24751403',
                           Description='*B2B - Customer Reference: R2427270'
                           +'\n*R3 - R3 Sales Order Exchange: 0024751403'
                           +'\n*R3 - R3 Notification: 000300684698'
                           +'\n*R3 - R3 Service Order: 10925664'
                           +'\n*B2B - Sales Force Number: 0002387078'
                           +'\n\n*B2B - STANDARD: Sales Force'
                           +'\n*R3 - R3 Customer Name: SKYWEST AIRLINES, INC.'
                           +'\n*R3 - Customer Number:0000097192'
                           +'\n\n*B2B - Contract based on: Priority'
                           +'\n*B2B - DocumentType: REPAIR'
                           +'\nFedEx Standard Overnight® 3567-7683-6'
                           +'\nWarning: by changing agreed contractual Ship to address you are assuming all charges related to this specific shipment. Feel free to'
                           +'\n\n*R3 - Line Number: 010100'
                           +'\n*R3 - Emb.Code: 000000000000977094'
                           +'\n*R3 - Part Number: 4260-0018-5'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 00'
                           +'\n*B2B - Lead time to deliver this item (hours): 0.00'
                           +'\n*B2B - PoolClass: [&1]'
                           +'\n\n*R3 - Line Number: 010200'
                           +'\n*R3 - Emb.Code: 000000000003883442'
                           +'\n*R3 - Part Number: 104003-2'
                           +'\n*B2B - External Line Number:1'
                           +'\n*R3 - Priority: 03'
                           +'\n*R3 - Need by date (yyyy/mm/dd): 20190408'
                           +'\n*B2B - Lead time to deliver this item (hours): 0.00'
                           +'\n*B2B - PoolClass: [&1]',
                           RecordTypeId=rTypeId,
                           Is_Unit_EC_Blocked__c='No'));
        
        insert cases;
    }
}