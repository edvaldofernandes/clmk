/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for copying the custom fields from the opportunity line items
* to related service contract line items when they are created.
*
* NAME: ContractCopyLineItem.cls
* AUTHOR: DPF                                                DATE: 16/12/2014
*
*******************************************************************************/
public with sharing class ContractCopyLineItem {

  public static void execute() {

    TriggerUtils.assertTrigger();

    set<Id> lstServiceContractId = new set<Id>();
    list<ContractLineItem> lstContractLineItem = new list<ContractLineItem>();
    for ( ContractLineItem iServiceContract : (list<ContractLineItem>) trigger.new  )
    {
      lstContractLineItem.add(iServiceContract);
      lstServiceContractId.add(iServiceContract.ServiceContractId);
    }

    map<Id, Id> mapServiceContractOpportunityId = new map<Id, Id>();
    for ( ServiceContract iServiceContract : [SELECT Id, Opportunity__c FROM ServiceContract WHERE Id =: lstServiceContractId ])
    {
      mapServiceContractOpportunityId.put(iServiceContract.Id, iServiceContract.Opportunity__c);
    }
    if ( mapServiceContractOpportunityId.isEmpty() ) return;

    map<Id, list<OpportunityLineItem>> mapOppOli = new map<Id, list<OpportunityLineItem>>();
    for ( OpportunityLineItem oli: [SELECT OpportunityId, PricebookEntryId,
      End_date__c, Entry_fee__c, Hotel__c, Margin_0__c, Maximum_Discount__c,
      Pilot__c, princing__c, Region__c, Slot__c, Slot_Errors__c, Slot_Image__c,
      Slot_status__c, Start_date__c, Training_id__c, Unit__c,
      Quantity, UnitPrice, Discount__c, Sales_Price__c, Catalogue_Price__c
      FROM OpportunityLineItem
      WHERE OpportunityId =: mapServiceContractOpportunityId.Values() ])
    {
      list<OpportunityLineItem> lstOli = mapOppOli.get(oli.OpportunityId);
      if ( lstOli == null )
      {
        lstOli = new list<OpportunityLineItem>();
        mapOppOli.put(oli.OpportunityId, lstOli);
      }
      lstOli.add(oli);
    }
    if ( mapOppOli.isEmpty() ) return;

    for ( ContractLineItem iServiceContract : lstContractLineItem )
    {
      Id idOpp = mapServiceContractOpportunityId.get(iServiceContract.ServiceContractId);
      if ( IdOpp == null ) continue;
      list<OpportunityLineItem> lstOli = mapOppOli.get(idOpp);
      if ( lstOli == null ) continue;
      Integer i = -1;
      for ( OpportunityLineItem oli : lstOli )
      {
        i++;
        if ( oli.PricebookEntryId == iServiceContract.PricebookEntryId  && oli.Quantity == iServiceContract.Quantity
          && oli.UnitPrice == iServiceContract.UnitPrice )
        {
          iServiceContract.End_date__c = oli.End_date__c;
          iServiceContract.Entry_fee__c = oli.Entry_fee__c;
          iServiceContract.Hotel__c = oli.Hotel__c;
          iServiceContract.Margin_0__c = oli.Margin_0__c;
          iServiceContract.Maximum_Discount__c = oli.Maximum_Discount__c;
          iServiceContract.Pilot__c = oli.Pilot__c;
          iServiceContract.princing__c = oli.princing__c;
          iServiceContract.Region__c = oli.Region__c;
          iServiceContract.Slot__c = oli.Slot__c;
          iServiceContract.Slot_Errors__c = oli.Slot_Errors__c;
          iServiceContract.Slot_Image__c = oli.Slot_Image__c;
          iServiceContract.Slot_status__c = oli.Slot_status__c;
          iServiceContract.Start_date__c = oli.Start_date__c;
          iServiceContract.Training_id__c = oli.Training_id__c;
          iServiceContract.Unit__c = oli.Unit__c;
          iServiceContract.Discount__c = oli.Discount__c;
          //iServiceContract.UnitPrice = oli.UnitPrice;          
          //iServiceContract.Sales_Price__c = oli.Sales_Price__c;
          iServiceContract.Catalogue_Price__c = oli.Catalogue_Price__c;
          break;
        }
      }
      if(lstOli.size() > i && i >= 0 ) lstOli.remove(i);
    }

  }
}