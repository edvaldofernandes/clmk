// Used by DIM - Market Intelligence.
@isTest
public class DIM_SampleDataTest {

    //--------------------------------------------------------------------------
    static testMethod void validateAccountsCreation() {
        DIM_SampleData.createSampleData();
        System.assertEquals(25, [SELECT count() FROM Account]);
        System.assertEquals(5, [SELECT count() FROM Opportunity]);
        System.assertEquals(2, [SELECT count() FROM Contact]);
        System.assertEquals(21, [SELECT count() FROM Case]);
    }
    
    //--------------------------------------------------------------------------
    static testMethod void testSandboxPreparation() {

        Test.startTest();
        Test.testSandboxPostCopyScript(new DIM_SampleData(), UserInfo.getOrganizationId(), UserInfo.getOrganizationId(), UserInfo.getOrganizationName());
        Test.stopTest();

        System.assertEquals(25, [SELECT count() FROM Account]);
        System.assertEquals(5, [SELECT count() FROM Opportunity]);
        System.assertEquals(2, [SELECT count() FROM Contact]);
        System.assertEquals(21, [SELECT count() FROM Case]);
    }
}