public class VSS_ControllerVssSurvey {
    
    private ApexPages.StandardController controller {get; set;}
    public Communications__c objCommu {get; set;}
    public String error {get; set;}
    
    public VSS_ControllerVssSurvey(ApexPages.StandardController controller){
        this.controller = controller;
        this.objCommu = (Communications__c) controller.getRecord();  
        error = '';
    }
    
    public PageReference saveCommu(){
        try{
            
            objCommu.Name = 'SurveyVSS2018_' + Datetime.now().format();  
            objCommu.Communication_Status__c = 'Draft';
            objCommu.RecordTypeId = Schema.SObjectType.Communications__c.getRecordTypeInfosByName().get('REC').getRecordTypeId();
            system.debug('VSS_ControllerVssSurvey.saveCommu.objCommu: ' + objCommu);
            insert objCommu;
            
        }catch(Exception e){
            
            system.debug('VSS_ControllerVssSurvey.saveCommu.e.getMessage(): ' + e.getMessage());
            error = e.getMessage();
        }finally{
            objCommu = new Communications__c();
            error = '';
        }
        
        return null;
    }
}