@isTest
public class FO_PDF2PNGControllerTest {
    public static ContentVersion generateContentVersion(Id recordId){
        return generateContentVersion(recordId, 20);
    }
    public static ContentVersion generateContentVersion(Id recordId, Integer blobSize){
        Blob loremIpsumBlob = Blob.valueOf('X'.repeat(blobSize));
        ContentVersion conVer = new ContentVersion(
            ContentLocation = 'S',
            PathOnClient = 'lorem_ipsum',
            Title = 'Lorem Ipsum',   
            VersionData = loremIpsumBlob
        );
        insert conVer;        
        Id documentId = [
            SELECT
                ContentDocumentId
            FROM
                ContentVersion
            WHERE
                Id =:conVer.Id
        ].ContentDocumentId;
        
        
        ContentDocumentLink cDe = new ContentDocumentLink(
            ContentDocumentId = documentId,
            LinkedEntityId = recordId,
            ShareType = 'I',
            Visibility = 'AllUsers'        
        );
        insert cDe;
        return [
            SELECT
            	Id,
				ContentLocation,
            	ContentDocumentId,
            	PathOnClient,
            	Title,
            	VersionData
            FROM
            	ContentVersion
            WHERE 
            	Id =: conVer.Id
        ];
    }

    @isTest static void canSaveImage(){
        Integer pageNumber = 10;
        
        String someBase64Image = 'base64 image here';
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion contentVersion = generateContentVersion(eod.Id);
        Id documentId = contentVersion.ContentDocumentId;
        
        // try to mock later
        FO_PDF2PNGController.saveImage(someBase64Image, documentId, pageNumber);        
    }

    @isTest static void canBuildJsonResponse(){
        Map<Id, String> contentVersionMap = new Map<Id, String>();
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion contentVersion = generateContentVersion(eod.Id);
        
        String data = EncodingUtil.base64Encode(contentVersion.VersionData);
        contentVersionMap.put(contentVersion.ContentDocumentId, data);
        String expectedResponse = JSON.serialize(contentVersionMap);
        
        String actualResponse = FO_PDF2PNGController.buildJsonResponse(new List<ContentVersion>{contentVersion});
        System.assertEquals(expectedResponse, actualResponse);
    }       
    @isTest static void canLoadJsonResponse(){
        Map<Id, String> contentVersionMap = new Map<Id, String>();
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion contentVersion = generateContentVersion(eod.Id);
        
        
        
        String data = EncodingUtil.base64Encode(contentVersion.VersionData);
        contentVersionMap.put(contentVersion.ContentDocumentId, data);
        String expectedResponse = JSON.serialize(contentVersionMap);
        
        FO_PDF2PNGController controller = new FO_PDF2PNGController();
        controller.recordId = eod.Id;
        String actualResponse = controller.getAttachedPdfList();
        
		System.assertEquals(expectedResponse, actualResponse);
    }   
    @isTest static void canExecuteAction(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion contentVersion = generateContentVersion(eod.Id);
        FO_PDF2PNGController controller = new FO_PDF2PNGController();
        controller.recordId = eod.Id;
        String actualResponse = controller.getAttachedPdfList();
        //System.debug(actualResponse);
    }
    @isTest static void testIfRedirectUrlIsCorrect(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion contentVersion = generateContentVersion(eod.Id);

        FO_PDF2PNGController controller = new FO_PDF2PNGController();
        controller.recordId = eod.Id;
        String expectedUrl = '/' + eod.Id;
        String url = controller.getPageReferenceUrl();
        System.assertEquals(expectedUrl, url);
    }
    @isTest static void testIfItIsReturningToRecordPage(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion contentVersion = generateContentVersion(eod.Id);

        FO_PDF2PNGController controller = new FO_PDF2PNGController();
        controller.recordId = eod.Id;
        PageReference page = controller.redirect();
    }        
    @isTest static void testIfItCanInsertImagesInSizeLimit(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        Integer blobSize = 1000000;
        generateContentVersion(eod.Id, blobSize);
        
        Test.startTest();
            FO_PDF2PNGController controller = new FO_PDF2PNGController();
            controller.recordId = eod.Id;
            String actualResponse = controller.getAttachedPdfList();
       	Test.stopTest();
    }
    @isTest static void testIfItThrowsAnExceptionWhenAttachmentIsTooBig(){
        String exceptionType = 'FlightOpsException';
        EOD__c eod = new FO_EODTestDataBuilder().build();
        Integer blobSize = FO_PDF2PNGController.FILE_SIZE_LIMIT;
        generateContentVersion(eod.Id, blobSize);
        
        Test.startTest();
            try{
                FO_PDF2PNGController controller = new FO_PDF2PNGController();
                controller.recordId = eod.Id;
                String actualResponse = controller.getAttachedPdfList();
            } catch(Exception e){
                System.assertEquals(exceptionType, e.getTypeName());
            }
       	Test.stopTest();
    }
    
}