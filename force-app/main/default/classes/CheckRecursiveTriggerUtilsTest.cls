@isTest
public class CheckRecursiveTriggerUtilsTest {
    static testmethod void checkRecursiveTest(){
        boolean hasRun = false;
        if(CheckRecursiveTriggerUtils.runOnce()){
            hasRun = true;
        }
        System.assertEquals(true, hasRun, 'Did not run for the fisrt time');
        hasRun = false;
        if(CheckRecursiveTriggerUtils.runOnce()){
            hasRun = true;
        }
        System.assertEquals(false, hasRun, 'It ran twice');
    }

}