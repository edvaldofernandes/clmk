@isTest
public class FO_FupDigestPreviewControllerTest {
    @isTest static void testCanGetEmailPreview(){
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        
        Contact contact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;
        
        
        String email = FO_FupDigestPreviewController.getEmailPreview(contact.Id);


        System.assert(!String.isEmpty(email));
    }
    @isTest static void testCanGetEmailPreviewWheNoContactIsInputted(){
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        
        Contact contact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;
        
        
        
        
        String email = FO_FupDigestPreviewController.getEmailPreview(null);
        System.assert(!String.isEmpty(email));
    }
    
    /**
    * @description test if can gets all contacts that must receive the FUP Report.
    **/
    @isTest static void testCanGetContacts(){
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        
        Contact contact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;
        
        
        List<Contact> contacts = FO_FupDigestPreviewController.getContacts();
        System.assert(!contacts.isEmpty());
    }
    /**
    * @description test if the method to send the digest is called.
    **/
    @isTest static void testCanSendFupDigest(){
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        
        Contact contact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;
        
        
        Test.startTest();
        FO_FupDigestPreviewController.sendFupDigest();
        Test.stopTest();
    }
}