/**
* Controller class for the visualforce page CFCProductView.
* Name: CFCProductViewController
* @author - elvsantos@deloitte.com
* @version 1.0 - 10/12/2015
**/ 
public with sharing class CFCProductViewController 
{	
	public CFCProductViewController()
	{      
	} 
    
    public pageReference RedirectToDetails(){
        system.debug('CFCProductDetailsController.RedirectToDetails()');
        
        String idProduct = ApexPages.currentPage().getParameters().get('id');
        
        PageReference page = new PageReference('/apex/CFCProductDetails?id='+ idProduct + '&View=true');
        page.setRedirect(true);
        return page;  
    }
}