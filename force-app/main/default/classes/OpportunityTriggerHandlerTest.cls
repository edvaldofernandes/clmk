/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class OpportunityTriggerHandlerTest
{


    static testMethod void myUnitTest() 
    {
        
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert acc;
  
        Contact contato = new Contact();
        contato.firstName = 'Teste';
        contato.LastName = 'unitário';
        contato.accountId = acc.Id;
        insert contato;
  
        Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
        Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
        pbAirMod.Name = 'Aircraft Modification';
        pbAirMod.Type__c = 'Aircraft Modification';
        insert pbAirMod;
  
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
        opp.Pricebook2Id = pbAirMod.id;
        opp.stageName = 'Qualification';
        insert opp ;
    
        Quote quote = SObjectInstanceTest.createQuote( opp.Id );
        quote.Pricebook2Id = pbAirMod.Id;
        quote.OpportunityId= opp.Id;
        quote.Name = 'First Quote';
        insert quote;  
        
        Quote quote1 = SObjectInstanceTest.createQuote( opp.Id );
        quote1.Pricebook2Id = pbAirMod.Id;
        quote1.OpportunityId= opp.Id;
        quote1.Name = 'Second Quote';
        insert quote1;    
    
        OpportunityContactRole oppCr  = new OpportunityContactRole();
        oppCr.ContactId = contato.Id;
        oppCr.OpportunityId = opp.Id;
        insert oppCr;
    
    	Product2 prod = SObjectInstanceTest.createProduct2();
      	prod.Unit__c = 'Aircraft';
      	prod.Product_Type__c = 'Aircraft Modification';
      	prod.Family = 'Aircraft Modification';
      	prod.Applicability__c = 'Turboprop';
      	prod.Rom__c = true;
      	insert prod;
    
      	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
      	insert pbe;
    
      	PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
      	insert pbeAirMod ;
      
      	list<OpportunityLineItem> oppItem = new list<OpportunityLineItem >();
    
      	OpportunityLineItem oppItemEntryFee = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
      	oppItemEntryFee.Princing__c = 'Entry fee';
      	oppItemEntryFee.opportunityId = opp.Id;
      	oppItem.add(oppItemEntryFee );
    
    
   	 	OpportunityLineItem oppItemNrec = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
      	oppItemNrec.Princing__c = 'NREC';
      	oppItemNrec.Entry_fee__c = 200;
      	oppItemNrec.opportunityId = opp.Id;
      	oppItem.add(oppItemNrec);
      
      	
      
      	insert oppItem; 
    
		opp.SyncedQuoteId = quote.Id;
		opp.StageName = 'Proposal/Price Quote';
		update opp;
		
		
        Opportunity oportunidade = new Opportunity();
        oportunidade.Name = 'Oportunidade Teste Unitário';
        oportunidade.CloseDate = System.Today() + 5;
        oportunidade.StageName = 'Prospect';
        oportunidade.Fleet_Type__c = 'EJET';
        oportunidade.Expected_Payment_Mode__c = 'CASH';
        oportunidade.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        oportunidade.Approval_status__c = 'Test';
        insert oportunidade;
       
        
        quote.Status = 'Presented to Customer';
        update quote;
       
        
        opp.StageName = 'Contract Negotiation & Review';
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
        update opp;
        
        oportunidade.Approval_status__c = 'Approved';
        update oportunidade;
        
        opp.RecordTypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        opp.StageName = 'Prospect';
        update opp;
        
        
        
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
        opp.StageName = 'Closed Lost';
        opp.Reason_for_Rejection__c = 'Price too high';
        opp.Comments_for_Rejection__c = 'Any comments, just to bypass';
        update opp;
        
        
        
                         
        delete opp;
        
        undelete opp;
        
        OpportunityTriggerHandler handler = new OpportunityTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
        
        try
        {
            oportunidade.StageName = 'Closed Won';
            oportunidade.Approval_status__c = 'Approved';
            update oportunidade;
        }
        catch(Exception ex)
        {
            System.Assert(ex.getMessage().contains('You can´t change opportunity stage to "Closed Won" without a related Signed Contract.'));
        }
        
       
             
    }
    
    static testMethod void myUnitTest2() 
    {
        
        Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert acc;
  
        Contact contato = new Contact();
        contato.firstName = 'Teste';
        contato.LastName = 'unitário';
        contato.accountId = acc.Id;
        insert contato;
  
        Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
        Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
        pbAirMod.Name = 'eSolutions';
        pbAirMod.Type__c = 'eSolutions';
        insert pbAirMod;
  
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        opp.Pricebook2Id = pbAirMod.id;
        opp.stageName = 'Qualification';
        insert opp ;
        
        OpportunityContactRole oppCr  = new OpportunityContactRole();
        oppCr.ContactId = contato.Id;
        oppCr.OpportunityId = opp.Id;
        insert oppCr;
    
    	Product2 prod = SObjectInstanceTest.createProduct2();
      	prod.Name = 'eSolutions product';
      	prod.Unit__c = 'License';
      	prod.Product_Type__c = 'eTechPubs';
      	prod.ProductCode = 'a499';
      	prod.Family = 'eSolutions';
      	prod.Applicability__c = 'ERJ';
      	prod.Rom__c = true;
      	prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
      	insert prod;
      	
      	Product2 prod2 = SObjectInstanceTest.createProduct2();
      	prod2.Unit__c = 'Aircraft';
      	prod2.name = 'Teste de Aircraft';
      	prod2.Product_Type__c = 'Aircraft Modification';
      	prod2.Family = 'Aircraft Modification';
      	prod2.Applicability__c = 'Turboprop';
      	prod2.Rom__c = true;
      	prod2.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
      	insert prod2;
    
      	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod2.Id );
      	insert pbe;
    
      	PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod2.Id );
      	insert pbeAirMod ;
    
    	PricebookEntry pbe1 = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
      	insert pbe1;
    
      	PricebookEntry pbeAirMod1 = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
      	insert pbeAirMod1 ;
    
      
      	list<OpportunityLineItem> oppItem = new list<OpportunityLineItem >();
    
      	OpportunityLineItem oppItemEntryFee = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
      	oppItemEntryFee.Princing__c = 'Entry fee';
      	oppItemEntryFee.opportunityId = opp.Id;
      	oppItemEntryFee.Product_type_updated__c = 'eSolutions';
      	oppItem.add(oppItemEntryFee );
    
    
   	 	OpportunityLineItem oppItemNrec = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
      	oppItemNrec.Princing__c = 'NREC';
      	oppItemNrec.Entry_fee__c = 200;
      	oppItemNrec.opportunityId = opp.Id;
      	oppItemNrec.Product_type_updated__c = 'eSolutions';
      	oppItem.add(oppItemNrec);
      
      	
      
      	insert oppItem; 
    
    	OpportunityLineItem oppItemEntryFee1 = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod1.Id);      
      	oppItemEntryFee1.Princing__c = 'Entry fee';
      	oppItemEntryFee1.opportunityId = opp.Id;
      	insert oppItemEntryFee1;
    
    
   	 	OpportunityLineItem oppItemNrec1 = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod1.Id);      
      	oppItemNrec1.Princing__c = 'NREC';
      	oppItemNrec1.Entry_fee__c = 200;
      	oppItemNrec1.opportunityId = opp.Id;
      	insert oppItemNrec1;
    
	    
    
             
    }


}