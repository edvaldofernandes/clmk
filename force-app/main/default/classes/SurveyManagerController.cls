public with sharing class SurveyManagerController {
    
    private ApexPages.StandardController stdControl;
    public Boolean editCSS {get; set;}
    public String surveyId {get;set;}
    public String reportId {get;set;}
    public SurveyForce__c survey {get;set;}

    public Message pageMessage {
        get {
            if (pageMessage == null) {
                pageMessage = new Message();
            }
            return pageMessage;
        }
        set;
    }

    public SurveyManagerController(ApexPages.StandardController stdController){
        this.stdControl = stdController;
        if(!test.isRunningTest()){stdController.addFields(new List<String>{'Hide_Survey_Name__c', 'Survey_Header__c', 'Thank_You_Text__c', 'Survey_Container_CSS__c'});}
        survey = (SurveyForce__c)stdController.getRecord();

        try {

            List<User> res = [SELECT Profile.PermissionsAuthorApex FROM User WHERE id = :Userinfo.getUserId()];
            User u = res[0];
            if (u.Profile.PermissionsAuthorApex) {
                this.editCSS = true;
            } else {
                this.editCSS = false;
            }

            surveyId = stdController.getRecord().Id;
            surveyId = surveyId.substring(0,15);
            String reportName = 'Survey with Questions and Responses';
            Sobject myReport = [select Id, Name From Report Where Name = :reportName];
            reportId = myReport.Id;
        }catch(Exception e){
            pageMessage.setMessage(e.getMessage(), 'error');
            ApexPages.addMessages(e);
        }
    }

    public Pagereference save(){
        //Adding default CSS to add some space around survey
        //But this is changeable
        try {
            survey.Survey_Container_CSS__c = (String.isEmpty(survey.Survey_Container_CSS__c))?'':survey.Survey_Container_CSS__c.replaceAll('<[^>]+>',' ');
            update survey;
        }catch(Exception e){pageMessage.setMessage(e.getMessage(), 'error');ApexPages.addMessages(e);}
        return null;
    }
}