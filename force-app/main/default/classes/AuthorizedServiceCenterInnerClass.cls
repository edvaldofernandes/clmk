public class AuthorizedServiceCenterInnerClass {
    private Attachment attachment;
    private Attachments__c attachmentCustom;
    
    public Attachment getAttachment () {
        return attachment;
    }
    
    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }
    
    public Attachments__c getAttachmentCustom() {
        return attachmentCustom;
    }
    
    public void setAttachmentCustom(Attachments__c attachmentCustom) {
        this.attachmentCustom = attachmentCustom;
    }
}