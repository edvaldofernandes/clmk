global class CRM360_MonthlyRegisterCloner implements Schedulable{ 

    global void execute(SchedulableContext context){
        
        Map<Integer, String> monthMap = new Map<Integer, String>();
        monthMap.put(1, '1 - JANUARY');
        monthMap.put(2, '2 - FEBRUARY');
        monthMap.put(3, '3 - MARCH');
        monthMap.put(4, '4 - APRIL');
        monthMap.put(5, '5 - MAY');
        monthMap.put(6, '6 - JUNE');
        monthMap.put(7, '7 - JULY');
        monthMap.put(8, '8 - AUGUST');
        monthMap.put(9, '9 - SEPTEMBER');
        monthMap.put(10, '10 - OCTOBER');
        monthMap.put(11, '11 - NOVEMBER');
        monthMap.put(12, '12 - DECEMBER');
        
        List<Account_Cockpit__c> accountCockpitsInsert = new List<Account_Cockpit__c>();
        List<Account_Cockpit__c> accountCockpits = [SELECT 
                                                  CRM360_Highlights__c,
                                                  CRM360_Highlights_Indicator__c,
                                                  CRM360_Highlights_Date__c,
                                                  CRM360_EOC__c, 
                                            	  CRM360_EOC_Indicator__c, 
                                                  CRM360_EOC_Date__c,
                                            	  CRM360_Finance_Description__c, 
                                                  CRM360_Finance_Indicator__c, 
                                            	  CRM360_Finance_Date__c, 
                                                  CRM360_Operational_Performance__c,
                                            	  CRM360_Operational_Performance_Indicator__c, 
                                            	  CRM360_Operational_Performance_Date__c,
                                            	  CRM360_Parts_Performance__c, 
                                                  CRM360_Parts_Performance_Indicator__c,
                                                  CRM360_Parts_Performance_Date__c,
                                            	  CRM360_Service_Sales__c, 
                                                  CRM360_Service_Sales_Indicator__c,
                                                  CRM360_Service_Sales_Date__c,
                                            	  CRM360_Tech_Issues__c, 
                                                  CRM360_Tech_Issues_Indicator__c, 
                                                  CRM360_Tech_Issues_Date__c,
                                                  Creation_Date__c,
                                                  Is_Record__c,
                                                  Is_Updated__c,
                                                  Account_Name__c,
                                                  Customer_Expectation__c,
                                                  Att_Level__c,
                                                  RecordTypeId,
                                                  Related_EOC__c,
                                                  Name
                                                  FROM Account_Cockpit__c
                                                  WHERE (Record_Type_ID_ref__c = 'CMR360_Dashboard_Data'
                                                  AND Is_Record__c = false)];
        
        for(Account_Cockpit__c accountCockpit : accountCockpits){
            
            //Clone the register and update its fields
            Account_Cockpit__c ac = accountCockpit.clone();
            ac.Is_Record__c = true;
            
            //Get the register date as current date
            Date registerDate = Date.today();
            ac.Year__c = String.valueOf(registerDate.year());
            ac.Month__c = monthMap.get(registerDate.month());
            
            accountCockpitsInsert.add(ac);
        }
        
        insert accountCockpitsInsert;
    }
    
}