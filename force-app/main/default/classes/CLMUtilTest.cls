@istest
private class CLMUtilTest {
	static testMethod void autoMapTest2()
   	{
        // LISTA ALEATORIA DE DADOS
        List<Account> accountList = new List<Account>{
            SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government')),
            SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government')),
            SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'))
        };
        
        Database.insert(accountList);
        
        List<Contact> contactList = new List<Contact>{
            SObjectInstanceTest.createContact(accountList.get(0).Id),
            SObjectInstanceTest.createContact(accountList.get(1).Id),
            SObjectInstanceTest.createContact(accountList.get(2).Id)
        };
        
        Database.insert(contactList);
        
        
        test.startTest();
        
        // MAPEAMENTO DE LISTA DE Contatos - UTILIZANDO ACCOUNT_NAME COMO CHAVE
        Map<String,List<Contact>> map1 = (Map<String,List<Contact>>) CLMUtil.autoMap([Select Id, Account.Name from Contact where Id in:contactList], 'Account.Name');
        
        
        test.stopTest();
    }
    
    static testMethod void autoMapTest1()
   	{
        // LISTA ALEATORIA DE DADOS
        List<String> names  = new List<String>{'A','B','C','D','E'};
        List<String> cities = new List<String>{'SaoPaulo','Diadema','Curitiba','Diadema','SaoPaulo'};
        List<Account> test_list = new List<Account>();
        List<Contact> empty_list = new List<Contact>();
        Account aux;
        
		for ( Integer i = 0; i < names.size(); i++ )
        {
        	aux = new Account ();
            aux.Name = names.get(i);
            aux.BillingCity = cities.get(i);
            
            test_list.add(aux);
        }
        
        
        test.startTest();
        
        // MAPEAMENTO DE LISTA DE ACCOUNTS - UTILIZANDO NAME COMO CHAVE
        Map<String,List<Account>> map1 = (Map<String,List<Account>>) CLMUtil.autoMap(test_list, 'Name');
        
        system.assertEquals('Diadema', map1.get('B').get(0).BillingCity);
        system.assertEquals('Curitiba', map1.get('C').get(0).BillingCity);
        
        // MAPEAMENTO DE LISTA DE ACCOUNTS - UTILIZANDO BILLINGCITY COMO CHAVE
        Map<String,List<Account>> map2 = (Map<String,List<Account>>) CLMUtil.autoMap(test_list, 'BillingCity');
        
        system.assertEquals(2, map2.get('SaoPaulo').size());
        system.assertEquals(1, map2.get('Curitiba').size());
        system.assertEquals('B', map2.get('Diadema').get(0).Name);
        
        // MAPEAMENTO DE LISTA VAZIA
        Map<String,List<Contact>> map3 = (Map<String,List<Contact>>) CLMUtil.autoMap(empty_list, 'Name');
        
        system.assertEquals(0, map3.size());
        
        test.stopTest();
    }
    
    static testMethod void setAdminMode ()
    {
        Product2 prod = SObjectInstanceTest.createProduct2();
        database.insert(prod);
        
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        insert opp;
        
        Kickoff__c kickoff = new Kickoff__c();
        kickoff.Opportunity__c = opp.id;
        insert kickoff;        
        
        id pb2 = SObjectInstanceTest.getPricebook2Std2();
                
        database.insert(SObjectInstanceTest.createPricebookEntry(pb2, prod.Id));  
        
        Account account = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
		
        Apttus__APTS_Agreement__c apttusAPTSAgreement = new Apttus__APTS_Agreement__c();
        apttusAPTSAgreement.Name = 'Teste 1';
        apttusAPTSAgreement.Apttus__Account__c = account.Id;
        apttusAPTSAgreement.Country__c = 'Brasil';
        apttusAPTSAgreement.Agreement_Code__c = 'ABCDEFG';
        apttusAPTSAgreement.Region_Code__c = 'RegionCODE';
        apttusAPTSAgreement.Apttus__Status_Category__c = 'In Amendment';
        apttusAPTSAgreement.RecordTypeId = RecordTypeMemory.getRecType('Apttus__APTS_Agreement__c', 'Purchase_Agreement');
        
        apttusAPTSAgreement.Apttus__Company_Signed_Date__c = Date.today(); 
        apttusAPTSAgreement.Kickoff_trigger_already_executed__c = false;
        System.debug('apttusAPTSAgreement.id: ' + apttusAPTSAgreement);
        insert apttusAPTSAgreement;
        
        Apttus__APTS_Agreement__c apttusAPTSAgreement2 = new Apttus__APTS_Agreement__c();
        apttusAPTSAgreement2.Name = 'Teste 1';
        apttusAPTSAgreement2.Apttus__Account__c = account.Id;
        apttusAPTSAgreement2.Country__c = 'Brasil';
        apttusAPTSAgreement2.Agreement_Code__c = 'ABCDEFG';
        apttusAPTSAgreement2.Region_Code__c = 'RegionCODE';
        apttusAPTSAgreement2.Apttus__Status_Category__c = 'In Amendment';
        apttusAPTSAgreement2.RecordTypeId = RecordTypeMemory.getRecType('Apttus__APTS_Agreement__c', 'Purchase_Agreement');
        
        apttusAPTSAgreement2.Apttus__Company_Signed_Date__c = Date.today(); 
        apttusAPTSAgreement2.Kickoff_trigger_already_executed__c = false;   
        System.debug('apttusAPTSAgreement.id: ' + apttusAPTSAgreement2);
        insert apttusAPTSAgreement2;
        
        
        Set<String> stringAgreementIds = new Set<String>{
            apttusAPTSAgreement.Id,
            apttusAPTSAgreement2.Id
        };
        
        Set<Id> idAgreementIds = new Set<Id>{
            apttusAPTSAgreement.Id,
            apttusAPTSAgreement2.Id
        };
        
        test.startTest();
        
        CLMUtil.setAdminMode ( stringAgreementIds, true, 'TEST001' );
        CLMUtil.setAdminMode ( stringAgreementIds, false, 'TEST001'  );
        
        CLMUtil.setAdminMode ( idAgreementIds, true, 'TEST002' );
        CLMUtil.setAdminMode ( idAgreementIds, false, 'TEST002'  );
        
        test.stopTest();
        
    }
}