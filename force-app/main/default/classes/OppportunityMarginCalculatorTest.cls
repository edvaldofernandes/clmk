@isTest
public class OppportunityMarginCalculatorTest {
    
    private static Id ACMOD_RecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
    private static Id TechServ_RecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();

    @testSetup
    static void setup(){
        
        //Create products
        Product2 prodNormal = new Product2(Name = 'Normal Product', recordtypeid = ACMOD_RecordType);
        insert prodNormal;
        Product2 prodNoCostControl = new Product2(Name = 'No Cost Product', recordtypeid = ACMOD_RecordType);
        insert prodNoCostControl;
        Product2 prodCCIncomplete = new Product2(Name = 'Incomplete PCC Product', recordtypeid = ACMOD_RecordType);
        insert prodCCIncomplete;
        Product2 TechServProd = new Product2(Name = 'TechServ Product', recordtypeid = TechServ_RecordType);
        insert TechServProd;
        
        //Create Product Cost Control
        Product_Cost_Control__c productCostControl1 = new Product_Cost_Control__c(
            Related_Product__c = prodNormal.Id,
            Amortization_Cost_per_a_c__c = 120,
            REC__c = 3500,
            Sales_Deduction__c = 5.2,
            Tech_Pubs_SB_Revision_Costs__c = 10000,
            Is_Active__c = true
        );
        insert productCostControl1;  
        

        
        Product_Cost_Control__c productCostControl2 = new Product_Cost_Control__c(
            Related_Product__c = prodCCIncomplete.Id,            
            Is_Active__c = true
        );
        insert productCostControl2;  
        
        
        //Create Opportunities
        List<Opportunity> oppList = new List<Opportunity>();
        
        Opportunity oppTest1 = new Opportunity(
            Name= 'OppTest 1',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest1);        
        Opportunity oppTest2 = new Opportunity(
            Name= 'OppTest 2',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest2);
        Opportunity oppTest3 = new Opportunity(
            Name= 'OppTest 3',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest3);        
        Opportunity oppTest4 = new Opportunity(
            Name= 'OppTest 4',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );        
        oppList.add(oppTest4);        
        Opportunity oppTest5 = new Opportunity(
            Name= 'OppTest 5',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest5);        
        Opportunity oppTest6 = new Opportunity(
            Name= 'OppTest 6',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest6);
        
        Opportunity oppTest7 = new Opportunity(
            Name= 'OppTest 7',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest7);
        Opportunity oppTest8 = new Opportunity(
            Name= 'OppTest 8',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        oppList.add(oppTest8);
        insert oppList;
        
        //Create Pricebook
        PricebookEntry pricebookEntry1 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prodNormal.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry1;
        PricebookEntry pricebookEntry2 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prodNoCostControl.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry2;
        PricebookEntry pricebookEntry3 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = TechServProd.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry3;
        PricebookEntry pricebookEntry4 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prodCCIncomplete.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry4;
        
        List<OpportunityLineItem> oppProdList = new List<OpportunityLineItem>();
        //Create Opportunity Products
        OpportunityLineItem oppProduct1 = new OpportunityLineItem(
            OpportunityId = oppTest1.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 7200,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 72000,
            Princing__c = 'REC'
        );
        oppProdList.add(oppProduct1);
        OpportunityLineItem oppProduct2 = new OpportunityLineItem(
            OpportunityId = oppTest1.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000,
            Princing__c = 'NREC'
        );
        oppProdList.add(oppProduct2);
        OpportunityLineItem oppProduct3 = new OpportunityLineItem(
            OpportunityId = oppTest2.Id,
            PricebookEntryId = pricebookEntry2.Id,
            Product2Id = prodNoCostControl.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,            
            Total_amount__c = 130000,
            Princing__c = 'NREC'
        );
        oppProdList.add(oppProduct3);
                OpportunityLineItem oppProduct4 = new OpportunityLineItem(
            OpportunityId = oppTest4.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 7200,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 72000,
            Princing__c = 'REC'
        );
        oppProdList.add(oppProduct4);
        OpportunityLineItem oppProduct5 = new OpportunityLineItem(
            OpportunityId = oppTest5.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000,
            Princing__c = 'NREC'
        );
        oppProdList.add(oppProduct5);
        OpportunityLineItem oppProduct6 = new OpportunityLineItem(
            OpportunityId = oppTest6.Id,
            PricebookEntryId = pricebookEntry3.Id,
            Product2Id = TechServProd.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000
        );
        oppProdList.add(oppProduct6);
        OpportunityLineItem oppProduct7 = new OpportunityLineItem(
            OpportunityId = oppTest7.Id,
            PricebookEntryId = pricebookEntry3.Id,
            Product2Id = TechServProd.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000
        );
        oppProdList.add(oppProduct7);
        OpportunityLineItem oppProduct8 = new OpportunityLineItem(
            OpportunityId = oppTest7.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 7200,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 72000,
            Princing__c = 'REC'
        );
        oppProdList.add(oppProduct8);
        OpportunityLineItem oppProduct9 = new OpportunityLineItem(
            OpportunityId = oppTest8.Id,
            PricebookEntryId = pricebookEntry4.Id,
            Product2Id = prodCCIncomplete.Id,
            UnitPrice = 7200,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 72000,
            Princing__c = 'REC'
        );
        oppProdList.add(oppProduct9);
        
        Insert oppProdList;
        
        Product_Margin_Calculation__c PMCNormal = new Product_Margin_Calculation__c(
            Related_Product__c = prodNormal.Id,
            Related_Opportunity__c = oppTest1.Id,
            Is_Active__c = true
        );
        insert PMCNormal;
        
        

        
        //Create Bulk Test opportunities
        /*List<Opportunity> opps = new List<Opportunity>();
        for(Integer i=0; i < 50; i++){
            opps.add(new Opportunity(Name= 'Bulk OppTest'+i,Fleet_Type__c = 'TestFleet', StageName = 'Prospecting', CloseDate = Date.today().addDays(7)));
        }
        insert opps;*/

    }
    static testmethod void test01_NormalProcedure(){
        Product_Margin_Calculation__c oldPMC = null;
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 1' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
    	omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
    
        oldPMC = [
            select
            	Is_Active__c,
            	Related_Opportunity__c,
            	Related_Product__c
            from
            	Product_Margin_Calculation__c
            where 
            	Related_Opportunity__c = :opp.Id
            		AND
            	Is_Active__c = false
        ];
    	System.assert(oldPMC != null, 'There is no old PMC deactivated');
        System.assertEquals('Valid', opp.Calculation_Status__c, 'Status');
        System.assertEquals(90.9, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(51.4, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(76.3, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
    static testmethod void test02_NoCostControlProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 2' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
    	omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('All Products must have a Product Cost Control associated with it. At least one does not have a cost associated to the product. Please contact your BD.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(0, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
    static testmethod void test03_NoProductsProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 3' limit 1];
        Test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
    	omc.updateOrInsert(opp.Id);

        Test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('This opportunity has no products associated with it.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(0, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
    static testmethod void test04_NormalRECOnlyProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 4' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
    	omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(51.4, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(51.4, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
    static testmethod void test05_NormalNRECOnlyProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 5' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
    	omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid', opp.Calculation_Status__c, 'Status');
        System.assertEquals(90.9, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(90.9, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
        static testmethod void test06_NoACMODProductProcedureOfMixedOpp(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 6' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
    	omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('This opportunity has no AC Mods products associated with it.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(0, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    static testmethod void test07_NormalProcedureOfMixedOpp(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 7' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
        omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid, but there are products that arent aicraft modifications and will not be considered in the gross margin calculation.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(51.4, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(51.4, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    static testmethod void test08_IncompletePCC(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 8' limit 1];
        test.startTest();
        OpportunityMarginCalculator omc = new OpportunityMarginCalculator();
        omc.updateOrInsert(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('All Product Cost Control must be completed. At least one does not have the required values. Please contact your BD.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(0, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
}