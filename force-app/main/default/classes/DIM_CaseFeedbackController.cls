// Used by DIM - Market Intelligence.
global class DIM_CaseFeedbackController {

    //----------------------------------------------------------------------------------------------------------------------------------------------------   
    global class WrapperClass {
    
        @InvocableVariable
        global ID feedbackId;
        
        @InvocableVariable
        global ID templateId;
        
        @InvocableVariable
        global String email;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------   
    @InvocableMethod(label='DIM Case Feedback Email')
    global static void sendEmail(List<WrapperClass> request) {
    
        List<WrapperClass> cls = new List<WrapperClass>();
        for(WrapperClass wrp: request) {
            cls.add(wrp);
        }
            
        try {
            User user = [SELECT Id, Email FROM User WHERE Email =: cls[0].email LIMIT 1];
            
            Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(cls[0].templateId, user.Id, cls[0].feedbackId);
            emailMessage.setToAddresses(new List<String>{user.Email});
            emailMessage.setSaveAsActivity(false);
            emailMessage.setUseSignature(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{emailMessage});
        }
        catch(Exception e) {
        }
    }
}