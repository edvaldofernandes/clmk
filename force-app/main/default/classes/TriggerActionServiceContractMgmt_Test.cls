/**
* @author Felipe Gouvea
* @date 11/09/2018
* @description: Test Class for TriggerActionServiceContractMgmt      
* @comments: 
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
**/

@isTest(SeeAllData=true)
private class TriggerActionServiceContractMgmt_Test
{
	@isTest
	static void itShould()
	{
		// Given
		Service_Contract_Management__c contractManagement = new Service_Contract_Management__c();

		TriggerHandlerServiceContractMgmt handler = new TriggerHandlerServiceContractMgmt();
		TriggerActionServiceContractMgmt action = new TriggerActionServiceContractMgmt(handler);

		// When
		insert contractManagement;
		database.delete(contractManagement);
		database.undelete(contractManagement.Id);

		// Then

	}
    
    @isTest
	static void itShouldTestEntitlementConversion()
	{
    
    	Product2 product = SObjectInstanceTest.createProduct2();
    	database.insert(product);
    
    	PricebookEntry pbEntry = SObjectInstanceTest.createPricebookEntry(test.getStandardPricebookId(), product.Id);
    	database.insert(pbEntry);

    	Account acc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Operator'));
    	database.insert(acc);
        
        Pricebook2 catalog = SObjectInstanceTest.getPricebook2Std();
        
		// Given
		ServiceContract serviceContract = new ServiceContract();
    	serviceContract.AccountId = acc.Id;
    	serviceContract.Pricebook2Id = catalog.Id;
    	serviceContract.Name = acc.Name;
    	serviceContract.DOCCON__c = '123456';

    	serviceContract.recordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Entitlement').getRecordTypeId();
    	database.insert(serviceContract);
    
    	ContractLineItem contractLineItem = new ContractLineItem();
    	contractLineItem.ServiceContractId = serviceContract.Id;
    	contractLineItem.PricebookEntryId = pbEntry.Id;
    	contractLineItem.Quantity = 13;
    	contractLineItem.UnitPrice = 10;
    	contractLineItem.Catalogue_Price__c = 10;
    	contractLineItem.Sales_Price__c = 10;
    	database.insert(contractLineItem);

    	Service_Contract_Management__c serviceContractManagement = new Service_Contract_Management__c();
    	serviceContractManagement.Quantity__c = 1.0;
        serviceContractManagement.Contract_Line_Item_conversion__c = contractLineItem.Id;
		serviceContractManagement.recordTypeId = Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId();
		
        Test.startTest();
        insert serviceContractManagement;
        Test.stopTest();

	}
    
    @isTest
    static void itShouldTriggerDisable()
    {
        // Given
        Service_Contract_Management__c contractManagement = new Service_Contract_Management__c();
        
        TriggerHandlerServiceContractMgmt handler = new TriggerHandlerServiceContractMgmt();
        TriggerActionServiceContractMgmt action = new TriggerActionServiceContractMgmt(handler);
        
        GEN_TriggerHelper.disableTrigger();
        
        insert contractManagement;

    }
    
}