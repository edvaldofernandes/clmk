public without sharing class AgreementLineItemDAO {
    public static List<Agreement_Aircraft__c> getLineItensBySetIds(Set<String> setIds, Integer year){//Agreement_Aircraft__c
        string setToString = '';
        for(String includeValue : setIds){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();

        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +  ', Aircraft__r.name, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
            'Agreement__r.RecordType.name, Agreement__r.Status_Category__c,' +
            'Agreement__r.Agreement_Color__c' +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Aircraft__c IN (' + setToString +') AND CALENDAR_YEAR(Contractual_Delivery_Month__c) =' + year +
            ' ORDER BY df_code__c ASC NULLS LAST';

        //system.debug('SELECT VALUE query: '+querySoql);
        daoList = database.query(querySoql);
        //system.debug('DAOLIST' +daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensBySetIds(Set<String> setIds){
        string setToString = '';
        for(String includeValue : setIds){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();

        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +  ', Aircraft__r.name, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
            'Agreement__r.RecordType.name, Agreement__r.Status_Category__c,' +//Status_Category__c
            'Agreement__r.Agreement_Color__c' +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Aircraft__c IN (' + setToString +') ' +
            ' ORDER BY df_code__c ASC NULLS LAST';

        //system.debug('SELECT VALUE query: '+querySoql);
        daoList = database.query(querySoql);
        //system.debug('DAOLIST' +daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensBySetAgreementsIds(Map<String, Agreement__c> mapAgreementAptt){
        string setToString = '';
        for(String includeValue : mapAgreementAptt.keySet()){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();
        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Agreement__c IN (' + setToString +') ';
        daoList = database.query(querySoql);
        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementId(String agreementId){
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();
        daoList = database.query(' Select ' + utils.getAllFields('Agreement_Aircraft__c') + ', Aircraft__r.name, Aircraft__r.Aircraft_Family__c, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
                                 'Agreement__r.RecordType.name, Agreement__r.Status_Category__c, Agreement__r.Agreement_Code__c' +
                                 ', Agreement__r.Region_Code__c FROM Agreement_Aircraft__c WHERE Agreement__c = \'' +
                                 agreementId +'\' ORDER BY Aircraft__r.Name ASC, Contractual_Delivery_Month__c');

        //System.debug('AgreementLineItemDAO daoList' + daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementId(String agreementId, String productFamily){
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();
        daoList = database.query(' Select ' + utils.getAllFields('Agreement_Aircraft__c') + ', Aircraft__r.name, Aircraft__r.Aircraft_Family__c, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
                                 'Agreement__r.RecordType.name, Agreement__r.Status_Category__c, Agreement__r.Agreement_Code__c' +
                                 ', Agreement__r.Region_Code__c FROM Agreement_Aircraft__c WHERE Agreement__c = \'' +
                                 agreementId +'\' AND Aircraft__r.Aircraft_Family__c = \''+productFamily+'\' ORDER BY Aircraft__r.Name ASC, Contractual_Delivery_Month__c');

        //System.debug('AgreementLineItemDAO daoList' + daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementIdOrderedByFirmTrend(String agreementId){

        List<Agreement_Aircraft__c> daoList = [SELECT Order_Type__c, TREND_Delivery_Date__c, Aircraft_Configuration__c,
                                                      Aircraft__r.Model__c, Agreement__r.Agreement_Code__c, Agreement__r.Region_Code__c,
                                                      TREND_DF_Code__c, TREND_Country_Code__c
                                                      FROM Agreement_Aircraft__c
                                                      WHERE Agreement__c =: agreementId AND Order_type__c = 'firm'
                                                      ORDER BY TREND_Delivery_Date__c , Aircraft__r.Model__c];
        //System.debug('daoList soql : ' + daoList);
        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementIdOrderedByTrend(String agreementId, String productFamily){

        List<Agreement_Aircraft__c> daoList = [SELECT Order_Type__c, DF_Code__c, DF_Country_Code__c, TREND_Delivery_Date__c, Aircraft_Configuration__c,
                                                      Aircraft__r.Model__c, Agreement__r.Agreement_Code__c, Agreement__r.Region_Code__c,
                                                      TREND_DF_Code__c, TREND_Country_Code__c
                                                      FROM Agreement_Aircraft__c
                                                      WHERE Agreement__c =: agreementId AND Order_type__c <> 'Purchase Right' AND Aircraft__r.Aircraft_Family__c =: productFamily
                                                      ORDER BY Order_type__c, TREND_Delivery_Date__c , Aircraft__r.Model__c];
        //System.debug('daoList soql : ' + daoList);
        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementIdOrderedByFirmTrend(String agreementId, String productFamily){

        List<Agreement_Aircraft__c> daoList = [SELECT Order_Type__c, TREND_Delivery_Date__c, Aircraft_Configuration__c,
                                                      Aircraft__r.Model__c, Agreement__r.Agreement_Code__c, Agreement__r.Region_Code__c,
                                                      TREND_DF_Code__c, TREND_Country_Code__c
                                                      FROM Agreement_Aircraft__c
                                                      WHERE Agreement__c =: agreementId AND Order_type__c = 'firm' AND Aircraft__r.Aircraft_Family__c =: productFamily
                                                      ORDER BY TREND_Delivery_Date__c , Aircraft__r.Model__c];
        System.debug('daoList soql : ' + daoList);
        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementIdOrderedByFirmContract(String agreementId){

        List<Agreement_Aircraft__c> daoList = [SELECT Order_Type__c, DF_Code__c, DF_Country_Code__c, TREND_Delivery_Date__c, Aircraft_Configuration__c,
                                                      Aircraft__r.Model__c, Agreement__r.Agreement_Code__c, Agreement__r.Region_Code__c,
                                                      TREND_DF_Code__c, TREND_Country_Code__c
                                                      FROM Agreement_Aircraft__c
                                                      WHERE Agreement__c =: agreementId
                                                      ORDER BY Order_Type__c, Contractual_Delivery_Month__c , Aircraft__r.Model__c];

        //System.debug('daoList soql : ' + daoList);
        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensByAgreementIdOrderedByFirmContract(String agreementId, String productFamily){

        List<Agreement_Aircraft__c> daoList = [SELECT Order_Type__c, DF_Code__c, DF_Country_Code__c, TREND_Delivery_Date__c, Aircraft_Configuration__c,
                                                      Aircraft__r.Model__c, Agreement__r.Agreement_Code__c, Agreement__r.Region_Code__c,
                                                      TREND_DF_Code__c, TREND_Country_Code__c
                                                      FROM Agreement_Aircraft__c
                                                      WHERE Order_Type__c <> 'Purchase Right' AND Agreement__c =: agreementId AND Aircraft__r.Aircraft_Family__c =:productFamily
                                                      ORDER BY Order_Type__c, Contractual_Delivery_Month__c , Aircraft__r.Model__c];

        //System.debug('daoList soql : ' + daoList);
        return daoList;
    }

    public static List<Agreement_Aircraft__c> getContractualDateOrderedAircrafts ( Set<Id> agreementsIds )
    {
        String query = getBasicNamingQuery ('agreementsIds', 'Contractual_Delivery_Month__c');
        List<Agreement_Aircraft__c> daoList = Database.query ( query );

        System.debug('AgreementLineItemDAO.getContractualDateOrderedAircrafts: ' + query );

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getTrendDateOrderedAircrafts ( Set<Id> agreementsIds )
    {
        String query = getBasicNamingQuery ('agreementsIds', 'TREND_Delivery_Date__c');

        System.debug('AgreementLineItemDAO.getTrendDateOrderedAircrafts: ' + query );
        List<Agreement_Aircraft__c> daoList = Database.query ( query );

        return daoList;
    }

    public static String getBasicNamingQuery ( String filterVariableName, String orderFieldName ){

        String basicQuery = 'SELECT '+
                    ' Order_Type__c, DF_Code__c, DF_Country_Code__c, TREND_Delivery_Date__c, Aircraft_Configuration__c, ' +
                            '   Aircraft__r.Model__c, Agreement__r.Agreement_Code__c, Agreement__r.Region_Code__c, ' +
                            '   TREND_DF_Code__c, TREND_Country_Code__c, Agreement__c, Aircraft__r.Aircraft_Family__c ' +
                            'FROM Agreement_Aircraft__c ' +
                            'WHERE Order_Type__c <> \'Purchase Right\' AND Agreement__c IN :'+filterVariableName+' ' +
                    'ORDER BY Agreement__c, Aircraft__r.Aircraft_Family__c, Order_Type__c, '+orderFieldName+', Aircraft__r.Model__c ';
        return basicQuery;
    }

    public static List<Agreement_Aircraft__c> getAgreementsByStatus (Set<String> notIn){
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();
        daoList = [SELECT
                   ID,
                   Current_Snapshot_Version__c,
                   Aircraft__c,
                   Aircraft_Family__c,
                   Aircraft_Name__c,
                   Contractual_Delivery_Month__c,
                   Agreement__c,
                   Agreement__r.Agreement_Color__c,
                   Agreement__r.Region_Code__c,
                   Agreement__r.Status_Category__c,
                   DF_Country_Code__c,
                   Aircraft__r.DF_Group__c,
                   DF_Code__c,
                   Order_Type__c,
                   Show_Aircraft_As__c,
                   Summary_Skyline__c,
                   TREND_Delivery_Date__c,
                   TREND_Country_code__c,
                   TREND_DF_Code__c
                   FROM
                   Agreement_Aircraft__c
                   WHERE Agreement__r.Status_Category__c = 'PA Signed'
                   AND Agreement__r.Status_Category__c NOT IN :notIn];
        //system.debug('daoList+++ ' +daoList);
        return daoList;
    }

    // GSAMICO: Nova função utilizada no Delivery Forecast, exclui os seguintes Line Itens:
    // 1) Line itens cujo Agreement (Proposta) Possui Status = "Proposal Signed" e que já possua um PA relacionado
    // 2) Line itens cujo Agreement (PA) Possui uma Proposta relacionada, mas essa proposta ainda não foi assinada (enable_Sync ainda ativo)
    public static List<Agreement_Aircraft__c> getLineItensBySetIdAndYearforDF(String aircraftFamily, Integer year, Set<String> notIn){
        string setToString = '';
        string notInString = '';
        /*for(String includeValue : setIds){
            setToString += '\''+includeValue + '\',';
        }*/
        for(String includeValue : notIn){
            notInString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        notInString = notInString.removeEnd(',');

        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();

        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +  ', Aircraft__r.name, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
            'Agreement__r.RecordType.name, Agreement__r.Status_Category__c,' +
            'Agreement__r.Agreement_Color__c, Agreement__r.Region_Code__c' +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Aircraft__r.Show_In_DF__c = true AND Aircraft__r.Aircraft_Family__c = \''+ aircraftFamily + '\' AND Agreement__r.Status_Category__c NOT IN ('+ notInString +')' +
            ' AND (CALENDAR_YEAR(Contractual_Delivery_Month__c) =' + year + ' OR CALENDAR_YEAR(TREND_Delivery_Date__c)  = ' + year + ')' +         
            ' AND (Agreement__r.Status_Category__c <> \'Proposal Signed\' OR Agreement__r.Related_Proposal__c = NULL)' +
            //Status__c diferente dos abaixo, não aparecerá no Delivery Forecast - 01/12/2016 - Henrique DC
            ' AND Status__c <> \'Converted\'' +
            ' AND Status__c <> \'Expired\'' +
            ' AND Status__c <> \'Terminated\'' +
            ' AND ( Agreement__r.RecordType.name <> \'Purchase Agreement\' OR Agreement__r.Enable_Sync__c <> TRUE OR Agreement__r.Related_Proposal__c = NULL )' +
            ' ORDER BY df_code__c ASC NULLS LAST';

      //  system.debug('QUERY ONLINE>>> ' +querySoql);
        daoList = database.query(querySoql);
       // system.debug('AgreementLineItemDAO getLineItensBySetIdAndYear daoList : ' +daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensBySetIdAndYear(Set<String> setIds, Integer year, Set<String> notIn){
        string setToString = '';
        string notInString = '';
        for(String includeValue : setIds){
            setToString += '\''+includeValue + '\',';
        }
        for(String includeValue : notIn){
            notInString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        notInString = notInString.removeEnd(',');

        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();

        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +  ', Aircraft__r.name, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
            'Agreement__r.RecordType.name, Agreement__r.Status_Category__c,' +
            'Agreement__r.Agreement_Color__c, Agreement__r.Region_Code__c' +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Aircraft__c IN (' + setToString +') AND Agreement__r.Status_Category__c NOT IN ('+ notInString +')' +
            ' AND (CALENDAR_YEAR(Contractual_Delivery_Month__c) =' + year + ' OR CALENDAR_YEAR(TREND_Delivery_Date__c)  = ' + year + ')' +
            //Status__c diferente dos abaixo, não aparecerá no Delivery Forecast PUB - 01/12/2016 - Henrique DC
            ' AND Status__c <> \'Converted\'' +
            ' AND Status__c <> \'Expired\'' +
            ' AND Status__c <> \'Terminated\'' +
            ' ORDER BY df_code__c ASC NULLS LAST';

        //system.debug('QUERY ONLINE>>> ' +querySoql);
        daoList = database.query(querySoql);
        //system.debug('AgreementLineItemDAO getLineItensBySetIdAndYear daoList : ' +daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensBySetIdAndYear(Set<String> notIn){
        string notInString = '';

        for(String includeValue : notIn){
            notInString += '\''+includeValue + '\',';
        }
        notInString = notInString.removeEnd(',');

        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();

        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +  ', Aircraft__r.name, Aircraft__r.DF_Grouping__c, Aircraft__r.DF_Group__c,'+
            'Agreement__r.RecordType.name, Agreement__r.Status_Category__c,' +
            'Agreement__r.Agreement_Color__c, Agreement__r.Region_Code__c' +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE Agreement__r.Status_Category__c NOT IN ('+ notInString +')' +
            ' AND (Agreement__r.Status_Category__c <> \'Proposal Signed\' OR Agreement__r.Related_Proposal__c = NULL)' +
            ' AND ( Agreement__r.RecordType.name <> \'Purchase Agreement\' OR Agreement__r.Enable_Sync__c <> TRUE OR Agreement__r.Related_Proposal__c = NULL )' +
            //Status__c diferente dos abaixo, não aparecerá no Delivery Forecast PUB - 01/12/2016 - Henrique DC
            ' AND Status__c <> \'Converted\'' +
            ' AND Status__c <> \'Expired\'' +
            ' AND Status__c <> \'Terminated\'' +
            ' ORDER BY df_code__c ASC NULLS LAST';

        system.debug('AgreementLineItemDAO getLineItensBySetIdAndYear querySoql : ' +querySoql);
        daoList = database.query(querySoql);
        system.debug('AgreementLineItemDAO getLineItensBySetIdAndYear daoList : ' +daoList);

        return daoList;
    }

    public static List<Agreement_Aircraft__c> getLineItensBySetIdAndAgreementsNotSigned(Set<id> setIds){
        if(setIds.size() == 0)
            return new List<Agreement_Aircraft__c>();

        String setToString = '';
        for(String includeValue : setIds){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        //system.debug('TESTE2 >>> ' + setToString);
        List<Agreement_Aircraft__c> daoList = new List<Agreement_Aircraft__c>();

        String querySoql =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') + ', Agreement__r.Enable_Sync__c' +
            ' FROM Agreement_Aircraft__c' +
            ' WHERE id IN (' + setToString +')' +
            ' AND Agreement__r.Status_Category__c != \'PA Signed\'';

        //system.debug('SELECT VALUE query: '+querySoql);
        daoList = database.query(querySoql);
        //system.debug('DAOLIST' +daoList);

        return daoList;
    }
}