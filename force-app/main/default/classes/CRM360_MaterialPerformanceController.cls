public class CRM360_MaterialPerformanceController {

    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private static Account acc = [SELECT Name, Id FROM Account WHERE Id = :accId];
    
    public Account getAccount(){
        return acc;
    }
}