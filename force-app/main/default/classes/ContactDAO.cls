public without sharing class ContactDAO {
    
    public List<Contact> getContactsByEmail(Set<String> emails){

        return [
            SELECT Id,
                   AccountId,
                   FirstName,
                   LastName,
                   Name,
                   Email
            FROM Contact
            WHERE Email IN :emails
        ];

    }

}