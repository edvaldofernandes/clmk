public class CLMAircraftSOBEventCreation {
    
    private static List<SOB_Event__c> sobEventList = new List<SOB_Event__c>();  
    private static List<SOB_Event__c> newSOBList = new List<SOB_Event__c>();
    private static List<SOB_Event__c> createdSOBList = new List<SOB_Event__c>();
    private static Boolean statusChanged, orderTypeChanged, modelChanged;
    
    private static final String AC_FIRM = 'Firm';
    private static final String AC_OPTION = 'Option';
    private static final String AC_PURCHASE_RIGHT = 'Purchase Right';
    
    private static final String FIRM = 'Firm';
    private static final String OPTION = 'Option';
    private static final String PURCHASE_RIGHT = 'PRA';
    private static final String TO_PLAN = 'Planned';
    private static final String TO_INCLUDE = 'Included';
    private static final String TO_TRANSFER = 'Transferred';
    private static final String TO_DELIVER = 'Delivered';
    private static final String TO_TERMINATE = 'Terminated';
    private static final String TO_EXPIRE = 'Expired';
    private static final String CONVERT_FROM = 'converted from';
    private static final String TO = 'to';

    private static final String UNKNOWN_STATUS = 'Case Not Registered';
    private static final String BLANK_SPACE = ' ';

    public static void updateSOBEvent(Map<Id,Agreement_Aircraft__c> newMap, Map<Id,Agreement_Aircraft__c> oldMap, List<Agreement_Aircraft__c> newList){
        system.debug('NEW MAP '+  newMap.size() + ' | ' + 'OLD MAP '+ oldMap.size() + ' | ' + 'NEW LIST ' + newList.size());
        sobEventList = [SELECT Commercial_Agreement__c, Contract_Aircraft__c, Date_Type__c, Order_Type__c, Event_Date__c, Book_Status__c
                        FROM SOB_Event__c WHERE Contract_Aircraft__r.Id in :newMap.keySet()];
        
        system.debug('EVENTS: ' + sobEventList.size());

        if(sobEventList.size()>0){
            for(SOB_Event__c toUpdateSOB : sobEventList){
                Agreement_Aircraft__c newAircraft = newMap.get(toUpdateSOB.Contract_Aircraft__c);
                Agreement_Aircraft__c oldAircraft = oldMap.get(toUpdateSOB.Contract_Aircraft__c);
                
                if (!newAircraft.Agreement_Signed__c){                   
                    if (!isFinalStatus(newAircraft.Status__c) && !newAircraft.Is_Amend__c){
                        
                        if(hasChanges(newAircraft, oldAircraft)){
                            system.debug('UPDATE TYPE');
                            
                            if(newAircraft.Order_Type__C == AC_FIRM || newAircraft.Order_Type__C == AC_OPTION)
                                toUpdateSOB.Book_Status__c = newAircraft.Order_Type__c + BLANK_SPACE + TO_INCLUDE;
                            else
                                toUpdateSOB.Book_Status__c = PURCHASE_RIGHT + BLANK_SPACE + TO_INCLUDE;
                            
                            toUpdateSOB.Event_Date__c = null;
                    	}
                    }else if(!isFinalStatus(newAircraft.Status__c) && newAircraft.Is_Amend__c){
                        
                        if(hasChanges(newAircraft, oldAircraft)){
                            system.debug('UPDATE SOB FOR AMEND');
                            updateSOB(statusChanged, orderTypeChanged, modelChanged, newAircraft, oldAircraft,toUpdateSOB);
                        }
                    }else if(isFinalStatus(newAircraft.Status__c)){
                        
                        if(hasChanges(newAircraft, oldAircraft)){
                            system.debug('UPDATE STATUS AND DATE');
                            if(newAircraft.Status__c == TO_DELIVER){
                                system.debug('delivered');
                                toUpdateSOB.Event_Date__c = newAircraft.Actual_Delivery_Date__c;
                                toUpdateSOB.Book_Status__c = TO_DELIVER;
                            }else if(newAircraft.Status__c == TO_TERMINATE){
                                system.debug('terminated');
                                toUpdateSOB.Event_Date__c = newAircraft.Termination_Date__c;
                                toUpdateSOB.Book_Status__c = normalizePicklistValue(newAircraft.Order_Type__c + BLANK_SPACE + TO_TERMINATE);
                            }else if(newAircraft.Status__c == TO_EXPIRE){
                                system.debug('expired');
                                toUpdateSOB.Event_Date__c = newAircraft.Expiration_Date__c;
                                toUpdateSOB.Book_Status__c = normalizePicklistValue(newAircraft.Order_Type__c + BLANK_SPACE + TO_EXPIRE);
                            }
                        }
                    }
                    
                    toUpdateSOB.Date_Type__c = newAircraft.Status__c;
                    toUpdateSOB.Order_Type__c = newAircraft.Order_Type__c;
                }else{
                    
                    if(hasChanges(newAircraft, oldAircraft)){
                        system.debug('NEW SOB FOR SIGNED');
                        //List<SOB_Event__c> sobEventFirstList = sobEventList;
                    	SOB_Event__c newSOB = createSOB(statusChanged, orderTypeChanged, modelChanged, newAircraft, oldAircraft, sobEventList);
                    	if(newSOB != null)
                    		newSOBList.add(newSOB);
                    }
                }
            }
            database.update(sobEventList);
            
        }else{
            for(Agreement_Aircraft__c ac : newList){
                Agreement_Aircraft__c newAircraft = newMap.get(ac.Id);
                Agreement_Aircraft__c oldAircraft = oldMap.get(ac.Id);
                system.debug('AMEND: ' + newAircraft.Is_Amend__c);
                if(hasChanges(newAircraft, oldAircraft)){
                    system.debug('NEW SOB FOR AMEND');
                    
                    SOB_Event__c newSOB = createSOB(statusChanged, orderTypeChanged, modelChanged, newAircraft, oldAircraft, sobEventList);
                    if(newSOB != null)
                    	newSOBList.add(newSOB);
                    
                }
            }
        }
        if(!newSOBList.isEmpty()){
            database.upsert(newSOBList);
        }
             
    }

    private static SOB_Event__c createSOB(Boolean statusChanged, 
                                          Boolean orderTypeChanged, 
                                          Boolean modelChanged, 
                                          Agreement_Aircraft__c newAircraft, 
                                          Agreement_Aircraft__c oldAircraft, 
                                          List<SOB_Event__c> sobEventFirstList){
                                    
        SOB_Event__c newSOB = new SOB_Event__c();
        String bookStatus = '';
        system.debug('newAircraft: ' + newAircraft.Status__c);                                    
        if(statusChanged){
            if(newAircraft.Status__c == TO_DELIVER){
                newSOB.Event_Date__c = newAircraft.Actual_Delivery_Date__c;
                bookStatus = TO_DELIVER;
            }else if(newAircraft.Status__c == TO_TERMINATE){
                newSOB.Event_Date__c = newAircraft.Termination_Date__c;
            	bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + TO_TERMINATE;
            }else if(newAircraft.Status__c == TO_EXPIRE){
                newSOB.Event_Date__c = newAircraft.Expiration_Date__c;
           		bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + TO_EXPIRE;
            }
        }else{
            if(orderTypeChanged && !modelChanged){
                if(newAircraft.Is_Amend__c){
                    bookStatus = oldAircraft.Original_Order_Type__c + BLANK_SPACE + TO + BLANK_SPACE + newAircraft.Order_Type__c;
                }else{
                    bookStatus = oldAircraft.Order_Type__c + BLANK_SPACE + TO + BLANK_SPACE + newAircraft.Order_Type__c; 
                }
                
            }else if(modelChanged){       
                if(newAircraft.Is_Amend__c){
                    bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + CONVERT_FROM + BLANK_SPACE + oldAircraft.Original_Aircraft_Model__c + BLANK_SPACE + oldAircraft.Original_Order_Type__c;
                }else{
                    bookStatus = bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + CONVERT_FROM + BLANK_SPACE + oldAircraft.Aircraft_Model__c + BLANK_SPACE + oldAircraft.Order_Type__c;
                }
            }
        }
                                              
        newSOB.Book_Status__c = normalizePicklistValue(bookStatus);
        newSOB.Date_Type__c = newAircraft.Status__c;
        newSOB.Commercial_Agreement__c = newAircraft.Agreement__c;
        newSOB.Contract_Aircraft__c = newAircraft.Id;
        newSOB.Order_Type__c = newAircraft.Order_Type__c;
                                              
        system.debug('newSOB: ' + newSOB);
                                              
        if (!sobExists(newSOB, sobEventFirstList)){
            system.debug('NOT EXIST newSOB: ' + newSOB);
        	return newSOB;
        }else{
            system.debug('EXIST newSOB: ' + newSOB);
            return null;
        }  
    }

    private static SOB_Event__c updateSOB(Boolean statusChanged, 
                                          Boolean orderTypeChanged, 
                                          Boolean modelChanged, 
                                          Agreement_Aircraft__c newAircraft, 
                                          Agreement_Aircraft__c oldAircraft,
                                          SOB_Event__c toUpdateSOB){
                                    
        String bookStatus = '';
        system.debug('newAircraft: ' + newAircraft.Status__c);       
        if(statusChanged){
            if(newAircraft.Status__c == TO_DELIVER){
                toUpdateSOB.Event_Date__c = newAircraft.Actual_Delivery_Date__c;
                bookStatus = TO_DELIVER;
            }else if(newAircraft.Status__c == TO_TERMINATE){
                toUpdateSOB.Event_Date__c = newAircraft.Termination_Date__c;
            	bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + TO_TERMINATE;
            }else if(newAircraft.Status__c == TO_EXPIRE){
                toUpdateSOB.Event_Date__c = newAircraft.Expiration_Date__c;
           		bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + TO_EXPIRE;
            }
        }else{
            if(orderTypeChanged && !modelChanged)
                bookStatus = oldAircraft.Original_Order_Type__c + BLANK_SPACE + TO + BLANK_SPACE + newAircraft.Order_Type__c;
            else if(modelChanged)
                bookStatus = newAircraft.Order_Type__c + BLANK_SPACE + CONVERT_FROM + BLANK_SPACE + oldAircraft.Original_Aircraft_Model__c + BLANK_SPACE + oldAircraft.Original_Order_Type__c;
        }
                                              
        toUpdateSOB.Book_Status__c = normalizePicklistValue(bookStatus);
                                              
        system.debug('toUpdateSOB: ' + toUpdateSOB);
        return toUpdateSOB;
    }

    private static boolean hasChanges (Agreement_Aircraft__c newAircraft, Agreement_Aircraft__c oldAircraft){
        statusChanged = false; orderTypeChanged = false; modelChanged  = false;
        
        if(newAircraft.Status__c != oldAircraft.Status__c) statusChanged = true;
        if(newAircraft.Order_Type__c != oldAircraft.Order_Type__c) orderTypeChanged = true;
        if(newAircraft.Aircraft_Model__c != oldAircraft.Aircraft_Model__c) modelChanged = true;
        
        system.debug('statusChanged || orderTypeChanged || modelChanged' + statusChanged + orderTypeChanged + modelChanged);
        if(statusChanged || orderTypeChanged || modelChanged){
            return true;
        }else{
            return false;
        }
    }

    private static boolean sobExists (SOB_Event__c sobEvent, List<SOB_Event__c> sobEventFirstList){
        //createdSOBList = [SELECT Commercial_Agreement__c, Contract_Aircraft__c, Date_Type__c, Order_Type__c, Event_Date__c, Book_Status__c
        //                  FROM SOB_Event__c WHERE Book_Status__c =: sobEvent.Book_Status__c AND Contract_Aircraft__c =: sobEvent.Contract_Aircraft__c];
        for (SOB_Event__c sob : sobEventFirstList){
            if (sob.Book_Status__c == sobEvent.Book_Status__c && sob.Contract_Aircraft__c == sobEvent.Contract_Aircraft__c) return true;
        }
        return false;  
    }    

    private static String normalizePicklistValue(String picklistValue){
        system.debug('BEFORE ' + picklistValue);
        
        if(picklistValue.contains(AC_PURCHASE_RIGHT))
            picklistValue = picklistValue.replace(AC_PURCHASE_RIGHT, PURCHASE_RIGHT);
        
        List<String> bookStatusPicklist = GEN_Util.getListStringFromPickList(SOB_Event__c.Book_Status__c.getDescribe().getPicklistValues(),false);
        
        if(!bookStatusPicklist.contains(picklistValue)){
            picklistValue = UNKNOWN_STATUS;
        }
        
        system.debug('AFTER ' + picklistValue);
        return picklistValue;
    }

    private static Boolean isFinalStatus(String newStatus){
        if (newStatus == TO_DELIVER || newStatus == TO_EXPIRE || newStatus == TO_TERMINATE){
            return true;
        }else{
            return false;
        }
    }
    
}