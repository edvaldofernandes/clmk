public with sharing class ITRequestController {
 
    //Variable get and set used in PriorizationBoard page
    public List<ITRequest__c> prioritized   {get;set;}
    public List<ITRequest__c> backlogENF    {get;set;} //show the requests NON prioritized type Enhancements or New Features   
    public List<ITRequest__c> backlogTG     {get;set;} //Show the requests NON prioritized type Tasks or Guidances
    public List<ITRequest__c> requests      {get;set;} //Show all open requests
    
    //Variable get and set used in StatusBoard page    
    public List<ITRequest__c> allNew          {get;set;}
    public List<ITRequest__c> allProgress     {get;set;}
    public List<ITRequest__c> allPending      {get;set;}
    public List<ITRequest__c> allOnHold       {get;set;}
    public List<ITRequest__c> allDone         {get;set;}
    public Decimal sumHours                   {get;set;}
    public Map<String, Decimal> sumBacklog    {get;set;}
    public Integer backlogSize                { get { return sumBacklog.size(); } }
    
    //Constructor
    public ITRequestController (ApexPages.StandardController controller) {
    
        //Calling methods
        requestFilter();
        statusFilter();
        prioritizedFilter();
        
    }

    //Method that only shows items with status different than Closed
    public void requestFilter(){
    
    List<ITRequest__c> requests;
    
        requests = [ SELECT RecordTypeIcon__c, RecordType.Name, Description__c, RequestedBy__r.name, RequestID__c,BacklogPriority__c, 
                            DeliveryPeriod__c, Name, Status__c, CreatedDate, CreatedBy.name, ApplicationArea__c, ApplicationArea__r.Name, Comments__c,
                            GUT__c, EstimatedHour__c, ProgressOfCompletion__c, RequestedDate__c, ExpectedResolutionDate__c, 
                            AnalysisDescription__c, AssignedTo__c, RequestedBy__r.SmallPhotoUrl, RequestedBy__r.Id, RequestedBy__r.alias, BusinessValue__c
                       FROM ITRequest__c 
                      WHERE Status__c != 'Done'
                   ORDER BY CreatedDate DESC ];             
        
        backlogENF   = new List<ITRequest__c>();
        backlogTG    = new List<ITRequest__c>();
        
         for (ITRequest__c reqItens : requests) {
                if (reqItens.BacklogPriority__c == null && (reqItens.RecordType.Name == 'New Feature' || reqItens.RecordType.Name == 'Enhancement')) { 
                    backlogENF.add(reqItens);
                } 
                else if (reqItens.BacklogPriority__c == null && (reqItens.RecordType.Name == 'Task' || reqItens.RecordType.Name == 'Guidance'))  {
                        backlogTG.add(reqItens);  
                }
         } 
         
         List<AggregateResult> sumBacklogAR = [SELECT Count(id), RecordType.Name name
                                                FROM ITRequest__c
                                               WHERE BacklogPriority__c = NULL 
                                                 AND Status__c != 'Done'
                                                 AND (RecordType.Name = 'New Feature' OR RecordType.Name = 'Enhancement')
                                            GROUP BY RecordType.name];
                                            
        sumBacklog = new Map<String, Decimal>();
        for(AggregateResult ar : sumBacklogAR) {
            
            //sumBacklog.put("New Feature", 3);
            system.debug('Aggregate Result....++++++++++++++++++++++++++++++++++++' + ar);
            sumBacklog.put((String) ar.get('name'), (Decimal) ar.get('expr0'));
        }
        //sumBacklog = (Decimal) sumBacklogAR[0].get('expr0');                                   
                                                 
    }

   //Method that get list of requests that have backlog priority and are set to delivery this or next week   
     public void prioritizedFilter() {
        
      prioritized = [SELECT RecordTypeIcon__c, RecordType.Name, Description__c, RequestedBy__r.name, RequestedBy__r.alias, RequestID__c, BacklogPriority__c,
                            DeliveryPeriod__c, Name, Status__c, CreatedDate, CreatedBy.name, ApplicationArea__c, ApplicationArea__r.Name, ApplicationArea__r.ApplicationtKey__c, 
                            GUT__c, EstimatedHour__c, ProgressOfCompletion__c, RequestedDate__c, ExpectedResolutionDate__c, Comments__c,
                            AnalysisDescription__c, AssignedTo__c, RequestedBy__r.SmallPhotoUrl, RequestedBy__r.Id, RequestedBy__c, DaysOpen__c,
                            AssignedTo__r.SmallPhotoUrl, AssignedTo__r.alias, DeliveryDate__c, RemainingDays__c
                       FROM ITRequest__c
                      WHERE (BacklogPriority__c != NULL AND ResolutionStatus__c = NULL) OR (BacklogPriority__c = NULL and RecordType.Name = 'Bug' AND Status__c NOT IN ('Done', 'Cancelled'))
                   ORDER BY BacklogPriority__c ASC NULLS First];        
                   
         List<AggregateResult> sumHoursAR =  [SELECT Sum(EstimatedHour__c)
                                                FROM ITRequest__c
                                               WHERE (BacklogPriority__c != NULL AND ResolutionStatus__c = NULL) 
                                                  OR (BacklogPriority__c = NULL  AND RecordType.Name = 'Bug'    AND Status__c NOT IN ('Done', 'Cancelled'))];
                              
                                         
         sumHours = (Decimal) sumHoursAR[0].get('expr0');
     }
     
   //Methdod 
    
    public void statusFilter(){
    
        List<ITRequest__c> allStatuses;
    
        allStatuses = [SELECT RecordTypeIcon__c, RecordType.Name, RequestID__c,DeliveryPeriod__c, Name, Status__c, CreatedDate, 
                              CreatedBy.name, ApplicationArea__c, GUT__c 
                         FROM ITRequest__c];
   
        allNew       = new List<ITRequest__c>();
        allProgress  = new List<ITRequest__c>();
        allPending   = new List<ITRequest__c>();
        allOnHold    = new List<ITRequest__c>();
        allDone      = new List<ITRequest__c>();
    
        for (ITRequest__c reqStatus : allStatuses) {
                if (reqStatus.Status__c == 'New'){
                  allNew.add(reqStatus);
                  
                }else if (reqStatus.Status__c == 'In Progress'){
                  allProgress.add(reqStatus);
                  
                }else if (reqStatus.Status__c == 'Pending'){
                  allPending.add(reqStatus);
                  
                }else if (reqStatus.Status__c == 'On Hold'){
                  allOnHold.add(reqStatus);
                  
                }else if (reqStatus.Status__c == 'Done'){
                  allDone.add(reqStatus);
                }
        }                                  
    }
    
    //This "For" is for trace log purposes
    public pageReference save(){
        update backlogENF;
        update backlogTG;
        update prioritized;
        return ApexPages.currentPage();
        }
}