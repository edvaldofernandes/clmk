/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class QuoteCopyLineItemFromOpp
*
* NAME: QuoteCopyLineItemFromOppTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*
*******************************************************************************/
@isTest
private class QuoteCopyLineItemFromOppTest {
	
	private static final Integer LOTE = 1;

    static testMethod void myUnitTest() {
    
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
      
      Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
      Database.insert(pbe);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      Database.insert(opp);
      
      OpportunityLineItem oli = SObjectInstanceTest.createOppItem(opp.Id, pbe.Id);
      oli.Region__c = 'Africa';
      oli.Hotel__c = true;
      Database.insert(oli);
      
      oli = [SELECT Quantity, UnitPrice, Region__c, Hotel__c  FROM OpportunityLineItem WHERE Id =: oli.Id];
      
      Quote cotacao = SObjectInstanceTest.createQuote(opp.Id);
      cotacao.Pricebook2Id = stdPB;
      cotacao.Status = 'Presented to Customer';
      Database.insert(cotacao);
      
      QuoteLineItem qli = SObjectInstanceTest.createQuoteLineItem(cotacao.Id, pbe.Id);
      qli.Quantity = oli.Quantity;
      qli.UnitPrice = oli.UnitPrice;
      
      Test.startTest();
      Database.insert(qli);
      Test.stopTest();
      
      qli = [SELECT Region__c, Hotel__c FROM QuoteLineItem WHERE Id =: qli.Id];
      system.assertEquals(oli.Region__c, qli.Region__c);
      system.assertEquals(oli.Hotel__c, qli.Hotel__c);
    }
    
    static testMethod void lote() {
    
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
      
      Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
      Database.insert(pbe);
      
      List<Opportunity> lstOpp = new List<Opportunity>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        lstOpp.add(opp);
      }
      Database.insert(lstOpp);
      
      
      List<OpportunityLineItem> lstOli = new List<OpportunityLineItem>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        OpportunityLineItem oli = SObjectInstanceTest.createOppItem(lstOpp.get(i).Id, pbe.Id);
	      oli.Region__c = 'Africa';
	      oli.Hotel__c = true;
	      lstOli.add(oli);
      }
      Database.insert(lstOli);
      
      lstOli = [SELECT Quantity, UnitPrice, Region__c, Hotel__c  FROM OpportunityLineItem WHERE Id =: lstOli];
      
      List<Quote> lstQuo = new List<Quote>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        Quote cotacao = SObjectInstanceTest.createQuote(lstOpp.get(i).Id);
        cotacao.Pricebook2Id = stdPB;
        cotacao.Status = 'Presented to Customer';
        lstQuo.add(cotacao);
      }
      Database.insert(lstQuo);
      
      List<QuoteLineItem> lstqli = new List<QuoteLineItem>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        QuoteLineItem qli = SObjectInstanceTest.createQuoteLineItem(lstQuo.get(i).Id, pbe.Id);
	      qli.Quantity = lstOli.get(i).Quantity;
	      qli.UnitPrice = lstOli.get(i).UnitPrice;
      }
      
      Test.startTest();
      Database.insert(lstqli);
      Test.stopTest();
      
      Integer i = 0;
      for ( QuoteLineItem qli : [SELECT Region__c, Hotel__c FROM QuoteLineItem WHERE Id =: lstqli] )
      {
        system.assertEquals(lstOli.get(i).Region__c, qli.Region__c);
        system.assertEquals(lstOli.get(i).Hotel__c, qli.Hotel__c);
        i++;
      }
    }
}