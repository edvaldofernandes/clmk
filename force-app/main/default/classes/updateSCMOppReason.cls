/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; Mar-2017.
**/
global class updateSCMOppReason implements Database.Batchable<sObject> 
{

       
    global database.querylocator start(Database.BatchableContext BC) 
    {
        string query = '';
        query = 'SELECT Contract_Line_Item_Master__r.Opp_reason__c,Opp_Reason__c FROM Service_Contract_Management__c Where Contract_Line_Item_Master__r.Opp_reason__c != Null' ;
        
        return Database.getQueryLocator(query);
    }

    global void finish(Database.BatchableContext BC) 
    {

    }


    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        list<Service_Contract_Management__c> serviceContractManagement = new List<Service_Contract_Management__c>();
        
        for(sObject tempObject : scope)
        {
            Service_Contract_Management__c scm = (Service_Contract_Management__c)tempObject;
            scm.Opp_Reason__c = scm.Contract_Line_Item_Master__r.Opp_reason__c;
            serviceContractManagement.add(scm);
        }    
        
        if(!serviceContractManagement.isEmpty())
            update serviceContractManagement;
        
    }
    
    
    
    
}