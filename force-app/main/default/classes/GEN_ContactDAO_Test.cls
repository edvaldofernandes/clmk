/**
* @author Marcilio Leite de Souza
* @date 30/05/2018
* @description: TEST CLASS GEN_ContactDAO
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           30 MAY 2018             Original Version
**/
@isTest
private class GEN_ContactDAO_Test {
	
    @isTest
    static void getCommunityContact_it_should_return_contact() {
        create_contact_test_data();      
        
        Contact c = GEN_ContactDAO.getCommunityContact('test@test.com.br', 'testFlyEmbraerId');
        
        Test.startTest();
        System.assertNotEquals(c , null);
        Test.stopTest();
        
    }
    
    @isTest
    static void getCommunityContact_it_should_not_return_contact() {
        create_contact_test_data();      
        
        Contact c = GEN_ContactDAO.getCommunityContact('email', 'FlyEmbraerUserId__c');
        
        Test.startTest();
        System.assertEquals(c , null);
        Test.stopTest();
        
    }
    
    static Contact create_contact_test_data(){
        Contact c = new Contact();
        c.Title = 'testTitle';
        c.LastName = 'testLastName';
        c.Gender__c = 'Male';
        c.Contact_Status__c ='Active';
        c.Email = 'test@test.com.br';
        c.FlyEmbraerUserId__c = 'testFlyEmbraerId';
        c.AccountId = create_account_test_data().Id;
        
        insert c;
        return c;
    }
    
     static Account create_account_test_data(){
      Account a = new Account();
      a.BillingCountry = 'Brazil';
      a.Name = 'Test';
      a.Company_Nickname__c = 'testNickname';
      a.FlyEmbraerId__c = '99999';
	  insert a;
      return a;
    }
    
}