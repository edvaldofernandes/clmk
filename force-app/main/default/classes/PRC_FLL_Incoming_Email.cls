public class PRC_FLL_Incoming_Email {
    
    public class UserWrapper{
        public User who {get;set;}
        public List<Case> greenList {get;set;}
        public List<Case> yellowList {get;set;}
        public List<Case> redList {get;set;}
        
        public UserWrapper(User u, List<List<Case>> list2D){
            this.who = u;
            this.greenList = list2d[0];
            this.yellowList = list2d[1];
            this.redList = list2d[2];
        }
    }    
    public List<Case> cases;
       
    public Integer total {get; set;}
    
    public Integer cases_green {get; set;}
    public Integer cases_yellow {get; set;}
    public Integer cases_red {get; set;}
    
    public Integer aognfoTotal {get; set;}
    public Integer aognfoGreen {get; set;}
    public Integer aognfoYellow {get; set;}
    public Integer aognfoRed {get; set;}
    public Datetime aognfoMessage {get; set;}
    public Decimal aognfoElapsed {get; set;}
    public String aognfoCaseNumber {get;set;}
    
    public Integer aogTotal {get; set;}
    public Integer aogGreen {get; set;}
    public Integer aogYellow {get; set;}
    public Integer aogRed {get; set;}
    public Datetime aogMessage {get; set;}
    public Decimal aogElapsed {get; set;}
    public string aogCaseNumber {get;set;}
    
    public Integer crtTotal {get; set;}
    public Integer crtGreen {get; set;}
    public Integer crtYellow {get; set;}
    public Integer crtRed {get; set;}
    public Datetime crtMessage {get; set;}
    public Decimal crtElapsed {get; set;}
    public string crtCaseNumber {get;set;}
    
    public Integer rtnTotal {get; set;}
    public Integer rtnGreen {get; set;}
    public Integer rtnYellow {get; set;}
    public Integer rtnRed {get; set;}
    public Datetime rtnMessage {get; set;}
    public Decimal rtnElapsed {get; set;}
    public string rtnCaseNumber {get;set;}
    
    public Decimal avgIncoming {get; set;}
    public Decimal avgIncomingGreen {get; set;}
    public Decimal avgIncomingYellow {get; set;}
    public Decimal avgIncomingRed {get; set;}
    
    //cases list per agent
    private List<List<Case>> Adolfo2DList {get;set;}
    private List<List<Case>> Agustin2DList {get;set;}
    private List<List<Case>> Sam2DList {get;set;}
    private List<List<Case>> Marta2DList {get;set;}
    private List<List<Case>> Robert2DList {get;set;}
    private List<List<Case>> Armando2DList {get;set;}
    private List<List<Case>> Maria2DList {get;set;}
    private List<List<Case>> Keissy2DList {get;set;}
    private List<List<Case>> Paula2DList {get;set;}
    private List<List<Case>> Adam2DList {get;set;}
    private List<List<Case>> Unclassified2DList {get;set;}
    
    private List<String> ScottsAccounts {get;set;}
    
    public Boolean displayPopUp {get;set;}
    public List<Case> selectedGreenCases {get;set;}
    public List<Case> selectedYellowCases {get;set;}
    public List<Case> selectedRedCases {get;set;}
    public String selectedAgentName {get;set;}
    public Map<String, UserWrapper> UserWrapperMap{get;set;}
    
    public List<UserWrapper> userWrapperList {get;set;}
        
    public PRC_FLL_Incoming_Email(){
    
        //Query to get the average of all Incoming Emails
        
        List<AggregateResult> resultsAvg = [SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                            FROM Case 
                                            WHERE 
                                            PRC_FLL__c = true
                                            AND Incoming_email_i__c = true
                                            AND Oldest_Case_Message_Date__c != NULL
                                            AND PRC_Support_Plant__c!='SJK'
                                            GROUP BY PRC_FLL__c];
        
        if(resultsAvg.size() > 0 ){   
            avgIncoming = ((decimal)resultsAvg[0].get('avgDec'));
            avgIncoming = avgIncoming.setScale(2);
        }
        
        //Query to get the average of Incoming Email with Status Green
        
        List<AggregateResult> resultsAvgGreen = [SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                                 FROM Case 
                                                 WHERE 
                                                 PRC_FLL__c = true
                                                 AND Incoming_email_i__c = true 
                                                 AND Incoming_Email_Status__c = 'Green'
                                                 AND Oldest_Case_Message_Date__c != NULL
                                                 AND PRC_Support_Plant__c!='SJK'
                                                 GROUP BY PRC_FLL__c];
              
        if(resultsAvgGreen.size() > 0 ){   
            avgIncomingGreen = ((decimal)resultsAvgGreen[0].get('avgDec'));
            avgIncomingGreen = avgIncomingGreen.setScale(2);
        }
        
        //Query to get the average of Incoming Email with Status Yellow
        
        List<AggregateResult> resultsAvgYellow = [SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                                  FROM Case 
                                                  WHERE 
                                                  PRC_FLL__c = true
                                                  AND Incoming_email_i__c = true 
                                                  AND Incoming_Email_Status__c = 'Yellow'
                                                  AND Oldest_Case_Message_Date__c != NULL
                                                  AND PRC_Support_Plant__c!='SJK'
                                                  GROUP BY PRC_FLL__c];       
     
        if(resultsAvgYellow.size() > 0 ){   
            avgIncomingYellow = ((decimal)resultsAvgYellow[0].get('avgDec'));
            avgIncomingYellow = avgIncomingYellow.setScale(2);
        }
        
        //Query to get the average of Incoming Email with Status Red
        
        List<AggregateResult> resultsAvgRed = [SELECT AVG(Elapsed_Time_Oldest_Message__c) avgDec
                                               FROM Case 
                                               WHERE 
                                               PRC_FLL__c = true
                                               AND Incoming_email_i__c = true 
                                               AND Incoming_Email_Status__c = 'Red'
                                               AND PRC_Support_Plant__c!='SJK'
                                               GROUP BY PRC_FLL__c];
       
        if(resultsAvgRed.size() > 0 ){ 
            avgIncomingRed = ((decimal)resultsAvgRed[0].get('avgDec'));
            avgIncomingRed = avgIncomingRed.setScale(2);
        }
        //Initialize the variables in zero 
        total = 0;
       
        cases_green = 0;
        cases_yellow = 0;
        cases_red = 0;
    
        aognfoTotal = 0;
        aognfoGreen = 0;
        aognfoYellow = 0;
        aognfoRed = 0;
        
        aogTotal = 0;
        aogGreen = 0;
        aogYellow = 0;
        aogRed = 0;
        
        crtTotal = 0;
        crtGreen = 0;
        crtYellow = 0;
        crtRed = 0;
        
        rtnTotal = 0;
        rtnGreen = 0;
        rtnYellow = 0;
        rtnRed = 0;
        
        //2D list will hold 3 lists of cases 
        //index 0: list of green cases, index 1: is a list of yellow cases, index 2: list of red cases     
        Adolfo2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Agustin2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Sam2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Marta2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Robert2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Armando2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Maria2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Keissy2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Paula2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Adam2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()};
        Unclassified2DList = new List<List<case>>{new List<Case>(), new List<Case>(), new List<Case>()}; 
            
        //define list of scott's accounts
        //ScottsAccounts = new List<String>{'EMBRAER NETHERLANDS B.V.', 'Aeromexico', 'AEROMEXICO CONNECT', 'Compass Airlines', 
        //    'Horizon Air', 'Nordic Aviation Capital A/S', 'Republic Airline Inc.'};
        
        selectedGreenCases = new List<Case>();
        selectedYellowCases = new List<Case>();
        selectedRedCases = new List<Case>();
        userWrapperMap = new Map<String, UserWrapper>();
        displayPopUp = false;
        
        cases = getIncomingCases();			//run query to get cases
        AnalyseCases();						//run the analysis 
        userWrapperList = getPRCUsers();	//get the users for incoming email by user
        System.debug('Uncounted Cases: \nGreen: ' + Unclassified2DList[0] + '\nYellow: ' + Unclassified2DList[1] + '\nRed: ' + Unclassified2DList[2]); //log uncounted cases for debugging
    }
    
    // Query to get all Cases with Incoming Email
    public List<Case> getIncomingCases(){
        return [SELECT RecordType.ID, RecordType.Name, Account.Name, PRC_Support_Plant__c, CaseNumber, Incoming_Email_Status__c, Priority, Oldest_Case_Message_Date__c, Elapsed_Time_Oldest_Message__c 
                FROM Case
                WHERE
                PRC_FLL__c = true
                AND Incoming_email_i__c = true
                AND PRC_Support_Plant__c!='SJK'
                ORDER BY Oldest_Case_Message_Date__c DESC ];
    }
    
    //gets the PRC Users
    //creates a wrapper class for each user
    //sets the count on the wrapper class
    public List<UserWrapper> getPRCUsers(){
        List<String> namesList = new List<String> {'Adolfo Felix', 'Agustin Diaz', 'Samuel Lopez', 'Marta Mendez', 'Robert Faller', 'Armando Diaz', 'Maria Martinez',
            'Keissy Yzaguirre', 'Paula Cardona', 'Adam Stancin'};
		List<UserWrapper> wrapperList = new List<UserWrapper>();
        for(User u : [SELECT Id, Name, FirstName, FullPhotoUrl FROM User WHERE Name IN :namesList AND IsActive=true AND (Profile.Name='PRC' OR Profile.Name='PRC Plus')]){
            UserWrapper uWrapper;
            if(u.Name=='Adolfo Felix'){
                uWrapper = new UserWrapper(u, Adolfo2DList);
            }else if(u.Name=='Agustin Diaz'){
                uWrapper = new UserWrapper(u, Agustin2DList);
            }else if(u.Name=='Samuel Lopez'){
                uWrapper = new UserWrapper(u, Sam2DList);
            }else if(u.Name=='Marta Mendez'){
                uWrapper = new UserWrapper(u, Marta2DList);
            }else if(u.Name=='Robert Faller'){
                uWrapper = new UserWrapper(u, Robert2DList);
            }else if(u.Name=='Armando Diaz'){
                uWrapper = new UserWrapper(u, Armando2DList);
            }else if(u.Name=='Maria Martinez'){
                uWrapper = new UserWrapper(u, Maria2DList);
            }else if(u.name=='Keissy Yzaguirre'){
                uWrapper = new UserWrapper(u, Keissy2DList);
            }else if(u.name=='Paula Cardona'){
                uWrapper = new UserWrapper(u, Paula2DList);
            }else if(u.name=='Adam Stancin'){
                uWrapper = new UserWrapper(u, Adam2DList);
            }else
                System.debug('Something is wrong. The users name does not match any PRC names');
            userWrapperMap.put(uWrapper.who.FirstName, uWrapper);
        }
        return userWrapperMap.values();	//return the list of user wrapper objects
    }
    
    //Analyse the data
    public void AnalyseCases(){
        for(Case caseIncoming : cases){
            total++;
            if(caseIncoming.Incoming_Email_Status__c == 'Green'){
                cases_green++;
            }
            if (caseIncoming.Incoming_Email_Status__c == 'Yellow'){
                 cases_yellow++;
            }
            if (caseIncoming.Incoming_Email_Status__c == 'Red'){    
                 cases_red++;
            }
            if(caseIncoming.Priority == 'AOG NFO'){
                aognfoTotal++;
                
                aognfoMessage = caseIncoming.Oldest_Case_Message_Date__c;
                aognfoElapsed = caseIncoming.Elapsed_Time_Oldest_Message__c;
                aognfoCaseNumber = caseIncoming.CaseNumber;
                                   
                if(caseIncoming.Incoming_Email_Status__c == 'Green'){
                    aognfoGreen++;
                }
                else if(caseIncoming.Incoming_Email_Status__c == 'Yellow'){
                    aognfoYellow++;
                }
                else aognfoRed++;             
            }
            if(caseIncoming.Priority == 'AOG'){
                aogTotal++;
                
                aogMessage = caseIncoming.Oldest_Case_Message_Date__c;
                aogElapsed = caseIncoming.Elapsed_Time_Oldest_Message__c;
                aogCaseNumber = caseIncoming.CaseNumber;
                          
                if(caseIncoming.Incoming_Email_Status__c == 'Green'){
                    aogGreen++; 
                }
                else if(caseIncoming.Incoming_Email_Status__c == 'Yellow'){
                    aogYellow++; 
                }
                else aogRed++;
            }
            if(caseIncoming.Priority == 'Critical'){
                crtTotal++;
                
                crtMessage = caseIncoming.Oldest_Case_Message_Date__c;
                crtElapsed = caseIncoming.Elapsed_Time_Oldest_Message__c;
                crtCaseNumber = caseIncoming.CaseNumber;
                               
                if(caseIncoming.Incoming_Email_Status__c == 'Green'){
                    crtGreen++;
                }
                else if(caseIncoming.Incoming_Email_Status__c == 'Yellow'){
                    crtYellow++;
                }
                else crtRed++;
            }
            if(caseIncoming.Priority == 'Routine' || caseIncoming.Priority == 'OSS'){
                rtnTotal++;
                rtnMessage = caseIncoming.Oldest_Case_Message_Date__c;
                rtnElapsed = caseIncoming.Elapsed_Time_Oldest_Message__c;  
                rtnCaseNumber = caseIncoming.CaseNumber;
                               
                if(caseIncoming.Incoming_Email_Status__c == 'Green'){
                    rtnGreen++;
                }
                else if(caseIncoming.Incoming_Email_Status__c == 'Yellow'){
                    rtnYellow++;
                }
                else rtnRed++;
            }
            //count cases with incoming email for each FLL agent
            //criteria specified by Bryan Johnson
            if(caseIncoming.RecordType.Name=='Pool Exchanges'){														//check record type = Pool Exchanges
                if(caseIncoming.PRC_Support_Plant__c=='FLL'){														//check support plant = FLL
                    if(caseIncoming.Account.Name!='Azul Linhas Aéreas')
                        evaluateEmailStatusPerUser2D(Adolfo2DList, caseIncoming);									//Adolfo: all accounts except Compass, Copa and Azul
                    else
                        evaluateEmailStatusPerUser2D(Robert2DList, caseIncoming);									//Robert: Azul Pool Exchange
                }else if(caseIncoming.PRC_Support_Plant__c=='LBG'){													//check support plant = LBG
                    evaluateEmailStatusPerUser2D(Sam2DList, caseIncoming);											//Sam and Paula: Pool LBG
                	evaluateEmailStatusPerUser2D(Paula2DList, caseIncoming);										
                }else
                    evaluateEmailStatusPerUser2D(Unclassified2DList, caseIncoming);
            }else if(caseIncoming.RecordType.Name=='Repair Administ.' || caseIncoming.RecordType.Name=='Warranty'){	//check record type = Repair Admin or Warranty
                if(caseIncoming.PRC_Support_Plant__c=='FLL'){														//check support plant = FLL
                    if(caseIncoming.Account.Name=='Azul Linhas Aéreas'){
                        if(caseIncoming.RecordType.Name=='Repair Administ.')
                            evaluateEmailStatusPerUser2D(Robert2DList, caseIncoming);								//Robert: Azul Repair Admin
                        else
                            evaluateEmailStatusPerUser2D(Unclassified2DList, caseIncoming);
                    }else if(caseIncoming.Account.Name=='Copa Holding - Compañia Panameña de Aviación' || caseIncoming.Account.Name=='Austral Lineas Aereas - Cielos del Sur S.A.')
                        evaluateEmailStatusPerUser2D(Armando2DList, caseIncoming);									//Armando: Copa or Austral
                    else if(caseIncoming.Account.Name=='Envoy Air Inc.' || caseIncoming.Account.Name.left(17)=='American Airlines'){
                        evaluateEmailStatusPerUser2D(Keissy2DList, caseIncoming);									//Keissy and Adam: Envoy or American
                        evaluateEmailStatusPerUser2D(Adam2DList, caseIncoming);
                    }else if(caseIncoming.Account.Name=='SkyWest Airlines'){
                        evaluateEmailStatusPerUser2D(Agustin2DList, caseIncoming);									//Agustin: Skywest
                    }else
                        evaluateEmailStatusPerUser2D(Maria2DList, caseIncoming);									//Maria: all other accounts
                }else if(caseIncoming.PRC_Support_Plant__c=='LBG')													//check support plant = LBG
                    evaluateEmailStatusPerUser2D(Marta2DList, caseIncoming);										//Marta: all PRC_Support_Plant = LBG
                else
                    evaluateEmailStatusPerUser2D(Unclassified2DList, caseIncoming);
            }else
                evaluateEmailStatusPerUser2D(Unclassified2DList, caseIncoming);
        }
    }
    
    //takes a 2D list and a case to evaluate
    //the outer list holds lists of cases, where index 0 is the list of green cases, index 1 is the list of yellow cases, index 2 is the list of red cases
    private void evaluateEmailStatusPerUser2D(List<List<Case>> List2D, Case caseToEvaluate){
        if(caseToEvaluate.Incoming_Email_Status__c == 'Green'){
            List2D[0].add(caseToEvaluate); //add the case to the green list
        }else if(caseToEvaluate.Incoming_Email_Status__c == 'Yellow'){
            List2D[1].add(caseToEvaluate); //add the case to the yellow list
        }else if(caseToEvaluate.Incoming_Email_Status__c == 'Red'){
            List2D[2].add(caseToEvaluate); //add the case to the red list
        }
    }
    
    public void showPopUp(){
        displayPopUp = true;
        UserWrapper uw = userWrapperMap.get(selectedAgentName);
       	selectedGreenCases = uw.greenList;
        selectedYellowCases = uw.yellowList;
        selectedRedCases = uw.redList;
    }
    
    public void hidePopUp(){
        displayPopUp = false;
    }
}