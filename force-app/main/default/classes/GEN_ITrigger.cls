/**
* @author Marcilio Leite de Souza
* @date 20/08/2018
* @description: Interface to implement trigger design patterns
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           20 AGO 2018             Original Version
**/
public interface GEN_ITrigger {

    /**
    * @description : This method is called prior to execution of a BEFORE trigger. Use this to cache
	*              any data required into maps prior execution of the trigger.
    **/
	void bulkBefore();
	
	/**
	 * @description : This method is called prior to execution of an AFTER trigger. Use this to cache
	 *              any data required into maps prior execution of the trigger.
	 */
	void bulkAfter();

	/**
	 * @description : This method is called once all records have been processed by the trigger. Use this 
	 *                method to accomplish any final operations such as creation or updates of other records.
	 */
	void andFinally();
}