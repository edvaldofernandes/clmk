@isTest
public class regForm_ControllerTest {
    
    @isTest static void testRegAnother(){
        regForm_Controller rfc = new regForm_Controller();
        rfc.regAnother();
        System.assertEquals('incomplete',rfc.regStatus);
    }
    
    @isTest static void testFalseValidateCampaign(){
        regForm_Controller rfc = new regForm_Controller();
        rfc.campaignId = '111';
        rfc.validateCampaign();
        System.assertEquals('false', rfc.validCampaign);
    }
    
    @isTest static void testTrueValidateCampaign(){
        regForm_Controller rfc = new regForm_Controller();
        RecordType[] RecType = [Select Id From RecordType Where id = '012i0000000tuKd'];
        
        Campaign camp = new Campaign(Name = 'c', CurrencyIsoCode = 'USD', Registration_Form_End_Date__c = system.today()+2,
                                     Registration_Form_Start_Date__c = system.today()-2, RecordType = RecType[0]);
        try {
            insert camp;
        } catch(DmlException e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
        
        rfc.campaignId = camp.id;
        rfc.validateCampaign();
        System.assertEquals('true', rfc.validCampaign);
    }
    
    
    @isTest static void testSearchForm()
    {

        Campaign camp = new Campaign(Name = 'c', CurrencyIsoCode = 'USD', Registration_Form_End_Date__c = system.today()+2,
                                     Registration_Form_Start_Date__c = system.today()-2, RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Training slots').getRecordTypeId());
        try 
        {
            insert camp;
        } 
        catch(DmlException e) 
        {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
        
        Registration_Form__c regForm = new Registration_Form__c();
        regForm.Date_of_Birth__c = '01/01/' + string.ValueOf(date.Today().year()); 
        regForm.Campaign__c = camp.Id;  
        regForm.City_of_Birth__c = 'São Paulo'; 
        regForm.Company__c = 'Test Company';
        regForm.Country_of_Birth__c = 'Brazil'; 
        regForm.Family_Name__c = 'Test'; 
        regForm.First_Name__c = 'Unit'; 
        insert regForm;
        
        PageReference pageRef = Page.Regform_page;
            
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('id',regform.Id);
        
        regForm_Controller rfc = new regForm_Controller();     
        
        rfc.searchForm();
    }
    
    @isTest static void testSaveOK(){
        regForm_Controller rfc = new regForm_Controller();
        RecordType[] RecType = [Select Id From RecordType Where id = '012i0000000tuKd'];
        Campaign camp = new Campaign(Name = 'c', CurrencyIsoCode = 'USD', Registration_Form_End_Date__c = system.today()+2,
                                     Registration_Form_Start_Date__c = system.today()-2, RecordType = RecType[0]);
        try {
            insert camp;
        } catch(DmlException e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
        
        rfc.campaignId = camp.id;
        rfc.validateCampaign();
        
        rfc.campaign = camp;
        rfc.regForm = new Registration_Form__c(First_Name__c = 'firstName', Family_Name__c = 'familyName', Date_of_Birth__c = '01/01/2000', 
                                               City_of_Birth__c = 'city', Country_of_Birth__c = 'Brazil', Company__c = 'company');
        rfc.save();
        System.assertEquals('c - firstName familyName', rfc.regForm.Name);
        
    }
    
    @isTest static void testSaveNOK(){
        try{
        regForm_Controller rfc = new regForm_Controller();
        RecordType[] RecType = [Select Id From RecordType Where Id = '012i0000000tuKd'];
        Campaign camp = new Campaign(Name = 'c', CurrencyIsoCode = 'USD', Registration_Form_End_Date__c = system.today()+2,
                                     Registration_Form_Start_Date__c = system.today()-2, RecordType = RecType[0]);
        try {
            insert camp;
        } catch(DmlException e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
        
        rfc.campaignId = camp.id;
        rfc.validateCampaign();
        
        rfc.campaign = camp;
        rfc.regForm = new Registration_Form__c(First_Name__c = 'firstName', Family_Name__c = 'familyName', Date_of_Birth__c = '01/01/1000', 
                                               City_of_Birth__c = 'city', Country_of_Birth__c = 'Brazil', Company__c = 'company');
        
        boolean saved = false;
        
        rfc.save();
        } catch(Exception e){
        Boolean expectedExceptionThrown =  e.getMessage().contains('Failed to Save Form') ? true : false;
        System.AssertEquals(expectedExceptionThrown, true);
        } 
    }
}