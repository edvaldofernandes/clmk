global class RCP {
    
    webservice List<RCP_fhfc> rCP_fhfc;
    webservice List<RCP_HistAeronave> rCP_HistAeronave;
    webservice List<RCP_Mtbur> rcp_Mtbur;
    webservice List<RCP_Problem> rcp_Problem;
    webservice List<RCP_Remocoes> rcp_Remocoes;
    
    public RCP(){
        
        rCP_fhfc = new List<RCP_fhfc>();
        rCP_HistAeronave = new List<RCP_HistAeronave>();
        rcp_Mtbur = new List<RCP_Mtbur>();
        rcp_Problem = new List<RCP_Problem>();
        rcp_Remocoes = new List<RCP_Remocoes>();
    }
    
    public RCP(List<RCP_fhfc> rCP_fhfc, List<RCP_HistAeronave> rCP_HistAeronave, List<RCP_Mtbur> rcp_Mtbur, 
               List<RCP_Problem> rcp_Problem, List<RCP_Remocoes> rcp_Remocoes){
        
        this.rCP_fhfc = rCP_fhfc;
        this.rCP_HistAeronave = rCP_HistAeronave;
        this.rcp_Mtbur = rcp_Mtbur;
        this.rcp_Problem = rcp_Problem;
        this.rcp_Remocoes = rcp_Remocoes;
    }
}