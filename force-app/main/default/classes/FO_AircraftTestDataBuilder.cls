@isTest
public class FO_AircraftTestDataBuilder {
    private String aircraftStatus = 'In Service';
    private String commercialName = 'EMBRAER 195 STD';
    private String serialNumber = '00000000';
    public FO_AircraftTestDataBuilder withStatus(String status){
        this.aircraftStatus = status;
        return this;
    }  
    public FO_AircraftTestDataBuilder withCommercialName(String commercialName){
        this.commercialName = commercialName;
        return this;
    }  
    public FO_AircraftTestDataBuilder withSerialNumber(String serialNumber){
        this.serialNumber = serialNumber;
        return this;
    }  
    public Aircraft__c build(){
        Aircraft__c aircraft = new Aircraft__c(
            Name = serialNumber,
            Commercial_Name__c = commercialName,
            Aircraft_Status__c = aircraftStatus
        );
        insert aircraft;
        return aircraft;
    }
}