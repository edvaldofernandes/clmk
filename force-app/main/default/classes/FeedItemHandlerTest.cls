@isTest
private class FeedItemHandlerTest {

        static FeedItem loadData() {
        	FeedItem item = new FeedItem();
			item.Body = '<soapenv:Envelope xmlns:com="http://www.example.org/compass/"></soapenv:Envelope>';
        
        	return item;
    	}

    	static FeedItem loadDataWithAttach(){
        
        List<FeedItem> itemToClone = [SELECT Id, Body, ParentId, RelatedRecordId, HasContent 
                                FROM FeedItem 
                                WHERE HasContent = true
                               	AND ParentId = :[SELECT Id FROM CollaborationGroup WHERE Name like '%QMI - %']
                               LIMIT 1];
        
        FeedItem item = loadData();
        item.ParentId = itemToClone.get(0).parentId;
        item.RelatedRecordId = itemToClone.get(0).RelatedRecordId;
        
        return item;
    }
    
    @isTest(seeAllData = true)
    static void testCallWS() {
        
        Test.startTest();
        
        Transaction_Delivery__c td = [ SELECT Name, URL__c, Authorization__c
										FROM Transaction_Delivery__c
										WHERE Name = 'CPSInformation'
										LIMIT 30];

        String body = '<soapenv:Envelope xmlns:com="http://www.example.org/compass/"></soapenv:Envelope>';
        
        Test.setMock( HttpCalloutMock.class , new CPSMockServer() );
        FeedItemHandler.callWS( td.URL__c , td.Authorization__c , body);
        
        Test.stopTest();
    }
    
 
    
    @isTest(seeAllData = true)
    static void buildBody2( ) {

        FeedItem item = loadDataWithAttach();
        FeedItemHandler.buildBody(item);

    }
    
   	@isTest(seeAllData = true)
    static void testBody() {
        
        Set<Id> itemsIds = new Set<Id>();
        
        List<FeedItem> feedIds = [SELECT Id, Body, ParentId, RelatedRecordId, HasContent 
                                FROM FeedItem 
                                WHERE HasContent = true
                               	AND ParentId = :[SELECT Id FROM CollaborationGroup WHERE Name like '%QMI - %']
                               LIMIT 50];
        
        itemsIds.add(feedIds.get(0).id);
        
        String JSONString = JSON.serialize(itemsIds);
        FeedItemHandler.onInsert(JSONString);
        
    }
}