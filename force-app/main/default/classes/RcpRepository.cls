public class RcpRepository {
    
    public static Map<String,String> buildMapWithAllAccount(){
        
        Map<String,String> accountIdMap = new Map<String,String>();
        
        for(Account account : [SELECT id,name,COD_ORGz__c FROM Account])
            accountIdMap.put(String.valueOf(account.COD_ORGz__c), account.id);
        
        return accountIdMap;
    }
    
    public static Map<String,String> buildMapWithAllAircraft(){
        
        Map<String,String> aircraftIdMap = new Map<String,String>();
        
        for(Aircraft__c aircraft : [SELECT id, Name FROM Aircraft__c])
            aircraftIdMap.put(aircraft.Name, aircraft.id);
        
        return aircraftIdMap;
    }
    
    public static AccountTeamMember findAccountTeamMemberUpdated(){
        
        AccountTeamMember teamMemberList;
        
        try{
            
            teamMemberList = [SELECT id, TeamMemberRole, UserId, AccountId 
                              FROM AccountTeamMember
                              WHERE LastModifiedDate > :DateTime.now().addHours(-1) and LastModifiedDate < :DateTime.now()];
        
        }catch(QueryException e){
            
            System.debug('..... error ..... ' + e.getMessage());
        }
            
        return teamMemberList;
    }
    
    public static AccountTeamMember findAccountTeamMemberInserted(){
        
        AccountTeamMember teamMemberList;
        
        try{
            
            teamMemberList = [SELECT id, TeamMemberRole, UserId, AccountId 
                              FROM AccountTeamMember
                              WHERE CreatedDate > :DateTime.now().addHours(-1) and CreatedDate < :DateTime.now()];
        
        }catch(QueryException e){
            
            System.debug('..... error ..... ' + e.getMessage());
        }
            
        return teamMemberList;
    }
    
    public static List<AccountHistory> findAccountHistoryByAccountId(String accountId){
        
        List<AccountHistory> accountHistoryList = [SELECT id, 
                                	   					  CreatedDate,
                               		   					  CreatedById,
                               		   					  Field,
                               		   					  OldValue,
                               		   					  NewValue,
                               		   					  AccountId
                                					FROM AccountHistory
                                					WHERE AccountId = :accountId];
        
        return accountHistoryList;
    }
    
    public static List<Aircraft__History> findAircraftHistoryByAircratId(String aircraftId){
        
        List<Aircraft__History> aircraftHistList = [SELECT id, 
                                                    	   CreatedDate, 
                                                           CreatedById,
                                                           Field,
                                                           OldValue,
                                                           NewValue, 
                                                           ParentId 
                                                    FROM Aircraft__History 
                                                    WHERE ParentId = :aircraftId];
        return aircraftHistList;
    }

    
    public static List<User> searchUserByUserId(List<User> id){
        
        List<User> userList = [SELECT id, phone, E_mail__c, name FROM User WHERE id IN :id];
        
        return userList;
    }
    
    public static String findUserByUserId(String id){
        
        User user = [SELECT id, phone, E_mail__c FROM User WHERE id = :id];
        
        return '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';
    }
    
    public static List<User> searchUserByUserId(List<String> id){
        
        List<User> userList = [SELECT id, phone, E_mail__c, name FROM User WHERE id IN :id];
        
        return userList;
    }
    
    public static List<Account> searchFlyembraerId(List<Account> id){
        
        List<Account> accountList = [SELECT id, flyEmbraerId__c
                                     FROM Account
                                     WHERE id IN :id];
        return accountList;
    }
    
    public static List<Account> searchFlyembraerId(List<String> id){
        
        List<Account> accountList = [SELECT id, flyEmbraerId__c
                                     FROM Account
                                     WHERE id IN :id];
        return accountList;
    }
    
    public static List<AccountTeamMember> searchAccountTeamMemberUpdated(){
        
        List<AccountTeamMember> teamMemberList;
        
        try{
            
            teamMemberList = [SELECT id, TeamMemberRole, UserId, AccountId 
                              FROM AccountTeamMember
                              WHERE LastModifiedDate > :DateTime.now().addHours(-1) and LastModifiedDate < :DateTime.now()];
        
        }catch(QueryException e){
            
            System.debug('..... error ..... ' + e.getMessage());
        }
            
        return teamMemberList;
    }
    
    public static List<AccountTeamMember> searchAccountTeamMemberInserted(){
        
        List<AccountTeamMember> teamMemberList;
        
        try{
            
            teamMemberList = [SELECT id, TeamMemberRole, UserId, AccountId 
                              FROM AccountTeamMember
                              WHERE CreatedDate > :DateTime.now().addHours(-1) and CreatedDate < :DateTime.now()];
        
        }catch(QueryException e){
            
            System.debug('..... error ..... ' + e.getMessage());
        }
            
        return teamMemberList;
    }
}