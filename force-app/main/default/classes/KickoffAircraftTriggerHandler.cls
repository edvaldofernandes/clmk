public class KickoffAircraftTriggerHandler {
    public Map<Id,Kickoff_Aircraft__c> newRecordsMap = new Map<Id,Kickoff_Aircraft__c>();
    public Map<Id,Kickoff_Aircraft__c> oldRecordsMap = new Map<Id,Kickoff_Aircraft__c>(); 
    public List<Kickoff_Aircraft__c> newRecords = new List<Kickoff_Aircraft__c>();
    public List<Kickoff_Aircraft__c> oldRecords = new List<Kickoff_Aircraft__c>();
    
    public Map<Id,Kickoff_Aircraft__c> newRecordsMapWithAgreementFields;
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    public static boolean isRecursive = false;

    public KickoffAircraftTriggerHandler(boolean isExecuting) {
        this.isExecuting = isExecuting;
    }

    public void OnBeforeInsert()
    {
    }

    public void OnBeforeUpdate()
    {
    }

    public void OnBeforeDelete()
    {
        for(Kickoff_Aircraft__c KO: oldRecords) {
            if(KO.submitted__c == true) {
                KO.addError('You may not delete a submitted Kickoff Checklist.');
            }
        }
    }

    public void OnAfterInsert()
    {
    }

    public void OnAfterUpdate()
    {
    }

    public void OnAfterDelete()
    {
    }

    public void OnUndelete()
    {
    }
}