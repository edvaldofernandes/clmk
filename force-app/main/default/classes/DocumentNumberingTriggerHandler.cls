public without sharing class DocumentNumberingTriggerHandler {

	public Map<Id,Document_Numbering__c> newRecordsMap = new Map<Id,Document_Numbering__c>();
    public Map<Id,Document_Numbering__c> oldRecordsMap = new Map<Id,Document_Numbering__c>(); 
    public List<Document_Numbering__c> newRecords = new List<Document_Numbering__c>();
    public List<Document_Numbering__c> oldRecords = new List<Document_Numbering__c>();
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isFirstRun = false;
    public static boolean isRecursive = false;
  
    public DocumentNumberingTriggerHandler(boolean isFirstRun) {

    }

    public void OnBeforeInsert(){
		DocumentNumberingGenerator.setDate(newRecords);
    }

    public void OnBeforeUpdate(){
        
    }

    public void OnBeforeDelete(){

    }

    public void OnAfterInsert(){
		DocumentNumberingGenerator.generate(newRecords);
    }

    public void OnAfterUpdate(){

    }

    public void OnAfterDelete(){

    }

    public void OnUndelete(){

    }
}