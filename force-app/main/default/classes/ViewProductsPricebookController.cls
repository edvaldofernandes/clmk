/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Controller class for visualforce page ViewProductsPricebook 

* NAME: ViewProductsPricebookController.cls
* AUTHOR:JFS                                                DATE: 17/10/2014
*
*******************************************************************************/
public with sharing class ViewProductsPricebookController
{
  public list<PricebookEntry> Pricebookprodutos{get;set;}
  public Id Idproduct{get;set;}
  public Id IdPricebook{get;set;}
  
  public String labelPrice1 { get; set; }
  public String labelPrice2 { get; set; }
  public String labelPrice3 { get; set; }
  public boolean hasEditAcess { get; set; }
  
  private static final Map<String, String> mapRecordType = new Map<String, String>{
     'Aircraft Modification / Technical Services' => 'Aircraft_Modification',
     'eSolutions' => 'eSolutions',
     'Mix Products' => 'Mix_Products',
     'Pilot Services / On Site Services' => 'Pilot_services',
     'Tailored' => 'Tailored',
     'Training' => 'Training'
  };
  
  public Boolean showerror { get; set; }
  
  private Id idRegistro;

  public ViewProductsPricebookController(ApexPages.StandardController controller)
  {
    showerror = false;      
    idRegistro = controller.getId();
    this.hasEditAcess = false;
    
    if ( idRegistro.getSObjectType() == Schema.Pricebook2.SObjectType )
    {
        List<Pricebook2> lstPb2 = [SELECT Type__c FROM Pricebook2 WHERE Id =: idRegistro ];
        if ( !lstPb2.isEmpty() )
        {
          labelPrice1 = Utils.returnLabelField(mapRecordType.get(lstPb2[0].Type__c), 'Price1__c');
          labelPrice2 = Utils.returnLabelField(mapRecordType.get(lstPb2[0].Type__c), 'Price2__c');
          labelPrice3 = Utils.returnLabelField(mapRecordType.get(lstPb2[0].Type__c), 'Price3__c');
        }
    }
    else 
    {
      List<Product2> lstProd2 = [SELECT RecordType.DeveloperName FROM Product2 WHERE Id =: idRegistro ];
      if ( !lstProd2.isEmpty() )
      {
        labelPrice1 = Utils.returnLabelField(lstProd2[0].RecordType.DeveloperName, 'Price1__c');
          labelPrice2 = Utils.returnLabelField(lstProd2[0].RecordType.DeveloperName, 'Price2__c');
          labelPrice3 = Utils.returnLabelField(lstProd2[0].RecordType.DeveloperName, 'Price3__c');
      }
    }
    
    
    selectPbes();

    IdPricebook = controller.getRecord().Id;  
    List<UserRecordAccess> listUserAccess = new List<UserRecordAccess>();

    listUserAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = : UserInfo.getUserId()  AND RecordId = :IdPricebook];
    
    if(!listUserAccess.isEmpty())
        this.hasEditAcess = listUserAccess[0].HasEditAccess;    
  }
  
  private void selectPbes()
  {
    String query = 'SELECT Id, name, Product2Id, ProductCode, Pricebook2Id, ' +
      ' Pricebook2.name, Pricebook2.Annual_Escalation__c, Pricebook2.Type__c, ' +
      ' UseStandardPrice, UnitPrice, RECPrice__c, IsActive, Margin_0__c, Entry_fee__c, ' +
      ' Maximum_discount__c, Maximum_discount_REC__c, Maximum_disccount_Entry_fee__c, ' +
      ' CurrencyIsoCode, Product2.RecordType.DeveloperName, Product2.Unit__c ' +
      ' FROM PricebookEntry WHERE ';
    if ( idRegistro.getSObjectType() == Schema.Pricebook2.SObjectType )
    {
      query += ' Pricebook2id = \'' + idRegistro + '\' ' + 
        ' ORDER BY Pricebook2.IsStandard DESC, Product2.name ';
    }
    else 
    {
      query += ' Product2id = \'' + idRegistro + '\' ' + 
        ' ORDER BY Pricebook2.IsStandard DESC, Pricebook2.name ';
    }
    
    Pricebookprodutos = Database.query(query);
    
  }

  public PageReference EditAllProd()
  {
    PageReference Editpage = page.PricebookEditProduct;
    Editpage.setRedirect(true);
    Editpage.getParameters().put('id', IdPricebook);
    return Editpage;
  }

  public PageReference EditAllPrice()
  {
    PageReference Editpage = page.ProductEditPricebook;
    Editpage.setRedirect(true);
    Editpage.getParameters().put('id', IdPricebook);
    return Editpage;
  }

  public void ProductDelete()
  {
    showerror = false;
    
    list <PricebookEntry> Delproduct = [SELECT id, Pricebook2.IsStandard, Product2Id
      FROM PricebookEntry WHERE Product2Id =: Idproduct and Pricebook2id = :IdPricebook];
    if(Delproduct.isEmpty()) return;
    
    if ( Delproduct[0].Pricebook2.IsStandard )
    {
        List<PricebookEntry> lstPbe = [SELECT Id FROM PricebookEntry 
          WHERE Product2Id =: Delproduct[0].Product2Id AND Pricebook2.IsStandard = false ];
        if ( !lstPbe.isEmpty() )
        {
            showerror = true;
        return;
        }
    }
    
    delete Delproduct;
     
    selectPbes();  
  }

  public void PricebookDelete()
  {
    showerror = false;
       
    list <PricebookEntry> Delpricebook = [SELECT id, Pricebook2.IsStandard, Product2Id
      FROM PricebookEntry WHERE Product2Id =: Idproduct and Pricebook2id = :IdPricebook];
    if(Delpricebook.isEmpty()) return;
    
    if ( Delpricebook[0].Pricebook2.IsStandard )
    {
      List<PricebookEntry> lstPbe = [SELECT Id FROM PricebookEntry 
        WHERE Product2Id =: Delpricebook[0].Product2Id AND Pricebook2.IsStandard = false ];
      if ( !lstPbe.isEmpty() )
      {
        showerror = true;
        return;
      }
    }
    
        delete Delpricebook;
       
      selectPbes();    
  }

  public PageReference viewproduct()
  {
    PageReference Vproduct = new PageReference('/' + Idproduct);
    Vproduct.setRedirect(true);

    return Vproduct;
  }

  public PageReference viewpricebook()
  {
    PageReference Vpricebook = new PageReference('/' + IdPricebook);
    Vpricebook.setRedirect(true);

    return Vpricebook;
  }
  
}