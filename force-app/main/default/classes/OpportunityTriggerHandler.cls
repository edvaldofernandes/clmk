public without sharing class OpportunityTriggerHandler
{

    
    public Map<Id,Opportunity> newRecordsMap = new Map<Id,Opportunity>();
    public Map<Id,Opportunity> oldRecordsMap = new Map<Id,Opportunity>(); 
    public List<Opportunity> newRecords = new List<Opportunity>();
    public List<Opportunity> oldRecords = new List<Opportunity>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    private static Id RecordTypeAcMod = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();       
    private static Id RecordTypeESolution = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();       
    private static Id RecordTypeMaintenance = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Maintenance Services').getRecordTypeId();       
    private static Id RecordTypeMaterial = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();       
    private static Id RecordTypeMixProducts = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Mix Products').getRecordTypeId();       
    private static Id RecordTypePilots = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pilot Services / On Site Services').getRecordTypeId();       
    private static Id RecordTypeTech = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();       
    private static Id RecordTypeTraining = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Training').getRecordTypeId();       

    public OpportunityTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        popularType();
        popularServiceProgramType();
        
        saveClosureAndRevenueChangeCommentsOnInsert();
       

    }

 

    public void OnAfterInsert()
    {

        // EXECUTE AFTER INSERT LOGIC

    }

 

    public void OnBeforeUpdate()
    {
        saveClosureAndRevenueChangeCommentsOnUpdate();

        validateStatusContractChange();
        popularType();
        popularServiceProgramType();
        validateeSolutionUser();
        
        

    }

 

    public void OnAfterUpdate()
    {

        sendApprovalMessage();
        updateRelatedQuotes();

    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 
    
    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }
    
    private void saveClosureAndRevenueChangeCommentsOnInsert(){
        for(Opportunity opp : newRecords){
            if(opp.Closure_Revenue_Comments_for_changes__c != null && (opp.RecordTypeId == RecordTypeAcMod || opp.RecordTypeId == RecordTypeESolution || opp.RecordTypeId == RecordTypeMaintenance  || opp.RecordTypeId == RecordTypeMaterial || opp.RecordTypeId == RecordTypeMixProducts || opp.RecordTypeId == RecordTypePilots || opp.RecordTypeId == RecordTypeTech || opp.RecordTypeId == RecordTypeTraining)){
                //opp.Date_revenue_this_year_changes_history__c = '(' + Date.Today().format() +' by '+ UserInfo.getName() +'): ' + opp.Closure_Revenue_Comments_for_changes__c;
                opp.Date_revenue_this_year_changes_history__c = 'Created ' + Date.Today().format()  + ' by ' + UserInfo.getName() + '.\nClosure:' + opp.CloseDate.format() + '\nRevenue: ' + opp.Amount_THIS_YEAR__c + '\nComments: ' + opp.Closure_Revenue_Comments_for_changes__c;
                opp.Closure_Revenue_Comments_for_changes__c = null;
            }
        }
    }
    
    private void saveClosureAndRevenueChangeCommentsOnUpdate(){
        for(Opportunity opp : newRecords){
            if(opp.Closure_Revenue_Comments_for_changes__c != null  && (opp.RecordTypeId == RecordTypeAcMod || opp.RecordTypeId == RecordTypeESolution || opp.RecordTypeId == RecordTypeMaintenance  || opp.RecordTypeId == RecordTypeMaterial || opp.RecordTypeId == RecordTypeMixProducts || opp.RecordTypeId == RecordTypePilots || opp.RecordTypeId == RecordTypeTech || opp.RecordTypeId == RecordTypeTraining)){
                if(oldRecordsMap.get(opp.Id).Date_revenue_this_year_changes_history__c != null){
                    //opp.Date_revenue_this_year_changes_history__c = '(' + Date.Today().format() +' by '+ UserInfo.getName() +'): ' + opp.Closure_Revenue_Comments_for_changes__c + '\n' + oldRecordsMap.get(opp.Id).Date_revenue_this_year_changes_history__c;
                    opp.Date_revenue_this_year_changes_history__c = 'Modified ' + Date.Today().format()  + ' by ' + UserInfo.getName() + '.\nClosure:' + oldRecordsMap.get(opp.Id).CloseDate.format() + ' -> ' + opp.CloseDate.format() + '\nRevenue: ' + oldRecordsMap.get(opp.Id).Amount_THIS_YEAR__c + ' -> ' + opp.Amount_THIS_YEAR__c + '\nComments: ' + opp.Closure_Revenue_Comments_for_changes__c + '\n- - - - \n'+ oldRecordsMap.get(opp.Id).Date_revenue_this_year_changes_history__c;
                    //'Modified ' + Date.Today().format()  + ' by ' + UserInfo.getName() + '\nClosure: ’ + oldRecordsMap.get(opp.Id).CloseDate + ' -> ’ + opp.CloseDate + '\nRevenue: ' + oldRecordsMap.get(opp.Id).Amount_THIS_YEAR__c + ' ->’ + opp.Amount_THIS_YEAR__c + '\nComments: ' + opp.Closure_Revenue_Comments_for_changes__c; 
                    opp.Closure_Revenue_Comments_for_changes__c = null;
                } else {
                    opp.Date_revenue_this_year_changes_history__c = 'Modified ' + Date.Today().format()  + ' by ' + UserInfo.getName() + '.\nClosure:' + oldRecordsMap.get(opp.Id).CloseDate.format() + ' -> ' + opp.CloseDate.format() + '\nRevenue: ' + oldRecordsMap.get(opp.Id).Amount_THIS_YEAR__c + ' -> ' + opp.Amount_THIS_YEAR__c + '\nComments: ' + opp.Closure_Revenue_Comments_for_changes__c;
                    opp.Closure_Revenue_Comments_for_changes__c = null;

                }
                
            }
        }
    }
	
    private void sendApprovalMessage()
    {
        List<String> listaCorpo = new List<String>();
        Map<Id,String> mapaIdsObjetoNome = new Map<Id,String>();  
        Map<Id,String> mapaIdsAccount = new Map<Id,String>();
        List<Id> listaIdsUsuarios = new List<Id>();
        List<String> listaAssunto = new List<String>();
        Set<Id> idsOportunidades = new Set<Id>();
        
        for(Opportunity opp : this.newRecords)
        {
            if(opp.Approval_status__c != this.oldRecordsMap.get(opp.Id).Approval_status__c && (opp.Approval_status__c == 'Approved' || opp.Approval_status__c == 'Rejected'))
                idsOportunidades.add(opp.Id);
        }
        if(!idsOportunidades.isEmpty())
        {
            for(Opportunity opp : [Select Id,Name, OwnerId, Account.Name From Opportunity Where Id in : idsOportunidades])
            {
                mapaIdsObjetoNome.put(opp.Id,opp.Name);
                mapaIdsAccount.put(opp.Id,opp.Account.Name);
                listaIdsUsuarios.add(opp.OwnerId);
                listaAssunto.add('Your Approval request was concluded');
            }
            
            listaCorpo = utils.comporTextoEmailAprovacao(mapaIdsObjetoNome,mapaIdsAccount);
            utils.enviarEmail(listaIdsUsuarios, listaCorpo, listaAssunto,true);
        }     
    
    }
    
    private void popularType()
    {
        Id recordTypeACM = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
        Id recordTypeTS = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
        boolean registroElegivel = false;
         
        
        for(Opportunity opp : this.newRecords)
        {
            if(this.IsInsert)
            {
                registroElegivel = opp.RecordTypeId == recordTypeACM || opp.RecordTypeId == recordTypeTS;
            }
            else
            {
                registroElegivel = (this.oldRecordsMap.get(opp.Id).RecordTypeId != opp.RecordTypeId) && (opp.RecordTypeId == recordTypeACM || opp.RecordTypeId == recordTypeTS);
            }
            if(registroElegivel)
                opp.Type = (opp.RecordTypeId == recordTypeACM ? 'Aircraft Modification'  : 'Technical Services');
        }   
        
        
    }
    
    private void popularServiceProgramType()
    {
        Id recordTypeMaterial = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
        Id recordTypeTS = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
        Id recordTypeMaintenance = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Maintenance Services').getRecordTypeId();
        boolean registroElegivel = false;
         
        
        for(Opportunity opp : this.newRecords)
        {
            if(this.IsInsert)
            {
                registroElegivel = opp.RecordTypeId == recordTypeMaterial || opp.RecordTypeId == recordTypeTS || opp.RecordTypeId == recordTypeMaintenance;
            }
            else
            {
                registroElegivel = (this.oldRecordsMap.get(opp.Id).RecordTypeId != opp.RecordTypeId) && (opp.RecordTypeId == recordTypeMaterial || opp.RecordTypeId == recordTypeTS || opp.RecordTypeId == recordTypeMaintenance);
            }
            if(registroElegivel)
                opp.Service_Program_Type__c = (opp.RecordTypeId == recordTypeTS ? 'Technical Services'  : (opp.RecordTypeId == recordTypeMaterial ? 'Material Services'  : 'Maintenance Services'));
        }   
        
        
    }
    

    
    private void validateStatusContractChange()
    {
    
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        Set<Id> idUsersEsolutionsGroup = utils.getUserIdsFromGroup('eSolutions_ADMIN');
        Set<Id> elegibleIds = new Set<Id>();
        
        for(Opportunity opp : this.newRecords)
        {
            if(opp.recordTypeId == recordTypeId && opp.StageName!= this.oldRecordsMap.get(opp.Id).StageName && opp.StageName == 'Closed Won')
            {
                if(!idUsersEsolutionsGroup.contains(UserInfo.getUserId()))
                    elegibleIds.add(opp.Id);
                    //opp.StageName.addError('You can´t change opportunity stage to "Closed Won" without a related Signed Contract.');
            
            }
                 
        
        }
        
        if(!elegibleIds.isEmpty())
        {
            for(Opportunity opp : [Select Id, (Select Id  From Service_Contracts__r Where Contract_Status__c = 'signed') From Opportunity Where Id in : elegibleIds])
            {
                if(opp.Service_Contracts__r == Null)
                    this.newRecordsMap.get(opp.Id).StageName.addError('You can´t change opportunity stage to "Closed Won" without a related Signed Contract.');
            }    
        }
        
    }


    private void updateRelatedQuotes()
    {
    
        Map<Id,String> mapaOppStatus = new Map<Id,String>();
        List<Quote> quotesToUpdate = new List<Quote>();
        
        for(Opportunity opp : this.newRecords)
        {
            if(opp.StageName != this.oldRecordsMap.get(opp.Id).StageName && (opp.StageName == 'Closed Lost' || opp.StageName == 'Contract Negotiation & Review'))
                mapaOppStatus.put(opp.Id,opp.StageName);    
        }
        
        if(!mapaOppStatus.isEmpty())
        {
            for(Quote qt : [Select Status,IsSyncing, OpportunityId From Quote Where OpportunityId in : mapaOppStatus.keySet()])
            {
                if(mapaOppStatus.get(qt.OpportunityId) == 'Closed Lost')
                {
                    qt.Status = 'Dropped';  
                }
                else
                {
                    if(qt.Status != 'Accepted by Customer' && !qt.IsSyncing)
                        qt.Status = 'Denied';   
                }
                quotesToUpdate.add(qt);
                
            }
            
            if(!quotesToUpdate.isEmpty())
                update quotesToUpdate;
            
        }
        
    }


    private void validateeSolutionUser()
    {
    
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        Set<Id> idUsersEsolutionsGroup = utils.getUserIdsFromGroup('eSolutions_ADMIN');
        Set<Id> idOppWitheSolutionsProducts = new Set<Id>(); 
        string product = '';
    
        for(Opportunity opp : this.newRecords)
        {
            if(opp.recordTypeId == recordTypeId && (opp.eSolutions_product_count__c > this.oldRecordsMap.get(opp.Id).eSolutions_product_count__c) || Test.isRunningTest())
            {
                opp.ItemInsertedbyeSolutionsUser__c = idUsersEsolutionsGroup.contains(UserInfo.getUserId());
                idOppWitheSolutionsProducts.add(opp.Id);
            }
        }
        System.Debug('Tamanho dos Ids - ' + idOppWitheSolutionsProducts.size());
        
        if(!idOppWitheSolutionsProducts.IsEmpty())
        {
            for(Opportunity opp : [Select Id, (Select Product2.Name From OpportunityLineItems Where Product_type_updated__c = 'eSolutions') From Opportunity  Where Id in : idOppWitheSolutionsProducts])
            {
                product = '';
                System.Debug('Tamanho dos itens- ' +  opp.OpportunityLineItems.size());
                for(OpportunityLineItem  lineItem : opp.OpportunityLineItems)
                {
                    System.Debug('Nome do Produto - ' + lineItem.Product2.Name);
                    if(lineItem.Product2.Name != Null)
                    {
                        if(!product.contains(lineItem.Product2.Name))
                        {
                            if(product != '')
                                product += '\n';
                                
                            product += lineItem.Product2.Name;
                            this.newRecordsMap.get(opp.Id).eSolutionsProducts__c = product;
                        }
                    }
                }
            }     
        } 
        
    }


}