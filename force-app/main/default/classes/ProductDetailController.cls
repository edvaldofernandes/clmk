/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Controller class for the visualforce page ProductDetail    
*
* NAME: ProductDetailController.cls
* AUTHOR: RLdO                                                DATE: 23/12/2014
*******************************************************************************/
public with sharing class ProductDetailController
{
  private static Id RT_PROD_ACFT_MODIFICATION = RecordTypeMemory.getRecType('Product2','Aircraft_Modification');
  private static Id RT_PROD_TRAINING = RecordTypeMemory.getRecType('Product2','Training');
  private static Id RT_PROD_PILOT_SVC = RecordTypeMemory.getRecType('Product2','Pilot_services');
  private static Id RT_PROD_ESOLUTIONS = RecordTypeMemory.getRecType('Product2','eSolutions');
  private static Id RT_PROD_TAILORED = RecordTypeMemory.getRecType('Product2','Tailored');
  
  public String labelPrice1 { get; set; }
  public String labelPrice2 { get; set; }
  public String labelPrice3 { get; set; }

  public Product2 lProduct {get; set;}
  public List<PricebookEntry> lPricebookEntry { get; set; }
  //
  public Boolean ShowFieldAcftMod { get { return (this.lProduct.RecordTypeId == RT_PROD_ACFT_MODIFICATION); } }
  public Boolean ShowFieldTraining { get { return (this.lProduct.RecordTypeId == RT_PROD_TRAINING); } }
  public Boolean ShowFieldPilotSvc { get { return (this.lProduct.RecordTypeId == RT_PROD_PILOT_SVC); } }
  public Boolean ShowFieldESolutions { get { return (this.lProduct.RecordTypeId == RT_PROD_ESOLUTIONS); } }
  public Boolean ShowFieldTailored { get { return (this.lProduct.RecordTypeId == RT_PROD_TAILORED); } }
  //
  public Boolean ShowPrdCode { get { return (!this.ShowFieldTailored); } }
  //public Boolean ShowPrdDescr { get { return (!this.ShowFieldTraining && !this.ShowFieldESolutions); } }
  public Boolean ShowPrdDescr { get { return (this.ShowFieldAcftMod || this.ShowFieldPilotSvc
    || this.ShowFieldTailored); } }
    
  public Boolean ShowPricebookEntry { get; set; }

  public ProductDetailController()
  {
    String sProductId = ApexPages.currentPage().getParameters().get('Id');
    list<Product2> lstProd = [select Id, Name, ProductCode, Description__c, Benefits__c,
      Applicability__c, Estimated_Aircraft_Downtime__c, Estimated_Aircraft_Man_Hours__c, 
      Estimated_Weight_Change__c, Course_Description__c, Course_Objectives__c,
      Minimum_Number_of_trainees__c, Course_Duration__c, Features__c, 
      Aircraft_Level_Requirements__c, Airline_Level_Requirements__c, Trial_Period__c,
      Product_Status__c, RecordTypeId, RecordType.Name, RecordType.DeveloperName
      from Product2
      where Id = :sProductId];
      
    this.lProduct = (sProductId == null || lstProd == null || lstProd.isEmpty())
      ? new Product2() : lstProd[0];
      
    labelPrice1 = Utils.returnLabelField(lProduct.RecordType.DeveloperName, 'Price1__c');
    labelPrice2 = Utils.returnLabelField(lProduct.RecordType.DeveloperName, 'Price2__c');
    labelPrice3 = Utils.returnLabelField(lProduct.RecordType.DeveloperName, 'Price3__c');
      
    lPricebookEntry = [ SELECT Pricebook2.name, CurrencyIsoCode, UnitPrice, RECPrice__c, Entry_fee__c, 
      Maximum_discount__c, Maximum_discount_REC__c, Maximum_disccount_Entry_fee__c, IsActive    
      FROM PricebookEntry WHERE Product2Id =: sProductId ];
      
    ShowPricebookEntry = !lPricebookEntry.isEmpty();
  }
}