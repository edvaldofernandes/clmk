@isTest
public with sharing class ProductWithPrice_Test {

    public ProductWithPrice_Test() {
        testMe();
    }
    
    public static TestMethod void testMe() {
        ProductWithPrice pwp = new ProductWithPrice();
        Product2 testProduct = new Product2(name='Test Product');
        insert testProduct;
        pwp.ProductID = testProduct.id;
        pwp.Name = 'Josh';
        pwp.UnitPrice = 50.00;
        pwp.ImageUrl = 'http://en.wikipedia.org/wiki/File:SFDC_logo.jpg';
        System.assertEquals(testProduct.id,pwp.productId);
    }

}