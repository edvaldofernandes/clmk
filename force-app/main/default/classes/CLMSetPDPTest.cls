//This class tests CLMSetPDP and CLMSetPDPController

@isTest
public class CLMSetPDPTest {
	
    @testSetup 
    static void generateTestData(){
        Product2 productTest = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        database.insert(productTest);
        
        id priceBook2Id = SObjectInstanceTest.getPricebook2Std2();
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(priceBook2Id, productTest.Id); 
        database.insert(priceBookEntryTest);
        
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        //Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(new List<Agreement__c>{Agreement1});
        
        //create contracts pdp to be cloned
        Contract_PDP__c contract1 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = 'Initial Deposit', Payment_ammount__c = 10000000, Payment_Date__c = Date.today().addDays(1));
        Contract_PDP__c contract2 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '1 Progress Payment', Payment_Percentage__c = 50, Payment_Date_Delivery_Based__c = 3);
        //Contract_PDP__c contract3 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '2 Progress Payment', Payment_ammount__c = 3, Payment_Date__c = Date.today().addDays(60));
        //Contract_PDP__c contract4 = new Contract_PDP__c(Commercial_Agreement__c = Agreement1.Id, Installment_Type__c = '3 Progress Payment', Payment_ammount__c = 4, Payment_Date__c = Date.today().addDays(90));
        database.insert(new List<Contract_PDP__c>{contract1,contract2});
        
        //create aircraft prices to be cloned
        Aircraft_Price__c aircraftPrice1 = createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice2 = createAircraftPrice(Agreement1.Id, productTest.id);
        Aircraft_Price__c aircraftPrice3 = createAircraftPrice(Agreement1.Id, productTest.id);
        database.insert(new List<Aircraft_Price__c>{aircraftPrice1,aircraftPrice2,aircraftPrice3});
        
        //create line item to be cloned
        Agreement_Aircraft__c agreementAircraft1 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        agreementAircraft1.Aircraft_Price__c = aircraftPrice1.Id;
        Agreement_Aircraft__c agreementAircraft2 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        agreementAircraft2.Aircraft_Price__c = aircraftPrice2.Id;
        Agreement_Aircraft__c agreementAircraft3 = SObjectInstanceTest.createAgreementAircraft(productTest.Id, Agreement1.Id, 'Firm');
        agreementAircraft3.Aircraft_Price__c = aircraftPrice3.Id;
        database.insert(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3});
        
        Agreement1.Status_Category__c = 'Proposal Request';
        database.update(new List<Agreement__c>{Agreement1});

        agreementAircraft1.Basic_Price__c = 20000000;
        agreementAircraft2.Basic_Price__c = 20000000;
        agreementAircraft3.Basic_Price__c = 20000000;
        database.update(new List<Agreement_Aircraft__c>{agreementAircraft1,agreementAircraft2,agreementAircraft3});
    }
    
    
    static testMethod void setPDPtest(){
        
		List<Agreement__c> Agreement1 = [SELECT Id From Agreement__c Limit 1];
        
        test.startTest();
       		CLMSetPDP sPDP = new CLMSetPDP();
            String Agreement = sPDP.setPDP(String.valueOf(Agreement1[0].Id));
            system.debug('CLMSetPDP.setPDP >>> ' + Agreement);
        test.stopTest();
        system.debug('[SELECT id FROM Aircraft_PDP__c].size()' + [SELECT id FROM Aircraft_PDP__c].size());
        system.assert([SELECT id FROM Aircraft_PDP__c].size() == 6);
        
        List<Agreement_Aircraft__c> agreementAircraft3 = [SELECT Id FROM Agreement_Aircraft__c LIMIT 1];
        
        List <Aircraft_PDP__c> AircraftPayment = [SELECT Id, Contract_Aircraft__c, Payment_Amount__c, Due_Date_L__c, Status_L__c FROM Aircraft_PDP__c WHERE Contract_Aircraft__c = :agreementAircraft3[0].Id];
        
        system.assert(AircraftPayment.size() == 2);
        
        for(Aircraft_PDP__c aPDP : AircraftPayment){
        	system.assert(aPDP.Payment_Amount__c == 10000000);
        }
                   
                      
		//system.debug('ac basic price: ' + agreementAircraft1.Basic_Price__c);
        //system.debug('ac basic price: ' + agreementAircraft2.Basic_Price__c);
        //system.debug('ac basic price: ' + agreementAircraft3.Basic_Price__c);
        //system.debug('ac payment: ' + [SELECT Id, Contract_Aircraft__c, Payment_Amount__c, Due_Date_L__c, Status_L__c FROM Aircraft_PDP__c WHERE Contract_Aircraft__c = :agreementAircraft3.Id]);


    }
    
    static testMethod void controllerTestNoAircraft(){
        
		Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);

        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(new List<Agreement__c>{Agreement1});
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement1.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMSetPDPController sPDPC = new CLMSetPDPController(sc);
        
        sPDPC.setPDP();
        
        system.assert([SELECT id FROM Aircraft_PDP__c].size() == 0);
        
    }
    
    static testMethod void controllerTestNormalAgreement(){
        
		List<Agreement__c> Agreement1 = [SELECT Id From Agreement__c Limit 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement1[0].Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1[0]);
        CLMSetPDPController sPDPC = new CLMSetPDPController(sc);
        
        sPDPC.setPDP();
        system.debug('[SELECT id FROM Aircraft_PDP__c].size()' + [SELECT id FROM Aircraft_PDP__c].size());
        system.assert([SELECT id FROM Aircraft_PDP__c].size() == 6);
        
    }
    
    static testMethod void controllerTestNullAgreement(){
        
        List<Agreement__c> Agreement1 = [SELECT Id From Agreement__c Limit 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',null);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1[0]);
        CLMSetPDPController sPDPC = new CLMSetPDPController(sc);
        
        sPDPC.setPDP();
        
        system.assert([SELECT id FROM Aircraft_PDP__c].size() == 0);
        
    }
    
    public static Aircraft_Price__c createAircraftPrice(Id agreementId, String modelId) {
        Aircraft_Price__c aircraftPrice = new Aircraft_Price__c(
            Commercial_Agreement__c = agreementId,
            Model__c = modelId,
            Aircraft_Version__c = 'LR',
            Economic_Condition__c = 'Jan/16',
            Aircraft_Basic_Price__c = 20000000
        );

        return aircraftPrice;
    }
}