/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the class ProductEditPricebookController

* NAME: ProductEditPricebookControllerTest.cls
* AUTHOR:DPF                                                DATE: 04/11/2014
*
*******************************************************************************/

@isTest(seeAllData=true)
private class ProductEditPricebookControllerTest {

  static testMethod void teste()
  {
    Product2 lProduct = SObjectInstanceTest.createProduct2();
    Database.insert(lProduct);
      
    Id stdPricebook2Id = SObjectInstanceTest.getPricebook2Std2();
      
    PricebookEntry pbeStd = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct.id);
    Database.insert(pbeStd);
        
    Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
    lPricebook.Name = 'pricebook teste 1';
    lPricebook.Annual_Escalation__c = 10;
    Database.insert(lPricebook);
                
    PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lProduct.Id);
    lPricebookentry.UnitPrice = 20;
    lPricebookentry.RECPrice__c = 100; 
    Database.insert(lPricebookentry);
    
     PageReference pageRef = Page.ProductEditPricebook;
     Test.setCurrentPage(pageRef);
     ApexPages.currentPage().getParameters().put('lstaPb', lPricebook.Id);
      
        
    ProductEditPricebookController controller = new ProductEditPricebookController(new Apexpages.Standardcontroller(lProduct));
    
   

    Test.startTest();      
    controller.mysave();
    Test.stopTest();
  }
  
  static testMethod void unitTest()
  {
    Product2 lProduct = SObjectInstanceTest.createProduct2();
    Database.insert(lProduct);
      
    Id stdPricebook2Id = SObjectInstanceTest.getPricebook2Std2();
      
    PricebookEntry pbeStd = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct.id);
    Database.insert(pbeStd);
        
    Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
    lPricebook.Name = 'pricebook teste 1';
    lPricebook.Annual_Escalation__c = 10;
    Database.insert(lPricebook);
                
    PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lProduct.Id);
    lPricebookentry.UnitPrice = 20;
    lPricebookentry.RECPrice__c = 100; 
    Database.insert(lPricebookentry);
    
     PageReference pageRef = Page.ProductEditPricebook;
     Test.setCurrentPage(pageRef);
    ProductEditPricebookController controller = new ProductEditPricebookController(new Apexpages.Standardcontroller(lProduct));
    
    Test.startTest();      
    controller.mysave();
    id teste = controller.RecProductTailored;
    id teste1 = controller.RecProductPilots;
    id teste2 = controller.RecProductTraining;
    Test.stopTest();
  }
}