/**
* @author Marcilio Leite de Souza
* @date 03/07/2018
* @description: TEST CLASS FOR GEN_Util
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           03 JUL 2018             Original Version
**/
@isTest
private class GEN_Util_Test{

    @isTest
    static void it_picklist_to_option() {

        Test.startTest();
        List<SelectOption> listOpt = GEN_Util.getSelectOptionFromPickList(Case.Status.getDescribe().getPicklistValues(), false);
        System.assertEquals(Case.Status.getDescribe().getPicklistValues().size(), listOpt.size(), 'List size has to be the same');
        Test.stopTest();
    }
    
    @isTest
    static void it_date_from_string() {

        Test.startTest();
        String strDt = '03/07/2018';
        Date dt = Date.newInstance(2018, 07, 03);
        System.assertEquals(dt, GEN_Util.getDateFromStringDate(strDt), 'Date has to be the same');
        Test.stopTest();
    }
    
    @isTest
    static void it_time_from_string() {
        
        Test.startTest();
        String strTm = '14:30';
        Time tm = Time.newInstance(14, 30, 0, 0);
        System.assertEquals(tm, GEN_Util.getTimeFromStringTime(strTm), 'Time has to be the same');
        Test.stopTest();
    }
    
}