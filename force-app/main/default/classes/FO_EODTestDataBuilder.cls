@isTest
public class FO_EODTestDataBuilder {
	private String Description;
    private String Operational_Disposition;
    private String Original_Report_Type;
    private String Status;
    private String Subject = 'Lorem Ipsum';
	private Date Expiry_Date;
    private Date Issue_Date = Date.parse('05/17/1994');
    private Date Original_Report_Date;
    private Id Parent_EOD;
    private Id Related_Case = generateRelatedCase();
    private Boolean Show_Disclaimers;

    public static Id generateRelatedCase(){
        Case c = new Case();
        insert c;
        return c.Id;
    }
    public FO_EODTestDataBuilder withDescription(String Description){
        this.Description = Description;
        return this;
    }  
    public FO_EODTestDataBuilder withOperationalDisposition(String operationalDisposition){
        this.Operational_Disposition = operationalDisposition;
        return this;
    }
    public FO_EODTestDataBuilder withOriginalReportType(String originalReportType){
        this.Original_Report_Type = originalReportType;
        return this;
    }
    public FO_EODTestDataBuilder withStatus(String Status){
        this.Status = Status;
        return this;
    }
    public FO_EODTestDataBuilder withSubject(String Subject){
        this.Subject = Subject;
        return this;
    }
    public FO_EODTestDataBuilder withExpiryDate(Date expiryDate){
        this.Expiry_Date = expiryDate;
        return this;
    }
    public FO_EODTestDataBuilder withIssueDate(Date issueDate){
        this.Issue_Date = issueDate;
        return this;
    }
    public FO_EODTestDataBuilder withOriginalReportDate(Date originalReportDate){
        this.Original_Report_Date = originalReportDate;
        return this;
    }
    public FO_EODTestDataBuilder withParentEOD(Id parentEOD){
        this.Parent_EOD = parentEOD;
        return this;
    }
    public FO_EODTestDataBuilder withRelatedCase(Id relatedCase){
        this.Related_Case = relatedCase;
        return this;
    }
    public FO_EODTestDataBuilder withShowDisclaimers(Boolean showDisclaimers){
        this.Show_Disclaimers = showDisclaimers;
        return this;
    }
    public EOD__c build(){
        EOD__c eod = new EOD__c(
            Description__c = Description,
            Expiry_Date__c = Expiry_Date,
            Issue_Date__c = Issue_Date,
            Operational_Disposition__c = Operational_Disposition,
            Original_Report_Date__c = Original_Report_Date,
            Original_Report_Type__c = Original_Report_Type,
            Parent_EOD__c = Parent_EOD,
            Related_Case__c = Related_Case,
//            Show_Disclaimers__c = Show_Disclaimers,
            Status__c = Status,
            Subject__c = Subject
        );
        insert eod;
        return eod;
    }
}