@isTest
private class PlanningFLLTest {
    
    							   /*********************************
    --------------------------------** PlanningReportFLLController **--------------------------------
    								*********************************/
    //TEST UTILITIES CLASS
    @isTest static void testSearchPlanningReportEcode(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string ecode = '7063957';
        string regionAndSegment = 'FLL COM';
        string result = PlanningReportUtilities.searchPlanningReport(ecode, regionAndSegment);
        Test.stopTest();
        
        string expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, result);
    }
    
    @isTest static void testSearchPlanningReportPartNumber(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string partNumber = 'HEA3610E145';
        string regionAndSegment = 'FLL COM';
        string result = PlanningReportUtilities.searchPlanningReport(partNumber, regionAndSegment);
        Test.stopTest();
        
        string expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, result);
    }
    
	@isTest static void testSearchPlanningReportNoMatch(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string partNumber = '1234abcd';
        string regionAndSegment = 'FLL COM';
        string result = PlanningReportUtilities.searchPlanningReport(partNumber, regionAndSegment);
        Test.stopTest();
        
        string expectedTopmost = null;
        System.assertEquals(expectedTopmost, result);
    }    
    
    @isTest static void testGetStockInfosFLLCOM(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'FLL COM';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getStockInfosByRegionAndSegment(topmost, regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 3;
        System.assertEquals(expectedResult, results.Size());
    }    
    
    @isTest static void testGetStockInfosLBGCOM(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'LBG COM';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getStockInfosByRegionAndSegment(topmost, regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 1;
        System.assertEquals(expectedResult, results.Size());
    }
    
    @isTest static void testGetStockInfosFLLEXE(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'FLL EXE';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getStockInfosByRegionAndSegment(topmost, regionAndSegment);
        Test.stopTest();
        
        List<Stock_Info_FLL__c> expectedResult = null;
        System.assertEquals(expectedResult, results);
    }
    
    @isTest static void testGetStockInfosLBGEXE(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'LBG EXE';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getStockInfosByRegionAndSegment(topmost, regionAndSegment);
        Test.stopTest();
        
        List<Stock_Info_FLL__c> expectedResult = null;
        System.assertEquals(expectedResult, results);
    }
    
    @isTest static void testGetRepairInfos(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'FLL COM';
        List<Repair_Information__c> results = PlanningReportUtilities.getRepairInfosByRegionAndSegment(topmost, regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 4;
        System.assertEquals(expectedResult, results.Size());
    }  

	@isTest static void testGetCoreInfos(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'FLL COM';
        List<Core_Information__c> results = PlanningReportUtilities.getCoreInfosByRegionAndSegment(topmost, regionAndSegment.left(3), regionAndSegment.right(3) + '%');
        Test.stopTest();
        
        integer expectedResult = 2;
        System.assertEquals(expectedResult, results.Size());
    }

	@isTest static void testGetRessInfos(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'FLL COM';
        List<RESS_Info__c> results = PlanningReportUtilities.getRessInfosByRegionAndSegment(topmost, regionAndSegment.left(3), regionAndSegment.right(3) + '%');
        Test.stopTest();
        
        integer expectedResult = 2;
        System.assertEquals(expectedResult, results.Size());
    }
        @isTest static void testGetPOInfos(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string topmost = '7430';
        string regionAndSegment = 'FLL COM';
        List<PO__c> results = PlanningReportUtilities.getPOInfosByRegionAndSegment(topmost, regionAndSegment.left(3), regionAndSegment.right(3) + '%');
        Test.stopTest();
        
        integer expectedResult = 4;
        System.assertEquals(expectedResult, results.Size());
    }
    
    
    @isTest static void testdoRESSCount(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportUtilities.doRESSCount();
        Test.stopTest();
        
        List<Stock_Info_FLL__c> results = [SELECT RESS_Count__c, RESS_IDs__c FROM Stock_Info_FLL__c WHERE Region_and_Segment__c='FLL COM' AND Is_Summary_Record__c=true LIMIT 1];
        integer expectedResult = 2;
        string expectedRESS1 = '1111 2222';
        string expectedRESS2 = '2222 1111';
        System.assertEquals(expectedResult, results[0].RESS_Count__c);	//verify ress count is 2
        System.assert(results[0].RESS_IDs__c==expectedRESS1 || results[0].RESS_IDs__c==expectedRESS2);	//verify ress ids string
    } 
    
    @isTest static void testGetAdvancedSearchResultsFLLCOM(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = 'FLL COM';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getAdvancedSearchResults(regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 1;
        System.assertEquals(expectedResult, results.Size());
    }
    
    @isTest static void testGetAdvancedSearchResultsLBGCOM(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = 'LBG COM';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getAdvancedSearchResults(regionAndSegment);
        Test.stopTest();
        
        integer expectedResult = 1;
        System.assertEquals(expectedResult, results.Size());
    }
    
    @isTest static void testGetAdvancedSearchResultsFLLEXE(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = 'FLL EXE';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getAdvancedSearchResults(regionAndSegment);
        Test.stopTest();
        
        List<Stock_Info_FLL__c> expectedResult = null;
        System.assertEquals(expectedResult, results);
    }
    
    @isTest static void testGetAdvancedSearchResultsLBGEXE(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        string regionAndSegment = 'LBG EXE';
        List<Stock_Info_FLL__c> results = PlanningReportUtilities.getAdvancedSearchResults(regionAndSegment);
        Test.stopTest();
        
        List<Stock_Info_FLL__c> expectedResult = null;
        System.assertEquals(expectedResult, results);
    }
    
    @isTest static void testGetUsageInfoPool(){
        PlanningFLLDataFactory.createTestDataWithCoreComment();
        
        Test.startTest();
        List<PlanningReportUtilities.UsageInfoWrapper> results = PlanningReportUtilities.getUsageInformationPool('7430', 'FLL');
        Test.stopTest();
        
        integer expectedNumOfResults = 3;
        System.assertEquals(expectedNumOfResults, results.size());	//there should be three usageInfoWrappers for pool, one for each customer who used this part and one for the total
    }
    
    @isTest static void testGetUsageInfoRepAdmin(){
        PlanningFLLDataFactory.createTestDataWithCoreComment();
        
        Test.startTest();
        List<PlanningReportUtilities.UsageInfoWrapper> results = PlanningReportUtilities.getUsageInformationRepAdmin('7430', 'FLL');
        Test.stopTest();
        
        integer expectedNumOfResults = 3;
        System.assertEquals(expectedNumOfResults, results.size());	//there should be three usageInfoWrappers for Rep Admin
    }
    
    @isTest static void testGetUsageInfoEPEP(){
        PlanningFLLDataFactory.createTestDataWithCoreComment();
        
        Test.startTest();
        List<PlanningReportUtilities.UsageInfoWrapper> results = PlanningReportUtilities.getUsageInformationEPEP('7430', 'FLL');
        Test.stopTest();
        
        integer expectedNumOfResults = 3;
        System.assertEquals(expectedNumOfResults, results.size());	//there should be three usageInfoWrappers for EPEP
    }
    
    //***********************************************************************************************************************************************************
    //TEST CONTROLLER CLASS
	@isTest static void testConstuctorFLLCOM(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Test.stopTest();
        
        String expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, ctrl.SelectedTopmost);
        Integer expectedListSize = 1;
        System.assertEquals(expectedListSize, ctrl.AdvancedSearchList.Size());
        expectedListSize = 3;
        System.assertEquals(expectedListSize, ctrl.StockInfos.Size());
        expectedListSize = 4;
        System.assertEquals(expectedListSize, ctrl.RepairInfos.Size());
        System.assertEquals(expectedListSize, ctrl.POInfos.Size());
        expectedListSize = 2;
        System.assertEquals(expectedListSize, ctrl.CoreInfos.Size());
        System.assertEquals(expectedListSize, ctrl.RessInfos.Size());
    }  
    
    @isTest static void testConstuctorLBGCOM(){
        PlanningFLLDataFactory.createTestDataAndPageRefLBGCOM();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Test.stopTest();
        
        String expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, ctrl.SelectedTopmost);
        Integer expectedListSize = 1;
        System.assertEquals(expectedListSize, ctrl.AdvancedSearchList.Size());
        System.assertEquals(expectedListSize, ctrl.StockInfos.Size());
        expectedListSize = 0;
        System.assertEquals(expectedListSize, ctrl.RepairInfos.Size());
        System.assertEquals(expectedListSize, ctrl.CoreInfos.Size());
        System.assertEquals(expectedListSize, ctrl.RessInfos.Size());
    } 
    
    @isTest static void testConstuctorE2(){
        PlanningFLLDataFactory.createTestDataAndPageRefE2();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Test.stopTest();
        
        String expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, ctrl.SelectedTopmost);
        Integer expectedListSize = 1;
        System.assertEquals(expectedListSize, ctrl.AdvancedSearchList.Size());
        expectedListSize = 3;
        System.assertEquals(expectedListSize, ctrl.StockInfos.Size());
        expectedListSize = 4;
        System.assertEquals(expectedListSize, ctrl.RepairInfos.Size());
        expectedListSize = 2;
        System.assertEquals(expectedListSize, ctrl.CoreInfos.Size());
        System.assertEquals(expectedListSize, ctrl.RessInfos.Size());
    }  
    
    @isTest static void testConstuctorE2Mode(){
        PlanningFLLDataFactory.createTestDataAndPageRefE2Mode();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Test.stopTest();
        
        String expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, ctrl.SelectedTopmost);
        Integer expectedListSize = 1;
        System.assertEquals(expectedListSize, ctrl.AdvancedSearchList.Size());
        expectedListSize = 3;
        System.assertEquals(expectedListSize, ctrl.StockInfos.Size());
        expectedListSize = 4;
        System.assertEquals(expectedListSize, ctrl.RepairInfos.Size());
        System.assertEquals(expectedListSize, ctrl.POInfos.Size());
        expectedListSize = 2;
        System.assertEquals(expectedListSize, ctrl.CoreInfos.Size());
        System.assertEquals(expectedListSize, ctrl.RessInfos.Size());
    }  
    
    @isTest static void testSetCurrentCore(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrlr = new PlanningReportFLLController();
        ctrlr.CoreIndex = '0';
        ctrlr.SetCurrentCore();
        Test.stopTest();
        
        system.assert(ctrlr.CurrentCore != null);
    }
    
    @isTest static void testCreateCoreComment(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrlr = new PlanningReportFLLController();
        ctrlr.CoreIndex = '0';
        ctrlr.SetCurrentCore();
        Id coreId = ctrlr.CurrentCore.Id;	//save the core id for assert step
        ctrlr.CoreCommentBody = 'This is the body text for a new core comment';
        ctrlr.createCoreComment();
        Test.stopTest();
        
        Core_Information__c actualCore = [SELECT Core_Comment_Body__c, Notification__c FROM Core_Information__c WHERE Id=:coreId LIMIT 1];
        List<Core_Comment__c> actualComment = [SELECT Body__c, Notification__c, Timestamp__c FROM Core_Comment__c LIMIT 1];
        String expectedCommentBody = 'This is the body text for a new core comment';
        
        System.assertEquals(expectedCommentBody, actualCore.Core_Comment_Body__c);			//verify that the core object's comment body field has the expected value
        System.assert(!actualComment.isEmpty());											//verify that a comment record was created
        System.assertEquals(expectedCommentBody, actualComment[0].Body__c);					//verify that the comment record has the expected body value
        System.assertEquals(actualCore.Notification__c, actualComment[0].Notification__c);	//verify that the comment record has the notification from the core record
        System.assert(actualComment[0].Timestamp__c <= datetime.now());						//verify that the timestamp value is in the past
    }
    
    @isTest static void testCreateSecondComment_BlankBody(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrlr = new PlanningReportFLLController();
        ctrlr.CoreIndex = '0';
        ctrlr.SetCurrentCore();
        Id coreId = ctrlr.CurrentCore.Id;	//save the core id for assert step
        ctrlr.CoreCommentBody = 'This is the body text for a new core comment';
        ctrlr.createCoreComment();
        
        //create a second comment with a blank body. This will delete the first comment, but fail to insert the new comment
        ctrlr.CoreIndex = '0';
        ctrlr.SetCurrentCore();
        ctrlr.CoreCommentBody = '';
        ctrlr.createCoreComment();
        Test.stopTest();
        
        List<Core_Comment__c> actualComment = [SELECT Id FROM Core_Comment__c];
        system.assert(actualComment.isEmpty());
    }
    
    
    //CURRENT PO
    @isTest static void testSetCurrentPO(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrlrPO = new PlanningReportFLLController();
        ctrlrPO.POIndex = '0';
        ctrlrPO.SetCurrentPO();
        Test.stopTest();
        
        system.assert(ctrlrPO.CurrentPO != null);
    }
    
    
    @isTest static void testCreatePOComment(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrlrPO = new PlanningReportFLLController();
        ctrlrPO.POIndex = '0';
        ctrlrPO.SetCurrentPO();
        Id POId = ctrlrPO.CurrentPO.Id;	//save the core id for assert step
        ctrlrPO.POCommentBody = 'This is the body text for a new PO comment';
        ctrlrPO.createPOComment();
        Test.stopTest();
        
        PO__c actualPO = [SELECT PO_Comment_Body__c, Notification__c FROM PO__c WHERE Id=:POId LIMIT 1];
        List<PO_Comment__c> actualPOComment = [SELECT Body__c, Notification__c, Timestamp__c FROM PO_Comment__c LIMIT 1];
        String expectedPOCommentBody = 'This is the body text for a new PO comment';
        
   //     System.assertEquals(expectedPOCommentBody, actualPO.PO_Comment_Body__c);			//verify that the core object's comment body field has the expected value
   //     System.assert(!actualPOComment.isEmpty());											//verify that a comment record was created
    //    System.assertEquals(expectedPOCommentBody, actualPOComment[0].Body__c);					//verify that the comment record has the expected body value
     //   System.assertEquals(actualPO.Notification__c, actualPOComment[0].Notification__c);	//verify that the comment record has the notification from the core record
      //  System.assert(actualPOComment[0].Timestamp__c <= datetime.now());						//verify that the timestamp value is in the past
    }
    
    
    @isTest static void testCreateSecondPOComment_BlankBody(){
        PlanningFLLDataFactory.createTestDataAndPageRef();
        
        Test.startTest();
        PlanningReportFLLController ctrlrPO = new PlanningReportFLLController();
        ctrlrPO.POIndex = '0';
        ctrlrPO.SetCurrentPO();
        Id POId = ctrlrPO.CurrentPO.Id;	//save the core id for assert step
        ctrlrPO.POCommentBody = 'This is the body text for a new PO comment';
        ctrlrPO.createPOComment();
        
        //create a second comment with a blank body. This will delete the first comment, but fail to insert the new comment
        ctrlrPO.POIndex = '0';
        ctrlrPO.SetCurrentPO();
        ctrlrPO.POCommentBody = '';
        ctrlrPO.createPOComment();
        Test.stopTest();
        
        List<PO_Comment__c> actualPOComment = [SELECT Id FROM PO_Comment__c];
        system.assert(actualPOComment.isEmpty());
    }
    
    @isTest static void testSearchChain(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.MultiSearchInput='HEA3610E145';
        ctrl.SearchChain();
        Test.stopTest();
        
        String expectedTopmost = '7430';
        System.assertEquals(expectedTopmost, ctrl.SelectedTopmost);
    }  
    
    @isTest static void testCallRESSCount(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.CallRESSCount();
        Test.stopTest();
        
        List<Stock_Info_FLL__c> results = [SELECT RESS_Count__c, RESS_IDs__c FROM Stock_Info_FLL__c WHERE Region_and_Segment__c='FLL COM' AND Is_Summary_Record__c=true LIMIT 1];
        integer expectedResult = 2;
        string expectedRESS1 = '1111 2222';
        string expectedRESS2 = '2222 1111';
        System.assertEquals(expectedResult, results[0].RESS_Count__c);	//verify ress count is 2
        System.assert(results[0].RESS_IDs__c==expectedRESS1 || results[0].RESS_IDs__c==expectedRESS2);	//verify ress ids string
    }
    
    @isTest static void testCallRepairsInTransitCount(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.CallRepairsInTransitCount();
        Test.stopTest();
        
        List<Stock_Info_FLL__c> results = [SELECT Repairs_In_Transit__c FROM Stock_Info_FLL__c WHERE Region_and_Segment__c='FLL COM' AND Is_Summary_Record__c=true LIMIT 1];
        integer expectedResult = 1;
        System.assertEquals(expectedResult, results[0].Repairs_In_Transit__c);	//verify in transit repairs count is 1
    }
    
    @isTest static void testCallCoresInTransitCount(){
        PlanningFLLDataFactory.createTestDataWithCoreComment();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.CallCoresInTransitCount();
        Test.stopTest();
        
        List<Stock_Info_FLL__c> results = [SELECT Cores_In_Transit__c FROM Stock_Info_FLL__c WHERE Region_and_Segment__c='FLL COM' AND Is_Summary_Record__c=true LIMIT 1];
        integer expectedResult = 2;
        System.assertEquals(expectedResult, results[0].Cores_In_Transit__c);	//verify in transit cores count is 2
    }
    
    @isTest static void testGetAdvancedSearchResultsInController(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.GetAdvancedSearchResults();
        Test.stopTest();
        
        integer expectedResult = 1;
        System.assertEquals(expectedResult, ctrl.AdvancedSearchList.size());
    }
    
    @isTest static void testGoToAdvancedSearch(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        PageReference pageRef = ctrl.goToAdvancedSearch();
        Test.stopTest();
        
        System.assertEquals('/apex/planningreportsearchpage?RegionAndSegment=FLL+COM', pageRef.getUrl());
    }
    
    @isTest static void testRedirectToPlanningReport(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        PageReference pageRef = ctrl.redirectToPlanningPage();
        Test.stopTest();
        
        string expectedURL = '/apex/planning_report_fll?RegionAndSegment=FLL+COM';
        System.assertEquals(expectedURL, pageRef.getUrl());
        System.assertEquals(null, pageRef.getParameters().get('topmost'));
    }
    
    @isTest static void testRedirectToPlanningReportWithParameter(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.SelectedTopmost='7430';
        PageReference pageRef = ctrl.redirectToPlanningPage();
        Test.stopTest();
        
        System.assertEquals('7430', pageRef.getParameters().get('topmost'));
    }
    
    @isTest static void testRedirectToPlanningReportWithAnchor(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        ctrl.Anchor='Cores';
        PageReference pageRef = ctrl.redirectToPlanningPage();
        Test.stopTest();
        
        System.assertEquals('Cores', pageRef.getParameters().get('Anchor'));
    }
    
    @isTest static void testToggleCores(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Boolean before = ctrl.ShowCores;
        ctrl.toggleCores();
        Test.stopTest();
        
        System.assertEquals(!before, ctrl.ShowCores);
    }
    
    @isTest static void testTogglePartsAtRepair(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Boolean before = ctrl.ShowPartsAtRepair;
        ctrl.togglePartsAtRepair();
        Test.stopTest();
        
        System.assertEquals(!before, ctrl.ShowPartsAtRepair);
    }
    
    @isTest static void testToggleRESS(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Boolean before = ctrl.ShowRESS;
        ctrl.toggleRESS();
        Test.stopTest();
        
        System.assertEquals(!before, ctrl.ShowRESS);
    }
    
    @isTest static void testToggleQM(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Boolean before = ctrl.ShowQM;
        ctrl.toggleQM();
        Test.stopTest();
        
        System.assertEquals(!before, ctrl.ShowQM);
    }
    
    @isTest static void testTogglePO(){
        PlanningFLLDataFactory.createTestData();
        
        Test.startTest();
        PlanningReportFLLController ctrl = new PlanningReportFLLController();
        Boolean before = ctrl.ShowPO;
        ctrl.togglePO();
        Test.stopTest();
        
        System.assertEquals(!before, ctrl.ShowPO);
    }
}