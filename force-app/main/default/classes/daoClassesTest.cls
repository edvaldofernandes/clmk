/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public class daoClassesTest  
{
    static testMethod void myUnitTest() 
    {
        
        Product2 product = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
        product.Applicability__c = 'EJET';
        product.Aircraft_Family__c = 'E1';
        product.Publication_Status__c = 'Published';
        insert product;
        
        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(test.getStandardPricebookId(), product.Id);
        insert pbe;  
        
        Production_Capability__c prodCap = new Production_Capability__c(Aircraft_Model__c = product.Id,Production_Capability__c = 10,Delivery_s_Date__c = date.Today(),Unique_Validation__c = '12345678910111213141');
        insert prodCap;
        
        Task_Setup__c  taskSetup = new Task_Setup__c();    
        taskSetup.Name = 'Task Setup Tes'; 
        taskSetup.Leap_Time__c = 1; 
        taskSetup.Selected_Date__c = 'Contract Signature Date'; 
        taskSetup.Answer__c = 'Answer test';
        taskSetup.Question__c = 'All'; 
        taskSetup.Question_API__c = 'All';
        taskSetup.Before_Selected_date__c = true;
        taskSetup.Description__c = 'Test Description';
        taskSetup.Sequence__c = 997;
        taskSetup.RecordTypeId = Schema.SObjectType.Task_Setup__c.getRecordTypeInfosByName().get('Contract Milestones').getRecordTypeId();
        insert taskSetup;
        
        //PricebookEntryDao
        List<PricebookEntry> listTest1 =  PricebookEntryDao.getInstance().listByPricebookIdAndSetProductId(test.getStandardPricebookId(), new Set<String>{product.Id});
        List<PricebookEntry> listTest2 =  PricebookEntryDao.getInstance().getByPricebookId(new Set<String>{product.Id});
        
        
        //Product2Dao
        List<Product2> listProduct1 =  Product2Dao.getInstance().getListBySetId(new Set<String>{product.Id});
        List<Product2> listProduct2 =  Product2Dao.getInstance().getAllListBySetId(new Set<String>{product.Id});
        List<Product2> listProduct3 =  Product2Dao.getInstance().listPublishedBySetApplicability(new Set<String>{'EJET'});
        List<Product2> listProduct4 =  Product2Dao.getInstance().listPublishedByNotSetApplicability(new Set<String>{'ERJ'});
        List<Product2> listProduct5 =  Product2Dao.getInstance().listPublishedByNotSetApplicability(new Set<String>());
        List<Product2> listProduct6 =  Product2Dao.getInstance().getAllProducts('E1');   
        List<Product2> listProduct7 =  Product2Dao.getInstance().getAllProducts('E2');   
        List<Product2> listProduct8 =  Product2Dao.getInstance().getAllProducts('F1');          
        Product2 productTest1 =  Product2Dao.getInstance().getProductById(product.Id);
        Product2 productTest2 =  Product2Dao.getInstance().getProductPublishById(product.Id); 
        Product2 productTest3 =  Product2Dao.getInstance().getAirCraftModelById(product.Id);  
        Product2 productTest4 =  Product2Dao.getInstance().getProductByCaseId(product.Id);  
        Product2 productTest5 =  Product2Dao.getInstance().getProductById(''); 
        Product2 productTest6 =  Product2Dao.getInstance().getProductPublishById(''); 
        Product2 productTest7 =  Product2Dao.getInstance().getProductByCaseId(''); 
        Product2 productTest8 =  Product2Dao.getInstance().getAirCraftModelById(''); 
        
        
        //ProductionCapability
        List<Production_Capability__c> prodCapList1 = ProductionCapabilityDAO.getCapabilityByAircraftId(date.Today().year(), new Set<String>{product.Id});
        List<Production_Capability__c> prodCapList2 = ProductionCapabilityDAO.getCapabilityByAircraftFamily(date.Today().year(), new Set<String>{'E1','E2'});        
        List<Production_Capability__c> prodCapList3 = ProductionCapabilityDAO.getCapabilityByAircraftFamily();        
        List<AggregateResult> aggResult1 = ProductionCapabilityDAO.getProductionByYear(date.Today().year(), new Set<String>{'E1','E2'});
        List<AggregateResult> aggResult2 = ProductionCapabilityDAO.getProductionByYearPub(date.Today().year(), new Set<String>{'E1','E2'},'0');
        
        
        //SnapshotAgreementLineItem
        List<Snapshot_Agreement_Line_Item__c> snapLineItemList1 = SnapshotAgreementLineItemDAO.getLineItensBySetIds(product.Id, date.Today().year(), 'E1');
        List<Snapshot_Agreement_Line_Item__c> snapLineItemList2 = SnapshotAgreementLineItemDAO.getLineItensBySetIds(new Set<String>{product.Id},product.Id, date.Today().year());
        
        //TaskSetup
        List<Task_Setup__c> taskSetupList1 = TaskSetupDAO.getInstance().getTaskSetup();
        List<Task_Setup__c> taskSetupList2 = TaskSetupDAO.getInstance().getTaskSetupByRecordType('Contract Milestone');
        List<Task_Setup__c> taskSetupList3 = TaskSetupDAO.getInstance().getTaskSetupBySequence(new Set<Integer>{997});
        Task_Setup__c taskSetupTemp =  TaskSetupDAO.getInstance().getSObjectTaskSetup('Deleted Fields');
        Task_Setup__c taskSetupTemp1 =  TaskSetupDAO.getInstance().getSObjectTaskSetup('Deleted_Fields'); 
        
    
    }
}