/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for executing DML statements and for handling error messages.
*
* NAME: DMLExecution.cls
* AUTHOR: LRSA                                                DATE: 20/05/2014
*
*******************************************************************************/
public with sharing class DMLExecution
{
  // Método que faz o insert de um objeto
  public static boolean insertCMD( SObject aObj )
  {
    return insertCMD( new list< SObject > { aObj } ) ;
  }
  // Método que faz o insert de uma lista
  public static boolean insertCMD( list< SObject > aObjList )
  {
    if ( aObjList == null || aObjList.isEmpty() ) return false;
    return handleResult( aObjList, Database.insert( aObjList, false ) );
  }
  
  // Método que faz o update de um objeto
  public static boolean updateCMD( SObject aObj )
  {
    return updateCMD( new list< SObject > { aObj } ) ;
  }
  // Método que faz o update de uma lista
  public static boolean updateCMD( list< SObject > aObjList )
  {
    if ( aObjList == null || aObjList.isEmpty() ) return false;
    return handleResult( aObjList, Database.update( aObjList, false ) );
  }
  
  // Método que faz o delete de um objeto
  public static boolean deleteCMD( SObject aObj )
  {
    return deleteCMD( new list< SObject > { aObj } ) ;
  }
  // Método que faz o delete de uma lista
  public static boolean deleteCMD( list< SObject > aObjList )
  {
    if ( aObjList == null || aObjList.isEmpty() ) return false;
    
    for ( Integer i = ( aObjList.size() - 1 ); i >= 0; i-- )
      if ( aObjList[ i ].get( 'Id' ) == null ) aObjList.remove( i );
    
    if ( aObjList.isEmpty() ) return false;
    else return handleResult( aObjList, Database.delete( aObjList, false ) );
    
  }
  
  // Método que faz o upsert de uma lista
  public static boolean upsertCMD( list< SObject > aObjList )
  {
    if ( aObjList == null || aObjList.isEmpty() ) return false;
    return handleResult( aObjList, Database.upsert( aObjList, false ) );
  }
  
  // Método que faz o tratamento de um upsert result.
  // Deve ser usado quando o upsert é por um campo de ID Externo
  public static boolean upsertCMD( list< SObject > aObjList, list< Database.UpsertResult > aResultList )
  {
    return handleResult( aObjList, aResultList );
  }
  
  // Método que faz o tratamento da mensagem de erro. Método de insert e update
  private static boolean handleResult( list< SObject > aObjList, list< Database.SaveResult > aResultList )
  {
    if ( aObjList == null || aResultList == null ) return false;
    boolean lResult = true;
    integer lLen = aResultList.size();
    for ( integer i = 0; i < lLen; i++ )
    {
      Database.SaveResult lSaveRes = aResultList[ i ];
      if ( lSaveRes.isSuccess() ) continue;
      
      lResult = false;
      
      aObjList[ i ].addError( handleMessage( lSaveRes.getErrors() ) );
    }
    return lResult;
  }
  
  // Método que faz o tratamento da mensagem de erro. Método de delete
  private static boolean handleResult( list< SObject > aObjList, list< Database.DeleteResult > aResultList )
  {
    if ( aObjList == null || aResultList == null ) return false;
    boolean lResult = true;
    integer lLen = aResultList.size();
    for ( integer i = 0; i < lLen; i++ )
    {
      Database.DeleteResult lSaveRes = aResultList[ i ];
      if ( lSaveRes.isSuccess() ) continue;
      
      lResult = false;
      
      aObjList[ i ].addError( handleMessage( lSaveRes.getErrors() ) );
    }
    return lResult;
  }
  
  // Método que faz o tratamento da mensagem de erro. Método de upsert
  private static boolean handleResult( list< SObject > aObjList, list< Database.UpsertResult > aResultList )
  {
    if ( aObjList == null || aResultList == null ) return false;
    boolean lResult = true;
    integer lLen = aResultList.size();
    for ( integer i = 0; i < lLen; i++ )
    {
      Database.UpsertResult lSaveRes = aResultList[ i ];
      if ( lSaveRes.isSuccess() ) continue;
      
      lResult = false;
      
      aObjList[ i ].addError( handleMessage( lSaveRes.getErrors() ) );
    }
    return lResult;
  }
  
  // Método que concatena todas as mensagens de erro
  public static String handleMessage( list< Database.Error > aErrors )
  {
    if ( aErrors == null ) return '';
    String lMsgError = '';
    
    for ( Database.Error lErr : aErrors )
      lMsgError += lErr.getMessage() + '\n';
    
    return lMsgError;
  }

}