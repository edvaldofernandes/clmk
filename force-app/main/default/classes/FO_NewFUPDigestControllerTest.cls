@isTest
public class FO_NewFUPDigestControllerTest {
    /**
    * @description Test if the controller can get all updates and build response
    * to the web page.
    **/
    @isTest static void testCanSendResponse(){
        Integer N = FO_FupService.FUP_DIGEST_INTERVAL_IN_MONTHS;
        Contact contact = new Contact(
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        FUP_Update__c unexpectedUpdate = new FO_FupUpdateTestDataBuilder()
            .withStatus('Published')
            .withPublishDate(System.today().addMonths(-N))
            .withParentFup(fup)
            .build();
        
        
        FO_NewFUPDigestController controller = new FO_NewFUPDigestController();
        controller.contact = contact;
        Map<FUP__c, List<FUP__c>> actualResponse = controller.getResponse();
        System.debug('actualResponse = ' + actualResponse);
        //System.assert(!actualResponse.isEmpty());
    }
}