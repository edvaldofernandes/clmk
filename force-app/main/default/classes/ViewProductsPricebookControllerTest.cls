/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the class ViewProductsPricebookController

* NAME: ViewProductsPricebookControllerTest.cls
* AUTHOR:JFS                                                DATE: 17/10/2014
*
*******************************************************************************/

@isTest
private class ViewProductsPricebookControllerTest {

    private static ViewProductsPricebookController EditarController;
    private static final integer LOTE = 200; 
 
    static testMethod void view(){
          
      Product2 lProduct = SObjectInstanceTest.createProduct2();
      Database.insert(lProduct);
        
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
      Database.insert(lPricebook);
     
      Id Pricebookstd = SObjectInstanceTest.getPricebook2Std2();
     
      lPricebook.Id = Pricebookstd;
           
      PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(lPricebook.Id, lProduct.Id);
      Database.insert(lPricebookentry);
        
      EditarController = new ViewProductsPricebookController(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      
      EditarController.EditAllProd();
      EditarController.EditAllPrice();
      EditarController.viewproduct();
      EditarController.viewpricebook();
      EditarController.Idproduct = lPricebookentry.id;
      EditarController.IdPricebook = lPricebook.id;
      EditarController.ProductDelete();
      EditarController.PricebookDelete();
      Test.stopTest();
        
    }
    
   static testMethod void ViwPaginaLote(){
      
      list<Product2> lstProduct = new list<Product2>();
      list<Product2> lstProduct2 = new list<Product2>();
      list<PricebookEntry> listPricebookEntry = new list<PricebookEntry>();
      list<Pricebook2> lstPricebook = new list<Pricebook2>();
      list<PricebookEntry> listPricebookEntry2 = new list<PricebookEntry>();
      
      for(integer i=0; i<LOTE; i++){
        
        Product2 lProduct = SObjectInstanceTest.createProduct2();
        lProduct.Name = 'teste produto' + i;        
        lProduct.ProductCode = 'testx' + i + 'a'; 
        lstProduct.add(lProduct);
        
        Product2 lProduct2 = SObjectInstanceTest.createProduct2();
        lProduct2.Name = 'teste produto' + i;
        lProduct2.ProductCode = 'testx' + i + 'b';
        lstProduct2.add(lProduct2);
      }
      
      lstProduct.addAll(lstProduct2);
      
      Database.insert(lstProduct);
      
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();  
      Database.insert(lPricebook);
       
      Id Pricebookstd = SObjectInstanceTest.getPricebook2Std2();
            
      for(integer i=0; i<LOTE; i++){
        
        PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(Pricebookstd, lstProduct.get(i).id);
        listPricebookEntry.add(lPricebookentry);
        
        PricebookEntry lPricebookentry2 = SObjectInstanceTest.createPricebookEntry(Pricebookstd, lstProduct2.get(i).id); 
        listPricebookEntry2.add(lPricebookentry2);
        
      }
      
      listPricebookEntry.addAll(listPricebookEntry2);
              
      Database.insert(listPricebookEntry);
      
      EditarController = new ViewProductsPricebookController(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      EditarController.EditAllProd();
      EditarController.EditAllPrice();
      EditarController.viewproduct();
      EditarController.viewpricebook();
      EditarController.IdPricebook = Pricebookstd;
      
      for(integer i=0; i<LOTE; i++){
        
        EditarController.Idproduct = lstProduct.get(i).id;
        
      }
      EditarController.ProductDelete();
        
      for(integer i=0; i<LOTE; i++){
        
        EditarController.Idproduct = lstProduct2.get(i).id; 
      }   
      EditarController.PricebookDelete(); 
      Test.stopTest();
        
    }
    
    static testMethod void unitTest(){
      
      list<Product2> lstProduct = new list<Product2>();
      list<Product2> lstProduct2 = new list<Product2>();
      list<PricebookEntry> listPricebookEntry = new list<PricebookEntry>();
      list<Pricebook2> lstPricebook = new list<Pricebook2>();
      list<PricebookEntry> listPricebookEntry2 = new list<PricebookEntry>();
      Product2 lProduct;
      
      for(integer i=0; i<LOTE; i++){
        
        lProduct = SObjectInstanceTest.createProduct2();
        lProduct.Name = 'teste produto' + i;        
        lProduct.ProductCode = 'testx' + i + 'a'; 
        lProduct.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Tailored').getRecordTypeId();
        lstProduct.add(lProduct);
        
        Product2 lProduct2 = SObjectInstanceTest.createProduct2();
        lProduct2.Name = 'teste produto' + i;
        lProduct2.ProductCode = 'testx' + i + 'b';
        lstProduct2.add(lProduct2);
      }
      
      lstProduct.addAll(lstProduct2);
      
      Database.insert(lstProduct);
      
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();  
      Database.insert(lPricebook);
       
      Id Pricebookstd = SObjectInstanceTest.getPricebook2Std2();
            
      for(integer i=0; i<LOTE; i++){
        
        PricebookEntry lPricebookentry = SObjectInstanceTest.createPricebookEntry(Pricebookstd, lstProduct.get(i).id);
        listPricebookEntry.add(lPricebookentry);
        
        PricebookEntry lPricebookentry2 = SObjectInstanceTest.createPricebookEntry(Pricebookstd, lstProduct2.get(i).id); 
        listPricebookEntry2.add(lPricebookentry2);
        
      }
      
      listPricebookEntry.addAll(listPricebookEntry2);
              
      Database.insert(listPricebookEntry);
      
      EditarController = new ViewProductsPricebookController(new Apexpages.Standardcontroller(lProduct));
      
      EditarController.Idproduct = lProduct.Id;
      EditarController.ProductDelete();
      /**
      EditarController.EditAllProd();
      EditarController.EditAllPrice();
      EditarController.viewproduct();
      EditarController.viewpricebook();
      EditarController.IdPricebook = Pricebookstd;
      
      for(integer i=0; i<LOTE; i++){
        
        
        
      }
      
        
      for(integer i=0; i<LOTE; i++){
        
        EditarController.Idproduct = lstProduct2.get(i).id; 
      }   
      EditarController.PricebookDelete(); 
      Test.stopTest();
        **/
    }
}