@isTest
public class WS_ZipUtilTest {
    
    testMethod static void Testando() {
    
        Test.startTest();
        
        Case c = new Case();
        Database.insert(c);
        
        EmailMessage em = new EmailMessage(ParentId = c.Id);
        Database.insert(em);
        
        WS_ZipUtil.getAttachmentByParentId(em.Id);
        
        Attachment at = new Attachment(Name='nomeAnexo', Body=Blob.valueOf('bodyAnexo'), ContentType='.html',ParentId = em.Id);
        Database.insert(at);
        
        WS_ZipUtil.getAttachmentByParentId(at.ParentId);
        WS_ZipUtil.saveToDocument('bla','bla');
        WS_ZipUtil.saveToDocument('bla','bla');    
        
        Test.stopTest();
    
    }

}