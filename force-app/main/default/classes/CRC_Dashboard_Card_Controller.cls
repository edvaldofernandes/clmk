/*
----------------------------------------------------------------------------------------------
-- - Company:     Embraer
-- - Name:        CRC_Dashboard_Card_Controller
-- - Description: Class responsible for gathering the server information and sending them to
--				  the CRC Dashboard Card controller.
-- - @Author: Tiago de Jesus Rodrigues
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version
----------------------------------------------------------------------------------------------
-- 06/05/2019		Tiago de Jesus Rodrigues		1.0
-- 06/10/2019		André Vitor Leinio Graça		1.1
----------------------------------------------------------------------------------------------
*/

public class CRC_Dashboard_Card_Controller {

    @auraEnabled
    // Method that calls the correct service method, based on the card name.
    public static IndicatorBodyDTO getIndicatorsBody(string cardName){
		CRC_CaseService service = new CRC_CaseService();
        IndicatorBodyDTO body = new IndicatorBodyDTO();
        if ((cardName=='TOTAL')||(cardName=='AOG NFO')||(cardName=='AOG EXP')||(cardName=='SCHEDULED')){
            body = service.getCardDataByPriority(cardName);
        } else if ((cardName=='ECIP')||(cardName=='SB')||(cardName=='RECONFIG')){
            body = service.getCardDataByType(cardName);
        } else if (cardName=='ATD') {
            body = service.getATDData();
        }else if ((cardName=='BACK ORDER')||(cardName=='PRICING')||(cardName=='ENG/RTS')||(cardName=='OTHERS')) {
            body = service.getCardDataByStatus(cardName);
        }
		return body;
	}
}