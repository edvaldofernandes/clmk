public class CLMChangeShowAircraftAs {
	public static void changeShowAircraftAs(Map<Id,Agreement__c> oldRM, List<Agreement__c> newR){
        String actualvalue = '';
        Set<Id> agreementIdSet = new Set<Id>();
        for(Agreement__c agr : newR){
            Agreement__c oldAgr = oldRM.get(agr.Id);
            if (oldAgr==null) continue;
            Id pARecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByDeveloperName().get('Purchase_Agreement').getRecordTypeId();
            //if (agr.Show_All_Aircraft_As__c != oldAgr.Show_All_Aircraft_As__c){
            if ((agr.Show_All_Aircraft_As__c != oldAgr.Show_All_Aircraft_As__c)&&(agr.RecordTypeId==pARecordTypeId)){
                agreementIdSet.add(agr.Id);
            }
        }
        if (agreementIdSet.isEmpty()){
        	return;
		}
        List<Agreement_Aircraft__c> allAircraft = [SELECT Id, Show_Aircraft_As__c, Agreement__r.Show_All_Aircraft_As__c, Order_Type__c 
                                                   FROM Agreement_Aircraft__c WHERE Agreement__c IN :agreementIdSet];
		List<Agreement_Aircraft__c> listToUpdate = new List<Agreement_Aircraft__c>();
        for (Agreement_Aircraft__c ac : allAircraft){
            actualvalue = ac.Agreement__r.Show_All_Aircraft_As__c;
           	ac.Show_Aircraft_As__c = ac.Agreement__r.Show_All_Aircraft_As__c;
            listToUpdate.add(ac);
        }
        if(listToUpdate.size()>0)
            database.update(listToUpdate);
    }    
}