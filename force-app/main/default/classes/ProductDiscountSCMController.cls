public with sharing class ProductDiscountSCMController
{
    
    public List<ContractLineItem> itens { get; set; }
    public list<SelectOption> otherESolutionsOptions { get; set; }
    public list<SelectOption> contractTimeOptions { get; set; }
    public String idProductSelectedRow { get; set; }
    public String pricingSelectedRow { get; set; }
    public string erros { get; set; }
    public boolean enableSaveButton{ get; set; }
    private Map<string,decimal> mapaDescontoContract = new Map<string,decimal>(); 
    private Map<string,decimal> mapaDescontoOther = new Map<string,decimal>();
    private id idItem;  
    

    public ProductDiscountSCMController(ApexPages.StandardController controller)
    {
        
        if(!Test.isRunningTest())
            controller.addFields(new List<String>{'Service_Contract_line_item__c'});
            
        Service_Contract_Management__c scm = (Service_Contract_Management__c )controller.getRecord();
        
        idItem = scm.Service_Contract_line_item__c;
        
        inicializarComponentes();
       
    }
    
    private void inicializarComponentes()
    {
        enableSaveButton = false;
        contractTimeOptions = new List<SelectOption>();
        otherESolutionsOptions = new List<SelectOption>();
    
        otherESolutionsOptions.add(new SelectOption('--None--','--None--'));
        contractTimeOptions.add(new SelectOption('--None--','--None--'));
    
        for (Schema.PicklistEntry a : Product2.getSObjectType().getDescribe().fields.getmap().get('OthereSolutions__c').getDescribe().getPicklistValues() )
            otherESolutionsOptions.add(new SelectOption(a.getLabel(), a.getValue()));
        
        for (Schema.PicklistEntry a : Product2.getSObjectType().getDescribe().fields.getmap().get('ContractTimeDiscount__c').getDescribe().getPicklistValues() )
            contractTimeOptions.add(new SelectOption(a.getLabel(), a.getValue()));
        
        itens = new List<ContractLineItem>();
        itens = [Select UnitPrice,Catalogue_Price__c,Discount__c,Maximum_Discount__c,ContractTimeDiscount__c,PricebookEntry.Product2.ApplyContractTimeDiscount__c,OtherESolutions__c,PricebookEntry.Product2.ApplyOthereSolutions__c,PricebookEntry.Product2Id,Unit__c,Quantity,Princing__c,MaxDiscountContract__c,MaxDiscountOther__c From ContractLineItem Where Id = : idItem];
        
    }
    
    
    private void popularMapasDesconto()
    {
        for(Discount_policy__c politica : [SELECT Discount__c, eSolutionsDiscountCriteria__c, Discount_type__c  FROM Discount_policy__c where Discount_type__c In ('Other eSolutions','Contract Time')])
        {
            
            if(politica.Discount_type__c =='Contract Time')
            {
                mapaDescontoContract.put(politica.eSolutionsDiscountCriteria__c.trim(),politica.Discount__c);
            }
            else 
            {
                mapaDescontoOther.put(politica.eSolutionsDiscountCriteria__c.trim(),politica.Discount__c);
            }
        }
    }
    
    public Pagereference salvar()
    {
        for (Database.SaveResult saveResult : Database.update(itens, false)) 
        {
            if (!saveResult.isSuccess()) 
            {
                for(Database.Error err : saveResult .getErrors()) 
                    erros += 'The following error has occurred. - ' + err.getStatusCode() + ': ' + err.getMessage();
            }
    
        }
        
        enableSaveButton = false;
        
        return null;
    }
        
    
    public void updateESolutionsDiscount()
    {
        
        Decimal descontoContract = 0;
        Decimal descontoOther = 0;
        Decimal descontoESolutionsAnterior = 0;
        Decimal descontoTotal = 0;
        ContractLineItem itemSelecionado;
        
        if(mapaDescontoContract.isEmpty() || mapaDescontoOther.isEmpty())
            popularMapasDesconto();

        for ( ContractLineItem item : itens)
        {
            if(item.PricebookEntry.Product2ID == idProductSelectedRow  && item.Princing__c == pricingSelectedRow)
                 itemSelecionado = item;
        } 
        
        if(itemSelecionado != Null)
        {
        
            enableSaveButton = true;
            erros = '';
            if(mapaDescontoContract.containsKey(itemSelecionado.ContractTimeDiscount__c.trim()))
                descontoContract  += mapaDescontoContract.get(itemSelecionado.ContractTimeDiscount__c.trim());
        
            if(mapaDescontoOther.containsKey(itemSelecionado.OtherEsolutions__c.trim()))
                descontoOther  = mapaDescontoOther.get(itemSelecionado.OtherEsolutions__c.trim());
                
            descontoESolutionsAnterior = (itemSelecionado.MaxDiscountContract__c != Null  ? itemSelecionado.MaxDiscountContract__c : 0)  +  (itemSelecionado.MaxDiscountOther__c != Null  ? itemSelecionado.MaxDiscountOther__c: 0); 
            
            descontoTotal  = itemSelecionado.Maximum_Discount__c - descontoESolutionsAnterior ;
            
            descontoTotal  += descontoOther  + descontoContract;
            
            itemSelecionado.MaxDiscountOther__c = descontoOther.setScale(2)  ;
            itemSelecionado.MaxDiscountContract__c = descontoContract.setScale(2);
            itemSelecionado.Maximum_Discount__c = descontoTotal.setScale(2);
            
        }
        
    }



}