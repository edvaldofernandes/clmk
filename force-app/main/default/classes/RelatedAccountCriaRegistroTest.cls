/***************************************************************************************************
*                               Cloud2b - 2014
*---------------------------------------------------------------------------------------------------
* Classe responsável por testes unitários da trigger RelatedAccountCriaRegistro.
*
* NAME: RelatedAccountCriaRegistroTest.cls
* AUTHOR: RNM                                                                       DATE: 08/05/2014
***************************************************************************************************/
@isTest
public class RelatedAccountCriaRegistroTest {
    static final Id accAircraftRecTypeId = RecordTypeMemory.getRecType('Account', 'Aircraft_OEM');
    static final Id accOperatorRecTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
    static final Id accMRORecTypeId = RecordTypeMemory.getRecType('Account', 'MRO');
    
    
    // Testo criação de cópia do registro Related_accounts__c sendo criado.
    static testMethod void criaCopiaSucessoTest() {
    	Account originAcc = SObjectInstanceTest.createAccount(accMRORecTypeId);
    	Account targetAcc = SObjectInstanceTest.createAccount(accOperatorRecTypeId);
    	Database.insert( new Account[]{originAcc,targetAcc});
    	
    	// Defino flag para criar registro com valores invertidos
    	Related_accounts__c relatedAcc = SObjectInstanceTest.createRelatedAccount(originAcc.Id, targetAcc.Id);
    	relatedAcc.Trigger_on__c = true;
    	
    	Test.startTest();
    	
    	Database.insert( relatedAcc );
    	
    	Test.stopTest();
    	
    	// Confirmo criação de registro com valores de relacionamento com Conta invertidos
    	map<Id,Related_accounts__c> relatedAccMap = new map<Id,Related_accounts__c>(
            [select Id, Trigger_on__c from Related_accounts__c 
    	       where Origin_account__c=:targetAcc.Id and Target_account__c=:originAcc.Id]);
        system.assertEquals(1,relatedAccMap.size());
        Related_accounts__c relatedAccCopy = relatedAccMap.values()[0];
        system.assertEquals(false,relatedAccCopy.Trigger_on__c);
        
        // Confirmo que registro original teve flag alterada
        relatedAcc = [select Id, Trigger_on__c from Related_accounts__c where Id = :relatedAcc.Id];
        system.assertEquals(false,relatedAcc.Trigger_on__c);
    }
    
    // Testo não criação de cópia do registro Related_accounts__c sendo criado.
    static testMethod void criaCopiaFalhaTest() {
        Account originAcc = SObjectInstanceTest.createAccount(accMRORecTypeId);
        Account targetAcc = SObjectInstanceTest.createAccount(accOperatorRecTypeId);
        Database.insert( new Account[]{originAcc,targetAcc});
        
        // Defino flag para não criar registro com valores invertidos
        Related_accounts__c relatedAcc = SObjectInstanceTest.createRelatedAccount(originAcc.Id, targetAcc.Id);
        relatedAcc.Trigger_on__c = false;
        
        Test.startTest();
        
        Database.insert( relatedAcc );
        
        Test.stopTest();
        
        // Confirmo não criação de registro com valores de relacionamento com Conta invertidos
        map<Id,Related_accounts__c> relatedAccMap = new map<Id,Related_accounts__c>(
            [select Id, Trigger_on__c from Related_accounts__c 
               where Origin_account__c=:targetAcc.Id and Target_account__c=:originAcc.Id]);
        system.assert(relatedAccMap.isEmpty());
    }
    
    // Testo não criação de cópia do registro Related_accounts__c sendo criado.
    static testMethod void duplicarTest() {
    	Account originAcc = SObjectInstanceTest.createAccount(accMRORecTypeId);
      Account targetAcc = SObjectInstanceTest.createAccount(accOperatorRecTypeId);
      Database.insert( new Account[]{originAcc,targetAcc});
      
      // Defino flag para criar registro com valores invertidos
      Related_accounts__c relatedAcc = SObjectInstanceTest.createRelatedAccount(originAcc.Id, targetAcc.Id);
      relatedAcc.Trigger_on__c = true;
      Database.insert( relatedAcc );
      
      relatedAcc = SObjectInstanceTest.createRelatedAccount(originAcc.Id, targetAcc.Id);
      relatedAcc.Trigger_on__c = true;
      
      Test.startTest();
        
	    Database.Saveresult lSaveRes = Database.insert( relatedAcc, false );
	    
	    Test.stopTest();
	    
	    system.assert( !lSaveRes.isSuccess(), 'Deveria ter apresentado erro na trigger por duplicidade e não aconteceu' );
    }
    
    // Testo não criação de cópia do registro Related_accounts__c sendo criado.
    static testMethod void tipoDeRegistroTest() {
      Account originAcc = SObjectInstanceTest.createAccount(accAircraftRecTypeId);
      Account targetAcc = SObjectInstanceTest.createAccount(accOperatorRecTypeId);
      Database.insert( new Account[]{originAcc,targetAcc});
      
      // Defino flag para criar registro com valores invertidos
      Related_accounts__c relatedAcc = SObjectInstanceTest.createRelatedAccount(originAcc.Id, targetAcc.Id);
      relatedAcc.Trigger_on__c = true;
      
      Test.startTest();
        
      Database.Saveresult lSaveRes = Database.insert( relatedAcc, false );
      
      Test.stopTest();
      
      system.assert( !lSaveRes.isSuccess(), 'Relacionamento para esse tipo de registro não é permitido' );
    }
    
     static testMethod void unitTest()
	{
   
       	Account originAcc = SObjectInstanceTest.createAccount(accMRORecTypeId);
    	Account targetAcc = SObjectInstanceTest.createAccount(accOperatorRecTypeId);
    	Database.insert( new Account[]{originAcc,targetAcc});
    	
    	// Defino flag para criar registro com valores invertidos
    	Related_accounts__c relatedAcc = SObjectInstanceTest.createRelatedAccount(originAcc.Id, targetAcc.Id);
    	relatedAcc.Trigger_on__c = true;
    	
    	Test.startTest();
    	
    	Database.insert( relatedAcc );
    	
    	delete relatedAcc;
    	
    	Test.stopTest();
    	
      
     
    }
}