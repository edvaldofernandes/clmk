public class ESightSrcrService {
    
    private List<ESightSrcr> eSightSrcrList;
    private List<EsightSrCr__c > esightSrCrObjectList;
    private List<SrcrRankCrs__c> rankCrsObjectList;
    private List<SrcrRankSrs__c> srcrRankSrsList;
    private Map<String,String> accountIdMap;
    private IEvent iEvent;
    
    public ESightSrcrService(){}
    
    public ESightSrcrService(List<ESightSrcr> eSightSrcrList){
        
        this.eSightSrcrList = eSightSrcrList;
        
        esightSrCrObjectList = new List<EsightSrCr__c >();
        rankCrsObjectList    = new List<SrcrRankCrs__c>();
        srcrRankSrsList      = new List<SrcrRankSrs__c>();
        iEvent               = new EventTemplate();
        
        initCommonsServices();
    }
    
    private void initCommonsServices(){
        
        accountIdMap  = eSightRepository.buildMapWithAllAccount();
    }
    
    public void parseToObject(){
        
        for(ESightSrcr eSightSrcr : eSightSrcrList){
            
            EsightSrCr__c esightSrCrObj = new EsightSrCr__c();
            
            esightSrCrObj.cr__c 		   = eSightSrcr.cr;
            esightSrCrObj.family__c 	   = eSightSrcr.family;
            esightSrCrObj.operatorId__c    = eSightSrcr.operatorId;
            esightSrCrObj.operatorName__c  = eSightSrcr.operatorName;
            esightSrCrObj.rateCr__c 	   = eSightSrcr.rateCr;
            esightSrCrObj.rateSr__c        = eSightSrcr.rateSr;
            esightSrCrObj.referenceDate__c = eSightSrcr.referenceDate;
            esightSrCrObj.sr__c            = eSightSrcr.sr;
            
            buildAccountId(esightSrCrObj);
            
            esightSrCrObjectList.add(esightSrCrObj);
            RepositoryTemplate.save(parseToObjectRankCrs(eSightSrcr));
            RepositoryTemplate.save(parseToObjectRankSrs(eSightSrcr));
        }
        
        RepositoryTemplate.save(esightSrCrObjectList);    
    }
    
    public List<SrcrRankCrs__c> parseToObjectRankCrs(ESightSrcr eSightSrcr){
        
        List<SrcrRankCrs__c> rankCrsList = new List<SrcrRankCrs__c>();
        
        for(SrcrRankCrs srcrRankCrs : eSightSrcr.srcrRankCrsList){
            
            SrcrRankCrs__c rankCrs = new SrcrRankCrs__c();
            
        	rankCrs.delayContrib__c 	   =  srcrRankCrs.delayContrib;
        	rankCrs.failCode__c	 		   =  srcrRankCrs.failCode;
        	rankCrs.failCodeDescription__c =  srcrRankCrs.failCodeDescription;
        	rankCrs.qtyEvents__c 		   =  srcrRankCrs.qtyEvents;
        	rankCrs.ranking__c 			   =  srcrRankCrs.ranking;
        	rankCrs.operatorId__c 		   =  eSightSrcr.operatorId;
            
            rankCrsList.add(rankCrs);
        }
        
        return rankCrsList;
    }
    
    public List<SrcrRankSrs__c> parseToObjectRankSrs(ESightSrcr eSightSrcr){
        
        List<SrcrRankSrs__c> rankSrsList = new List<SrcrRankSrs__c>();
        
        for(RankSr rankSr : eSightSrcr.rankSrList){
            
            SrcrRankSrs__c rankSrs = new SrcrRankSrs__c();
        
            rankSrs.delayContrib__c 	   = rankSr.delayContrib;
        	rankSrs.failCode__c	 		   = rankSr.failCode;
        	rankSrs.failCodeDescription__c = rankSr.failCodeDescription;
        	rankSrs.qtyEvents__c 		   = rankSr.qtyEvents;
        	rankSrs.ranking__c 			   = rankSr.ranking;
        	rankSrs.operatorId__c 		   =  eSightSrcr.operatorId;
            
            rankSrsList.add(rankSrs);
        }
        
        return rankSrsList;
    }
    
    private void buildAccountId(EsightSrCr__c esightSrCrObj){
        
        if(accountIdMap.get(String.valueOf(esightSrCrObj.operatorId__c)) != null)
            esightSrCrObj.Account__c  = accountIdMap.get(String.valueOf(esightSrCrObj.operatorId__c));
    }
}