/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* When an opportunity is deleted or updated to closed and lost, this class
* updates the available quantity of the related slot.
*
* NAME: OpportunityUpdateSlot.cls
* AUTHOR: LRSA                                                DATE: 03/12/2014
*
*******************************************************************************/

public with sharing class OpportunityUpdateSlot {

  public static void delOpp()
  {
    TriggerUtils.assertTrigger();

    // if tem slot devolve o saldo
    list< OpportunityLineItem > lLstOppItens = [ select Id, Slot__c, Product_Type__c, quantity
      from OpportunityLineItem where OpportunityId =:( list< Opportunity > )trigger.old
      and Slot_status__c = 'Success' ];

    LineItemSearchSlot.deleteProducts( lLstOppItens );
  }

  public static void closeOpp()
  {
    TriggerUtils.assertTrigger();

    list< id > lListOppID = new list< id >();
    for ( Opportunity lOpp : ( list< Opportunity > )trigger.new )
    {
      if ( lOpp.IsClosed && !lOpp.IsWon )
        lListOppID.add( lOpp.id );
    }

    if ( lListOppID.isEmpty() ) return;

    list< OpportunityLineItem > lLstOppItens = [ select Slot__c, Product_Type__c, quantity
      from OpportunityLineItem where OpportunityId =:lListOppID and Slot_status__c = 'Success' ];

    LineItemSearchSlot.deleteProducts( lLstOppItens );
  }
}