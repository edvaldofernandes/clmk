/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class TaskTriggerHandlerTest 
{
	static testmethod void myUnitTest() 
	{
		Profile profile = [Select Id From Profile Where UserType= 'Standard' Limit 1];
    	User us = SObjectInstanceTest.createUser(profile.Id);
        insert us;

    
    	Task u = new Task();
        u.ownerId = us.Id;
        u.Subject = 'Run Test Trigger';
        u.Status = 'Not Started';
        u.Priority = 'Normal';
   	 	insert u;
   	 	
   	 	u.Status = 'Completed';
   	 	u.ActivityDate = System.Today();
   	 	u.Completion_date__c = System.Today();
   	 	u.SendClosureNotification__c = true;
   	 	u.Description = 'teste unitário';
   	 	update u;
   	 	
   	 	delete u;
   	 	
   	 	undelete u;
   	 	
   	 	TaskTriggerHandler handler = new TaskTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
       
   	 	
   
	}

}