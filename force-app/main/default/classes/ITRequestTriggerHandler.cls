/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; November-2016.
**/
public without sharing class ITRequestTriggerHandler
{

    
    public Map<Id,ITRequest__c> newRecordsMap = new Map<Id,ITRequest__c>();
    public Map<Id,ITRequest__c> oldRecordsMap = new Map<Id,ITRequest__c>(); 
    public List<ITRequest__c> newRecords = new List<ITRequest__c>();
    public List<ITRequest__c> oldRecords = new List<ITRequest__c>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public ITRequestTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        

    }

 

    public void OnAfterInsert()
    {
        checkRecordsToMailITRequest();
    }

 

    public void OnBeforeUpdate()
    {
        
    }

 

    public void OnAfterUpdate()
    {
        checkRecordsToMailITRequest();

    }

 

    public void OnBeforeDelete()
    {

        
    }

 

    public void OnAfterDelete()
    {

        
    }

 

    public void OnUndelete()
    {

        
    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }

    
    private void checkRecordsToMailITRequest()
    {

        List<ITRequest__c> listEligibleRecords = new List<ITRequest__c>();
        Map<Id,string> mapIdRequestPriorStatus = new Map<Id,string>();

        
        
        for(ITRequest__c iTRequest : this.newRecords)
        {
            if(this.IsInsert)
            {
                listEligibleRecords.add(itRequest);
            }
            else
            {
                if((itRequest.Status__c != this.oldRecordsMap.get(itRequest.Id).Status__c) || (this.oldRecordsMap.get(itRequest.Id).AssignedTo__c != itRequest.AssignedTo__c))
                {
                    listEligibleRecords.add(itRequest);    
                    mapIdRequestPriorStatus.put(itRequest.Id,this.oldRecordsMap.get(itRequest.Id).Status__c);
                }
            }
        }  
        
        if(!listEligibleRecords.isEmpty())
            sendITRequestMail(listEligibleRecords,mapIdRequestPriorStatus);
    
    }
    
    private void sendITRequestMail(List<ITRequest__c> p_ListEligibleRecords,Map<Id,string> p_MapIdRequestPriorStatus)
    {
    
        List<String> returnValue = new List<String>();
        string text = '';
        string priorStatus = '';
        string requestedOnBehalfOf = '';
        List<Messaging.SingleEmailMessage> listMessages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage emailMessage;
        Set<Id> setUsers;
        Map<String,String> mapApplicationBySupportAgentMail = new Map<String,String>();
        List<String> listSupportAgentsMail;
        String applicationArea = '';
        boolean hasMessageToSend = false;

        //Send the message to the support agents
        for(ITRequestsEmailSupportUsers__c supportUser : [Select Id,SupportAgentMail__c,RelatedApllicationAreas__c From ITRequestsEmailSupportUsers__c])
            mapApplicationBySupportAgentMail.put(supportUser.SupportAgentMail__c,supportUser.RelatedApllicationAreas__c);
        
        for(ITRequest__c iTRequest : p_ListEligibleRecords)
        {
        
            priorStatus = '';
            if(p_MapIdRequestPriorStatus.containsKey(itRequest.Id))
                priorStatus = p_MapIdRequestPriorStatus.get(itRequest.Id);    
            
            requestedOnBehalfOf = '';
            if(itRequest.RequestedOnBehalfOf__c != null)
                requestedOnBehalfOf = itRequest.RequestedOnBehalfOfName__c;     
            
            
            setUsers = new Set<Id>();
            setUsers.add(itRequest.RequestedBy__c);
            setUsers.add(itRequest.AssignedTo__c);
            
            //Insert null Id for the Support Agents
            setUsers.add(null);
            
                                    
            if(string.IsNotEmpty(requestedOnBehalfOf))
                setUsers.add(itRequest.RequestedOnBehalfOf__c);
            
            mapApplicationBySupportAgentMail = deduplicateUsers(setUsers,mapApplicationBySupportAgentMail);
            
            for(Id idUser : setUsers)
            {
                hasMessageToSend = true;
                
                if(idUser == Null)
                {
                    listSupportAgentsMail = new List<String>();
                    applicationArea = '';
                    for(String supportAgent : mapApplicationBySupportAgentMail.keySet())
                    {
                        applicationArea = mapApplicationBySupportAgentMail.get(supportAgent);
                        if(applicationArea == 'All' || applicationArea.contains(itRequest.ApplicationAreaName__c))
                            listSupportAgentsMail.add(supportAgent);    
                    }
                    hasMessageToSend = !listSupportAgentsMail.isEmpty();
                }
                
                if(hasMessageToSend)
                {
                
                    text = '';
                    text += 'Dear user, <br><br>' + (string.IsNotEmpty(priorStatus) ? 'An' : 'A new') + '  IT request have just been';
                    
                    if(itRequest.RequestedOnBehalfOf__c == idUser && idUser != Null)
                    {
                        text += (string.IsNotEmpty(priorStatus) ? ' updated' : ' requested') + '  by ' + itRequest.RequestedByName__c + ' in your behalf.';
                    }
                    else if(itRequest.RequestedBy__c == idUser && idUser != Null)
                    {
                        if(itRequest.LastModifiedById == idUser)
                        {
                            text += (string.IsNotEmpty(priorStatus) ? ' updated' : ' requested')  + '  by you.' ;               
                        }
                        else
                        {
                            text += (string.IsNotEmpty(priorStatus) ? ' updated' : ' requested')  + '  by ' + itRequest.LastModifiedByName__c + ' in your name.' ;               
                        }
                    }
                    else if(itRequest.AssignedTo__c == idUser && idUser != Null)
                    {
                        text += ' assigned to you.' ;               
                    }
                    else if(idUser == Null)
                    {
                        text+= (string.IsNotEmpty(priorStatus) ? ' updated.' : ' requested.');
                    }        
                    text+= '<br><br>';
                    
                    text += 'Request : <a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+itRequest.Id+'">'+ itRequest.RequestID__c + ' - ' + itRequest.Name  + '</a> <br>';
                    text += 'Type : ' +  itRequest.RequestType__c +'<br>';
                    
                    text += 'Application Area : ' +  itRequest.ApplicationAreaName__c  + '<br>';
                    text += 'Requested By : ' +  itRequest.RequestedByName__c + '<br>';
                
                    if(string.IsNotEmpty(requestedOnBehalfOf))
                        text += 'Requested on Behalf of  : ' +  requestedOnBehalfOf + '<br>';
                
                    text += 'Assigned to : ' +  itRequest.AssignedToName__c   + '<br>';
                    text += 'Current status : ' +  itRequest.Status__c   + '<br>';
                
                    if(string.IsNotEmpty(priorStatus))
                        text += 'Prior status : ' +  priorStatus   + '<br>';
                        
                    if(string.IsNotEmpty(itRequest.ResolvedBy__c))
                        text += 'Resolved By : ' +  itRequest.ResolvedByName__c   + '<br>';
                    
                    text += 'Description : ' +  itRequest.Description__c +'<br>';
                
                    text += '<br>Please check.' ;
                    text += '<br>Kind regards.' ;
                    
                    
                    emailMessage = new Messaging.SingleEmailMessage();
                    emailMessage.setSenderDisplayName('CRM - Commercial Aviation IT Support');
                    emailMessage.setSubject((string.IsNotEmpty(priorStatus) ? 'IT Request updated' : 'New IT Request'));
                    emailMessage.setHtmlBody(text);
                    emailMessage.setSaveAsActivity(false);
                    System.Debug('idUser ' + idUser );
                    if(idUser == null)
                    {
                        emailMessage.setToAddresses(listSupportAgentsMail);
                        System.Debug('setToAddresses ' + listSupportAgentsMail);
                    }
                    else
                    {
                        emailMessage.setTargetObjectId(idUser);
                    }
                    listMessages.add(emailMessage);
                    System.Debug(idUser + ' - Id do User');
                }                    
            }
            
        }
        
        
        if(!listMessages.isEmpty())    
        {
            
            for(Messaging.SendEmailResult result : Messaging.sendEmail(listMessages))
            {
                for(Messaging.SendEmailError erro : result.getErrors())
                    System.Debug(erro.getMessage());
            }

        }      
        return;
    
    
    }
    
    private Map<String,String> deduplicateUsers(set<Id> p_SetUsers,Map<String,String> p_MapSupportAgentsMail)
    {
        for(User userTemp : [Select email From User Where Id in :  p_SetUsers])
        {
           if(p_MapSupportAgentsMail.containsKey(userTemp.email))
                p_MapSupportAgentsMail.remove(userTemp.email);
        }
        
        return  p_MapSupportAgentsMail;      
    }
    
}