/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Class for rollup Aircraft and update the parent object       
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
public class TriggerRollupAircraft {
	
    public List<Aircraft__c> listAircraft;
    private GEN_LREngine.Context ctx;
    private GEN_LREngine.RollupSummaryField aggregateField;
    
    private static final String IN_SERVICE_EMBRAER = 'Aircraft_Status__c = \'In Service\' AND RecordType.Name = \'Embraer\'';
    private static final String AIRCRAFT_EMBRAER = 'RecordType.Name = \'Embraer\'';
    private static final String AIRCRAFT_NON_EMBRAER = 'RecordType.Name <> \'Embraer\'';
        
    /**
    * @description : Constructor for TriggerRollupAircraft
    * @param List<Aircraft__c> listAircraft : List of aircraft in the trigger execution
    **/
    public TriggerRollupAircraft(List<Aircraft__c> listAircraft){
        this.listAircraft = listAircraft;
		
        executeAccountRollup();
    }
    
    /**
    * @description : Execute and persist the rollup for all account fields with their bussines rule
	* @return void
    **/
    private void executeAccountRollup(){
        Map<String, Account> mapIdAccountByAccount = new Map<String, Account>();     
        List<Account> listAccount = new List<Account>();
        
        //Add rollup Account Operator Aircraft Embraer In Service
        aggregateField = new GEN_LREngine.RollupSummaryField(Schema.SObjectType.Account.fields.Embraer_a_c_in_service_only__c,
                                           							 						  Schema.SObjectType.Aircraft__c.fields.Id,
                                           							 						  GEN_LREngine.RollupOperation.Count);
        
        ctx = new GEN_LREngine.Context(Account.SobjectType, 
                                       Aircraft__c.SobjectType,
                                       Schema.SObjectType.Aircraft__c.fields.Operator__c,
                                       IN_SERVICE_EMBRAER,
                                       aggregateField
                                       );
        
        listAccount.addAll((List<Account>) GEN_LREngine.rollUp(ctx, listAircraft));
        
         //Add rollup Account Operator Aircraft Embraer
        aggregateField.master =  Schema.SObjectType.Account.fields.Quantity_Embraer_Aircraft_Operator__c;
        ctx.detailWhereClause = AIRCRAFT_EMBRAER;
        
        listAccount.addAll((List<Account>) GEN_LREngine.rollUp(ctx, listAircraft));
        
        //Add rollup Account Operator Aircraft Non Embraer
        aggregateField.master =  Schema.SObjectType.Account.fields.Quantity_Non_Embraer_Aircraft_Operator__c;
        ctx.detailWhereClause = AIRCRAFT_NON_EMBRAER;
        
        listAccount.addAll((List<Account>) GEN_LREngine.rollUp(ctx, listAircraft));
        
        //Add rollup Account Owner Aircraft Embraer
        aggregateField.master =  Schema.SObjectType.Account.fields.Quantity_Embraer_Aircraft_Owner__c;
        ctx.lookupField = Schema.SObjectType.Aircraft__c.fields.Owner__c;
        ctx.detailWhereClause = AIRCRAFT_EMBRAER;
        
        listAccount.addAll((List<Account>) GEN_LREngine.rollUp(ctx, listAircraft));
        
        //Add rollup Account Owner Aircraft Non Embraer
        aggregateField.master =  Schema.SObjectType.Account.fields.Quantity_Non_Embraer_Aircraft_Owner__c;
        ctx.detailWhereClause = AIRCRAFT_NON_EMBRAER;
        
        listAccount.addAll((List<Account>) GEN_LREngine.rollUp(ctx, listAircraft));
        

        for(Account obj : listAccount){
            if(mapIdAccountByAccount.containsKey(obj.Id)){
                if(obj.Embraer_a_c_in_service_only__c != null){mapIdAccountByAccount.get(obj.Id).Embraer_a_c_in_service_only__c = obj.Embraer_a_c_in_service_only__c;}
                if(obj.Quantity_Embraer_Aircraft_Owner__c != null){mapIdAccountByAccount.get(obj.Id).Quantity_Embraer_Aircraft_Owner__c = obj.Quantity_Embraer_Aircraft_Owner__c;}
                if(obj.Quantity_Non_Embraer_Aircraft_Owner__c != null){mapIdAccountByAccount.get(obj.Id).Quantity_Non_Embraer_Aircraft_Owner__c = obj.Quantity_Non_Embraer_Aircraft_Owner__c;}
                if(obj.Quantity_Embraer_Aircraft_Operator__c != null){mapIdAccountByAccount.get(obj.Id).Quantity_Embraer_Aircraft_Operator__c = obj.Quantity_Embraer_Aircraft_Operator__c;}
                if(obj.Quantity_Non_Embraer_Aircraft_Operator__c != null){mapIdAccountByAccount.get(obj.Id).Quantity_Non_Embraer_Aircraft_Operator__c = obj.Quantity_Non_Embraer_Aircraft_Operator__c;}
                
            }else{
                mapIdAccountByAccount.put(obj.Id, obj);
            }
        }

        system.debug('TriggerRollupAircraft.mapIdAccountByAccount.values(): ' + mapIdAccountByAccount.values());
        if(mapIdAccountByAccount.values().size() > 0){GEN_TriggerHelper.updateObjectListTriggerDisabled(mapIdAccountByAccount.values());}
    }
}