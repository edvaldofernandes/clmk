global class ESightFosInfo {
    
    webservice long aircraftsDelivered;
    webservice long aircraftsInService;
    webservice Decimal averageFlightTime;
    webservice long dailyUtilizationCycles;
    webservice Decimal dailyUtilizationHours;
    webservice String family;
    webservice String fleetLeaderCycles;
    webservice String fleetLeaderHours;
    webservice DateTime lastUpdate;
    webservice long leaderCycles;
    webservice Decimal leaderHours;
    webservice long operatorId;
    webservice String operatorName;
    webservice DateTime referenceDate;
    webservice long totalFlightCycles;
    webservice Decimal totalFlightHours;
    
    public ESightFosInfo(){}
}