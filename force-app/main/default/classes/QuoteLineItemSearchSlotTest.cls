/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class QuoteLineItemSearchSlot
* NAME: QuoteLineItemSearchSlotTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*
*******************************************************************************/

@isTest
private class QuoteLineItemSearchSlotTest {

    private static final integer QTD_LOTE = 200;
  
    private static Slots__c fSlot;
  
    private static QuoteLineItem newOppItem() {
      Id lPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      Opportunity lOpportunity = SObjectInstanceTest.createOpportunity();
      insert lOpportunity;
      
      Quote lOpp = SObjectInstanceTest.createQuote(lOpportunity.Id);
      lOpp.Pricebook2Id = lPB;
      insert lOpp;
      
      Product2 lProd = SObjectInstanceTest.createProduct2();
      lProd.RecordTypeId = RecordTypeMemory.getRecType( 'Product2', 'Training' );
      lProd.Product_Type__c = 'Training';
      insert lProd;      
      
      PricebookEntry lPBE = SObjectInstanceTest.createPricebookEntry( lPB, lProd.id );
      insert lPBE;
      
      fSlot = SObjectInstanceTest.createSlot();
      insert fSlot;
      
      fSlot = [ select Avalilable_quantity__c from Slots__c where id=:fSlot.id ];

      QuoteLineItem sobj = SObjectInstanceTest.createQuoteLineItem( lOpp.id, lPBE.id );
      return sobj;
    }
    
    static testMethod void addItem()
    { 
      QuoteLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      
      Test.StartTest();
      insert lOppItem;
      Test.StopTest();
      
    }
    
    static testMethod void changeItem()
    { 
      QuoteLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      insert lOppItem;
      
      Slots__c fSlot2 = SObjectInstanceTest.createSlot();
      insert fSlot2;
      
      fSlot2 = [ select id, Avalilable_quantity__c from Slots__c where id=:fSlot2.id ];
      
      lOppItem.Slot__c = fSlot2.id; 
      
      Test.StartTest();
      update lOppItem;
      Test.StopTest();
      
    }
    
    static testMethod void deleteItem()
    { 
      QuoteLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      insert lOppItem;
      
      Test.StartTest();
      delete lOppItem;
      Test.StopTest();
           
    }
    
    static testMethod void semSlot()
    { 
      QuoteLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today().addDays(40); 
      lOppItem.End_date__c = System.today().addDays( 50 );
      
      Test.StartTest();
      Database.SaveResult lResult = database.insert( lOppItem, false );
      Test.StopTest();
      
    }
    
    static testMethod void maisSlot()
    { 
      Slots__c fSlot2 = SObjectInstanceTest.createSlot();
      insert fSlot2;
      
      QuoteLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      
      Test.StartTest();
      Database.SaveResult lResult = database.insert( lOppItem, false );
      Test.StopTest();
      
    }
    
    static testMethod void semQtt()
    { 
      QuoteLineItem lOppItem = newOppItem();
      lOppItem.Start_date__c = System.today(); 
      lOppItem.End_date__c = System.today().addDays( 3 );
      
      fSlot.Quantity__c = 0;
      update fSlot;
      
      Test.StartTest();
      Database.SaveResult lResult = database.insert( lOppItem, false );
      Test.StopTest();
      
    }
    
    static testMethod void lote()
    { 
      Id lPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      Opportunity lOpportunity = SObjectInstanceTest.createOpportunity();
      insert lOpportunity;
      
      Quote lOpp = SObjectInstanceTest.createQuote(lOpportunity.Id);
      lOpp.Pricebook2Id = lPB;
      insert lOpp;
      
      Product2 lProd = SObjectInstanceTest.createProduct2();
      lProd.RecordTypeId = RecordTypeMemory.getRecType( 'Product2', 'Training' );
      lProd.Product_Type__c = 'Training';
      insert lProd;
            
      PricebookEntry lPBE = SObjectInstanceTest.createPricebookEntry( lPB, lProd.id );
      insert lPBE;
      
      fSlot = SObjectInstanceTest.createSlot();
      fSlot.Quantity__c = QTD_LOTE;
      fSlot.Start_date__c = System.today().addDays( 100 );
      fSlot.End_date__c = System.today().addDays( 110 );
      insert fSlot;
      
      fSlot = [ select Avalilable_quantity__c from Slots__c where id=:fSlot.id ];
      
      list< QuoteLineItem > lLstOppItem = new list< QuoteLineItem >();
      for ( integer i=0; i<QTD_LOTE; i++ )
      {
        QuoteLineItem lOppItem = SObjectInstanceTest.createQuoteLineItem( lOpp.id, lPBE.id );
        lOppItem.Start_date__c = System.today().addDays( 100 ); 
        lOppItem.End_date__c = System.today().addDays( 103 );
        lLstOppItem.add( lOppItem );
      }
      
      Test.StartTest();
      insert lLstOppItem;
      Test.StopTest();
      
    }

}