public class RcpTemplate implements IRCPService{
	
    public static void save(List<sObject> obj){
        
        IEvent iEvent = new EventTemplate();
        System.debug('SIZE OF LIST ' + obj.size());
        if(obj != null && obj.size() > 0){
            try{
                
                insert obj;
                iEvent.buildInfoEvent(obj, EventQueueStatusType.SUCCESS.name(), null);
            
            }catch(Dmlexception ex){

                errorEvent(iEvent, obj, ex.getMessage());
                
            }catch(Exception ex){

                errorEvent(iEvent, obj, ex.getMessage());
            }
        }   
    }
    
    private static void errorEvent(IEvent iEvent, List<sObject> obj, String error){
        
        iEvent.buildInfoEvent(obj,EventQueueStatusType.ERROR.name(), error);
    }
}