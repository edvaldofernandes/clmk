/**
* Controller class for the visualforce page CFCProductEdit.
* Name: CFCProductEditController
* @author - elvsantos@deloitte.com
* @version 1.0 - 10/12/2015
**/ 
public with sharing class CFCProductEditController 
{
	public Product2 fieldProduct{get; set;}
	
	public CFCProductEditController(ApexPages.StandardController stdController)
	{
		this.fieldProduct = (Product2)stdController.getRecord();
		fieldProduct = [Select Name,Publication_Status__c,Pardot_Status__c, Custom_Redirect_Pardot__c,Favorite_Custom_Redirect_Pardot__c,Print_Custom_Redirect_Pardot__c From Product2 Where Id = :fieldProduct.Id ];
        system.debug('CFCProductEditController.fieldProduct >>> ' + fieldProduct);
	}
	
    public pageReference validatePublication()
    {
        system.debug('CFCProductEditController.validatePublication()');
        PageReference page;      
        
        if(fieldProduct.Publication_Status__c!= null && fieldProduct.Publication_Status__c.equals('Published'))
        {            
            return null;
        } else{            
            page = new PageReference('/'+ fieldProduct.Id + '/e?nooverride=1&retURL=%2F'+fieldProduct.Id);       
            return page;
        }
    }
    
    public PageReference saveProduct(){
        system.debug('CFCProductEditController.saveProduct()');
        system.debug('CFCProductEditController.saveProduct.fieldProduct >>> ' + fieldProduct);
        
        update fieldProduct;
        
        PageReference page = new PageReference('/'+ fieldProduct.Id); 
        return page;        
    }
    
}