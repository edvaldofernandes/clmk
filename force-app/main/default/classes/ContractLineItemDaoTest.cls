@isTest
public class ContractLineItemDaoTest
{
        static testMethod void myUnitTest() 
        {
            RecordType recordType = [Select Name, Id From RecordType Where SobjectType = 'Task_Setup__c' And DeveloperName = 'Agreement' limit 1];
            RecordType recordType1 = [Select Name, Id From RecordType Where SobjectType = 'Agreement__c' And DeveloperName = 'Purchase_Agreement' limit 1];
            RecordType recordType2 = [Select Name, Id From RecordType Where SobjectType = 'Task_Setup__c' And DeveloperName = 'Deleted_Fields' limit 1];
                 
            Product2 prod = new Product2();
            prod.Name = 'Produto Teste';
            prod.ProductCode = 'a488';
            prod.Product_Status__c = 'Active';
            prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId();
            prod.IsActive = true;
            //AgreementLineItemTriggerHandler.isRecursive = false;
            database.insert(prod);
           
            id pb2 = SObjectInstanceTest.getPricebook2Std2();
            
            PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2, Prod.Id);
            //AgreementLineItemTriggerHandler.isRecursive = false;
            database.insert(pbe);  
            
            Account account = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
            
            Agreement__c apttusAPTSAgreement = new Agreement__c();
            apttusAPTSAgreement.Name = 'Teste 1';
            apttusAPTSAgreement.Account__c = account.Id;
            apttusAPTSAgreement.Country__c = 'Brazil';
            apttusAPTSAgreement.Agreement_Code__c = 'ABCDEFG';
            apttusAPTSAgreement.Region_Code__c = 'RegionCODE';
            apttusAPTSAgreement.Status_Category__c = 'PA Signed';
            apttusAPTSAgreement.RecordTypeId = recordType1.id;
            apttusAPTSAgreement.RecordType = recordType1;
            apttusAPTSAgreement.RecordType.Name = recordType1.name;
            apttusAPTSAgreement.Company_Signed_Date__c = Date.today(); 
            apttusAPTSAgreement.Kickoff_trigger_already_executed__c = false; 
            //apttusAPTSAgreement.Admin_Mode__c = 'true';
            //AgreementLineItemTriggerHandler.isRecursive = false;
            System.debug('apttusAPTSAgreement.id: ' + apttusAPTSAgreement);
            insert apttusAPTSAgreement;
            
            Aircraft_Price__c price = new Aircraft_Price__c();
            price.Model__c = Prod.Id;
            price.Aircraft_Version__c = 'IGW';
            price.Economic_Condition__c = '132131';
            price.Commercial_Agreement__c = apttusAPTSAgreement.Id;
            price.Aircraft_List_Price__c = 32131231.00;
            price.Aircraft_Basic_Price__c = 32141212321.00;
            insert price;
                    
            Agreement_Aircraft__c apttusAgreementLineItem = new Agreement_Aircraft__c();
            apttusAgreementLineItem.Aircraft__c  = Prod.Id;
            apttusAgreementLineItem.Contractual_Delivery_Month__c = Date.today();
            apttusAgreementLineItem.Contract_Aircraft_Number__c = 5;
            apttusAgreementLineItem.Order_Type__c = 'Firm';
            apttusAgreementLineItem.Status__c = 'Planned';
            apttusAgreementLineItem.Basic_Price__c = 200;
            apttusAgreementLineItem.Aircraft_Configuration__c = 'AK';
            apttusAgreementLineItem.Agreement__c = apttusAPTSAgreement.Id;
            apttusAgreementLineItem.Aircraft_Price__c = price.Id;
/*
            apttusAgreementLineItem.Agreement__r.Agreement_Code__c = apttusAPTSAgreement.Id;
            apttusAgreementLineItem.Agreement__r.Status_Category__c = apttusAPTSAgreement.Status_Category__c;
            apttusAgreementLineItem.Agreement__r.RecordTypeId = apttusAPTSAgreement.RecordTypeId;
            apttusAgreementLineItem.Agreement__r.RecordType = apttusAPTSAgreement.RecordType;
            apttusAgreementLineItem.Agreement__r.RecordType.Name = apttusAPTSAgreement.RecordType.Name;
            */
            //AgreementLineItemTriggerHandler.isRecursive = false;
            insert apttusAgreementLineItem; 
            
            List<ContractLineItem> listItens = ContractLineItemDao.getInstance().listBySetServiceContract(new Set<String>{apttusAgreementLineItem.id});
            

    }
}