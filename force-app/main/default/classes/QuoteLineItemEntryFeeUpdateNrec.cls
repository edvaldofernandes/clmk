/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* When Quote Line Item Entry Fee is deleted, the field Entry Fee in 
* Quote Line Item NREC must be zero.
*
* NAME: QuoteLineItemEntryFeeUpdateNrec.cls
* AUTHOR: AFC                                                  DATE: 24/03/2015
*******************************************************************************/
public with sharing class QuoteLineItemEntryFeeUpdateNrec 
{
  public static void clearEntryFee()
  {
    TriggerUtils.assertTrigger();
    
    set< id > setPbe = new set< id >();
    set< id > setQuote = new set< id >();
    
    for( QuoteLineItem lineItemDeleted : ( list< QuoteLineItem > )trigger.old  )
    {     
      if( lineItemDeleted.Princing__c == 'Entry fee' )
      {
        setPbe.add( lineItemDeleted.PricebookEntryId );
        setQuote.add( lineItemDeleted.QuoteId );
      }
    }    
    if( setQuote.isEmpty() ) return;
        
    list< QuoteLineItem > lstLineItemNrec = new list< QuoteLineItem >( [  
      SELECT Id, Princing__c, PricebookEntryId, QuoteId, Entry_fee__c
      FROM QuoteLineItem WHERE Princing__c =: 'NREC'
      AND PricebookEntryId =: setPbe 
      AND QuoteId =: setQuote ] );
      
    if ( lstLineItemNrec.isEmpty() ) return;
    
    for( QuoteLineItem lineItemNrec : lstLineItemNrec )
    {       
      lineItemNrec.Entry_fee__c = 0;
    }
    update lstLineItemNrec;
  }
}