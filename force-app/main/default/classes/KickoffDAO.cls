/* Classe implementadora de SOBjectDAO para operações DML no objeto  Kickoff__c, utilizando pattern de Singleton.
* @author - Elvia Serpa
*			elvsantos@deloitte.com
* @version 1.0 27/01/2016
*/
public with sharing class KickoffDAO {
    
    private static final KickoffDAO instance = new KickoffDAO();    
    
    private KickoffDAO(){
    }    
    
    public static KickoffDAO getInstance(){
        return instance;
    }
    
    public List<Kickoff__c> getKickoff(Set<String> setAgreement){
        System.debug('KickoffDAO getKickoff setAgreement : ' + setAgreement);
        List<Kickoff__c> lstKickoff = [Select 
                                       Id , 
                                       Opportunity__c 
                                       From 
                                       Kickoff__c 
                                       Where 
                                       Opportunity__c IN : setAgreement];
        System.debug('KickoffDAO getKickoff lstKickoff : ' + lstKickoff);
        return lstKickoff;        
    }     
}