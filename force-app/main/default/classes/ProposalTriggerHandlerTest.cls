@isTest
public class ProposalTriggerHandlerTest{
    @isTest
    public static void testWithoutHistory()
    {
        RecordType recordTypeDocument = [ Select Name, Id From RecordType Where SobjectType = 'Attachments__c' And DeveloperName = 'Billboards' limit 1 ];
        RecordType recordTypeProduct = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        RecordType recordTypeProposal = [ Select Name, Id From RecordType Where SobjectType = 'Proposal__c' And DeveloperName = 'Training' limit 1 ];
        Id standardPB = Test.getStandardPricebookId();
        system.debug('PRICEBOOK>>> '+standardPB);
        
            Product2 prod2 = new Product2();
            prod2.Name = 'Teste Produto';
            prod2.Applicability__c = 'ERJ';
            prod2.RecordTypeId = recordTypeProduct.Id;
            insert prod2;
            
            Pricebook2 price2 = new Pricebook2();
            price2.Name = 'Standard Price Book';
            price2.IsActive = true;
            insert price2;
        	system.debug('PRICEBOOK2>>>' +price2);
            
            PricebookEntry price = new PricebookEntry();
            price.Pricebook2Id = standardPB;        
            price.Product2Id = prod2.Id;
            price.UnitPrice = 9999;
            price.IsActive = true;
            price.UseStandardPrice = false;
            insert price;
            
            Proposal__c proposalCustom = new Proposal__c();
            proposalCustom.RecordTypeId = recordTypeProposal.Id;
            proposalCustom.Email__c = '';
            proposalCustom.Aircraft_Type__c = 'EMB'; 
            proposalCustom.Aircraft_Model__c = 'EMB110';
            proposalCustom.Opportunity_Create__c = FALSE;
            
            proposalCustom.ID__c = '1';
            insert proposalCustom;
            system.debug('IDPROPOSAL>>>' +proposalCustom);
            proposalCustom.ID__c = '2';
           
            Proposal_Items__c proposalItems = new Proposal_Items__c();
            proposalItems.Proposal__c = proposalCustom.Id;
            proposalItems.Product__c = prod2.Id;
            insert proposalItems;
            
            Attachment attDefault = new Attachment();
            attDefault.Name = 'Test Attachment';
            Blob bodyBlob = Blob.valueOf('Test Attachment Body');
            attDefault.body = bodyBlob;
            attDefault.parentId = proposalCustom.Id;
            System.debug('attDefault ' + attDefault);
            insert attDefault;
            
            system.debug('ATTDEFAULT+++' +attDefault);
            
            List<Attachment> attachments = [Select Id, Name From Attachment Where ParentId =: proposalCustom.id];
            system.debug('LISTATTACHMENTS+++'+attachments.size());
	        System.assertEquals(1, attachments.size());
        
			//proposalCustom.OwnerId = userOwnerId.ID;
            //proposalCustom.Email__c = 'teste@teste.com';
            //proposalCustom.Aircraft_Model__c = 'EMB120';
           // update proposalCustom;
           // 
            update proposalCustom;
           
    }
    
    @isTest
    public static void testWithHistory()
    {
        RecordType recordTypeDocument = [ Select Name, Id From RecordType Where SobjectType = 'Attachments__c' And DeveloperName = 'Billboards' limit 1 ];
        RecordType recordTypeProduct = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        RecordType recordTypeProposal = [ Select Name, Id From RecordType Where SobjectType = 'Proposal__c' And DeveloperName = 'Training' limit 1 ];
        Id standardPB = Test.getStandardPricebookId();
       
            Product2 prod2 = new Product2();
            prod2.Name = 'Teste Produto';
            prod2.Applicability__c = 'ERJ';
            prod2.RecordTypeId = recordTypeProduct.Id;
            insert prod2;
            
            Pricebook2 price2 = new Pricebook2();
            price2.Name = 'Standard Price Book';
            price2.IsActive = true;
            insert price2;
            
            PricebookEntry price = new PricebookEntry();
            price.Pricebook2Id = standardPB;        
            price.Product2Id = prod2.Id;
            price.UnitPrice = 9999;
            price.IsActive = true;
            price.UseStandardPrice = false;
            insert price;
            
            Proposal__c proposalCustom = new Proposal__c();
            proposalCustom.RecordTypeId = recordTypeProposal.Id;
            proposalCustom.Email__c = '';
            proposalCustom.Aircraft_Type__c = 'ERJ'; 
            proposalCustom.Aircraft_Model__c = 'E170';
            proposalCustom.Opportunity_Create__c = FALSE;
            
            
            proposalCustom.ID__c =  null;
            insert proposalCustom;
            
            system.debug('PROPOSALCUSTOM__C+++' +proposalCustom);
            
            Proposal_Items__c proposalItems = new Proposal_Items__c();
            proposalItems.Proposal__c = proposalCustom.Id;
            proposalItems.Product__c = prod2.Id;
            insert proposalItems;
            
            Proposal_Items__c proposalItems2 = new Proposal_Items__c();
            proposalItems2.Proposal__c = proposalCustom.Id;
            proposalItems2.Product__c = prod2.Id;
            insert proposalItems2;
            
            Attachment attDefault = new Attachment();
            attDefault.Name = 'Test Attachment';
            Blob bodyBlob = Blob.valueOf('Test Attachment Body');
            attDefault.body = bodyBlob;
            attDefault.parentId = proposalCustom.Id;
            System.debug('attDefault ' + attDefault);
            insert attDefault;
            
            List<Attachment> attachments = [Select Id, Name From Attachment Where ParentId =: proposalCustom.id];
	        System.assertEquals(1, attachments.size());
        
            Opportunity opp = SObjectInstanceTest.createOpportunity();
            database.insert(opp);
        
            OpportunityLineItem Oli = SObjectInstanceTest.createOppItem(opp.Id, price.Id);
            Oli.Sales_Price__c = Oli.TotalPrice;
            database.insert(Oli);
            
            proposalCustom.Email__c = 'teste@teste.com';
        	update proposalCustom;
            
            ProposalTriggerHandler controller = new ProposalTriggerHandler(true);
            controller.OnBeforeDelete();
            controller.OnAfterDelete();
            controller.OnUndelete();
            boolean teste = controller.IsTriggerContext;
        	system.assert(controller != null); 
    }
}