/**
* @author Marcilio Leite de Souza
* @date 06/05/2019
* @description: Test class for CLMMigration
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           06 MAY 2019             Original Version
**/
@IsTest 
public with sharing class CLMMigrationTest {

   static testMethod void checkMigrationTest(){
       List<Agreement__c> listAgg = new List<Agreement__c>();
       
       for(Apttus__APTS_Agreement__c obj : database.query(CLMMigration.apttusAgree)){
           Agreement__c agg = CLMMigration.apttusToCommercial(obj);
           listAgg.add(agg);
       }
       system.debug('listAgg: ' + listAgg);
       
       List<Agreement_Aircraft__c> listAircraft = new List<Agreement_Aircraft__c>();
       system.debug(CLMMigration.apttusAircraft);
       for(Apttus__AgreementLineItem__c obj : database.query(CLMMigration.apttusAircraft)){
           Agreement_Aircraft__c aggAir = CLMMigration.apttusToCommercialAircraft(obj);
           listAircraft.add(aggAir);
       }
       
       CLMMigration.CLMMigrationErroModel migErro = new CLMMigration.CLMMigrationErroModel();
       migErro.recordId = 'test';
       migErro.erroCode = System.StatusCode.ALREADY_IN_PROCESS;
       migErro.erroMsg = 'test';
       migErro.listFields = new List<String>{'test'};
   }
    

   @TestSetup
   static void makeData(){
        RecordType recordType1 = [Select Name, Id From RecordType Where SobjectType = 'Apttus__APTS_Agreement__c' And DeveloperName = 'Purchase_Agreement' limit 1];
        RecordType recordType3 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
             
        Product2 Produto = new Product2();
        Produto.Name = 'Produto Teste';
        Produto.ProductCode = 'a488';
        Produto.Product_Status__c = 'Active';
        Produto.RecordTypeId = recordType3.Id;
        Produto.IsActive = true;
        //AgreementLineItemTriggerHandler.isRecursive = false;
        database.insert(Produto);
       
        id pb2 = SObjectInstanceTest.getPricebook2Std2();
        
        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2, Produto.Id);
        //AgreementLineItemTriggerHandler.isRecursive = false;
        database.insert(pbe);  
        
        Account account = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        
        Apttus__APTS_Agreement__c apttusAPTSAgreement = new Apttus__APTS_Agreement__c();
        apttusAPTSAgreement.Name = 'Teste 1';
        apttusAPTSAgreement.Apttus__Account__c = account.Id;
        apttusAPTSAgreement.Country__c = 'Brasil';
        apttusAPTSAgreement.Agreement_Code__c = 'ABCDEFG';
        apttusAPTSAgreement.Region_Code__c = 'RegionCODE';
        apttusAPTSAgreement.Apttus__Status_Category__c = 'PA Signed';
        apttusAPTSAgreement.RecordTypeId = recordType1.id;
        apttusAPTSAgreement.RecordType = recordType1;
        apttusAPTSAgreement.RecordType.Name = recordType1.name;
        apttusAPTSAgreement.Apttus__Company_Signed_Date__c = Date.today(); 
        apttusAPTSAgreement.Kickoff_trigger_already_executed__c = false; 
        apttusAPTSAgreement.Admin_Mode__c = 'true';
        
        //AgreementLineItemTriggerHandler.isRecursive = false;
        insert apttusAPTSAgreement;

        Apttus__AgreementLineItem__c apttusAgreementLineItem = new Apttus__AgreementLineItem__c();
        apttusAgreementLineItem.Aircraft__c  = Produto.Id;
        apttusAgreementLineItem.Dellivery_Month__c = Date.today();
        apttusAgreementLineItem.Contract_Aircraft_Number__c = 5;
        apttusAgreementLineItem.Order_Type__c = 'Firm';
        apttusAgreementLineItem.Basic_Price__c = 200;
        apttusAgreementLineItem.Aircraft_Configuration__c = 'AK';
        apttusAgreementLineItem.Apttus__AgreementId__c = apttusAPTSAgreement.Id;
        
        //AgreementLineItemTriggerHandler.isRecursive = false;
        insert apttusAgreementLineItem; 
   }
}