//Rodrigo Gimenes
@isTest
private class ProductDiscountSCMControllerTest {

    static testMethod void myUnitTest() 
    {
        
        Id stdPbk = Test.getStandardPricebookId();
    
        Product2 prod = SObjectInstanceTest.createProduct2();
        prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        Database.insert(prod);

        PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPbk, prod.Id);
        Database.insert(pbe);       
        
        Account acc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType( 'Account', 'Operator' ));
        Database.insert(acc);
      
        ServiceContract svc = SObjectInstanceTest.createServiceContract(acc.Id, RecordTypeMemory.getRecType( 'ServiceContract', 'Aircraft_Modification' ));
        svc.Pricebook2Id = stdPbk;
        Database.insert(svc);
            
        ContractLineItem conli = SObjectInstanceTest.createContractLineItem(svc.Id, pbe.Id);
        conli.Quantity = 5.00;
        conli.Delivered__c = 0.00;
        conli.Princing__c = 'NREC';
        conli.ContractTimeDiscount__c = '3 years';
        conli.OtherEsolutions__c = 'Above 4 years';
        conli.Maximum_Discount__c = 80;
        Database.insert(conli);
      
        Service_Contract_Management__c scm = SObjectInstanceTest.createSvcContractMgmt(acc.Id, svc.Id, null,RecordTypeMemory.getRecType( 'Service_Contract_Management__c', 'Deliverable_eSolutions' ));
        scm.Contract_Line_Item_Deliverable__c = conli.Id;
        insert scm;
        
        Discount_policy__c descontoAge;
        Discount_policy__c descontoAge2;
        Discount_policy__c descontoFleet;
        Discount_policy__c descontoContract;
        Discount_policy__c descontoOther;
        List<Discount_policy__c> listaDescontos = new List<Discount_policy__c>();
        
        descontoAge = new Discount_policy__c();
        descontoAge.Discount__c = 10;
        descontoAge.Quantity_max__c = 10;
        descontoAge.Quantity_min__c = 0;
        descontoAge.Discount_type__c = 'Age';
        listaDescontos.add(descontoAge);
        
        descontoOther = new Discount_policy__c();
        descontoOther.Discount__c = 10;
        descontoOther.eSolutionsDiscountCriteria__c = 'Above 4 years';
        descontoOther.Discount_type__c = 'Other eSolutions';
        listaDescontos.add(descontoOther);
        
        descontoContract = new Discount_policy__c();
        descontoContract.Discount__c = 10;
        descontoContract.eSolutionsDiscountCriteria__c = '3 years';
        descontoContract.Discount_type__c = 'Contract Time';
        listaDescontos.add(descontoContract);
        
        descontoAge2 = new Discount_policy__c();
        descontoAge2.Discount__c = 10;
        descontoAge2.Quantity_max__c = 1;
        descontoAge2.Quantity_min__c = 1;
        descontoAge2.Discount_type__c = 'Age';
        listaDescontos.add(descontoAge2);
        
        descontoFleet = new Discount_policy__c();
        descontoFleet.Discount__c = 20;
        descontoFleet.Quantity_max__c = 10;
        descontoFleet.Quantity_min__c = 0;
        descontoFleet.Discount_type__c = 'Fleet';
        listaDescontos.add(descontoFleet);
        
        insert listadescontos;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(scm);
         
        //PageReference pageRef = Page.PricebookEditProduct;
        //Test.setCurrentPage(pageRef);
        ProductDiscountSCMController controller = new ProductDiscountSCMController(stdCOntroller);
        controller.itens = new list<ContractLineItem>{conli};
        list<SelectOption> propertyList2 = controller.otherESolutionsOptions;
        list<SelectOption> propertyList3 = controller.contractTimeOptions;
        controller.idProductSelectedRow = conli.PricebookEntry.Product2Id;
        controller.pricingSelectedRow = conli.Princing__c;
        String property3 = controller.erros;
        controller.enableSaveButton = true;
        controller.salvar();
        controller.updateESolutionsDiscount();
        
    }
}