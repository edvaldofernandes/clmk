@isTest
public class ApttusAgreementLineItemDAOTest {
    /*
    static testMethod void myUnitTest(){
        
         RecordType recordTypeProposal = [Select Name, Id From RecordType Where SobjectType = 'Apttus__APTS_Agreement__c' And DeveloperName = 'Proposal' limit 1];
        RecordType recordType2Purchase = [Select Name, Id From RecordType Where SobjectType = 'Apttus__APTS_Agreement__c' And DeveloperName = 'Purchase_Agreement' limit 1];
        RecordType recordTypeProduct2 = [Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'DCT_Aircraft' limit 1];
        
          Account account = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        
        List<Product2> lstAircraft = new List<Product2>();
        for (Integer i = 0; i < 6; i++){
            Product2 aircraft = new Product2();
            aircraft.RecordTypeId = recordTypeProduct2.id;
            if (i < 3){
                aircraft.Name = 'E1_10001';
                aircraft.Aircraft_Family__c = 'E1';
                aircraft.DF_Grouping__c = '170/175';
                lstAircraft.add(aircraft);
            } else {
                aircraft.Name = 'E2_10002';
                aircraft.Aircraft_Family__c = 'E2';
                aircraft.DF_Grouping__c = '190/195';
                lstAircraft.add(aircraft);                    
            }
        }

        Database.insert(lstAircraft); 
        
        Apttus__APTS_Agreement__c apttusAPTSAgreement = new Apttus__APTS_Agreement__c();
        apttusAPTSAgreement.Name = 'Teste 1';
        apttusAPTSAgreement.Apttus__Account__c = account.Id;
        apttusAPTSAgreement.Apttus__Status_Category__c = 'Proposal Signed';
        apttusAPTSAgreement.Country__c = 'Brasil';
        apttusAPTSAgreement.RecordTypeId = recordTypeProposal.id;
        apttusAPTSAgreement.RecordType = recordTypeProposal;
        apttusAPTSAgreement.Agreement_Code__c = 'EMB';
        
        Database.insert(apttusAPTSAgreement);
        
        Aircraft_Price__c ap2 = new Aircraft_Price__c();
        ap2.Model__c = lstAircraft.get(1).id;
        ap2.Aircraft_Version__c = 'IGW';
        ap2.Economic_Condition__c = 'JAN/2018'; 
        ap2.Agreement__c = apttusAPTSAgreement.Id;
        ap2.Aircraft_list_Price__c = 10000;
        ap2.Aircraft_Basic_Price__c = 60;
        Database.insert(ap2);
        
        Apttus__AgreementLineItem__c lineItem1 = new Apttus__AgreementLineItem__c();
        lineItem1.Aircraft__c = lstAircraft.get(1).id;
        lineItem1.Aircraft__r = lstAircraft.get(1);
        lineItem1.Current_Snapshot_Version__c = 0;
        lineItem1.Apttus__AgreementId__c = apttusAPTSAgreement.id;
        lineItem1.Apttus__AgreementId__r = apttusAPTSAgreement;
        lineItem1.Aircraft_Price__c = ap2.Id;
        lineItem1.Escalated_Price__c = 300;
        lineItem1.Delivery_Date__c = Date.today().addMonths(6);
        lineItem1.PDP_Already_set__c = true;
        lineItem1.Basic_Price__c = 400;
        lineItem1.Dellivery_Month__c = Date.today().addMonths(6);

       
        Database.insert(lineItem1);
        
        Apttus__AgreementLineItem__c lineItem2 = new Apttus__AgreementLineItem__c();
        lineItem2.Aircraft__c = lstAircraft.get(1).id;
        lineItem2.Aircraft__r = lstAircraft.get(1);
        lineItem2.Current_Snapshot_Version__c = 0;
        lineItem2.Apttus__AgreementId__c = apttusAPTSAgreement.id;
        lineItem2.Apttus__AgreementId__r = apttusAPTSAgreement;
        lineItem2.Aircraft_Price__c = ap2.Id;
        lineItem2.Escalated_Price__c = 300;
        lineItem2.Delivery_Date__c = Date.today().addMonths(6);
        lineItem2.PDP_Already_set__c = true;
        lineItem2.Basic_Price__c = 400;
        lineItem2.Dellivery_Month__c = Date.today().addMonths(6);
 
       
        Database.insert(lineItem2);
        
        test.startTest();
        
        ApttusAgreementLineItemDAO.getInstance().getApttusAgreementLineItemById(lineItem1.Id);
        ApttusAgreementLineItemDAO.getInstance().getAgreementLineItem( new Set<String>{apttusAPTSAgreement.id} );
        ApttusAgreementLineItemDAO.getInstance().getAgreementLineItemByAgreementId( new Map<String, Apttus__APTS_Agreement__c>{apttusAPTSAgreement.id => apttusAPTSAgreement} );
        ApttusAgreementLineItemDAO.getInstance().getApttusAgreementLineItem(new Set<String>{lineItem1.id}); 
        
        test.stopTest();
        
    }*/
}