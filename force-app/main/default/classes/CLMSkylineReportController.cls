global class CLMSkylineReportController 
{
    
    global static string filterInitialYear{get;set;}
    global static string filterFinalYear{get;set;}
    public static string selectedYearSkyline{get;set;}
    global static string selectedInitialYear{get;set;}
    global static string selectedFinalYear{get;set;}
    public list<string> seriesNames { get;set;} 
    public boolean canChangeSettings{get;private set;}

    global CLMSkylineReportController()
    {
        selectedYearSkyline = string.valueOf(date.today().year());
        selectedInitialYear = string.valueOf(date.today().year());
        selectedFinalYear = string.valueOf(date.today().year());
        canChangeSettings = [Select Id From Profile Where Name = 'System Administrator'].Id == userInfo.getProfileId();
    }

    public PageReference loadChart()
    {
        return null;
    }

    public List<SelectOption> getFilterInitialYears()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        for(integer year =2009;year <= date.today().addYears(10).year();year++ )
           options.add(new SelectOption(string.valueOf(year), string.valueOf(year)));
   
       return options;
    }
    
    public List<SelectOption> getFilterFinalYears()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        for(integer year =2009;year <= date.today().addYears(10).year();year++ )
           options.add(new SelectOption(string.valueOf(year), string.valueOf(year)));
   
       return options;
    }
    
    public PageReference updateParameters()
    {
        return null;
    }
    
    global class ChartDataWrapper 
    {
       
        public String axisName { get;set;}
        public integer goalValue { get;set;}
        public integer totalColumns { get;set;}
        public integer gapValue { get;set;}
        public list<integer> seriesValues{get;set;}
        public list<string> seriesNames{get;set;} 
        public list<string> seriesAnnotation{get;set;} 
        
        public chartDataWrapper(string p_AxisName,integer p_GoalValue,list<integer> p_SeriesValues,integer p_TotalColumns,list<string> p_seriesNames ) 
        {
            integer totalValues = 0;
            this.axisName = p_AxisName;
            this.goalValue = p_GoalValue;
            this.totalColumns  = p_TotalColumns;
            this.seriesNames = p_seriesNames;
            seriesAnnotation = new list<string>();
            
            for(integer value : p_SeriesValues)
            {
                seriesAnnotation.add(value==0 ? '' : string.valueOf(value));
                totalValues += value; 
            }
            
            this.seriesValues = p_SeriesValues;
            this.gapValue = this.goalValue - totalValues;
            
            if(this.gapValue < 0)
               this.gapValue = 0; 
            
        }
    }

    @RemoteAction  
    global static List<ChartDataWrapper> loadChartData(string p_initialYear,string p_finalYear) 
    {
        System.Debug('entrou na função');
        List<ChartDataWrapper> returnValue = new List<ChartDataWrapper>();
        List<integer> listValues; 
        DCTReportsSettings__c axisSeries;
        string seriesName = '';
        integer goal = 0;
        integer totalValue = 0;
        integer initialYear;
        integer finalYear;
        string year = '';
        
        selectedYearSkyline = p_initialYear;
        
        initialYear = integer.valueOf(p_initialYear);
        finalYear = integer.valueOf(p_finalYear);
        
        list<integer> listSeriesValues;
        Map<string,list<integer>> mapYearSeriesValues = new Map<string,list<integer>>() ;
        list<string> seriesNames = new list<string>();
        map<string,integer> mapSeriesNameListIndex = new map<string,integer>();

        integer index = 0;
        for(Schema.PicklistEntry picklistEntry : Agreement_Aircraft__c.Summary_SKYLINE__c.getDescribe().getPicklistValues())
        {        
            seriesNames.add(picklistEntry.getValue());
            mapSeriesNameListIndex.put(picklistEntry.getValue(),index);
            index++;
        }
        
         for(AggregateResult result : [SELECT Skyline_Summary_Calculated__c seriesName, Count(Id) totalRecords,
                                              Calendar_Year(TREND_Delivery_Date__c) year
                                         FROM Agreement_Aircraft__c 
                                        WHERE Agreement__r.Status_Category__c = 'PA Signed' 
                                          AND Calendar_Year(TREND_Delivery_Date__c) >= : initialYear 
                                          AND Calendar_Year(TREND_Delivery_Date__c) <= : finalYear 
                                     GROUP BY Skyline_Summary_Calculated__c ,Calendar_Year(TREND_Delivery_Date__c) 
                                     ORDER BY Calendar_Year(TREND_Delivery_Date__c)])
        {
            seriesName = string.ValueOf(result.get('seriesName'));
            year = string.ValueOf(result.get('year'));
            
            if(mapSeriesNameListIndex.containsKey(seriesName))
            {
                if(string.isNotEmpty(seriesName) && string.isNotEmpty(year))
                {
                    if(mapYearSeriesValues.containsKey(year))
                    {
                        listSeriesValues = mapYearSeriesValues.get(year);   
                    }
                    else
                    {
                        listSeriesValues = new list<integer>();
                        for(integer count = 0 ; count <= mapSeriesNameListIndex.size();count++)
                            listSeriesValues.add(0);
                    }
                    listSeriesValues[mapSeriesNameListIndex.get(seriesName)] = integer.ValueOf(result.get('totalRecords'));
                    mapYearSeriesValues.put(year,listSeriesValues);
                }
            }
        }

        for(string yearTemp : mapYearSeriesValues.keySet())
        {
            axisSeries = DCTReportsSettings__c.getInstance(string.ValueOf(yearTemp));
            
            goal = 0;
            
            if(axisSeries != null)
                goal = integer.valueOf(axisSeries.YearGoal__c);
                
                returnValue.add(new ChartDataWrapper(string.ValueOf(yearTemp),goal,mapYearSeriesValues.get(yearTemp),seriesNames.size(),seriesNames));
        }
        return returnValue ;
    }
    
    public PageReference changeSettings()
    {
        return new PageReference('/apex/DCTReportsSettingsInterface?year='+ selectedInitialYear + '&origin=CLMSkylineReport');
    }
}