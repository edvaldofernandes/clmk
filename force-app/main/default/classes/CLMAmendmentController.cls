public class CLMAmendmentController {    
    public boolean isHide {get;set;}
    public boolean isAmendmentReady {get;set;}
    public String newAmendmentId {get;set;}
    
    public CLMAmendmentController(ApexPages.StandardController controller){
        isHide = true;
        isAmendmentReady = false;
        newAmendmentId = '';
    }
    
    public PageReference amend(){
        String agreementId = ApexPages.currentPage().getParameters().get('id');
        
        if (agreementId==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return null;
        }
        Agreement__c agr = new Agreement__c();
        agr.Id = Id.valueOf(agreementId);
        agr.Error_Log__c = '';
        database.update(agr);
        agreementId = CLMCloneAgreement.cloneAgreement(agreementId, 'Amend');
        
        if(agreementId=='Not Approved or Signed Yet'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Agreement not Signed'));
            return null;
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your amendment request is being processed. It can take a few seconds.'));
        isHide = !isHide;
        return null;
	
    }

    public PageReference amendCompleted(){
        if(isAmendmentReady){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your amendment is ready!'));
            return null;
        }
        Agreement__c agr = [SELECT name, Error_Log__c FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(agr == null) return null;
        if(agr.Error_Log__c!=null&&agr.Error_Log__c!=''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Amendment was not completed. '+ agr.Error_Log__c));
            return null;
        }
        List<Agreement__c> agrList = [SELECT id FROM Agreement__c WHERE name = :(agr.name + ' Amendment') LIMIT 1];
        if(agrList.isEmpty()){
            if(!isHide)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your amendment request is being processed. It can take a few seconds.'));
            return null;
        }
        newAmendmentId = String.valueOf(agrList.get(0).id);
        //newAmendmentId = String.valueOf([SELECT id FROM Agreement__c WHERE name = :([SELECT name FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1].name + + ' Amendment') LIMIT 1].get(0).id);
        if(newAmendmentId!=''){
            isAmendmentReady = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your amendment is ready!'));
        }
        return null;
    }

    public PageReference goToNewAmend(){
        PageReference returnPage = new PageReference('/'+ newAmendmentId);  
        returnPage.setRedirect(true);
        return returnPage;
    }
    

}

/*
 * public class CLMAmendmentController {    
    public boolean isHide {get;set;}
    public boolean isAmendmentReady {get;set;}
    public String newAmendmentId {get;set;}
    
    public CLMAmendmentController(ApexPages.StandardController controller){
        isHide = true;
        isAmendmentReady = false;
        newAmendmentId = '';
    }
    
    public PageReference amend(){
        String agreementId = ApexPages.currentPage().getParameters().get('id');
        
        if (agreementId==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return null;
        }
        
        agreementId = CLMCloneAgreement.cloneAgreement(agreementId, 'Amend');
        
        if(agreementId=='Not Approved or Signed Yet'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Agreement not Signed'));
            return null;
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your amendment request is being processed. It can take a few seconds.'));
        isHide = !isHide;
        return null;
	
    }

    public PageReference amendCompleted(){
        if(isAmendmentReady){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your amendment is ready!'));
            return null;
        }
        Agreement__c agr = [SELECT name FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(agr == null) return null;
        List<Agreement__c> agrList = [SELECT id FROM Agreement__c WHERE name = :(agr.name + + ' Amendment') LIMIT 1];
        if(agrList.isEmpty()){
            if(!isHide)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your amendment request is being processed. It can take a few seconds.'));
            return null;
        }
        newAmendmentId = String.valueOf(agrList.get(0).id);
        //newAmendmentId = String.valueOf([SELECT id FROM Agreement__c WHERE name = :([SELECT name FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1].name + + ' Amendment') LIMIT 1].get(0).id);
        if(newAmendmentId!=''){
            isAmendmentReady = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your amendment is ready!'));
        }
        return null;
    }

    public PageReference goToNewAmend(){
        PageReference returnPage = new PageReference('/'+ newAmendmentId);  
        returnPage.setRedirect(true);
        return returnPage;
    }
    

}*/