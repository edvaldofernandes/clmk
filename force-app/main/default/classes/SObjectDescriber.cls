/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for doing describing operations about sObjects. 
*
* NAME: SObjectDescriber.cls
* AUTHOR: ASDM                                                 DATE: 27/08/2013
*
*******************************************************************************/
public with sharing class SObjectDescriber{
  
  public static List< String > getSObjectFieldsList (Schema.SObjectType obj){
    return new List< String >( obj.getDescribe().fields.getMap().keySet() );
  }
  
  public static String getSObjectFieldsString (Schema.SObjectType obj){
    return String.join( SObjectDescriber.getSObjectFieldsList( obj ), ', ' );
  }
 
  public static List< String > getSObjectRequiredFieldsList (Schema.SObjectType obj){
    List< String > fieldList = new List< String >();
    Map<String,Schema.SObjectField> fieldMap = obj.getDescribe().fields.getMap();
    for( Schema.SObjectField field : fieldMap.values() ){
      if( !field.getDescribe().isNillable() ) fieldList.add( String.valueOf( field ) );
    }
    return fieldList;
  }
  
  public static String getSObjectRequiredFieldsString (Schema.SObjectType obj){
    return String.join( SObjectDescriber.getSObjectRequiredFieldsList( obj ), ', ' );
  }
  
  public static String getLabel( Schema.SObjectType obj, String fieldName ){
    return obj.getDescribe().fields.getMap().get( fieldName ).getDescribe().getLabel();
  }
}