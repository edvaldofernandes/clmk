public without sharing class CFCPurchaseHistory {
    public class serviceContractWrapper{
        public id IdContract{get;set;}
        public string ContractNumber{get;set;}
        public date SignatureDate{get;set;}
        public string ContractStatus{get;set;}
        public Id OwnerId{get;set;}
        public DateTime CreatedDate{get;set;}
        public List<ContractLineItem> ContractItemList{get;set;}
    } 
    
    public class contractLineItemWrapper{
        public string Name{get;set;}
        public id ServiceContractId{get;set;}
        public string ContractNumber{get;set;}
        public date SignatureDate{get;set;}
        public double Quantity{get;set;}
        public datetime CreatedDate{get;set;}
        public String ContractStatus{get;set;}
        public String ProductName{get;set;}
        public String ProductId {get;set;}
        public String ProductPublicationStatus{get;set;}
        public String ProductPardotStatus {get;set;}
        public String ProductCustomRedirectPardot {get;set;}
    }
    
    public List<serviceContractWrapper> serviceContractList{get;set;}
    public List<contractLineItemWrapper> contractItemList{get;set;}
    
    public CFCPurchaseHistory(List<ServiceContract> pServiceContractList, List<ContractLineItem> pContractItemList){
        serviceContractList = new List<serviceContractWrapper>();
        contractItemList = new List<contractLineItemWrapper>();
        
        for(ServiceContract sc : pServiceContractList){
            serviceContractWrapper serviceWrapper = new serviceContractWrapper();
            serviceWrapper.IdContract = sc.Id;
            serviceWrapper.ContractNumber = sc.ContractNumber;
            serviceWrapper.SignatureDate = sc.Signature_Date__c;
            serviceWrapper.ContractStatus = sc.Contract_Status__c;
            serviceWrapper.OwnerId = sc.OwnerId;
            serviceWrapper.CreatedDate = sc.CreatedDate;
            this.serviceContractList.add(serviceWrapper);
        }
        
        for(ContractLineItem cl : pContractItemList){
            contractLineItemWrapper contractWrapper = new contractLineItemWrapper();
            contractWrapper.ServiceContractId = cl.ServiceContractId;
            contractWrapper.ContractNumber = cl.ServiceContract.ContractNumber;
            contractWrapper.SignatureDate = cl.ServiceContract.Signature_Date__c;
            contractWrapper.ContractStatus = cl.ServiceContract.Contract_Status__c;
            contractWrapper.CreatedDate = cl.CreatedDate;
            contractWrapper.ProductName = cl.PricebookEntry.Product2.Name;
            contractWrapper.ProductId = cl.PricebookEntry.Product2.Id;
            contractWrapper.ProductPublicationStatus = cl.PricebookEntry.Product2.Publication_status__c;
            contractWrapper.Quantity = cl.Quantity;
            contractWrapper.ProductPardotStatus = cl.PricebookEntry.Product2.Pardot_Status__c;
            contractWrapper.ProductCustomRedirectPardot = cl.PricebookEntry.Product2.Custom_Redirect_Pardot__c;
            this.contractItemList.add(contractWrapper);
        }
    }
}