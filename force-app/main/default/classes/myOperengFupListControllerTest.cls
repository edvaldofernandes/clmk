/**
* @description: myOperengFupListController test class.
**/
@isTest
public class myOperengFupListControllerTest {
    @isTest static void testCanGetPublishedFup(){
        Fup__c expectedFup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
        Fup__c unpublishedFup = new FO_FupTestDataBuilder()
            .withPublishStatus('Unpublished')
            .build();
        
        
        List<Fup__c> fups = myOperengFupListController.getFups(null, null, null);

        System.debug(!fups.contains(unpublishedFup));
    }
}