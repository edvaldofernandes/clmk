/**
* Controller class for the visualforce page CFCSearchResult
* Name: CFCSearchResultController
* @author - Guilherme Nascimento - gjesus@deloitte.com
* @version 1.0 - 15/12/2015
**/

public without sharing class CFCSearchResultController {
    
    public List<Product2> lstProducts {get; set;}
    public String requestProductId {get;set;}
    public String title {get;set;}
    public String subTitle {get;set;}
    
    public Boolean showTitleInSearchResult {get;set;}
    public Map<String, String> mapThumbnails {get; set;}
    
    public Boolean isShowFilter {get;set;}
    public Integer countSearch {get;set;}
    public Boolean isPardotActive {get;set;}
    public String selectedValueOrderBy {get; set;}
    
    public List<SelectOption> getOptionsOrderBy() {
        List<SelectOption> options = new LIST<SelectOption>();
        options.add(new SelectOption('Newest Arrivals', 'Newest Arrivals'));
        options.add(new SelectOption('Products A-Z', 'Products A-Z'));
        options.add(new SelectOption('Products Z-A', 'Products Z-A'));
        return options;
    }    
    public void MethodOne() { 
        system.debug('CFCSearchResultController.selectedValueOrderBy >>> ' + selectedValueOrderBy);
        SearchFilter();
    }
    
    public CFCSearchResultController(){
        system.debug('CFCSearchResultController.CFCSearchResultController()');
        SearchFilter();
    }
    
    public CFCSearchResultController(ApexPages.StandardController stdController){
        system.debug('CFCSearchResultController.CFCSearchResultController()');
        SearchFilter();
    }
    
    public pageReference RequestProposal(){
        system.debug('CFCSearchResultController.RequestProposal.requestProductId >>> ' + requestProductId);
        
        User userCFC = UserDAO.getInstance().getUserById(UserInfo.getUserId());
        system.debug('CFCProductDetailsController.RequestProposal.userCFC >>> ' + userCFC);
        Product2 productCFC = Product2DAO.getInstance().getProductPublishById(requestProductId);
        system.debug('CFCSearchResultController.RequestProposal.productCFC >>> ' + productCFC);  
        
        Proposal__c proposal;      
        
        proposal = ProposalDao.getInstance().getOpenProposalByContactAndAccount(userCFC.Contact.id, userCFC.Contact.Account.id);        
        if(proposal == null){
            //if proposal is null, so we create a new proposal
            //proposal = ProposalDao.getInstance().createProposal(accountCFC, contactCFC, productCFC);            
            proposal = ProposalDao.getInstance().createProposal(userCFC.Contact.AccountId, userCFC.ContactId, userCFC.Contact.Email, userCFC.Contact.Phone);
            system.debug('CFCSearchResultController.RequestProposal.proposal >>> ' + proposal);
        }
        system.debug('CFCSearchResultController.RequestProposal.proposal >>> ' + proposal);
        
        //add proposal items at proposal
        Proposal_Items__c item = ProposalItemsDao.getInstance().getItemByProposalAndProduct(proposal.Id, productCFC.Id);
        if(item == null){
            //create a new proposal item if he doesn't exist
            item = ProposalItemsDao.getInstance().createProposalItem(productCFC.Id, proposal.Id);
        }
        
        //PageReference page = new PageReference('/apex/CFCSearchResult');
        PageReference page = new PageReference('/apex/CFCRequestProposal');
        page.setRedirect(true);
        return page;
    }
    
    public void GenerateThumbnails(List<Product2> products){
        system.debug('CFCSearchResultController.GenerateThumbnails() >>> ' + products);
        
        mapThumbnails = new Map<String, String>();        
        Set<String> setIdProducts = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_home');
        
        //creating a set of products, just to ensure that there is not equals id
        for(Product2 item : products){
            setIdProducts.add(item.id);
        }
        
        //pass to a map the ids
        for(String item : setIdProducts){
            mapThumbnails.put(item, 'no image');
        }
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetProductIdAndRecordTypeId(setIdProducts, rType.Id);
        
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            try{
            mapThumbnails.put(lstAttch.get(i).Product__c, lstAttch.get(i).Attachments[0].id);    
            }catch(Exception ex){
                mapThumbnails.put(lstAttch.get(i).Product__c, 'no image');
            }
            
        }
        
        System.debug('CFCSearchResultController.GenerateThumbnails.mapThumbnails >>> ' + mapThumbnails);
    }
    
    public static String getLicense(){
        String profileId = UserInfo.getProfileId();
        Profile profile = [select Name, Id, UserLicenseId from Profile where Id = :profileId];
        UserLicense userLicense = [select Name, Id from UserLicense where Id = :profile.UserLicenseId];
        return userLicense.Name;
    }
    
    
    public void GeneratePageTitle(String categoryQueryString){
        showTitleInSearchResult = false;
        system.debug('categoryQueryString >>> ' + categoryQueryString);
        if (categoryQueryString != null){
            if (categoryQueryString.equalsIgnoreCase('Support')) {
                title = 'Support';
            } 
            if (categoryQueryString.equalsIgnoreCase('Flight')) {
                title = 'Flight Operations';
            } 
            if (categoryQueryString.equalsIgnoreCase('Maintenance')) {
                title = 'Maintenance';
            } 
            if (categoryQueryString.equalsIgnoreCase('Materials')) {
                title = 'Materials';
            }
            if (categoryQueryString.equalsIgnoreCase('Training')) {
                title = 'Training';
            }
            if (categoryQueryString.equalsIgnoreCase('Modifications')) {
                title = 'Modifications';
            }
            if (categoryQueryString.equalsIgnoreCase('Idea')) {
                title = 'Conceptual Idea';
            }
            
            /*if (categoryQueryString.equalsIgnoreCase('Lessors')) {
                title = 'Lessors';
            } 
            if (categoryQueryString.equalsIgnoreCase('eSolutions')) {
                title = 'eSolutions';
            }*/
        } else {
            title = 'Search Result';
            showTitleInSearchResult = true;
        }
        system.debug('showTitleInSearchResult: ' + showTitleInSearchResult);
        system.debug('title >>> '+ title );
    }
    
    public String GenerateProductCategory(string categoryQueryString){
        String category = '';
        
        if (categoryQueryString.equalsIgnoreCase('Support')) {
            category = 'Support';
        }
        if (categoryQueryString.equalsIgnoreCase('Flight')) {
            category = 'Flight Operations';
        } 
        if (categoryQueryString.equalsIgnoreCase('Maintenance')) {
            category = 'Maintenance & Engineering';
        } 
        if (categoryQueryString.equalsIgnoreCase('Materials')) {
            category = 'Materials & Logistics';
        }
        if (categoryQueryString.equalsIgnoreCase('Training')) {
            category = 'Training';
        }
        if (categoryQueryString.equalsIgnoreCase('Modifications')) {
            category = 'Modifications';
        }
        if (categoryQueryString.equalsIgnoreCase('Idea')) {
            category = 'Conceptual Idea';
        }
        
        /*if (categoryQueryString.equalsIgnoreCase('Lessors')) {
            category = 'Lessors';
        }
        if (categoryQueryString.equalsIgnoreCase('eSolutions')) {
            category = 'eSolutions';
        */
        return category;
    }
    
    public void SearchFilter(){
        VerifyPardot();
        
        isShowFilter = false;
        countSearch = 0;
        
        String queryString = ApexPages.currentPage().getParameters().get('q');
        String productCategory = ApexPages.currentPage().getParameters().get('productCategory');
        String fleetType = ApexPages.currentPage().getParameters().get('fleetType');
        String operationContext = ApexPages.currentPage().getParameters().get('operationContext');
        String productFamily = ApexPages.currentPage().getParameters().get('productFamily');
        String applicabilityCompany = ApexPages.currentPage().getParameters().get('applicabilityCompany');
        String productType = ApexPages.currentPage().getParameters().get('productType');
        String ataChapter = ApexPages.currentPage().getParameters().get('ataChapter');
        System.debug('SearchFilter.queryString: ' + queryString);
        System.debug('SearchFilter.FLEETTYPE+++: ' + fleetType);
        
        if (productCategory == null){
            subTitle = queryString;
            System.debug('CFCSearchResultController.SearchFilter.subTitle: ' + subTitle);
        }
        
        GeneratePageTitle(productCategory);
        
        if(!String.isBlank(productCategory)){
            productCategory = GenerateProductCategory(productCategory);
        }
        
        queryString = '\'%'+(queryString == null ? '' : queryString)  +'%\'';
        system.debug('CFCSearchResultController.queryString >>> ' + queryString);
        system.debug('CFCSearchResultController.productCategory >>> ' + productCategory);
        system.debug('CFCSearchResultController.fleetType >>> ' + fleetType);
        system.debug('CFCSearchResultController.operationContext >>> ' + operationContext);
        system.debug('CFCSearchResultController.productFamily >>> ' + productFamily);
        system.debug('CFCSearchResultController.applicabilityCompany >>> ' + applicabilityCompany);
        system.debug('CFCSearchResultController.productType >>> ' + productType);
        system.debug('CFCSearchResultController.ataChapter >>> ' + ataChapter);
        
        system.debug('CFCSearchResultController.SearchFilter.selectedValueOrderBy >>> ' + selectedValueOrderBy);
        
        string querySOQL = 'SELECT id, name, Family, Publication_Status__c, Show_Proposal__c, ProductCode, Commercial_Description__c,Applicability__c,Category__c,Customer_Phase__c,Applicability_company__c,Product_Type__c,ATA_Chapter__c, RecordType.name,Pardot_Status__c, Custom_Redirect_Pardot__c' 
            + ' FROM Product2'
            + ' WHERE Publication_Status__c = \'Published\''
            + ' AND (name LIKE ' + queryString +''
            + ' OR ProductCode LIKE ' + queryString +''
            + ' OR Family LIKE ' + queryString +''
            + ' OR ATA_Chapter__c LIKE ' + queryString +''
            + ' OR Applicability__c = ' + queryString.replace('%', '') +''
            + ' OR Product_Type__c = ' + queryString.replace('%', '') +''
            + ' OR Applicability_company__c = ' + queryString.replace('%', '') +''
            + ' OR Customer_Phase__c = ' + queryString.replace('%', '') +')';
        
        system.debug('FleetType SELECT ' + fleetType);
        
        if(!String.isBlank(productCategory)){
            querySOQL += ' AND Category__c INCLUDES ( ' + '\'' + productCategory + '\')';
        }        
        if(!String.isBlank(fleetType)){
            system.debug('FleetType IF@@@@ ' + fleetType);
            querySOQL += ' AND Applicability__c INCLUDES ( ' + '\'' + fleetType + '\')';
        }       
        if(!String.isBlank(operationContext)){
            querySOQL += ' AND Customer_Phase__c INCLUDES ( ' + '\'' + operationContext + '\')';
        }        
        if(!String.isBlank(productFamily)){
            List<String> ops = productFamily.split(';');
            system.debug('ops.size() >>> ' + ops.size());
            if(ops.size() == 1){
                querySOQL += ' AND Family = ' + '\'' + productFamily + '\'';
            } else {
                querySOQL += ' AND (';
                for(Integer i = 0; i < ops.size(); i++){
                    system.debug('i >>> ' + i);
                    querySOQL += ' Family = ' + '\'' + ops[i] + '\'';
                    if((i +1) != ops.size()){
                        querySOQL += ' OR'; 
                    } else {
                        querySOQL += ')';
                    }
                }
            }            
        }
        if(!String.isBlank(applicabilityCompany)){
            querySOQL += ' AND Applicability_company__c INCLUDES ( ' + '\'' + applicabilityCompany + '\')';
        }
        if(!String.isBlank(productType)){
            List<String> ops = productType.split(';');
            system.debug('ops.size() >>> ' + ops.size());
            if(ops.size() == 1){
                querySOQL += ' AND Product_Type__c = ' + '\'' + productType + '\'';
            } else {
                querySOQL += ' AND (';
                for(Integer i = 0; i < ops.size(); i++){
                    system.debug('i >>> ' + i);
                    querySOQL += ' Product_Type__c = ' + '\'' + ops[i] + '\'';
                    if((i +1) != ops.size()){
                        querySOQL += ' OR'; 
                    } else {
                        querySOQL += ')';
                    }
                }
            }
            //querySOQL += ' AND Product_Type__c = ' + '\'' + productType + '\'';
        }
        if(!String.isBlank(ataChapter)){
            List<String> ops = ataChapter.split(';');
            system.debug('ops.size() >>> ' + ops.size());
            if(ops.size() == 1){
                querySOQL += ' AND ATA_Chapter__c = ' + '\'' + ataChapter + '\'';
            } else {
                querySOQL += ' AND (';
                for(Integer i = 0; i < ops.size(); i++){
                    system.debug('i >>> ' + i);
                    querySOQL += ' ATA_Chapter__c = ' + '\'' + ops[i] + '\'';
                    if((i +1) != ops.size()){
                        querySOQL += ' OR'; 
                    } else {
                        querySOQL += ')';
                    }
                }
            }
            //querySOQL += ' AND ATA_Chapter__c = ' + '\'' + ataChapter + '\'';
        }
        
        if(!String.isBlank(selectedValueOrderBy)){            
            selectedValueOrderBy = GenerateValueOrderBy(selectedValueOrderBy);            
            //querySOQL += ' Order By ' + selectedValueOrderBy + ' DESC';
            querySOQL += selectedValueOrderBy;
        }
        
        system.debug('CFCSearchResultController.SearchFilter.querySOQL >>> ' + querySOQL);      
        
        if(queryString == '%%'){
            querySOQL+= ' LIMIT 50'; 
        }

        lstProducts = Database.query(querySOQL);
        system.debug('CFCSearchResultController.SearchFilter.lstProducts >>> ' + lstProducts);
        system.debug('CFCSearchResultController.SearchFilter.lstProducts.size() >>> ' + lstProducts.size());
        
        if(lstProducts.size() > 0){
            isShowFilter = true;
            countSearch = lstProducts.size();
        }
        system.debug('CFCSearchResultController.SearchFilter.isShowFilter >>> ' + isShowFilter);
        system.debug('CFCSearchResultController.SearchFilter.countSearch >>> ' + countSearch);
        
        GenerateThumbnails(lstProducts);
    }
    
    public String GenerateValueOrderBy(string opt){
        if(opt=='Products A-Z'){
            return ' Order By Name ASC';
        }else if(opt=='Products Z-A'){
            return  ' Order By Name DESC';
        }else{
            return  ' Order By CreatedDate ASC';
        }
    }
    
    public void VerifyPardot(){
        system.debug('CFCHomeController.GenerateViewProducts.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCHomeController.GenerateViewProducts.isPardotActive >>> ' + isPardotActive);
    }

}