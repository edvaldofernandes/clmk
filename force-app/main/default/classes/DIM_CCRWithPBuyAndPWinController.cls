// Used by DIM - Market Intelligence.
public with sharing class DIM_CCRWithPBuyAndPWinController {

    public Customer_Contact_Report__c ccr {get; set;}
    public Boolean hasOpportunity {get; set;}
    
    public DIM_PBuyNewEditController pBuyController {get; set;}
    public DIM_PWinNewEditController pWinController {get; set;}
    
    public EmailMessage email {get; set;}
    public String emailDistributionList { get; set; }
    public String emailTo {get;set;}
    public String emailCc {get;set;}
    public String emailBcc {get;set;}
    
    private Contact contact;
    public String contactEmail {get;set;}
    public EmailTemplate emailTemplate {get;set;}
    public EmailTemplate emailTemplateToBeSent {get;set;}
    
    private ApexPages.StandardController stdController;
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public DIM_CCRWithPBuyAndPWinController(ApexPages.StandardController stdController) {
        
        this.stdController = stdController; 
        
        String ccrId = System.currentPageReference().getParameters().get('id');
        ccr = [SELECT Id, Account_Name__c, Account_Name__r.Name, Opportunity__c, Opportunity__r.Name, Name, Meeting_Date_Time__c, Owner.Email FROM Customer_Contact_Report__c WHERE Id =: ccrId];
        if (ccr.Meeting_Date_Time__c == null) {
            ccr.Meeting_Date_Time__c = DateTime.now();
        }
        
        hasOpportunity = false;
        
        ApexPages.StandardController controller;
        
        controller = new ApexPages.StandardController(new PBuy__c());
        pBuyController = new DIM_PBuyNewEditController(controller);
        updatePBuy();

        controller = new ApexPages.StandardController(new PWin__c());
        pWinController = new DIM_PWinNewEditController(controller);
        
        email = new EmailMessage();
        email.ParentId = ccr.Id;
        email.Subject = 'CCR ' + ccr.Account_Name__r.Name + ' - ' + ccr.Name;
        email.FromAddress = UserInfo.getUserEmail();
        
        emailTo = '';
        emailCc = '';
        emailBcc = UserInfo.getUserEmail();
        
        emailTemplate = new EmailTemplate();
        emailTemplateToBeSent = new EmailTemplate();
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public PageReference step1() {
        return Page.DIM_CCR_Step1;
    }
    
    public PageReference step2() {
        if(!pWinController.hasSumOfWeightsEqualTo100()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The sum of the PWin criteria weights must be 100%.'));
            return null;
        }
        return Page.DIM_CCR_Step2;
    }
    
    public PageReference step3() {
        if(String.isBlank(emailTo) && String.isBlank(emailCc) && String.isBlank(emailBcc)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'There is no defined recipient.'));
            return null;
        }
        if((emailTo.countMatches('@') + emailCc.countMatches('@') + emailBcc.countMatches('@')) > 100) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The maximum number of email addresses allowed is 100.'));
            return null;
        }  
        if(emailBcc.countMatches('@') > 25) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The maximum number of Bcc recipients allowed is 25.'));
            return null;
        } 
        concludePBuy();
        if(hasOpportunity) {concludePWin();}
        concludeEmail();
        return Page.DIM_CCR_Step3;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public PageReference cancel() {
        return stdController.cancel();
    }
        
    public PageReference save() {
        try {insert pBuyController.newPBuy;}
        catch (DmlException e) {}
          
        if(hasOpportunity) {
            try {insert pWinController.newPWin;}
            catch (DmlException e) {}
        }
        
        ccr.PBuy__c = pBuyController.newPBuy.Score_Formula__c;
        ccr.PWin__c = pWinController.newPWin.Score_Formula__c;
        update ccr;
        
        PageReference ccrPage = new ApexPages.StandardController(ccr).view();
        ccrPage.setRedirect(true);
        return Page.DIM_PBuy_PWin_Page;
   }
   
    public PageReference saveAndSend() {
        PageReference page = save();
        String sendEmail = sendEmail();
        if(sendEmail != '') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Send Email failed. Please report the following error: ' + sendEmail));
            return null;
        }
        return page;
   }
   
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void updatePBuy() {
        if(ccr.Account_Name__c != null) {
            pBuyController.newPBuy.Account__c = ccr.Account_Name__c;
            pBuyController.updateNewPBuyAccordingToTheLastPBuy();
        }
    }
    
    public void updatePWin() {
        if(hasOpportunity && ccr.Opportunity__c != null) {
            pWinController.newPWin.Opportunity__c = ccr.Opportunity__c;
            pWinController.updateNewPWinAccordingToTheLastPWin();
        }
        else {
            hasOpportunity = false;
        }
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    private void concludePBuy() {
        pBuyController.newPBuy.CCR__c = ccr.Id;
        pBuyController.newPBuy.Date__c = ccr.Meeting_Date_Time__c.date();
        pBuyController.newPBuy.recalculateFormulas();
    }
    
    private void concludePWin() {
        //updatePWin();
        pWinController.newPWin.CCR__c = ccr.Id;
        pWinController.newPWin.Date__c = ccr.Meeting_Date_Time__c.date();
        pWinController.newPWin.recalculateFormulas();
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void setAddresses() {
        emailTo = getDistributionListContent('to');
        emailCc = getDistributionListContent('cc');
        emailBcc = getDistributionListContent('bc');
    }
    
    private String getDistributionListContent(String type) {
        Integer startIndex = emailDistributionList.indexOf(type + '[');
        if (startIndex > -1) { // Manual
            Integer endIndex = emailDistributionList.indexOf(']', startIndex);
            if (endIndex > -1) {
                return emailDistributionList.substring(startIndex + type.length() + 1, endIndex);
            }
        }
        else {
            type = type + 'G'; // Group
            startIndex = emailDistributionList.indexOf(type + '[');
            if (startIndex > -1) {
                Integer endIndex = emailDistributionList.indexOf(']', startIndex);
                if (endIndex > -1) {
                    List<String> groups = emailDistributionList.substring(startIndex + type.length() + 1, endIndex).split(';');
                    List<User> users = [SELECT Email FROM User WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName IN :groups) AND IsActive = TRUE ORDER BY Email ASC];
                    if (users.size() > 0) {
                        String emails = '';
                        for (User currentUser : users) {
                            emails += currentUser.Email + '; ';
                        }
                        return emails;
                    }
                }
            }
        }
        return '';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    private void concludeEmail() {
        email.ToAddress = emailTo;
        email.CcAddress = emailCc;
        email.BccAddress = emailBcc;
        
        contact = [SELECT Id FROM Contact WHERE Email =: contactEmail LIMIT 1];
        Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(emailTemplate.Id, contact.Id, ccr.Id);
        emailTemplate.HtmlValue = emailMessage.getHtmlBody();
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    private String sendEmail() {
        try {
            List<List<String>> emails = removeEmailsDuplicates();
            List<String> emailsTo  = emails.get(0);
            List<String> emailsCc  = emails.get(1);
            List<String> emailsBCc = emails.get(2);
            
            List<String> emailsToAndCc = new List<String>();
            emailsToAndCc.addAll(emailsTo);
            emailsToAndCc.addAll(emailsCc);            
        
            Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(emailTemplateToBeSent.Id, contact.Id, ccr.Id); 
            if(!String.isBlank(emailTo) || !String.isBlank(emailCc)) {emailMessage.setToAddresses(emailsToAndCc);}
            if(!String.isBlank(emailBcc))                            {emailMessage.setBccAddresses(emailsBcc);}
            emailMessage.setSubject(email.Subject);
            emailMessage.setSaveAsActivity(true);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{emailMessage});
        }
        catch(Exception e) {
            return e.getMessage();
        }
        return '';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    @TestVisible
    private List<String> getContacts(String addressesInString) {
    
        String DEL = ';';
        List<String> addresses = new List<String>();
        
        if(addressesInString.contains(DEL)){
            addresses = addressesInString.split(DEL);
        }
        else if(addressesInString != '') {
            addresses.add(addressesInString);
        }
        
        Set<String> uniqueAddresses = new Set<String>();
        uniqueAddresses.addAll(addresses);
        
        addresses = new List<String>();
        addresses.addAll(uniqueAddresses);
        addresses.sort();
        
        return addresses;
    } 

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @TestVisible
    private List<List<String>> removeEmailsDuplicates() {
    
        List<List<String>> emails = new List<List<String>>();
        emails.add(getContacts(emailTo));
        emails.add(getContacts(emailCc));
        emails.add(getContacts(emailBcc));
    
        for (Integer lista = 1; lista < emails.size(); lista++) { 
            for (Integer check = (lista - 1); check >= 0; check--) {
                for (Integer row = 0; row < emails.get(lista).size(); row++) {            
                    if (emails.get(check).contains(emails.get(lista).get(row))) {
                        emails.get(lista).remove(row);
                        row--;
                    }
                }
            }
        }  
    
        return emails;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public String getOpportunityName() {
        if(ccr.Opportunity__c != null) {
            return [SELECT Name FROM Opportunity WHERE Id =: ccr.Opportunity__c].Name;
        }
        return '';
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public List<SelectOption> getOpportunities() {

        List<Opportunity> opportunities = [SELECT Id, Name, Opp_Name__c
                                            FROM Opportunity
                                            WHERE AccountId =: ccr.Account_Name__c AND
                                                RecordTypeId IN ('012i0000001IyKG', '012i0000001IyKH') AND
                                                (NOT StageName LIKE 'Close%') AND
                                                (NOT StageName LIKE 'Archive%')
                                            ORDER BY RecordType.Name ASC, Leadership_Report_Firm_Order__c ASC];
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- NONE --'));
        
        if (opportunities.size() > 0) {
        
            hasOpportunity = true;
                
            for (Opportunity opportunity : opportunities) {
                options.add(new SelectOption(opportunity.Id, opportunity.Opp_Name__c));
            }
        }
        
        return options;
    }

}