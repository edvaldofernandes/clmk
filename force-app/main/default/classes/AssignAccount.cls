/* Author: Fabiano Albino Ferreira - TCS
 * 
 * Description: Search account according to mfir.
 * 
 * CreatedDate: 19/09/2019.
 * 
 * LastModifiedDate: 20/09/2019.
 */

public class AssignAccount {

    private static final Id CRC_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();
	
    private MFIRDAO mfirDAO;
    private ContactDAO contactDAO;

    private Map<String, List<Case>> casesByMfir;
    private Map<String, List<Case>> casesByEmail;

    private List<Case> cases;
    
    //Constructor
    public AssignAccount(List<Case> cases){
        this.cases = cases;
        this.mfirDAO = new MFIRDAO();
        this.contactDAO = new ContactDAO();
        this.casesByMfir = new Map<String, List<Case>>();
        this.casesByEmail = new Map<String, List<Case>>();
    }
    
    //Search by customer number or name Sap for a case account.
    public void assignAccountToCase(){
        assignContactToCaseByEmail();
        assignAccountToCaseByMFIR();
    }

    private void assignContactToCaseByEmail(){

        List<Contact> contacts = getContactsByEmail();

        if(contacts == null || contacts.isEmpty())
            return;

        for(Contact contact : contacts){

            if(!casesByEmail.containsKey(contact.Email))
                continue;

            List<Case> casesFound = casesByEmail.get(contact.Email);
			
            for(Case c : cases){
                System.debug('##BEFORE Contact Id + ' + c.ContactId);    
            	System.debug('##BEFORE + ' + c);    
                c.ContactId = contact.Id;
                System.debug('##AFTER Contact Id + ' + c.ContactId);    
            	System.debug('##AFTER + ' + c);
                if(c.Case_Type__c == 'Spare Parts')
                    System.debug('##AFTER Spare Parts');
            }
        }

    }

    private void assignAccountToCaseByMFIR(){

        List<MFIR__c> listMFir = getMfirByNumbers();
            
        System.debug('listMFir: ' + listMFir);

        if(listMFir == null || listMFir.isEmpty())
            return;
        
        for(MFIR__c mfir : listMfir){
            
            List<Case> casesFound = new List<Case>();
            
            if(casesByMfir.containsKey(mfir.MFIR_number__c)){
                                             
                casesFound = casesByMfir.get(mfir.MFIR_number__c);
                                             
            }else if(casesByMfir.containsKey(mfir.SAP_Name__c)){
                
                casesFound = casesByMfir.get(mfir.SAP_Name__c);
                
            }

            if(casesFound.isEmpty())
                continue;

            System.debug('mfir.Account__c: ' + mfir.Account__c);
            
            for(Case cas : casesFound){
                cas.AccountId = mfir.Account__c;
            }
           
        }
    }

    private List<MFIR__c> getMfirByNumbers(){
                        
        for(Case cas : cases){
            
            if(cas.RecordTypeId != CRC_ID)
                continue;
                
            String mfir = getMFIRNumber(cas.Description);
            
            if(String.isNotBlank(mfir)){
                
                if(mfir.startsWith('0000')){
                	mfir = mfir.removeStart('0000');
                }
                
                mfir = mfir.trim();

            }else{
				mfir = getSAPName(cas.Description);
            }
            
            if(String.isBlank(mfir))
                continue;
                        
            if(!casesByMfir.containsKey(mfir)){
                casesByMfir.put(mfir, new List<Case>());
            }
            
            casesByMfir.get(mfir).add(cas);  
        }
                
        System.debug('casesByMfir: ' + casesByMfir);

        if(casesByMfir.keySet() == null || casesByMfir.keySet().isEmpty())
            return null;

        List<MFIR__c> listMfir = mfirDAO.getMFIRByNumberORSapName(casesByMfir.keySet());
        
        System.debug('listMfir: ' + listMfir);
        
        return listMfir;
    }
    
    private String getSAPName(String description){
        
        String mfir = '';
        
        if(String.isNotBlank(description)){
            
            if(description.contains(' -')){
                mfir = description.substringBetween('Customer : ', ' -');
            }else{
                mfir = description.substringBetween('Customer : ', ' Customer Reference');
            }
            
            if(String.isNotBlank(mfir))
            	mfir = mfir.trim();
        }
        
        return mfir;
    }
    
    private String getMFIRNumber(String description){
        
        String mfir = '';
        
        if(String.isNotBlank(description)){
        	mfir = description.substringBetween('Customer Number:', '*');
        }
        
        return mfir;
    }

    private List<Contact> getContactsByEmail(){

        for(Case cas : cases){
            
            if(cas.RecordTypeId != CRC_ID)
                continue;

            String email = getEmail(cas.Description);

            if(String.isBlank(email))
                continue;
            
            if(!this.casesByEmail.containsKey(email)){
                this.casesByEmail.put(email, new List<Case>());
            }
            
            this.casesByEmail.get(email).add(cas);
        }

        if(this.casesByEmail == null || this.casesByEmail.keySet() == null)
            return null;
        
        List<Contact> contacts = contactDAO.getContactsByEmail(casesByEmail.keySet());

        return contacts;
    }

    private String getEmail(String description){

        String email = '';
        
        if(String.isNotBlank(description)){
            
            email = description.substringBetween('Customer Contact Mail: ', '=');

            if(String.isNotBlank(email)){
            	email = email.trim();
            }
        }

        return email;
    }
    
}