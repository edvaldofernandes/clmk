@isTest
public with sharing class CaseTrgCloseMilestoneTest {
    
    public static final Integer cont = 3;
    
    @TestSetup
    public static void setup(){
        LLPDatabase__c  database = new LLPDatabase__c(Is_Part_Serialized__c='Serialized');
        insert database;
        
        Account lConta = SObjectInstanceTest.conta();
        lConta.Type = 'Airline';
        lConta.Company_Nickname__c = 'testNickname';
        insert( lConta );
        
        Entitlement lEnt = SObjectInstanceTest.createEntitlement( lConta.id );
        lEnt.Name = 'Financial Cases';
        lEnt.Type = 'Web Support';
        Insert( lEnt );
        
        Case lCaso = new Case();
        lCaso.RecordTypeId = CaseTrgCloseMilestone.TYPE_Financial;
        lCaso.AccountId = lConta.id;
        lCaso.EntitlementId = lEnt.Id;
        lCaso.Invoice_Number__c = '12345';
        lCaso.Invoice_Reference_Number__c = 'test';
        lCaso.Invoice_issuing_date__c = System.today();
        lCaso.Invoice_due_date__c = System.today();
        lCaso.Status = 'Open';
        lCaso.Priority = 'N/A';
        lCaso.PO__c = '12312312312';
        lCaso.Notification__c = 'abcdef';
        lCaso.Part_Number__c = [Select id from LLPDatabase__c ].Id;
        insert( lCaso );
    }
    
    @isTest
    static void FinancialProcessTest() {
        
        Case lCaso = [Select AccountId, EntitlementId, Invoice_Number__c, Invoice_Reference_Number__c, Invoice_issuing_date__c,
                      Invoice_due_date__c, Status, Priority, PO__c, Notification__c, Part_Number__c From Case limit 1];
        
        
        Test.startTest();
        
        lCaso.Status = CaseTrgCloseMilestone.ANALYZE;
        database.update( lCaso );
        
        lCaso.Status = CaseTrgCloseMilestone.CLOSE;
        database.update( lCaso );
        Test.stopTest();
        
    }
    
    @isTest
    static void PendingCustInfoTest(){
        Case lCaso = [Select AccountId, EntitlementId, Invoice_Number__c, Invoice_Reference_Number__c, Invoice_issuing_date__c,
                      Invoice_due_date__c, Status, Priority, PO__c, Notification__c, Part_Number__c From Case limit 1];
        
        lCaso.Status = CaseTrgCloseMilestone.PENDING_CUST_INFO;
        database.update( lCaso );
        
        lCaso.Status = CaseTrgCloseMilestone.ISSUE_FULL_CREDIT_NOTE;
        database.update( lCaso );
        
        lCaso.Status = CaseTrgCloseMilestone.APPLY_CREDIT_NOTE;
        database.update( lCaso );
        lCaso = lCaso.clone(true, true, false, false);
    }
}