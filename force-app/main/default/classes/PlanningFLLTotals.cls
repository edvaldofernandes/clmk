//class which will be used to hold the totals and average values for FLL Planning report
public class PlanningFLLTotals {
    //Part Information and Analysis section
    public double TQA170{get;set;} {TQA170 = 0;}
    public double TQA190{get;set;} {TQA190 = 0;}
    public double TQA145{get;set;} {TQA145 = 0;}
    public double TotalNetwork{get;set;} {TotalNetwork = 0;}
    public double RepairAdmin{get;set;} {RepairAdmin = 0;}
    public double OSSAllocation{get;set;} {OSSAllocation = 0;}
    public double StockMax{get;set;} {StockMax = 0;}
    public double SafetyStock{get;set;} {SafetyStock = 0;}
    public double OverageShortage{get;set;} {OverageShortage = 0;}
    public double Usage12M{get;set;} {Usage12M = 0;}
    public double AVGUsage{get;set;} {AVGUsage = 0;}
    public double ScrapLast12M{get;set;} {ScrapLast12M = 0;}
    public decimal AVGCost{get;set;} {AVGCost = 0;}
    //On Hand section
    public double FLL2{get;set;} {FLL2 = 0;}
    public double US13{get;set;} {US13 = 0;}
    public double FLL3NewUsed{get;set;} {FLL3NewUsed = 0;}
    public double FLL30693{get;set;} {FLL30693 = 0;}
    public double FLL30730{get;set;} {FLL30730 = 0;}
    public double FLL30684EX{get;set;} {FLL30684EX = 0;}
	public double BNA0660{get;set;} {BNA0660 = 0;}
    public double OSSOH{get;set;} {OSSOH = 0;}
    public double FLL30785{get;set;} {FLL30785 = 0;}
    public double FLL30787{get;set;} {FLL30787 = 0;}
    public double TOTALOH{get;set;} {TOTALOH = 0;}
     public double TOTALOHFLL1and2{get;set;} {TOTALOHFLL1and2 = 0;}
    
    //In ACQ and Unserviceable section
    public double OpenPOs{get;set;} {OpenPOs = 0;}
    public double OpenPOsFLL1and2{get;set;} {OpenPOsFLL1and2 = 0;}
    public double OpenPRs{get;set;} {OpenPRs = 0;}
    public double OpenPRsFLL1and2{get;set;} {OpenPRsFLL1and2 = 0;}
    
    public double AtRepair{get;set;} {AtRepair = 0;}
    public double Cores{get;set;} {Cores = 0;}
    public double FLL20646{get;set;} {FLL20646 = 0;}
    public double FLL30697{get;set;} {FLL30697 = 0;}
    public double FLL30643{get;set;} {FLL30643 = 0;}
    public double FLL30630{get;set;} {FLL30630 = 0;}
    public double US135149{get;set;} {US135149 = 0;}
    //Restricted section
    public double FLL3Blkd{get;set;} {FLL3Blkd = 0;}
    public double US13Blkd{get;set;} {US13Blkd = 0;}
    public double FLL3Quar{get;set;} {FLL3Quar = 0;}
    public double US13Quar{get;set;} {US13Quar = 0;}
    public double FLL30673{get;set;} {FLL30673 = 0;}
    public double QMsOpen{get;set;} {QMsOpen = 0;}
    public double QMsOpenFLL1and2{get;set;} {QMsOpenFLL1and2 = 0;}
	//In Transit section
	public double OpenTOs{get;set;} {OpenTOs = 0;}
    public double FLL3DS{get;set;} {FLL3DS = 0;}
    public double US13DS{get;set;} {US13DS = 0;}
    public double FLL3ToRep{get;set;} {FLL3ToRep = 0;}
    public double US13ToRep{get;set;} {US13ToRep = 0;}
    
    //LBG Commercial 
    //Part Info Section
    public double TQA{get;set;} {TQA = 0;}
    public double OpenPOsLBG1and2{get;set;} {OpenPOsLBG1and2 = 0;}
    public double OpenPRsLBG1and2{get;set;} {OpenPRsLBG1and2 = 0;}
    //On Hand Section
    public double LBG2{get;set;} {LBG2 = 0;}
    public double LBG2onhandLBG2and1{get;set;} {LBG2onhandLBG2and1 = 0;}
    public double LBG3New{get;set;} {LBG3New = 0;}
    public double LBG3Used{get;set;} {LBG3Used = 0;}
    public double LBG3OVH{get;set;} {LBG3OVH = 0;}
    //Unserviceable section
    public double AtRepairLBG2{get;set;} {AtRepairLBG2 = 0;}
    public double AtRepairLBG3{get;set;} {AtRepairLBG3 = 0;}
    public double X0904Scrap{get;set;} {X0904Scrap = 0;}
    public double X0910BUsUnserv{get;set;} {X0910BUsUnserv = 0;}
    public double X0941OLDScrap{get;set;} {X0941OLDScrap = 0;}
    //Restricted section
    public double BlockedLBG3{get;set;} {BlockedLBG3 = 0;}
    public double BlockedLBG2{get;set;} {BlockedLBG2 = 0;}
    public double X0909PartOut{get;set;} {X0909PartOut = 0;}
    public double X0921Buyback{get;set;} {X0921Buyback = 0;}
    public double X0929OLDTool{get;set;} {X0929OLDTool = 0;}
    public double X0930OLDQuar{get;set;} {X0930OLDQuar = 0;}
    public double X0942OLDExcess{get;set;} {X0942OLDExcess = 0;}
    public double X0957BUsAtRS{get;set;} {X0957BUsAtRS = 0;}
    public double X0980OGMA{get;set;} {X0980OGMA = 0;}
    public double X0950DCJNB{get;set;} {X0950DCJNB = 0;}
    public double QMsOpenLBG1and2{get;set;} {QMsOpenLBG1and2 = 0;}
    //In Transit
    public double X0913BUsInTransit{get;set;} {X0913BUsInTransit = 0;}
    
    //Constructor with no arguments does nothing
    public PlanningFLLTotals(){}
    
    //Constructor with List<Stock_Info__c> as an argument
    public PlanningFLLTotals(List<Stock_Info_FLL__c> stockInfoList, String regionAndSegment){
        if(!stockInfoList.IsEmpty()){
            //if list is not empty, calculate sums of all fields
            for(Stock_Info_FLL__c si : stockInfoList){
                TotalNetwork += si.TOTAL_NETWORK__c;
                OSSAllocation += si.OSS_ALLOCATION__c;
                StockMax += si.STOCK_MAX__c;
                OverageShortage += si.OVERAGE_SHORTAGE__c;
                Usage12M += si.USAGE_12M__c;
                ScrapLast12M += si.SCRAP_12M__c;
                AVGCost += si.ACQUISITION_COST__c; 		//average this field after loop
                OSSOH += si.OSS_OH__c;
                OpenPOs += si.OPEN_POS__c;
               // OpenPOsFLL1and2 += si.OpenPOFLL1andFLL2__c;
                OpenPRs += si.OPEN_PRS__c;
              // OpenPRsFLL1and2 += si.OpenPRFLL1andFLL2__c;
                Cores += si.CORES__c;
                QMsOpen += si.QMS_OPEN__c;
               // QMsOpenFLL1and2 += si.QMFLL1andFLL2__c;
               
                OpenTOs += si.OPEN_TOS__c;
                TOTALOH += si.OH_Total__c;
               // TOTALOHFLL1and2 += si.OH_FLL1_FLL2__c;
                if(regionAndSegment=='FLL COM'){			//FLL Commercial
                    AVGUsage += si.AVG_USAGE__c;			//average this field after loop
                    TQA170 = si.TQA_170__c;
                    TQA190 = si.TQA_190__c;
                    TQA145 = si.TQA_145__c;
                    FLL2 += si.FLL2_OH__c;
                    US13 += si.US13_OH__c;
                    RepairAdmin += si.REPAIR_ADMIN_ORDERS__c;
                    FLL3NewUsed += si.FLL3_NEW_USED__c;
                    FLL30693 += si.FLL3_OVHL_0693__c;
                    FLL30730 += si.FLL3_0730_COMPLIANCE__c;
                    FLL30684EX += si.FLL3_0684_EX__c;
                    BNA0660 += si.BNA_0660__c;
                    FLL30785 += si.FLL3_0785_LLP_COMPLETE__c;
                    FLL30787 += si.FLL3_0787_LLP_PEND__c;
                    AtRepair += si.AT_REPAIR__c;
                    FLL20646 += si.FLL2_0646_UN__c;
                    FLL30697 += si.FLL3_0697_PEND_TEC__c;
                    FLL30630 += si.FLL3_0630_SCRAP__c;
                    US135149 += si.US13_5149_SCRAP__c;
                    FLL3Blkd += si.FLL3_BLKD__c;
                    US13Blkd += si.US13_BLKD__c;
                    FLL3Quar += si.FLL3_QUAR__c;
                    US13Quar += si.US13_QUAR__c;
                    FLL30673 += si.FLL3_0673_SHELF_LIFE_EXP__c;
                    FLL3DS += si.FLL3_D_S__c;
                    US13DS += si.US13_D_S__c;
                    FLL3ToRep += si.FLL3_TO_REP__c;
                    US13ToRep += si.US13_TO_REP__c;
                    OpenPOsFLL1and2 += si.OpenPOFLL1andFLL2__c;
                    TOTALOHFLL1and2 += si.OH_FLL1_FLL2__c;
                    QMsOpenFLL1and2 += si.QMFLL1andFLL2__c;
                    OpenPRsFLL1and2 += si.OpenPRFLL1andFLL2__c;
                     FLL30643 += si.FLL3_0643__c;
                    SafetyStock += si.Safety_Stock__c;
                    
                }else if(regionAndSegment=='LBG COM'){	//LBG Commercial
                    TQA = si.TQA__c;
                    LBG2 += si.LBG2__c;
                    LBG3New += si.LBG3_New__c;
                    LBG3Used += si.LBG3_Used__c;
                    LBG3OVH += si.LBG3_OVH__c;
                    AtRepairLBG2 += si.At_Repair_LBG2__c;
                    AtRepairLBG3 += si.At_Repair_LBG3__c;
                    X0904Scrap += si.X0904_Scrap__c;
                    X0910BUsUnserv += si.X0910_BUs_Unserv__c;
                    X0941OLDScrap += si.X0941_OLD_Scrap_Custom__c;
                    BlockedLBG3 += si.Blocked_LBG3__c;
                    BlockedLBG2 += si.Blocked_LBG2__c;
                    X0909PartOut += si.X0909_PartOut_KASI__c;
                    X0921Buyback += si.X0921_Buyback__c;
                    X0929OLDTool += si.X0929_OLD_TOOL_TOT__c;
                    X0930OLDQuar += si.X0930_OLD_Com_Quarantine__c;
                    X0942OLDExcess += si.X0942_COM_EXCESS__c;
                    X0957BUsAtRS += si.X0957_BUS_AT_RS__c;
                    X0980OGMA += si.X0980_COM_ENG_OGMA__c;
                    X0950DCJNB += si.X0950_COM_DC_JNB__c;
                    X0913BUsInTransit += si.X0913_BUs_In_Transit__c;
                    OpenPOsLBG1and2 += si.OpenPOLBG1andLBG2__c;
                    OpenPRsLBG1and2 += si.OpenPRLBG1andLBG2__c;
                    QMsOpenLBG1and2 += si.QMLBG1andLBG2__c;
                    LBG2onhandLBG2and1 += si.OH_LBG1_LBG2__c;
                     SafetyStock += si.Safety_Stock__c;
                }
            }
            //calculate averages
            AVGCost = (AVGCost/stockInfoList.size()).setScale(2, RoundingMode.HALF_UP);
        }
    }
}