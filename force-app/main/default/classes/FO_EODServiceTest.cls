@isTest
public class FO_EODServiceTest {
    
    /**
    * @description Test if eod's pdf filename is valid.
    **/
    @isTest static void testIfEscapedSubjectIsValidFilename(){
        String regex = '\\||/||\\?||:||\\*||"||<||>||\\|';
        Pattern regexPattern = Pattern.compile(regex);
        String subject = '\\/?:*"<>|';      	
        EOD__c eod = new FO_EODTestDataBuilder()
            .withSubject(subject)
            .build();
        String output = FO_EODService.getEscapedSubject(eod.Id);
        Matcher regexMatcher = regexPattern.matcher(subject);
        System.assertEquals(output, '_________');        
    }
    
    @isTest static void testIfIfCanIssueWhenEodIsApproved(){
        EOD__c eod = new FO_EODTestDataBuilder()
            .withStatus('Approved')
            .build();        
        System.assert(FO_EODService.canIssue(eod.Id));
    }

    @isTest static void testIfCannotIssueWhenEodIsNotApproved(){        
        EOD__c eod = new FO_EODTestDataBuilder()
            .withStatus('Draft')
            .build();
        
        System.assert(!FO_EODService.canIssue(eod.Id));   
    }
    
    /**
    * @description Test if the current EOD version is calculated correctly with leaf
    * EOD.
    **/
    @isTest static void testCanGetCurrentVersionWithMultipleRevisions(){
        Integer testReviewDepth = 2;
        EOD__c parent_eod = new FO_EODTestDataBuilder(
        						).build();
        EOD__c son_eod = new FO_EODTestDataBuilder()
                                .withParentEOD(
                                    parent_eod.Id
                                ).build();
        EOD__c grandson_eod = new FO_EODTestDataBuilder()
                                .withParentEOD(
                                    son_eod.Id
                                ).build();
        System.assertEquals(testReviewDepth, FO_EODService.getCurrentVersion(grandson_eod.Id));               
    }
    
    /**
    * @description Test if the current EOD version is calculated correctly with root
    * EOD.
    **/
    @isTest static void testCanGetCurrentVersionWhenEodIsOriginal(){
        Integer testReviewDepth = 0;
        Id eodId = new FO_EODTestDataBuilder().build().Id;
        
        System.assertEquals(testReviewDepth, FO_EODService.getCurrentVersion(eodId));       
    }
    
    /**
    * @description Test if the review log is in correct format.
    **/
    @isTest static void testIfRevisionLogIsRight(){
        Integer testReviewDepth = 2;
        map<String, Date> testReviewLog = new map<String, Date>();
        
        testReviewLog.put('ORIGINAL', Date.parse('05/17/1994'));
        testReviewLog.put('REV 1', Date.parse('05/17/2004'));
        testReviewLog.put('REV 2', Date.parse('05/17/2014'));
        
        Id originalId = new FO_EODTestDataBuilder()
            .withIssueDate(testReviewLog.get('ORIGINAL'))
            .build().Id;
        Id rev1Id = new FO_EODTestDataBuilder()
            .withParentEOD(originalId)
            .withIssueDate(testReviewLog.get('REV 1'))
            .build().Id;
        Id rev2Id = new FO_EODTestDataBuilder()
            .withParentEOD(rev1Id)
            .withIssueDate(testReviewLog.get('REV 2'))
            .build().Id;
        
         map<String, Date> reviewLog = FO_EODService.getRevisionLog(rev2Id);
            
        for (String key : testReviewLog.keySet()) {
            System.assertEquals(testReviewLog.get(key), reviewLog.get(key)); 
		}     
    }
    
    @isTest static void testIfOriginalEodIsParentOfItsClone(){  
        EOD__c eod  = new FO_EODTestDataBuilder()
            .build();
        
        EOD__c review = FO_EODService.cloneForRevision(eod.Id);
        
        System.assertEquals(eod.Id, review.Parent_EOD__c);   
    }
    
    /**
    * @description Test if the EOD's "Issue Date" field is filled with today's date.
    **/
    @isTest static void testIfPublishedEodIssueDateChanged(){
        EOD__c eod = new FO_EODTestDataBuilder()
            .withStatus('Approved')
            .build();
        
        FO_EODService.publish(eod.Id);
        Date issueDate = [SELECT Issue_Date__c FROM EOD__c WHERE Id = :eod.Id].Issue_Date__c;
        
        System.assertEquals(System.today(), issueDate);
    }
    
    /**
    * @description Test if a published EOD has it's status changed to 'Issued'.
    **/
    @isTest static void testIfPublishedEodStatusChanged(){
        EOD__c eod = new FO_EODTestDataBuilder()
            .withStatus('Approved')
            .withIssueDate(Date.parse('05/17/1994'))
            .build();
        FO_EODService.publish(eod.Id);
        String status = [SELECT Status__c FROM EOD__c WHERE Id = :eod.Id].Status__c;

        System.assertEquals('Issued', status);
    }
    
    /**
    * @description Test if a the Pdf document is being built.
    **/
    @isTest static void testCanBuildPdf(){
        EOD__c eod = new FO_EODTestDataBuilder()
            .withOperationalDisposition('muspI meroL')
            .withDescription('Lorem Ipsum')
            .withStatus('Approved')
            .withIssueDate(Date.parse('05/17/1994'))
            .build();
        
        FO_Document document = FO_EODService.buildPdf(eod.Id);

        //TODO: mock getContent method       
        System.assertEquals(Blob.valueOf('UNIT.TEST'), document.getContent());
    } 
    
    /**
    * @description Test if all aircraft related to an EOD is returned.
    **/
    @isTest static void testCanGetAssociatedAircraft(){
        Id eodId = new FO_EODTestDataBuilder().build().Id;
        Aircraft__c a = new FO_AircraftTestDataBuilder().build();
        EOD_Aircraft_Association__c r = new EOD_Aircraft_Association__c(
            EOD__c = eodId,
            Aircraft__c = a.Id            
        );
        insert r;
        
        System.assertEquals(a.Name, FO_EODService.getEodAssociatedAircraft(eodId)[0].Name);
    }  
    
    @isTest static void testIfEodPdfFilenameIsValid(){
        String accountName = 'test';
        Account acc = new Account(
            Name = accountName, 
            Company_Nickname__c = 'asdf'
        );
        insert acc;
        Case c = new Case(
        	AccountId = acc.Id
        );
        insert c;  
        Id eodId = new FO_EODTestDataBuilder()
            .withSubject('Lorem Ipsum')
            .withRelatedCase(c.Id)
            .build().Id;
        
        String filename = FO_EODService.getFilename(eodId);
        String CaseNumber = [SELECT CaseNumber FROM Case WHERE Id = :c.Id LIMIT 1].CaseNumber;

        System.assertEquals('EOD' + CaseNumber + '_' + accountName + '_' + 'Lorem Ipsum', filename);
    }
    
    /**
    * @description Test if the fields from a parent EOD is been copied to it's child.
    **/
    @isTest static void testIsCloningEodFields(){
        Case c = new Case();
        insert c;  
        EOD__c original = new FO_EODTestDataBuilder()
            .withRelatedCase(c.Id)
            .withOperationalDisposition('Lorem Ipsum')
            .withDescription('Lorem Ipsum')
            .withSubject('Lorem Ipsum')
            .build();
        
        EOD__c clone = FO_EODService.cloneForRevision(original.Id);
        System.assertEquals(original.Related_Case__c, clone.Related_Case__c);
        System.assertEquals(original.Operational_Disposition__c, clone.Operational_Disposition__c);
        System.assertEquals(original.Description__c, clone.Description__c);
        System.assertEquals(original.Subject__c, clone.Subject__c);
    }
    
    @isTest static void testCanCancelEODs(){
        EOD__c eod = new FO_EODTestDataBuilder()
            	.withStatus('Approved')
            	.build();
        
        FO_EODService.cancel(eod.Id);
        EOD__c reloadedEod = [SELECT Id, Status__c FROM EOD__c WHERE Id =: eod.Id];
        
        System.assertEquals('Cancelled', reloadedEod.Status__c);

    }
    
    @isTest static void testCannotCancelIssuedEODs(){
        String exceptionType = 'FlightOpsException';
        EOD__c eod = new FO_EODTestDataBuilder()
            	.withStatus('Issued')
            	.build();
        try{
            FO_EODService.cancel(eod.Id);
        }catch(Exception e){
            System.assertEquals(exceptionType, e.getTypeName());
        } 
    }
}