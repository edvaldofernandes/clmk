/*******************************************************************************
*                               Embraer - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class Opportunity_SharingRule
* NAME: Opportunity_SharingRuleTest.cls
* AUTHOR: Bruno Severino                                       DATE: 28/04/2015
*
*******************************************************************************/
@isTest
public class Opportunity_SharingRuleTest {
    /***
     * all opp created by SJK ACC MANAGER with MRO Srvs.. will share with SJK_ACC_MANAGER / SJK_BDM_MRO_SVCS / [*SITE*]_BDM / [*SITE*]_ACC_MANAGER 
     * */            
    static testMethod void Test1(){        
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEi'; //SJK_ACC_MANAGER
        insert(user1);
            
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Maintenance Services').getRecordTypeId();
            Test.startTest();
            insert(Opp);
            Test.stopTest();
                    
            //verify if the shares where created
            boolean hasBDM = false, hasACCM = false, hasSJKACCM = false, hasSJKBDM = false;
            List<OpportunityShare> listUserOrGroupId = [SELECT UserOrGroupId FROM OpportunityShare WHERE OpportunityId = :opp.Id AND RowCause = 'Manual'];
            Set<Id> idGroups = new Set<Id>();
            
            for( OpportunityShare opShare : listUserOrGroupID)
                idgroups.add(opShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( OpportunityShare opShare : listUserOrGroupID){
                string groupD = mapGroup.get(opShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('LATAM_ACC_MANAGER')) hasACCM = true;
                if(groupD.equals('LATAM_BDM')) hasBDM = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_MRO_SVCS')) hasSJKBDM = true;
            }        
            system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_MRO_SVCS, Deveria estar inserido no OpportunityShare');                   
            system.assert(hasBDM, 'O Grupo: LATAM_BDM, Deveria estar inserido no OpportunityShare');
            system.assert(hasACCM, 'O Grupo: LATAM_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
        }
    } 
    
    /***
     * all opp created by ERCA_ACC MANAGER with Tech Srvs.. will share with SJK_BDM_TECH_SVCS / SJK_ACC_MANAGER / SJK_FLIGHT_OPS_LIASION / SJK_SS_CONTRACT_ADMIN 
     * */  
    static testMethod void Test2(){
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEHEA0'; //ERCA_ACC
        insert(user1);
            
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'ERCA_ACC_MANAGER';
            
            Test.startTest();
            insert(Opp);
            Test.stopTest();
            
            //verify if the shares where created
            boolean hasFLOP = false, hasSSCA = false, hasSJKACCM = false, hasSJKBDM = false;
            List<OpportunityShare> listUserOrGroupId = [SELECT UserOrGroupId FROM OpportunityShare WHERE OpportunityId = :opp.Id AND RowCause = 'Manual'];
            Set<Id> idGroups = new Set<Id>();
            
            for( OpportunityShare opShare : listUserOrGroupID)
                idgroups.add(opShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( OpportunityShare opShare : listUserOrGroupID){
                string groupD = mapGroup.get(opShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('SJK_FLIGHT_OPS_LIASION')) hasFLOP = true;
                if(groupD.equals('SJK_SS_CONTRACT_ADMIN')) hasSSCA = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_TECH_SVCS')) hasSJKBDM = true;
            }
            system.assert(hasFLOP, 'O Grupo: SJK_FLIGHT_OPS_LIASION, Deveria estar inserido no OpportunityShare');
            system.assert(hasSSCA, 'O Grupo: SJK_SS_CONTRACT_ADMIN, Deveria estar inserido no OpportunityShare');
            system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_TECH_SVCS, Deveria estar inserido no OpportunityShare');        
        }
    }
    
    /***
     * all opp created by MEA_BDM with Tech Srvs.. will share with SJK_BDM_TECH_SVCS / SJK_ACC_MANAGER / MEA_ACC_MANAGER 
     * */  
    static testMethod void Test3(){        
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvESEA0'; // MEA_BDM
        insert(user1);
            
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'CHI_BDM';
            
            Test.StartTest();
            insert(Opp);
            
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services';
            update(opp);
            opp.Owner_Role__c = 'ERCA_BDM';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Technical Services';
            update(opp);
            
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services';
            update(opp);
            opp.Owner_Role__c = 'NA_BDM';            
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Technical Services';
            update(opp);
            Test.StopTest();
            
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services';
            update(opp);
            opp.Owner_Role__c = 'AP_BDM';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Technical Services';
            update(opp);
            
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services';
            update(opp);
            opp.Owner_Role__c = 'LATAM_BDM';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Technical Services';
            update(opp);
            
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services';
            update(opp);
            opp.Owner_Role__c = 'MEA_BDM';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Technical Services';
            update(opp);
            
            
            //verify if the shares where created
            boolean hasACCM = false, hasSJKACCM = false, hasSJKBDM = false;
            
            List<OpportunityShare> listUserOrGroupId = [SELECT UserOrGroupId FROM OpportunityShare WHERE OpportunityId = :opp.Id AND RowCause = 'Manual' LIMIT 30];
            Set<Id> idGroups = new Set<Id>();
            
            for( OpportunityShare opShare : listUserOrGroupID)
                idgroups.add(opShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( OpportunityShare opShare : listUserOrGroupID){
                string groupD = mapGroup.get(opShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('MEA_ACC_MANAGER')) hasACCM = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_TECH_SVCS')) hasSJKBDM = true;
            }
            
            system.assert(hasACCM, 'O Grupo: MEA_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_TECH_SVCS, Deveria estar inserido no OpportunityShare');  
                
        }
    }
    
    /***
     * all opp created by SJK ACC MANAGER with MAterial Srvs.. will share with SJK_ACC_MANAGER / SJK_BDM_MAT_SVCS / SJK_FLIGHT_OPS_LIASION / SJK_SS_CONTRACT_ADMIN 
     * */   
    static testMethod void Test4(){              
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        insert(user1);
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            opp.Account_Embraer_Site__c = 'Embraer Europe and Central Asia';
            
            Test.startTest();
            insert(Opp);
            
            opp.Account_Embraer_Site__c = 'Embraer China';
            update(opp);
            
            opp.Account_Embraer_Site__c = 'Embraer North America';
            update(opp);
            
            opp.Account_Embraer_Site__c = 'Embraer Middle East & Africa';
            update(opp);
            
            opp.Account_Embraer_Site__c = 'Embraer Asia Pacific';
            update(opp);
            
            opp.Account_Embraer_Site__c = 'Embraer Latin America';
            update(opp);
            
            
            Test.stopTest();
            
            //verify if the shares where created
            boolean hasBDM = false, hasACCM = false, hasSJKACCM = false, hasSJKBDM = false;
            List<OpportunityShare> listUserOrGroupId = [SELECT UserOrGroupId FROM OpportunityShare WHERE OpportunityId = :opp.Id AND RowCause = 'Manual'];
            Set<Id> idGroups = new Set<Id>();
            
            for( OpportunityShare opShare : listUserOrGroupID)
                idgroups.add(opShare.UserOrGroupId);
            
            Map<Id,Group> mapGroup = new Map<Id,Group>([SELECT DeveloperName FROM Group WHERE Id in :idgroups and Type='Role']);
            
            for( OpportunityShare opShare : listUserOrGroupID){
                string groupD = mapGroup.get(opShare.UserOrGroupId).DeveloperName;
                if(groupD.equals('LATAM_ACC_MANAGER')) hasACCM = true;
                if(groupD.equals('LATAM_BDM')) hasBDM = true;
                if(groupD.equals('SJK_ACC_MANAGER')) hasSJKACCM = true;
                if(groupD.equals('SJK_BDM_MAT_SVCS')) hasSJKBDM = true;
            }
            //system.assert(hasACCM, 'O Grupo: LATAM_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasBDM, 'O Grupo: LATAM_BDM, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKACCM, 'O Grupo: SJK_ACC_MANAGER, Deveria estar inserido no OpportunityShare');
            //system.assert(hasSJKBDM, 'O Grupo: SJK_BDM_TECH_SVCS, Deveria estar inserido no OpportunityShare');           
        }  
    } 
    
    static Opportunity createOpportunity(){
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
        //opp.Service_Program_Type__c = 'Technical Services';       
        opp.Fleet_Type__c = 'EJET';
        opp.Name = 'Teste - Opportunity Sharing Rule';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.newInstance(2015, 4, 14);
        opp.Account_Embraer_Site__c = 'Embraer Latin America';
        return opp;
    }
    
     static testMethod void Test5()
     {              
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        insert(user1);
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            opp.Account_Embraer_Site__c = 'Embraer China';
            
            Test.startTest();
            insert(Opp);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test6()
     {              
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        insert(user1);
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            opp.Account_Embraer_Site__c = 'Embraer North America';
            
            Test.startTest();
            insert(Opp);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test7()
     {              
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        insert(user1);
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            opp.Account_Embraer_Site__c = 'Embraer Middle East & Africa';
            
            Test.startTest();
            insert(Opp);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test8()
     {              
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        insert(user1);
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            opp.Account_Embraer_Site__c = 'Embraer Asia Pacific';
            
            Test.startTest();
            insert(Opp);
            Test.stopTest();
        }
      
     }
     
      static testMethod void Test9()
     {              
        //Creating Users to opportunity Owner
        User user1 = SObjectInstanceTest.createUser('00ei00000016vZqAAI'); //System Administrator
        user1.UserRoleId = '00Ei0000000GvEiEAK'; //SJK ACC MANANGER
        insert(user1);
        
        System.RunAs(user1)
        {
            //create Opportunity with Owner and Service Contract
            Opportunity opp = createOpportunity();
            opp.Owner_Role__c = 'SJK_ACC_MANAGER';
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Material Solutions').getRecordTypeId();
            //opp.Service_Program_Type__c = 'Material Services'; 
            opp.Account_Embraer_Site__c = 'Embraer Latin America';
            
            Test.startTest();
            insert(Opp);
            Test.stopTest();
        }
      
     }      
             
}