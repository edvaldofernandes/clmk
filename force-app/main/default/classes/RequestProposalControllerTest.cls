@isTest
public class RequestProposalControllerTest {
    
    static testMethod void validProposalRequestTest(){
        Opportunity validOppy = SObjectInstanceTest.createOpportunity();
        validOppy.Requested_Proposal__c = false;
        database.insert(validOppy);
        
        Kickoff__c validKick = new Kickoff__c();
        validKick.Name = 'Valid Oppy Kickoff';
        validKick.Opportunity__c = validOppy.Id;
        database.insert(validKick);
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',validOppy.Id);
        
        ApexPages.StandardController vp = new ApexPages.StandardController(validOppy);
        RequestProposalController pc = new RequestProposalController(vp);
        pc.requestProposal();
        
        List<Opportunity> testOppys = [SELECT Id FROM Opportunity WHERE Requested_Proposal__c = true and Sum_of_Kickoffs__c > 0];
        System.assertEquals(1,testOppys.size());
    }
    
    static testMethod void invalidProposalRequestTest(){
        Opportunity invalidOppy = SObjectInstanceTest.createOpportunity();
        invalidOppy.Requested_Proposal__c = false;
        database.insert(invalidOppy);
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',invalidOppy.Id);
        
        ApexPages.StandardController vp = new ApexPages.StandardController(invalidOppy);
        RequestProposalController pc = new RequestProposalController(vp);
        pc.requestProposal();
        
        List<Opportunity> testOppys = [SELECT Id FROM Opportunity WHERE Requested_Proposal__c = true and Sum_of_Kickoffs__c > 0];
        System.assertEquals(0,testOppys.size());
    }
    
    static testMethod void requestedProposalRequestTest(){
        Opportunity invalidOppy = SObjectInstanceTest.createOpportunity();
        invalidOppy.Requested_Proposal__c = true;
        database.insert(invalidOppy);
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',invalidOppy.Id);
        
        ApexPages.StandardController vp = new ApexPages.StandardController(invalidOppy);
        RequestProposalController pc = new RequestProposalController(vp);
        pc.requestProposal();
        
        List<Opportunity> testOppys = [SELECT Id FROM Opportunity WHERE Requested_Proposal__c = true and Sum_of_Kickoffs__c > 0];
        System.assertEquals(0,testOppys.size());
    }
    
}