// Used by DIM - Market Intelligence.
@isTest
public class DIM_PBuyPWinTrendControllerTest {

    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateAccountsList() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test Air';
        insert account;
        
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.RecordTypeId = '012i0000001IyKG';
        opportunity1.Name = '10 Aircraft';
        opportunity1.StageName = 'Business Development';
        opportunity1.CloseDate = System.today();
        insert opportunity1;
        
        Opportunity opportunity2 = new Opportunity();
        opportunity2.AccountId = account.Id;
        opportunity2.RecordTypeId = '012i0000001IyKG';
        opportunity2.Name = '20 Aircraft';
        opportunity2.StageName = 'Business Development';
        opportunity2.CloseDate = System.today();
        insert opportunity2;
        
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        List<SelectOption> accounts = controller.getAccounts();
        System.assertEquals(2, accounts.size()); // Inclui o NONE.
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateOpportunitiesList() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test Air';
        insert account;
        
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.RecordTypeId = '012i0000001IyKG';
        opportunity1.Name = '10 Aircraft';
        opportunity1.StageName = 'Business Development';
        opportunity1.CloseDate = System.today();
        insert opportunity1;
        
        Opportunity opportunity2 = new Opportunity();
        opportunity2.AccountId = account.Id;
        opportunity2.RecordTypeId = '012i0000001IyKG';
        opportunity2.Name = '20 Aircraft';
        opportunity2.StageName = 'Business Development';
        opportunity2.CloseDate = System.today();
        insert opportunity2;
        
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        controller.account = account.Id;
        
        List<SelectOption> opportunities = controller.getOpportunities();
        System.assertEquals(3, opportunities.size()); // Inclui o NONE.
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateSingleOpportunyWithMissingInformation() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test Air';
        insert account;
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.RecordTypeId = '012i0000001IyKG';
        opportunity.Name = '10 Aircraft';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        opportunity.Leadership_Report_Firm_Order__c = null;
        insert opportunity;
        
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        controller.account = account.Id;
        
        List<SelectOption> opportunities = controller.getOpportunities();
        System.assertEquals(1, opportunities.size());
        System.assert(!opportunities[0].getLabel().contains('null'));
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateRemoteActionToGetSeries() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.RecordTypeId = '012i0000001IyKG';
        opportunity.Name = '10 Aircraft';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        insert opportunity;
        
        PBuy__c pBuy1 = new PBuy__c();
        pBuy1.Account__c = account.Id;
        pBuy1.Date__c = Date.today();
        insert pBuy1;
        
        PBuy__c pBuy2 = new PBuy__c();
        pBuy2.Account__c = account.Id;
        pBuy2.Date__c = Date.today();
        insert pBuy2;
        
        PWin__c pWin = new PWin__c();
        pWin.Opportunity__c = opportunity.Id;
        pWin.Date__c = Date.today();
        insert pWin;
        
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        
        List<List<sObject>> series = DIM_PBuyPWinTrendController.getSeries(account.Id, opportunity.Id, '2000-01-01', '2100-01-01');
        System.assertEquals(2, series.size());
        System.assertEquals(2, series.get(0).size());
        System.assertEquals(1, series.get(1).size());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateIfImageExists() {
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        String urlDefault = '/resource/DIM_PBuy_PWin/Tails/_Undefined.png';
        String urlReturn = controller.getFinalURL('/resource/DIM_PBuy_PWin/Tails/Airline_XYZ.png', urlDefault);
        System.assertEquals(urlDefault, urlReturn);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateAccountName() {    
        String accountName = DIM_PBuyPWinTrendController.getAccountName('Full Name', 'Short Name');
        System.assertEquals('Short Name', accountName);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateFormattedAccountLabel() {    
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();   
        
        String accountLabelWithValue = controller.getFormattedAccountLabel('Airline (X&Z)');
        System.assertEquals('AirlineXZ', accountLabelWithValue);
        
        String accountLabelNull = controller.getFormattedAccountLabel(null);
        System.assertEquals('', accountLabelNull);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateAccountImage() {
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        controller.accountLabel = 'AirlineXZ'; 
        System.assertEquals('/resource/DIM_PBuy_PWin/Tails/_Undefined.png', controller.getAccountImage());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateOfficeImage() {
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        controller.officeLabel = 'OfficeXZ'; 
        System.assertEquals('/resource/DIM_PBuy_PWin/Icons/PWin_Office_All.png', controller.getOfficeImage());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateNewLog() {
        PageReference page = Page.DIM_PBuy_PWin_Trend_Page;
        Test.setCurrentPage(page);
        ApexPages.currentPage().getParameters().put('isMobile', 'TRUE');
    
        DIM_PBuyPWinTrendController controller = new DIM_PBuyPWinTrendController();
        controller.addLog();
        List<DIM_Audit__c> logs = [SELECT Id FROM DIM_Audit__c];
        System.assertEquals(1, logs.size());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
}