public class CRM360_CustomerSelectionController {
    // Loading Data from ID parameter
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private static Id RecordTypeIdAirline = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId();
    private static Id RecordTypeIdLeasing = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Leasing Company').getRecordTypeId();
    private static Id RecordTypeIdMRO = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MRO').getRecordTypeId();

    public static String getSiteName(){
        if(accId == null){
            return 'All Sites';
        }
        else{
            Account acc = [SELECT Name FROM Account WHERE Id = :accId];
            return acc.Name;
        }
    }
    
    //Get a list of costumer from the site 'acc'
    //Use list name: customerList
    //Fields: Name, Geographical_Area__c, Score_Fleet_Size__c, Embraer_Fleet_Type__c
    public static List<Account> getCustomerList(){
        
        List<Account> customerList = new List<Account>();
        if(accId != null){
            customerList = [SELECT Name, Has_CRM360_Data__c, Geographical_Area__c, Quantity_Embraer_Aircraft_Operator__c, 
                            Quantity_Embraer_Aircraft_Owner__c, Embraer_Fleet_Type__c, Account_Type__c
                            FROM Account 
                            WHERE (Embraer_Site__c = :accId 
                                   AND (RecordTypeId = :RecordTypeIdAirline OR RecordTypeId = :RecordTypeIdLeasing OR RecordTypeId = :RecordTypeIdMRO)
                            	   AND (Company_Status__c = 'Active' OR Company_Status__c = 'In EIS Process'))
                            ORDER BY Has_CRM360_Data__c DESC, Name ASC];
        }
        else{
            customerList = [SELECT Name, Has_CRM360_Data__c, Geographical_Area__c, Quantity_Embraer_Aircraft_Operator__c, 
                            Quantity_Embraer_Aircraft_Owner__c, Embraer_Fleet_Type__c, Account_Type__c
                            FROM Account 
                            WHERE (RecordTypeId = :RecordTypeIdAirline OR RecordTypeId = :RecordTypeIdLeasing OR RecordTypeId = :RecordTypeIdMRO)
                            AND (Company_Status__c = 'Active' OR Company_Status__c = 'In EIS Process')
                            ORDER BY Has_CRM360_Data__c DESC, Name ASC];
        }
        return customerList;
        
    }
    
}