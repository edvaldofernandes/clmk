@isTest(SeeAllData=true)
public class EtrackRepositoryTest {
    
    @isTest static void findAllAircraft(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test aircraftInformation estrack';
     	account.Company_Nickname__c = 'tt account etrack';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = 'TEST 123411';
        insert account;
        
        Aircraft__c aircraft = new Aircraft__c ();
     	aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '88888888';
        aircraft.Operator__c = account.Id;
        aircraft.Commercial_Name__c = 'EMB-110P1';
    	aircraft.Aircraft_Status__c = 'Other';
        insert aircraft;  
        
        Map<String, String> aircraftMap = EtrackRepository.findAllAircraft();
        Map<String, String> accountMap = EtrackRepository.buildAccountMap();
        
        System.assertEquals(account.id, aircraftMap.get(aircraft.name));
        System.assertEquals(account.FlyEmbraerId__c, accountMap.get(account.id));
        
        Test.stopTest();
    }
}