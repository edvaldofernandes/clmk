@isTest
private class CRM360_MonthlyRegisterClonerTest {
    
    private static String CRON_EXP = '0 0 0 15 3 ? 2022';
    private static String accountName = 'Test Account record';
    private static String accountCockpitName = 'Test Meeting';
    private static Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();       
    
    @testSetup
    static void setup(){    
        
        //Create an Account
        Account testAccount 			= new Account();
        testAccount.Name				= accountName;
        testAccount.Company_Nickname__c ='TesterComp';
        insert testAccount;
        
        //Create an Executive Summary end associate it to the account
        Account_Cockpit__c ac = new Account_Cockpit__c();
        ac.Name = accountCockpitName;
        ac.Customer_Expectation__c = 'Expects to test stuff';
        ac.Att_Level__c = 'High';
        ac.RecordTypeId = RecordTypeIdCRM360Data;
        ac.Related_EOC__c = 'EOC WW EJET 2019 WARSAW';
        ac.Account_Name__c = testAccount.Id;
        ac.Is_Updated__c = true;
        ac.Is_Record__c = false;
        insert ac;
    }
    
    static testmethod void testScheduledJob(){
  
        Test.startTest();
		String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new CRM360_MonthlyRegisterCloner());        
        Test.stopTest();
        
        List<Account_Cockpit__c> accountCockpitsTest = [SELECT Id, 
                                                        Is_Record__c, 
                                                        Name,
                                                        Account_Name__c,
                                                        Month__c,
                                                        Year__c
                                                        FROM Account_Cockpit__c 
                                                        WHERE Record_Type_ID_ref__c = 'CMR360_Dashboard_Data'
                                                        AND Is_Record__c = true];
        List<Account> accountsTest = [SELECT Name, Id FROM Account WHERE Name = :accountName]; 
        
        //Verify if the Executive Summary has been cloned and its information is correct
        System.assertEquals(accountCockpitName, accountCockpitsTest[0].Name);
        System.assertEquals(accountsTest[0].Id, accountCockpitsTest[0].Account_Name__c);
        System.assertEquals(true, accountCockpitsTest[0].Is_Record__c);
        
        //Verify if register date is correct
        System.assertEquals(String.valueOf(Date.today().year()), accountCockpitsTest[0].Year__c);
        System.assert(accountCockpitsTest[0].Month__c.contains(String.valueOf(Date.today().month())+' - '));
    }
    
}