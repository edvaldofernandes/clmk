global class myHandler implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(
       Messaging.InboundEmail email,
       Messaging.InboundEnvelope envelope) {
	//Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

 		CaseEmailMerge handler = new CaseEmailMerge();
        Messaging.InboundEmailResult result = handler.processInboundEmail(email);
        return result;        
    }
}