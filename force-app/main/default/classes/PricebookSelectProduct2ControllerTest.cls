/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
* 
* Class for testing and covering the code of the class PricebookSelectProduct2Controller
*
* NAME: PricebookSelectProduct2ControllerTest.cls
* AUTHOR:DPF                                                DATE: 02/02/2015
*
*******************************************************************************/
@isTest
private class PricebookSelectProduct2ControllerTest {

    static testMethod void myUnitTest() {
      Product2 lProduct = SObjectInstanceTest.createProduct2();
      Product2 lProduct2 = SObjectInstanceTest.createProduct2();
      lProduct2.Name = 'test2';
      lProduct2.ProductCode = 'test3';
      Database.insert(new List<Product2>{lProduct, lProduct2 });
      
      Id stdPricebook2Id = SObjectInstanceTest.getPricebook2Std2();
      
      PricebookEntry pbeStd = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct.id);
      PricebookEntry pbeStd2 = SObjectInstanceTest.createPricebookEntry(stdPricebook2Id, lProduct2.id);
      Database.insert(new List<PricebookEntry>{pbeStd, pbeStd2 });
              
      Pricebook2 lPricebook = SObjectInstanceTest.createPricebook2();
      lPricebook.Name = 'pricebook teste 1';
      Database.insert(lPricebook);
      
      Test.setCurrentPage(Page.PricebookSelectProduct2);
      apexpages.currentPage().getParameters().put('addTo', lPricebook.Id);          
      PricebookSelectProduct2Controller controller = new PricebookSelectProduct2Controller(new Apexpages.Standardcontroller(lPricebook));
      
      Test.startTest();
      controller.Selected();
      Test.stopTest();
    }
}