@isTest
public class CalendarOfEventsControllerTest {
    
    @testSetup static void setup() {
        CalendarOfEventsYearList__c setting = new CalendarOfEventsYearList__c();
        setting.Name = 'Test Setting';
        setting.Available_Years__c = '2020,2021';
        insert setting;
    }
    private static testmethod void Test(){
        
        CalendarOfEventsController COEController = new CalendarOfEventsController();
		Test.startTest();
        
        COEController.updateType();
        COEController.updateSites();
        COEController.updateDate();
        
        List<SelectOption> yearSelect = COEController.getYears();
        List<SelectOption> typesSelect = COEController.getTypes();
        List<SelectOption> sitesSelect = COEController.getSitesL();
        
        List<String> types = COEController.getEvTypes();
        List<String> sites = COEController.getSites();
        
        test.stopTest();
		        
    }
}