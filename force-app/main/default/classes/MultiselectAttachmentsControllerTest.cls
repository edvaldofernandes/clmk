@isTest
private class MultiselectAttachmentsControllerTest {
  private testMethod static void testMultiselectAttachmentsController() {
    Communications__c rec = createREC();
    rec.Email_attachments__c = '0690m000001QuVGAA0;Embraer-S-A';
    Database.insert(rec);

    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(rec);
    Test.startTest();
    MultiselectAttachmentsController mac = new MultiselectAttachmentsController(
      sc
    );

    System.assertEquals(mac.recId, rec.Id, '[ERROR] The Id isnt the same!');

    System.assertEquals(
      mac.selectedAttachs.size(),
      0,
      '[ERROR] Selected attachments doesnt match!'
    );
    System.assertEquals(
      mac.allRelatedAttachs.size(),
      0,
      '[ERROR] All related attachments doesnt match!'
    );
    Test.stopTest();
  }

  private static Communications__c createREC() {
    Id idRECRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('REC')
      .getRecordTypeId();
    Communications__c rec = new Communications__c();
    rec.RecordTypeId = idRECRecordType;
    rec.communication_status__c = 'draft';
    rec.reference_date__c = system.today();
    rec.ata_chapter__c = '00 GENEREAL';
    rec.technology__c = 'AMS';
    rec.fleet_type__c = '195-E2';
    rec.Name = 'rectest';
    return rec;
  }
}