public class ServicesUtils {
    
    private Map<String, String> aircraftModelMap;
    private static String START_WITH_ZERO = '0';
    private static INTEGER LENGHT_OF_SERIAL_NUMBER = 8;
    
    
    public ServicesUtils(){
        
        aircraftModelMap = loadMap();
    }
    
    public String changeToAircrfatModelInSalesforce(String model){
        
        model = checksIfModelStartWithZero(model);
        
        Set <String> keySet = aircraftModelMap.keySet();
        
        String newAircraftModel = model;
        
        for(String key : keySet){

            if(model.startsWith(key)){
                newAircraftModel = model.replace(key, aircraftModelMap.get(key));
                break;
            }
        }
        
        return newAircraftModel;
    }
    
     private Map<String, String> loadMap(){
        
        Map<String, String> aircraftModelMap = new Map<String, String>();
        
        for(ModelDataMapping__c dataMap : [select id, key__c, value__c from ModelDataMapping__c])
            aircraftModelMap.put(dataMap.key__c, dataMap.value__c);
         
        return aircraftModelMap;
    }
    
    public String createFieldToRelationshipmentBetwenObject(String projectCode, String model){
        
        return (projectCode+changeToAircrfatModelInSalesforce(model));
    }
    
    public String compleTeWithZero(String model, String serialNumber){

        Integer modelSize = model.length();
        Integer serialNumberSize = serialNumber.length();
        
        String concat ='';
        
        Integer result = modelSize + serialNumberSize;
        Integer restOfSize = LENGHT_OF_SERIAL_NUMBER - result;
        
        if(result < 8)
            for(integer iterator = 0; iterator < restOfSize; iterator++)
                concat = concat + '0'.trim();
         
        return (model+concat+serialNumber);
    }
    
    public static String buildSerialNumber(String serialNumber){
        
        String sn = serialNumber;
        String[] serialNumberStr = sn.split(' ');
        
        return  serialNumberStr[1];   
    }
    
    public static String breackSerialNumberReturnModel(String model){
        
        String newModel = model.substring(0,3);
        
        return newModel;
    }
    
    public static String breackSerialNumberReturnSerial(String model, String serialNumber){
        
        String newSerialNumber = serialNumber.replace(model,'');
        
        return newSerialNumber;
    }
    
    public static String checksIfModelStartWithZero(String model){
                
        if(model.startsWith(START_WITH_ZERO))
            model = model.substring(1, model.length());
        
        return model;
    }
}