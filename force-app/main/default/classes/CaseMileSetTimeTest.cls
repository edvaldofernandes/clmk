@isTest
public with sharing class CaseMileSetTimeTest {

 @TestVisible private static final Id TYPE_ECIP = RecordTypeMemory.getRecType('Case', 'ECIP_Sales');
  public static final Integer cont = 33;

  static testMethod void testPossitivoMile1orMile33() {

      Account lConta = SObjectInstanceTest.conta();
      lConta.Type = 'Airline';
      //lConta.BillingCountry = 'Brasil';
      lConta.Company_Nickname__c = 'VanTseng';
      database.insert( lConta );

      User lUser = SObjectInstanceTest.createUser(UserInfo.getProfileId());
      database.insert( lUser );

      User lUser2 = SObjectInstanceTest.createUser(UserInfo.getProfileId());
      lUser2.Username = 'user1name@mail.com.br';
      lUser2.LastName += 1;
      lUser2.Alias += 1;
      lUser2.CommunityNickname += 1;
      database.insert( lUser2 );

      Entitlement lEnt = SObjectInstanceTest.createEntitlement( lConta.id );
      lEnt.Name = 'Default';
      lEnt.Type = 'Phone Support';
      database.insert( lEnt );

      //EEJ_Aircraft__c aircraft1 = SObjectInstanceTest.createAircraft();
      //aircraft1.Owner__c = lConta.id;
      //database.insert( aircraft1 );

      Case lCaso = SObjectInstanceTest.createCase();
      lCaso.RecordTypeId = TYPE_ECIP;
      lCaso.AccountId = lConta.id;
      lCaso.OwnerId = lUser.Id;
      lCaso.EntitlementId = lEnt.Id;
      lCaso.Phase__c =  'BACK ORDER' ;
      lCaso.Status = 'EHS/Dock';
      lCaso.Reason = 'Orders';
      lCaso.Priority = 'AOG';
      

      Test.startTest();
      //lCaso.OwnerId = lUser2.Id;
      //lCaso.Phase__c = 'Track & Trace';
      //lCaso.Status = 'EHS/Shipping';
      //database.update( lCaso );
      database.insert( lCaso );
      Test.stopTest();


      List<CaseMilestone> resultMilestone = [Select Id, StartDate, caseId from CaseMilestone where caseId =: lCaso.Id order by StartDate asc];
      for(CaseMilestone Miles : resultMilestone ) {
        System.assert(Miles.StartDate != Null, 'O Milestone não foi aberto.');
      }

  }
  }