global class CLMSOBDataWrapper implements Comparable {
    
    public SOB_Data__c sob;
    
    // Constructor
    public CLMSOBDataWrapper(SOB_Data__c op) {
        sob = op;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        
        // Cast argument to CLMSOBDataWrapper
        CLMSOBDataWrapper compareToSob = (CLMSOBDataWrapper)compareTo;
         
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        
        String current = sob.Country__c  + sob.Customer__c;
        String compared = compareToSob.sob.Country__c + compareToSob.sob.Customer__c;
        
        if (current > compared) {
            returnValue = 1;
        } else if (current < compared) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }
       // system.debug('returnValueComparator' +current + ' compare to ' + compared + 'result '+returnValue); 

        return returnValue;      
    }
}