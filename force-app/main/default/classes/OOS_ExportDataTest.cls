@isTest(SeeAllData=true)
public with sharing class OOS_ExportDataTest {
  private testMethod static void fillInitialValuesTest() {
    Map<String, Object> test = new Map<String, Object>();
    test.put('test', 'test');
    test.put('teste2', 'test2');

    test = (new OOS_ExportDataHandler()).fillInitialValues(test, 'test,test2');

    System.assertEquals(
      test.get('test'),
      '',
      '[ERROR] Filling empty map is not working!'
    );
    System.assertEquals(
      test.get('test2'),
      '',
      '[ERROR] Filling empty map is not working!'
    );
  }

  private testMethod static void getOOSIdMapTest() {
    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(0),
      createOOSRecord(1),
      createOOSRecord(2)
    };
    Integer counter = 0;
    for (Database.SaveResult res : Database.insert(newOOS)) {
      createOOSExample(res.getId(), 'test' + counter);
      counter += 1;
    }
    List<FC_OOS_Association__c> fcOOS = [
      SELECT Out_of_Service__c, Fail_Code__c
      FROM FC_OOS_Association__c
      LIMIT 3
    ];
    System.assertEquals(
      (new OOS_ExportDataHandler())
        .getOOSIdMap(fcOOS, 'Fail_Code__c')
        .keySet()
        .size(),
      3,
      '[ERROR] Association between related objects and Out of Service failed!'
    );
  }
  private testMethod static void getOOSIdMapTest1() {
    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord1(0),
      createOOSRecord1(1),
      createOOSRecord1(2)
    };
    Integer counter = 0;
    for (Database.SaveResult res : Database.insert(newOOS)) {
      createOOSExample(res.getId(), 'test' + counter);
      counter += 1;
    }
    List<FC_OOS_Association__c> fcOOS = [
      SELECT Out_of_Service__c, Fail_Code__c
      FROM FC_OOS_Association__c
      LIMIT 3
    ];
    System.assertEquals(
      (new OOS_ExportDataHandler())
        .getOOSIdMap(fcOOS, 'Fail_Code__c')
        .keySet()
        .size(),
      3,
      '[ERROR] Association between related objects and Out of Service failed!'
    );
  }

  private testMethod static void getRecordIdMapTest() {
    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(0),
      createOOSRecord(1),
      createOOSRecord(2)
    };

    Database.insert(newOOS);
    List<Out_of_service__c> oosList = [
      SELECT Id, Name
      FROM Out_of_service__c
      LIMIT 1
    ];
    Map<Id, Map<String, Object>> mapOOS = (new OOS_ExportDataHandler())
      .getRecordIdMap(oosList);

    for (Out_of_Service__c oos : oosList) {
      System.assertEquals(
        mapOOS.get(oos.Id).get('Id'),
        oos.Id,
        '[ERROR] It was not possible make a Map<Id, sObject> over sObject!'
      );
    }
  }

  private testMethod static void getRecordsTest() {
    List<Out_of_service__c> newOOS = new List<Out_of_service__c>{
      createOOSRecord(0),
      createOOSRecord(1),
      createOOSRecord(2)
    };

    List<Id> oosIds = new List<Id>();
    Integer counter = 0;
    for (Database.SaveResult res : Database.insert(newOOS)) {
      oosIds.add(res.getId());
      createOOSExample(
        res.Id,
        'FCTEST' + counter,
        'RCTEST' + counter,
        'SUPTEST' + counter
      );
      counter += 1;
    }

    OOS_ExportDataHandler handler = new OOS_ExportDataHandler();
    handler.oosData = [SELECT Id FROM Out_of_Service__c WHERE Id IN :oosIds];
    Map<Id, Map<String, Object>> records = handler.getRecords();
    counter = 0;
    for (Id oosId : oosIds) {
      System.assertEquals(
        records.get(oosId).get('Fail_Code__c_Name'),
        'FCTEST' + counter,
        '[ERROR] Export OOS Data to EXCEL tool could not get the Fail Codes'
      );
      System.assertEquals(
        records.get(oosId).get('Root_Code__c_Name'),
        'RCTEST' + counter,
        '[ERROR] Export OOS Data to EXCEL tool could not get the Root Codes'
      );
      System.assertEquals(
        records.get(oosId).get('Root_Code__c_Supplier__c'),
        'SUPTEST' + counter,
        '[ERROR] Export OOS Data to EXCEL tool could not get the Suppliers'
      );

      counter += 1;
    }
  }

  public static void createOOSExample(
    Id oosId,
    String fcName,
    String rcName,
    String supplierName
  ) {
    Fail_Code__c fc = createFailCodeRecord(fcName);
    Id fcId = Database.insert(fc).getId();
    FC_OOS_Association__c fcOOS = createFCAssociation(oosId, fcId);
    Database.insert(fcOOS);

    Root_Code__c rc = createRootCodeRecord(rcName, supplierName);
    Id rcId = Database.insert(rc).getId();
    RC_OOS_Association__c rcOOS = createRCAssociation(oosId, rcId);
    Database.insert(rcOOS);
  }

  public static void createOOSExample(Id oosId, String fcName) {
    Fail_Code__c fc = createFailCodeRecord(fcName);
    Id fcId = Database.insert(fc).getId();
    FC_OOS_Association__c fcOOS = createFCAssociation(oosId, fcId);
    Database.insert(fcOOS);
  }

  public static Out_of_Service__c createOOSRecord() {
    Out_of_Service__c oos = new Out_of_Service__c();
    oos.Serial_Number__c = [SELECT Id FROM Aircraft__c LIMIT 1].Id;
    oos.reference_date__c = Date.today();
    oos.event_description__c = 'This is a test';
    return oos;
  }

  public static Out_of_Service__c createOOSRecord(Integer offset) {
    Out_of_Service__c oos = new Out_of_Service__c();
    OOS_TriggerHandler th = new OOS_TriggerHandler();
    oos.Serial_Number__c = [SELECT Id FROM Aircraft__c LIMIT 1 OFFSET :offset]
    .Id;
    th.isTestClass = true;
    oos.reference_date__c = Date.today();
    oos.event_description__c = 'This is a test';
    oos.OOS_Total_Time__c = 3;
    oos.Flight_Number__c = 'Fligth Number';
    oos.Log_Number__c = 'Log Number';
    oos.Start_Time__c = time.newInstance(1, 1, 1, 100);
    oos.Release_Time__c = time.newInstance(2, 2, 2, 200);
    oos.EMIT_Comments__c = 'aaaa';
    oos.RTS_Comments__c = 'aaaa';
    return oos;
  }
  //Test the update function if it is for the else
  public static Out_of_Service__c createOOSRecord1(Integer offset) {
    Out_of_Service__c oos = new Out_of_Service__c();
    OOS_TriggerHandler th = new OOS_TriggerHandler();
    th.isTestClass = false;
    oos.Serial_Number__c = [SELECT Id FROM Aircraft__c LIMIT 1 OFFSET :offset]
    .Id;
    oos.reference_date__c = Date.today();
    oos.event_description__c = 'This is a test';
    oos.OOS_Total_Time__c = 5;
    oos.Flight_Number__c = 'Fligth Number';
    oos.Log_Number__c = 'Log Number';
    oos.Start_Time__c = time.newInstance(1, 1, 1, 100);
    oos.Release_Time__c = time.newInstance(2, 2, 2, 200);
    oos.EMIT_Comments__c = '';
    oos.RTS_Comments__c = '';
    return oos;
  }
    
  public static Root_Code__c createRootCodeRecord(
    String name,
    String supplierName
  ) {
    Root_Code__c rc = new Root_Code__c();
    rc.Name = name;
    rc.ATA__c = '05';
    rc.Supplier__c = Database.insert(createSupplierRecord(supplierName))
      .getId();
    return rc;
  }

  public static Supplier__c createSupplierRecord(String name) {
    Supplier__c sup = new Supplier__c();
    sup.Name = name;
    return sup;
  }

  public static RC_OOS_Association__c createRCAssociation(Id oosId, Id rcId) {
    RC_OOS_Association__c rcOOS = new RC_OOS_Association__c();
    rcOOS.Out_of_Service__c = oosId;
    rcOOS.Root_Code__c = rcId;

    return rcOOS;
  }

  public static Fail_Code__c createFailCodeRecord(String name) {
    Fail_Code__c fc = new Fail_Code__c();
    fc.Name = name;
    fc.ATA__c = '05';
    fc.Sub_ATA__c = 0;

    return fc;
  }

  public static FC_OOS_Association__c createFCAssociation(Id oosId, Id fcId) {
    FC_OOS_Association__c fcOOS = new FC_OOS_Association__c();
    fcOOS.Out_of_Service__c = oosId;
    fcOOS.Fail_Code__c = fcId;

    return fcOOS;
  }
}