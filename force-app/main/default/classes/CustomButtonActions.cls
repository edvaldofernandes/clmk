public with sharing class CustomButtonActions{
    
    @TestVisible private final Case c;
    @TestVisible private final PageReference caseView;
    
    public CustomButtonActions(ApexPages.StandardController stdController){
        this.c = (Case)stdController.getRecord();
        caseView = stdController.view();
        caseView.setRedirect(true);
    }
	
    public PageReference SetEmailRead(){
        c.Incoming_email_i__c=false;
        update c;
         
		return caseView;
    }
    
    public PageReference DismissPRCAlert(){
        c.Notify_POS__c=false;
        update c;
        
        return caseView;
    }
    
    public PageReference DismissPOSAlert(){
        c.POS_Dispo__c=false;
        update c;
        
        return caseView;
    }
    
    public PageReference FillEpoolCase(){
        if(EpoolCaseTriggerHandler.isEpoolCase([SELECT Subject, Description, RecordTypeId FROM Case WHERE Id=:c.Id LIMIT 1][0]))
            EpoolCaseTriggerHandler.populateEpoolCases(new List<Id>{c.Id});
        return caseView;
    }
    
    //if the current user is not a system admin, and not using Lightning Experience, and not the case owner
    //then make the current user the case owner
    public void UpdateOwnerByApex(){
        if(caseOwnerNeedsUpdate()){
            c.OwnerId = userInfo.getUserId();
            update c;
        }
    }
    
    //returns true if a case's current owner is different than the current user, current user is not system admin and not using lightning experience
    //returns false if the owner and user are equal, or if the current user is a system admin, or if the current user is in lightning experience
    private boolean caseOwnerNeedsUpdate(){
        Set<String> userExceptionSet = new Set<String>{'Ramon Ribeiro Lopes', 'Test User'};	//set of users who should still update despite being system admin
        List<User> currentUserList = [SELECT Id, Profile.Name, UserRole.Name, Name FROM User WHERE Id = :userInfo.getUserId() LIMIT 1];
        List<Case> currentCaseList = [SELECT OwnerId FROM Case WHERE Id = :c.Id LIMIT 1];
        return (
            UserInfo.getUIThemeDisplayed() != 'Theme4u' 
            && (
                currentCaseList[0].OwnerId != currentUserList[0].Id 
                && (
                    (currentUserList[0].Profile.Name != 'System Administrator' && currentUserList[0].UserRole.Name != 'PRC Manager')
                    || 
                    userExceptionSet.contains(currentUserList[0].Name)
                )
            )
        );
    }
}