public with sharing class ChatterFiles { 
 
    public List<FeedItem> files { get; set; }     
    public string category { get; set; }     
    public Id recordId { get 
                            {    return recordId;    }
                        set { 
                                recordId = value; 
                                SearchFiles();
                            }
                        }
 
    public class entry {
        public FeedItem item { get; set; }
        public List<string> topics { get; set; }
        public string categories { get; set; }
    }
    public List<entry> items { get; set; }     
             
    public ChatterFiles() {
        items = new List<entry>();
    }
         
    public void SearchFiles () {        
        items.clear();
        List<FeedItem> feedFiles = [ SELECT Id, CreatedById, CreatedDate, Body, CommentCount, ContentFileName, ContentDescription, 
                                         InsertedById, ParentId, RelatedRecordId, Title, Type
                                          FROM FeedItem 
                                         WHERE Type = 'ContentPost' AND ParentId = :this.recordId
                                         ORDER BY CreatedDate DESC
                                    ];
         
        Set<Id> feedIds = new Set<Id> (new Map<Id,SObject>(feedFiles).keySet());
        List<TopicAssignment> topicList = [ SELECT Topic.Name, TopicId, Id, EntityId FROM TopicAssignment WHERE EntityId IN :feedIds ORDER BY Topic.Name ASC];
         
        for(FeedItem f : feedFiles){           
               List<string> topics = new List<string>();
            entry line = new entry();
            boolean validCategory = false;
            line.item = f;
            line.categories = '';
            for(TopicAssignment t : topicList){
                if (t.EntityId == f.Id) {
                    topics.add(t.Topic.Name);
                    line.categories += t.Topic.Name + ', ';
                    if (this.category == t.Topic.Name) {
                        validCategory = true;
                    }
                }
            }
            if (line.categories.length()>4) {
                line.categories = line.categories.substring(0, line.categories.length()-2);                
            }
            line.topics = topics;
 
            // is selected Category and Feed contains valid category OR category is not selected              
            if ((String.isNotEmpty(this.category) && (validCategory)) || String.isEmpty(this.category)) {
                items.add(line);            
            }           
        }
    }
         
    public List<Selectoption> getTopicItems(){
        List<SelectOption> options = new List<SelectOption>();  
        options.add(new SelectOption('', '-- select --')); 
         
        List<FeedItem> filesForTopics = [ SELECT Id, CreatedById, CreatedDate, Body, CommentCount, ContentFileName, ContentDescription, 
                                          InsertedById, ParentId, RelatedRecordId, Title, Type
                                           FROM FeedItem 
                                          WHERE Type = 'ContentPost' AND ParentId = :this.recordId 
                                          ORDER BY CreatedDate DESC
                                         ];        
         
        Set<Id> topicIds = new Set<Id> (new Map<Id,SObject>(filesForTopics).keySet());
                          
        List<TopicAssignment> topicList = [ SELECT Topic.Name, TopicId, Id, EntityId FROM TopicAssignment WHERE EntityId IN :topicIds ORDER BY Topic.Name ASC];
 
        Set<Id> myset = new Set<Id>();
        List<TopicAssignment> result = new List<TopicAssignment>();
        for (TopicAssignment t : topicList) {
            if (!myset.contains(t.TopicId)) {
                myset.add(t.TopicId);
                options.add(new SelectOption(t.Topic.Name, t.Topic.Name)); 
            }
        }
 
        return options;
    }
     
    public PageReference GoToNewFile() {
        PageReference pageRef = new PageReference('/apex/ChatterFilesNew');        
        pageRef.setRedirect(true);
        return pageRef;
    }    
}