global class WS_ZipUtil {

  //Normal Status Code
  private static String API_STATUS_NORMAL = '200';
  //Error Status Code
  private static String API_STATUS_ERROR  = '400';
    
  webService static String getAttachmentByParentId( String sfdcId ){
      
    if( String.isEmpty( sfdcId ) ) return errorJson('Parameter Id is required.');
    
    List<Attachment> attachmentList =  [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :sfdcId LIMIT 25];
    if( attachmentList == null || attachmentList.size() == 0 ) return errorJson('Attachment not found.');

    return wrapAttachmentList( attachmentList );
      
  }

  webService static String saveToDocument( String zipFileData, String fileName ){
    //try{
     
        String userId = UserInfo.getUserId();
      	List<Document> docList = [SELECT Id, Name, FolderId, Body FROM Document WHERE Name = :fileName AND FolderId = :userId LIMIT 1 ];
      	Document doc = new Document();
      
        if( docList == null || docList.size() == 0 ) {
        	doc.Name = fileName;
        	doc.FolderId = UserInfo.getUserId();
        	doc.Body = EncodingUtil.base64Decode( zipFileData );
        	insert doc;
        } else {
        	doc = docList.get(0);
        	doc.Body = EncodingUtil.base64Decode( zipFileData );
        	update doc;
      }
      return normalJson( doc.Id );
    //} catch ( Exception ex ) {
    //  return errorJson( ex.getMessage() );
    //}
  }
  
  private static String wrapAttachmentList( List<Attachment> attachmentList ){
    List<Object> dataList = new List<Object>();
    for( Attachment at : attachmentList ){
      Map<String, String> atMap = new Map<String, String>();
      //atMap.put( 'Id', at.Id );
      atMap.put( 'Name', at.Name );
      atMap.put( 'Body', EncodingUtil.base64Encode( at.Body ) );
      //atMap.put( 'ContentType', at.ContentType );
      dataList.add( atMap );
    }
    return normalJson( dataList );
  }


  public static String normalJson( Object respData ) {
    Map<String, Object> response = new Map<String, Object>();
    response.put('status', API_STATUS_NORMAL);
    if( respData != null ) response.put('data', respData);
    return JSON.serialize( response );
  }

  public static String errorJson( String message ) {
    Map<String, Object> response = new Map<String, Object>();
    response.put('status', API_STATUS_ERROR);
    if( message != null ) response.put('error', message);
    return JSON.serialize( response );
  }

}