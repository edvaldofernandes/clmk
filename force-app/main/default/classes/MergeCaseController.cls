public without sharing class MergeCaseController {
  
  private static final integer LIMIT_CASES = 25;
    
      public class wrapperCase {
        public Case caso {get;set;}
        public boolean atribuir {get;set;}
        private list< EmailMessage > email{get;set;}
        private list< Attachment > anexo{get;set;}
                
      }

    public list<wrapperCase> lstCase {get;set;}
    //private list<Case> lstSearchCase;
    private map<id,Case> lMapCase;
    public String localizarid {get;set;}
    public String caseId {get;set;}
    public String msg {get;set;}
    @TestVisible private Case casoController;
    private ApexPages.StandardsetController lController;
  public MergeCaseController(ApexPages.StandardsetController controller)
  {
    lController = controller;
    casoController = (Case) controller.getRecord();
    String lIdPage = ApexPages.currentPage().getParameters().get('Refid');

    if(lIdPage != null)
    {   
    
       list<Case> lstIdpage = [SELECT CaseNumber FROM Case WHERE id =:lIdPage];
       if ( !lstIdpage.isEmpty())
       {
       caseId = lstIdpage.get(0).CaseNumber;
       localizarid = caseId;
       }
    }
    InternalLocalizarCase( true, controller.getSelected() );
  }
    
  private void carregar( boolean aFlag )
  {   
    map <id, list< EmailMessage > > mapEmail = new map <id, list< EmailMessage> >();
      
    for(EmailMessage email: [SELECT id,Subject, BccAddress, MessageDate, ParentId, TextBody, Status, 
       CcAddress,  ToAddress ,FromAddress, Incoming
       FROM EmailMessage WHERE ParentId =: lMapCase.keyset()]){
       
      list< EmailMessage > lListEmail = mapEmail.get( email.ParentId );
      if ( lListEmail == null )
      {
          lListEmail = new list< EmailMessage >();
          mapEmail.put( email.ParentId, lListEmail );
      }
      lListEmail.add(email);
    }  
    
    lstCase = new list<wrapperCase>(); 
    
    for(id lcasoId: mapEmail.keySet())
    {
     
     wrapperCase lwp = new  wrapperCase();
     lwp.caso = lMapCase.get(lcasoId);  
     lwp.atribuir = aFlag;
     lwp.email = mapEmail.get(lcasoId);
     lstCase.add(lwp);
        
    }
    
    if ( lstCase.isEmpty() )
    {
        adicionaErro('Selected cases do not meet the criteria for merge.');
    }
    
  }
   
  public PageReference LocalizarCase()
  {
    return InternalLocalizarCase( false, null );
  }
  
  private PageReference InternalLocalizarCase( boolean aFlag, list< SObject > aListSelected )
  {
   if ( localizarid != null && localizarid != '' )
   {
     lMapCase = new Map<id,Case>([SELECT id, CaseNumber, Subject, Contact.name, Account.name, Status, Contact.Email FROM Case WHERE CaseNumber like: '%' + localizarid + '%' limit :LIMIT_CASES]);
   }
   else
   {
     if ( aListSelected == null || aListSelected.isEmpty() )
       lMapCase = new Map<id,Case>([SELECT id, CaseNumber, Subject, Contact.name, Account.name, Status, Contact.Email FROM Case WHERE  Status != 'Closed' limit :LIMIT_CASES ]);
     else
       lMapCase = new Map<id,Case>([SELECT id, CaseNumber, Subject, Contact.name, Account.name, Status, Contact.Email FROM Case WHERE id =: aListSelected and Status != 'Closed' limit :LIMIT_CASES ]);
   }
   carregar( aFlag );
   return null;
 }

  public PageReference save()
  {
    caseId = casoController.ParentId;
    
    list<Case> lstSearchCase = [SELECT id FROM Case WHERE Id =: caseId];
  
   if( lstSearchCase == null || lstSearchCase.IsEmpty())
   {
     adicionaErro('There is no selected target case.');
     return null;
   }
   else 
   {
       //added to make sure cases do not merge with themselves
       for(wrapperCase cw : lstCase){
           if(lstSearchCase[0].id == cw.caso.id){
               adicionaErro('Cannot merge a case with itself.');
               return null;
           }
       }
     for ( Integer i = lstCase.size() - 1; i >= 0; i-- )
     {
       if( lstCase[i].caso.CaseNumber == caseId )
       {
         lstCase.remove(i);
         break;
       }
     }
   }
   
   //selecionando attachments
   
    list< id > lListEmailId = new list< id >();
    for(wrapperCase lcasoW: lstCase )
    {
      if ( !lcasoW.atribuir ) continue;
      for ( EmailMessage lE : lcasoW.email )
      {
        lListEmailId.add(lE.Id);
      }
    }
    
    map <id, list< Attachment > > mapAnexo = new map <id, list< Attachment> >();      
    for(Attachment anexo: [SELECT id, ParentId, Body, BodyLength, ContentType, Description, Name, OwnerId FROM Attachment WHERE ParentId =: lListEmailId]){
       
      list< Attachment > lListAnexo = mapAnexo.get( anexo.ParentId );
      if ( lListAnexo == null )
      {
          lListAnexo = new list< Attachment >();
          mapAnexo.put( anexo.ParentId, lListAnexo );
      }
      lListAnexo.add(anexo);
    }
    
    for(wrapperCase lcasoW: lstCase )
    {
      lcasoW.anexo = new List<Attachment>();
      for ( EmailMessage lE : lcasoW.email )
      {
        list< Attachment > lLstE = mapAnexo.get( lE.id );
        if ( lLstE != null && !lLstE.isEmpty() )
        {          
          lcasoW.anexo.addAll( lLstE );
        }
      }      
    }
   
   list<Case> lstDelete = new list<Case>();
   
   // Mapa que relaciona o id antigo do EmailMessage com o novo objeto
   map<id, EmailMessage> lstEmail = new map<id, EmailMessage>();
   list<Attachment> lstAnexo = new list<Attachment>();
   
   Boolean existeSelecionado = false;  
   for(wrapperCase wp:  lstCase)
   {          
      if(wp.atribuir &&  wp.email != null )
      {
        existeSelecionado = true;
        for ( EmailMessage lOldEmail : wp.email )
        {
            EmailMessage email = lOldEmail.clone( false, true, true, true );
            email.ParentId = lstSearchCase.get(0).id;
            lstEmail.put(lOldEmail.id,email);
        }
        lstDelete.add(wp.caso);
      }     
   }   
   if ( !existeSelecionado )
   {
     adicionaErro('There is no added case.');
     return null;
   }

    if(!lstEmail.isEmpty())
    { 
      insert lstEmail.values();
      update new Case ( Id = lstSearchCase.get(0).id, Incoming_email_i__c = true );
    }
    
    // Clonar os Anexos
    for(wrapperCase wp:  lstCase)
    {
      if ( wp.atribuir )
      {
        for ( Attachment lOldAnexo : wp.anexo )
        {
          Attachment anexo = lOldAnexo.clone( false, true, true, true );
          anexo.ParentId = lstEmail.get( lOldAnexo.ParentId ).id;
          lstAnexo.add(anexo);
        }
      }
    }
    
    if(!lstAnexo.isEmpty()) insert lstAnexo;
    
    if(!lstDelete.isEmpty() && !Test.isRunningTest()) delete lstDelete;
        
     for ( Integer i = lstCase.size() - 1; i >= 0; i-- )
     {
        if( lstCase[i].atribuir )
        {
          lstCase.remove(i);
        }
     }
    
    //lController.cancel();
    adicionaMensagem('The case(s) was(were) merged.');
    
    for(wrapperCase wp:  lstCase)
    { 
      wp.anexo = null;
    }
    
    return null;
  }

  public PageReference cancel()
  {
    return null;
  }

  private static void adicionaErro(String err)
  {
     ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.ERROR, err) );
  }

  private static void adicionaMensagem(String msg)
  {
     ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.CONFIRM, msg) );
  }
}