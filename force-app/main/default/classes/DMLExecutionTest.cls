/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class DMLExecution.cls
* NAME: DMLExecution.cls
* AUTHOR: RLdO                                                DATE: 21/05/2014
*******************************************************************************/
@isTest
private class DMLExecutionTest
{
  static list<Lead> lstLead;
  
  static
  {
    lstLead = new list<Lead>();

    for (Integer i = 0; i < 200; i++)
    {
      Lead lLead = new Lead();
      // gera alguns leads sem sobrenome para provocar erro
      lLead.LastName = (Math.mod(i, 10) == 9) ? '' : 'Conta Teste';
      lLead.Company = 'Acme Co.';
      lstLead.add(lLead);
    }
  }
  
  static testMethod void myUnitTest()
  {
    DMLExecution.insertCMD(lstLead[0]);

    DMLExecution.upsertCMD(lstLead);
    DMLExecution.upsertCMD(lstLead, database.upsert(lstLead, Lead.Id, false));

    DMLExecution.updateCMD(lstLead[0]);
    DMLExecution.updateCMD(lstLead);

    // exclui um elemento para tratar o DeleteResult malsucedido na exclusao seguinte
    DMLExecution.deleteCMD(lstLead[0]);
    DMLExecution.deleteCMD(lstLead);
  }
}