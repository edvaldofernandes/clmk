/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Batch process that results in the inactivation of the expired Pricebook2,
* according to the field Expiration_date__c.
* 
* NAME: schInactivePricebookEntry.cls
* AUTHOR: LRSA                                                DATE: 02/10/2014
*
*******************************************************************************/

global class schInactivePricebookEntry implements Schedulable, Database.Batchable<SObject>{
  
  /*
  // Codigo para agendar via execute anonymous
  schInactivePricebookEntry sch = new schInactivePricebookEntry();
  String cron = '0 0 1 * * ?';
  system.schedule( 'Inactivate PricebookEntry', cron, sch );
  */
  
  global void execute(SchedulableContext sc)
  {
    Database.executeBatch( this, 200 );
  }

  global Iterable< sObject > start(Database.Batchablecontext bc)
  {
    return [ Select id from Pricebook2 where isActive = true and Expiration_date__c <= Today ];
  }

  global void execute(Database.Batchablecontext bc, list<SObject> scope)
  {
    for ( Pricebook2 lPB : ( List< Pricebook2 > ) scope )
    {
      lPB.isActive = false;
    }
    
    update scope;
  }
  
  global void finish(Database.Batchablecontext bc)
  {
    
  }

}