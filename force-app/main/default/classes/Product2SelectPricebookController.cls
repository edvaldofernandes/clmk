/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Controller class for the visualforce page Product2SelectPricebook 
*
* NAME: Product2SelectPricebookController.cls
* AUTHOR:JFS                                                DATE: 04/12/2014
*******************************************************************************/
public with sharing class Product2SelectPricebookController {

  public class pbwrapper
  {    
    public Boolean isSelected { get; set; }
    public Pricebook2 pb2 {get;set;} 
  
    public pbwrapper (Pricebook2 aPb, boolean isEmpty )
    {
      this.isSelected = aPb.IsStandard && isEmpty;   
      this.pb2 = aPb;
    }    
  }  
  public  list < pbwrapper > lstPbProd {get;set;}
  public String busca {get;set;}
  public string Idprod {get;set;}
  
  public boolean fPBEIsEmpty {get;set;}
  
  private Id recTypeTailored = RecordTypeMemory.getRecType('Product2', 'Tailored');
  private Id recTypeProd;
  
  public Product2SelectPricebookController(Apexpages.Standardcontroller controller)
  {
    idprod = apexpages.currentPage().getParameters().get('addTo');
    
    for ( Product2 prod : [SELECT RecordTypeId FROM Product2 WHERE Id =: idprod ])
    {
    	recTypeProd = prod.RecordTypeId;
    }
    
    fPBEIsEmpty = true;
    for ( PricebookEntry pbe : [SELECT Pricebook2Id FROM PricebookEntry 
      WHERE Product2Id =: idprod ])
    {
      fPBEIsEmpty = false;
      break;
    }        
        
    String query = ' SELECT Name, Description, LastModifiedDate, IsActive, IsStandard ' +
    ' FROM Pricebook2  WHERE ';
    if ( recTypeProd == recTypeTailored ) query += ' ( isStandard = true OR Type__c = \'Tailored\' ) AND ';
    query += ' IsActive = true ';
    query += ' ORDER BY Name ';
    
    lstPbProd = new list< pbwrapper >();
    list < Pricebook2 > lstPb = Database.query(query);

    for(Pricebook2 Pb: lstPb)
    {
      lstPbProd.add(new pbwrapper(Pb, fPBEIsEmpty ));
    }
    
  }
    
  public void CarregarPricebooks()
  {  
    String lquery = 'SELECT Name, Description, LastModifiedDate, IsActive, IsStandard FROM Pricebook2';

    list<String> lstCriteria = new list<String>();
    lstCriteria.add(' IsActive = true ');

    if (busca != null)
     lstCriteria.add('Name like \'%' + busca + '%\'');

    if ( recTypeProd == recTypeTailored )
      lstCriteria.add('( isStandard = true OR Type__c = \'Tailored\' ) ');

    if (!lstCriteria.isEmpty()) lQuery += ' WHERE ' + String.join(lstCriteria, ' AND ');
    
    lquery +=' ORDER BY Name';
    
    list < Pricebook2 > lPricebook = database.query(lquery);
    
    lstPbProd.clear();
    
    for(Pricebook2 Pb: lPricebook)
    {
      lstPbProd.add(new pbwrapper(Pb, fPBEIsEmpty ));
    }   
    
  }
  
  public PageReference Selected()
  {
    String lstpb = '';
        
    for(pbwrapper pb: lstPbProd)
    {
      if(pb.isSelected)
      {
        lstpb += pb.pb2.id + ',';
      }
    }
    
    PageReference page = new PageReference('/apex/ProductEditPricebook?id=' + Idprod +'&lstaPb=' + lstpb); 

    page.setRedirect(true);
    return page;
    
  }

}