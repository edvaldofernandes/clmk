@isTest
public class TeamMemberSchedullerTest {
    
    @isTest static void teamMemberSchedullerTest(){
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist sFInfoAircraftHist = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist();
        sFInfoAircraftHist.createdDate = Date.today();
        sFInfoAircraftHist.createdById = 'Test Rcp Callout';
        sFInfoAircraftHist.field = 'Test Rcp Callout';
        sFInfoAircraftHist.oldValue = 'Test Rcp Callout';
        sFInfoAircraftHist.newValue = 'Test Rcp Callout';
        sFInfoAircraftHist.parentId = 'Test Rcp Callout';
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist sfInfoAccountHist = new  RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist();
        sfInfoAccountHist.createdDate = Date.today();
        sfInfoAccountHist.createdById = 'Test Rcp Callout';
        sfInfoAccountHist.field = 'Test Rcp Callout';
        sfInfoAccountHist.oldValue = 'Test Rcp Callout';
        sfInfoAccountHist.newValue = 'Test Rcp Callout';
        sfInfoAccountHist.parentId = 'Test Rcp Callout';
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount sfInfoAccount = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount();
        sfInfoAccount.accountType = 'Test Rcp Callout';
        sfInfoAccount.companyName = 'Test Rcp Callout';
        sfInfoAccount.companyNickname = 'Test Rcp Callout';
        sfInfoAccount.icaoCode = 'Test Rcp Callout';
        sfInfoAccount.geographicalArea = 'Test Rcp Callout';
        sfInfoAccount.flyEmbraerID = 'Test Rcp Callout';
        sfInfoAccount.flyEmbraerName = 'Test Rcp Callout';
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember sfAccountTeamMember = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember();
        sfAccountTeamMember.teamMemberRole = 'Test Rcp Callout';
        sfAccountTeamMember.user_x = 'Test Rcp Callout';    
        
        List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist> sFInfoAircraftHistList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist>();
        sFInfoAircraftHistList.add(sFInfoAircraftHist);
        
        List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist> sfInfoAccountHistList = new  List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist>(); 
        sfInfoAccountHistList.add(sfInfoAccountHist);
        
        List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount> sfInfoAccountList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount>();
        sfInfoAccountList.add(sfInfoAccount);    
        
        List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember> sfAccountTeamMemberList = new List<RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember>();
        sfAccountTeamMemberList.add(sfAccountTeamMember);
        
        RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element sfInfoElement = new RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element();
        sfInfoElement.aircraftHistory = sFInfoAircraftHistList; 
        sfInfoElement.accountHistory = sfInfoAccountHistList;
        sfInfoElement.account = sfInfoAccountList;
        sfInfoElement.accountTeamMember = sfAccountTeamMemberList;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(sfInfoElement));
        
        EventConfiguration__c eventConfiguration = new EventConfiguration__c();
        eventConfiguration.Name = Constants.RCP_OUT_BOUND_PROXY;
        eventConfiguration.endPointUrl__c = 'http://ogwtest.com.br';
        insert eventConfiguration;
        
        BaseOnEvents__c base = new BaseOnEvents__c();
        base.sender__c = 'S_FORCE_COMERCIAL';
        base.receiver__c = 'RCP';
        base.BaseOn__c = 'SF_TO_RCP_TEAM_MEMBER';
        base.application__c = 'RCP_TEAM_MEMBER';
        insert base;
        
        EventTemplate event = new EventTemplate();
        
        BaseOnEvents__c baseEvents = event.buildInfoEvent('RCP_TEAM_MEMBER');
        
        event.buildInfoEvent('Received', 'RCP_TEAM_MEMBER');
        
        String jobId = System.schedule('Scheduled Job to Delete BET Denied Member Requests', '0 0 0 15 3 ? 2022', new TeamMemberScheduller());  
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id =: jobId];
        
        
        
        Test.stopTest(); 
        
        System.abortJob(jobId);
    }
}