/**
* @author Marcilio Leite de Souza
* @date 07/11/2018
* @description: Login Flow Controller redirect user to login            
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           07 NOV 2018             Original Version
* Renan de Castan e Carvalho		17 DEC 2019				Add possibility to show a second message
**/
public class LoginFlowController {
    public boolean isRendered {get;set;}

    public LoginFlowController(){
        isRendered = true;
    }

	/**
    * @description : Show second message
	* @return refresh page
    **/
	public PageReference redirectToSecondPage() {
		isRendered=false;
		return null;
    }
    
    /**
    * @description : Execute login and send to home
	* @return PageReference user home page
    **/
    public PageReference finishLoginFlowHome() {
        return Auth.SessionManagement.finishLoginFlow(System.currentPageReference().getParameters().get('retURL'));
    }
}