public class DashboardUtils {
    //CONSTANTS
    //************************************************************************************************************************************************
    //record types
    @TestVisible private static final Id recTypeCaso = RecordTypeMemory.getRecType('Case', 'PRC');
    @TestVisible private static final Id TYPE_ECIP = RecordTypeMemory.getRecType('Case', 'ECIP_Sales');
    @TestVisible private static final Id TYPE_REPAIR = RecordTypeMemory.getRecType('Case', 'Repair_Administ'); 
    @TestVisible private static final Id TYPE_OTHER = RecordTypeMemory.getRecType('Case', 'Other'); 
    @TestVisible private static final Id TYPE_NOTIF_DISCREPANCY = RecordTypeMemory.getRecType('Case', 'Notification_Discrepancy'); 
      
    //SLA
    public static final String VERMELHO = '3. Red';
    public static final String AMARELO = '2. Yellow';
    private static final String VERDE = '1. Green';
    private static final String RESPONSE = 'Response';
    private static final String ANSWERED = 'Answered';
    private static final String GREEN = '1. Green';
    private static final String YELLOW = '2. Yellow';
    private static final String RED = '3. Red';
    
    //picture urls
    private static final String CARINHA_NOK = 'Carinha_NOK.png';
    private static final String CARINHA_SUANDO = 'Carinha_Suando.png';
    private static final String CARINHA_OK = 'Carinha_OK.png';
    private static final String VAZIA = 'Vazia.png';
    private static final String USER = 'User.png';
    private static final String USER_UNKNOWN = 'UserUnkown.png';
    private static final String SETA_VERMELHA = 'Seta_Vermelha.png';
    private static final String SETA_VERDE = 'Seta_Verde.png';
    
    //Priority
    @TestVisible public static final String AOG_NFO = 'AOG NFO';
    @TestVisible public static final String AOG = 'AOG';
    @TestVisible public static final String CRI = 'Critical';
    @TestVisible public static final String RTN = 'Routine';
    @TestVisible public static final String CLOSE = 'Closed';
    @TestVisible public static final String OSS = 'OSS';
     
    //Variaveis Milestones
    @TestVisible private static final String MILE1='001_YOUR PROCESS TIME - ECIP';
    @TestVisible private static final String MILE2='002_YOUR PROCESS TIME - POOL';
    @TestVisible private static final String MILE3='003_YOUR PROCESS TIME - WARRANTY';
    @TestVisible private static final String MILE4='004_TIME TO CLOSE NOTIF - WARRANTY';
    @TestVisible private static final String MILE5='005_YOUR PROCESS TIME - BO ANALYZE';
    @TestVisible private static final String MILE6='006_WARRANTY TAT';
    @TestVisible private static final String MILE7='007_YOUR PROCESS TIME - EPEP CONTRACT';
    @TestVisible private static final String MILE8='008_YOUR PROCESS TIME - EPEP ORDER';
    @TestVisible private static final String MILE9='009_CONTRACTED TAT';
    @TestVisible private static final String MILE10='010_AOG NFO ORDER';
    @TestVisible private static final String MILE11='011_AOG NFO ACK';
    @TestVisible private static final String MILE12='012_AOG NFO POS';
    @TestVisible private static final String MILE13='013_AOG NFO EXT REQUEST';
    @TestVisible private static final String MILE14='014_AOG NFO INT REQUEST';
    @TestVisible private static final String MILE15='015_AOG ORDER';
    @TestVisible private static final String MILE16='016_AOG ACK';
    @TestVisible private static final String MILE17='017_AOG POS';
    @TestVisible private static final String MILE18='018_AOG EXT REQUEST';
    @TestVisible private static final String MILE19='019_AOG INT REQUEST';
    @TestVisible private static final String MILE20='020_CRITICAL ORDER';
    @TestVisible private static final String MILE21='021_CRITICAL POS';
    @TestVisible private static final String MILE22='022_CRITICAL EXT REQUEST';
    @TestVisible private static final String MILE23='023_CRITICAL INT REQUEST';
    @TestVisible private static final String MILE24='024_ROUTINE ORDER';
    @TestVisible private static final String MILE25='025_ROUTINE POS';
    @TestVisible private static final String MILE26='026_ROUTINE EXT REQUEST';
    @TestVisible private static final String MILE27='027_ROUTINE INT REQUEST';
    @TestVisible private static final String MILE28='028_AOG NFO EHS SHIPPING';
    @TestVisible private static final String MILE29='029_AOG EHS SHIPPING';
    @TestVisible private static final String MILE30='030_CRITICAL EHS SHIPPING';
    @TestVisible private static final String MILE31='031_ROUTINE EHS SHIPPING';
    @TestVisible private static final String MILE32='032_AOG NFO EHS DOCK';
    @TestVisible private static final String MILE33='033_AOG EHS DOCK';
    @TestVisible private static final String MILE34='034_CRITICAL EHS DOCK';
    @TestVisible private static final String MILE35='035_ROUTINE EHS DOCK';
    @TestVisible private static final String MILE36='INBOX time';
      
    //CaseReason
    @TestVisible public static final String INBOX = 'INBOX';
    @TestVisible private static final String ORDERS = 'Orders';    
    @TestVisible private static final String EXTERNAL = 'External Request';    
    @TestVisible private static final String INTERNAL = 'Internal Request';    
        
    //Status
    @TestVisible private static final String DISPATCH = 'Dispatch';
    @TestVisible private static final String PROCESSING = 'Processing';
    @TestVisible private static final String BACKORDER = 'Back Order';
    @TestVisible private static final String EHS = 'EHS/Dock';  
    @TestVisible private static final String EHSSHIP = 'EHS/Shiping'; 
    
    //priority dev name
    private static final map<String, String> MAP_REPORT_DEVNAME = new map<String, String>{
        INBOX => 'Inbox', AOG_NFO => 'AOG_NFO', AOG => 'AOG' , CRI => 'Critical', RTN => 'OSS', RTN => 'OSS'};
    private static final set<String> LISTA_MOTIVOS_RESPONSE = new set<String>{AOG_NFO, AOG, CRI, RTN, OSS};
      
    //lista milestones
    private static final set<String> LISTA_MILESTONES_RESPONSE = new set<String>{
        MILE6,MILE9,MILE10,MILE13,MILE14,MILE15,MILE18,MILE19,MILE20,MILE22,MILE23,MILE24,MILE26,MILE27};
    
    //FUNCTIONS
    //*************************************************************************************************************************************************
    //return today and month datetimes
    public static List<Datetime> getHojeAndMes(){
        Time zeroHour = Time.newInstance(0, 0, 0, 0);
        Datetime hoje = Datetime.newInstance(system.today(), zeroHour);
        Datetime inicioMes = Datetime.newInstance(Date.newInstance(hoje.year(), hoje.month(), 1), zeroHour);
        List<Datetime> HojeAndInicioMes = new List<Datetime>();
        HojeAndInicioMes.add(hoje);
        HojeAndInicioMes.add(inicioMes);
        return HojeAndInicioMes;
    }
    
    //build closed cases summary
    public static map<String, map<Boolean, Integer>> BuildClosedCasesSummary(list<AggregateResult> lstAggrCase, String sReason){
        map<String, map<Boolean, Integer>> mapCasesPassados = new map<String, map<Boolean, Integer>>();
        for (AggregateResult lResult: lstAggrCase){
            Boolean violated = (Boolean) lResult.get('IsViolated');
            String razao = (String)lResult.get('Priority');
            razao = (sReason != null) ? sReason : razao;
            map<Boolean, Integer> mapQtd = mapCasesPassados.get(razao);
            if (mapQtd == null){
                mapQtd = new map<Boolean, Integer>();
                mapCasesPassados.put(razao, mapQtd);
            }
            Integer iNewQtd = mapQtd.get(violated);
            iNewQtd = (iNewQtd == null) ? 0 : iNewQtd;
            iNewQtd += Integer.valueOf(lResult.get('qtd'));
            mapQtd.put(violated, iNewQtd);
        }
        return mapCasesPassados;
    }
    
    //set totals
    public static void calculaTotais(map<String, CasoWrapper> mapCasoWrapper){
        list<AggregateResult> lstCase = GetTotaisQuery();
        for (AggregateResult iResult : lstCase){
            String motivo = String.valueOf(iResult.get('Priority'));
            motivo = (motivo == 'OSS') ? 'Routine' : motivo;	//convert OSS priorities to Routine
            //String qtd = String.valueOf(iResult.get('qtd'));
            Integer qtd = Integer.valueOf(iResult.get('qtd'));
            
            CasoWrapper casoW = mapCasoWrapper.get(motivo);
            
            if(casoW == null){
                casoW = new CasoWrapper();
                mapCasoWrapper.put(motivo, casoW);
            }
            casoW.quantidadeTotal = (casoW.quantidadeTotal == null) ? String.valueOf(qtd) : String.valueOf(qtd + Integer.valueOf(casoW.quantidadeTotal));	//add qtd to the total
        }
    }
    
    //calculate values for each traffic light
    public static map<String, map<String, Integer>> calculaSemaforo(){
        map<String, map<String, Integer>> mapCasosPorMotivo = new map<String, map<String, Integer>>();
        list<AggregateResult> lstCasosPorMotivoStatus = GetSemaforoCases();
        for (AggregateResult iResult : lstCasosPorMotivoStatus ){
            String motivo = String.valueOf(iResult.get('Priority'));
            motivo = (motivo == 'OSS') ? 'Routine' : motivo; //convert OSS priorities to routine
            map<String, Integer>  mapTemp = mapCasosPorMotivo.get(motivo);
            if ( mapTemp == null ){
                mapTemp = new map<String, Integer>();
                mapCasosPorMotivo.put(motivo, mapTemp);
            }
            string SLAStatus = String.valueOf(iResult.get('SLA_report_status__c'));	//get SLA Status
            integer quantityResult = Integer.valueOf(iResult.get('qtd'));			//get quantity for this individual result
            integer quantityTotal = (mapTemp.get(SLAStatus) == null) ? quantityResult : mapTemp.get(SLAStatus) + quantityResult;	//if the SLA already has a quantity, add to it
            mapTemp.put(SLAStatus, quantityTotal);
        }
        return mapCasosPorMotivo;
    }
    
    //set the arrow and face for each case wrapper
    public static void verificaStatus(CasoWrapper casoW){
        if(casoW.month != null && casoW.lastDay != null){
            if(Integer.valueOf(casoW.month) > Integer.valueOf(casoW.lastDay)){
                casoW.seta = SETA_VERMELHA;
            }else if(Integer.valueOf(casoW.month) < Integer.valueOf(casoW.lastDay)){
                casoW.seta = SETA_VERDE;
            }
        }
        casoW.month = (casoW.month != null) ? casoW.month + ' %' : '-- %';
        casoW.lastDay = (casoW.lastDay != null) ? casoW.lastDay + ' %' : '-- %';
        if(casoW.quantidadeVermelho != null && Integer.valueOf(casoW.quantidadeVermelho) > 0){
            casoW.urlCarinha = CARINHA_NOK;
        }else if(casoW.quantidadeAmarelo != null && Integer.valueOf(casoW.quantidadeAmarelo) > 0){
            casoW.urlCarinha = CARINHA_SUANDO;
        }else{
            casoW.urlCarinha = CARINHA_OK;
        }
    }
    
    public static String fmtPercent(Double dPart, Double dTotal){
        Double lPercent = (dPart / dTotal) * 100;
        return String.valueOf(Math.roundToLong(lPercent));
    }
    
    //QUERIES
    //***********************************************************************************************************************************************
    //get totais
    public static List<AggregateResult> GetTotaisQuery(){
      return [SELECT  Case.Priority  , COUNT(Case.Id) qtd
      		  FROM  CaseMilestone
      		  WHERE((MilestoneType.Name = :MILE36 AND Case.Priority = :INBOX)
                    OR (MilestoneType.Name IN :LISTA_MILESTONES_RESPONSE AND Case.Priority IN :LISTA_MOTIVOS_RESPONSE AND Case.Status = :PROCESSING))
                    //OR(MilestoneType.Name IN :LISTA_MILESTONES_RESPONSE AND Case.Priority IN :LISTA_MOTIVOS_RESPONSE AND Case.Status = : EHS)
      				AND Case.SLA_report_status__c != :ANSWERED
      				AND Case.RecordTypeId != :TYPE_OTHER
              		AND Case.RecordTypeId != :TYPE_NOTIF_DISCREPANCY
      				AND Case.Status != :CLOSE			 
      				GROUP BY Case.Priority];
    }
    
    public static map<String, map<Boolean, Integer>> retornaTotaisCasosFechadosResponse(DateTime dataInicio, DateTime dataFim){
        list<AggregateResult> lstCasePassadosTotais = [SELECT Case.Priority, IsViolated, count(CaseId) qtd
                                                       FROM CaseMilestone
                                                       WHERE MilestoneType.Name in :LISTA_MILESTONES_RESPONSE
                                                           AND Case.Priority IN :LISTA_MOTIVOS_RESPONSE
                                                           AND Case.CreatedDate >= :dataInicio AND Case.CreatedDate < :dataFim
                                                           AND Case.RecordTypeId != :TYPE_NOTIF_DISCREPANCY
                                                           AND Case.To_delete__c = false
                                                           AND Case.Status != :CLOSE
                                                           //AND Case.Type IN ('PO', 'Quote')
                                                           GROUP BY Case.Priority, IsViolated];
        return BuildClosedCasesSummary(lstCasePassadosTotais, null);
    }
    
    public static map<String, map<Boolean, Integer>> retornaTotaisCasosFechadosInbox(DateTime dataInicio, DateTime dataFim){
        list<AggregateResult> lstCasePassadosTotais = [SELECT Case.Priority, IsViolated, count(CaseId) qtd
                                                       FROM CaseMilestone
                                                       WHERE MilestoneType.Name = :MILE36
                                                           AND CompletionDate >= :dataInicio AND CompletionDate < :dataFim
                                                           AND Case.RecordTypeId != :TYPE_NOTIF_DISCREPANCY
                                                           AND Case.Priority = :INBOX
                                                           AND Case.To_delete__c = false
                                                           AND Case.Status != :CLOSE
                                                           GROUP BY Case.Priority, IsViolated];
        return BuildClosedCasesSummary(lstCasePassadosTotais, INBOX);
    }
    
    
    /*//get closed cases
    public static list<AggregateResult> retornaTotaisCasosFechadosResponse(DateTime dataInicio, DateTime dataFim){
        return [SELECT Case.Priority, IsViolated, count(CaseId) qtd
                FROM CaseMilestone
                WHERE MilestoneType.Name in :LISTA_MILESTONES_RESPONSE
                    AND Case.Priority IN :LISTA_MOTIVOS_RESPONSE
                    AND Case.CreatedDate >= :dataInicio
                    AND Case.CreatedDate < :dataFim
                    //AND Case.RecordTypeId = :recTypeCaso
                    //AND Case.To_delete__c = false
                    AND Case.Status != :CLOSE
                    //AND Case.Type IN ('PO', 'Quote')
                    GROUP BY Case.Priority, IsViolated];
    }
    
    //get closed cases inbox
    public static List<AggregateResult> retornaTotaisCasosFechadosInbox(DateTime dataInicio, DateTime dataFim){
        return [SELECT Case.Priority, IsViolated, count(CaseId) qtd
                FROM CaseMilestone
                WHERE MilestoneType.Name = :MILE36
                    AND CompletionDate >= :dataInicio AND CompletionDate < :dataFim
                    //AND Case.RecordTypeId = :recTypeCaso
                    AND Case.Priority = :INBOX
                    AND Case.To_delete__c = false
                    AND Case.Status != :CLOSE
                    GROUP BY Case.Priority, IsViolated];
    }*/
    
    //get cases grouped by priority and sla status
    public static List<AggregateResult> GetSemaforoCases(){
        List<AggregateResult> results = [SELECT Case.Priority, Case.SLA_report_status__c, COUNT(Case.Id) qtd
                FROM CaseMilestone
                WHERE ((MilestoneType.Name = :MILE36 AND Case.Priority = :INBOX)
                       OR
                       (MilestoneType.Name IN :LISTA_MILESTONES_RESPONSE AND Case.Priority IN :LISTA_MOTIVOS_RESPONSE AND Case.Status = :PROCESSING))
                    AND Case.SLA_report_status__c != :ANSWERED
                    AND Case.RecordTypeId != :TYPE_NOTIF_DISCREPANCY
                    AND Case.SLA_report_status__c != null
                    AND Case.Status != :CLOSE
                    AND Case.RecordTypeId != :TYPE_OTHER
                    GROUP BY Case.Priority, Case.SLA_report_status__c ];
        List<AggregateResult> results2 = [SELECT Case.Priority, Case.SLA_report_status__c, Case.Id qtd
                FROM CaseMilestone
                WHERE ((MilestoneType.Name = :MILE36 AND Case.Priority = :INBOX)
                       OR
                       (MilestoneType.Name IN :LISTA_MILESTONES_RESPONSE AND Case.Priority IN :LISTA_MOTIVOS_RESPONSE AND Case.Status = :PROCESSING))
                    AND Case.SLA_report_status__c != :ANSWERED
                    AND Case.RecordTypeId != :TYPE_NOTIF_DISCREPANCY
                    AND Case.SLA_report_status__c != null
                    AND Case.Status != :CLOSE
                    AND Case.RecordTypeId != :TYPE_OTHER
                    GROUP BY Case.Priority, Case.Id, Case.SLA_report_status__c ];
        for(AggregateResult result : results2){
            system.debug(result.get('Priority') + ' ' + result.get('qtd'));
        }
        return results;
    }
    
    //ARCHIVE - unused code
    //*************************************************************************************************************************************************
    //calculate remaining time - does not work
    /*private void calcularTempoRestante(String reason)
  {
    set<String> setReasons = (reason == INBOX) ? MAP_REPORT_DEVNAME.keyset() : new set<String>{ reason };

    CasoWrapper casoW = mapCasoWrapper.get(reason);
    if ( casoW == null ) return;

       String query = 'SELECT CaseId, TargetDate, Case.Priority, Case.BusinessHoursId, Case.CreatedDate, Case.OwnerId, ' +
      'Case.SLA_report_status__c, Case.ContactId, Case.Owner.Name, Case.Contact.Name, Case.Contact.AccountId, ' +
      'Case.Contact.Account.Name ' +
      'FROM CaseMilestone WHERE ';
     query += 'MilestoneType.Name = \'' + ( reason == INBOX  ? MILE36 : MILE22 ) + '\' '; 
    query += ' AND Case.Priority = : INBOX ';
    query +=
     // 'AND Case.RecordTypeId = \'' + recTypeCaso + '\' ' +
      'AND Case.SLA_report_status__c != \'Answered\'  ' +
      'AND Case.SLA_report_status__c != null ' +
      'ORDER BY Case.CreatedDate asc limit 1';

    list<CaseMilestone> lstTempoRestanteInbox = Database.query(query);

      for ( CaseMilestone iCaseMile : lstTempoRestanteInbox ){}
    /*{
      Long tempo = BusinessHours.diff( iCaseMile.Case.BusinessHoursId, system.now(), iCaseMile.TargetDate );
      Long segundosTemp = math.mod((tempo/1000), 60);
      Long minutosTemp = math.mod((tempo/1000/60), 60);
      Long horasTemp = tempo/1000/60/60;

      String segundos = fmtZeroLeft(String.valueOf(math.abs(segundosTemp)), 2);
      String minutos = fmtZeroLeft(String.valueOf(math.abs(minutosTemp)), 2);
      String horas = fmtZeroLeft(String.valueOf(math.abs(horasTemp)), 2);

      casoW.rTime = ( horasTemp < 0 || minutosTemp < 0  ? '-' : '' ) +  horas + ':' + minutos + ':' + segundos;
      casoW.atendente = iCaseMile.Case.Owner.Name;
      casoW.idAtendente = iCaseMile.Case.OwnerId;
      casoW.dataH = iCaseMile.Case.CreatedDate.Date().format();
      casoW.hora = iCaseMile.Case.CreatedDate.format('HH:mm:ss');
      casoW.contato = iCaseMile.Case.Contact.Name;
      casoW.idContato = iCaseMile.Case.ContactId;
      casoW.idCaso = iCaseMile.CaseId;
      
       if ( iCaseMile.Case.SLA_report_status__c.contains(VERMELHO) ) casoW.status = VERMELHO;
       else if ( iCaseMile.Case.SLA_report_status__c.contains(AMARELO) ) casoW.status = '#ffd700';
       else casoW.status = VERDE;

              
      //se não tiver atendente
      casoW.ownerIsQueue = casoW.idAtendente.getSObjectType() != Schema.User.SObjectType;
      if ( casoW.ownerIsQueue  )
        casoW.urlFotoUser = USER_UNKNOWN;
    }*/
    /*if ( casoW.rTime == null ) casoW.rTime = '--:--:--';
    }*/
}