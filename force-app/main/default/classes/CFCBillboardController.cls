public class CFCBillboardController {
   
    public Map<String,Billboard__c> mapBillboardImages {get;set;}
    public Map<String,Billboard__c> mapBillboardImagesTablet {get;set;}
    public Map<String,Billboard__c> mapBillboardImagesMobile {get;set;}
    
    public Map<String, String> mapBannerBillboard {get; set;}
    public Map<String, String> mapBannerBillboardTablet {get; set;}
    public Map<String, String> mapBannerBillboardMobile {get; set;}
    
    List<Billboard__c> lstBillboard {get; set;}
    
    public Boolean isShowBillboardDesktop {get;set;}
    public Boolean isShowBillboardTablet {get;set;}
    public Boolean isShowBillboardMobile {get;set;}
    
    public String intervalBillboard {get;set;}
    
    public CFCBillboardController(){
        System.debug('CFCBillboardController inicio');
        GenerateBillboard();
        GenerateIntervalBillboard();
        System.debug('CFCBillboardController Fim');
    }
    
    public void GenerateIntervalBillboard(){
        system.debug('CFCHomeController.GenerateViewProducts.GenerateIntervalBillboard()');
        intervalBillboard = '3000';
        
        CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults();
        if(cs.Billboard_Time__c != 0 && cs.Billboard_Time__c != null){
            system.debug('CFCHomeController.GenerateViewProducts.cs.Billboard_Time__c >>> ' + cs.Billboard_Time__c);
            intervalBillboard = string.valueOf(cs.Billboard_Time__c * 1000);
        }
        
        system.debug('CFCHomeController.GenerateViewProducts.intervalBillboard >>> ' + intervalBillboard);
    }
    
    public void GenerateBillboard(){
        system.debug('CFCHomeController.GenerateViewProducts.GenerateBillboard()');
        
        String idBillboard = ApexPages.currentPage().getParameters().get('id');
        system.debug('CFCBillboardController.GenerateBillboard.idBillboard >>> ' + idBillboard);
        
        if(!String.isBlank(idBillboard)){
             List<Billboard__c> lstBillboard = BillboardDao.getInstance().listById(idBillboard);
            system.debug('CFCHomeController.GenerateBillboard.lstBillboard >>> ' + lstBillboard);
            
            mapBillboardImages = new Map<String,Billboard__c>();
            mapBillboardImagesTablet = new Map<String,Billboard__c>();
            mapBillboardImagesMobile = new Map<String,Billboard__c>();
            
            mapBannerBillboard = new Map<String, String>();
            mapBannerBillboardTablet = new Map<String, String>();
            mapBannerBillboardMobile = new Map<String, String>();
            
            GenerateBannerBillboard(lstBillboard);
        }        
    }  
    
    public void GenerateBannerBillboard(List<Billboard__c> lstBillboard){
        System.debug('CFCHomeController.GenerateBannerBillboard()');
        
        isShowBillboardDesktop = false;
        isShowBillboardTablet = false;
        isShowBillboardMobile = false;
        
        Set<String> setIdBillboard = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Billboards');
        System.debug('CFCHomeController.GenerateBannerBillboard.rType >>> ' + rType.Id);
        
        //creating a set of products, just to ensure that there is not equals id
        for(Billboard__c item : lstBillboard){
            setIdBillboard.add(item.id);
            mapBillboardImages.put(item.Id, item);
        }
        System.debug('CFCHomeController.GenerateBannerBillboard.setIdBillboard >>> ' + setIdBillboard);
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetBillboardIdAndRecordTypeId(setIdBillboard, rType.Id);
        System.debug('CFCHomeController.GenerateBannerBillboard.lstAttch >>> ' + lstAttch);
        System.debug('CFCHomeController.GenerateBannerBillboard.lstAttch.size() >>> ' + lstAttch.size());
        
         
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            if(lstAttch.get(i).Attachments.size() > 0){ //set image billboard in your map by device
                if(lstAttch.get(i).Device_Type__c == 'Mobile'){
                    mapBannerBillboardMobile.put(lstAttch.get(i).Billboard__c, lstAttch.get(i).Attachments[0].id);
                } else if (lstAttch.get(i).Device_Type__c == 'Tablet'){
                    mapBannerBillboardTablet.put(lstAttch.get(i).Billboard__c, lstAttch.get(i).Attachments[0].id);
                } else{
                    mapBannerBillboard.put(lstAttch.get(i).Billboard__c, lstAttch.get(i).Attachments[0].id);
                }
            } else { //remove billboard without images
                system.debug('item >>> NULL');
                if(mapBillboardImages.containsKey(lstAttch.get(i).Billboard__c)){
                    mapBillboardImages.remove(lstAttch.get(i).Billboard__c);
                }
            }            
        }
        
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboard >>> ' + mapBannerBillboard);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboardMobile >>> ' + mapBannerBillboardMobile);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBannerBillboardTablet >>> ' + mapBannerBillboardTablet);
        
        //fill map billboard by device according with map image by device
        for(Billboard__c item : lstBillboard){
            if(mapBannerBillboardTablet.containsKey(item.Id)){
                mapBillboardImagesTablet.put(item.Id, item);
            }
            
            if(mapBannerBillboardMobile.containsKey(item.Id)){
                mapBillboardImagesMobile.put(item.Id, item);
            }
        }
        
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImages >>> ' + mapBillboardImages);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImagesTablet >>> ' + mapBillboardImagesTablet);
        System.debug('CFCHomeController.GenerateBannerBillboard.mapBillboardImagesMobile >>> ' + mapBillboardImagesMobile);   
        
        //prevent crash in case of billboard published but without images
        isShowBillboardDesktop = mapBannerBillboard.size() > 0 ? true : false;
        isShowBillboardTablet = mapBannerBillboardTablet.size() > 0 ? true : false;
        isShowBillboardMobile = mapBannerBillboardMobile.size() > 0 ? true : false;
    }  
}