public without sharing class RECTriggerHandler {
  Id idRECRecordType;
  Map<Id, Communications__c> newRec;

  public RECTriggerHandler(List<Communications__c> newRecords) {
    idRECRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('REC')
      .getRecordTypeId();
    newRec = getIncomingREC(newRecords);
  }

  public Id getIdRECRecordType() {
    return this.idRECRecordType;
  }

  public Map<Id, Communications__c> getNewREC() {
    return this.newRec;
  }

  /*
   * @Return new map for Record type REC
   */
  public Map<Id, Communications__c> getIncomingREC(
    List<Communications__c> newRecords
  ) {
    Map<Id, Communications__c> mapAux = new Map<Id, Communications__c>();
    for (Communications__c obj : newRecords) {
      if (obj.RecordTypeId == this.idRECRecordType) {
        mapAux.put(obj.Id, obj);
      }
    }
    return mapAux;
  }

  /*
   * @Return normalized fleet type (e.g. 145/170/190 -> [ERJ, EJet] or 170-E2/190-E2 -> [E2]...)
   */
  public List<String> normalizeRECFleetType(String recFleetType) {
    List<String> fleetTypes = new List<String>{ '-', '-' };

    if (recFleetType.contains('E2')) {
      fleetTypes[0] = 'E2';
    } else {
      if (Pattern.matches('.*(170|175|190|195).*', recFleetType)) {
        fleetTypes[0] = 'EJet';
      }
      if (Pattern.matches('.*(145).*', recFleetType)) {
        fleetTypes[1] = 'ERJ';
      }
    }

    return fleetTypes;
  }

  /*
   * @Return Get Contact and User Ids to send the REC Email
   */
  public Map<Id, String> getEmailsREC() {
    Map<Id, String> emails = new Map<Id, String>();
    // List<Id> emailsId = new List<Id>();
    // List<String> emailsListed = new List<String>(); //to avoid send duplicated emails to the same person
    List<String> recFleetTypes = this.normalizeRECFleetType(
      this.newRec.values().get(0).Fleet_Type__c
    );

    for (Contact c : [
      SELECT Id, Email
      FROM Contact
      WHERE
        EmailBouncedDate = NULL
        AND Relevant_Events_Communication__c = TRUE
        AND Contact_Status__c = 'Active'
        AND AccountId IN (
          SELECT Id
          FROM Account
          WHERE
            (Account_type__c = 'Airline'
            AND Company_Status__c IN ('In EIS Process', 'Active')
            AND Embraer_a_c_in_service_only__c > 0
            AND Embraer_Fleet_Type__c INCLUDES (
              :recFleetTypes[0],
              :recFleetTypes[1]
            ))
            OR Account_Type__c = 'Embraer Site'
        )
    ]) {
      if (!(emails.values().contains(c.Email))) {
        emails.put(c.Id, c.Email);
      }
      // emailsId.add(c.Id);
      // emailsListed.add(c.Email);
    }

    for (User s : [
      SELECT Id, Email
      FROM User
      WHERE REC_Relevant_Event_Communication__c = TRUE
    ]) {
      if (!(emails.values().contains(s.Email))) {
        emails.put(s.Id, s.Email);
        // emailsId.add(s.Id);
        // emailsListed.add(s.Email);
      }
    }
    return emails;
  }

  public List<Object> slice(List<Object> input, Integer ge, Integer l) {
    List<Object> output = input.clone();
    for (Integer i = 0; i < ge; i++) {
      output.remove(0);
    }
    Integer elements = l - ge;
    while (output.size() > elements) {
      output.remove(elements);
    }
    return output;
  }

  public Id getEmailTemplate() {
    Id emailId;
    emailId = [
      SELECT id
      FROM emailTemplate
      WHERE developerName = 'REC_TEMPLATE'
    ]
    .Id;

    return emailId;
  }

  public List<Messaging.EmailFileAttachment> getRelatedAttachments() {
    Id recId = this.newRec.values().get(0).Id;
    List<Id> selectedAttachIds = new List<Id>();
    for (
      SelectOption op : MultiselectAttachmentsController.getPersistedSelectedAttachs(
        recId
      )
    ) {
      selectedAttachIds.add(op.getValue());
    }

    List<ContentDocumentLink> docIds = [
      SELECT ContentDocumentId
      FROM ContentDocumentLink
      WHERE LinkedEntityId = :recId AND ContentDocumentId IN :selectedAttachIds
    ];

    List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
    for (ContentDocumentLink docId : docIds) {
      ContentVersion attachData = [
        SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId
        FROM ContentVersion
        WHERE ContentDocumentId = :docId.ContentDocumentId
        LIMIT 1
      ];
      Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
      attachment.setBody(attachData.VersionData);
      attachment.setFileName(
        attachData.Title +
        '.' +
        attachData.FileType.toLowerCase()
      );
      attachments.add(attachment);
    }
    return attachments;
  }

  public void sendEmailPreview() {
    List<Messaging.EmailFileAttachment> attachments = this.getRelatedAttachments();

    Id emailTemplateId = this.getEmailTemplate();
    List<String> mailingList = this.getEmailsREC().values();
    Id dummyContactId = [SELECT id FROM Contact LIMIT 1].Id;
    List<OrgWideEmailAddress> owea = [
      SELECT Id
      FROM OrgWideEmailAddress
      WHERE Address = 'fleet.performance@embraer.net.br'
    ];
    // SEND TEMPLATE EMAIL
    Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
    singleEmail.setToAddresses(
      new List<String>{ this.newRec.values().get(0).Email_Preview__c }
    );
    singleEmail.setTemplateId(emailTemplateId);
    singleEmail.setFileAttachments(attachments);
    singleEmail.setWhatId(this.newRec.values().get(0).Id); // Fill the template according the REC Id
    singleEmail.setTargetObjectId(dummyContactId); // To set WhatId, the TargetObjectId needs to be a Contact
    singleEmail.setTreatTargetObjectAsRecipient(false); // Avoid to send the email to the target object Contact
    singleEmail.setSaveAsActivity(false);
    singleEmail.setUseSignature(false);
    if (!Test.isRunningTest()) {
      if (owea.size() > 0) {
        singleEmail.setOrgWideEmailAddressId(owea.get(0).Id);
      }
      Messaging.sendEmail(
        new List<Messaging.SingleEmailMessage>{ singleEmail }
      );
    }
    // SEND MAILING LIST
    Messaging.SingleEmailMessage mailingListEmail = new Messaging.SingleEmailMessage();
    mailingListEmail.setSubject('Mailing List REC');
    mailingListEmail.setPlainTextBody(String.join(mailingList, '\n'));
    mailingListEmail.setToAddresses(
      new List<String>{ this.newRec.values().get(0).Email_Preview__c }
    );
    if (!Test.isRunningTest()) {
      if (owea.size() > 0) {
        mailingListEmail.setOrgWideEmailAddressId(owea.get(0).Id);
      }
      Messaging.sendEmail(
        new List<Messaging.SingleEmailMessage>{ mailingListEmail }
      );
    }
  }

  public void sendEmailToRECInterests() {
    List<Messaging.EmailFileAttachment> attachments = this.getRelatedAttachments();

    Id emailTemplateId = this.getEmailTemplate();
    List<Id> emailIds = new List<Id>(this.getEmailsREC().keySet());
    Id dummyContactId = [SELECT id FROM Contact LIMIT 1].Id;
    List<OrgWideEmailAddress> owea = [
      SELECT Id
      FROM OrgWideEmailAddress
      WHERE Address = 'fleet.performance@embraer.net.br'
    ];
    for (Integer i = 0; i < emailIds.size() / 140 + 1; i++) {
      //split the list in lists of 100 ids (maximum)
      List<Id> subsetEmailIds = (List<Id>) slice(
        emailIds,
        i * 140,
        (i + 1) * 140
      );
      Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
      singleEmail.setBccAddresses(subsetEmailIds);
      singleEmail.setTemplateId(emailTemplateId);
      singleEmail.setFileAttachments(attachments);
      singleEmail.setWhatId(this.newRec.values().get(0).Id); // Fill the template according the REC Id
      singleEmail.setTargetObjectId(dummyContactId); // To set WhatId, the TargetObjectId needs to be a Contact
      singleEmail.setTreatTargetObjectAsRecipient(false); // Avoid to send the email to the target object Contact
      singleEmail.setSaveAsActivity(false);
      singleEmail.setUseSignature(false);
      if (!Test.isRunningTest()) {
        if (owea.size() > 0) {
          singleEmail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        Messaging.sendEmail(
          new List<Messaging.SingleEmailMessage>{ singleEmail },
          false
        );
      }
    }
  }

  public void changeTextAreaImagePrivateToPublic() {
    // REC_DOCController imgControl = new REC_DOCController();
    REC_DOCController.hostImageInPublicDomain(this.newRec.values().get(0).Id);
  }
}