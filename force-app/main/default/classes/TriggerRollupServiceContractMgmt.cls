/**
* @author Marcilio Leite de Souza
* @date 28/08/2018
* @description: Class for rollup Service Contract Management and update the parent object       
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
public class TriggerRollupServiceContractMgmt {

    public List<Service_Contract_Management__c> listServiceContractMgmt;
    private GEN_LREngine.Context ctx;
    
    private static final String INVOICE_STATUS = 'Invoice_status__c = \'INVOICED\'';
    
    /**
    * @description : Constructor for TriggerRollupServiceContractMgmt
    * @param List<Service_Contract_Management__c> listServiceContractMgmt : List of Service Contract Management in the trigger execution
    **/
    public TriggerRollupServiceContractMgmt(List<Service_Contract_Management__c> listServiceContractMgmt){
        this.listServiceContractMgmt = listServiceContractMgmt;
		
        executeServiceContractRollup();
    }
    
    /**
    * @description : Execute and persist the rollup for all Service Contract fields with their bussines rule
	* @return void
    **/
    private void executeServiceContractRollup(){
        Map<String, ServiceContract> mapIdServiceContractByServiceContract = new Map<String, ServiceContract>();     
        List<ServiceContract> listServiceContract = new List<ServiceContract>();
        
        GEN_LREngine.RollupSummaryField aggregateField = new GEN_LREngine.RollupSummaryField(Schema.SObjectType.ServiceContract.fields.Recognized_NREC__c,
                                           							 								Schema.SObjectType.Service_Contract_Management__c.fields.Amount_of_this_receivable__c,
                                           							 								GEN_LREngine.RollupOperation.Sum);
        
        ctx = new GEN_LREngine.Context(ServiceContract.SobjectType, 
                                       Service_Contract_Management__c.SobjectType,
                                       Schema.SObjectType.Service_Contract_Management__c.fields.Service_Contract__c,
                                       INVOICE_STATUS,
                                       aggregateField
                                       );
        
        listServiceContract.addAll((List<ServiceContract>) GEN_LREngine.rollUp(ctx, listServiceContractMgmt));
        
        //Change the aggregatefield and where clause
        aggregateField.master =  Schema.SObjectType.ServiceContract.fields.Sum_of_All_Receivables__c;
        ctx.detailWhereClause = '';
        
        listServiceContract.addAll((List<ServiceContract>) GEN_LREngine.rollUp(ctx, listServiceContractMgmt));

        for(ServiceContract obj : listServiceContract){
            if(mapIdServiceContractByServiceContract.containsKey(obj.Id)){
                if(obj.Recognized_NREC__c != null){mapIdServiceContractByServiceContract.get(obj.Id).Recognized_NREC__c = obj.Recognized_NREC__c;}
                if(obj.Sum_of_All_Receivables__c != null){mapIdServiceContractByServiceContract.get(obj.Id).Sum_of_All_Receivables__c = obj.Sum_of_All_Receivables__c;}
                         
            }else{
                mapIdServiceContractByServiceContract.put(obj.Id, obj);
            }
        }
        system.debug('TriggerRollupServiceContractMgmt.mapIdServiceContractByServiceContract.values(): ' + mapIdServiceContractByServiceContract.values());
        if(mapIdServiceContractByServiceContract.values().size() > 0){GEN_TriggerHelper.updateObjectListTriggerDisabled(mapIdServiceContractByServiceContract.values());}
    }
}