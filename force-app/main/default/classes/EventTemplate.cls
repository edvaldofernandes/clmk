public class EventTemplate implements IEvent{
    
    private BaseOnEvents__c baseOnEvents;
    private  Queue__c event;
    
    public void buildInfoEvent (List<sObject> obj, String status, String error) {

        buildInfoEvent(obj);
        buildEventObject(obj, status, error);
        saveEvent();
    }
    
    public void buildInfoEvent(String response, String obj){
        
        buildInfoEvent(obj);
        buildEventObject(response);
        saveEvent();
    }
    
    public BaseOnEvents__c buildInfoEvent(String obj){
        
        baseOnEvents = [SELECT id, BaseOn__c, CreateFor__c, sender__c, receiver__c, application__c 
                        FROM BaseOnEvents__c
                        WHERE application__c =: obj];
        return baseOnEvents;
    }
    
    public BaseOnEvents__c buildInfoEvent(List<sObject> obj){
        
        baseOnEvents = [SELECT id, BaseOn__c, CreateFor__c, sender__c, receiver__c, application__c 
                        FROM BaseOnEvents__c
                        WHERE application__c =: String.valueOf(obj.get(0).getsObjectType())];
        return baseOnEvents;
    }

	public Queue__c buildEventObject(String response){
        
        EventBuilder builder = new EventBuilder();
        event = new Queue__c();
        
		event = builder.createEventBaseOn(event, baseOnEvents.BaseOn__c)
            		   .createEventFor(baseOnEvents.CreateFor__c)
					   .withPayload( JSON.serialize(response) )
					   .withSender (baseOnEvents.sender__c)
					   .withReceiver (baseOnEvents.receiver__c)
					   .build();
        event.businessDocumentNumber__c = baseOnEvents.id;
        event.internalId__c = baseOnEvents.id;  
        event.status__c = EventQueueStatusType.SUCCESS.name();
        return event;
    }
    
    public Queue__c buildEventObject(List<sObject> objList, String status, String error){
        
        EventBuilder builder = new EventBuilder();
        event = new Queue__c();
        
		event = builder.createEventBaseOn(event, baseOnEvents.BaseOn__c)
            		   .createEventFor(baseOnEvents.CreateFor__c)
					   .withPayload( JSON.serialize(objList) )
					   .withSender (baseOnEvents.sender__c)
					   .withReceiver (baseOnEvents.receiver__c)
					   .build();
        event.businessDocumentNumber__c = baseOnEvents.id;
        event.internalId__c = baseOnEvents.id;  
        event.status__c = status;
        event.exceptionStackTrace__c = error;
        return event;
    }
   
    private void saveEvent(){
        
        try{
            
            insert event;
        
        }catch(DmlException error ){
            
            payloadSizeError('........... Error .........');
            
        }catch(Exception error){
            
            payloadSizeError('........... Error .........');
        }
    }
    
    private void payloadSizeError(String error){
        
        event = new Queue__c();
        event.payload__c = Constants.PAYLOAD_SIZE_ERROR;
            
        List<sObject> payloadSizeErrorList = new List<Queue__c>();
        payloadSizeErrorList.add(event);
              
        buildEventObject(payloadSizeErrorList, EventQueueStatusType.SUCCESS.name(), error);
        this.saveEvent();
    }
}