/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class EventTriggerHandlerTest 
{
    static testmethod void myUnitTest() 
    {
        Profile profile = [Select Id From Profile Where UserType= 'Standard' Limit 1];
        User us = SObjectInstanceTest.createUser(profile.Id);
        insert us;

        Event e = new Event();
        e.ownerId = us.Id;
        e.Subject = 'Run Test Trigger';
        e.DurationInMinutes= 1;
        e.ActivityDate = date.today();
        e.ActivityDateTime = dateTime.now();
        insert e;
        
        
        e.Description = 'teste unitário';
        update e;
        
        delete e;
        
        undelete e;
        
        EventTriggerHandler handler = new EventTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
       
        
   
    }
    
    @isTest(seeAllData=true)
    static void myUnitTest1() 
    {
        Event eventPublic = [Select ownerId From Event Where PublicEvent__c = true Limit 1];

        Event e = new Event();
        e.ownerId = eventPublic.ownerId;
        e.Subject = 'Run Test Trigger';
        e.DurationInMinutes= 1;
        e.ActivityDate = date.today();
        e.ActivityDateTime = dateTime.now();
        insert e;
        
        
        e.Description = 'teste unitário';
        update e;
        
        delete e;
        
        undelete e;
        
        EventTriggerHandler handler = new EventTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
       
        
   
    }

}