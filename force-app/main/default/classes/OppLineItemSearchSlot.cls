/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* When a opportunity line item is created or updated this class searchs for available
* slots. If there is an available slot, the class will update the available quantity of
* the slot, acconding to the quantity of the opportunity line item and will update the fields
* Slot__c, Slot_Image__c and Slot_status__c of the opportunity line item with the success
* information. If there is no available slot, the class will update the fields Slot_Image__c
* and Slot_status__c with the error information.
*
* NAME: OppLineItemSearchSlot.cls
* AUTHOR: LRSA                                                DATE: 02/12/2014
*
*******************************************************************************/

public with sharing class OppLineItemSearchSlot {

  public static void newProducts()
  {
    LineItemSearchSlot.newProducts();
  }
}