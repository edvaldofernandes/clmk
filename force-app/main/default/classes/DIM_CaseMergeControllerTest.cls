// Used by DIM - Market Intelligence.
@isTest 
public class DIM_CaseMergeControllerTest {

    //--------------------------------------------------------------------------
    static testMethod void validateNumberOfEmailsAfterMerge() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;     
        
        EmailMessage email1 = SObjectInstanceTest.email(caso1.Id);
        EmailMessage email2 = SObjectInstanceTest.email(caso1.Id);
        database.insert(email1);
        database.insert(email2);
        
        Case caso2 = DIM_TestUtilsTest.newCase();
        caso2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso2;      
        
        PageReference pageReference = Page.DIM_CaseMergePage;
        pageReference.getParameters().put('sourceId', String.valueOf(caso1.Id));
        pageReference.getParameters().put('targetId', String.valueOf(caso2.Id));
        
        Test.setCurrentPageReference(pageReference);
        ApexPages.StandardController sc = new ApexPages.StandardController(caso1);
        DIM_CaseMergeController controller = new DIM_CaseMergeController(sc);
        
        controller.caseFrom = caso1;
        controller.caseTo = caso2;
        controller.save();
        
        System.assertEquals(2, [SELECT count() FROM EmailMessage WHERE ParentId =: caso2.Id]);
    }  
    
    //--------------------------------------------------------------------------
    static testMethod void checkErrorIfSourceCaseIsNotOpen() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;
        caso1.Status = 'Rejected';
        update caso1;
        
        EmailMessage email1 = SObjectInstanceTest.email(caso1.Id);
        EmailMessage email2 = SObjectInstanceTest.email(caso1.Id);
        database.insert(email1);
        database.insert(email2);
        
        Case caso2 = DIM_TestUtilsTest.newCase();
        caso2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso2;      
        
        PageReference pageReference = Page.DIM_CaseMergePage;
        pageReference.getParameters().put('sourceId', String.valueOf(caso1.Id));
        pageReference.getParameters().put('targetId', String.valueOf(caso2.Id));
        
        test.setCurrentPageReference(pageReference);
        ApexPages.StandardController sc = new ApexPages.StandardController(caso1);
        DIM_CaseMergeController controller = new DIM_CaseMergeController(sc);
        
        controller.caseFrom = caso1;
        controller.caseTo = caso2;
        controller.save();
        
        System.assertEquals(false, controller.isValid);
    }
    
    //--------------------------------------------------------------------------
    static testMethod void checkErrorIfSourceCaseIsEmpty() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;
              
        PageReference pageReference = Page.DIM_CaseMergePage;
        pageReference.getParameters().put('sourceId', '');
        pageReference.getParameters().put('targetId', '');
        
        test.setCurrentPageReference(pageReference);
        ApexPages.StandardController sc = new ApexPages.StandardController(caso1);
        DIM_CaseMergeController controller = new DIM_CaseMergeController(sc);
        
        controller.save();
        
        System.assertEquals(false, controller.isValid);
    }
    
    //--------------------------------------------------------------------------
    static testMethod void checkErrorIfTargetCaseIsEmpty() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;
        
        EmailMessage email1 = SObjectInstanceTest.email(caso1.Id);
        EmailMessage email2 = SObjectInstanceTest.email(caso1.Id);
        database.insert(email1);
        database.insert(email2);     
        
        PageReference pageReference = Page.DIM_CaseMergePage;
        pageReference.getParameters().put('sourceId', String.valueOf(caso1.Id));
        pageReference.getParameters().put('targetId', '');
        
        test.setCurrentPageReference(pageReference);
        ApexPages.StandardController sc = new ApexPages.StandardController(caso1);
        DIM_CaseMergeController controller = new DIM_CaseMergeController(sc);
        
        controller.caseFrom = caso1;
        controller.save();
        
        System.assertEquals(false, controller.isValid);
    }
    
//--------------------------------------------------------------------------
    static testMethod void validateSearchWithResult() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;
    
        Case caso2 = DIM_TestUtilsTest.newCase();
        caso2.Subject = 'Airline Study';
        caso2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso2;
        
        PageReference pageReference = Page.DIM_CaseMergePage;
        pageReference.getParameters().put('sourceId', String.valueOf(caso1.Id));
        pageReference.getParameters().put('caseToId', String.valueOf(caso2.Id));
        pageReference.getParameters().put('lksrch', 'Study');
        
        test.setCurrentPageReference(pageReference);
        ApexPages.StandardController sc = new ApexPages.StandardController(caso1);
        DIM_CaseMergeController controller = new DIM_CaseMergeController(sc);
        
        controller.updateCaseTo();
        
        controller.searchString = 'Airline';
        controller.search();
        
        System.assertEquals(1, controller.results.size());
    }
    
//--------------------------------------------------------------------------
    static testMethod void validateSearcAfterClear() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;
        
        Case caso2 = DIM_TestUtilsTest.newCase();
        caso2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso2;
        
        Case caso3 = DIM_TestUtilsTest.newCase();
        caso3.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso3;
        
        Case caso4 = DIM_TestUtilsTest.newCase();
        caso4.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso4;
        
        PageReference pageReference = Page.DIM_CaseMergePage;
        pageReference.getParameters().put('sourceId', String.valueOf(caso1.Id));
        
        test.setCurrentPageReference(pageReference);
        ApexPages.StandardController sc = new ApexPages.StandardController(caso1);
        DIM_CaseMergeController controller = new DIM_CaseMergeController(sc);
        
        controller.clear();
        
        System.assertEquals(3, controller.results.size());
    }
}