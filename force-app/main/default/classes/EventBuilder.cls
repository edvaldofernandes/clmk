/**
 *
 *
 * @author: Eduardo Ribeiro de Carvalho - ercarval
 */
public with sharing class EventBuilder {

	private Queue__c event;

    public EventBuilder createEventBaseOn(Queue__c event, String eventName ) {
    	this.event = event;
        this.event.eventName__c = eventName;
        this.event.status__c = EventQueueStatusType.QUEUED.name();
        return this;
    }

    public EventBuilder createEventFor(String eventName) {
        event = new Queue__c();
        return createEventBaseOn (event,eventName);
    }

	public EventBuilder createOutboundEventFor(String eventName) {
		return createEventFor(eventName);
	}

	public EventBuilder forEvent(String eventName) {
		event.eventName__c = eventName;
		return this;
	}

	public EventBuilder forObjectId (String id) {
		event.objectId__c = id;
		return this;
	}

	public EventBuilder usingRetryStrategy () {
		event.retryCount__c = 10;
		return this;
	}

	public EventBuilder disablingRetryStrategy () {
		event.retryCount__c = 0;
		return this;
	}

	public EventBuilder withSender (String sender) {
		event.sender__c = sender;
		return this;
	}

	public EventBuilder withReceiver (String receiver) {
		event.receiver__c = receiver;
		return this;
	}

	public EventBuilder withPayload (String payload) {
		event.payload__c = payload;
		return this;
	}

	public EventBuilder withBusinessDocumentNumber (String businessDocumentNumber) {
		event.businessDocumentNumber__c = businessDocumentNumber;
		return this;
	}

	public EventBuilder withBusinessDocumentCorrelatedNumber (String businessDocumentCorrelatedNumber) {
		event.businessDocumentCorrelatedNumber__c  = businessDocumentCorrelatedNumber;
		return this;
	}

	public EventBuilder withInternalID (String internalId) {
		event.internalId__c  = internalId;
		return this;
	}

	public Queue__c build() {
		return event;
	}

}