@isTest
public class CFCProductViewControllerTest {
    
    static testMethod void myUnitTestSucess() 
    {
        RecordType recordType = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        Product2 produto = new Product2();
        produto.Name = 'Produto Teste';
        produto.ProductCode = 'a488';
        produto.Product_Status__c = 'Active';
        produto.RecordTypeId = recordType.Id;
        produto.IsActive = true;
        produto.Publication_Status__c = 'Published';
        database.insert(produto); 
        system.debug('CFCProductViewController.myUnitTest.produto >>> ' + produto);
        
        ApexPages.currentPage().getParameters().put('id', '123');
        CFCProductViewController controller = new CFCProductViewController();
        controller.RedirectToDetails();    
        system.assert(controller != null); 
    }
    
    

}