/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class OppContractRelatedProductsLstCtrTest 
{
	
	static testMethod void testeUnitario() 
	{
             
     
		test.startTest();
      	Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
    	insert acc;
    	
    	Contact contato = new Contact();
    	contato.firstName = 'Teste';
    	contato.LastName = 'unitário';
    	contato.accountId = acc.Id;
    	insert contato;
    	
    	
    
    	Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    	Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
	  	pbAirMod.Name = 'Aircraft Modification';
	  	pbAirMod.Type__c = 'Aircraft Modification';
    	insert pbAirMod;
    
    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
    	opp.Pricebook2Id = pbAirMod.id;
    	opp.stageName = 'Qualification';
    	insert opp ;
        
        OpportunityContactRole oppCr  = new OpportunityContactRole();
        oppCr.ContactId = contato.Id;
        oppCr.OpportunityId = opp.Id;
        insert oppCr;
        
    	Product2 prod = SObjectInstanceTest.createProduct2();
	  	prod.Unit__c = 'Aircraft';
	  	prod.Product_Type__c = 'Aircraft Modification';
	  	prod.Family = 'Aircraft Modification';
	  	prod.Applicability__c = 'Turboprop';
	  	prod.Rom__c = true;
    	insert prod;
    
    	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    	insert pbe;
    
    	PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    	insert pbeAirMod ;
      
    	list<OpportunityLineItem> oppItem = new list<OpportunityLineItem >();
    
    	OpportunityLineItem oppItemEntryFee = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
    	oppItemEntryFee.Princing__c = '';
    	oppItem.add(oppItemEntryFee );
    
   	 	OpportunityLineItem oppItemNrec = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
	  	oppItemNrec.Princing__c = '';
	  	oppItemNrec.Entry_fee__c = 200;
    	oppItem.add(oppItemNrec);
    
    	insert oppItem; 
    	
       	PageReference pageRef = Page.OpportunitiesContractRelatedProductsLst;
       	ApexPages.StandardController stdController = new ApexPages.standardController(prod);
        
        Test.setCurrentPageReference(pageRef);
       	OppContractRelatedProductsLstController controller = new OppContractRelatedProductsLstController(stdController);    
       	controller.viewData();
       	controller.SortExpressionOpp = '';
		controller.SortExpression = '';
       	System.AssertEquals('ASC',controller.getSortDirectionOpp());
       	System.AssertEquals('ASC',controller.getSortDirection());
       	controller.SortExpressionOpp = 'OpportunityId';
		controller.SortExpression = 'ServiceContractId';
       	controller.setSortDirectionOpp('asc');
       	System.AssertEquals('asc',controller.getSortDirectionOpp());
       	controller.setSortDirection('desc');
       	System.AssertEquals('desc',controller.getSortDirection());
       	test.stopTest();
           
    }    

}