/**
* @author Marcilio Leite de Souza
* @date 30/05/2018
* @description: TEST CLASS FOR AccountDAO
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           12 JUN 2018             Original Version
**/
@isTest
private class AccountDAO_Test {

    @isTest
    static void it_should_return_account_by_recordType_and_maintance_capability(){
        Id idRecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId();
                                 
		Set<String> setString = new Set<String>();
        setString.add('testMaintance');
        
        Account a = create_account_test_data_record_type(idRecordTypeAccount);
        
        List<Account> listAcc = AccountDAO.getInstance().listByRecordTypeIdAndMaintanceCapability(idRecordTypeAccount, setString);
        Test.startTest();
        System.assertEquals(listAcc.size(), 1);
        Test.stopTest();
    }
    
    @isTest
    static void it_should_return_account_by_fly_embraer_id(){
        Account a = create_account_test_data();
        
        Account listAcc = AccountDAO.getListAccountByFlyEmbraerId(a.FlyEmbraerId__c);
        Test.startTest();
        System.assertNotEquals(listAcc, null);
        Test.stopTest();
    }
    
    static Account create_account_test_data(){
        Account a = new Account();
        a.BillingCountry = 'Brazil';
        a.Name = 'Test';
        a.Company_Nickname__c = 'testNickname';
        a.FlyEmbraerId__c = '99999';
        insert a;
        return a;
    }
    
     static Account create_account_test_data_record_type(Id idRecordType){
        Account a = new Account();
        a.BillingCountry = 'Brazil';
        a.Name = 'Test';
        a.Company_Nickname__c = 'testNickname';
        a.FlyEmbraerId__c = '99999';
        a.RecordTypeId = idRecordType;
        a.Maintance_Capability__c = 'testMaintance';
        a.EOSC__c = true;
        a.EASC__c = true;
        a.Company_Status__c = 'Active';
        insert a;
        return a;
    }
}