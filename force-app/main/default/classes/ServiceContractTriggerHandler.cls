public without sharing class ServiceContractTriggerHandler
{

    
    public Map<Id,ServiceContract> newRecordsMap = new Map<Id,ServiceContract>();
    public Map<Id,ServiceContract> oldRecordsMap = new Map<Id,ServiceContract>(); 
    public List<ServiceContract> newRecords = new List<ServiceContract>();
    public List<ServiceContract> oldRecords = new List<ServiceContract>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public ServiceContractTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        validateFlyEmbraerFieldsChange();

    }

 

    public void OnAfterInsert()
    {


    }

 

    public void OnBeforeUpdate()
    {
        validateFlyEmbraerFieldsChange();
    }

 

    public void OnAfterUpdate()
    {

        

    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }

    

    private void validateFlyEmbraerFieldsChange()
    {
    
        //Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        Set<Id> idUsersEsolutionsGroup = utils.getUserIdsFromGroup('eSolutions_ADMIN');
        boolean usereSolutionsGroup = idUsersEsolutionsGroup.contains(UserInfo.getUserId());
        Id recordType = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        
        
        for(ServiceContract sc : this.newRecords)
        {
            if(!usereSolutionsGroup)
            {
                if(this.isInsert)
                {
                    if(sc.RecordTypeId == recordType)
                        sc.addError('Only the members in eSolutions public group can create eSolutions Service Contracts.');
                }
                else
                {
                     if((this.oldRecordsMap.get(sc.Id).RecordTypeId  != sc.RecordTypeId) && sc.RecordTypeId == recordType)
                        sc.addError('Only the members in eSolutions public group can create eSolutions Service Contracts.');
                }
            } 
        }
        
    }
   





}