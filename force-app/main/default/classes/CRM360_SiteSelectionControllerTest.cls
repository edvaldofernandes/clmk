@isTest
private class CRM360_SiteSelectionControllerTest {
    
    private static Id RecordTypeIdEmbSite = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId();
    private static List<String> accountNames = new List<String>{
        'Account 1', 
        'Account 2', 
        'Account 3', 
        'Account 4', 
        'All Sites'
    };
    
    @testSetup 
    static void setup(){            
        List<Account> accountsToInsert = new List<Account>();
        
        //Account 1 is an Embraer Site
        Account account1 = new Account();
        account1.Name = accountNames[0];
        account1.Company_Nickname__c = accountNames[0];
        account1.RecordTypeId = RecordTypeIdEmbSite;
        account1.CRM360_Site__c = true;
        accountsToInsert.add(account1);
        
        //Account 2 is an Embraer Site
        Account account2 = new Account();
        account2.Name = accountNames[1];
        account2.Company_Nickname__c = accountNames[1];
        account2.RecordTypeId = RecordTypeIdEmbSite;
        account2.CRM360_Site__c = true;
        accountsToInsert.add(account2);
        
        //Account 3 is not a site
        Account account3 = new Account();
        account3.Name = accountNames[2];
        account3.Company_Nickname__c = accountNames[2];
        account3.RecordTypeId = RecordTypeIdEmbSite;
        account3.CRM360_Site__c = false;
        accountsToInsert.add(account3);
        
        //Account 4 has not a 'Embraer Site' Record Type ID
        Account account4 = new Account();
        account4.Name = accountNames[3];
        account4.Company_Nickname__c = accountNames[3];
        account4.CRM360_Site__c = true;
        accountsToInsert.add(account4);
        
        insert accountsToInsert;
    }
    
    @isTest
    public static void testListSites(){
       
        Test.StartTest();   
        Account acc = new Account(Name='Test');    	         
        PageReference pageReference = Page.CRM360_SiteSelectionPage;
        Test.setCurrentPage(pageReference);      
        ApexPages.StandardController controller = new ApexPages.StandardController(acc); 
        CRM360_SiteSelectionController siteSelController = new CRM360_SiteSelectionController(controller);          
        List<Account> sites = CRM360_SiteSelectionController.getSiteList();
        Test.StopTest();
        
        //Verify if site number is not equals to null
        system.assertNotEquals(null, sites, 'sites is null');
        
        //Verify if all the Embraer Sites have been recovered
        system.assertEquals(3, sites.size(), 'Site number is incorrect');
        
        //Verify if the ordenation is correct
        system.assertEquals(accountNames[0], sites[0].Name, 'Ordenation is incorrect');
        system.assertEquals(accountNames[1], sites[1].Name, 'Ordenation is incorrect');
        
        //Verify if 'All sites' option has been recovered
        system.assertEquals(accountNames[4], sites[2].Name, 'Ordenation is incorrect');
        
        //Confirm that Accounts that are not Embraer Sites have not been recorvered 
        system.assertNotEquals(accountNames[2], sites[0].Name, 'Filter is incorrect');
        system.assertNotEquals(accountNames[2], sites[1].Name, 'Filter is incorrect');
        system.assertNotEquals(accountNames[3], sites[0].Name, 'Filter is incorrect');
        system.assertNotEquals(accountNames[3], sites[1].Name, 'Filter is incorrect');
        
    }
    
}