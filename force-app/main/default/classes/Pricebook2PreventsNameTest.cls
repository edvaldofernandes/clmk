/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class Pricebook2PreventsName
*
* NAME: Pricebook2PreventsNameTest.cls
* AUTHOR: KHPS                                               DATE: 18/12/2014
*******************************************************************************/

@isTest
private class Pricebook2PreventsNameTest {

    static testMethod void testeFuncional() {
    	
    	Pricebook2 pbk1 = SObjectInstanceTest.createPricebook2();
    	pbk1.Name = 'teste';
    	Database.insert(pbk1);
    	
    	Pricebook2 pbk2 = SObjectInstanceTest.createPricebook2();
    	pbk2.Name = 'teste';
    	
    	Test.startTest();
    	Database.Saveresult pbkResult = Database.insert(pbk2, false);
    	Test.stopTest();
    	
    	System.assertEquals('There is already a pricebook with this name.', pbkResult.getErrors()[0].getMessage());
    	
    }
    
    static testMethod void testeLista() {
    	
    	Pricebook2 pbk1 = SObjectInstanceTest.createPricebook2();
    	pbk1.Name = 'teste';
    	
    	Pricebook2 pbk2 = SObjectInstanceTest.createPricebook2();
    	pbk2.Name = 'teste';
    	
    	Test.startTest();
    	list<Database.Saveresult> lstPbkResult = Database.insert(new list<Pricebook2>{pbk1, pbk2}, false);
    	Test.stopTest();	
    	
    }
    
}