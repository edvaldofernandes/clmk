public class AircraftInformation {
    
    private Aircraft__c aircraft;
    private Account account;
    private String objectType;
    private String status;
    
    public AircraftInformation(){}
    
    public AircraftInformation(Aircraft__c aircraft, Account account, String objectType, String status){
        
        this.aircraft = aircraft;
        this.account = account;
        this.objectType = objectType;
        this.status = status;        
    }
    
    public Aircraft__c getAircraft(){
        
        return this.aircraft;
    }
    
    public String getStatus(){
        
        return this.status;
    }
    
    public String getObjectType(){
        
        return this.objectType;
    }
    
    public Account getAccount(){
        
        return this.account;
    }
}