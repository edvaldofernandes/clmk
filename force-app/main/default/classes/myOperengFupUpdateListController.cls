/**
* @description: This class is the controller of the Follow-up updates for myOpereng.
**/
public with sharing class myOperengFupUpdateListController {   
    @AuraEnabled
    public static List<FUP_Update__c> getUpdates(Id fupId) {
        List<FUP_Update__c> updates = FO_FupRepository.getPublicUpdatesByFup(fupId);
        return updates;
    }   
}