public class FO_FupDigestPreviewController {
    @AuraEnabled
    public static String getEmailPreview(Id whoId) {
        if(whoId == null){
            try{
                whoId = [
                    SELECT
                        Id
                    FROM
                        Contact
                    WHERE
                        FlightOps_FUP_Report__c = TRUE
                        AND FlightOps_ERJ_WW_FUP_Report__c = TRUE
                        AND FlightOps_E2_FUP_Report__c = TRUE
                    LIMIT 1
                ].Id;
            }
            catch(System.QueryException e){
                whoId = [
                    SELECT
                        Id
                    FROM
                        Contact
                    WHERE
                        FlightOps_FUP_Report__c = TRUE
                        OR FlightOps_ERJ_WW_FUP_Report__c = TRUE
                        OR FlightOps_E2_FUP_Report__c = TRUE
                    LIMIT 1
                ].Id;
            }
        }
        EmailTemplate template = FO_FupRepository.getFupDigestEmailTemplate();
        Messaging.SingleEmailMessage message = Messaging.renderStoredEmailTemplate(template.Id, whoId, null);
        return message.getHtmlBody();
    }
    
    @AuraEnabled
    public static List<Contact> getContacts() {
        return FO_ContactRepository.getContactsReceivingFupReport();
    }
    
    @AuraEnabled
    public static void sendFupDigest() {
        FO_FupService.sendFupDigestToContactsWhoWantsToReceiveIt();
    }
}