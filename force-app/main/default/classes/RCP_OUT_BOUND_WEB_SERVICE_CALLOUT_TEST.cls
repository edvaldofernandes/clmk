@isTest
global class RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST implements WebServiceMock {
    
    private RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element sfInfoElement;
    
    public RCP_OUT_BOUND_WEB_SERVICE_CALLOUT_TEST(RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element sfInfoElement){
        
        this.sfInfoElement = sfInfoElement; 
    }
    
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       
     
           RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse updateFromSalesForceResponse = new RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse();
            
           updateFromSalesForceResponse.status = 'Mock Success';
           
           System.debug('response ' + updateFromSalesForceResponse.status);    
           response.put('response_x', updateFromSalesForceResponse);
   }
}