@isTest
public class AircraftPriceTriggerHandlerTest {
    
    static testMethod void testPriceUpdate()
    {   
        Product2 productTest = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
        database.insert(productTest);
        
        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(test.getStandardPricebookId(), productTest.Id);
        database.insert(priceBookEntryTest);
        
        Account accountTest = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        database.insert(accountTest);
        
        Agreement__c agreementTest = SObjectInstanceTest.createCommercialAgreement(accountTest.Id);
        database.insert(agreementTest);
        
        Aircraft_Price__c priceTest = new Aircraft_Price__c();
        priceTest.Model__c = productTest.Id;
        priceTest.Commercial_Agreement__c = agreementTest.Id;
        priceTest.Aircraft_Version__c = 'STD';
        priceTest.Economic_Condition__c = 'Jan/18';
        priceTest.Aircraft_Basic_Price__c = 1000;
        database.insert(priceTest);
        
        Agreement_Aircraft__c aircraftTest = SObjectInstanceTest.createAgreementAircraft(productTest.Id, agreementTest.Id, 'Firm');
        aircraftTest.Aircraft_Price__c = priceTest.Id;
        database.insert(aircraftTest);
        
        test.startTest();
        
        priceTest.Aircraft_Basic_Price__c = 2000;
        database.update(priceTest);
        
        Agreement_Aircraft__c updateAircraft = [SELECT Id, Basic_Price__c FROM Agreement_Aircraft__c WHERE Id = :aircraftTest.Id];
        
        System.assertEquals(2000,updateAircraft.Basic_Price__c);
        
        test.stopTest();
    }
    
}