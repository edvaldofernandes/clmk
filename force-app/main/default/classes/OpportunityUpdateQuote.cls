/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* When the account of an opportunity is updated this class searchs the quote
* related to this opportunity and update its status to "Denied".
*
* NAME: OpportunityUpdateQuote.cls
* AUTHOR: DPF                                                DATE: 19/01/2015
*
*******************************************************************************/

public with sharing class OpportunityUpdateQuote {

  public static void execute()
  {
  	TriggerUtils.assertTrigger();

    map<Id, Opportunity> mapOpp = new map<Id, Opportunity>();
    for ( Opportunity opp : (list<Opportunity>) trigger.new )
    {
      if ( TriggerUtils.wasChanged(opp, Opportunity.AccountId) )
      {
        mapOpp.put(opp.Id, opp);
      }
    }
    if ( mapOpp.isEmpty() ) return;

    List<Quote> lstQuote = [SELECT Status FROM Quote WHERE OpportunityId =: mapOpp.keyset() ];
    for (Quote iQuote : lstQuote )
    {
      iQuote.status = 'Denied';
    }
    if ( !lstQuote.isEmpty() ) update lstQuote;  	
  }
  
}