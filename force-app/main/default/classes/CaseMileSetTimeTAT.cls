global class CaseMileSetTimeTAT implements Support.MilestoneTriggerTimeCalculator {
    private static final Integer UNIT_MIN=1; 
    
     global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId)
     {
     
         Case lCaso = [ Select Id, Status, LastModifiedDate, CreatedDate, Fed_Ex_Pick_Up_Date__c, (select Id, StartDate, MilestoneTypeId, MilestoneType.Name from CaseMilestones
         where MilestoneTypeId = :milestoneTypeId order by StartDate desc) from Case where id = :caseId ];
         list<CaseMilestone> lstMil = lCaso.CaseMilestones;
        
        
        
        Datetime dtBase = (lstMil == null || lstMil.isEmpty() || lstMil[0].StartDate == null)
          ? lCaso.CreatedDate : lstMil[0].StartDate;
        Date dtsys = lCaso.Fed_Ex_Pick_Up_Date__c; 
        DateTime dt = system.today();
        DateTime dttrg = dtsys + 22 ;
    
        Integer istdate = Math.max(UNIT_MIN, DateDiffInMinutes(dtBase, dtsys));
        Integer iDiff = Math.max(UNIT_MIN, DateDiffInMinutes(dtBase, dttrg));

        return istdate + iDiff;
        
        

   
     }

         private Integer DateDiffInMinutes(Datetime dtMin, Datetime dtMax)
          {
            long lTempo1 = dtMax.getTime() / 1000;
            long lTempo2 = dtMin.getTime() / 1000;
            long lDivisao = Math.mod( ltempo1 - lTempo2, 60 );
            lDivisao = ( ( ltempo1 - lTempo2 )/60 ) + ( lDivisao > 0 ? 1 : 0 );
            return Integer.valueOf( lDivisao );
          }

}