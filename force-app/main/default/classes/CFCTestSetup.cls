@isTest
public class CFCTestSetup {
    @isTest
    public static User getUser(){ 
        RecordType recordTypeAccountOEM = [Select Name, Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Aircraft_OEM' limit 1 ];
        system.debug('CFCHomeControllerTest.view.recordTypeAccountOEM >>> ' + recordTypeAccountOEM);
        
        RecordType recordTypeAccountCAAA = [Select Name, Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Airworthiness_authority' limit 1 ];
        system.debug('CFCHomeControllerTest.view.recordTypeAccountCAAA >>> ' + recordTypeAccountCAAA);
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator' or name = 'Administrador do sistema'];
        system.debug('profile1 is ' + profile1);
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'testcfc20@embraer.net.br',
            Alias = 'batman',
            Email='bruce.wayne@embraer.net.br',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        system.debug('portalAccountOwner1 is ' + portalAccountOwner1);

        User user1;
        System.runAs ( portalAccountOwner1 ) {
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id,
                Company_Nickname__c = 'TAM99',
                Cam__c = portalAccountOwner1.Id
            );
            Database.insert(portalAccount1);
            system.debug('portalAccount1 is ' + portalAccount1);
            
            //Create account OEM
            Account accountOEM = new Account(
                Name = 'TestAccountOEM',
                OwnerId = portalAccountOwner1.Id,
                Company_Nickname__c = 'OEM99',
                RecordTypeId = recordTypeAccountOEM.Id
            );
            Database.insert(accountOEM);
            system.debug('accountOEM is ' + accountOEM);
            
            //Create account Civil Aviation Airworthiness Authority
            Account accountCAAA = new Account(
                Name = 'EASA - Europe',
                OwnerId = portalAccountOwner1.Id,
                Company_Nickname__c = 'EASA99',
                RecordTypeId = recordTypeAccountCAAA.Id
            );
            Database.insert(accountCAAA);
            system.debug('accountOEM is ' + accountCAAA);
            
            AccountTeamMember accTeamMember = new AccountTeamMember(
                TeamMemberRole = 'CAM - Customer Account Manager',
                AccountId = portalAccount1.id,
                UserId = portalAccountOwner1.id
            );
            Database.insert(accTeamMember);
            system.debug('accTeamMember is ' + accTeamMember);
            
            //Create contact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'testecfc@embraer.net.br'
            );
            Database.insert(contact1);
            system.debug('contact1 is ' + contact1);
            
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name='Services Catalog' LIMIT 1];
            system.debug('portalProfile is ' + portalProfile);
            user1 = new User(
                Username = System.now().millisecond() + 'teste12345@embraer.net.br',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@embraer.net.br',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            system.debug('user1 is ' + user1);
        }
        return user1;
    }
    
    public static User getUserWithoutAccountCam(){ 
        RecordType recordTypeAccountOEM = [Select Name, Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Aircraft_OEM' limit 1 ];
        system.debug('CFCHomeControllerTest.view.recordTypeAccountOEM >>> ' + recordTypeAccountOEM);
        
        RecordType recordTypeAccountCAAA = [Select Name, Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Airworthiness_authority' limit 1 ];
        system.debug('CFCHomeControllerTest.view.recordTypeAccountCAAA >>> ' + recordTypeAccountCAAA);
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator' or name = 'Administrador do sistema'];
        system.debug('profile1 is ' + profile1);
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'testeCFC22@embraer.net.br',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        system.debug('portalAccountOwner1 is ' + portalAccountOwner1);

        User user1;
        System.runAs ( portalAccountOwner1 ) {
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id,
                Company_Nickname__c = 'TAM99'
            );
            Database.insert(portalAccount1);
            system.debug('portalAccount1 is ' + portalAccount1);
            
            //Create account OEM
            Account accountOEM = new Account(
                Name = 'TestAccountOEM',
                OwnerId = portalAccountOwner1.Id,
                Company_Nickname__c = 'OEM99',
                RecordTypeId = recordTypeAccountOEM.Id
            );
            Database.insert(accountOEM);
            system.debug('accountOEM is ' + accountOEM);
            
            //Create account Civil Aviation Airworthiness Authority
            Account accountCAAA = new Account(
                Name = 'EASA - Europe',
                OwnerId = portalAccountOwner1.Id,
                Company_Nickname__c = 'EASA99',
                RecordTypeId = recordTypeAccountCAAA.Id
            );
            Database.insert(accountCAAA);
            system.debug('accountOEM is ' + accountCAAA);
            
            //Create contact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'test@embraer.net.br'
            );
            Database.insert(contact1);
            system.debug('contact1 is ' + contact1);
            
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name='Services Catalog' LIMIT 1];
            system.debug('portalProfile is ' + portalProfile);
            user1 = new User(
                Username = System.now().millisecond() + 'testeCFC12345789@embraer.net.br',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@embraer.net.br',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            system.debug('user1 is ' + user1);
        }
        return user1;
    }
    
    
}