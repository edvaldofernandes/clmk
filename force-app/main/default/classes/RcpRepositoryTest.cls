@isTest(seeAllData=true)
public class RcpRepositoryTest {
    
    @isTest static void buildMapWithAllAccountTest(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test trigger account deleted';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.COD_ORGz__c = 123456;
        insert account;
        
        Map<String,String> accountIdMap = RcpRepository.buildMapWithAllAccount();
        
        System.assertEquals(accountIdMap.get(String.valueOf(account.COD_ORGz__c)), account.Id);
        
        Test.stopTest();
    }
    
    @isTest static void buildMapWithAllAircraftTest(){
        
        Test.startTest();
        
        Aircraft__c aircraft = new Aircraft__c ();
        aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '88877666';
        aircraft.Aircraft_Status__c = 'In Service';
        aircraft.Commercial_Name__c = 'EMB-110P1';
        insert aircraft;
        
        Map<String,String> aircraftIdMap = RcpRepository.buildMapWithAllAircraft();
        
        System.assertEquals(aircraftIdMap.get(aircraft.name), aircraft.Id);
        
        Test.stopTest();
    }
    
    @isTest static void findAccountTeamMemberUpdatedTest(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'accountTeamMemberTest';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
            
        Profile p = [select id from Profile where name='System Administrator'];
            
        User user = new User(alias = 'utest5', email='Unit5.Test@unittest.com',emailencodingkey='UTF-8', 
                             firstName='First5', lastname='Last5', languagelocalekey='en_US', localesidkey='en_US', 
                             profileid = p.id, timezonesidkey='Europe/London', username='Unit5.Test@unittest.com');
        insert user;
            
        AccountTeamMember accountTeamMember = new AccountTeamMember();
        accountTeamMember.UserId = user.Id;
        accountTeamMember.AccountId = account.Id;
        accountTeamMember.teamMemberRole = 'Role_test';
        
        insert accountTeamMember;
        
        AccountTeamMember accountTeamMemberInserted = RcpRepository.findAccountTeamMemberInserted();
        
        accountTeamMember.teamMemberRole = 'ROLE_TEST_UPDATE';
        
        update accountTeamMember;
        
        AccountTeamMember accountTeamMemberUpdated = RcpRepository.findAccountTeamMemberUpdated();
        
        Test.stopTest();
    }
    
    @isTest static void findAccountHistoryByAccountIdTest(){
        
        Test.startTest();
        
        DescribeFieldResult field = AccountHistory.Field.getDescribe();
        List<PicklistEntry> availableValues = field.getPicklistValues();
        
        Account account = new Account();
        account.Name = 'accountTeamMemberTest';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
        
        Profile p = [select id from Profile where name='System Administrator'];
            
        User user = new User(alias = 'utest6', email='Unit6.Test@unittest.com',emailencodingkey='UTF-8', 
                             firstName='First6', lastname='Last6', languagelocalekey='en_US', localesidkey='en_US', 
                             profileid = p.id, timezonesidkey='Europe/London', username='Unit6.Test@unittest.com');
        insert user;
        
        AccountHistory accountHistoryTest = new AccountHistory();
        accountHistoryTest.Field = availableValues.get(0).getValue();
        accountHistoryTest.AccountId = account.Id;
        
        insert accountHistoryTest;
        
        List<AccountHistory> accountHistoryList = RcpRepository.findAccountHistoryByAccountId(account.Id);
        
        for(AccountHistory AccountHistoryCheck : accountHistoryList)
            System.assertEquals(AccountHistoryCheck.Field, accountHistoryTest.Field);
            
        Test.stopTest();
    }
    
    @isTest static void findAircraftHistoryByAircratIdTest(){
        
        Test.startTest();
        
        DescribeFieldResult field = Aircraft__History.Field.getDescribe();
        List<PicklistEntry> availableValues = field.getPicklistValues();
        
        Account account = new Account();
        account.Name = 'Test aircraftInformation';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = 'TEST 123456';
        insert account;
        
        Aircraft__c aircraft = new Aircraft__c ();
        aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '09876543';
        aircraft.Operator__c = account.Id;
        aircraft.Aircraft_Status__c = 'In Service';
        aircraft.Commercial_Name__c = 'EMB-110P1';
        insert aircraft;
        
        Aircraft__History aircraftHistoryTest = new Aircraft__History();
        aircraftHistoryTest.Field = availableValues.get(0).getValue();
        aircraftHistoryTest.ParentId = aircraft.Id;
        insert aircraftHistoryTest;
        
        List<Aircraft__History> aircraftHistoryList = RcpRepository.findAircraftHistoryByAircratId(aircraft.Id);
        
        for(Aircraft__History aircraftHistoryCheck : aircraftHistoryList)
            System.assertEquals(aircraftHistoryCheck.Field, aircraftHistoryTest.Field);
        
        Test.stopTest();
    }
    
    @isTest static void searchUserAnFindUserTest(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'accountTeamMemberTest';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
        
        Profile p = [select id from Profile where name='System Administrator'];
            
        User user = new User(alias = 'utest7', email='Unit7.Test@unittest.com',emailencodingkey='UTF-8', 
                             firstName='First7', lastname='Last7', languagelocalekey='en_US', localesidkey='en_US', 
                             profileid = p.id, timezonesidkey='Europe/London', username='Unit7.Test@unittest.com');
        user.Phone = '000000000';
        
        List<User> userInsertList = new List<User>();
        userInsertList.add(user);
        
        insert userInsertList;
        
        
        List<User> userList = RcpRepository.searchUserByUserId(userInsertList);
        
        System.assertEquals(userList.get(0).Phone, userInsertList.get(0).Phone);
        
        String userInformation = RcpRepository.findUserByUserId(user.Id);
        
        List<String> userIdList = new List<String>();
        userIdList.add(user.Id);
        
        List<User> userIdAfterList = RcpRepository.searchUserByUserId(userIdList);    
    	
        System.assert(userIdAfterList != null && userIdAfterList.size() > 0);
	        
        Test.stopTest();
    }
    
    @isTest static void searchFlyembraerIdTest(){
        
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'accountTeamMemberTest';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = '12345987';
        insert account;
        
        List<Account> accountBeforeList = new List<Account>();
        accountBeforeList.add(account);
        
        List<Account> accountList = RcpRepository.searchFlyembraerId(accountBeforeList);
        
        System.assertEquals(accountList.get(0).FlyEmbraerId__c, accountBeforeList.get(0).FlyEmbraerId__c);
        
        List<String> accountIdBeforeList = new List<String>();
        accountIdBeforeList.add(account.id);
        
        List<Account> accounAftertList = RcpRepository.searchFlyembraerId(accountIdBeforeList);
        
        System.assertEquals(accounAftertList.get(0).FlyEmbraerId__c, accountBeforeList.get(0).FlyEmbraerId__c);
        
        Test.stopTest();
    }
    
     @isTest static void accountTeamMemberTest(){
        
        Test.startTest();
         
        Account account = new Account();
        account.Name = 'accountTeamMemberTest';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
            
        Profile p = [select id from Profile where name='System Administrator'];
            
        User user = new User(alias = 'utest3', email='Unit3.Test@unittest.com',emailencodingkey='UTF-8', 
                             firstName='First3', lastname='Last3', languagelocalekey='en_US', localesidkey='en_US', 
                             profileid = p.id, timezonesidkey='Europe/London', username='Unit3.Test@unittest.com');
        insert user;
            
        AccountTeamMember accountTeamMember = new AccountTeamMember();
        accountTeamMember.UserId = user.Id;
        accountTeamMember.AccountId = account.Id;
        accountTeamMember.teamMemberRole = 'Role_test';
            
        insert accountTeamMember;
        
        AccountTeamMember teamMemberAfterInsert = RcpRepository.findAccountTeamMemberInserted();
                
        Test.stopTest(); 
    }
    
    @isTest static void accountTeamMemberUpdateTest(){
        
        Test.startTest();
   
        Account account = new Account();
        account.Name = 'accountTeamMemberTest';
        account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        insert account;
            
        Profile p = [select id from Profile where name='System Administrator'];
            
        User user = new User(alias = 'utest4', email='Unit4.Test@unittest.com',emailencodingkey='UTF-8', 
                             firstName='First4', lastname='Last4', languagelocalekey='en_US', localesidkey='en_US', 
                             profileid = p.id, timezonesidkey='Europe/London', username='Unit4.Test@unittest.com');
        insert user;
            
        AccountTeamMember accountTeamMember = new AccountTeamMember();
        accountTeamMember.UserId = user.Id;
        accountTeamMember.AccountId = account.Id;
        accountTeamMember.teamMemberRole = 'Role_test';
            
        insert accountTeamMember;
        
        List<AccountTeamMember> accountTeamMemberInsertedList = RcpRepository.searchAccountTeamMemberInserted();
        
        List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
        accountTeamMemberList.add(accountTeamMember);
            
            
        accountTeamMember.teamMemberRole = 'Role_test_update';
        update accountTeamMember;
            
        List<AccountTeamMember> accountTeamMemberListUpdate = new List<AccountTeamMember>();
        accountTeamMemberListUpdate.add(accountTeamMember);
        
        List<AccountTeamMember> accountTeamMemberUpdatedList = RcpRepository.searchAccountTeamMemberUpdated();
        
        Test.stopTest();  
    }
}