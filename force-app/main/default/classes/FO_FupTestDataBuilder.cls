/**
* @description: class for create Follow-ups test data.
**/
@isTest
public class FO_FupTestDataBuilder {
    private String ata = '00 - General';
    private String status = 'Investigation';
    private String publishStatus = 'Unpublished';
    private String name;
    private String authorities = 'ANAC';
    private String aircraftFamilies = 'EMBRAER 190';
    public FO_FupTestDataBuilder withName(String name){
        this.name = name;
        return this;
    }  
    public FO_FupTestDataBuilder withStatus(String status){
        this.status = status;
        return this;
    }  
    
    public FO_FupTestDataBuilder withPublishStatus(String publishStatus){
        this.publishStatus = publishStatus;
        return this;
    }  
    public FO_FupTestDataBuilder withAta(String ata){
        this.ata = ata;
        return this;
    }
    public FO_FupTestDataBuilder withAuthorities(String authorities){
        this.authorities = authorities;
        return this;
    } 
    public FO_FupTestDataBuilder withAuthorities(List<String> authorities){
        String authoritiesString = '';
        for (String authority : authorities){
            authoritiesString += authority + ';';
        }
        this.authorities = authoritiesString;
        return this;
    }
    public FO_FupTestDataBuilder withAircraftFamily(String aircraftFamilies){
        this.aircraftFamilies = aircraftFamilies;
        return this;
    } 
    public FO_FupTestDataBuilder withAircraftFamily(List<String> aircraftFamilies){
        String aircraftFamiliesString = '';
        for (String aircraftFamily : aircraftFamilies){
            aircraftFamiliesString += aircraftFamily + ';';
        }
        this.aircraftFamilies = aircraftFamiliesString;
        return this;
    }
    public FO_FupTestDataBuilder whichIsPublic(){
        this.publishStatus = 'Published';
		return this;
    }
    /**
    * @description: builds an follow-up using data from this class instance.
    **/
    public Fup__c build(){
        Fup__c fup = new Fup__c(
            Status__c = status,
            Ata__c = ata,
            Name = name,
            Aircraft_Family__c = aircraftFamilies,
            Authority__c = authorities,
            Publish_Status__c  = publishStatus
        );
        insert fup;
        return fup;
    }
}