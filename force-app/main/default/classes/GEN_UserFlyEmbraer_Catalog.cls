/**
* @author Tulio Fukuoka
* @date 01/10/2020
* @description: Receive information from Fly Embraer and handle with communities Usuers
*
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Tulio Fukuoka                     01 NOV 2020             Original Version
**/
global class GEN_UserFlyEmbraer_Catalog implements Auth.SamlJitHandler {
    
    private class JitException extends Exception{}
    private User sfUser {get;set;}
    private Contact sfCon {get;set;}
    
    /**
* @description Implements Auth.SamlJitHandler method to create customer community user
* @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
* @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
* @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
* @param String federationId : The ID Salesforce expects to be used for this user.
* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
* @param String assertion : The default SAML assertion, base-64 encoded.
* @return User : User created
**/
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId, 
                           String federationIdentifier, Map<String, String> attributes, String assertion) {
                               
                               System.debug('createUser.samlSsoProviderId: ' + samlSsoProviderId);    
                               System.debug('createUser.communityId: ' + communityId); 
                               System.debug('createUser.portalId: ' + portalId); 
                               System.debug('createUser.federationIdentifier: ' + federationIdentifier); 
                               System.debug('createUser.attributes: ' + attributes); 
                               System.debug('createUser.assertion: ' + assertion); 
                               
                               return handleUser(communityId, attributes, federationIdentifier,false);
                           }
    
    /**
* @description Implements Auth.SamlJitHandler method to update customer community user
* @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
* @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
* @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
* @param String federationId : The ID Salesforce expects to be used for this user.
* @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
* @param String assertion : The default SAML assertion, base-64 encoded.
* @return void
**/
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
                           String federationIdentifier, Map<String, String> attributes, String assertion) { 
                               System.debug('Entrou Upload ');
                               handleUser(communityId, attributes, federationIdentifier,True);
                           }
    
    /**
* @description Handle rules: Create new user OR new Contact and User Assign Permission Set
* @param Id communityId : Id for the related community attempt to login
* @param Map<String, String> attribute : Map attributes from FlyEmbraer
* @param String federationIdentifier : FlyEmbraer = login Salesforce = User.FederationIdentifier (Federation ID)
* @return void
*/
    public User handleUser(Id communityId, Map<String, String> attributes, String federationIdentifier,Boolean upda ) {
        
        String strJson = JSON.serialize(attributes);
        
        FlyEmbraerUser flyUser = (FlyEmbraerUser) JSON.deserialize(strJson, FlyEmbraerUser.class);
        flyUser.federationIdentifier = federationIdentifier;
        System.debug('federation/login/user '+federationIdentifier);
        String str = 'SELECT ' + Utils.getAllFields('Community_Setting__mdt') + ' FROM ';
        str +='Community_Setting__mdt WHERE Community_Id__c = \'' + communityId + '\' LIMIT 1 ';     
        Community_Setting__mdt mdtCommu = Database.query(str);
        
        sfUser = UserDAO.getUserByFederationIdentifier(federationIdentifier);
        sfCon = GEN_ContactDAO.getCommunityContactByFlyEmbraerId(federationIdentifier);
        Savepoint dml; 
        Account a = AccountDAO.getListAccountByFlyEmbraerId(flyUser.embCompanyCode);
        try{
            
            dml = Database.setSavepoint();
            
            if(sfUser == null){
                
                if(sfCon == null){
                    
                    if(a!=null){
                        insert setCommuContact(a , flyUser);
                        insert setCommuUser(sfCon , mdtCommu);
                    }else{
                        System.debug('erro account não possui companycode' +flyUser.embCompanyCode);
                    }
                }else{
                    insert setCommuContact(a , flyUser);
                    update setCommuUser(sfCon , mdtCommu);
                }
                
                if(!UserDAO.isPermissionSetAssingUser(sfUser.Id, mdtCommu.PermissionSet_Id__c) && upda ==false)
                    UserDAO.upsertUserPermissionFuture(sfUser.Id, mdtCommu.PermissionSet_Id__c);                
                
            }else{
                if(!UserDAO.isPermissionSetAssingUser(sfUser.Id, mdtCommu.PermissionSet_Id__c) && upda ==false)
                    UserDAO.upsertUserPermissionFuture(sfUser.Id, mdtCommu.PermissionSet_Id__c);
                updateInfoContact(flyUser);
                updateInfoUser(flyUser);
                
            }
            
        }catch(Exception e){
            System.debug('error '+e);
            Database.rollback(dml);
        }
        
        return sfUser;
    }
    
    
    /**
* @description Set a community Contact
* @param Account a : The Account related to the contact
* @param FlyEmbraerUser flyUser : The user related to FlyEmbraer
* @return Contact : contact setted
**/
    private Contact setCommuContact(Account a, FlyEmbraerUser flyUser){
        this.sfCon = new Contact();
        this.sfCon.AccountId = a.Id;
        this.sfCon.Phone = flyUser.telephoneNumber;
        this.sfCon.Email = flyUser.mail;
        this.sfCon.FlyEmbraerUserId__c = flyUser.federationIdentifier;
        this.sfCon.Contact_Status__c = 'Active';
        this.sfCon.Title = flyUser.Title;
        this.sfCon.LastName = flyUser.fullName;
        
        return this.sfCon;
    }
    
    /**
* @description Set a community User
* @param Contact c : The Contact related to the user
* @param Community_Setting__mdt mdt : The community settings related 
* @return User : user setted
**/
    private User setCommuUser(Contact c, Community_Setting__mdt mdt){
        Account nameAccount = new Account();
        nameAccount = [Select name,FlyEmbraerId__c  from Account where id =: c.AccountId];
        this.sfUser = new User();
        this.sfUser.Username = c.FlyEmbraerUserId__c + '@crmvpc.com';
        this.sfUser.LastName = c.LastName;
        this.sfUser.Email = c.Email;
        this.sfUser.ContactId = c.Id;
        this.sfUser.Title = c.Title;
        this.sfUser.Alias = c.FlyEmbraerUserId__c.left(8);
        this.sfUser.FederationIdentifier = c.FlyEmbraerUserId__c;
        this.sfUser.ProfileId = mdt.Profile_Id__c;
        this.sfUser.TimeZoneSidKey = string.valueOf(UserInfo.getTimeZone());
        this.sfUser.LocaleSidKey = UserInfo.getLocale();
        this.sfUser.EmailEncodingKey = 'ISO-8859-1';
        this.sfUser.LanguageLocaleKey = UserInfo.getLanguage();  
        this.sfUser.IsActive = true;
        this.sfUser.CompanyName = nameAccount.FlyEmbraerId__c+'_'+nameAccount.Name;
        
        return this.sfUser;
    }
    
    /**
* @description FlyEmbraerUser Model Class to FlyEmbraer user
**/
    @TestVisible
    private Class FlyEmbraerUser{
        @TestVisible String embCompanyCode;
        @TestVisible String embLogin;
        @TestVisible String fullName ;
        @TestVisible String co;
        @TestVisible String mail;
        @TestVisible String federationIdentifier;
        @TestVisible String City;
        @TestVisible String telephoneNumber;
        @TestVisible String Title;
        @TestVisible String CompannyName;
        @TestVisible FlyEmbraerUser(){}
    }
    
    private void  updateInfoUser(FlyEmbraerUser flyuser){
        Account nameAccount = new Account();
        nameAccount = [Select name,FlyEmbraerId__c  from Account where FlyEmbraerId__c =: flyuser.embCompanyCode];
        if(this.sfUser.Phone!=flyuser.telephoneNumber){this.sfUser.Phone =flyuser.telephoneNumber;}
        if(this.sfUser.City!=flyuser.City){this.sfUser.City = flyuser.City;}
        if(this.sfUser.Email!=flyuser.mail){this.sfUser.Email = flyuser.mail;}
        if(this.sfUser.Title!=flyuser.Title){this.sfUser.Title = flyuser.Title;}
        if(this.sfUser.LastName!= flyuser.fullName.substringAfter(' ')){this.sfUser.LastName = flyuser.fullName.substringAfter(' ');}
        if(this.sfUser.FirstName!=flyuser.fullName.substringBefore(' ')){this.sfUser.FirstName = flyuser.fullName.substringBefore(' ');}
        if(this.sfUser.CompanyName!=nameAccount.FlyEmbraerId__c+'_'+nameAccount.Name){this.sfUser.CompanyName = nameAccount.FlyEmbraerId__c+'_'+nameAccount.Name;}
        
        update sfUser;
    }
    
    private void updateInfoContact(FlyEmbraerUser flyContact){
        if(this.sfCon.LastName != flyContact.fullName.substringAfter(' ')){this.sfCon.LastName = flyContact.fullName.substringAfter(' ');}
        if(this.sfCon.FirstName != flyContact.fullName.substringBefore(' ')){this.sfCon.FirstName = flyContact.fullName.substringBefore(' ');}
        if(this.sfCon.Phone != flyContact.telephoneNumber){this.sfCon.Phone = flyContact.telephoneNumber;}
        if(this.sfCon.Email != flyContact.mail){this.sfCon.Email = flyContact.mail;}
        
        update sfCon;
    }   
}