/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for copying the related aircraft from the opportunity line items
* to related quote line items when they are created.
*
* NAME: QuoteLineItemCopyRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 10/12/2014
*
*******************************************************************************/
public with sharing class QuoteLineItemCopyRelatedAircraft {

   public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    Set<Id> lstQuoteId = new Set<Id>();
    List<QuoteLineItem> lstQuoteLineItem = new List<QuoteLineItem>();
    for ( QuoteLineItem iQuote : (List<QuoteLineItem>) trigger.new  )
    {
      lstQuoteLineItem.add(iQuote);
      lstQuoteId.add(iQuote.QuoteId);
    }    
    
    Map<Id, Id> mapQuoteOpportunityId = new Map<Id, Id>();
    for ( Quote iQuote : [SELECT Id, OpportunityId FROM Quote WHERE Id =: lstQuoteId ])
    {
      mapQuoteOpportunityId.put(iQuote.Id, iQuote.OpportunityId);
    }    
    if ( mapQuoteOpportunityId.isEmpty() ) return;
    
    List<String> lstOliId = new List<String>();
    Map<Id, List<OpportunityLineItem>> mapOppOli = new Map<Id, List<OpportunityLineItem>>();
    
    for ( OpportunityLineItem oli: [SELECT OpportunityId, PricebookEntryId, 
      Quantity, UnitPrice, princing__c
      FROM OpportunityLineItem
      WHERE OpportunityId =: mapQuoteOpportunityId.Values() ])
    {
      List<OpportunityLineItem> lstOli = mapOppOli.get(oli.OpportunityId);
      if ( lstOli == null )
      {
        lstOli = new List<OpportunityLineItem>();
        mapOppOli.put(oli.OpportunityId, lstOli);
      }
      lstOli.add(oli);
      lstOliId.add(oli.Id);
    }
    if ( mapOppOli.isEmpty() ) return;
    
    Map<String, List<Related_Aircraft__c>> mapOliAircraft = new Map<String, List<Related_Aircraft__c>>();
    
    for ( Related_Aircraft__c relAir: [SELECT Aircraft__c, Opportunity_Fleet_Type__c, Opportunity_product_id__c
      FROM Related_Aircraft__c
      WHERE Opportunity_product_id__c =: lstOliId ])
    {
      List<Related_Aircraft__c> lstRelAircraft = mapOliAircraft.get(relAir.Opportunity_product_id__c);
      if ( lstRelAircraft == null )
      {
        lstRelAircraft = new List<Related_Aircraft__c>();
        mapOliAircraft.put(relAir.Opportunity_product_id__c, lstRelAircraft);
      }
      lstRelAircraft.add(relAir);     
    }
    if ( mapOliAircraft.isEmpty() ) return;
    
    
    List<Related_Aircraft__c> lstQuoteRelatedAircraft = new List<Related_Aircraft__c>();
    for ( QuoteLineItem iQuote : lstQuoteLineItem )
    {
      Id idOpp = mapQuoteOpportunityId.get(iQuote.QuoteId);
      if ( IdOpp == null ) continue;
      List<OpportunityLineItem> lstOli = mapOppOli.get(idOpp);
      if ( lstOli == null ||  lstOli.isEmpty() ) continue;
      Integer i = -1;
      for ( OpportunityLineItem oli : lstOli )
      {
        i++;
        if ( oli.PricebookEntryId == iQuote.PricebookEntryId  && oli.Quantity == iQuote.Quantity 
          && oli.UnitPrice == iQuote.UnitPrice && oli.princing__c == iQuote.princing__c )
        {
        	String idOli = oli.Id;
          List<Related_Aircraft__c> lstRelAircraft = mapOliAircraft.get(idOli);
          if ( lstRelAircraft == null ) continue; 
          for ( Related_Aircraft__c relAirOpp : lstRelAircraft )
          {
          	Related_Aircraft__c relAirQuote = new Related_Aircraft__c();
          	relAirQuote.Aircraft__c = relAirOpp.Aircraft__c;
            relAirQuote.Opportunity_Fleet_Type__c = relAirOpp.Opportunity_Fleet_Type__c;
            relAirQuote.Quote__c = iQuote.QuoteId;
            relAirQuote.Quote_Line_Item__c = iQuote.Id;
            lstQuoteRelatedAircraft.add(relAirQuote);
          }
          break;
        }
      }
      lstOli.remove(i);
    }
    if ( !lstQuoteRelatedAircraft.isEmpty() ) insert lstQuoteRelatedAircraft;   
    
  }
}