/**
 * 
 */
@isTest
private class CFCFavoritesControllerTest 
{
    static testMethod void myUnitTest() 
    {
        Id recordTypeId =  Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
        Id recordTypeImageToHomeId = Schema.SObjectType.Attachments__c.getRecordTypeInfosByName().get('Images to home').getRecordTypeId();  

        User user = CFCTestSetup.getUser(); 
    
        System.runAs(user) 
        {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 

            Product2 Produto = new Product2();
            Produto.Name = 'Produto Teste';
            Produto.ProductCode = 'a488';
            Produto.Product_Status__c = 'Active';
            Produto.RecordTypeId = recordTypeId;
            Produto.IsActive = true;
            database.insert(Produto); 
            
            //ATTACHMENT IMAGE TO HOME
            Attachments__c attToHome = new Attachments__c();
            attToHome.Product__c = Produto.Id;
            attToHome.Active__c = true;
            attToHome.Description__c = 'teste';
            attToHome.Status__c = 'Published';
            attToHome.RecordTypeId = recordTypeImageToHomeId;
            database.insert(attToHome);
            
            Blob b = Blob.valueOf('Test Data'); 
            Attachment attachment = new Attachment();  
            attachment.ParentId = attToHome.Id;  
            attachment.Name = 'Test Attachment for Parent';  
            attachment.Body = b;        
            database.insert(attachment); 
            
            Favorite__c favorite = new Favorite__c(); 
            favorite.CurrencyIsoCode = 'BRL';
            favorite.Product__c = Produto.Id;
            favorite.Active__c = true;
            favorite.User__c = user.Id;
            Insert(favorite);
            
            CFCFavoritesController favorites = New CFCFavoritesController(); 
            favorites.selectedValueOrderBy = 'Products A-Z';
            favorites.deleteItemId = favorite.Id;
            favorites.getOptionsOrderBy();
            favorites.MethodOne();
            favorites.searchFavorites();
                        
            favorites.selectedValueOrderBy = 'Products Z-A';
            favorites.MethodOne();          
            
            favorites.DeleteItem();        
            system.assert(favorites != null); 
        }       
    }    
}