/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class QuoteLineItemDeleteRelatedAircraft
*
* NAME: QuoteLineItemDeleteRelatedAircraftTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*******************************************************************************/
@isTest
private class QuoteLineItemDeleteRelatedAircraftTest {
  
  private static final Integer LOTE = 200;

    static testMethod void myUnitTest() {
    	 Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    	 
    	 Opportunity oportunidade = SObjectInstanceTest.createOpportunity();
    	 Database.insert(oportunidade);
    	 
       Quote opp = SObjectInstanceTest.createQuote(oportunidade.Id);
       opp.Pricebook2Id = stdPB;
       Database.insert(opp);
       
       Product2 prod = SObjectInstanceTest.createProduct2();
       Database.insert(prod);
       
       PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
       Database.insert(pbe);
       
       QuoteLineItem oli = SObjectInstanceTest.createQuoteLineItem(opp.Id, pbe.Id);
       Database.insert(oli);
       
       Aircraft__c air = SObjectInstanceTest.createAircraft();
       Database.insert(air);
       
       Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
       rel.Quote_Line_Item__c = oli.Id;
       Database.insert(rel);
       
       Test.startTest();
       Database.delete(oli);
       Test.stopTest();
       
       List<Related_Aircraft__c> lstRel = [SELECT Id from Related_Aircraft__c WHERE Quote_Line_Item__c =: oli.Id];
       system.assert(lstRel.isEmpty());
    }
    
    static testMethod void lote() {
    	 Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
       
       Opportunity oportunidade = SObjectInstanceTest.createOpportunity();
       Database.insert(oportunidade);
      
      List<Quote> lstOpp = new List<Quote>();
      for (Integer i = 0; i < LOTE ; i++ )
      {
         Quote opp = SObjectInstanceTest.createQuote(oportunidade.Id);
	       opp.Pricebook2Id = stdPB;
	       lstOpp.add(opp);
      }
      Database.insert(lstOpp);
       
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
       
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(SObjectInstanceTest.catalogoDePrecoPadrao(), prod.Id);
      Database.insert(pbe);
     
      List<QuoteLineItem> lstOli = new List<QuoteLineItem>();  
      for (Integer i = 0; i < LOTE ; i++ )
      {
        lstOli.add(SObjectInstanceTest.createQuoteLineItem(lstOpp.get(i).Id, pbe.Id));
      }
      Database.insert(lstOli);
       
      Aircraft__c air = SObjectInstanceTest.createAircraft();
      Database.insert(air);
      
      List<String> lstIds = new List<String>(); 
      List<Related_Aircraft__c> lstRel = new List<Related_Aircraft__c>();  
      for (Integer i = 0; i < LOTE ; i++ )
      {
        Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
        rel.Quote_Line_Item__c = lstOli.get(i).Id;
        lstIds.add(lstOli.get(i).Id);
        lstRel.add(rel);
      }
      Database.insert(lstRel);
       
      Test.startTest();
      Database.delete(lstOli);
      Test.stopTest();
       
      List<Related_Aircraft__c> lstRelResult = [SELECT Id from Related_Aircraft__c WHERE Quote_Line_Item__c =: lstIds];
      system.assert(lstRelResult.isEmpty());
    }
}