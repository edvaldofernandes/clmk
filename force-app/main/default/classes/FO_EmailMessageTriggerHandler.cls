/*
'0' = New
'1' = Read
'2' = Replied
'3' = Sent
'4' = Forwarded
'5' = Draft
*/

public without sharing class FO_EmailMessageTriggerHandler {
   
    public Map<Id,EmailMessage> newRecordsMap = new Map<Id,EmailMessage>();
    public Map<Id,EmailMessage> oldRecordsMap = new Map<Id,EmailMessage>(); 
    public List<EmailMessage> newRecords = new List<EmailMessage>();
    public List<EmailMessage> oldRecords = new List<EmailMessage>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public FO_EmailMessageTriggerHandler(boolean isExecuting){   this.isExecuting = isExecuting;   }
      
    public void OnBeforeInsert(){}

    public void OnAfterInsert(){
        
        CaseLastEmailUpdate(this.newRecords[0]);
        CaseStatusUpdate(this.newRecords[0]);
        
    }

    public void OnBeforeUpdate(){}

    public void OnAfterUpdate(){
        
        CaseStatusUpdate(this.newRecords[0]);
        
    }

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}

    public boolean IsTriggerContext{get{ return isExecuting;}}
    
    private void CaseLastEmailUpdate(EmailMessage em){
        try {         
            Case c = [SELECT FlightOps_Last_Email_Received__c,Id,RecordTypeId FROM Case WHERE Id =: em.ParentId];
        
            //verifica se o case dono do email é do tipo FlightOps Case Record Type
            RecordType rt = [SELECT Name FROM RecordType WHERE Id =: c.RecordTypeId AND isActive=true AND SobjectType='Case'];
            if ( rt.Name == 'FlightOps Case Record Type'){
         
                if (em.FlightOps_Flag__c == true) {
                    
                    if ( em.Incoming==True ){ 
                        c.FlightOps_Last_Email_Received__c = System.now();
                        update c;
                    }
                }
            } 
        } catch (Exception e) {         
            System.debug('Exception = ' + e);
            System.debug('EmailMessage = ' + em);
        }

    }    
    private void CaseStatusUpdate(EmailMessage em){
        try {         
        String CaseId;
        String ToAd;
        Datetime MessageDateTime;
                      
        Case c = [SELECT Subject,CaseNumber,FlightOps_Last_Email_Received__c,Id,OwnerId,Contact.Name,ContactId,SuppliedEmail,FlightOps_Initial_Feedback__c,Status,RecordTypeId FROM Case WHERE Id =: em.ParentId];
    
        //verifica se o case dono do email é do tipo FlightOps Case Record Type
        RecordType rt = [SELECT Name FROM RecordType WHERE Id =: c.RecordTypeId AND isActive=true AND SobjectType='Case'];
        if (rt.Name == 'FlightOps Case Record Type') {
             
            if (em.FlightOps_Flag__c == true) {
            
                //---------------------------case status update e envia email-------------------------------------
            	if ( em.Status=='0' ){ // 0 = New
                
                    c.FlightOps_Email_Unread__c = 'True'; 
                	List<EmailMessage> emails = [SELECT Id FROM EmailMessage WHERE ParentId =: c.Id LIMIT 10];
                	if (emails.size()>1){
                  
                    	List<User> listaU = [SELECT Id,Name,Email FROM User WHERE Id =: c.OwnerId ];
                    	if(listaU!=null){
                        	for(User u : listaU ){
                    			enviarEmail(c,em);
                        	}
                    	}
                	}
                
            	}else{
                
                	List<EmailMessage> emails = [SELECT ParentId FROM EmailMessage WHERE ParentId =: c.Id AND Status = '0']; 
                	if(emails!=null){
                    	c.FlightOps_Email_Unread__c = 'True';
                	}else{
                    	c.FlightOps_Email_Unread__c = 'False';
                	}
            	}
            	//---------------------------- fim case status update-------------------------------
            
                  
            	//-------------------------  initial feedback----------------------------
            	// initial feedback se o email é de um contato
            	if (c.ContactId!=null){
            	
                	Contact[] cont = [SELECT Id,Email,Secondary_Email__c FROM Contact WHERE Id =: c.ContactId];
            
            		if(cont!=null){
                
                		for (integer i=0;i<cont.size();i++){
                        
                        	//se deu reply para quem enviou o email
                        	if (em.Status=='3'){
                            
                            	if ( (em.ToAddress!=null) && ( ( (cont[i].Email!=null)&&(em.ToAddress.toLowerCase().contains(cont[i].Email)) )||( (cont[i].Secondary_Email__c!=null)&&(em.ToAddress.toLowerCase().contains(cont[i].Secondary_Email__c)) ) ) ){

                					if (c.Status=='New'){
                            			c.Status = 'In Progress';
                            			c.FlightOps_Initial_Feedback__c = Datetime.now();
                        			}
            					}
                        	}
            			
                    		//se deu reply e nao foi salvo como draft
                			if ( (em.Incoming==False) && ( ( (cont[i].Email!=null)&&(em.ToAddress.toLowerCase().contains(cont[i].Email)) )||( (cont[i].Secondary_Email__c!=null)&&(em.ToAddress.toLowerCase().contains(cont[i].Secondary_Email__c)) ) ) && (em.Status!='5') ){
    
                				CaseId = em.ParentId;  
                				MessageDateTime = em.MessageDate;
                				ToAd = em.ToAddress.toLowerCase(); 
        
                				for (EmailMessage emm : this.newRecords){
            
                    				List<EmailMessage> email = [SELECT ParentId FROM EmailMessage WHERE ParentId =: CaseId AND Incoming=True AND FromAddress =: ToAd AND MessageDate <: MessageDateTime]; 
            
                    				if (email!=null) {
                
                        				if (c.Status=='New'){
                            				c.Status = 'In Progress';
                            				c.FlightOps_Initial_Feedback__c = Datetime.now();
                        				}
                    				}
                				} 
            				}
                		}
            		}
            	}else{//se o email nao é de um contato
                
                	// se deu reply para o mesmo email que chegou 
                	if (em.Status=='3'){
                    
                    	if ( (em.ToAddress!=null) && (c.SuppliedEmail!=null) && ( em.ToAddress.toLowerCase().contains(c.SuppliedEmail) ) ){
                        
                        	if (c.Status=='New'){
                            			c.Status = 'In Progress';
                            			c.FlightOps_Initial_Feedback__c = Datetime.now();
                        			}
                		}
                	}
                
                	// se deu reply para o mesmo email e nao salvou draft
                	if ( (em.Incoming==False) && (em.ToAddress!=null) && (c.SuppliedEmail!=null) && ( em.ToAddress.toLowerCase().contains(c.SuppliedEmail) ) && (em.Status!='5') ){
    
                			CaseId = em.ParentId;  
                			MessageDateTime = em.MessageDate;
                			ToAd = em.ToAddress.toLowerCase(); 
        
                			for (EmailMessage emm : this.newRecords){
            
                    			List<EmailMessage> email = [SELECT ParentId FROM EmailMessage WHERE ParentId =: CaseId AND Incoming=True AND FromAddress =: ToAd AND MessageDate <: MessageDateTime]; 
            
                    			if (email!=null) {
                
                        			if (c.Status=='New'){
                            			c.Status = 'In Progress';
                            			c.FlightOps_Initial_Feedback__c = Datetime.now();
                        			}
                    			}
                			} 
            			}
            	}
            	//--------------------------------- fim initial ------------------------
            
            
            	//---------------------------------- abrir o case fechado ----------------------------------
            	if ( (em.Incoming==True)&&(  (c.Status=='Closed')||(c.Status=='Archived')) ){
                	c.Status='In Progress';
            	}
            	//--------------------------- fim abri o case fechado --------------------------------------
                        
            update c; 
        	}
        }
            
        } catch (Exception e) {         
            System.debug('Exception = ' + e);
            System.debug('EmailMessage = ' + em);
        }
    }
    
    private static Boolean runningInASandbox() {
      return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    private void enviarEmail(Case caso,EmailMessage em){
        if (!runningInASandbox()) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            String emailAddr = [select Email from User where Id = :caso.OwnerId].Email;
            String OwnerName = [select Name from User where Id = :caso.OwnerId].Name;
            String ide = caso.Id;
            ide = ide.substring(0, ide.length()-3);
            
            String[] toAddresses = new String[] {emailAddr};
                mail.setToAddresses(toAddresses);
            
            mail.setSubject('New Email Message');
            
            mail.setHtmlBody('Dear '+OwnerName+', your case number <b>'+caso.CaseNumber+'</b> has a new email message.'+
                             '<br><br>Subject: <b>'+caso.Subject+'</b>'+
                             //'<br></br>Link: '+URL.getSalesforceBaseUrl().toExternalForm()+ '/console#%2F'+ide );
                             '<br></br>Link: <a href=" https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ).toLowerCase() + '/console#%2F' + ide + '" target="_blank">'+ 'https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ).toLowerCase() +'/console#%2F' +ide+'</a>');
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }     
}