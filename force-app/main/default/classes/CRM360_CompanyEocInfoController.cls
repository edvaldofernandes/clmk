public class CRM360_CompanyEocInfoController{

    public CRM360_CompanyEocInfoController() {
        getMfirs();
    }
    // Gethering Account Info
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private static Account acc = [SELECT Name, Id FROM Account WHERE Id = :accId];
        
    public static Account getAcc(){
        return acc;
    }

    // Filter Parameters
    private static Id RecordTypeIdEOCSMeetings = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('EOC Side Meeting').getRecordTypeId();
    private static Id RecordTypeIdEventHeadsUp = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('Operators Conference - Heads up').getRecordTypeId();
    private static String leadFilter = 'Embraer E-Jets Worldwide Digital Conference 2020';
    private static String relatedEOCFilter = 'Embraer E-Jets Worldwide Digital Conference 2020';
    private static Campaign campaign = [SELECT Id, Name FROM Campaign WHERE Name = 'Embraer E-Jets Worldwide Digital Conference 2020'];
    private static List<String> memberStatusFilter = new List<String>();
    private List<MFIR__c> mfirs = new list<MFIR__c>();

    
    // Count Variables
    private static Integer leadAmount = 0;
    private static Integer registeredAmount = 0; 
    private static Integer sMeetingsAmount = 0; 
    
    Public Id KeyCustomer {get; set;}
    
    // Getter for count variables
    public static Integer getLeadAmount(){return leadAmount;}
    public static Integer getRegisteredAmount(){return registeredAmount;}
    public static Integer getSMeetingsAmount(){return sMeetingsAmount;}
    public Decimal receivables {get; set;}
    public Decimal toBeDue {get; set;}
    public Decimal overdue {get; set;}
    public Decimal dispute {get; set;}
    public Decimal toBeApplied {get; set;}
    public Decimal creditLimit {get; set;}
    public Decimal exposure {get; set;}
    
    //getMfirData
    public List<MFIR__c> getMfirs (){
        this.toBeDue = 0;
        this.overdue = 0;
        this.dispute = 0;
        this.toBeApplied = 0;
        this.creditLimit = 0;
        this.exposure = 0;
        this.mfirs = [SELECT MFIR_number__c, To_be_applied_QK__c, Block_Status_QK__c, Credit_Limit_QK__c, Dispute_QK__c, Exposure_QK__c, Overdue_QK__c, To_be_due_QK__c FROM MFIR__c WHERE Account__c = :accId];
        if(this.mfirs.size() != 0){
            for(MFIR__c mfir : this.mfirs){
                If(mfir.To_be_due_QK__c != null && mfir.Overdue_QK__c != null && mfir.Dispute_QK__c != null && mfir.To_be_applied_QK__c != null && mfir.Credit_Limit_QK__c != null && mfir.Exposure_QK__c != null){
                       this.toBeDue += mfir.To_be_due_QK__c;
                       this.overdue += mfir.Overdue_QK__c;
                       this.dispute += mfir.Dispute_QK__c;
                       this.toBeApplied += mfir.To_be_applied_QK__c;
                       this.creditLimit += mfir.Credit_Limit_QK__c;
                       this.exposure += mfir.Exposure_QK__c;
                       
                   }
            }
            this.toBeDue = this.toBeDue/1000;
            this.overdue = this.overdue/1000;
            this.dispute = this.dispute/1000;
            this.toBeApplied = this.toBeApplied/1000;
            this.receivables = this.toBeDue + this.overdue;
            this.creditLimit = this.creditLimit/1000;
            this.exposure = this.exposure/1000;
        }
        return this.mfirs;
    }
    
    /*Items for Select List of EOC
    
	public string EOC { get; set;}
 
    public List<SelectOption> getEOCs() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All', 'All'));
        
        Schema.DescribeFieldResult fieldResult = Account_Cockpit__c.Related_EOC__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    } 
    
    public PageReference updateInfo(){
        return null;
    } 
    */
    // Get List of Leads acoording to Lead Filter
    // Use list name: leadsList     <===============
    // Fields : Product__c, Sales_Potential__c, Level_of_interest__c
    
    public static List<S_S_Leads__c> getLeadsList(){
        List<S_S_Leads__c> leadsList = new List<S_S_Leads__c>();
        leadsList = [	SELECT Product_ref__c, Sales_Potential__c, Level_of_interest__c, CurrencyIsoCode, Id
                     	FROM S_S_Leads__c 
                     	WHERE (Event__c = :leadFilter AND Company__c = :acc.Name)
                    ];
        if(leadsList.size() > 0){
            leadAmount = leadsList.size();
            // Return results
            return leadsList;
        }
        return null;
    }
    
    //Get Side Meetings List for the account
    //Use list name: sMeetingsList
    //Fields: Name, Customer_Expectation__c
    //For color use style="color :{! if(sMeeting.Att_Level__c == 'Low', green,if(sMeeting.Att_Level__c == 'Medium', yellow, red))}"
    public static List<Account_Cockpit__c> getSideMeetingsList(){
        List<Account_Cockpit__c> sideMeetingsList = new List<Account_Cockpit__c>();
        sideMeetingsList = [SELECT Name, Customer_Expectation__c, Att_Level__c, Id 
                            FROM Account_Cockpit__c 
                            WHERE (RecordTypeId = :RecordTypeIdEOCSMeetings 
                            AND Related_EOC__c = :relatedEOCFilter AND Account_Name__c = :acc.Id)
                            ORDER BY Att_Level__c DESC, Name ASC
                           ];
        
        if(sideMeetingsList.size() > 0){
            sMeetingsAmount = sideMeetingsList.size();
            //Return results
            
            return sideMeetingsList;
        }
        return null;
    }
    public static Account_Cockpit__c getHeadsUpInfo(){
        List<Account_Cockpit__c> headsUpInfoList = new List<Account_Cockpit__c>();
        headsUpInfoList = [SELECT Name, Key_Person__c, Key_Customer_Name__c, Key_Customer_Person_Linkedin__c, Heads_Up__c 
                            FROM Account_Cockpit__c 
                            WHERE (RecordTypeId = :RecordTypeIdEventHeadsUp 
                            AND Related_EOC__c = :relatedEOCFilter AND Account_Name__c = :acc.Id)
                            ORDER BY Name ASC
                           ];
        
        if(headsUpInfoList.size() > 0){
            //Return results
            return headsUpInfoList[0];

        }
        return null;
    }
    
    //Get Campaign Members
    //Use list name: campaignMembers
    //Fields: FirstName, LastName, CompanyOrAccount
    public static List<CampaignMember> getCampaignMembers(){
        memberStatusFilter.add('Registered');
        memberStatusFilter.add('Attended');
        
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        
        List<Contact> contactsList = new List<Contact>();
        contactsList = [Select ID from Contact where AccountId = :accId];

        campaignMembers = [SELECT FirstName, LastName, CompanyOrAccount, Id, Title 
                           FROM CampaignMember 
                           WHERE (ContactId in :contactsList AND CampaignId = :campaign.Id AND Status in :memberStatusFilter)
                           ORDER BY FirstName, LastName
                          ];
        
        /*
        campaignMembers = [SELECT FirstName, LastName, CompanyOrAccount, Id, Title 
                           FROM CampaignMember 
                           WHERE(CompanyOrAccount = :acc.Name AND CampaignId = :campaign.Id AND (Status = 'Registered' OR Status = 'Attended'))
                           ORDER BY FirstName, LastName];
        */
        if(campaignMembers.Size() > 0){
            registeredAmount = campaignMembers.size();
            //Return results
            return campaignMembers;
        }
        return null;
    }
    
    // Get EOC Att Level
    // Field: EOCIndicator
    public static String getEOCIndicator(){
        
        List <Account_Cockpit__c> dashboardData = [SELECT CRM360_EOC_Indicator__c,Id
                                			FROM Account_Cockpit__c
                                			WHERE (Record_Type_ID_ref__c = 'CMR360_Dashboard_Data'
                                       		AND Account_Name__c =:acc.Id )];
        if(dashboardData.Size() > 0){
            return dashboardData[0].CRM360_EOC_Indicator__c;
        }
        return null;
    }
}