/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Trigger Action for Service Contract Management  Object       
* @comments: Class to handle methods actions in Service Contract Management
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
public class TriggerActionServiceContractMgmt {
    
    private List<Service_Contract_Management__c> newRecords;
    private List<Service_Contract_Management__c> oldRecords;
    private Map<Id, Service_Contract_Management__c> newRecordsMap;
    private Map<Id, Service_Contract_Management__c> oldRecordsMap;
    
    
    public TriggerActionServiceContractMgmt(TriggerHandlerServiceContractMgmt handler){
        this.newRecords = handler.newList;
        this.oldRecords = handler.oldList;
        this.oldRecordsMap = handler.oldMapRecords;
        this.newRecordsMap = handler.newMapRecords;
    }
    
    public void updateContractLineItemMaster(){
        system.debug('TriggerActionServiceContractMgmt.updateContractLineItemMaster.updateContractLineItemMaster: ' + newRecords);
        for(Service_Contract_Management__c scm : newRecords){
            
            if(scm.Service_Contract_line_item__c != null){
                scm.Contract_Line_Item_Master__c = scm.Service_Contract_line_item__c;
            }else if(scm.Contract_Line_Item_conversion__c != null){
                scm.Contract_Line_Item_Master__c = scm.Contract_Line_Item_conversion__c;
            }else if(scm.Contract_Line_Item_Deliverable__c != null){
                scm.Contract_Line_Item_Master__c = scm.Contract_Line_Item_Deliverable__c;
            }
            
        }
    }
    
    public void updateOppReason(){
        map<id,string> idContractOppReasonMap = new map<id,string>();
        
        for(Service_Contract_Management__c scm : newRecords){
            idContractOppReasonMap.put(scm.Contract_Line_Item_Master__c,'');
        }
        
        for(ContractLineItem cli : [Select id,Opp_Reason__c From ContractLineItem Where Id in : idContractOppReasonMap.keySet()]){
            idContractOppReasonMap.put(cli.Id,cli.Opp_Reason__c);
        }
        
        for(Service_Contract_Management__c scm : newRecords){
            scm.Opp_Reason__c = idContractOppReasonMap.get(scm.Contract_Line_Item_Master__c);
        }
    }
    
    public void recalculateItemsQuantityOnUpdate(){
        Set<Id> eligibleIds = TriggerHelperServiceContractMgmt.getEligibleRecordTypes();
        
        Set<Id> contractLineItemsIds = new Set<Id>();
        for(Service_Contract_Management__c scm : this.newRecords)
        {
            if(eligibleIds.contains(scm.RecordTypeId)) 
            {
                if((scm.Quantity__c != this.oldRecordsMap.get(scm.Id).Quantity__c) || (scm.Contract_Line_Item_Master__c != this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c))
                {
                    contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);                        
                    contractLineItemsIds.add(this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c);                        
                }
            }    
        }
    }
    
    public void recalculateItemsQuantityOnInsert(){
        Set<Id> eligibleIds = TriggerHelperServiceContractMgmt.getEligibleRecordTypes();
        Set<Id> contractLineItemsIds = new Set<Id>();
        for(Service_Contract_Management__c scm : this.newRecords)
        {
            if(eligibleIds.contains(scm.RecordTypeId))
            {
                contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);
            }    
        }
    }
    
    public void recalculateItemsQuantityOnDelete(){
        Set<Id> eligibleIds = TriggerHelperServiceContractMgmt.getEligibleRecordTypes();
        Set<Id> contractLineItemsIds = new Set<Id>();
        List<Service_Contract_Management__c> records = new List<Service_Contract_Management__c>();
        
        
        for(Service_Contract_Management__c scm : this.oldRecords)
        {
            if(eligibleIds.contains(scm.RecordTypeId))
            {
                contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);
            }    
        }
        
        TriggerHelperServiceContractMgmt.recalculateSCMQuantities(contractLineItemsIds);
    }
    
    public void recalculateItemsQuantityOnUnDelete(){
        Set<Id> eligibleIds = TriggerHelperServiceContractMgmt.getEligibleRecordTypes();
        Set<Id> contractLineItemsIds = new Set<Id>();
        List<Service_Contract_Management__c> records = new List<Service_Contract_Management__c>();
        
        
        for(Service_Contract_Management__c scm : this.newRecords)
        {
            if(eligibleIds.contains(scm.RecordTypeId))
            {
                contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);                
            }    
        }
        
        TriggerHelperServiceContractMgmt.recalculateSCMQuantities(contractLineItemsIds);
    }
    
    
    public void newServiceContractManagement(){
        system.debug('===newServiceContractManagement: ');
        AccReceivablesUpdateContractLineItem.newServiceContractManagement(newRecords);
    }
    
    public void updateServiceContractManagement(){
        AccReceivablesUpdateContractLineItem.updateServiceContractManagement(newRecords, oldRecordsMap); 
    } 
    
    public void deleteServiceContractManagement(){
        AccReceivablesUpdateContractLineItem.deleteServiceContractManagement(oldRecords);
    }
    
    
    public void updateProductRecordTypeOnAfterInsert()
    {
        List<Service_Contract_Management__c> recordList = new List<Service_Contract_Management__c>();
        Set<Id> eligibleIds = new Set<Id>();
        eligibleIds = this.newRecordsMap.keySet();
        for(Service_Contract_Management__c scm : [Select Id,Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name 
                                                  from Service_Contract_Management__c Where id in : eligibleIds] )
        {
            scm.productRecordType__c = scm.Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name;
            recordList.add(scm);
        }
        
        if(!recordList.isEmpty())
            update recordList;
        
    }
    
    public void updateProductRecordTypeOnAfterUpdate(){
        List<Service_Contract_Management__c> recordList = new List<Service_Contract_Management__c>();
        Set<Id> eligibleIds = new Set<Id>();
        for(Service_Contract_Management__c scm : this.newRecords)
        {
            if(scm.Contract_Line_Item_Master__c != this.oldRecordsMap.get(scm.Id).Contract_Line_Item_Master__c)
                eligibleIds.add(scm.Id);    
        }
        for(Service_Contract_Management__c scm : [Select Id,Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name from Service_Contract_Management__c Where id in : eligibleIds] )
        {
            scm.productRecordType__c = scm.Contract_Line_Item_Master__r.PricebookEntry.Product2.RecordType.Name;
            recordList.add(scm);
        }
        
        if(!recordList.isEmpty())
            update recordList;
        
    }
    
    public void updateEntitlementConversion(){
        system.debug('===updateEntitlementConversion:');
        
        Id recTypeConversion = RecordTypeMemory.getRecType('Service_Contract_Management__c', 'Entitlement_Conversion');
        
        list<Service_Contract_Management__c> lstSvcContractManagement = new list<Service_Contract_Management__c>();
        set<Id> lstContractLineItemId = new set<Id>();
        
        if(Trigger.isUpdate || Trigger.isInsert) {
            
            for(Service_Contract_Management__c scm : (list<Service_Contract_Management__c>) this.newRecords) {
                if(TriggerUtils.wasChanged(scm, Service_Contract_Management__c.Quantity__c)) {
                    if (scm.Contract_Line_Item_Deliverable__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_Deliverable__c);
                    if (scm.Contract_Line_Item_conversion__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_conversion__c);
                    lstSvcContractManagement.add(scm);
                }
            }
            
        }
        else if(Trigger.isDelete) {
            
            for(Service_Contract_Management__c scm : (list<Service_Contract_Management__c>) this.oldRecords) {
                if (scm.Contract_Line_Item_Deliverable__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_Deliverable__c);
                if (scm.Contract_Line_Item_conversion__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_conversion__c);
                lstSvcContractManagement.add(scm);
            }
            
        }
        
        if(lstSvcContractManagement.isEmpty()) return;
        
        map<Id, ContractLineItem> mapContractLineItem = new map<Id, ContractLineItem>([SELECT Id, Delivered__c, Quantity, Converted__c 
                                                                                       FROM ContractLineItem WHERE Id = :lstContractLineItemId ]); //and Status__c != 'Converted']);
        
        if(mapContractLineItem.isEmpty()) return;
        
        list<ContractLineItem> lstUpdateContractLineItem = new list<ContractLineItem>();
        
        for(Service_Contract_Management__c scm : lstSvcContractManagement) {
            Decimal qtd = scm.Quantity__c != null ? scm.Quantity__c : 0;
            
            ContractLineItem conli;
            if ( scm.RecordTypeId == recTypeConversion )
            {
                conli = mapContractLineItem.get(scm.Contract_Line_Item_conversion__c);
            }
            else
            {
                conli = mapContractLineItem.get(scm.Contract_Line_Item_Deliverable__c);
            }
            if(conli == null) return;
            
            if ( scm.RecordTypeId == recTypeConversion )
            {
                if(conli.Converted__c == null) conli.Converted__c = 0;
                
                if(Trigger.isUpdate )
                {
                    Decimal qtdOld = ( ( Service_Contract_Management__c ) this.oldRecordsMap.get( scm.id ) ).Quantity__c;
                    conli.Converted__c -= qtdOld != null ? qtdOld : 0;
                }
                
                if(Trigger.isUpdate || Trigger.isInsert) {
                    conli.Converted__c += qtd;
                }
                else if(Trigger.isDelete) {
                    if(conli.Converted__c > 0) {
                        conli.Converted__c -= qtd;
                    }
                    else {
                        conli.Converted__c = 0;
                    }
                }
            }
            else
            {
                if(conli.Delivered__c == null) conli.Delivered__c = 0;
                
                if(Trigger.isUpdate )
                {
                    Decimal qtdOld = ( ( Service_Contract_Management__c ) this.oldRecordsMap.get( scm.id ) ).Quantity__c;
                    conli.Delivered__c -= qtdOld != null ? qtdOld : 0;
                }
                
                if(Trigger.isUpdate || Trigger.isInsert) {
                    conli.Delivered__c += qtd;
                }
                else if(Trigger.isDelete) {
                    if(conli.Delivered__c > 0) {
                        conli.Delivered__c -= qtd;
                    }
                    else {
                        conli.Delivered__c = 0;
                    }
                }
            }
            
            lstUpdateContractLineItem.add(conli);
        }
        system.debug('===updateEntitlementConversion.lstUpdateContractLineItem:' + lstUpdateContractLineItem);
	        if(!lstUpdateContractLineItem.isEmpty()) update lstUpdateContractLineItem;
    }
    
}