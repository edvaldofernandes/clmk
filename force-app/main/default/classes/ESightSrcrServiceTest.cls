@isTest(SeeAllData=true)
public class ESightSrcrServiceTest {
    
    @isTest static void parseToObjectTest(){
        
        List<ESightSrcr> eSightSrcrList     = new List<ESightSrcr>();
		List<SrcrRankCrs> rankCrsObjectList = new List<SrcrRankCrs>();
        List<RankSr> rankSrsObjectList      = new List<RankSr>();
        
        SrcrRankCrs rankCrs = new SrcrRankCrs();
        RankSr rankSrs = new RankSr();
        
        rankCrs.delayContrib = 10.06;
        rankCrs.failCode = 2L;
        rankCrs.failCodeDescription = 'fail test code';
        rankCrs.qtyEvents = 3L;
        rankCrs.ranking = 4L;
        
        rankSrs.delayContrib = 10.06;
        rankSrs.failCode = 2L;
        rankSrs.failCodeDescription = 'fail test code';
        rankSrs.qtyEvents = 3L;
        rankSrs.ranking = 4L;
        
        rankCrsObjectList.add(rankCrs);
        rankSrsObjectList.add(rankSrs);
        
        ESightSrcr eSightSrcr  = new ESightSrcr(rankCrsObjectList, rankSrsObjectList);
        
        eSightSrcr.cr = 10.02;
        eSightSrcr.family = 'test 190';
        eSightSrcr.operatorId = 1111111111112222L;
        eSightSrcr.operatorName = 'test pilot';
        eSightSrcr.rateCr = 10.03;
        eSightSrcr.rateSr = 10.04;
        eSightSrcr.referenceDate = datetime.newInstance(2015, 12, 5);
        eSightSrcr.sr = 10.05;
        
        eSightSrcrList.add(eSightSrcr);
        
        ESightInboundEndPoint.execute(eSightSrcrList);    
        //ESightSrcrService eSightSrcrService = new ESightSrcrService(eSightSrcrList);
        
        Test.startTest();
        
        //eSightSrcrService.parseToObject();
        
        EsightSrCr__c esightSrCrObj = [SELECT id,
                                              cr__c ,
                                              family__c , 
                                       		  operatorId__c,
                                       		  operatorName__c ,
                                              rateCr__c ,rateSr__c ,
                                              referenceDate__c , 
                                              sr__c 
                                              FROM EsightSrCr__c
                                              WHERE operatorName__c = 'test pilot' limit 1];
        
        SrcrRankCrs__c srcrRankCrsObj = [SELECT id,
                                       operatorId__c,
                                       delayContrib__c,
                                       failCode__c,
                                       failCodeDescription__c,
                                       qtyEvents__c,
                                       ranking__c
                                       FROM SrcrRankCrs__c
                                       WHERE failCodeDescription__c = 'fail test code' limit 1];
        
         SrcrRankSrs__c srcrRankSrsObj = [SELECT id,
                                       operatorId__c,
                                       delayContrib__c,
                                       failCode__c,
                                       failCodeDescription__c,
                                       qtyEvents__c,
                                       ranking__c
                                       FROM SrcrRankSrs__c
                                       WHERE failCodeDescription__c = 'fail test code' limit 1];
		
        ESightSrcr eSightSrcrConstrutor = new ESightSrcr();        
        
        System.assertEquals(esightSrCrObj.family__c,  eSightSrcr.family);
        System.assertEquals(srcrRankCrsObj.operatorId__c, esightSrCrObj.operatorId__c);
        System.assertEquals(srcrRankSrsObj.operatorId__c, esightSrCrObj.operatorId__c);

        
        Test.stopTest();
    }
}