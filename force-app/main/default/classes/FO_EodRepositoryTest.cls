/**
* @description: FO_FupRepository test class.
**/
@isTest
public class FO_EodRepositoryTest {
    @isTest static void testCanGetEodById(){
        EOD__c expectedFup = new FO_EodTestDataBuilder().build();
        
        
        EOD__c actualFup = FO_EodRepository.getById(expectedFup.Id);
        
        
        System.assertEquals(expectedFup.Id, actualFup.Id);
    }
}