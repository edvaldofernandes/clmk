/*
----------------------------------------------------------------------------------------------
-- - Company:     Embraer
-- - Name:        CRC_IndicatorBodyQuery
-- - Description: Class responsible for retrieving data regarding CRC Dashboard cards
-- - @Author: Tiago de Jesus Rodrigues
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version
----------------------------------------------------------------------------------------------
-- 06/05/2019		Tiago de Jesus Rodrigues		1.0
-- 06/10/2019		André Vitor Leinio Graça		1.1
-- 10/09/2019		Fabiano Albino Ferreira			1.2 
-- 19/11/2019		Fabiano Albino Ferreira			1.3         Remove condition LastModifiedDate => :firstDayOfMonth
----------------------------------------------------------------------------------------------
*/

public class CRC_IndicatorBodyQuery {

    public static Date firstDayOfMonth = System.Date.today().toStartOfMonth();
    public static string CRCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();

    // Method that returns the count of all CRC Cases in Processing, with a specified priority, from the types Spare Parts, Broker or RSPL. The results are
    // grouped by case reason and SLA status.
    // Add IsClosed = false
    public static List<AggregateResult> getTotalCardDataByPriority(String priority){
        List<AggregateResult> data = new List<AggregateResult>();
        if (priority=='TOTAL'){
        	data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status = 'Processing'
                    AND Reason <> NULL
                    AND RecordTypeId =: CRCRecordType
                    AND (Type = 'Spare Parts' OR Type = 'Broker' or Type = 'RSPL')
                    AND IsClosed = false
                    GROUP BY Reason,SLA_report_status__c];
        } else {
            data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status = 'Processing'
                    AND Reason <> NULL
                    AND Priority =: priority
                    AND RecordTypeId =: CRCRecordType
                    AND (Type = 'Spare Parts' OR Type = 'Broker' or Type = 'RSPL')
                    AND IsClosed = false
                    GROUP BY Reason,SLA_report_status__c];
        }
        return data;
    }

    // Method that returns the count of all CRC Cases, excepet for the dispatch status, with a specified priority. The results are grouped by SLA status.
    public static List<AggregateResult> getAllTotalCardDataByPriority(String priority){
        List<AggregateResult> data = new List<AggregateResult>();
        if (priority=='TOTAL'){
        	data = [SELECT count(Id),SLA_report_status__c
                    FROM Case
                    WHERE Reason <> NULL
                    AND Status <> 'Dispatch'
                    AND RecordTypeId =: CRCRecordType
                    AND CreatedDate  >= :firstDayOfMonth
                    AND IsClosed = false
                    GROUP BY SLA_report_status__c];
        } else {
            data = [SELECT count(Id),SLA_report_status__c
                    FROM Case
                    WHERE Reason <> NULL
                    AND Priority =: priority
                    AND Status <> 'Dispatch'
                    AND RecordTypeId =: CRCRecordType
                    AND CreatedDate  >= :firstDayOfMonth
                    AND IsClosed = false
                    GROUP BY SLA_report_status__c];
        }
        return data;
    }

    // Method that returns the count of all CRC Cases in Processing, with a specified type. The results are grouped by case reason and SLA status.
    public static List<AggregateResult> getTotalCardDataByType(String type){
        List<AggregateResult> data = new List<AggregateResult>();
        if (type != 'SB') {
        	data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status = 'Processing'
                    AND Reason <> NULL
                    AND Type =: type
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = false
                    GROUP BY Reason,SLA_report_status__c];
        } else {
            data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status = 'Processing'
                    AND Reason <> NULL
                    AND (Type = 'SB' OR Type = 'Credit Concession')
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = false
                    GROUP BY Reason,SLA_report_status__c];
        }
        return data;
    }

    // Method that returns the count of all CRC Cases, except for the dispatch, with a specified type. The results are grouped by SLA status.
    //Add IsClosed = false
	public static List<AggregateResult> getAllTotalCardDataByType(String type){
        List<AggregateResult> data = new List<AggregateResult>();
        if (type != 'SB') {
        	data = [SELECT count(Id),SLA_report_status__c
                    FROM Case
                    WHERE Reason <> NULL
                    AND Type =: type
                    AND Status <> 'Dispatch'
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = false
                    GROUP BY SLA_report_status__c];
        } else {
            data = [SELECT count(Id),SLA_report_status__c
                    FROM Case
                    WHERE Reason <> NULL
                    AND (Type = 'SB' OR Type = 'Credit Concession')
                    AND Status <> 'Dispatch'
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = false
                    GROUP BY SLA_report_status__c];
        }
        return data;
    }

	// Method that returns the count of all CRC Cases in Processing with ATD origin. The results are grouped by case reason and SLA status.
    // Add IsClosed = false
    public static List<AggregateResult> getATDCardData(){
        List<AggregateResult> data = new List<AggregateResult>();
        data = [SELECT count(Id),Reason,SLA_report_status__c
                FROM Case
                WHERE Status = 'Processing'
                AND Reason <> NULL
                AND RecordTypeId =: CRCRecordType
                AND Origin = 'ATD'
                AND IsClosed = false
                GROUP BY Reason,SLA_report_status__c];
        return data;
    }

    // Method that returns the count of all CRC Cases, except for the dispatch, with ATD origin. The results are grouped by SLA status.
    //Add isClosed = false
	public static List<AggregateResult> getAllATDCardData(){
        List<AggregateResult> data = new List<AggregateResult>();
       	data = [SELECT count(Id),SLA_report_status__c
                FROM Case
                WHERE Reason <> NULL
                AND Status <> 'Dispatch'
                AND RecordTypeId =: CRCRecordType
                AND Origin = 'ATD'
                AND IsClosed = false
                GROUP BY SLA_report_status__c];
        return data;
    }
    // Method that returns the count of all CRC Cases with defined Status. The results are grouped by case reason and SLA status.
      public static List<AggregateResult> getTotalCardDataByStatus(String status){
          List<AggregateResult> data = new List<AggregateResult>();

          if(status == 'OTHERS'){
            data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status IN ('Contract Creation', 'Financial','Follow Up','New Customer','Warehouse')
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = FALSE
                    GROUP BY Reason,SLA_report_status__c];
          }
          else if(status == 'ENG/RTS'){
            data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status IN ('Engineering', 'RTS')
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = FALSE
                    GROUP BY Reason,SLA_report_status__c];
          }
          else{
            data = [SELECT count(Id),Reason,SLA_report_status__c
                    FROM Case
                    WHERE Status =: status
                    AND RecordTypeId =: CRCRecordType
                    AND IsClosed = FALSE
                    GROUP BY Reason,SLA_report_status__c];
          }
          return data;
      }

    
    
}