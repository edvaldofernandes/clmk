public class SpecialInvestigationsTriggerHandler{
    
    static final Id RTYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
    
    //before insert add error to duplicate inserts of special investigations cases
    public static void beforeInsert(List<Case> newInserts){
        eliminateDuplicatesInDatabase(eliminateDuplicatesInBatch(filterRecordType(newInserts)));
    }
    
    //after insert parse email content and populate case
    public static void afterInsert(List<Case> newInserts){
        List<Id> validCases = new List<Id>();
        for(Case newInsert : newInserts){
            if(isValidCase(newInsert))
                validCases.add(newInsert.Id);
        }
        
        if(!validCases.isEmpty())
        	populateCases(validCases);
    }
    
    @future
  	private static void populateCases(List<Id> caseIds){
        Map<string, List<Case>> partNumberMap = new Map<string, List<Case>>();
        Map<string, List<Case>> serialNumberMap = new Map<string, List<Case>>();
        
        List<Case> newCases = [SELECT Id, Subject, Description FROM Case WHERE Id IN :caseIds FOR UPDATE];
        
        for(Case newCase : newCases){
            string partNumber = parseSubject(newCase);
            if(!partNumberMap.containsKey(partNumber))
                partNumberMap.put(partNumber, new List<Case>());
            partNumberMap.get(partNumber).add(newCase);
            
            string serialNumber = parseDescription(newCase);
            if(!serialNumberMap.containsKey(serialNumber))
                serialNumberMap.put(serialNumber, new List<Case>());
            serialNumberMap.get(serialNumber).add(newCase);
        }
        
        for(LLPDatabase__c part : [SELECT Id, Name FROM LLPDatabase__c WHERE Name IN :partNumberMap.keySet()]){
            for(Case newCase : partNumberMap.get(part.Name)){
                newCase.Part_Number__c = part.Id;
            }
        }
        
        for(Aircraft__c aircraft : [SELECT Id, Name FROM Aircraft__c WHERE Name IN :serialNumberMap.keySet()]){
            for(Case newCase : serialNumberMap.get(aircraft.Name)){
                newCase.Aircraft_Serial_Number__c = aircraft.Id;
            }
        }
        
        database.update(newCases, false);
    }
    
    //filter out cases which are not Special Investigations record type
    private static List<Case> filterRecordType(List<Case> casesToFilter){
        List<Case> specialInvestigationsCases = new List<Case>();
        for(Case newCase : casesToFilter){
            if(newCase.RecordTypeId == RTYPE_ID)
                specialInvestigationsCases.add(newCase);
    	}
        return specialInvestigationsCases;
    }
    
    //adds error to cases with duplicates within the inserted batch
    private static List<Case> eliminateDuplicatesInBatch(List<Case> casesToValidate){
        Integer i = 0, j = 0;
        while(i < casesToValidate.size()){
            j=i+1;
            while(j < casesToValidate.size()){
                if(casesToValidate[i].Subject == casesToValidate[j].Subject){
                    casesToValidate[i].addError('This case is a duplicate within the insert batch.');
                    casesToValidate.remove(i);
                    continue;
                }
                j++;
            }
            i++;
        }
        return casesToValidate;
    }
    
    //adds error to cases with duplicates already in the database
    private static List<Case> eliminateDuplicatesInDatabase(List<Case> casesToValidate){
        List<Case> possibleDuplicates = getPossibleDuplicates(casesToValidate);
        
        //put the notifs and subjects of each possible duplicate into a map
        //then use the map to check if each case to validate already exists in the DB with same notif and subject
        if(!possibleDuplicates.isEmpty()){
            Map<String, String> possibleDuplicateMap = new Map<String, String>();
            for(Case possibleDuplicate : possibleDuplicates){
                possibleDuplicateMap.put(possibleDuplicate.Notification__c, possibleDuplicate.Subject);	//key = notification, value = subject
            }
            integer i = 0;
            while (i < casesToValidate.size()){
                if(possibleDuplicateMap.containsKey(casesToValidate[i].Notification__c)){
                    if(possibleDuplicateMap.get(casesToValidate[i].Notification__c) == casesToValidate[i].Subject){
                        casesToValidate[i].addError('This case has a duplicate in the database.');
                        casesToValidate.remove(i); 
                        continue;
                    }
                }
                i++;
            }
        }
        return casesToValidate;
    }
    
    //get cases from the DB with those notifications
    private static List<Case> getPossibleDuplicates(List<Case> casesToValidate){
        //set notification numbers for each case
        List<String> notifs = new List<String>();
        for(Case caseToValidate : casesToValidate){
            string notif = caseToValidate.Subject.substringBetween('Notif ', ' on');
            if(String.isNotBlank(notif)){
                caseToValidate.Notification__c = notif.trim();
            	notifs.add(caseToValidate.Notification__c);
            }
        }
        return [SELECT Notification__c, Subject, Description FROM Case WHERE Notification__c IN :notifs AND Status!='Closed' AND RecordType.Name='Special Investigations'];
    }
    
    private static boolean isValidCase(Case checkCase){
        return (checkCase.RecordTypeId == RTYPE_ID 
                && !String.IsBlank(checkCase.Subject) 
                && (checkCase.Subject.contains('Automation Identified') 
                    || checkCase.Subject.contains('Customer Request Identified') 
                    || checkCase.Subject.contains('Investigation Item Detected')
                   )
               );
    }
    
    //Parse the case's subject
    //Set fields: Reason for Requesting Investigation and Serial Number
    //Return part number
    private static string parseSubject(Case theCase){
        string subject = theCase.Subject;
        
        //set Reason for Requesting Investigation
        string reason = subject.substringBefore('- P/N');
        if(String.isNotBlank(reason)){
            reason = reason.trim();
            subject = subject.remove(reason);
            theCase.Reason_for_Requesting_Investigation__c = (reason == 'Automation Identified LTOW') ? 'Low Time On Wing' 
                : (reason == 'Automation Identified ROGUE') ? 'Rogue' 
                    : (reason == 'Customer Request Identified LTOW') ? 'Low Time On Wing;Customer Request' 
                        : (reason == 'Investigation Item Detected') ? 'Investigation' 
                            : '';
        }
        
        //get Part Number
        string partNumber = subject.substringBetween('- P/N ', ', S/N');
        partNumber = (String.isNotBlank(partNumber)) ? partNumber.trim() : null;
        
        //set Serial Number
        string serialNumber = subject.substringBetween(', S/N', 'removed');
        if(String.isNotBlank(serialNumber))
            theCase.Serial_Number__c = serialNumber.trim();
        
        return partNumber;
    }
    
    //Parse case description
    //set fields: Reason for Removal, Days on Wing, Part Removal History
    //return aircraft serial number
    private static string parseDescription(Case theCase){
        string description = theCase.Description;
        
		//check for TSN:, days on wing, SAP History:...
        boolean hasTSN = (description.indexOf('TSN:') > -1);
        boolean hasDaysOnWing = (description.indexOf('days on wing') > -1);
        boolean hasSAPHistory = (description.indexOf('SAP History:') > -1);
        
        //get aircraft serial number
        string serialNumber = description.SubstringBetween('[',']');
        if(String.isNotBlank(serialNumber)){
            List<string> splitSerialNumber = serialNumber.trim().split('/');
            serialNumber = (splitSerialNumber.size() > 1) ? splitSerialNumber[0].right(3) + splitSerialNumber[1].leftPad(5,'0') : null;
        }
        description = description.remove(description.substringBefore('RFR:'));
        
        //set Reason for Removal
        string reasonForRemoval = (hasTSN) ? description.substringBetween('RFR:', 'TSN:') 
            : (hasDaysOnWing) ? description.substringBetween('RFR:', 'days on wing').substringBeforeLast('\n') 
                : (hasSAPHistory) ? description.substringBetween('RFR:', 'SAP History:') 
                    : description.substringBetween('RFR:', '---');
        
        if(String.isNotBlank(reasonForRemoval))
            theCase.Reason_for_Removal__c = reasonForRemoval.trim();
        
 		//set Days on Wing
        if(hasDaysOnWing){
         	description = description.remove(description.substringBefore('days on wing').substringBeforeLast('\n'));
            string daysOnWing = description.substringBefore('days on wing');
            if(String.isNotBlank(daysOnWing))
            	theCase.Days_On_Wing__c = integer.valueOf(daysOnWing.trim());
        }
        
        //set Part Removal History
        if(hasSAPHistory){
        	description = description.remove(description.substringBefore('SAP History:'));
            string SAPHistory = description.substringBefore('---');
            if(String.isNotBlank(SAPHistory))
            	theCase.Part_Removal_History__c = SAPHistory.trim();
        }
        
        return serialNumber;
    }
}