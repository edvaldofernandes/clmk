public interface Visitator {
    
   void visit(List<AccountInformation> accountInformationList, List<AccountHistoryInformation> accountHistoryInformationList, String className);
   void visit(List<AircraftInformation> aircraftInformationList, List<AircraftHistoryInformation> aircraftHistoryList, String className);  
}