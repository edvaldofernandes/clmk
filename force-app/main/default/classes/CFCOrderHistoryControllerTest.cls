@isTest
public class CFCOrderHistoryControllerTest {
    
    @testSetup
    private static void dataSetup()
    {
         User user = CFCTestSetup.getUser();
    
    }
    
    public static testMethod void TestOrderHistoryNew()
    {
       
        //User user = [Select Id,UserRoleId From User Where ProfileId = '00ei00000016vZqAAI' And isActive  = true Limit 1]; //System Administrator 
        User user = [Select Id,UserRoleId 
                     FROM User 
                     WHERE isActive  = TRUE
                     AND (Profile.Name = 'System Administrator' OR Profile.Name = 'Administrador do Sistema') LIMIT 1]; //System Administrator 
        
        System.runAs(user){
            
            Datetime currentDate = Datetime.now();
                    
            Id recordTypeProductId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
            Id recordTypeOperatorId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId(); 
            Id recordTypeTrainingId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId(); 
            
            Account acc = new Account();
            acc.Name='teste acc';
            acc.RecordTypeId = recordTypeOperatorId;
            acc.Company_Nickname__c='Tam998';
            insert acc;
            
            MFIR__c mfir = new MFIR__c();
            mfir.Name = 'Teste mfir';
            mfir.MFIR_number__c = '1234567890';
            mfir.Account__c = acc.Id;
            Database.insert(mfir);
            
            Id standardPBId = Test.getStandardPricebookId();
            system.debug('standardPB: ' + standardPBId);
            
            Product2 prod2 = new Product2();
            prod2.Name = 'Teste Produto';
            prod2.Applicability__c = 'ERJ';
            prod2.RecordTypeId = recordTypeProductId;
            insert prod2;
            
            PricebookEntry priceEntry = new PricebookEntry();
            priceEntry.Pricebook2Id = standardPBId;        
            priceEntry.Product2Id = prod2.Id;
            priceEntry.UnitPrice = 9999;
            priceEntry.IsActive = true;
            priceEntry.UseStandardPrice = false;
            insert priceEntry;
                        
            Datetime orderDateTime = Datetime.newInstance(2016, 1, 17);

            ServiceContract opp = new ServiceContract();
            opp.Name = 'Teste';
            opp.RecordTypeId = recordTypeTrainingId;
            opp.AccountId = acc.Id;
            opp.Pricebook2Id = standardPBId;
            //opp.Signature_Date__c = Date.today();
            Database.insert(opp);
            system.debug('opp: ' + opp);
            
            ContractLineItem oli1 = new ContractLineItem();
            oli1.ServiceContractId = opp.id;
            oli1.PricebookEntryId = priceEntry.id;
            oli1.Quantity = 1;
            oli1.Sales_Price__c = 50;
            oli1.UnitPrice = 50;
            insert oli1;
                       
            CFCOrderHistoryController order = new CFCOrderHistoryController();
            
            order.controleMap = true;
            order.lastDays = 1;
            
            order.lstServiceContract.add(opp);
            order.MethodOne();
            order.getOptionsOrderBy();
            order.selectedValueOrderBy = String.valueOf(currentDate.year());
            order.MethodOne();
            order.selectedValueOrderBy = String.valueOf(currentDate.addYears(-1).year());
            order.MethodOne();
            order.selectedValueOrderBy = String.valueOf(currentDate.addYears(-2).year());
            order.MethodOne();
            system.assert(order != null); 
            
        }
    }
    
    public static testMethod void myUnitTest()
    {
       
            Id recordTypeProductId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Training').getRecordTypeId(); 
            Id recordTypeOperatorId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId(); 
            
            
            User user = [Select Id,AccountId From User  WHERE isActive  = TRUE and ProfileId in (SELECT Id FROM Profile WHERE Name='Services Catalog' ) Limit 1];
            
            
           
            
            
          
           
            Pardot_Setings__c cs = new Pardot_Setings__c();
            cs.Name = 'Pardot General';
            cs.Pardot_Status__c = 'Active';
            insert cs;   
            
             Id standardPBId = Test.getStandardPricebookId();
            system.debug('standardPB: ' + standardPBId);
            
            Product2 prod2 = new Product2();
            prod2.Name = 'Teste Produto';
            prod2.Applicability__c = 'ERJ';
            prod2.RecordTypeId = recordTypeProductId;
            insert prod2;
            
            PricebookEntry priceEntry = new PricebookEntry();
            priceEntry.Pricebook2Id = standardPBId;        
            priceEntry.Product2Id = prod2.Id;
            priceEntry.UnitPrice = 9999;
            priceEntry.IsActive = true;
            priceEntry.UseStandardPrice = false;
            insert priceEntry;
   
            MFIR__c mfir = new MFIR__c();
            mfir.Name = 'Teste mfir';
            mfir.MFIR_number__c = '1134567890';
            mfir.Account__c = user.accountId;
            Database.insert(mfir);
         
            
            
            Datetime currentDate = Datetime.now();
                    
            Id recordTypeTrainingId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId(); 
        
           
            Datetime orderDateTime = Datetime.newInstance(2017, 1, 17);

            ServiceContract opp = new ServiceContract();
            opp.Name = 'Teste';
            opp.RecordTypeId = recordTypeTrainingId;
            opp.AccountId = user.accountId;
            opp.Pricebook2Id = Test.getStandardPricebookId();
            opp.Signature_Date__c = Date.today();
            opp.Contract_Status__c = 'Signed';
            opp.MFIR__C = mfir.id;
            Database.insert(opp);
            system.debug('opp: ' + opp);
            
            ContractLineItem oli1 = new ContractLineItem();
            oli1.ServiceContractId = opp.id;
            oli1.PricebookEntryId = [Select Id From PricebookEntry Limit 1].id;
            oli1.Quantity = 1;
            oli1.Sales_Price__c = 50;
            oli1.UnitPrice = 50;
            insert oli1;
           
            
             System.runAs(user)
            {           
                CFCOrderHistoryController order = new CFCOrderHistoryController();
            
                order.controleMap = true;
                order.lastDays = 1;
                
                order.lstServiceContract.add(opp);
                order.MethodOne();
                order.getOptionsOrderBy();
                order.selectedValueOrderBy = String.valueOf(currentDate.year());
                order.MethodOne();
                order.selectedValueOrderBy = String.valueOf(currentDate.addYears(-1).year());
                order.MethodOne();
                order.selectedValueOrderBy = String.valueOf(currentDate.addYears(-2).year());
                order.MethodOne();
                system.assert(order != null); 
                
            }                
        
        
    }
}