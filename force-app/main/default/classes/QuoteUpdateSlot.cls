/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* When a quote is deleted or updated to Denied, this class
* updates the available quantity of the related slot.
*
* NAME: QuoteUpdateSlot.cls
* AUTHOR: DPF                                                DATE: 17/12/2014
*
*******************************************************************************/
public with sharing class QuoteUpdateSlot {

  public static void delOpp()
  {
    TriggerUtils.assertTrigger();

    // if tem slot devolve o saldo
    list< QuoteLineItem > lLstOppItens = [ select Id, Slot__c, Product_Type__c, Quantity
      from QuoteLineItem where QuoteId =:( list< Quote > )trigger.old
      and Slot_status__c = 'Success' ];

    LineItemSearchSlot.deleteProducts( lLstOppItens );
  }

  public static void closeOpp()
  {
    TriggerUtils.assertTrigger();

    list< id > lListOppID = new list< id >();
    for ( Quote lQuote : ( list< Quote > )trigger.new )
    {
      if ( TriggerUtils.wasChangedTo( lQuote, Quote.Status , 'Denied' ) )
        lListOppID.add( lQuote.id );
    }

    if ( lListOppID.isEmpty() ) return;

    list< QuoteLineItem > lLstOppItens = [ select Slot__c, Product_Type__c, quantity
      from QuoteLineItem where QuoteId =:lListOppID and Slot_status__c = 'Success' ];

    LineItemSearchSlot.deleteProducts( lLstOppItens );
  }
}