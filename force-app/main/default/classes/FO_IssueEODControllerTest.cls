/**
* @author Marcos Vinicius de Oliveira Duque
* @date 28/11/2018
* @description: FO_IssueEODController test class.
**/
@isTest
public class FO_IssueEODControllerTest {
    /**
    * @description Test if current record detail page is returned on redirect() method.
    **/
    @isTest static void testIsGettingReviewEODPage(){
        EOD__c eod = new FO_EODTestDataBuilder()
            .withStatus('Approved')
            .build();
        
		ApexPages.currentPage().getParameters().put('id',eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
        
	    FO_IssueEODController controller  = new FO_IssueEODController(stdController);
        
        PageReference recordPage = controller.redirect();
        System.assertEquals(recordPage.getUrl(), '/' + eod.Id);
    }
}