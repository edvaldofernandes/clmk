public with sharing class AircraftSearchController
{

    // the soql without the order and limit---------------------------------
        private String soql {get;set;}

    // the collection of aircraft to display--------------------------------
            public List<Aircraft__c> aircrafts {get;set;}

    // the current sort direction. defaults to asc--------------------------
        public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
        }

    // the current field to sort by. defaults to last name--------------------------
        public String sortField {
            get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
            set;
        }

    // format the soql for display on the visualforce page--------------------------
        public String debugSoql {
            get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 350'; }
            set;
        }

    // init the controller and display some sample data when the page loads---------
        public AircraftSearchController() {
                    soql = 'SELECT Id,RecordType.Name,Name,Age__c,Flying_For__r.name,Operator__r.name,Owner__r.name,Aircraft_Status__c,Model_Type__c,Ascend_Lease_End_Date__c,Geografic_Region__c,Operator_Country__c,Registration__c FROM Aircraft__c where RecordType.Name = \'Embraer\' ';
                    runQuery();
   }

    // toggles the sorting of query from asc<-->desc--------------------------------
        public void toggleSort() {
            sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
            runQuery();
        }

    // runs the actual query--------------------------------------------------------
        public void runQuery() {
        try {
            aircrafts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 350');
            }
                catch (Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
            }
        }

    // runs the search with parameters passed via Javascript------------------------
        public PageReference runSearch() {

            String Model_type = Apexpages.currentPage().getParameters().get('model_type');
            String Status     = Apexpages.currentPage().getParameters().get('status');
            String Geoloc     = Apexpages.currentPage().getParameters().get('geoloc');
            String Owner      = Apexpages.currentPage().getParameters().get('owner');
            String Operator   = Apexpages.currentPage().getParameters().get('operator');
            String Flying     = Apexpages.currentPage().getParameters().get('flying');

            soql = 'SELECT Id,RecordType.Name,Name,Age__c,Flying_For__r.name,Operator__r.name,Owner__r.name,Aircraft_Status__c,Model_Type__c,Ascend_Lease_End_Date__c,Geografic_Region__c,Operator_Country__c,Registration__c FROM Aircraft__c where RecordType.Name = \'Embraer\'';
            if (!Owner.equals(''))
            soql += ' and Owner__r.name LIKE \''+String.escapeSingleQuotes(Owner)+'%\'';
            if (!Operator.equals(''))
            soql += ' and Operator__r.name LIKE \''+String.escapeSingleQuotes(Operator)+'%\'';
            if (!Flying.equals(''))
            soql += ' and Flying_For__r.name LIKE \''+String.escapeSingleQuotes(Flying)+'%\'';
            if (!Model_type.equals(''))
            soql += ' and   Model_Type__c LIKE \''+Model_type+'%\'';
            if (!Status.equals(''))
            soql += ' and   Aircraft_Status__c LIKE \''+Status+'%\'';
            if (!Geoloc.equals(''))
            soql += ' and   Geografic_Region__c LIKE \''+Geoloc+'%\'';

            // run the query again
            runQuery();

            return null;
        }


}