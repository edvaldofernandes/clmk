public without sharing class FO_CaseTriggerHandler {
    
    public Map<Id,Case> newRecordsMap = new Map<Id,Case>();
    public Map<Id,Case> oldRecordsMap = new Map<Id,Case>(); 
    public List<Case> newRecords = new List<Case>();
    public List<Case> oldRecords = new List<Case>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    List<Entitlement> ent = new List<Entitlement>();
    
    public FO_CaseTriggerHandler(boolean isExecuting){   this.isExecuting = isExecuting;   }

    public void OnBeforeInsert(){ 
        UpdateAll();
        
    }

    public void OnAfterInsert(){
        
    }

    public void OnBeforeUpdate(){  
        UpdateAll();
    }

    public void OnAfterUpdate(){
        
    }

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}

    public boolean IsTriggerContext{get{ return isExecuting;}}

	public static User getCompanyLiaison(Id AirlineAccountId){
        /*
         * Gets the Embraer Liaison responsible for the inputted Company.
         * 
         */ 
		if(AirlineAccountId==null)
			return null;
		return [
			SELECT Id
			FROM
				User
			WHERE
				Id IN (
					SELECT UserId
					FROM AccountTeamMember
					WHERE AccountId =: AirlineAccountId
						AND TeamMemberRole='Flight Ops Liaison'
				)
			LIMIT 1
		];
	}
	public static String getEmailDomain(String email){
    	/*
    	 * Gets the e-mail domain from the e-mail address.
    	 */
        
//		This is not beeing used
//		Pattern p = Pattern.compile('.+@(.*)');            
        String emailDomain = '@' + email.split('@',2)[1];
		return emailDomain;
	}
	public static Account getCompanyAccountByEmailDomain(String EmailDomain){
		/*
		 * Gets the Company Account based on the e-mail domain inputted
		 * 
		 */
		List<List<SObject>> searchResult = [
			FIND :EmailDomain IN EMAIL FIELDS
			RETURNING
				Account(
					Id
				WHERE
					FlightOps_Account_Domains__c != NULL
					AND RecordTypeId IN (
						SELECT Id
						FROM RecordType
						WHERE Name = 'Airline'
					)
					AND Company_Status__c != 'Inactive'
				)
            LIMIT 1
		];
        // Casting results to a list
        system.debug('searchResult = ' + searchResult);              
        List<Account> accounts =  searchResult[0];
        system.debug('accounts = ' + accounts);  
        if(accounts.isEmpty())
            return null;
        return accounts[0];
        
	}   
	public static List<Case> setOwner(List<Case> newCases){
        /*
         * Sets the responsible Liaisons for each cases as its owners based on
         * which company the case is.
         * 
         */
		for(Case c : newCases){
			if(c.AccountId==null && c.SuppliedEmail!=null ){
				String email = String.valueOf(c.SuppliedEmail);
                
  				system.debug('email = ' + email);              
                
				String emailDomain = FO_CaseTriggerHandler.getEmailDomain(email);
                
                system.debug('emailDomain = ' + emailDomain);              
                
				Account airlineAccount = FO_CaseTriggerHandler.getCompanyAccountByEmailDomain(emailDomain);
                system.debug([SELECT Id, Name, FlightOps_Account_Domains__c FROM Account]);
                system.debug('airlineAccount = ' + airlineAccount);
                if(airlineAccount != NULL)
					c.AccountId = airlineAccount.Id;
			}
            try{
                User caseOwner = FO_CaseTriggerHandler.getCompanyLiaison(c.AccountId);
                c.OwnerId = caseOwner.Id;
            }catch(System.DmlException e){
                System.debug('WARNING: can\'t assign a liaison to the case: ' + c.Id);
            }catch(System.QueryException e){
                System.debug('WARNING: can\'t assign a liaison to the case: ' + c.Id);
            }
		}
		return newCases;
	}
 
    
    private void UpdateAll(){
        
        //---------------------- Update Owner para liaison------------------------
        Map<Id, Id> ownerUser = new Map<Id, Id>();  
        Map<Id, Id> ownerUser2 = new Map<Id, Id>();  
        String email;
        List<String> emaill;
        List<case> casos = new List<case>();
        //List<RecordType> rtAirline = [SELECT id from RecordType where Name = 'Airline'];
        //List<User> FLLPRC = [SELECT Id FROM User WHERE Name = 'FLLPRC Default' LIMIT 1];
        //List<RecordType> rtypes = [Select Id From RecordType where sObjectType='Account' and Name='Airline' and isActive=true LIMIT 1];
    
        //for(Case record: this.newRecords) {
        Case caso = this.newRecords[0];
        
        if (caso.RecordTypeId!=null){
  
          RecordType rt = [SELECT Name FROM RecordType WHERE Id =: caso.RecordTypeId];
        
          if (rt.Name == 'FlightOps Case Record Type'){
            
              if (caso.AccountId!=null){
            
                  ownerUser.put(caso.AccountId, null);
        
              AccountTeamMember[] recorde = [SELECT AccountId,UserId FROM AccountTeamMember WHERE AccountId IN :ownerUser.keySet() AND TeamMemberRole='Flight Ops Liaison' ];
                
                if (recorde.isEmpty()==false){
                    
                      if((recorde[0].AccountId!=null) && (recorde[0].UserId!=null) ){
                      ownerUser.put(recorde[0].AccountId, recorde[0].UserId);
                      caso.OwnerId = ownerUser.get(recorde[0].AccountId);
                      }
                }
                
              }else{
                
            
              if(caso.SuppliedEmail!=null){
                    
                      email = String.valueOf(caso.SuppliedEmail);               
                      emaill = email.split('@',2);
                      emaill[1] = '@'+emaill[1];
                
                      List<RecordType> rtAirline = [SELECT id from RecordType where Name = 'Airline'];
                        
                      if(rtAirline!=null){
                                                    
                        Account[] ac = [SELECT Id,FlightOps_Account_Domains__c FROM Account WHERE FlightOps_Account_Domains__c!=null AND RecordTypeId =: rtAirline[0].Id AND Company_Status__c!='Inactive'];

                          if (ac.isEmpty()==false){
                
                            for (Integer i=0; i<ac.size() ;i++){
                   
                            //  if( (String.isEmpty(ac[i].FlightOps_Account_Domains__c))!=true ) { 
                        
                                  if( (ac[i].FlightOps_Account_Domains__c.contains(email)) || (ac[i].FlightOps_Account_Domains__c.contains(emaill[1])) ) {      
                        
                                      caso.AccountId = ac[i].Id;
                                            
                                        ownerUser.clear();
                                        
                                        AccountTeamMember[] rec = [SELECT AccountId,UserId FROM AccountTeamMember WHERE AccountId =: ac[i].Id AND TeamMemberRole='Flight Ops Liaison' ];
                                        
                                        if (rec.isEmpty()==false){
                                            
                                            if ((rec[0].AccountId!=null)&&(rec[0].UserId!=null)){
                                            ownerUser.put(rec[0].AccountId, rec[0].UserId);
                        caso.OwnerId = ownerUser.get(rec[0].AccountId);
                                              //record.AccountId = rec.AccountId;
                                              break;
                                            }
                                        }
                                    }
                              //  }                  
                              }
                          }
                      }
                }
              }
            
            
              //}
              //-----------------------fim update owner liaison---------------------------------------
              
              //------------------------------------ update fleet-----------------------------------------
              if ((caso.AccountId!=null)&&(caso.Status=='New')){
                
                  List<RecordType> rtypes = [Select Id From RecordType where sObjectType='Account' and Name='Airline' LIMIT 1];
                
                  if(rtypes!=null){
                    
                      //Account Record Type Airline Id : 012i0000001ITuF
                      List<Account> ac = [SELECT Id,Embraer_Fleet_Type__c FROM Account WHERE Id =: caso.AccountId AND RecordTypeId =: rtypes[0].Id LIMIT 1];
                      if(ac!=null){
                      
                            for (Account a : ac){
                        
                            if ( (a.Id!=null)&&(a.Embraer_Fleet_Type__c!=null) ){

                                if (a.Embraer_Fleet_Type__c.contains('EJet')){           
                                    caso.E170__c = true;
                                }else{
                                    caso.E170__c = false;
                                }
                                if (a.Embraer_Fleet_Type__c.contains('Turboprop')){
                                    caso.Turboprops__c = true;        
                                }else{
                                    caso.Turboprops__c = false;
                                }
                                if (a.Embraer_Fleet_Type__c.contains('ERJ')){
                                    caso.ERJ__c = true;   
                                }else{
                                    caso.ERJ__c = false; 
                                }
                                    if (a.Embraer_Fleet_Type__c.contains('E2')){
                                    caso.E2__c = true;   
                                }else{
                                    caso.E2__c = false; 
                                }
                            } 
                        }
                       }
                  }
               }
              //----------------------------------fim  update fleet-----------------------------------------
            
              //---------------------- update owner FLLPRC Default Id : 005i0000006F6Nz--------------
              List<User> FLLPRC = [SELECT Id FROM User WHERE Name = 'FLLPRC Default' LIMIT 1];
              if (FLLPRC!=null){
                  if  (caso.OwnerId==FLLPRC[0].Id) {
                
                      Group que = [SELECT Id FROM Group WHERE Name = 'FlightOps Case Queue' and Type = 'Queue'];
                      if(que!=null){
                    
                          ownerUser2.put(caso.OwnerId , que.Id);
                          caso.OwnerId = ownerUser2.get(caso.OwnerId);
                       
                      }       
                  }
              }
              //---------------------- fim update owner FLLPRC Default Id : 005i0000006F6Nz--------------
            
              //---------------------------------- image email update---------------------------------
              caso.FlightOps_Email_Unread__c = 'False';
              List<EmailMessage> em = [SELECT Status FROM EmailMessage WHERE Status='0' AND ParentId =: caso.Id LIMIT 10];
         
              if ((em!=null)&&(em.size()>0)){  
                
                  caso.FlightOps_Email_Unread__c = 'True';     

              }
              //--------------------------------fim  image email update---------------------------------
            
                //habilitar depois -> para a EOD
              //caso.EOD_Content__c = '<br/><br/>This document does not constitute an operational approval. If Local Regulatory Authority approval is required, it should be obtained by the Operator.<br/><br/>The information contained in this document is based solely on the data explicitly reported by the Operator to Embraer by (document / e-mail) dated as of month dd, YYYY. Therefore, Embraer disclaims any and all responsibility for incorrect, inaccurate or incomplete information provided by the Operator related to this matter.<br/><br/><br/>In case of any conflict between this EOD and any mandatory requirements issued by the Local Regulatory Authority, including but not limited to Airworthiness Directives, the Local Regulatory Authority document/orientation shall prevail.';
              
            }//if (rt.Name == 'FlightOps Case Record Type')
            
        }//if (caso.RecordTypeId!=null)
        
    }//UpdateAll()
    
    
    
    
}//FO_CaseTriggerHandler