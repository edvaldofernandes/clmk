@isTest
public class FO_CompanyChangeControllerTest {
   
    static testMethod void Testando() {
             
	  test.startTest();
        
      Case caso = new Case(Status = 'New');      
      list<Case> lstcaso = new list<Case>();
      lstcaso.add(caso);
      database.insert(lstcaso);   
        
      Account ac = new Account(Name='Embraer Airlines',Company_Nickname__c='EMB');
      Database.insert(ac); 
      
       PageReference pageRef = Page.FO_UpdateCompanyNamePage;
       pageRef.getParameters().put('recs',String.valueOf(caso.id));
       Test.setCurrentPageReference(pageRef);
       ApexPages.StandardController sc = new ApexPages.StandardController(caso); 
       FO_CompanyChangeController controller = new FO_CompanyChangeController(sc);    
       controller.caseX = new Case(AccountId=ac.Id);
       controller.msg = 'teste';
       controller.save();
        
          
      test.stopTest();
           
    }    
    
     
}