@isTest
public class FinancialDisputeTriggerTest {
    private static Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();

    @testSetup
    static void setup(){     
        
        //Create an Account
        Account testAccount 			= new Account();
        testAccount.Name				='Test Account record';
        testAccount.Company_Nickname__c ='TesterComp';
        insert testAccount;
        
        MFIR__c mfirTest = new MFIR__c();
        mfirTest.Name = '123';
        mfirTest.MFIR_number__c = '129381294';
        mfirTest.Account__c = testAccount.Id;
        insert mfirTest;
        
        Contact contact = new Contact();
        contact.FirstName = 'John';
        contact.LastName = 'Doe';
        insert contact;
        
        Financial_Dispute__c dispute= new Financial_Dispute__c();
        dispute.Subject__c = 'Teste';
        dispute.Comments__c = 'Test Comment';
        dispute.Status__c = 'Closed';
        dispute.Invoice_Number__c = '12398';
        dispute.Invoice_Reference_Number__c = 'Test Comment';
        dispute.Invoice_Issuing_Date__c = Date.today();
        dispute.Invoice_Due_Date__c = Date.today();
        dispute.Ammount__c = 112830878;
        dispute.Case_Owner__c = contact.Id;
        dispute.MFIR__c = mfirTest.Id;
        insert dispute;
    }
    
    @isTest
    public static void testInicialValue(){
        
        Financial_Dispute__c dispute= new Financial_Dispute__c();
        dispute = [select Comments__c, Comments_History__c,Subject__c from Financial_Dispute__c where Subject__c = 'Teste'];  
        Test.startTest();
        Test.stopTest();
        System.assertNotEquals(null, dispute.Comments_History__c);
        
    }
    
    @isTest
    public static void testUpdateValue(){
        
        Financial_Dispute__c dispute= new Financial_Dispute__c();
        dispute = [select Comments__c, Comments_History__c,Subject__c from Financial_Dispute__c where Subject__c = 'Teste'];  
        Test.startTest();
        
        dispute.Comments__c ='New Update';
        update dispute;
        Test.stopTest();
        System.assertEquals(false, dispute.Comments_History__c.contains('New Update'));
        
    }
}