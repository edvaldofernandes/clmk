@isTest
public class UpdateCodOperatorInRcpMtburTest {
    
    @isTest static void UpdateCodOperatorTest(){
        
        Try{
            
            Test.startTest();
        
        	EventConfiguration__c eventConfiguration = new EventConfiguration__c();
        	eventConfiguration.Name = 'ETRACK_GET_QUESTIONS_TEST';
        	eventConfiguration.endPointUrl__c = 'http://ogwtest.com.br';
                
        	insert eventConfiguration;
        
        	Account account = new Account();
        	account.Name = 'Update name test';
        	account.Company_Nickname__c = 'Update Nick';
        	account.COD_ORGz__c = 123456;
        	INSERT account;
        
        	Map<String,String> accountIdMap = RcpRepository.buildMapWithAllAccount();
        
        	RCP_MTBUR__c rcpMTBUR = new RCP_MTBUR__c();
        	rcpMTBUR.AtaChapter__c            = 10.2;
        	rcpMTBUR.Operador__c              = 123456;
        	rcpMTBUR.family__c                = '190';
        	rcpMTBUR.partNumber__c            = '123456789';
        	rcpMTBUR.GroupDescription__c 	  = 'test test test';
        	rcpMTBUR.PartNumberDescription__c = 'pn test test';
        	rcpMTBUR.RemovalsL12M__c          = 10.2;
        	rcpMTBUR.createdDate__c           = Date.today();
        	rcpMTBUR.Account__c = accountIdMap.get(String.valueOf(rcpMTBUR.Operador__c));
        
        	INSERT rcpMTBUR;
        	
            RCP_MTBUR__c rcpMtburAfeter  = [SELECT id,Operador__c, account__c FROM RCP_MTBUR__c];
            
        	System.assertEquals(account.Id, rcpMtburAfeter.account__c);
        
        	Test.stopTest();
        }catch(Exception e){}    
    }

}