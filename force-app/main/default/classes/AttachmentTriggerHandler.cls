public without sharing class AttachmentTriggerHandler {
    
    public Map<Id,Attachment> newRecordsMap = new Map<Id,Attachment>();
    public Map<Id,Attachment> oldRecordsMap = new Map<Id,Attachment>(); 
    public List<Attachment> newRecords = new List<Attachment>();
    public List<Attachment> oldRecords = new List<Attachment>();
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    public AttachmentTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }
    
    public boolean IsTriggerContext
    {
        get{return isExecuting;}
    }
    
    public void OnBeforeInsert(){
        //CheckLenghtAttachmentBillboard();
    }

    public void OnAfterInsert(){
        CheckLenghtAttachmentBillboard();
        CopyAttachmentLink();
    }

    public void OnBeforeUpdate(){}

    public void OnAfterUpdate(){}

    public void OnBeforeDelete(){}

    public void OnAfterDelete(){}

    public void OnUndelete(){}

    private void CheckLenghtAttachmentBillboard(){
		system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard()');
        
        //list attachment
        List<Attachment> lstAttachment = new List<Attachment>();

        //creating a map of ids of attachments__c and lenght of attachment
        Map<String, Integer> mapAttachment = new Map<String, Integer>();
        
        //creating a set of attachment id
        Set<String> setAttachmentId = new Set<String>();
        
        //populate the set
        for(Attachment att : (List<Attachment>) trigger.new){            
            mapAttachment.put(att.ParentId, att.BodyLength); 
            setAttachmentId.add(att.ParentId); 
            lstAttachment.add(att);
        }
        system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard.mapAttachment >>> ' + mapAttachment);
        system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard.setAttachmentId >>> ' + setAttachmentId);
        
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Billboards');
        
        List<Attachments__c> lstAttachmentsC = AttachmentCustomDao.getInstance().listBySetIdAndRecordType(setAttachmentId, rType.Id); 
        system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard.lstAttachmentsC >>> ' + lstAttachmentsC);
        
        if(!lstAttachmentsC.isEmpty()){    
            
            //custom settings
            CFC_Configurations__c settings = CFC_Configurations__c.getOrgDefaults();
            Integer desktopLimit = Integer.valueOf(String.valueOf(settings.Desktop_Limit_KB__c));
            Integer mobileLimit = Integer.valueOf(String.valueOf(settings.Mobile_Limits_KB__c));
            Integer tabletLimit = Integer.valueOf(String.valueOf(settings.Tablet_Limits_KB__c));
            system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard.settings >>> ' + settings);              
            
            for(Attachments__c item : lstAttachmentsC){
                Integer fileSize = mapAttachment.get(item.Id) / 1024;
                
                system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard.fileSize >>> ' + fileSize);
                system.debug('AttachmentTriggerHandler.CheckLenghtAttachmentBillboard.Device_Type__c >>> ' + item.Device_Type__c);
                
                if(item.Device_Type__c == 'Desktop'){
                    if(fileSize > desktopLimit){
                        lstAttachment.get(0).addError('Please insert an image with up to '+desktopLimit+' KB');
                    }                
                } else if(item.Device_Type__c == 'Mobile'){
                    if(fileSize > mobileLimit){
                        lstAttachment.get(0).addError('Please insert an image with up to '+mobileLimit+' KB');
                    }
                } else if(item.Device_Type__c == 'Tablet'){
                    if(fileSize > tabletLimit){
                        lstAttachment.get(0).addError('Please insert an image with up to '+tabletLimit+' KB');
                    }
                }
            }
        }
    }
    
    private void CopyAttachmentLink(){
        system.debug('CopyAttachmentLink() METHOD');
        
        //list attachment
        List<Attachment> lstAttachment = new List<Attachment>();
        List<Attachments__c> lstCatalogAttachment = new List<Attachments__c>();

        //creating a map of ids of attachments__c and Attachment Lnk
        Map<String, String> mapAttachment = new Map<String, String>();
        
        //creating a set of attachment id
        Set<String> setAttachmentId = new Set<String>();
        
        String AttachId;
        
        //populate the set
        for(Attachment att : (List<Attachment>) trigger.new){            
            mapAttachment.put(att.ParentId, att.id); 
            setAttachmentId.add(att.ParentId); 
            AttachId = att.id;
            lstAttachment.add(att);
        }
        system.debug('AttachmentTriggerHandler.CopyAttachmentLink.mapAttachment >>> ' + mapAttachment);
        system.debug('AttachmentTriggerHandler.CopyAttachmentLink.setAttachmentId >>> ' + setAttachmentId);
        
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Documents_to_Media_Center');
        
        List<Attachments__c> lstAttachmentsC = AttachmentCustomDao.getInstance().listBySetIdAndRecordType(setAttachmentId, rType.Id); 
        system.debug('AttachmentTriggerHandler.CopyAttachmentLink.lstAttachmentsC >>> ' + lstAttachmentsC);
            
        
        
        List<Attachments__c> lstAttachmentUpdt = new List<Attachments__c>();     
        
        if(!lstAttachmentsC.isEmpty()){  
            for(Attachments__c lstAttachmentCatalog : lstAttachmentsC){
                
               lstAttachmentCatalog.Attachment_Id__c = AttachId;                
                                              
               lstAttachmentUpdt.add(lstAttachmentCatalog);
            }             
            update lstAttachmentUpdt;
        }
        
    }
    
    
}