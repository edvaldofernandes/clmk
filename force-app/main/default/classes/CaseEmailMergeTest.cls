@isTest
private class CaseEmailMergeTest{
  @isTest static void testExtractAddress()
{

 system.debug('testExtractAddress ');   
 String expected = 'b.g@g.com';
 String target = 'Bryan <'+expected+'>';
 CaseEmailMerge prutil = new CaseEmailMerge();
 String results = prutil.extractAddress(target);
 System.assertEquals(expected,results);  
target = 'Helen W';
expected = 'Helen W'; //@on.com;

 results = prutil.extractAddress(target);
 System.assertEquals(expected,results);  



} 
@isTest static void testExtractRef()
{
 Test.startTest();
 String emailSubject = 'Subject Case: 300598987 asdas asdasd ';
 CaseEmailMerge prutil = new CaseEmailMerge();
 String notification = prutil.extractRef(emailSubject);
 
 system.assertEquals('300598987',notification);
 
 emailSubject = 'Subject cAse=300598987 asdas asdasd ';
 notification = prutil.extractRef(emailSubject);
 system.assertEquals('300598987',notification);

 
 Case aCase = new Case();
 insert aCase;
 Case bCase = [Select Id, CaseNumber, notification__c from Case where Id = :aCase.Id limit 1];
 
 String caseId = bCase.Id;
 String left = caseId.substring(0, 4);
 String right = caseId.substring(caseId.length()-8, caseId.length());
 right = right.substring(0,5);
system.debug(Logginglevel.ERROR,'testExtractRef case id: "' + caseId + '"');    
system.debug(Logginglevel.ERROR,'testExtractRef case n: "' + bCase.notification__c + '"');    
   
 emailSubject = 'For example in the ref [ ref:00D7JFzw.5007H3Rh8:ref ] ';
 emailSubject = 'For example in the ref [ ref:00D7JFzw.'+left+right+':ref ] ';
system.debug(Logginglevel.ERROR,'testExtractRef email subject: "' + emailSubject + '"');    
 
 notification = prutil.extractRef(emailSubject);
 system.assertEquals(bCase.notification__c,notification);
 Test.stopTest();
 
 //TRIM(" [ ref:" + LEFT( $Organization.Id, 4) + RIGHT($Organization.Id, 4) +"."+ LEFT( Id, 4)
 //  + SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(Id, RIGHT( Id, 4), ""), LEFT( Id, 4), ""), "0", "") 
 //  + RIGHT( Id, 4) + ":ref ] ")
 
// LEFT( Id, 4) 
//SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(Id, R4, ""), L4, ""), "0", "") + R4  
 
}

 
@isTest static void testExtractMain()
{
 CaseEmailMerge prutil = new CaseEmailMerge();
 String emailSubject = 'Some Subject Case';
 String fwSubject = 'FW: ' + emailSubject;
 
 String mainSubject;
 
 mainSubject = prutil.extractMainSubject(fwSubject);
system.debug(Logginglevel.ERROR,'testExtractMain "' + mainSubject + '"'); 
 system.assertEquals(emailSubject,mainSubject);

 String reSubject = 'Re: ' + emailSubject;
 mainSubject = prutil.extractMainSubject(reSubject);
system.debug(Logginglevel.ERROR,'testExtractMain "' + mainSubject + '"'); 
 system.assertEquals(emailSubject,mainSubject);

}

@isTest static  void testHandleError()
{
 CaseEmailMerge emailProcess = new CaseEmailMerge();
 // create a new email 
 Messaging.InboundEmail email;
 email = new Messaging.InboundEmail() ;
 email.subject = 'Test subject';
 email.plainTextBody = 'Test email';
 email.fromname = 'FirstName LastName';
 email.toAddresses = new String[] {'someaddress@email.com'};
 ///emailProcess.inboundEmail = email;
 
 emailProcess.handleError(null,'unit testing handle error'); 
}

@isTest static  void testProcessInboundEmailGoodWithAttachment() { 
  Test.startTest();
  // create a new email 
  Messaging.InboundEmail email = new Messaging.InboundEmail() ;
  email.subject = 'Subject asdas asdasd';
  email.plainTextBody = 'Test email';
  email.fromname = 'FirstName LastName';
  email.toAddresses = new String[] {'someaddress@email.com'};
  email.ccAddresses = new String[] {'one@email.com','two@email.com'};

  String csv = 'this is just a test';
  
  // add an attachment
  Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
  attachment.body = blob.valueOf(csv);
  attachment.fileName = 'data.csv';
  attachment.mimeTypeSubType = 'text/plain';
  
  email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
  
  Messaging.InboundEmail.TextAttachment tattachment = new Messaging.InboundEmail.TextAttachment();
  tattachment.body = csv;
  tattachment.fileName = 'data.csv';
  tattachment.mimeTypeSubType = 'text/plain';
  
  email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { tattachment };
  
  // call the email service class and test it with the data in the testMethod
  CaseEmailMerge emailProcess = new CaseEmailMerge();
  Messaging.InboundEmailResult result = emailProcess.processInboundEmail(email);
  System.assert(result.success == true);
  Test.stopTest();
 } 
 
/* @isTest static  void testProcessInboundEmailWithMatchingSubject() {
 
  Test.startTest();
  CaseEmailMerge emailProcess = new CaseEmailMerge();
  Messaging.InboundEmail email;
  Messaging.InboundEmailResult result;
  Case[] matchingCases;
  EmailMessage[] matchingEmailMessages;
  Case theCase;
  
  String mainSubject = 'Subject 350005987 asdas asdasd';

// Test Part 1
  // create a new email 
  email = new Messaging.InboundEmail() ;
  email.subject = mainSubject;
  email.plainTextBody = 'Test email';
  email.fromname = 'FirstName LastName';
  email.toAddresses = new String[] {'someaddress@email.com'};

  // call the email service class and test it with the data in the testMethod
  result = emailProcess.processInboundEmail(email);
  System.assert(result.success == true);
  matchingCases = [Select Id, CaseNumber,notification__c, Subject, Description from Case where Subject = :mainSubject];
  System.assertEquals(1,matchingCases.size());
  theCase = matchingCases[0];
  matchingEmailMessages = [Select Id from EmailMessage where ParentId = :theCase.Id];
  System.assertEquals(1,matchingEmailMessages.size());
  
// TEST Part 2
  // create a new email 
  email = new Messaging.InboundEmail() ;
  email.subject = mainSubject;
  email.plainTextBody = 'Test email';
  email.fromname = 'FirstName LastName';
  email.toAddresses = new String[] {'someaddress@email.com'};

  // call the email service class and test it with the data in the testMethod
  result = emailProcess.processInboundEmail(email);
  System.assert(result.success == true);
  matchingCases = [Select Id, CaseNumber,notification__c, Subject, Description from Case where Subject = :mainSubject];
  // still only one Case because the system will append the second email to the case Case
  System.assertEquals(1,matchingCases.size());
  System.assertEquals(theCase.Id,matchingCases[0].Id);
  // Should be two emails on the case
  matchingEmailMessages = [Select Id from EmailMessage where ParentId = :theCase.Id];
  System.assertEquals(2,matchingEmailMessages.size());
  
// TEST Part 3
  // create a new email 
  email = new Messaging.InboundEmail() ;
  email.subject = 'Different subject line but include Case: ' + theCase.Notification__c;
  email.plainTextBody = 'Test email';
  email.fromname = 'FirstName LastName';
  email.toAddresses = new String[] {'someaddress@email.com'};

  // call the email service class and test it with the data in the testMethod
  result = emailProcess.processInboundEmail(email);
  System.assert(result.success == true);
  matchingCases = [Select Id, CaseNumber,notification__c, Subject, Description from Case where Subject = :email.subject];
  // Should be no cases created with that subject 
  System.assertEquals(0,matchingCases.size());
  // Should be three emails on the case
  matchingEmailMessages = [Select Id from EmailMessage where ParentId = :theCase.Id];
  System.assertEquals(3,matchingEmailMessages.size());  
  Test.stopTest();
 } */
 
 /*@isTest static void testProcessInboundEmailWithContacts() {
 
  Test.startTest();
  CaseEmailMerge emailProcess = new CaseEmailMerge();
  Messaging.InboundEmail email;
  Messaging.InboundEmailResult result;
  Case[] matchingCases;
  EmailMessage[] matchingEmailMessages;
  Case theCase;
  Contact theContact = new Contact();
  theContact.FirstName = 'FirstName';
  theContact.LastName = 'LastName';
  theContact.Email = 'someaddress@email.com';
  
  String mainSubject = 'Subject 350005988 asdas asdasd';

// Test Part 1
  // create a new email 
  email = new Messaging.InboundEmail() ;
  email.subject = mainSubject;
  email.plainTextBody = 'Test email';
  email.fromname = 'FirstName LastName';
  email.toAddresses = new String[] {theContact.Email};

  // call the email service class and test it with the data in the testMethod
  result = emailProcess.processInboundEmail(email);
  System.assert(result.success == true);
  matchingCases = [Select Id, CaseNumber,notification__c, Subject, Description from Case where Subject = :mainSubject];
  System.assertEquals(1,matchingCases.size());
  theCase = matchingCases[0];
  matchingEmailMessages = [Select Id from EmailMessage where ParentId = :theCase.Id];
  System.assertEquals(1,matchingEmailMessages.size());
 }  */
}