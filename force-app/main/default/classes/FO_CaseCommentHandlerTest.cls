@isTest
public class FO_CaseCommentHandlerTest {

    static testMethod void testAll(){
        
        Test.startTest();
                      
        //Embraer Airlines Id : 0015500000JfG63
        //FlightOps Record Type Id : 01255000000Csmr
        RecordType rt2 = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'FlightOps Case Record Type']; 
        Case caseToCreate = new Case(RecordTypeId=rt2.Id, Description='Eitchaaa',Status='New',
                                     Subject='Case Test',SuppliedEmail='abc@gmail.com');
        Database.insert(caseToCreate);

        CaseComment cCToCreate = new CaseComment(CommentBody='blablabla',ParentId=caseToCreate.Id);
        Database.insert(cCToCreate);
        
        Database.update(cCToCreate);
        Database.delete(cCToCreate);
        Database.undelete(cCToCreate);
                     
        Test.stopTest();        
        
    } 
}