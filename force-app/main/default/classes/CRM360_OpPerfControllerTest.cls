@isTest
public class CRM360_OpPerfControllerTest {
    
    private static String accountName = 'Test Account';
    private static String opp1Name = 'Opp1';
    private static String opp2Name = 'Opp2';
    
    @testSetup
    static void setup(){

        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);

        insert accountsToInsert;
        
        List<CRM360_FP_Data__c> FPdataToInsert = new List<CRM360_FP_Data__c>();
        
        CRM360_FP_Data__c FPdata1 = new CRM360_FP_Data__c();
        FPdata1.Account__c = account.Id;
        FPdata1.FAMILY__c = 'ERJ-145 FAMILY';
        FPdataToInsert.add(FPdata1);
        
        insert FPdataToInsert;
        
        
        List<CRM360_Interruption_Data__c> InterruptiondataToInsert = new List<CRM360_Interruption_Data__c>();
        
        CRM360_Interruption_Data__c int1 = new CRM360_Interruption_Data__c();
        int1.Account__c = account.Id;
        int1.FAMILY__c = 'ERJ-145 FAMILY';
        InterruptiondataToInsert.add(int1);
        
        insert InterruptiondataToInsert;
    }

    
    @isTest
    public static void testCaseAll(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        //List<Case> Cases = [SELECT Id FROM Case WHERE Subject = :case1subject];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_OperationalPerformancePage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));

        Test.setCurrentPage(pageRef);
        
        CRM360_OperationalPerformanceController testAccPlan = new CRM360_OperationalPerformanceController();
            
        testAccPlan.loadFPData();
        testAccPlan.loadInterruptionData();
        testAccPlan.getFPData();
        testAccPlan.getInterruptions();
        testAccPlan.getAccount();
        testAccPlan.getFamilies();
        testAccPlan.getTopIssueDate();
        
        
        Test.stopTest();
    }
}