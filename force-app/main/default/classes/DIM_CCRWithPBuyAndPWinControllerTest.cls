// Used by DIM - Market Intelligence.
@isTest(SeeAllData=true)
public class DIM_CCRWithPBuyAndPWinControllerTest {

//----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod DIM_CCRWithPBuyAndPWinController newCCRController() {
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Customer_Contact_Report__c ccr = new Customer_Contact_Report__c();
        ccr.Account_Name__c = account.Id;
        ccr.Meeting_Date_Time__c = Datetime.now();
        insert ccr;
        
        PageReference pageRef = Page.DIM_CCR_Step1;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', ccr.Id);
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(ccr);
        DIM_CCRWithPBuyAndPWinController controller = new DIM_CCRWithPBuyAndPWinController(standardController);
        
        controller.contactEmail = [SELECT Email FROM Contact LIMIT 1].Email;
        controller.emailTemplate.Id = [SELECT Id FROM EmailTemplate WHERE TemplateType = 'html' LIMIT 1].Id;
        controller.emailTo = controller.contactEmail;
        
        return controller;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void checkStep1() {       
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        System.assertNotEquals(null, controller.step1());
    }
    
    static testMethod void checkStep2() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        System.assertNotEquals(null, controller.step2());
    }
    
    static testMethod void checkStep3() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        controller.hasOpportunity = true;
        System.assertNotEquals(null, controller.step3());
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkCancel() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        System.assertNotEquals(null, controller.cancel());
    }
    
    static testMethod void checkSave() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        System.assertNotEquals(null, controller.save());
    }
    
    static testMethod void checkSaveAndSend() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        System.assertEquals(null, controller.saveAndSend());
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkUpdatePBuy() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        
        controller.ccr.Account_Name__c = controller.ccr.Account_Name__r.Id;
        controller.updatePBuy();
        
        System.assertEquals(controller.ccr.Account_Name__c, controller.pBuyController.newPBuy.Account__c);
    }
    
    static testMethod void checkUpdatePWin() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = controller.ccr.Account_Name__r.Id;
        opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Aircraft_Sales').getRecordTypeId();
        opportunity.Name = 'Test Opp';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        insert opportunity;
        
        controller.ccr.Opportunity__c = opportunity.Id;
        controller.hasOpportunity = true;
        controller.updatePWin();
        
        System.assertEquals(controller.ccr.Opportunity__c, controller.pWinController.newPWin.Opportunity__c);
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkDistributionListEmailsDefiniedAsManual() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        
        Group newGroup = new Group();
        newGroup.Name = 'Test Group';
        newGroup.DeveloperName = 'Test_Group';
        insert newGroup;

        controller.emailDistributionList = 'to[a@a.com];cc[b@b.com];bcG[Test_Group]';
        controller.setAddresses();
        
        System.assertEquals(controller.emailTo, 'a@a.com');
        System.assertEquals(controller.emailCc, 'b@b.com');
        System.assertEquals(controller.emailBcc, '');
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkStringToList() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        System.assertEquals(2, controller.getContacts('a@a.com;b@b.com').size());
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateOpportunitiesList() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = controller.ccr.Account_Name__c;
        opportunity1.RecordTypeId = '012i0000001IyKG';
        opportunity1.Name = '10 Aircraft';
        opportunity1.StageName = 'Business Development';
        opportunity1.CloseDate = System.today();
        insert opportunity1;
        
        Opportunity opportunity2 = new Opportunity();
        opportunity2.AccountId = controller.ccr.Account_Name__c;
        opportunity2.RecordTypeId = '012i0000001IyKG';
        opportunity2.Name = '20 Aircraft';
        opportunity2.StageName = 'Business Development';
        opportunity2.CloseDate = System.today();
        insert opportunity2;
        
        List<SelectOption> opportunities = controller.getOpportunities();
        System.assertEquals(3, opportunities.size()); // Inclui o NONE.
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateOpportunityName() {
        DIM_CCRWithPBuyAndPWinController controller = newCCRController();
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = controller.ccr.Account_Name__r.Id;
        opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Aircraft_Sales').getRecordTypeId();
        opportunity.Name = 'Test Opp';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        insert opportunity;
        
        controller.ccr.Opportunity__c = opportunity.Id;
        controller.hasOpportunity = true;
        
        System.assertEquals(opportunity.Name, controller.getOpportunityName());
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------

}