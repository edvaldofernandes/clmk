/**
* @description: This class is the controller of the Follow-up list for myOpereng.
**/
public with sharing class myOperengFupListController {   
    @AuraEnabled
    public static List<FUP__c> getFups(String authority, String status, String family) {
        FO_FupQueryBuilder builder = new FO_FupQueryBuilder()
            .withStatus(status)
            .withAuthority(authority)
            .withFamily(family);
        return builder.get();
    }   
}