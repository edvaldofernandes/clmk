// Used by DIM - Market Intelligence.
public without sharing class DIM_CaseSplitController {

    public List<EmailMessage> emails {get;set;}
    public EmailMessage email {get;set;}
    public Case newCase {get;set;}
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public DIM_CaseSplitController(ApexPages.StandardController controller) {
        Case caso = (Case) controller.getRecord();
        emails = [SELECT Id, Subject, FromAddress, MessageDate, ParentId FROM EmailMessage WHERE ParentId =: caso.Id ORDER BY MessageDate DESC];
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public void createCase()  {
    
        String emailId = System.currentPageReference().getParameters().get('emailId');
        email = [SELECT ToAddress, CcAddress, BccAddress, FromAddress, FromName, HasAttachment, Headers, HtmlBody, Id, Incoming, MessageDate, ParentId, Status, Subject, TextBody FROM EmailMessage WHERE Id =: emailId];
        Case oldCase = [SELECT Id, CaseNumber, AccountId, Requested_By__c, Subject, SuppliedEmail, Public_Case__c, RecordTypeId FROM Case WHERE Id =: email.ParentId];
        
        // Cria no novo case.
        newCase = new Case();
        newCase.AccountId = oldCase.AccountId;
        newCase.Requested_By__c = oldCase.Requested_By__c;
        newCase.Subject = email.Subject;
        newCase.Description = email.TextBody;
        newCase.SuppliedEmail = email.FromAddress;
        newCase.RecordTypeId = oldCase.RecordTypeId;
        upsert newCase;
        
        // Atualiza as informações do case.
        newCase = [SELECT Id, CaseNumber FROM Case WHERE Id =: newCase.Id];
        
        // Copia o email e os anexos para o novo case.
        EmailMessage emailClone = email.clone(false, true, true, true);
        emailClone.ParentId = newCase.Id;
        insert emailClone;
        
        List<Attachment> attachments = [SELECT Body, BodyLength, ContentType, CreatedById, Description, Id, Name, OwnerId, ParentId FROM Attachment WHERE ParentId =: email.Id];
        for (Attachment attachment : attachments) {
            Attachment attachmentClone = attachment.clone(false, true, true, true);
            attachmentClone.ParentId = emailClone.Id;
            insert attachmentClone;
        }
        
        String taskTitle = 'Email: ' + email.Subject;
        List<Task> emailTask = [SELECT Id FROM Task WHERE WhatId =: oldCase.Id AND Subject =: taskTitle LIMIT 1];
        
        delete attachments;
        delete email;
        delete emailTask;
    
        // Gera um post indicando a criação do novo case.
        FeedItem post = new FeedItem();
        post.ParentId = email.ParentId;
        post.Body = 'Email with subject "' + email.Subject + '" moved to Case ' + newCase.CaseNumber + '.';
        insert post;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public Double offset {
        get{
            TimeZone timezone = UserInfo.getTimeZone();
            return timezone.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
        }
    }
}