public without sharing class TaskTriggerHandler 
{

    public Map<Id,Task> newRecordsMap = new Map<Id,Task>();
    public Map<Id,Task> oldRecordsMap = new Map<Id,Task>(); 
    public List<Task> newRecords = new List<Task>();
    public List<Task> oldRecords = new List<Task>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public TaskTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        updateOVApprovationRequests();

    }

 

    public void OnAfterInsert()
    {

        // EXECUTE AFTER INSERT LOGIC

    }

 

    public void OnBeforeUpdate()
    {
        
    }

 

    public void OnAfterUpdate()
    {
        System.debug('======================= on after update =========================');
        sendNotificationToOwner();
    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }

    private void updateOVApprovationRequests()
    {
        Set<Id> idTasks = new Set<Id>();
        for(Task task : this.newRecords)
        {
            if(task.subject == 'Please approve The OV for the related eSolution receivable')
                task.SendClosureNotification__c = true;
        }
        
    }

    private void sendNotificationToOwner()
    {
        System.debug('============ send notification to owner ==================');
        List<Id> listaIdsUsuarios = new List<Id>();
        List<String> listaBodys = new List<String>();
        List<String> listaAssunto = new List<String>();
        Set<Id> idTasks = new Set<Id>();
        String corpoEmail = '';
         
        for(Task task : this.newRecords)
        {
            System.debug('this.newRecords' + this.newRecords);
            if(task.IsClosed && !this.oldRecordsMap.get(task.Id).IsClosed && task.SendClosureNotification__c && (task.OwnerId != task.CreatedById))
                idTasks.add(task.Id);
                
                System.debug(idTasks);
        }
        
        if(!idTasks.IsEmpty())
        {
        System.debug('entrou no for !vazio');
                for(Task task : [SELECT CreatedById,Description,Completion_date__c,ActivityDate,Owner.Name,What.Name,Subject FROM Task Where Id in : idTasks])
                {
                    listaAssunto.add('Task ' + task.Subject + ' has been concluded.');
                    listaIdsUsuarios.add(task.CreatedById);
                    corpoEmail = '';
                    corpoEmail = 'Dear user, the task ' + task.Subject + ' has been concluded.<br><br>';
                    corpoEmail += 'Task : ' + '<a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+task.id+'">'+ task.Subject + '</a><br>';
                    corpoEmail += 'Assigned to : '+ task.Owner.Name + '<br>';
                    corpoEmail += 'Related to : ' + '<a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+task.WhatId+'">' + task.What.Name + '</a><br>';
                    corpoEmail += 'Due date : '+ task.ActivityDate.format() +'<br>';
                    corpoEmail += 'Completion date : '+ task.Completion_date__c.format() +'<br><br>';
                    if(!string.IsEmpty(task.Description))
                        corpoEmail += 'Comments : '+ task.Description +'<br>';
                    
                    //corpoEmail += 'Kind regards,';
                    listaBodys.add(corpoEmail);
                }   
                utils.enviarEmail(listaIdsUsuarios, listaBodys, listaAssunto,true);
        }
        
        
    }

}