/* Classe implementadora de SOBjectDAO para operações DML no objeto InputForm__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 14/01/2016
*/
public without sharing class InputFormDao {
    
    private static final InputFormDao instance = new InputFormDao();    
    
    private InputFormDao(){
    }    
    
    public static InputFormDao getInstance(){
        return instance;
    }
    
    public List<InputForm__c> listBySetProposal(Set<ID> setProposal){
        string setToString = '';
        for(String includeValue : setProposal){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        
        return database.query(' Select ' + utils.getAllFields('InputForm__c') + ' FROM InputForm__c WHERE id IN ('+setToString+')');
        //return [SELECT id, name, Proposal__c, Opportunity__c FROM InputForm__c WHERE Proposal__c IN :setProposal];
    }
    
    public InputForm__c createByProposal(String idProposal, String aircraft, String serialNumber, String descriptionRequest, String reasonRequest, String proposalType, String productionLine){
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Aircraft_Modification_Request');
        
        InputForm__c inputForm = new InputForm__c();
        inputForm.Proposal__c = idProposal;
        inputForm.AircraftModel__c = aircraft;
        inputForm.Serial_Number_Airplane_Information__c = serialNumber;
        inputForm.Description_of_request_by_the_customer__c = descriptionRequest;
        inputForm.Reason_for_Request_and_Expected_Benefits__c = reasonRequest;
        inputForm.Required_proposal_type__c = proposalType;
        inputForm.Required_for_production_line__c = productionLine;
        inputForm.RecordTypeId = rType.Id;
        insert inputForm;	
        
        return inputForm;
    }    
    
}