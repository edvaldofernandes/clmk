public interface IEvent {
    
    void buildInfoEvent(List<sObject> obj, String status, String error);
    void buildInfoEvent(String response, String obj);
}