/**
* @author Felipe Gouvea
* @date 11/09/2018
* @description: Test Class for TriggerHelperServiceContractMgmt   
* @comments: 
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
**/
@isTest(SeeAllData=true)
private class TriggerHelperServiceContractMgmt_Test
{

	private static Account acc;
    private static Pricebook2 catalog;
    private static Product2 product;
    private static PricebookEntry pbEntry;


    static
    {
    	catalog = SObjectInstanceTest.getPricebook2Std();
    
    	product = SObjectInstanceTest.createProduct2();
    	database.insert(product);
    
    	pbEntry = SObjectInstanceTest.createPricebookEntry(catalog.Id, product.Id);
    	database.insert(pbEntry);

    	acc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Operator'));
    	database.insert(acc);
    }


	@isTest
	static void getEligibleRecordTypesTest()
	{
		// Given
		Set<Id> eligibleIds = new Set<Id>();

		// When
		eligibleIds = TriggerHelperServiceContractMgmt.getEligibleRecordTypes();

		// Then
		//System.assertEquals(eligibleIds.size() > 0);
	}

	@isTest
	static void recalculateSCMQuantitiesTest()
	{
		// Given
		ServiceContract serviceContract = new ServiceContract();
    	serviceContract.AccountId = acc.Id;
    	serviceContract.Pricebook2Id = catalog.Id;
    	serviceContract.Name = acc.Name;
    	serviceContract.DOCCON__c = '123456';

    	serviceContract.recordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Entitlement').getRecordTypeId();
    	database.insert(serviceContract);
    
    	ContractLineItem contractLineItem = new ContractLineItem();
    	contractLineItem.ServiceContractId = serviceContract.Id;
    	contractLineItem.PricebookEntryId = pbEntry.Id;
    	contractLineItem.Quantity = 1;
    	contractLineItem.UnitPrice = 10;
    	contractLineItem.Catalogue_Price__c = 10;
    	contractLineItem.Sales_Price__c = 10;
    	database.insert(contractLineItem);

    	Service_Contract_Management__c serviceContractManagement = new Service_Contract_Management__c();
    	serviceContractManagement.Contract_Line_Item_Master__c = contractLineItem.Id;
    	insert serviceContractManagement;
		// When
		TriggerHelperServiceContractMgmt.recalculateSCMQuantities(new Set<id>{contractLineItem.Id});

		serviceContractManagement = new Service_Contract_Management__c();
		serviceContractManagement.Contract_Line_Item_Master__c = contractLineItem.Id;
		serviceContractManagement.recordTypeId = Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId();
		insert serviceContractManagement;
		
		TriggerHelperServiceContractMgmt.recalculateSCMQuantities(new Set<id>{contractLineItem.Id});
		// Then

	}
}