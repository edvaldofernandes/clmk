@IsTest 
public with sharing class PublicCalendarsHomePageButtonCtrTest
{
        
        
        static testMethod void unitTest()
        {
        
            List<PublicCalendarSettings__c> calendars = new List<PublicCalendarSettings__c>();
            calendars.add(new PublicCalendarSettings__c(name = 'Calendar 1',CalendarId__c = '023123456789102'));
            calendars.add(new PublicCalendarSettings__c(name = 'Calendar 2',CalendarId__c = '023210987654321'));
            insert calendars;
            
            
            PageReference pageRef = Page.PublicCalendarView;
            
            PublicCalendarsHomePageButtonController controller = new PublicCalendarsHomePageButtonController(); 
            controller.selectedCalendarId = '023123456789102';
            controller.buttonCaption = 'Unit Test';
            List<SelectOption> selectList = controller.getSelectedCalendarNames();
            controller.openPublicCalendarPageView();
            
            Test.setCurrentPage(pageRef);
            
        }
        
        


}