/********************************************************************************\ 
*                               Embraer - 2015                                   *
*--------------------------------------------------------------------------------*
*                                                                                *
* NAME:   LeadTriggerHandler                                                     *
* AUTHOR: Bruno de Oliveira Severino                    CREATED DATE: 25/05/2015 *
* MODIFIED BY:                                                                   *
*   Susane Antunes de Assis                                       IN: 15/04/2016 *
*   ** added four new fields to the campaign member and lead object **           *
*   Bruno de Oliveira Severino                                    IN: 11/11/2015 *
*   Bruno de Oliveira Severino                                    IN: 20/08/2015 *
*   Rodrigo Gimenes Rodrigues                                     IN: 26/06/2015 *
\********************************************************************************/
public without sharing class LeadTriggerHandler 
{
    //Variaveis
    public Map<Id,Lead> newRecordsMap = new Map<Id,Lead>();
    public Map<Id,Lead> oldRecordsMap = new Map<Id,Lead>(); 
    public List<Lead> newRecords = new List<Lead>();
    public List<Lead> oldRecords = new List<Lead>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    public List<CampaignMember> membrosInserir = new List<CampaignMember>();
    public List<CampaignMember> membrosAtualizar = new List<CampaignMember>();
         
    //unused methods
    public void OnBeforeInsert(){}    
    public void OnBeforeUpdate(){}
    public void OnAfterUpdate(){} 
    public void OnBeforeDelete(){}
    public void OnAfterDelete(){}
    public void OnUndelete(){}{ }
    
    //set class to execulting
    public LeadTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }
    
    //return if this class is execulting
    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }
    
    // execute after a Lead is inserted in salesforce
    public void OnAfterInsert()
    {         
        coreBusiness(); 
    }
    
    //All Core Business Rule
    public void coreBusiness()
    {
        Map<String,Lead> mapaEmails = new Map<String,Lead>();
        Map<Id,Id> mapaLeadCampanha = new Map<Id,Id>();
        Map<Id,String> mapaLeadCampanha_temp = new Map<Id,String>();
        List<Eventos__c> mapaEventos;
                           
        for(Lead leadTemp : this.newRecords) 
        {            
            mapaEmails.put(leadTemp.Email,leadTemp); 
            mapaLeadCampanha_temp.put(leadTemp.Id,leadTemp.events_participate__c);       
        }
        
        for (String key: mapaLeadCampanha_temp.keySet()){      
            if(mapaLeadCampanha_temp.get(key) != null){ //verificar se existe valor dentro do campo events_participate
                for (String event_part : mapaLeadCampanha_temp.get(key).split(';')){
                    if(Eventos__c.getValues(event_part)==null){
                        continue;
                    }
                    mapaLeadCampanha.put(key,Id.valueOf(event_part)); 
                    addMemberToCampaing(mapaLeadCampanha, mapaEmails,false);
                }
            }
        }    
                      
        try
        {
            if(!membrosInserir.isEmpty())
                insert membrosInserir;
    
            if(!membrosAtualizar.isEmpty())
                update membrosAtualizar;    
        }
        catch(exception ex)
        {
            System.debug(ex.getMessage());
        }
    }
    
    // Every time a new field required need to be created, you must add the field on the lead and campaign member 
    public void addMemberToCampaing(Map<Id,Id> mapaLeadCampanha, Map<String,Lead> mapaEmails, boolean isEOC){
        CampaignMember campaignMember;
        Lead lead;
        Boolean isContact = false;
        ID event_part;
        for (String key: mapaLeadCampanha.keySet()){ 
            event_part = mapaLeadCampanha.get(key);
        }
        System.debug(event_part);
        for(Contact contato : [Select Email, OwnerId, (Select CampaignId,Status, LeadId, ContactId, Member_Lead_ID__c From CampaignMembers Where CampaignId = :event_part) From Contact  Where Email in : mapaEmails.keySet()])
        {
            lead = mapaEmails.get(contato.Email);                       
            if(contato.CampaignMembers.size()>0)
            {                
                campaignMember = contato.CampaignMembers[0];
                if(campaignMember.Status == 'Not Invited')
                    return;
                campaignMember.Status = 'Registered';
                campaignMember.Member_Lead_ID__c = lead.Id;
                campaignMember.usertype__c = lead.User_Type__c;                
                campaignMember.Companion__c = lead.Companion__c;     
                campaignMember.Passport_number__c = lead.Passport_number__c;
                campaignMember.Functions_attending_EOCWW15__c = lead.Functions_attending__c;
                campaignMember.Special_Need__c = lead.Description;
                campaignMember.NeedHotelReservation__c = lead.NeedHotelReservation__c;
                campaignMember.Room__c = lead.Room__c;
                campaignMember.CheckIn__c = lead.CheckIn__c;
                campaignMember.CheckOut__c = lead.CheckOut__c;
                campaignMember.Paypal_Protocol__c = lead.Paypal_Protocol__c;
                
                membrosAtualizar.add(campaignMember);
                isContact = true;
            }
            else
            {
                if(contato.OwnerId == '005i0000007rpmk'){ // Owner id de prod CRM VPC Integration
                    continue;
                }
                campaignMember = new CampaignMember();
                campaignMember.ContactId = contato.Id;
                campaignMember.Member_Lead_ID__c = lead.Id;
                campaignMember.Status = 'Not Invited';
                campaignMember.usertype__c = lead.User_Type__c;
                campaignMember.CampaignId = event_part;   
                campaignMember.Companion__c = lead.Companion__c;
                campaignMember.Special_Need__c = lead.Description;
                campaignMember.Passport_number__c = lead.Passport_number__c;
                campaignMember.Functions_attending_EOCWW15__c = lead.Functions_attending__c;
                campaignMember.Paid_the_event__c = true; 
                campaignMember.NeedHotelReservation__c = lead.NeedHotelReservation__c;
                campaignMember.Room__c = lead.Room__c;
                campaignMember.CheckIn__c = lead.CheckIn__c;
                campaignMember.CheckOut__c = lead.CheckOut__c;
                campaignMember.Paypal_Protocol__c = lead.Paypal_Protocol__c;
                
                membrosInserir.add(campaignMember);
                isContact = true;
            }
            //mapaEmails.remove(contato.Email);
            
        }
        if(isContact)
            return;
        for(Lead leadTemp : mapaEmails.values())
        {
            campaignMember = new CampaignMember();
            campaignMember.LeadId = leadTemp.Id;
            campaignMember.Status = 'Not Invited';
            campaignMember.usertype__c = leadTemp.User_Type__c;
            campaignMember.CampaignId = event_part;
            campaignMember.Passport_number__c = leadTemp.Passport_number__c;
            campaignMember.Companion__c = leadTemp.Companion__c;
            campaignMember.Special_Need__c = leadTemp.Description;
            campaignMember.Functions_attending_EOCWW15__c = leadTemp.Functions_attending__c;
            campaignMember.Paid_the_event__c = true; 
            campaignMember.NeedHotelReservation__c = leadTemp.NeedHotelReservation__c;
            campaignMember.Room__c = leadTemp.Room__c;
            campaignMember.CheckIn__c = leadTemp.CheckIn__c;
            campaignMember.CheckOut__c = leadTemp.CheckOut__c;
            campaignMember.Paypal_Protocol__c = leadTemp.Paypal_Protocol__c;
            
            membrosInserir.add(campaignMember);       
        }
    }
}