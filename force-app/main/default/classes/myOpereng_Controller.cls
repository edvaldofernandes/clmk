public with sharing class myOpereng_Controller {
    public Account cia {get;set;}
    public Contact contato {get;set;}
    public String caseRadio {get;set;}
    public list<case> CasosAbertos {get;set;}
    public list<case> CasosFechados {get;set;}
    
    //Construtor
    public myOpereng_Controller(){
        contato = [SELECT Account.Id, Name, Id FROM Contact WHERE Email =: UserInfo.getUserEmail() LIMIT 1];
        if(contato!=null) cia = [SELECT Name FROM Account WHERE Id=:contato.Account.Id LIMIT 1];
       
        CaseRadio = '1';		//All Cases
        rodaBusca();
    }
    
    public void rodaBusca(){
        if(caseRadio == '0'){
        	BuscaCasosMeus();
        }else if(CaseRadio == '1'){
        	 BuscaCasosTodos();
        }
    }
        
    public PageReference BuscaCasosMeus() {
     
        CasosAbertos = [ SELECT CaseNumber, Subject, FlightOps_LastComment__c, CreatedDate, Status, Description
            FROM Case WHERE AccountId =: contato.AccountId AND (Status='New' OR Status='In Progress') AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c =: true
            AND ContactId =: contato.Id ORDER BY CaseNumber LIMIT 50];
        
          
        CasosFechados = [ SELECT CaseNumber, Subject, FlightOps_LastComment__c, CreatedDate, Status, Description
            FROM Case WHERE AccountId =: contato.AccountId AND Status = 'Closed' AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c =: true
            AND ContactId =: contato.Id ORDER BY CaseNumber LIMIT 100];
          
    	return null;
    }
    
    public PageReference BuscaCasosTodos() {
  		
       CasosAbertos =[ SELECT CaseNumber, Subject, FlightOps_LastComment__c, CreatedDate, Status, Description
            FROM Case WHERE AccountId =: contato.AccountId AND (Status='New' OR Status='In Progress') AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c =: true
            ORDER BY CaseNumber LIMIT 50];
        
        CasosFechados = [ SELECT CaseNumber, Subject, FlightOps_LastComment__c, CreatedDate, Status, Description
            FROM Case WHERE AccountId =: contato.AccountId AND Status = 'Closed' AND RecordType.Name = 'FlightOps Case Record Type' AND Customer_Visible__c =: true
            ORDER BY CaseNumber LIMIT 100];
        
        return  null;  
        	
    }
    
}