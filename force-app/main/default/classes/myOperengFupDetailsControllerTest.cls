/**
* @description: myOperengFupDetailsController test class.
**/
@isTest
public class myOperengFupDetailsControllerTest {
    @isTest static void testCanGetPublishedFup(){
        Fup__c expectedFup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
        
        Fup__c actualFup = myOperengFUPDetailsController.getFup(expectedFup.Id);
        
        System.assertEquals(expectedFup.Id, actualFup.Id);
    }
    
    /**
    * @description Test if an exception is thrown when trying to get an unpublished fup
    **/
    @isTest static void testCannotGetUnpublishedFup(){
        String exceptionType = 'System.AuraHandledException';
        Fup__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Unpublished')
            .build();
        try{
            myOperengFUPDetailsController.getFup(fup.Id);
        } catch(Exception e){
             System.assertEquals(exceptionType, e.getTypeName());
        }
    }
}