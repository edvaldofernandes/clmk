@isTest
public class KickoffTriggerHandlerTest {
	@isTest 
    static void preventDeleteTest(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp 1';
        opp.Fleet_Type__c = 'Turboprop';
        opp.StageName = 'Validation_of_approvals';
        opp.CloseDate = system.today();
        insert opp;
        Kickoff__c Ko = new Kickoff__c();
        Ko.Name = 'Checklist 1';
        Ko.Opportunity__c = opp.Id;
        Ko.Customer_Type__c = 'New Customer';
        Ko.submitted__c = true;
        insert Ko;
        System.assert([SELECT Count() FROM Kickoff__c]==1); 
        try{
            if(Test.isRunningTest()) delete Ko;
        } catch (DMLException ex) {
    		// We don't wanna catch them all       
		}
        System.assert([SELECT Count() FROM Kickoff__c]==1);
        Ko.submitted__c = false;
        update Ko;
        try{
            if(Test.isRunningTest()) delete Ko;
        } catch (DMLException ex) {
    		// We don't wanna catch them all       
		}
        System.assert([SELECT Count() FROM Kickoff__c]==0);
    }
}