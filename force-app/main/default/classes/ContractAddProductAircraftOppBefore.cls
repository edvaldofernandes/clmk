/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class responsible for copying the pricebook2Id from the opportunity to a new
* related service contract when its field Contract_Status__c is different from 
* 'Signed'.
*
* NAME: ContractAddProductAircraftOppBefore.cls
* AUTHOR:JFS                                                DATE: 03/12/2014
*******************************************************************************/

public class ContractAddProductAircraftOppBefore {
  public static void execute(){
    
    TriggerUtils.assertTrigger();
      
    set< id > lstContrato = new set< id >();
    
    for(ServiceContract crt: ( list < ServiceContract > ) trigger.new)
    {
      if(crt.Contract_Status__c != 'Signed') {
      	lstContrato.add( crt.Opportunity__c );
      }
    }

    map< id, Opportunity > mapOpp = new map< id, Opportunity > ( [SELECT id, Pricebook2Id FROM Opportunity WHERE Id =: lstContrato ] );
       
    for(ServiceContract crt: ( list < ServiceContract > ) trigger.new)
    {
      
      Opportunity lOpp = mapOpp.get( crt.Opportunity__c );
      if ( lOpp == null ) continue;
      crt.Pricebook2Id = lOpp.Pricebook2Id;
    }

  }

}