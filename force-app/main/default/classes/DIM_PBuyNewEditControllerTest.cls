// Used by DIM - Market Intelligence.
@isTest
public class DIM_PBuyNewEditControllerTest {

    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkIfTheLastValueOfCustomerNeedsIsImported() {
    
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        PBuy__c pBuyOld = new PBuy__c();
        pBuyOld.Account__c = account.Id;
        pBuyOld.Customer_Needs__c = '60';
        insert pBuyOld;
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(new PBuy__c());
        DIM_PBuyNewEditController controller = new DIM_PBuyNewEditController(standardController);
        
        controller.newPBuy.Account__c = account.Id;
        controller.updateNewPBuyAccordingToTheLastPBuy();
        
        System.assertEquals(pBuyOld.Customer_Needs__c, controller.newPBuy.Customer_Needs__c);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkIfTheDefaultValueOfCustomerNeedsIsImported() {

        ApexPages.StandardController standardController = new ApexPages.StandardController(new PBuy__c());
        DIM_PBuyNewEditController controller = new DIM_PBuyNewEditController(standardController);
        controller.updateNewPBuyAccordingToTheLastPBuy();
        
        System.assertEquals((String) PBuy__c.Customer_Needs__c.getDescribe().getDefaultValue(), controller.newPBuy.Customer_Needs__c);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
}