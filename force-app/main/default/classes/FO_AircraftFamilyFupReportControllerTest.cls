@isTest
public class FO_AircraftFamilyFupReportControllerTest {    
    /**
    * @description Test if it is possible to build an map with fups and
    * published updates.
    **/
    @isTest static void testCanGetPublishedUpdatesMap(){
        Map<Id, List<FUP_Update__c>> expectedResponse = new Map<Id, List<FUP_Update__c>>();
        FUP__c parentFup1 = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        
        FUP_Update__c fupUpdate1 = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .withParentFup(parentFup1)
            
            .build();
        FUP_Update__c fupUpdate2 = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .withParentFup(parentFup1)
            .build();
        FUP_Update__c fupUpdate3 = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .build();        
        
        
        FO_AircraftFamilyFupReportController controller = new FO_AircraftFamilyFupReportController();
        controller.showEJet = true;
        Map<Id, List<FUP_Update__c>> actualResponse = controller.getResponse();
        
        System.assert(!actualResponse.isEmpty());
        for(Id fupId : actualResponse.keyset()){
            List<Fup_Update__c> FupUpdates = actualResponse.get(fupId);
            for(Fup_Update__c u : FupUpdates){
                System.assertEquals(fupId, u.Fup__c);
            }            
        }
        
    }
    
    
    /**
    * @description Test if it is possible to build an map with fups and
    * published updates.
    **/
    @isTest static void testCanGetPublishedFups(){
        FUP__c parentFup1 = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        
        FO_AircraftFamilyFupReportController controller = new FO_AircraftFamilyFupReportController();
        controller.showEJet = true;
        List<FUP__c> actualResponse = controller.getPublishedFups();        
        
        System.assert(!actualResponse.isEmpty());
    }
    
}