@isTest(seeAllData=true)
private class ETRACK_GET_QUESTIONS_NEW_TEST {
    
    @isTest static void syncLSIETrackOutboundServiceTest() {
        
        Account account = new Account();
        account.Name = 'Test aircraftInformation';
     	account.Company_Nickname__c = 'tt account';
        account.Company_Status__c = 'Active';
        account.FlyEmbraerId__c = 'TEST 123456';
         
        Aircraft__c aircraft = new Aircraft__c ();
     	aircraft.Model_Type__c = 'TEST 190';
        aircraft.name = '19000111';
        aircraft.RecordTypeId = '012i0000001IU3DAAW';
        aircraft.Operator__c = account.Id;
         
        Test.startTest();
        
        ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element responseGuestions = new ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element();        
	   
        ETRACK_GET_QUESTIONS_NEW.questions questions = new ETRACK_GET_QUESTIONS_NEW.questions();
        questions.questionNumber = '30040011';
        questions.operator = 'test';
        questions.priority = 'test';
        questions.etdcode = 'test';
        questions.creationDate = Date.today() - Constants.DAYS;
        questions.closeDate = Date.today();
        questions.mro  = 'test';
        questions.doa = true;
        questions.dta = true;
        questions.chargedDisposition = true;
        questions.subject = 'test';
        questions.group_x = 'test';
        questions.category = 'test';
        questions.program = 'test';
        questions.ata = 'test';
        questions.subAta = 'test';
        questions.dueDate = Date.today();
        questions.cdd = Date.today();
        questions.ead = Date.today();  
       
        Test.setMock(WebServiceMock.class, new ETRACK_GET_QUESTIONS_NEW_SERVICE_TEST(questions));
        
        SyncLSIETrackOutboundService service = new SyncLSIETrackOutboundService();
        
        try{
            service.execute();
        }catch(Exception error){}
        
        responseGuestions.questions = new List<ETRACK_GET_QUESTIONS_NEW.questions>();
        responseGuestions.questions.add(questions);        
               
        Map<String, ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element> response_map_x = new Map<String, ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element>();

        response_map_x.put('response_x', responseGuestions);
        
        service.parseToEtrackObject(response_map_x.values());
        
        Test.stopTest();
    }
}