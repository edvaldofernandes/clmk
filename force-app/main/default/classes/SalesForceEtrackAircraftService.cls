public class SalesForceEtrackAircraftService {
    
    private List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft> salesForceToEtrackList;
    private ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement;
    private List<AircraftInformation> aircraftInformationList;
    private Map<String, String> accountMap;
    private List<Call_Out__c> calloutDeleteList;
    
    public SalesForceEtrackAircraftService(){
        
        this.calloutDeleteList = new List<Call_Out__c>();
        this.aircraftInformationList = deserializeAccountInformation();
        initWSElement();
        initDefaultObject();
    }
    
    private void initWSElement(){
        
        salesForceToEtrackList = new List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft>();
        salesForceElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
    }
    
    public void initDefaultObject(){
        
        if(accountMap == null)
            accountMap = EtrackRepository.buildAccountMap(); 
    }
    
    public List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft> buildAircraft(){
        
        for(AircraftInformation aircraftInformation : aircraftInformationList){
            
            Aircraft__c aircraft = aircraftInformation.getAircraft();
            Account account = aircraftInformation.getAccount();
            
            ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft sfAircraft = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAircraft();
            sfAircraft.modelType = aircraft.Model_Type__c;
            sfAircraft.operator =  accountMap.get(aircraft.Operator__c);
            sfAircraft.owner = accountMap.get(aircraft.Owner__c);
            sfAircraft.registration = aircraft.Registration__c;
            sfAircraft.serialNumber = aircraft.name;
            sfAircraft.flyEmbraerID = account.FlyEmbraerId__c;
            sfAircraft.objectType = aircraftInformation.getObjectType();
            sfAircraft.status = aircraftInformation.getStatus();
            salesForceToEtrackList.add(sfAircraft);
        }
        
        return salesForceToEtrackList;
    }
    
    private List<AircraftInformation> deserializeAccountInformation(){
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassName(String.valueOf(AircraftInformation.class));
        
        List<AircraftInformation>  aircraftInformationList = new List<AircraftInformation>();
        
        for(Call_Out__c callout : calloutList){
            
            List<AircraftInformation>  aircraftInfoList = (List<AircraftInformation>)JSON.deserialize(callout.payload__c, List<AircraftInformation>.class);
            
            for(AircraftInformation aircraftInformation : aircraftInfoList)
                aircraftInformationList.add(aircraftInformation);
            
            calloutDeleteList.add(callout);
        }
        
        return aircraftInformationList;
    }
    
    public List<Call_Out__c> getCalloutDeleteList(){
        
        return calloutDeleteList;
    }
}