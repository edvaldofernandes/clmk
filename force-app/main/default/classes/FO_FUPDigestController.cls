/**
* @description This class is the controller for a visual component that lists all
* updates by aircraft family.
**/
public class FO_FUPDigestController {
    public Contact contact {get; set;}
    /**
    * @description gets all recent fup updates and formats in a map for better
    * iterativity. 
    **/
    public Map<FUP__c, List<FUP_Update__c>> getResponse(){
        Map<FUP__c, List<FUP_Update__c>> response =
            new Map<FUP__c, List<FUP_Update__c>>();
        
        if (contact != null){
            List<FUP_Update__c> updates = FO_FupService.getUpdatesForDigestByContact(
                contact.Id
            );
            response = FO_FupService.buildFupUpdateMap(updates);
        }
		return response;
    }
    public Boolean getResponseIsEmpty(){
		return getResponse().isEmpty();
    }
}