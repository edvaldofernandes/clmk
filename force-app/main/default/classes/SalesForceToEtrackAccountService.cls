public class SalesForceToEtrackAccountService {
    
    private List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount> salesForceToEtrackList;
    private ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element salesForceElement;
    private List<AccountInformation> accountList;
    private List<Call_Out__c> calloutDeleteList;
    
    public SalesForceToEtrackAccountService(){
        
        this.calloutDeleteList = new List<Call_Out__c>();
        this.accountList = deserializeAccountInformation();
        initWSElement();
    }
  
    private void initWSElement(){
        
        salesForceToEtrackList = new List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount>();
        salesForceElement = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfo_element();
    }
    
    public List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount> buildAccount(){
        
        for(AccountInformation accountInformation : accountList){
            
            Account account = accountInformation.getAccount();
            ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount salesForceToEtrack = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoAccount();
            salesForceToEtrack.companyName = account.name;
            salesForceToEtrack.companyNickName = account.Company_Nickname__c;
            salesForceToEtrack.companyStatus = account.Company_Status__c;
            salesForceToEtrack.accountType = account.Account_Type__c;
            salesForceToEtrack.flyEmbraerID = account.FlyEmbraerId__c;
            salesForceToEtrack.objectType = accountInformation.getObjectType();
            salesForceToEtrack.status = accountInformation.getStatus();    
            salesForceToEtrackList.add(salesForceToEtrack);
        }
        
        return salesForceToEtrackList;
    }
    
    public static List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember> buildTeamMemberUpdatedOrInserted(List<AccountTeamMember> teamMemberRepositoryList, String ObjectAction){
        
        List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember> sfTeamMemberList = new List<ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember>();
        
        List<AccountTeamMember> teamMemberList = teamMemberRepositoryList;
        List<String> userIdList = new List<String>();
        List<String> accountIdList = new List<String>();
        
        if(teamMemberList == null)
            return null;
        
        for(AccountTeamMember accountTeamMember : teamMemberList){
            userIdList.add(accountTeamMember.userId);
            accountIdList.add(accountTeamMember.accountId);
        }
        
        List<user> userList = rcpRepository.searchUserByUserId(userIdList);
        
        List<Account> accountList = rcpRepository.searchFlyembraerId(accountIdList);
        
        for(AccountTeamMember accountTeamMember : teamMemberList){
            
            for(User user : userList){
                
                if(user.id == accountTeamMember.userId){
                    
                    ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember SfTeamMember = new ETRACK_OUT_BOUND_END_POINT.SalesForceInfoTeamMember();
            		
        			SfTeamMember.accountId = accountTeamMember.accountId;
        			SfTeamMember.email = user.E_mail__c;
       	 			SfTeamMember.name = user.name;
        			SfTeamMember.teamMemberRole = accountTeamMember.TeamMemberRole;
        			SfTeamMember.phone = user.phone;
                    SfTeamMember.objectType = ObjectAction;
                    
                    for(Account account : accountList)
                        if(account.id == accountTeamMember.accountId)
                        	SfTeamMember.flyEmbraerID = account.FlyEmbraerId__c;
                    
                    sfTeamMemberList.add(SfTeamMember);
                }
            }
        }
        
        return sfTeamMemberList;
    }
    
    private List<AccountInformation> deserializeAccountInformation(){
        
        List<AccountInformation>  accountInformationList = new  List<AccountInformation>();
        
        List<Call_Out__c> calloutList = CallOutRepository.findPayloadByClassName(String.valueOf(AccountInformation.class));
        
        for(Call_Out__c callout : calloutList){
            
            List<AccountInformation>  accountInfoList = (List<AccountInformation>)JSON.deserialize(callout.payload__c, List<AccountInformation>.class);
            
            for(AccountInformation accountInformation : accountInfoList)
                accountInformationList.add(accountInformation);
            
            calloutDeleteList.add(callout);
        }
        
        return accountInformationList;
    }
    
    public List<Call_Out__c> getCalloutDeleteList(){
        
        return calloutDeleteList;
    }
}