/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for updating the opportunity status when the service contract
* status is updated. If the service contract status is 'Signed', the opportunity
* status will be 'Closed Won'. If the service contract status is 'Dropped'
* the opportunity status will be 'Closed Lost'.
*
* NAME: ServiceContractUpdateOpportunity.cls
* AUTHOR: DPF                                                 DATE: 16/12/2014
*******************************************************************************/
public with sharing class ServiceContractUpdateOpportunity {

  private static final String SIGNED = 'Signed';
  private static final String DROPPED = 'Dropped';
  private static final String CLOSED_WON = 'Closed Won';
  private static final String CLOSED_LOST = 'Closed Lost';

  public static void execute()
  {
    TriggerUtils.assertTrigger();

    map<Id, ServiceContract> mapOppContrato = new map<Id, ServiceContract>();
    for ( ServiceContract contract : (list<ServiceContract>) trigger.new )
    {
      if ( ( TriggerUtils.wasChangedTo(contract, ServiceContract.Contract_Status__c, SIGNED) ||
           TriggerUtils.wasChangedTo(contract, ServiceContract.Contract_Status__c, DROPPED) ) && contract.Opportunity__c != Null )
      {
        mapOppContrato.put(contract.Opportunity__c, contract);
      }
    }
    if ( mapOppContrato.isEmpty() ) return;
    
    list<Opportunity> lstOppUpdate = new list<Opportunity>();
    for ( Id idOpp : mapOppContrato.keyset() )
    {
      ServiceContract contract = mapOppContrato.get(idOpp);
      
      lstOppUpdate.add(new Opportunity(Id = idOpp, StageName = ( contract.Contract_Status__c == SIGNED ? CLOSED_WON : CLOSED_LOST ),
        Reason_for_Acceptance__c = contract.Reason_of_Signature__c, Comments_for_Acceptance__c = contract.Comments_for_Signature__c,
        Reason_for_Rejection__c = contract.Reason_of_Rejection__c, Comments_for_Rejection__c = contract.Comments_for_Rejection__c, 
        Updated_by_trigger__c = true ));
    }
    if ( !lstOppUpdate.isEmpty() ) update lstOppUpdate;
  }
}