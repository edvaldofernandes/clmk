/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Sep-2015.
**/
@isTest
private class UpdateRecordTypesBatchTest
{

    static testmethod void test() 
    {
       
        Id recTypeOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
        Id recTypeQuote = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
        
        
        List<Opportunity> oppList = new List<Opportunity>();
        List<Quote> quoteList = new List<Quote>();
        
        Account acc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Operator'));
        insert(acc);
       
       
       for (Integer i=0;i<10;i++) 
       {
            oppList.add(new Opportunity(Name = 'Opp Teste Dev ' + string.valueOf(i),
                                        StageName = 'Prospecting',
                                        CloseDate = system.today(),
                                        Fleet_Type__c = 'Turboprop',
                                        RecordTypeId = recTypeOpp,
                                        AccountId = acc.Id
                                        )); 
        }
       insert oppList;
       
       for (Integer i=5;i<10;i++) 
            oppList[i].Type = 'Technical Services';
        
       update oppList;
       
       for (Integer i=0;i<6;i++) 
       {
            Quote quote = SObjectInstanceTest.createQuote(oppList[i].id);
            quote.RecordTypeId = recTypeQuote;
            quoteList.add(quote);        
       }
       insert quoteList;
       

       Test.startTest();
       UpdateRecordTypesBatch updtRT = new UpdateRecordTypesBatch();
       Database.executeBatch(updtRT);
       Test.stopTest();

       Integer i = [SELECT COUNT() FROM Opportunity where RecordTypeId = : recTypeOpp];
       System.assertEquals(i, 5);
       
       Integer x = [SELECT COUNT() FROM Opportunity where RecordTypeId = : Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId()];
       System.assertEquals(x, 5);
       
       Integer y = [SELECT COUNT() FROM Quote where RecordTypeId = : recTypeQuote];
       System.assertEquals(y, 5);
       
       Integer z = [SELECT COUNT() FROM Quote where RecordTypeId = : Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId()];
       System.assertEquals(z, 1);
       
    }
}