@istest public class myOperengCoveoSearchControllerTest {
    
    /**
    * @description Test if ....
    **/
    @isTest static void testCanGetCurrentUserAccountId(){
        Id accountIdTested;
        Account acc = new Account(
            Name = 'asdf', 
            Company_Nickname__c = 'test'
        );
        insert acc;
        Contact c = new Contact(
            LastName = 'asdf',
            accountId = acc.Id
        );
        insert c; 
        Profile p = [
            SELECT
            	Id
            FROM
            	Profile
            WHERE
            	Name='Services Catalog'
        ]; 
        User u = new User(
            contactId = c.Id,
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
        insert u;
        
		Id accountIdExpected = acc.Id; 
        system.runAs(u)
        {
            Test.startTest();
                accountIdTested = myOperengCoveoSearchController.getCurrentUserAccountId();
            Test.stopTest();
        }
		System.assertEquals(accountIdExpected, accountIdTested, 'Can not get the right account Id');        
    }
    
     /**
    * @description Test if ....
    **/
    @isTest static void testCanSplitEmailDomainsFromString(){
        Set<String> domainsExpected = new Set<String>{'@asdf.com', '@qwer.edu.ru'};
        String emailDomainString =  '@asdf.com, adfuvhd, @qwer.edu.ru';
        Set<String> domains = myOperengCoveoSearchController.splitEmailDomains(emailDomainString);

		System.assertEquals(domainsExpected, domains);
    }   
    
    /**
    * @description Test if ....
    **/
    @isTest static void testCanBuildEmailFilterProperly(){
        String expectedFilter = '("@asdf.com", "@qwer.com", "@apoes.edu.ru")';
        Set<String> domains = new Set<String>{'@asdf.com', '@qwer.com', '@apoes.edu.ru'};
        String filter =  myOperengCoveoSearchController.buildExternalEmailsOnlyFilter(domains);
        
        System.assertEquals(expectedFilter, filter);
    }       
    
    /**
    * @description Test if ....
    **/
    @isTest static void testCanGetEmailDomainsSetFromAccount(){
        Set<String> expectedDomains = new Set<String>{'@asdf.com', '@qwer.edu.ru'};
        Account acc = new Account(
            Name = 'asdf',
            FlightOps_Account_Domains__c = '@asdf.com, adfuvhd, @qwer.edu.ru',
            Company_Nickname__c = 'test'
        );
        insert acc;
        Set<String> domains = myOperengCoveoSearchController.getAccountEmailDomains(acc.Id);
        
        System.assertEquals(expectedDomains, domains);
	}
    
    /**
    * @description Test if ....
    **/
    @isTest static void testCanGetCurrentUserContactEmailAddressDomain(){
        String expectedDomain = '@asdf.com';
        String Email = 'asdf@asdf.com';
        String domain;
        Account acc = new Account(
            Name = 'asdf', 
            Company_Nickname__c = 'test'
        );
        insert acc;
        Contact c = new Contact(
            LastName = 'asdf',
            Email = Email,
            accountId = acc.Id
        );
        insert c; 
        Profile p = [
            SELECT
            	Id
            FROM
            	Profile
            WHERE
            	Name='Services Catalog'
        ]; 
        User u = new User(
            contactId = c.Id,
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
        insert u;

        system.runAs(u)
        {
            Test.startTest();
                domain = myOperengCoveoSearchController.getCurrentUserContactEmailAddressDomain();
            Test.stopTest();
        }
		System.assertEquals(expectedDomain, domain);   
        
    }
    
    @isTest static void testThrowsExceptionWhenContactDoesNotHaveEmailWhenAccountDoesNotHaveEither(){
        String exceptionType = 'FlightOpsException';
        Account acc = new Account(
            Name = 'asdf', 
            Company_Nickname__c = 'test'
        );
        insert acc;
        Contact c = new Contact(
            LastName = 'asdf',
            accountId = acc.Id
        );
        insert c; 
        Profile p = [
            SELECT
            	Id
            FROM
            	Profile
            WHERE
            	Name='Services Catalog'
        ]; 
        User u = new User(
            contactId = c.Id,
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
        insert u;

        
        system.runAs(u)
        {
            Test.startTest();                
                try{
                    String domain = myOperengCoveoSearchController.getCurrentUserContactEmailAddressDomain();
                }catch(Exception e){
                    System.assertEquals(exceptionType, e.getTypeName());
                } 
 
            Test.stopTest();
        }
    }
    /**
    * @description Test if ....
    **/
    @isTest static void testCanBuildCoveoToken(){
        String expectedFilter;
        Map<String, Object> searchToken;
        String emailDomain = '@asdf.com, adfuvhd, @qwer.edu.ru';
        Account acc = new Account(
            Name = 'asdf', 
            Company_Nickname__c = 'test',
            FlightOps_Account_Domains__c = '@asdf.com, adfuvhd, @qwer.edu.ru'
        );
        insert acc;
        Contact c = new Contact(
            LastName = 'asdf',
            accountId = acc.Id
        );
        insert c; 
        Profile p = [
            SELECT
            	Id
            FROM
            	Profile
            WHERE
            	Name='Services Catalog'
        ]; 
        User u = new User(
            contactId = c.Id,
            Alias = 'standt',
            Email='standarduser@asdf.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com'
        );
        insert u;
        
        expectedFilter = String.format(
            myOperengCoveoSearchController.COVEO_FILTER_TEMPLATE,
            new List<String>{
                acc.Id,
                '("@asdf.com", "@qwer.edu.ru")'
          	}
		);
        
        system.runAs(u)
        {
            Test.startTest();
                searchToken = myOperengCoveoSearchController.buildSearchToken();
            Test.stopTest();
        }

        System.assertEquals(expectedFilter, searchToken.get('filter'));
    }
    
    
}