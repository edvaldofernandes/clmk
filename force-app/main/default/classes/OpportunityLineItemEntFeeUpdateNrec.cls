/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* When Opportunity Line Item Entry Fee is deleted, the field Entry Fee in 
* Opportunity Line Item NREC must be zero.
*
* NAME: OpportunityLineItemEntFeeUpdateNrec.cls
* AUTHOR: AFC                                                  DATE: 24/03/2015
*******************************************************************************/
public with sharing class OpportunityLineItemEntFeeUpdateNrec 
{
  public static void clearEntryFee()
  {
    TriggerUtils.assertTrigger();
    
    set< id > setPbe = new set< id >();
    set< id > setOpp = new set< id >();
    
    for( OpportunityLineItem lineItemDeleted : ( list< OpportunityLineItem > )trigger.old  )
    {     
      if( lineItemDeleted.Princing__c == 'Entry fee' )
      {
        setPbe.add( lineItemDeleted.PricebookEntryId );
        setOpp.add( lineItemDeleted.OpportunityId );
      }
    }    
    if( setOpp.isEmpty() ) return;
        
    list< OpportunityLineItem > lstLineItemNrec = new list< OpportunityLineItem >( [  
      SELECT Id, Princing__c, PricebookEntryId, OpportunityId, Entry_fee__c
      FROM OpportunityLineItem WHERE Princing__c =: 'NREC'
      AND PricebookEntryId =: setPbe 
      AND OpportunityId =: setOpp ] );
      
    if ( lstLineItemNrec.isEmpty() ) return;
    
    for( OpportunityLineItem lineItemNrec : lstLineItemNrec )
    {       
      lineItemNrec.Entry_fee__c = 0;
    }
    update lstLineItemNrec;
  }  
}