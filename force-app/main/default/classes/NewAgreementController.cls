public class NewAgreementController {

    public Opportunity sourceOpportunity {get; set;}

    public NewAgreementController(){

        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'No Opportunity ID'));
            return;
        }
        
		sourceOpportunity = [SELECT Id, Name, AccountId FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        if(sourceOpportunity == null){
			ApexPages.addMessage(new ApexPages.Message
                                 (ApexPages.Severity.ERROR,'There is no Opportunity for that Id','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
    	}
    }
    
    public PageReference saveAgreement(){
        
        Agreement__c newAgreement = new Agreement__c();
        Datetime dateNow = System.now();
        
        newAgreement.Name = sourceOpportunity.Name + ' Proposal';
        newAgreement.Account__c = sourceOpportunity.AccountId;
        newAgreement.Buyer__c = sourceOpportunity.AccountId;
        newAgreement.Related_Opportunity__c = sourceOpportunity.Id;
        newAgreement.Proposal_Validity__c = Date.today();
        newAgreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId();
        newAgreement.Agreement_Color__c = '0000FF';
        newAgreement.DF_Color__c = 'Blue';
        database.insert(newAgreement);

        Kickoff__c kickoff = [SELECT Id, Opportunity__c, Commercial_Agreement__c FROM Kickoff__c WHERE Opportunity__r.Id =: sourceOpportunity.Id LIMIT 1];
        kickoff.Commercial_Agreement__c = newAgreement.Id;
        database.update(kickoff);
        
        Task creationTask = new Task();
        creationTask.WhatId = newAgreement.Id;
        creationTask.Description = 'Created Agreement From Opportunity - ' + sourceOpportunity.Name;
        creationTask.Subject = 'Other';
        creationTask.ActivityDate = Date.newInstance(dateNow.year(), dateNow.month(), dateNow.day());
        creationTask.Status = 'Completed';
        creationTask.OwnerId = UserInfo.getUserId();
        database.insert(creationTask);
        
        PageReference newPage;
        
        newPage = new PageReference('/' + newAgreement.id);
        
        return newPage.setRedirect(true);
    }
}