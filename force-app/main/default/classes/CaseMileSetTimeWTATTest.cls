@isTest 
private class CaseMileSetTimeWTATTest {
  @TestVisible private static final Id TYPE_REPAIR = RecordTypeMemory.getRecType('Case', 'Warranty');  

    static testMethod void testMilestoneTimeCalculator() {        
      
     Datetime dtsys = DateTime.now(); 
     Long i = dtsys.getTime();
       
      Account lConta = SObjectInstanceTest.conta();
      lConta.Type = 'Airline';
      //lConta.BillingCountry = 'Brasil';
      lConta.Company_Nickname__c = 'VanTseng';
      database.insert( lConta );

      User lUser = SObjectInstanceTest.createUser(UserInfo.getProfileId());
      database.insert( lUser );

      User lUser2 = SObjectInstanceTest.createUser(UserInfo.getProfileId());
      lUser2.Username = 'user1name@mail.com.br';
      lUser2.LastName += 1;
      lUser2.Alias += 1;
      lUser2.CommunityNickname += 1;
      database.insert( lUser2 );

      Entitlement lEnt = SObjectInstanceTest.createEntitlement( lConta.id );
      lEnt.Name = 'Default';
      lEnt.Type = 'Phone Support';
      database.insert( lEnt );

      //EEJ_Aircraft__c aircraft1 = SObjectInstanceTest.createAircraft();
      //aircraft1.Owner__c = lConta.id;
      //database.insert( aircraft1 );

      Case lCaso = SObjectInstanceTest.createCase();
      lCaso.RecordTypeId = TYPE_REPAIR;
      lCaso.AccountId = lConta.id;
      lCaso.OwnerId = lUser.Id;
      lCaso.EntitlementId = lEnt.Id;
      lCaso.Phase__c =  'Processing' ;
      lCaso.Status = 'Part at Repair';
      lCaso.Reason = 'Orders';
      lCaso.Fed_Ex_Delivery_Date__c = system.today();
      
      //lCaso.Priority = 'AOG';
        
      database.insert( lCaso );
        
        // Select an existing milestone type to test with
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        
        
        CaseMileSetTimeWTAT calculator = new CaseMileSetTimeWTAT();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(string.valueof(lCaso.id), mt.id);
        
        if(mt.name != null && mt.Name.equals(mt.id)) {
            System.assertEquals(actualTriggerTime, 7);
        }
        else {
            //System.assertEquals(actualTriggerTime, ( (dtsys.hour() *3600) + (dtsys.minute() *3600) ));
        }
        
        //c.priority = 'Low';
        update lCaso;
        actualTriggerTime = calculator.calculateMilestoneTriggerTime(lCaso.Id, mt.id);
        //System.assertEquals(actualTriggerTime, i);
    }
}