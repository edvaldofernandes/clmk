//Rodrigo Gimenes
@isTest
private class ControllerEOCTest {

    static testMethod void myUnitTest() 
    {
    	
    	Contact contact_temp = new Contact();
        contact_temp.Title = 'IT Analyst';
        contact_temp.Salutation = 'Mr.';
        contact_temp.FirstName = 'Bruno';
        contact_temp.LastName = 'Severino';
        contact_temp.Gender__c = 'Male';
        contact_temp.Contact_Status__c ='Active';
        insert contact_temp;
    	
    	Campaign campaign_temp = new Campaign();
        campaign_temp.Name = 'EOC WW 15';
        campaign_temp.Status = 'Planned';
        campaign_temp.IsActive = true;
        campaign_temp.Type = 'Seminar / Conference';
    	insert campaign_temp;
    	
    	CampaignMember campaignMember_temp = new CampaignMember();
        campaignMember_temp.CampaignId = campaign_temp.Id;
        campaignMember_temp.ContactId = contact_temp.Id;
        campaignMember_temp.Status = 'Added Only';
        campaignMember_temp.Functions_attending_EOCWW15__c = 'Special Dinner;MCW and Side Events Welcome Dinner;Welcome Cocktail;Day1;Day2;customers;MMEL;Training Workshop';
        insert campaignMember_temp;
        
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(campaignMember_temp);
         
      	//PageReference pageRef = Page.PricebookEditProduct;
      	//Test.setCurrentPage(pageRef);
      	ControllerEOC controller = new CONTROLLEREOC(stdCOntroller);
      	Map<String, Boolean> mapa = controller.Functions_attending;   
      	String testProperty1 = controller.optnsD1Selected;    
      	String testProperty2 =  controller.optnsD2Selected;
      	String testProperty3 =  controller.sideEvent;
   	  	String testProperty4 =  controller.eventParticipate;
        
    }
}