@isTest
public class CLMPromoteToPAControllerTest {

    static testMethod void alreadyPromoted(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        Agreement1.PA_Already_set__c = true;
        Agreement1.Related_Proposal__c = Agreement1.Id;
        database.insert(new List<Agreement__c>{Agreement1});
        Agreement1.Status_Category__c = 'Proposal Signed';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id', Agreement2.Id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMPromoteToPAController promoter = new CLMPromoteToPAController(sc);
        promoter.promote();
        
        promoter.promotionCompleted();
        promoter.goToNewPA();
        test.stopTest();
        
        system.assert([SELECT id FROM Agreement__c].size() == 1);
    }
    
    static testMethod void notApprovedOrSignedYet(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(Agreement1);
        Agreement1.Status_Category__c = 'Proposal Request';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement2.Id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMPromoteToPAController promoter = new CLMPromoteToPAController(sc);
        promoter.promote();
        promoter.promotionCompleted();
        promoter.goToNewPA();
		test.stopTest();
        
        system.assert([SELECT id FROM Agreement__c].size() == 1);
    }
    
    static testMethod void promote(){
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record to promote to PA
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        Agreement1.Signature_Date__c = date.today();
        Agreement1.Status_Category__c = 'Proposal Signed';
        Agreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        database.insert(Agreement1);
        Agreement1.Status_Category__c = 'Proposal Signed';
        database.update(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',Agreement2.Id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement1);
        CLMPromoteToPAController promoter = new CLMPromoteToPAController(sc);
        promoter.promote();
        promoter.promotionCompleted();
        promoter.goToNewPA();
        test.stopTest();
        
        system.assert([SELECT id FROM Agreement__c].size() == 2);
    }
    
    static testMethod void nullOption(){
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id', null);
        
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        Agreement__c Agreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        database.insert(Agreement1);
        Agreement__c Agreement2 = [SELECT id FROM Agreement__c LIMIT 1];
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Agreement2);
        CLMPromoteToPAController promoter = new CLMPromoteToPAController(sc);
        promoter.promote();
        test.stopTest();
                
        system.assert([SELECT id FROM Agreement__c].size() == 1);
    }

}