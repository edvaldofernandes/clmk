public class CLMSetPDP {
    
    static Map<String, String> keepAircraftPDP = new Map<String, String>();
    
    public CLMSetPDP(){}

    public String setPDP(String agreementId){
        
        //get all contracts from agreement
        List<Contract_PDP__c> listContracts = [SELECT id, Name, Aircraft_Type__c, Payment_Number_1__c, Payment_ammount__c, 
                                               Payment_Percentage__c, Payment_Date__c, Payment_Date_Delivery_Based__c,
                                               Installment_Type__c
                                               FROM Contract_PDP__c 
                                               WHERE Commercial_Agreement__c = :agreementId ORDER BY Aircraft_Type__c,Installment_Type__c];
        
        if(listContracts.isEmpty()){
            return 'There is no Payment Setup';
        }

        //get all items from agreement
        List<Agreement_Aircraft__c> agreementAircraftList = [SELECT id, Name, Order_Type__c, Basic_Price__c, Contractual_Delivery_Month__c, PDP_Already_set__c 
                                                            FROM Agreement_Aircraft__c 
                                                            WHERE Agreement__c = :agreementId AND PDP_Already_set__c = FALSE];
        
        //If there is no record to set, the following msg has to be shown.
        if(agreementAircraftList.isEmpty()){
            return 'None Elegible Aircraft';
        }
        
		//prepare set of aircraft id to delete Aircraft_PDP__c
		Set<String> setAircraftId = new Set<String>();
        for(Agreement_Aircraft__c aircraft : agreementAircraftList){
            setAircraftId.add(aircraft.Id);
        }
        
        //clear pending paymens and set a map of payments to keep
        prepareAircraftPDPList(setAircraftId);
        
        //list of aircraft payment to insert
        List<Aircraft_PDP__c> listAircraftPaymentToInsert = new List<Aircraft_PDP__c>();
        
        //create contract for each aircraft
        for(Agreement_Aircraft__c item : agreementAircraftList){
            for(Contract_PDP__c contract : listContracts){
                //System.debug('Contract PDP Type: ' + contract.Installment_Type__c);
                //System.debug('Aircraft PDP Type: ' + keepAircraftPDP.get(item.Id));
                //skip existent PDP               
                if(item.Id == keepAircraftPDP.get(contract.Installment_Type__c)){
                    //System.debug('|| SKIP ||');
                    continue;
                }
                
                // check if item's type and aircraft's type are equals
                if(item.Order_Type__c == contract.Aircraft_Type__c){
                    //System.debug('|| CREATE ||');
                    Aircraft_PDP__c aircraftPayment = new Aircraft_PDP__c();
                    aircraftPayment.Contract_Aircraft__c = item.Id;
                    aircraftPayment.Installment_Type_L__c = contract.Installment_Type__c;
                    aircraftPayment.Payment_Number__c = contract.Payment_Number_1__c;
                    aircraftPayment.Status_L__c = 'Pending';
                    
                    //if payment's value is flexible
                    if(contract.Payment_Percentage__c != null && contract.Payment_Percentage__c != 0 && item.Basic_Price__c != null){                        
                        aircraftPayment.Payment_Amount__c = (contract.Payment_Percentage__c/100) * item.Basic_Price__c;
                        //aircraftPayment.Planned_Amount__c = (contract.Payment_Percentage__c/100) * item.Basic_Price__c;
                    } else {
                        //payment's value is fixed
                        aircraftPayment.Payment_Amount__c = contract.Payment_ammount__c;
                        //aircraftPayment.Planned_Amount__c = contract.Payment_ammount__c;
                    }
                    
                    //if payment's date is flexible
                    if(contract.Payment_Date_Delivery_Based__c != null){
                        Integer subMonth = Integer.valueOf(contract.Payment_Date_Delivery_Based__c);
                        aircraftPayment.Due_Date_L__c = item.Contractual_Delivery_Month__c.addMonths(- subMonth);
                    } else {
                        //if payment's date is fixed
                        aircraftPayment.Due_Date_L__c = contract.Payment_Date__c;
                    }
                    
                    listAircraftPaymentToInsert.add(aircraftPayment);                    
                    
                    //we must set the field PDP_Already_set__c to TRUE
                    item.PDP_Already_set__c = TRUE;
                }
            }
        }
        
        //update lineitems to doesn't execute again
        update agreementAircraftList;
        
        //insert payments
        List<Database.SaveResult> result = Database.insert(listAircraftPaymentToInsert);     
       
        //Case there is any Aircraft Payment to de inserted, verify is the insert was successfull
        if(result.size() > 0){
            if(!result[0].isSuccess()){
                return 'Error while processing, please contact your administrator';
            }
        }
        //Case no records are inserted. Display message.
        else{
            return 'None Elegible Aircraft';    
        }
        return agreementId;
    }
    
    private static void prepareAircraftPDPList(Set<String> setAircraftId){
        List<Aircraft_PDP__c> listAircraftPDP = [SELECT Id, Contract_Aircraft__c, Installment_Type_L__c, Status_L__c 
                                                 FROM Aircraft_PDP__c 
                                                 WHERE Contract_Aircraft__c IN :setAircraftId ORDER BY Contract_Aircraft__c, Installment_Type_L__c];
        
        List<Aircraft_PDP__c> listAircraftPDPToDelete = new List<Aircraft_PDP__c>();
        
        for(Aircraft_PDP__c aircraftPDP : listAircraftPDP){
			
            System.debug('Aircraft PDP: ' + aircraftPDP);
            if (aircraftPDP.Status_L__c == 'Pending'){
                listAircraftPDPToDelete.add(aircraftPDP);
                //System.debug('|| REMOVED ||');
            }else{
                keepAircraftPDP.put(aircraftPDP.Installment_Type_L__c, aircraftPDP.Contract_Aircraft__c);
                //System.debug('|| KEPT ||');
            }
        }
             
        if(listAircraftPDPToDelete.size() > 0){
            delete listAircraftPDPToDelete;
        }
    }
}