/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for creating objects used in test class.
*
* NAME: SObjectInstanceTest.cls
* AUTHOR: RNM                                                  DATE: 08/05/2014
*******************************************************************************/
@isTest
public class SObjectInstanceTest { 

  private static Integer contador = 1;
/*
    public static EEJ_Aircraft__c createAircraft() {
        EEJ_Aircraft__c aircraft = new EEJ_Aircraft__c();
        aircraft.AC_Serial_Number__c = '0500'+ serialNr;
        aircraft.Model__c = 'P100';
        //aircraft.Registration_Number__c = '8776';
        aircraft.Total_Flight_Hours__c = 11;
        aircraft.Total_Flight_Cycles__c = 11;
        
        serialNr++;
        return aircraft;
    }

    
    
    // Cria caso com aircraft relacionado
    public static Case createCase(Id aircraftId) {
        Case caso = new Case();
        caso.AOG_Type__c = '1';
        caso.Priority = 'AOG';
        caso.EEJ_A_C__c = aircraftId;
        return caso;
    }
*/    
 public static EmailMessage email( id aParentId )
    {
      return new EmailMessage(
        BccAddress = 'felipe.souzaemb@gmail.com',
        MessageDate = system.today(),
        ToAddress = 'felipe.souzaemb@gmail.com',
        TextBody = 'textbody',
        CcAddress = 'felipe.souzaemb@gmail.com',
        Status = '1',
        FromAddress = 'felipe.souzaemb@gmail.com',
        ParentId = aParentId
        );
    }
   public static Case createCase() {
        Case caso = new Case();
        //caso.TSN__c = 10;
        //caso.CSN__c = 10;
        //caso.TSN__c = 10;
        //caso.CSN__c = 10;
        return caso;
    }
    
    public static Attachment anexo( id aParentId )
    {
      return new Attachment(
        Name = 'Anexo',
        Body = Blob.valueOf( 'Teste de anexo' ),
        ParentId = aParentId
      );
    }
  public static Account createAccount(Id accRecTypeId) {
    Account lAcc = new Account(
      BillingCountry = 'Brazil',
      Name = 'Test'+ contador,
      Company_Nickname__c = 'Test'+ contador,
      ICAO_Code__c = String.valueOf(contador),
      RecordTypeId = accRecTypeId
      //lAcc.CNPJ__c = '56.543.444/0001-28';
      //lAcc.Partnerid__c = 'Classe de teste C2B';
      );
        
    contador++;
    return lAcc;
  }
    
  public static Related_accounts__c createRelatedAccount(Id originAccId, Id targetAccId) {
    return new Related_accounts__c(
      Origin_account__c = originAccId, 
      //Origin_account_type__c = Origin_account__r.RecordType.Name, 
      Target_account__c = targetAccId 
      //Target_account_type__c = Target_account__r.RecordType.Name
      //Relationship_type__c = Origin_account_type__c & " >>> " & Target_account_type__c
      //Trigger_on__c = checked
      );
  }
  
 /* static SlaProcess lSLA = [ Select id From SlaProcess where isActive = true limit 1 ];*/
    
  public static Entitlement createEntitlement( id aAccId )
  {
    return new Entitlement( 
      Name = 'Default',
      Type = 'Phone Support',
      AccountID = aAccId,
      StartDate = Date.today(),
      endDate = Date.today().addDays( 300 )
   //   SlaProcessID = lSLA.id
      );
  }
  
/*
    public static Customer_Setup__c createCustomerSetup(Id ownerId, Id operatorId, Id aircraftId, Id contactId) {
        return new Customer_Setup__c(EEJ_A_C__c=aircraftId,
            A_C_Owner__c=ownerId,
            MFIR_Owner__c='MFIR Owner',
            A_C_Operator__c=operatorId,
            MFIR_Operator__c='MFIR Op Test',
            A_C_Model__c='E175',
            A_C_Serial_Number__c='123456',
            A_C_Registration_Number__c='654321',
            Default_Company_Admin_FlyEmbraer__c=contactId,
            Certification__c='ANAC');
    }
*/
  public static Group createGroup( String aName )
  {
    return new Group( Name = aName, Type = 'Queue' );
  }
    
  public static QueueSobject createQueue( ID aGroup, String aType )
  {
    return new QueueSobject( QueueId = aGroup, SobjectType = aType );
  }    
    
  public static User createUser( String profileId ) {
    return new User( 
      Username = 'username@embraer.com.br', 
      LastName = 'Last Name', 
      Email = 'email@embraer.com.br', 
      Alias = 'Alias', 
      CommunityNickname = 'Community Nickname', 
      TimeZoneSidKey = 'GMT', 
      LocaleSidKey = 'en_Us', 
      EmailEncodingKey = 'ISO-8859-1', 
      LanguageLocaleKey = 'en_Us', 
      ProfileId = profileId);
  }
/* 
    public static Root_Cause_Fail_Codes__c createFailCodeForCase( String aTipo, String aProgram ){
      return new Root_Cause_Fail_Codes__c(Name='FAIL', Type__c=aTipo, Program__c=aProgram );
    }    
*/
  public static Group createGroup ( ) {
    return new Group (
      Name = 'Grupo',
      Type = 'Regular',
      DeveloperName = 'Grupo'
    );
  }
    
  public static GroupMember createGroupMember ( Id aUserOrGroupId, Id aGroupId ) {
    return new GroupMember (
      UserOrGroupId = aUserOrGroupId,
      GroupId = aGroupId
      );
    }

  public static Pricebook2 getPricebook2Std ()
  {
    Pricebook2 pb2 = [ SELECT Id, isActive FROM Pricebook2
      WHERE IsStandard = true LIMIT 1];

    if ( !pb2.IsActive )
    {
      pb2.IsActive = true;
      database.update(pb2);
    }
    return pb2;
  }
  
  public static Id getPricebook2Std2 ()
  {
 
    return test.getStandardPricebookId();
  }
  

  public static Product2 createProduct2 () {
      contador++;
      return new Product2 (
          Name = 'Produto Teste'+contador,
          ProductCode = 'a488'+contador,
          Product_Status__c = 'Active'      
      );      
  }
    
    public static Product2 createProduct2 (id recordTypeId) {
        contador++;
        return new Product2 (
            Name = 'Produto Teste'+contador,
            ProductCode = 'a488'+contador,
            Product_Status__c = 'Active',
            RecordTypeId = recordTypeId
        );
    }
  
  public static Pricebook2 createPricebook2(){
    
    Pricebook2 Price = new Pricebook2(
    
    Name = 'Pricebook Teste',
    Annual_Escalation__c = 10,
    IsActive = true
    
    );
    return Price;
  
  }
  
  public static Id catalogoDePrecoPadrao() {
    return test.getStandardPricebookId();
  }  

  public static PricebookEntry createPricebookEntry ( Id aPricebook2Id, Id aProduct2Id ) {
    PricebookEntry pricebook = new PricebookEntry(
      Pricebook2Id = aPricebook2Id,
      Product2Id = aProduct2Id,
      IsActive = true,
      UseStandardPrice = false,
      UnitPrice = 1000
      );
      
      return pricebook;
  }
  
  public static Aircraft__c createAircraft()
  {
    Aircraft__c aircraft = new Aircraft__c (
     	Name = '12345678', 
        Commercial_Name__c = 'Other',
        Aircraft_Status__c = 'In Service'
    );
    
    return aircraft;
  }
  
  public static Opportunity createOpportunity()
  {
    Opportunity opp = new Opportunity(
     Name = 'Opp Teste Dev',
     StageName = 'Prospecting',
     CloseDate = system.today(),
     Fleet_Type__c = 'Turboprop'
    );
    
    return opp;
  }
  
  public static OpportunityLineItem createOppItem( id aOppItem, id aPBE )
  {
    return new OpportunityLineItem(
      OpportunityId = aOppItem,
      PricebookEntryId = aPBE,
      Quantity = 1,
      Sales_Price__c = 20,
      TotalPrice = 20.00
    );
  }
  
  public static Slots__c createSlot()
  {
    return new Slots__c(
      Start_date__c = System.today(),
      End_date__c = System.today().addDays( 10 ),
      Product_type__c = 'Training',
      Quantity__c = 100,
      Reserved_quantity__c = 0
    );
  }
  
  public static Related_Aircraft__c createRelatedAirCraft(id aAirCraft)
  {
    return new Related_Aircraft__c(
    Aircraft__c = aAirCraft 
    
    );
  }
  
  public static Contact createContact(id aAcc)
  {
    return new Contact(
    Title = 'Teste Contato',
    LastName = 'Contato',
    Gender__c = 'Male',
    Contact_Status__c ='Active',
    Email = 'Cloud2b@cloud2b.com.br',
    AccountId = aAcc
    );
  }
  
  public static ServiceContract createServiceContract( id aAccId, id aRecType )
  {
    return new ServiceContract(
      Name = 'Teste',
      AccountId = aAccId,
      RecordTypeId = aRecType
    );
  }

  public static Service_Contract_Management__c createSvcContractMgmt( id aAccId, id aServContract, id aEntId, id aRecType )
  {
    return new Service_Contract_Management__c(
      Account__c = aAccId,
      Service_contract__c = aServContract,
      
     /* Comentado linha abaixo pelo Fabio (Embraer) sob supervisão do Eduardo (C2B) 
     * Entitlement__c = aEntId,*/
      Credits__c = 1,
      RecordTypeId = aRecType
    );
  }

  public static ServiceContract createServiceContract()
  {
    return new ServiceContract(
      Name = 'Teste'
    );
  }

  public static ContractLineItem createContractLineItem(Id serviceContractId, Id pbeId)
  {
    return new ContractLineItem(
      ServiceContractId = serviceContractId,
      PricebookEntryId = pbeId,
      Quantity = 1,
      Sales_Price__c = 50,
      UnitPrice = 50
    );
  }
  
   public static Quote createQuote(Id OpportunityId)
  {
    return new Quote(
      Name = 'Teste',
      OpportunityId = OpportunityId,
      Quote_Submission_Date__c = System.Today()
    );
  }
  
  public static QuoteLineItem createQuoteLineItem(Id quoteId, Id pbeId)
  {
    return new QuoteLineItem(
      QuoteId = quoteId,
      PricebookEntryId = pbeId,
      Quantity = 1,
      Sales_Price__c = 50,
      UnitPrice = 50
    );
  }
  
  public static Service_catalog_setup__c serviceCatalog()
  {
    return new Service_catalog_setup__c(
     Per_diem__c = 1500,
     Per_diem_Hotel__c = 5000,
     Region__c = 'Africa'
    );
  }
  
  public static Account conta () {
      return new Account(
        Name = 'Tseng Aviation'
        //MFIR__c = '12'
      );
    }
  
  public static Discount_policy__c discountPolicy()
  {
    return new Discount_policy__c(
       Name='Policy 1',
       Discount__c = 10,
       Discount_type__c = 'Policy 1',
       Quantity_min__c = 1,
       Quantity_max__c = 10
    );
  }
    
    public static Agreement__c createApttusAgreement(Id accountId) {
        Agreement__c agreement = new Agreement__c(
            Name = 'Test'+ contador,
            Admin_Mode__c = 'true',
            Account__c = accountId,
            //Country__c = 'Brasil',
            DOCCON_Number__c = '150',
            Agreement_Color__c = 'FF3333',
            Status__c = 'Request'             
        );
        
        contador++;
        return agreement;
    }
    
    public static Agreement__c createCommercialAgreement(Id accountId) {
        Agreement__c agreement = new Agreement__c(
            Name = 'Test'+ contador,
            Admin_Mode__c = 'true',
            Account__c = accountId,
            //Country__c = 'Brasil',
            DOCCON_Number__c = '150',
            Agreement_Color__c = 'FF3333',
            Status__c = 'Request'             
        );
        
        contador++;
        return agreement;
    }
    
    public static Agreement_Aircraft__c createApttusAgreementLineItem(Id productId, Id agreementId, String orderType) {
        Agreement_Aircraft__c lineItem = new Agreement_Aircraft__c(
            Aircraft__c = productId,
            Contractual_Delivery_Month__c = Date.today(),
            Order_Type__c = orderType,
            //Basic_Price__c = 1000000,
            Aircraft_Configuration__c = 'AK',
            Agreement__c = agreementId,
            TREND_Delivery_Date__c = Date.today()
        );
        return lineItem;
    }
    
    public static Agreement_Aircraft__c createAgreementAircraft(Id productId, Id agreementId, String orderType) {
        Agreement_Aircraft__c lineItem = new Agreement_Aircraft__c(
            Aircraft__c = productId,
            Contractual_Delivery_Month__c = Date.today(),
            Order_Type__c = orderType,
            Status__c = 'Planned',
            //Basic_Price__c = 20000000,
            Aircraft_Configuration__c = 'AK',
            Agreement__c = agreementId,
            TREND_Delivery_Date__c = Date.today()
        );
        return lineItem;
    }


    public static Agreement_Aircraft__c createApttusAgreementLineItem(Id productId, Id agreementId, String orderType, Id priceId) {
        Agreement_Aircraft__c lineItem = new Agreement_Aircraft__c(
            Aircraft__c = productId,
            Contractual_Delivery_Month__c = Date.today(),
            //Basic_Price__c = 1000000,
            Aircraft_Configuration__c = 'AK',
            Agreement__c = agreementId,
            TREND_Delivery_Date__c = Date.today(),
            //Aircraft_Price__c = priceId,
            Included_Event_Confirmed__c = true,
            
            //Required fields
            Contract_Aircraft_Number__c = 0001,
            Status__c = 'Planned',
            Order_Type__c = orderType,  
            CurrencyIsoCode = 'USD'
        );
        return lineItem;
    }
    
    public static Escalation__c createEscalation(Id agreementId) {
        Escalation__c escalation = new Escalation__c(
            Commercial_Agreement__c = agreementId,
            Escalation_Rate__c = 2,
            Rate_Effective_Date__c = Date.today(),
            Escalation_Code__c = '12' + contador
        );
        
        contador++;
        return escalation;
    }
    
    public static Escalation__c createDefaultEscalation(Id agreementId, Integer num) {
        Escalation__c escalation = new Escalation__c(
            Commercial_Agreement__c = agreementId,
            Escalation_Rate__c = 2,
            Rate_Effective_Date__c = Date.today(),
            Escalation_Code__c = '12' + num
        );

        return escalation;
    }
    
    public static Contract_PDP__c createContractPDP(Id agreementId, String InstalType, String aircraftType) {
        Contract_PDP__c contract = new Contract_PDP__c(
            Commercial_Agreement__c = agreementId,
            Installment_Type__c = InstalType,
            Payment_Number__c = 1,
            Aircraft_Type__c = aircraftType            
        );
        return contract;
    }
    
    public static Aircraft_PDP__c createAircraftPDP(Id agreementLineItemId, string installtype, Integer paymentNumber, string statusPayment, date dueDate) {
        Aircraft_PDP__c pdp = new Aircraft_PDP__c(
            Contract_Aircraft__c = agreementLineItemId,
            Installment_Type_L__c = installtype,
            Payment_Number__c = paymentNumber,
            Status_L__c = statusPayment,
            Due_Date_L__c = dueDate            
        );
        return pdp;
    }
    
    
    public static Aircraft_Price__c createAircraftPrice(Id agreementId, String modelId) {
        Aircraft_Price__c aircraftPrice = new Aircraft_Price__c(
            Commercial_Agreement__c = agreementId,
            Model__c = modelId,
            Aircraft_Version__c = 'STD',
            Economic_Condition__c = 'Jan/16',
            Aircraft_List_Price__c = contador,
            Aircraft_Basic_Price__c = contador * 2
        );
        
        contador++;
        return aircraftPrice;
    }
    
    
     public static SOB_Event__c createSOBEvent(Id agreementLineItemId, string orderType, date eventDate, string bookStatus) {
        SOB_Event__c event = new SOB_Event__c(
            //Commercial_Agreement__c = agreementLineItemId,
            Order_Type__c = orderType,
            Event_Date__c = eventDate,
            Book_Status__c = bookStatus
        );
         return event;
     }
    
    public static SOB_Event__c createSOBEventForAircraft(Id agreementLineItemId, string orderType, date eventDate, string bookStatus) {
        SOB_Event__c event = new SOB_Event__c(
            Contract_Aircraft__c = agreementLineItemId,
            Order_Type__c = orderType,
            Event_Date__c = eventDate,
            Book_Status__c = bookStatus
        );
         return event;
     }

    
     public static Aircraft_Price__c createAircraftPrice(Id agreementLineItemId, Id productId, Integer value) {
        Aircraft_Price__c price = new Aircraft_Price__c(
            Commercial_Agreement__c = agreementLineItemId,
            Model__c = productId,
            Aircraft_Basic_Price__c = value,
            Aircraft_List_Price__c = value,
            Aircraft_Version__c = 'STD'
        );
         return price;
     }
    
    
    public static Task_Setup__c createTaskSetup() {
        Task_Setup__c ts = new Task_Setup__c(
            Name = 'Teste One',
            Description__c = 'Teste_de_criacao',
            Sequence__c = 10,
            Question__c = 'All',
            Leap_Time__c = 9,
            Question_API__c = 'All'
        );
       
        return ts;
    } 
    
    public static Task_Setup__c createTaskSetup(Integer sequence, String leapType, Integer leapTime, Boolean isBefore) {
        Task_Setup__c ts = new Task_Setup__c(
            Name = 'Teste One',
            Description__c = 'Teste_de_criacao',
            Sequence__c = sequence,
            Question__c = 'All',
            Leap_Time__c = leapTime,
            Leap_Time_Unit__c = leapType,
            Before_Selected_date__c = isBefore,
            Question_API__c = 'All'
        );
       
        return ts;
    }
    
      public static Task createTask() {
        Task t = new Task(        
            Task_Setup_Code__c = '5',
            Status = 'Open'
        );
        return t;
    } 
     
  
  
}