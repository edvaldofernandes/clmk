@isTest

private class AgreementLineItemDAOTest {
     static testMethod void myUnitTest(){
       
        //Create products
        Product2 productTest1 = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        productTest1.Show_In_SOB__c = TRUE;
        productTest1.DF_Group__c = '190/195';
        productTest1.Aircraft_Family__c = '';
        Product2 productTest2 = SObjectInstanceTest.createProduct2(RecordTypeMemory.getRecType('Product2', 'DCT_Aircraft'));
        productTest2.Show_In_SOB__c = TRUE;
        productTest2.DF_Group__c = '170/175';
        database.insert(new List<Product2>{productTest1,productTest2});
        
        //create account to create a agreement record
        Account accountTest = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(accountTest);
        
        //create agreement record
        Agreement__c apttusAPTSAgreement1 = SObjectInstanceTest.createApttusAgreement(accountTest.id);
        apttusAPTSAgreement1.Signature_Date__c = date.today();
        apttusAPTSAgreement1.Nickname__c = 'TST_FINAL';
        apttusAPTSAgreement1.Undisclosed_Nickname__c = 'UNDISCLOSED 01';
        apttusAPTSAgreement1.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        database.insert(new List<Agreement__c>{apttusAPTSAgreement1});
        
        //create price
        List<Aircraft_Price__c> prices = new List<Aircraft_Price__c>
        {
            SObjectInstanceTest.createAircraftPrice ( apttusAPTSAgreement1.Id, productTest1.Id, 100000000 ),
            SObjectInstanceTest.createAircraftPrice ( apttusAPTSAgreement1.Id, productTest2.Id, 200000000 )
        };

        database.insert(prices); 
        
        //create line item
        List<Agreement_Aircraft__c> agreementsCreationList = new List<Agreement_Aircraft__c>();
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest1.Id, apttusAPTSAgreement1.Id, 'Firm'));
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest2.Id, apttusAPTSAgreement1.Id, 'Firm'));
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest2.Id, apttusAPTSAgreement1.Id, 'Firm'));
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest1.Id, apttusAPTSAgreement1.Id, 'Option'));
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest2.Id, apttusAPTSAgreement1.Id, 'Option'));
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest1.Id, apttusAPTSAgreement1.Id, 'Purchase Right'));
        agreementsCreationList.add(SObjectInstanceTest.createAgreementAircraft(productTest2.Id, apttusAPTSAgreement1.Id, 'Purchase Right'));
		//system.debug('apttusAPTSAgreement1.Id ' + apttusAPTSAgreement1.Id);   
        agreementsCreationList[0].Aircraft_Price__c = prices[0].Id;
        agreementsCreationList[1].Aircraft_Price__c = prices[1].Id;
        agreementsCreationList[2].Aircraft_Price__c = prices[1].Id;
        agreementsCreationList[3].Aircraft_Price__c = prices[0].Id;
        agreementsCreationList[4].Aircraft_Price__c = prices[1].Id;
        agreementsCreationList[5].Aircraft_Price__c = prices[0].Id;
        agreementsCreationList[6].Aircraft_Price__c = prices[1].Id;
                                       
       /*                                
       for (Agreement_Aircraft__c key : agreementsCreationList) {
            //key.Aircraft_Price__r.Commercial_Agreement__r.Id = key.Aircraft_Price__r.Commercial_Agreement__r.Id
            system.debug('Aircraft_Price__r.Commercial_Agreement__r.Id ' +  key.Aircraft_Price__r.Commercial_Agreement__r.Id);
            system.debug('Aircraft_Price__r.Model__r.Id ' + key.Aircraft_Price__r.Model__r.Id);
            system.debug('Aircraft__r.Id ' + key.Aircraft__r.Id);
		}*/
        database.insert(agreementsCreationList);
        
        test.startTest();
            AgreementLineItemDAO.getLineItensBySetIds( new Set<String>{apttusAPTSAgreement1.id}, 2015 );  
            AgreementLineItemDAO.getLineItensBySetIds( new Set<String>{apttusAPTSAgreement1.id} );  
            AgreementLineItemDAO.getLineItensBySetAgreementsIds( new Map<String, Agreement__c>{apttusAPTSAgreement1.id => apttusAPTSAgreement1} );
            AgreementLineItemDAO.getLineItensByAgreementId(apttusAPTSAgreement1.Id);
            AgreementLineItemDAO.getLineItensByAgreementId(apttusAPTSAgreement1.Id, productTest1.Aircraft_Family__c);
            AgreementLineItemDAO.getLineItensByAgreementIdOrderedByFirmTrend(apttusAPTSAgreement1.Id);
            AgreementLineItemDAO.getLineItensByAgreementIdOrderedByTrend(apttusAPTSAgreement1.Id, productTest1.Aircraft_Family__c);
            AgreementLineItemDAO.getLineItensByAgreementIdOrderedByFirmTrend(apttusAPTSAgreement1.Id, productTest1.Aircraft_Family__c);
            AgreementLineItemDAO.getLineItensByAgreementIdOrderedByFirmContract(apttusAPTSAgreement1.Id);
            AgreementLineItemDAO.getLineItensByAgreementIdOrderedByFirmContract(apttusAPTSAgreement1.Id, productTest1.Aircraft_Family__c);
            AgreementLineItemDAO.getAgreementsByStatus(new Set<String>{'PA Request'});
            AgreementLineItemDAO.getLineItensBySetIdAndYearforDF(productTest1.Aircraft_Family__c, 2015, new Set<String>{'PA Request'});
            AgreementLineItemDAO.getLineItensBySetIdAndYear(new Set<String>{apttusAPTSAgreement1.id}, 2010, new Set<String>{'PA Request'});
            AgreementLineItemDAO.getLineItensBySetIdAndYear(new Set<String>{'PA Request'});
            AgreementLineItemDAO.getLineItensBySetIdAndAgreementsNotSigned(new Set<id>{apttusAPTSAgreement1.id}); 
        test.stopTest();
	
    }
}