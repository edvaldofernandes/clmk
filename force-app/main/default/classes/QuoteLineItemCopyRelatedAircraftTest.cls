/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class QuoteLineItemCopyRelatedAircraft
* NAME: QuoteLineItemCopyRelatedAircraftTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*
*******************************************************************************/
@isTest
private class QuoteLineItemCopyRelatedAircraftTest {

  private static final Integer LOTE = 200;

    static testMethod void myUnitTest() {
    
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
      
      Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
      Database.insert(pbe);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      Database.insert(opp);
      
      OpportunityLineItem oli = SObjectInstanceTest.createOppItem(opp.Id, pbe.Id);
      oli.princing__c = 'NREC';
      Database.insert(oli);
      
      oli = [SELECT Quantity, UnitPrice, princing__c  FROM OpportunityLineItem WHERE Id =: oli.Id];
      
      Aircraft__c air = SObjectInstanceTest.createAircraft();
      Database.insert(air);
      
      Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
      rel.Opportunity_product_id__c = oli.Id;
      Database.insert(rel);
            
      Quote cotacao = SObjectInstanceTest.createQuote(opp.Id);
      cotacao.Pricebook2Id = stdPB;
      Database.insert(cotacao);
      
      QuoteLineItem qli = SObjectInstanceTest.createQuoteLineItem(cotacao.Id, pbe.Id);
      qli.UnitPrice = oli.UnitPrice;
      qli.Quantity = oli.Quantity;
      qli.princing__c = oli.princing__c;
      
      Test.startTest();
      Database.insert(qli);
      Test.stopTest();
      
      Related_Aircraft__c relResult = [SELECT Aircraft__c FROM Related_Aircraft__c WHERE Quote_Line_Item__c =: qli.Id];
      system.assertEquals(rel.Aircraft__c, relResult.Aircraft__c);
    }
    
    static testMethod void lote() {
    
      Product2 prod = SObjectInstanceTest.createProduct2();
      Database.insert(prod);
      
      Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
      Database.insert(pbe);
      
      List<Opportunity> lstOpp = new List<Opportunity>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        Opportunity opp = SObjectInstanceTest.createOpportunity();
        lstOpp.add(opp);
      }
      Database.insert(lstOpp);
      
      
      List<OpportunityLineItem> lstOli = new List<OpportunityLineItem>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        OpportunityLineItem oli = SObjectInstanceTest.createOppItem(lstOpp.get(i).Id, pbe.Id);
        oli.princing__c = 'NREC';
        lstOli.add(oli);
      }
      Database.insert(lstOli);
      
      lstOli = [SELECT Quantity, UnitPrice, princing__c  FROM OpportunityLineItem WHERE Id =: lstOli];
      
      List<Quote> lstQuo = new List<Quote>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        Quote cotacao = SObjectInstanceTest.createQuote(lstOpp.get(i).Id);
        cotacao.Pricebook2Id = stdPB;
        lstQuo.add(cotacao);
      }
      Database.insert(lstQuo);
      
      List<QuoteLineItem> lstqli = new List<QuoteLineItem>();
      for ( Integer i = 0; i < LOTE ; i++ )
      {
        QuoteLineItem qli = SObjectInstanceTest.createQuoteLineItem(lstQuo.get(i).Id, pbe.Id);
        qli.Quantity = lstOli.get(i).Quantity;
        qli.UnitPrice = lstOli.get(i).UnitPrice;
        qli.princing__c = lstOli.get(i).princing__c;
      }
      
      Test.startTest();
      Database.insert(lstqli);
      Test.stopTest();
      
    }
}