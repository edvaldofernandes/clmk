public without sharing class ReportUtils
{

    
    public set<string> cellHeaderValues{get;private set;}
    public list<Reports.ReportFilter> reportFiltersList{get;set;}
    public boolean haveRowCount{get;set;}
    public id reportId{get;set;}
    public map<string,reportGroupWrapper> reportGroupMap{get;set;}
    
    private  map<integer,integer> indexColIndexAggregateMap;
    private  map<string,Reports.DetailColumn> detailColumnMap; 
    
    
    
    public ReportUtils()
    {
        
        cellHeaderValues = new set<string>();
        haveRowCount = false;
        reportGroupMap = new map<string,reportGroupWrapper>();
        indexColIndexAggregateMap = new map<integer,integer>();
        
    }

    public void runReport()
    {
    
        Reports.ReportDescribeResult describe;
        Reports.ReportExtendedMetadata reportExMd;
        Reports.ReportMetadata reportMd;   
        map<string,Reports.GroupingColumn> groupingColumnMap;
        map<string,Reports.AggregateColumn> aggregateColumnMap;
        Reports.ReportResults reportResults;
        Reports.ReportFilter reportFilter;
        Reports.ReportFactWithDetails factDetails;
        Reports.ReportFactWithDetails factDetailsLevel2;
        Reports.Dimension reportDim;
        map<string,integer> nameIndexColMap;
        map<integer,string> groupIndexLabelMap;
        reportGroupWrapper reportGroupLevel1;
        reportGroupWrapper reportGroupLevel2;
        reportGroupWrapper reportGroupLevel3;
        string factMapKey;
        string factMapKeyLevel2;
        string factMapKeyLevel3;
        integer index = 0;        
        integer rowCountIndex = 0;     
        
        nameIndexColMap = new map<string,integer>();  
        groupIndexLabelMap = new  map<integer,string>();

        describe = Reports.ReportManager.describeReport(this.reportId);
                
        // Get the report metadata
        reportMd = describe.getReportMetadata();
        

        
        
         //Add the existing filters
        for(Reports.ReportFilter rf : reportMd.getreportFilters())
        {
            reportFilter= new Reports.ReportFilter();
            reportFilter.setColumn(rf.getcolumn());
            reportFilter.setOperator(rf.getOperator()) ;
            reportFilter.setValue(rf.getValue());
            this.reportFiltersList.add(reportFilter);
        }
        
        reportMd.setReportFilters(reportFiltersList);
       
        // Get the Extended report metadata
        reportExMd = describe.getReportExtendedMetadata();
        detailColumnMap = reportExMd.getDetailColumnInfo()    ;    
        groupingColumnMap = reportExMd.getGroupingColumnInfo();
        aggregateColumnMap = reportExMd.getAggregateColumnInfo();
        
        
        //Get Group levels and names
        for(Reports.GroupingColumn col : groupingColumnMap.values())
            groupIndexLabelMap.put(col.getGroupingLevel(),col.getLabel());
            
        
        index = 0;
        for(Reports.DetailColumn col : detailColumnMap.values())
        {
            nameIndexColMap.put(col.getName(),index);
            cellHeaderValues.add(col.getLabel());
            index++;
        }
        
        index = 0;
        
         // Get all the aggregates
        for(Reports.AggregateColumn col : aggregateColumnMap.values())
        {
            if(col.getName().contains('!'))
            {
                indexColIndexAggregateMap.put(nameIndexColMap.get(col.getName().split('!')[1]),index);
                index++;
            }
                        
            if(col.getName().contains('RowCount'))
            {
                rowCountIndex = aggregateColumnMap.values().size()-1;
                haveRowCount = true;    
            }    
            
            
 
        }
        
        
        //Run the report with the new filter
        reportResults = Reports.ReportManager.runReport(reportId, reportMd,true);
        
        //Get all groupings for the report
        reportDim = reportResults.getGroupingsDown();
        
        index = 0;
        for(Reports.GroupingValue groupingVal : reportDim.getGroupings())
        {
            
            reportGroupLevel1 = new reportGroupWrapper();
            
            factMapKey = groupingVal.getKey() + '!T';
            
            factDetails = (Reports.ReportFactWithDetails)reportResults.getFactMap().get(factMapKey);
            
            reportGroupLevel1.groupLabel = groupIndexLabelMap.get(0);
            
            reportGroupLevel1.groupValue = groupingVal.getLabel();  
            
            reportGroupLevel1.groupRows = getRecordsByLevelList(factDetails.getRows());
            
            //Add subtotal lines
            if((haveRowCount && factDetails.getAggregates().size()>=2) || haveRowCount == false)
                reportGroupLevel1.subtotalRows =  getAggregatesByGroup(factDetails.getAggregates());
            
            if(haveRowCount)
                reportGroupLevel1.rowCount = ' (' + factDetails.getAggregates()[rowCountIndex].getLabel()  + ' record' + (integer.ValueOf(factDetails.getAggregates()[rowCountIndex].getLabel()) >=2 ? 's' :'') + ')' ;
            
            
            for(Reports.GroupingValue level2Grouping : groupingVal.getGroupings())
            {
                
                reportGroupLevel2 = new reportGroupWrapper();
                
                factMapKeyLevel2 = level2Grouping.getKey() + '!T';
                
                factDetails = (Reports.ReportFactWithDetails)reportResults.getFactMap().get(factMapKeyLevel2);
                
                reportGroupLevel2.groupRows = getRecordsByLevelList(factDetails.getRows());
                
                reportGroupLevel2.groupLabel = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + groupIndexLabelMap.get(1);
                
                reportGroupLevel2.groupValue = level2Grouping.getLabel();  
                
                //Add subtotal lines
                if((haveRowCount && factDetails.getAggregates().size()>1) || haveRowCount == false)
                    reportGroupLevel2.subtotalRows =  getAggregatesByGroup(factDetails.getAggregates());
                
                if(haveRowCount)
                    reportGroupLevel2.rowCount = ' (' + factDetails.getAggregates()[rowCountIndex].getLabel()  + ' record' + (integer.ValueOf(factDetails.getAggregates()[rowCountIndex].getLabel()) >=2 ? 's' :'') + ')' ;
                
                
                reportGroupLevel1.subGroup = reportGroupLevel2;
                
                for(Reports.GroupingValue level3Grouping : level2Grouping.getGroupings())
                {
                    
                    reportGroupLevel3 = new reportGroupWrapper();
                
                    factMapKeyLevel3 = level3Grouping.getKey() + '!T';
                    
                    factDetails = (Reports.ReportFactWithDetails)reportResults.getFactMap().get(factMapKeyLevel3);
                    
                    reportGroupLevel3.groupRows = getRecordsByLevelList(factDetails.getRows());
                
                    reportGroupLevel3.groupLabel = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + groupIndexLabelMap.get(2);
                    
                    reportGroupLevel3.groupValue = level3Grouping.getLabel();  
                
                    //Add subtotal lines
                    if((haveRowCount && factDetails.getAggregates().size()>1) || haveRowCount == false)
                        reportGroupLevel3.subtotalRows =  getAggregatesByGroup(factDetails.getAggregates());
                
                    if(haveRowCount)
                        reportGroupLevel3.rowCount = ' (' + factDetails.getAggregates()[rowCountIndex].getLabel()  + ' record' + (integer.ValueOf(factDetails.getAggregates()[rowCountIndex].getLabel()) >=2 ? 's' :'') + ')' ;
                
                
                
                    reportGroupLevel2.subGroup = reportGroupLevel3;
                }

            }
            
            reportGroupMap.put(string.valueOf(index),reportGroupLevel1);
            
            index++;
        }      
        
    }
    
    private list<ReportUtils.reportRecordRowWrapper> getRecordsByLevelList(list<Reports.ReportDetailRow> p_detailRowsList)
    {
        list<reportRecordRowWrapper> returnValue = new list<reportRecordRowWrapper>();
        reportRecordRowWrapper recordTemp;
        
        for(Reports.ReportDetailRow detailRow : p_detailRowsList) 
        {
                recordTemp = new reportRecordRowWrapper();
                recordTemp.cellsList = new list<reportRecordRowCellWrapper>(); 
                
                for(Reports.ReportDataCell cell : detailRow.getDataCells()) 
                {
                    
                    reportRecordRowCellWrapper cellTemp = new reportRecordRowCellWrapper();
                    cellTemp.cellValue= string.valueOf(cell.getValue());
                    cellTemp.haveURL = (cell.getValue() instanceOf Id);
                    cellTemp.cellLabel = cell.getLabel();
                    recordTemp.cellsList.add(cellTemp);
                    
                }
                
                returnValue.add(recordTemp);
        }
        
        return returnValue;
        
    }
    
    private list<reportRecordRowWrapper> getAggregatesByGroup(list<Reports.SummaryValue> p_summaryValues)
    {
        
        list<reportRecordRowWrapper> returnValue = new list<reportRecordRowWrapper>();
        reportRecordRowWrapper recordTemp;
        integer index = 0;
        
        recordTemp = new reportRecordRowWrapper();
        recordTemp.cellsList = new list<reportRecordRowCellWrapper>();
        
        index = 0;
        for(Reports.DetailColumn col : detailColumnMap.values())
        {
        
            reportRecordRowCellWrapper cellTemp = new reportRecordRowCellWrapper();
            cellTemp.cellValue= '&nbsp;';
            cellTemp.haveURL = false;
            cellTemp.cellLabel = '&nbsp;';
            
            if(indexColIndexAggregateMap.containsKey(index))
                cellTemp.cellLabel = p_summaryValues[indexColIndexAggregateMap.get(index)].getLabel();    
            
            recordTemp.cellsList.add(cellTemp);
            index++;
        }
        
        returnValue.add(recordTemp);
        
        return returnValue;
        
    }
    
    //Wrapper Classes
    
    public class reportRecordRowWrapper
    {
        public list<reportRecordRowCellWrapper> cellsList{get;set;}
    }
   
    public class reportRecordRowCellWrapper
    {
        public string cellValue{get;set;}
        public string cellLabel{get;set;}
        public boolean haveURL{get;set;}
    }
   
    public class reportGroupWrapper
    {
    
       public string groupLabel{get;set;}
       public reportGroupWrapper subGroup{get;set;}
       public list<reportRecordRowWrapper> groupRows{get;set;} 
       public list<reportRecordRowWrapper> subtotalRows{get;set;}
       public string rowCount{get;set;}
       public string groupValue{get;set;}
    
        public reportGroupWrapper()
        {
            groupRows = new list<reportRecordRowWrapper>();
            subtotalRows = new list<reportRecordRowWrapper>();
        }
    }
   
    public class reportGroupRowWrapper
    {
       public list<reportRecordRowWrapper> groupRowRecords{get;set;}
       
       public reportGroupRowWrapper()
       {
           groupRowRecords = new list<reportRecordRowWrapper>();
       }
       
   }
    
    
    
    
    

}