global class updateSCMQuantitiesBatch implements Database.Batchable<sObject> 
{

       
    global database.querylocator start(Database.BatchableContext BC) 
    {
        string query = '';
        Id recordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Entitlement').getRecordTypeId();
        
        query = 'Select Id,Contract_Line_Item_Master__c from Service_Contract_Management__c Where Contract_Line_Item_Master__r.ServiceContract.RecordTypeId = : recordTypeId' ;
        
        return Database.getQueryLocator(query);
    }

    global void finish(Database.BatchableContext BC) 
    {

    }


    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Set<Id> contractLineItemsIds = new Set<Id>();
        
        for(sObject tempObject : scope)
        {
            Service_Contract_Management__c scm = (Service_Contract_Management__c)tempObject;
            contractLineItemsIds.add(scm.Contract_Line_Item_Master__c);
        }    
        //utils.recalculateSCMQuantities(contractLineItemsIds);
    }
    
    
    
    
}