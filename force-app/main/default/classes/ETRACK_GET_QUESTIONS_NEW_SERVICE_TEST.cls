@isTest
global class ETRACK_GET_QUESTIONS_NEW_SERVICE_TEST  implements WebServiceMock {
   
    private ETRACK_GET_QUESTIONS_NEW.questions questions;
    
    public ETRACK_GET_QUESTIONS_NEW_SERVICE_TEST(ETRACK_GET_QUESTIONS_NEW.questions questions){
        
        this.questions = questions;
    }
    
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       
       ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element responseGuestions = new ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element();        
	   
       /*ETRACK_GET_QUESTIONS_NEW.questions questions = new ETRACK_GET_QUESTIONS_NEW.questions();
       questions.questionNumber = '30040011';
       questions.operator = 'test';
       questions.priority = 'test';
       questions.etdcode = 'test';
       questions.creationDate = Date.today() - Constants.DAYS;
       questions.closeDate = Date.today();
       questions.mro  = 'test';
       questions.doa = true;
       questions.dta = true;
       questions.chargedDisposition = true;
       questions.subject = 'test';
       questions.group_x = 'test';
       questions.category = 'test';
       questions.program = 'test';
       questions.ata = 'test';
       questions.subAta = 'test';
       questions.dueDate = Date.today();
       questions.cdd = Date.today();
       questions.ead = Date.today();*/  
       
       responseGuestions.questions = new List<ETRACK_GET_QUESTIONS_NEW.questions>();
       responseGuestions.questions.add(questions);        
               
       Map<String, ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element> response_map_x = new Map<String, ETRACK_GET_QUESTIONS_NEW.getQuestionsResponse_element>();
       
       response_map_x.put('response_x', responseGuestions);    
   }
}