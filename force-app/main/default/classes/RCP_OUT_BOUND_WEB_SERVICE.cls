//Generated by wsdl2apex

public class RCP_OUT_BOUND_WEB_SERVICE {
    public class SalesForceInfoAircraftHist {
        public Date createdDate;
        public String createdById;
        public String field;
        public String oldValue;
        public String newValue;
        public String parentId;
        private String[] createdDate_type_info = new String[]{'createdDate','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] createdById_type_info = new String[]{'createdById','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] field_type_info = new String[]{'field','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] oldValue_type_info = new String[]{'oldValue','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] newValue_type_info = new String[]{'newValue','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] parentId_type_info = new String[]{'parentId','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'createdDate','createdById','field','oldValue','newValue','parentId'};
    }
    public class updateFromSalesForce {
        public RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SalesForceInfo;
        private String[] SalesForceInfo_type_info = new String[]{'SalesForceInfo','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'SalesForceInfo'};
    }
    public class SalesForceInfoAccountHist {
        public Date createdDate;
        public String createdById;
        public String field;
        public String oldValue;
        public String newValue;
        public String parentId;
        private String[] createdDate_type_info = new String[]{'createdDate','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] createdById_type_info = new String[]{'createdById','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] field_type_info = new String[]{'field','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] oldValue_type_info = new String[]{'oldValue','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] newValue_type_info = new String[]{'newValue','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] parentId_type_info = new String[]{'parentId','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'createdDate','createdById','field','oldValue','newValue','parentId'};
    }
    public class SalesForceInfoAccount {
        public String accountType;
        public String companyName;
        public String companyNickname;
        public String icaoCode;
        public String geographicalArea;
        public String flyEmbraerID;
        public String flyEmbraerName;
        private String[] accountType_type_info = new String[]{'accountType','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] companyName_type_info = new String[]{'companyName','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] companyNickname_type_info = new String[]{'companyNickname','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] icaoCode_type_info = new String[]{'icaoCode','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] geographicalArea_type_info = new String[]{'geographicalArea','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] flyEmbraerID_type_info = new String[]{'flyEmbraerID','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] flyEmbraerName_type_info = new String[]{'flyEmbraerName','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'accountType','companyName','companyNickname','icaoCode','geographicalArea','flyEmbraerID','flyEmbraerName'};
    }
    public class SalesForceInfo_element {
        public RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraft[] aircraft;
        public RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAircraftHist[] aircraftHistory;
        public RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccount[] account;
        public RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountHist[] accountHistory;
        public RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfoAccountTeamMember[] accountTeamMember;
        private String[] aircraft_type_info = new String[]{'aircraft','http://webservice.rcp.embraer.com/',null,'1','-1','false'};
        private String[] aircraftHistory_type_info = new String[]{'aircraftHistory','http://webservice.rcp.embraer.com/',null,'1','-1','false'};
        private String[] account_type_info = new String[]{'account','http://webservice.rcp.embraer.com/',null,'1','-1','false'};
        private String[] accountHistory_type_info = new String[]{'accountHistory','http://webservice.rcp.embraer.com/',null,'1','-1','false'};
        private String[] accountTeamMember_type_info = new String[]{'accountTeamMember','http://webservice.rcp.embraer.com/',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'aircraft','aircraftHistory','account','accountHistory','accountTeamMember'};
    }
    public class SalesForceInfoAircraft {
        public String serialNumber;
        public String registration;
        public String aircraftStatus;
        public String aircraftCertification;
        public String modelType;
        public String operator;
        public String operatorCountry;
        public String geograficRegion;
        public String operatorCertification;
        public String owner;
        public String ownerCountry;
        public Date manufacturingDate;
        public Date oemDeliveryDate;
        public Date deliveryDateCurrentOperator;
        public String fhAccumulated;
        public String fcAccumulated;
        public String relevantInformation;
        public String oldOperator;
        public String oldOwner;
        public Date leasingStartDate;
        private String[] serialNumber_type_info = new String[]{'serialNumber','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] registration_type_info = new String[]{'registration','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] aircraftStatus_type_info = new String[]{'aircraftStatus','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] aircraftCertification_type_info = new String[]{'aircraftCertification','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] modelType_type_info = new String[]{'modelType','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] operator_type_info = new String[]{'operator','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] operatorCountry_type_info = new String[]{'operatorCountry','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] geograficRegion_type_info = new String[]{'geograficRegion','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] operatorCertification_type_info = new String[]{'operatorCertification','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] owner_type_info = new String[]{'owner','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] ownerCountry_type_info = new String[]{'ownerCountry','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] manufacturingDate_type_info = new String[]{'manufacturingDate','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] oemDeliveryDate_type_info = new String[]{'oemDeliveryDate','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] deliveryDateCurrentOperator_type_info = new String[]{'deliveryDateCurrentOperator','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] fhAccumulated_type_info = new String[]{'fhAccumulated','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] fcAccumulated_type_info = new String[]{'fcAccumulated','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] relevantInformation_type_info = new String[]{'relevantInformation','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] oldOperator_type_info = new String[]{'oldOperator','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] oldOwner_type_info = new String[]{'oldOwner','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] leasingStartDate_type_info = new String[]{'leasingStartDate','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'serialNumber','registration','aircraftStatus','aircraftCertification','modelType','operator','operatorCountry','geograficRegion','operatorCertification','owner','ownerCountry','manufacturingDate','oemDeliveryDate','deliveryDateCurrentOperator','fhAccumulated','fcAccumulated','relevantInformation','oldOperator','oldOwner','leasingStartDate'};
    }
    public class SalesForceInfoAccountTeamMember {
        public String teamMemberRole;
        public String user_x;
        private String[] teamMemberRole_type_info = new String[]{'teamMemberRole','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] user_x_type_info = new String[]{'user','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','true','false'};
        private String[] field_order_type_info = new String[]{'teamMemberRole','user_x'};
    }
    public class updateFromSalesForceResponse {
        public String status;
        private String[] status_type_info = new String[]{'status','http://webservice.rcp.embraer.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webservice.rcp.embraer.com/','false','false'};
        private String[] field_order_type_info = new String[]{'status'};
    }
    public class SFDCtoRCPWSPort {
        public String endpoint_x = WebServiceConfiguration.getEnPoint(Constants.RCP_OUT_BOUND_PROXY);
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://webservice.rcp.embraer.com/', 'RCP_OUT_BOUND_WEB_SERVICE'};
        public String updateFromSalesForce(RCP_OUT_BOUND_WEB_SERVICE.SalesForceInfo_element SalesForceInfo) {
            RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForce request_x = new RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForce();
            request_x.SalesForceInfo = SalesForceInfo;
            System.debug('Request rcp ' + request_x);
            RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse response_x;
            Map<String, RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse> response_map_x = new Map<String, RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://webservice.rcp.embraer.com/',
              'updateFromSalesForce',
              'http://webservice.rcp.embraer.com/',
              'updateFromSalesForceResponse',
              'RCP_OUT_BOUND_WEB_SERVICE.updateFromSalesForceResponse'}
            );
            System.debug('response into '  + response_map_x.get('response_x'));
            response_x = response_map_x.get('response_x');
            return response_x.status;
        }
    }
}