public with sharing class ChatterFilesNew {
 
    public ContentVersion file { get; set; }
    public string title { get; set; }
    public Id parentId { get; set; }
    public List<string> topics { get; set; }
    public string newTopic { get; set; }
     
    public ChatterFilesNew() {
        file = new ContentVersion();
        file.Origin = 'H';
        this.parentId = Apexpages.currentPage().getParameters().get('parentId');
    }
 
    public List<selectoption> getTopicItems(){
        List<selectoption> options = new List<selectoption>();  
        //options.add(new SelectOption('', Label.ResSearchSelect));        
        List<topic> tps = [ SELECT Id, Name FROM Topic ORDER BY Name ASC ];
        for(Topic t : tps ){ 
            options.add(new SelectOption(t.Id, t.Name)); 
        }
        return options;
    }
 
    public PageReference save() {
        insert file;
 
        FeedItem fi = new FeedItem(Body = this.title + '', ParentId = this.parentId, RelatedRecordId = file.id, Type = 'ContentPost');
        insert fi;
         
        for (string t : topics) {
            TopicAssignment ta = new TopicAssignment();
            ta.TopicId = t;
            ta.EntityId = fi.Id;    
            insert ta;          
        }       
         
        if (String.isNotempty(this.newTopic)) {
                         
            List<string> strings = newTopic.split(',');
            for (string s : strings) {
                Topic tpc = new Topic(Name = s.trim());
                insert tpc;
     
                TopicAssignment ta = new TopicAssignment();
                ta.TopicId = tpc.Id;
                ta.EntityId = fi.Id;
                insert ta;
            }
        }
        return new PageReference('/' + parentId);
    }    
        
    public PageReference cancel() {
        return new PageReference('/' + parentId);
    }
     
}