public class CLMUtil {

    public static Map<Id,Apttus__APTS_Agreement__c> memoryAgreements;
    
    /**
     * GSAMICO:
     * Função recebe como parâmetro uma lista e o nome de um campo (do objeto da lista)
     * gera um Map utilizando como chave (os valores do campo informado) e incluíndo os objetos da lista;
     * dentro de cada elemento do Map, haverá uma lista dos objetos que possuem  aquele valor.
     * 
     * @returns: Map<String,List<SObject>> - Utilizar casting para acesso direto aos campos do sObject desejado
     * 
     */
    public static Map<String,List<SObject>> autoMap ( List<SObject> listObjects, String fieldName )
    {
        Map<String,List<SObject>> returnMap = new Map<String,List<SObject>>();
        
        for ( SObject curObject :  listObjects )
        {
            List<SObject> mapList = returnMap.get( getFieldValue(curObject, fieldName) );
            if ( mapList == null )
            {
                mapList = new List<SObject>();
            }
            mapList.add(curObject);
            returnMap.put( getFieldValue(curObject, fieldName), mapList);
        }
        
        return returnMap;
    }

    private static String getFieldValue ( SObject sfObject, String fieldName )
    {
        String separator = '.';
        String returnValue = '';
        if ( !fieldName.contains(separator) )
        {
            if(sfObject.get(fieldName) != Null)
                returnValue = (String) sfObject.get(fieldName);
        }
        else
        {   if(getFieldValue ( (SObject) sfObject.getSObject(fieldName.substringBefore(separator)), fieldName.substringAfter(separator) ) != Null) 
                returnValue =  (String) getFieldValue ( (SObject) sfObject.getSObject(fieldName.substringBefore(separator)), fieldName.substringAfter(separator) );
        }
        
        return returnValue;
    }
    
    /*
    public static void setTriggerExecutionFlag(Set<Id> agreementsIds, Boolean flagValue){
        Set<String> agreementsStringIds = new Set<String>( (List<String>)new List<Id>( agreementsIds ) );
        
        CLMUtil.setTriggerExecutionFlag(agreementsStringIds, flagValue);
    }
    
    
    public static void setTriggerExecutionFlag(Set<String> agreementsIds, Boolean flagValue){
        List<Apttus__APTS_Agreement__c> agreementsUpdate = new List<Apttus__APTS_Agreement__c>();
        Set<String> setAgreement = new Set<String>();
        try {
            agreementsUpdate = [SELECT id, Trigger_Execution__c FROM Apttus__APTS_Agreement__c WHERE id IN:agreementsIds];
            for (Apttus__APTS_Agreement__c agreement : agreementsUpdate){
                agreement.Trigger_Execution__c = flagValue;
            }
            Database.update(agreementsUpdate);
            system.debug('setTriggerExecutionFlag.agreementsUpdate >>> ' + agreementsUpdate);
        } catch (Exception e){
            System.debug(e.getCause()); 
            System.debug(e.getStackTraceString()); 
        }    
    }
    */

    public static void setAdminMode(Set<Id> agreementsIds, Boolean flagValue, String methodName){
        Set<String> agreementsStringIds = new Set<String>( (List<String>)new List<Id>( agreementsIds ) );
        
        CLMUtil.setAdminMode(agreementsStringIds, flagValue, methodName);
    }

    public static void setAdminMode(Set<String> agreementsIds, Boolean flagValue, String methodName )
    {
        List<Apttus__APTS_Agreement__c> agreementsUpdate = new List<Apttus__APTS_Agreement__c>();
        Set<String> setAgreement = new Set<String>();

        startMemoryAgreements ();

        try {
            Boolean needUpdate = false;
            agreementsUpdate = getAgreementsList ( agreementsIds );
            for (Apttus__APTS_Agreement__c agreement : agreementsUpdate){
                if ( flagValue &&  String.isBlank(agreement.Admin_Mode__c) )
                {
                    agreement.Admin_Mode__c = methodName;
                    needUpdate = true;
                }
                else if ( !flagValue && methodName.equals(agreement.Admin_Mode__c) )
                {
                    agreement.Admin_Mode__c = '';
                    needUpdate = true;
                }
            }
            if ( needUpdate )
            {
                Database.update(agreementsUpdate);
            }
            system.debug('setTriggerExecutionFlag.agreementsUpdate >>> ' + agreementsUpdate);
        } catch (Exception e){
            System.debug(e.getCause()); 
            System.debug(e.getStackTraceString()); 
        }    
    }

    private static List<Apttus__APTS_Agreement__c> getAgreementsList ( Set<String> agreementsIds )
    {
        Set<String> agreementsToQuery = new Set<String>();
        List<Apttus__APTS_Agreement__c> returnList = new List<Apttus__APTS_Agreement__c>();

        for ( String agreementId : agreementsIds )
        {
            if ( memoryAgreements.containsKey(agreementId) )
            {
                // O AGREEMENT JÁ FOI BUSCADO - NÃO É NECESSÁRIO REALIZAR QUERY
                returnList.add(memoryAgreements.get(agreementId));
                continue;
            }

            agreementsToQuery.add(agreementId);
        }

        if (  agreementsToQuery.size()>0 )
        {
            List<Apttus__APTS_Agreement__c> agreementsFromDB = [SELECT id, Admin_Mode__c FROM Apttus__APTS_Agreement__c WHERE id IN:agreementsToQuery];
            memoryAgreements.putAll ( agreementsFromDB );
            returnList.addAll(agreementsFromDB);
        }

        return returnList;
    }

    private static void startMemoryAgreements ()
    {
        if ( memoryAgreements == null )
        {
            memoryAgreements = new Map<Id,Apttus__APTS_Agreement__c>();
        }
    }


    
    
    
    

}