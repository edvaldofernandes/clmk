public with sharing class FeedCommentHandler {
    
    @TestVisible
    private static string buildBody( FeedComment comment ){
    	
    	String attach = '<com:attachment/>';
    	
    	String postId = String.valueOf(comment.FeedItemId);//.substring(0, 15);
    	String commentId = String.valueOf(comment.Id).substring(0, 15);
    	
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.example.org/compass/">'+
				   '<soapenv:Header/>'+
				   '<soapenv:Body>'+
				   '   <com:receiveFeedBack>'+
				   '      <com:CPSFeedback>'+
				   '         <com:id>'+postId+'</com:id>'+
				   '         <com:Comment>'+
				   '            <com:feedbackId>'+ commentId +'</com:feedbackId>'+
				   '            <com:author>'+ [SELECT Name FROM User WHERE Id = :comment.CreatedById].Name +'</com:author>'+
				   '            <com:publishDate>'+ Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'America/Sao_Paulo') +'</com:publishDate>'+
				   '            <com:description>'+ comment.CommentBody +'</com:description>'+
				   				attach+
				   '         </com:Comment>'+
				   '      </com:CPSFeedback>'+
				   '   </com:receiveFeedBack>'+
				   '</soapenv:Body>'+
				'</soapenv:Envelope>';
				
		System.debug(body);
		return body;
    }
	
    @TestVisible
    @future( callout = true )
    private static void callWS( String url, String authorization, String body ) {
    	
		Http h = new Http();
        
	    HttpRequest req = new HttpRequest();
    	req.setEndpoint(url);
	    req.setMethod('POST');
        req.setBody( body );
        req.setHeader('Authorization', authorization);
        req.setHeader('Content-Type', 'text/xml;charset=utf-8');

    	// Send the request, and return a response
    	
    	if( Test.isRunningTest() ) {
    		Test.setMock( HttpCalloutMock.class , new CPSMockServer() );
    	}

        HttpResponse res = h.send(req);
    
        System.debug(res.getBody());    	
    }    
    
    public static void onInsert(List<FeedComment> comments) {
    	
    	Map<Id, List<FeedComment>> mapComments 	= new Map<Id, List<FeedComment>>();
		
		for(FeedComment comment : comments) {
			
			List<FeedComment> lista = new List<FeedComment>();
			
			if ( mapComments.get( comment.FeedItemId ) == null ) {
				
				lista.add( comment );
				
			} else {
				
				lista = mapComments.get( comment.FeedItemId );
				
				lista.add( comment );
			}
			
			mapComments.put( comment.FeedItemId, lista );
		}
    	
    	List<FeedItem> feeds = [SELECT Id, Body, ParentId
								FROM FeedItem
								WHERE ParentId = :[
								
										SELECT Id 
										FROM CollaborationGroup 
										WHERE Name = 'QMI - Quick Market Intelligence'
										LIMIT 1
									]
								AND Id IN :mapComments.keySet()
								AND InsertedById = :[
								
										SELECT Id
										FROM User
										//WHERE Username = 'integration@00dp000000036ctmai.com'
										WHERE Name = 'CRM VPC Integration'
										LIMIT 1
								]];
						
		Transaction_Delivery__c td = [ SELECT Name, URL__c, Authorization__c
										FROM Transaction_Delivery__c
										WHERE Name = 'CPSFeedback'
										LIMIT 1];						        
        
		for( FeedItem feedItem : feeds ) {			            
            
			for( FeedComment comment : mapComments.get(feedItem.Id) ) {
				
                if( !Test.isRunningTest() ) 
					callWs( td.URL__c, td.Authorization__c, buildBody(comment) );
			} 
		}
		
    }
}