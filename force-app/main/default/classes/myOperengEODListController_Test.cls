@isTest
public class myOperengEODListController_Test {
    		
    @isTest static void testeEods(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
        Database.insert(testUser);
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 1);
        	Database.insert(testCase);
            List<EOD__c> results = new List<EOD__c>();
            EOD__c[] testEod = FO_MyOP_TestDataFactory.createEods(testCase, 2);
            Database.insert(testEod[0]);
            results = myOperengEODListController.getEods();
              
        }
        Test.stopTest(); 
    }
    
    @isTest static void testeEodsOperator(){
        Test.startTest();
        //Instanciating class and databases
        Account[] testAccount = FO_MyOP_TestDataFactory.createAccounts(1);
        Database.insert(testAccount);
        Contact[] testContact = FO_MyOP_TestDataFactory.createContacts2(testAccount, 1, 4);
        Database.insert(testContact);
        User[] testUser = FO_MyOP_TestDataFactory.createUsers(testContact);
        Database.insert(testUser);
        //Executing test
        System.runAs(testUser[0]){
            Case[] testCase = FO_MyOP_TestDataFactory.createCases(testContact, 1);
        	Database.insert(testCase);
            List<EOD__c> results = new List<EOD__c>();
            EOD__c[] testEod = FO_MyOP_TestDataFactory.createEods(testCase, 2);
            Database.insert(testEod[0]);
            results = myOperengEODListController.getEods();
              
        }
        Test.stopTest(); 
    }
    
}