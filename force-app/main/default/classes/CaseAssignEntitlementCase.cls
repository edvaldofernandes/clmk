/*******************************************************************************
*                              Felipe - 2015
*-------------------------------------------------------------------------------
*
* Assing Entitlement x Case
*

*******************************************************************************/

public with sharing class CaseAssignEntitlementCase {


  @TestVisible private static final String entDEFAULT = 'Default';
  @TestVisible private static final Id recPRC = RecordTypeMemory.getRecType('Case', 'PRC');


  public static void execute() {

    TriggerUtils.assertTrigger();

    list<Case> lstCase = new list<Case>();
    set<String> lstEntNames = new set<String>{entDEFAULT};
    map<String, Id> mapEntitlement = new map<String, Id>();

    if(Trigger.isInsert) {
      lstCase = trigger.new;
    }
    //else if(Trigger.isUpdate) {
     // for(Case caso : (list<Case>)trigger.new) {
      //  if(TriggerUtils.wasChanged(caso, Case.RecordTypeId)) {
       //   lstCase.add(caso);
        //}
      //}
   // }

    if(lstCase.isEmpty()) return;

    for(Entitlement ent : [SELECT Id, AccountId, Name FROM Entitlement WHERE Name = :lstEntNames]) {
      mapEntitlement.put(ent.Name, ent.Id);
    }

    if(mapEntitlement.isEmpty()) return;

    for(Case caso : lstCase) {

      Id entId;
      string test2;
       
      if(caso.RecordTypeId == recPRC) {
        entId = mapEntitlement.get(entDEFAULT);
        
      }
     
      if(entId != null) {
      caso.EntitlementId = entId;
      }
     
    }

  }

}