@isTest
public class RFP_OnlineControllerTest {
    private static Id RecordTypeIdRFPOnline = Schema.SObjectType.InputForm__c.getRecordTypeInfosByName().get('RFP Online').getRecordTypeId();
    
    @testSetup static void setup() {
        insert new CRM_Customer_RFP__c(Enable_RFP_Notification_System__c = true, Email_address_to_send_by_default__c = 'mateus.caviglione@embraer.net.br', Account_Team_Roles_To_Receive_Email__c = 'ECAM - Embraer Customer Account Manager,CAM - Customer Account Manager,LCAM - Leasing Company Account Manager,Tech Rep');
        
    }    
    static TestMethod void RFPOnline_Test(){
        
        Test.startTest();
        try{

            PageReference pageRef = Page.RFP_OnlinePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Success','Success');
            
            RFP_OnlineController controller = new RFP_OnlineController();
            controller.save();
        }catch(Exception e){
        }
        Test.stopTest();
        
		List<InputForm__c> inptf = [select Subject__c from InputForm__c];
		System.assert(inptf.size() == 0, inptf);        
    }
    static TestMethod void RFPOnline_TestSuccess(){
        
        Test.startTest();
        try{
            InputForm__c inputForm = new InputForm__c(RecordTypeId = RecordTypeIdRFPOnline);
            inputForm.Subject__c = 'Test';
			inputForm.Description__c = 'Test';
            inputForm.Reason_for_Request_and_Expected_Benefits__c = 'Test';
            inputForm.QtyOfAircraft__c = 42;
            inputForm.Requester_Name__c = 'Test';
            inputForm.Requester_Company__c = 'Test';
            inputForm.Requester_Position__c = 'Test';
            inputForm.Requester_Email__c = 'contactEmail@email.com';
            inputForm.Requester_Phone__c = 'Test';
            //sr.Name='';
            // insert other fields

            PageReference pageRef = Page.RFP_OnlinePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Success','Success');
            
            RFP_OnlineController controller = new RFP_OnlineController(inputForm);
            controller.save();
        }catch(Exception e){
        }
        Test.stopTest();
        
		List<InputForm__c> inptf = [select Subject__c from InputForm__c where Subject__c = 'Test'];
		System.assert(inptf.size() == 1,'more then one' + inptf.size());        
    }
    
    static TestMethod void RFPOnline_TestFailure(){
        
        Test.startTest();
        try{
            InputForm__c inputForm = new InputForm__c(RecordTypeId = RecordTypeIdRFPOnline);
            inputForm.Subject__c = 'TestFail';
			inputForm.Description__c = 'Test';
            inputForm.Reason_for_Request_and_Expected_Benefits__c = 'Test';
            inputForm.QtyOfAircraft__c = 42;
            inputForm.Looking_for_Complete_Solution__c = 'Test';
            inputForm.Requester_Name__c = 'Test';
            inputForm.Requester_Company__c = 'Test';
            inputForm.Requester_Position__c = 'Test';
            inputForm.Requester_Email__c = 'contactEmail@email.com';
            inputForm.Requester_Phone__c = 'Test';
            //sr.Name='';
            // insert other fields

            PageReference pageRef = Page.RFP_OnlinePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Success','Success');
            
            RFP_OnlineController controller = new RFP_OnlineController(inputForm);
            controller.save();
        }catch(Exception e){
        }
        Test.stopTest();
        
		List<InputForm__c> inptf = [select Subject__c from InputForm__c where Subject__c = 'TestFail'];
		System.assert(inptf.size() == 0);        
    }
}