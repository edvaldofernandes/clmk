/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for deleting related aircraft when an opportunitylineitem is
* deleted.
*
* NAME: OppLineItemDeleteRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*******************************************************************************/

public with sharing class OppLineItemDeleteRelatedAircraft {

  public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    List<String> lstProdutoOppId = new List<String>();
    for ( OpportunityLineItem oli: (List<OpportunityLineItem>) trigger.old)
    {
    	lstProdutoOppId.add(oli.Id);
    }
    if ( lstProdutoOppId.isEmpty() ) return;
    
    List<Related_Aircraft__c> lstRelAircrafts = [ SELECT Id FROM Related_Aircraft__c 
      WHERE Opportunity_product_id__c =: lstProdutoOppId ];
      
    if ( !lstRelAircrafts.isEmpty() ) delete lstRelAircrafts;
  }
}