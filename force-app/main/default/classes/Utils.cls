/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class with helper methods
* NAME: Utils.cls
* AUTHOR: LRSA                                                 DATE: 09/01/2014
* Teste Histórico
*
*
*
* Deploy this class use the following test classes:
* - OpportunitySelectProductsControllerTest
* - UtilsTest
*******************************************************************************/

public without sharing class Utils 
{
  
    private static final String DECIMAL_SEPARATOR = '.';
    
    // Função para formatação de valores numéricos para o padrão brasileiro -99.999,99
    public static String formatValue( decimal aValue, integer aQttDec )
    {
     Decimal lIntValue = aValue.intValue();
     Decimal lDecValue = aValue - lIntValue;
     lDecValue = lDecValue.setScale( aQttDec );
     return lIntValue.format() + DECIMAL_SEPARATOR + lDecValue.toPlainString().right( aQttDec );
    }
    
    // Função para formatação de valores numéricos para o padrão brasileiro -99.999,99
    public static String formatValue( String aValue, integer aQttDec )
    {
      try
      {
        return formatValue( Decimal.valueOf( aValue ), aQttDec );
      }
      catch( Exception e )
      {
        return aValue;
      }
    }
    
    public static Decimal getNum( Decimal aValor ) 
    {
      return ( aValor == null ? 0 : aValor );
    }
    
    
    public static Decimal formatNumber(String aValor )
    {
        if ( String.isNotBlank(aValor) )
          return Decimal.valueOf(aValor.replace(',',''));
        return null;
    }
    
    public static string formatCEP( String aValue )
    {
        if ( aValue == null || aValue.length() != 8 ) return aValue;
        else return aValue.left( 2 ) + '.' + aValue.substring( 2, 5 ) + '-' + aValue.right( 3 );
    }
    
    public static String formatCPF( String aValue )
    {
        if ( aValue == null || aValue.length() != 11 ) return aValue;
        else return aValue.left( 3 ) + '.' + aValue.substring( 3, 6 ) + '.' + aValue.substring( 6, 9 ) + '-' + aValue.right( 2 );
    }
    
    public static String formatCNPJ( String aValue )
    {
      if ( aValue == null || aValue.length() != 14 ) return aValue;
      else return aValue.left( 2 ) + '.' + aValue.substring( 2, 5 ) + '.' + aValue.substring( 5, 8 ) + '/' + aValue.substring( 8, 12 ) + '-' + aValue.right( 2 );
    }
    
    public static String returnLabelField(String recordType, String field)
    {
        Pricing_Labels__c configLabel = Pricing_Labels__c.getValues(recordType);
        if ( configLabel != null ) return String.valueOf(configLabel.get(field));
        return '';
    }
    
    // Return '' when string is null
    public static String getStringWhenNull(String str){
        if(String.isBlank(str)) return ''; else return str;
    }
    
    public static String getStringFromSet(Set<Id> setId){
        String str = '';
        for(String val : setId){
            str += '\''+val + '\',';
        }
        return str.removeEnd(',');
    }
    
    public static Set<id> getUserIdsFromGroup(String pDeveloperName)
    {
        // store the results in a set so we don't get duplicates
        Set<Id> result=new Set<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();

        // Loop through all group members in a group
        for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where GroupId in (Select Id from Group Where DeveloperName = :pDeveloperName)])
        {
            // If the user or group id is a user
            if (((String)m.UserOrGroupId).startsWith(userType))
            {
                result.add(m.UserOrGroupId);
            }
            // If the user or group id is a group
            // Note: there may be a problem with governor limits if this is called too many times
            else if (((String)m.UserOrGroupId).startsWith(groupType))
            {
                // Call this function again but pass in the group found within this group
                result.addAll(GetUSerIdsFromGroup(m.UserOrGroupId));
            }
        }

        return result;  
    }
    
    public static List<String> comporTextoEmailAprovacao(Map<Id,String> pMapaIdNome,Map<Id,String> pMapaIdAccount)
    {
        List<String> retorno = new List<String>();
        string texto = '';
        
        for (ProcessInstance processo : [SELECT Status,TargetObjectId,(Select Comments From Steps ORDER BY CreatedDate ASC NULLS FIRST) FROM ProcessInstance Where TargetObjectId in : pMapaIdNome.keySet() ]) 
        {
            texto = '';
            texto += 'Dear user,your approval request was concluded.<br><br>' ;
            texto += 'Approval status : ' +  processo.status   + '<br>';
            texto += 'Item : ' +  processo.TargetObjectId.getSobjectType() + ' <a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+processo.TargetObjectId +'">'+ pMapaIdNome.get(processo.TargetObjectId)  + '</a><br>';
            texto += 'Account : ' +  pMapaIdAccount.get(processo.TargetObjectId)   + '<br>';
            System.Debug('processo puro  ' + processo);
            System.Debug('processo tamanho dos passos  ' + processo.Steps.size());
            if(processo.Steps.size()>=0)
            {
                if(!string.IsEmpty(processo.Steps[processo.Steps.size()-1].Comments))
                    texto += '<br>Comments : ' + processo.Steps[processo.Steps.size()-1].Comments;
            }
            texto += '<br>Kind regards.' ;
            
            retorno.add(texto) ;        
        }      
        return retorno;
    }
    public static void enviarEmail(List<Id> pListaIdUser, List<String> pListaCorpo, List<String> pListaAssunto,boolean pHtml)
    {
        List<Messaging.SingleEmailMessage> listaMensagens = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage email;
        
        for (integer indice = 0; indice <= pListaAssunto.size()-1; indice++) 
        {
            if(indice < pListaCorpo.size())
            {
                email = new Messaging.SingleEmailMessage();
                email.setSenderDisplayName('CRM - Commercial Aviation');
                email.setSubject(pListaAssunto[indice]);
                if(pHtml)
                {
                    email.setHtmlBody(pListaCorpo[indice]);
                }
                else
                {
                    email.setPlainTextBody(pListaCorpo[indice]);
                }
                email.setSaveAsActivity(false);
                email.setTargetObjectId(pListaIdUser[indice]);
                listaMensagens.add(email);
            }
        }      
        Messaging.sendEmail(listaMensagens); 
    }
  
    public static id insertTailoredProduct(Product2 pProduto)
    {
        List<Schema.FieldSetMember> campos = new List<Schema.FieldSetMember>();
        Product2 produto = new Product2();
        
        campos.addAll(sObjectType.Product2.FieldSets.CreateTailoredProductFieldsProduct.getFields());
        campos.AddAll(sObjectType.Product2.FieldSets.CreateTailoredProductFieldsDetailed.getFields());
        
        for(Schema.FieldSetMember campo : campos) 
            produto.put(campo.getFieldPath(),pProduto.get(campo.getFieldPath())) ;
        
        produto.recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Tailored').getRecordTypeId();
        insert produto;
        
        return produto.Id;
        
    }
    
    public static string insertPricebookEntries(PricebookEntry pStdPricebookEntry,PricebookEntry pPricebookEntry,Product2 pProduto)
    {
        
        Savepoint sp;
        String erros = '';
        Database.SaveResult result1;
        Database.SaveResult result2;
        Id idProduto = insertTailoredProduct(pProduto);

        
        pStdPricebookEntry.Product2Id = idProduto;
        pPricebookEntry.Product2Id = idProduto;
        
        System.Debug(pStdPricebookEntry.Pricebook2Id + ' id do Pricebook padrão');
        System.Debug(pStdPricebookEntry.Product2Id + ' id do produto para o Pricebook padrão');
        
        
        
        sp = Database.setSavepoint();
        result1 = Database.insert( pStdPricebookEntry, false );
      
        if ( !result1.isSuccess() )
            erros += result1.getErrors()[0].getMessage() + '\n ';
            
        System.Debug('Passou do primeiro - ' + erros);
        
        result2 = Database.insert( pPricebookEntry, false );
      
        if ( !result2.isSuccess() )
            erros += result2.getErrors()[0].getMessage() + '\n ';
            
        
        
        
        if ( String.isNotBlank(erros) )
            Database.rollback(sp);
            
        return erros;
    }
    
    public static string getAllFields(String pSObject) 
    {
        string returnString = '';
        
        Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(pSObject).getDescribe().fields.getMap();
        for(Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();
            returnString +=  (!string.isEmpty(returnString) ? ',' + dfield.getName() : dfield.getName());
        }

        
        return returnString;
    }
        
  
}