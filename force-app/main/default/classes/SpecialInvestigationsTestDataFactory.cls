@isTest
public class SpecialInvestigationsTestDataFactory{
    @isTest public static void createSpecialInvestigationDataWithDuplicate(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='1'));
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='2'));
        insert cases;
    }
    
    @isTest public static void createDuplicatesInSeparateBatches(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
        insert new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='1');
        insert new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='2');
    }
    
    @isTest public static void createNonDuplicatesInSeparateBatches(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
        insert new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 9/99/2099', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='1');
       insert new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 11/11/2011', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='1');
    }
    
    @isTest public static void createCasesNoDuplicate(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='1'));
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1899 removed under Notif 300659899 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, Notification__c='300659803', SO__c='2'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationDataNoDuplicate(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationDataRogue(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified ROGUE - P/N 7028419-1902, S/N 06041094 removed under Notif 300660093 on 12/27/2018', 
                           Description='Current Notif: 300660093'
                           +'\nReported removed by Aeroméxico on 12/12/2018 from XA-ALP[Embraer 190/138],'
                           +'\nAC delivered 4/20/2016'
                           +'\nRFR: MAU3 GPS2 (EGPWS)/WRG FAULT, FC 34410033EG1, MAU1 GPS1 (EGPWS)/ WRG'
                           +'\nFAULT, FC 34410032EG1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\nSAP History:'
                           +'\n01/24/2018 - Delivered TO:'
                           +'\n01/23/2018 - Returned FROM:'
                           +'\n01/11/2018 - Delivered TO:'
                           +'\n01/10/2018 - Returned FROM:'
                           +'\n01/01/2018 -'
                           +'\n08/27/2016 - Delivered TO:'
						   +'\n07/24/2016 - Delivered TO:'
						   +'\n07/20/2016 - Returned FROM:'
                           +'\n05/24/2016 - Delivered TO:'
                           +'\n05/24/2016 - Returned FROM:'
                           +'\n05/18/2016 - Notif 300467385:'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO:'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationDataCustomerIdentifiedLTOW(){
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Customer Request Identified LTOW - P/N 174692-94, S/N 17699 removed under Notif 300661927 on 12/13/2018', 
                           Description='Current Notif: 300660093'
                           +'\nReported removed by Aeroméxico on 12/12/2018 from XA-ALP[Embraer 190/138],'
                           +'\nAC delivered 4/20/2016'
                           +'\nRFR: MAU3 GPS2 (EGPWS)/WRG FAULT, FC 34410033EG1, MAU1 GPS1 (EGPWS)/ WRG'
                           +'\nFAULT, FC 34410032EG1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\nSAP History:'
                           +'\n01/24/2018 - Delivered TO:'
                           +'\n01/23/2018 - Returned FROM:'
                           +'\n01/11/2018 - Delivered TO:'
                           +'\n01/10/2018 - Returned FROM:'
                           +'\n01/01/2018 -'
                           +'\n08/27/2016 - Delivered TO:'
						   +'\n07/24/2016 - Delivered TO:'
						   +'\n07/20/2016 - Returned FROM:'
                           +'\n05/24/2016 - Delivered TO:'
                           +'\n05/24/2016 - Returned FROM:'
                           +'\n05/18/2016 - Notif 300467385:'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO:'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationDataInvestigationItemDetected(){
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Investigation Item Detected - P/N 1007086-7, S/N 2008071382 removed under Notif 300666040 on 01/08/2019', 
                           Description='Current Notif: 300660093'
                           +'\nReported removed by Aeroméxico on 12/12/2018 from XA-ALP[Embraer 190/138],'
                           +'\nAC delivered 4/20/2016'
                           +'\nRFR: MAU3 GPS2 (EGPWS)/WRG FAULT, FC 34410033EG1, MAU1 GPS1 (EGPWS)/ WRG'
                           +'\nFAULT, FC 34410032EG1'
                           +'\nTSN:N/A | CSN: | TSI:N/A | CSI:N/A | TSO:N/A | CSO:N/A'
                           +'\n\nSAP History:'
                           +'\n01/24/2018 - Delivered TO:'
                           +'\n01/23/2018 - Returned FROM:'
                           +'\n01/11/2018 - Delivered TO:'
                           +'\n01/10/2018 - Returned FROM:'
                           +'\n01/01/2018 -'
                           +'\n08/27/2016 - Delivered TO:'
						   +'\n07/24/2016 - Delivered TO:'
						   +'\n07/20/2016 - Returned FROM:'
                           +'\n05/24/2016 - Delivered TO:'
                           +'\n05/24/2016 - Returned FROM:'
                           +'\n05/18/2016 - Notif 300467385:'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO:'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationNoTSNSection(){
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\n\n314 days on wing'
                           +'\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationNoTsnOrDaysOnWing(){
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\n\nSAP History:'
                           +'\n01/24/2018 - Delivered TO: AEROMEXICO'
                           +'\n01/23/2018 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/11/2018 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n01/10/2018 - Returned FROM: COMPASS AIRLINES, LLC'
                           +'\n01/01/2018 - Notif 300590459: A/C: N621CZ - RFR:TAT FAIL'
                           +'\n08/27/2016 - Delivered TO: COMPASS AIRLINES'
						   +'\n07/24/2016 - Delivered TO: EMBRAER - MSP'
						   +'\n07/20/2016 - Returned FROM: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Delivered TO: ROSEMOUNT AEROSPACE INC.'
                           +'\n05/24/2016 - Returned FROM: AEROMEXICO'
                           +'\n05/18/2016 - Notif 300467385: (*ROGUE*) - RFR:DISCREPANCY N / P NO ACTIVE'
                           +'\nDISCHARGED - *NFF*'
                           +'\n05/09/2016 - Delivered TO: AEROMEXICO'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationNoTsnOrDaysOnWingOrSapHistory(){
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Automation Identified LTOW - P/N 0102AY1AF, S/N B1897 removed under Notif 300659803 on 12/04/2018', 
                           Description='Current Notif: 300659803'
                           +'\nReported removed by Aeroméxico on 12/3/2018 from XA-ACC[Embraer 190/499],'
                           +'\nAC delivered 12/9/2011'
                           +'\nRFR: LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1'
                           +'\n\n----------------------------------------------------------------------'
                           +'The information contained in this transmission may contain confidential and proprietary information. It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to postmaster@embraer.com.',
                           RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest public static void createSpecialInvestigationDataNoNotificationInSubject(){  
        //create parts, accounts and aircraft
        createSpecialInvestigationPartsAccountsAndAircraft();
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Special Investigations').getRecordTypeId();
       	List<Case> cases = new List<Case>();
        cases.add(new Case(Subject='Subject Test', Description='Description Test', RecordTypeId=rTypeId, SO__c='1'));
        insert cases;
    }
    
    @isTest private static void createSpecialInvestigationPartsAccountsAndAircraft(){
        //create parts
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        parts.add(new LLPDatabase__c(Name='0102AY1AF', Description__c='SENSOR, TOTAL TEMPERATURE', Ecode__c='3755240', Top_Most__c='3755240', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create accounts
        List<Account> accts = new List<Account>();
        accts.add(new Account(Name='Aeromexico', Company_Nickname__c='Aeromexico'));
        insert accts;
        
        //create aircraft
        Id rTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Embraer').getRecordTypeId();
        List<Aircraft__c> planes = new List<Aircraft__c>();
        planes.add(new Aircraft__c(Name='19000499', Registration__c='XA-ACC', Aircraft_Status__c='In Service', Fleet_Type__c='EJET', OEM_Delivery_Date__c=Date.newInstance(2008, 7, 14), RecordTypeId=rTypeId));
        insert planes;
    }
}