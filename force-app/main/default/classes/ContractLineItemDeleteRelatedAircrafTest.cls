/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class ContractLineItemDeleteRelatedAircraft
*
* NAME: ContractLineItemDeleteRelatedAircrafTest.cls
* AUTHOR: DPF                                                DATE: 12/12/2014
*******************************************************************************/
@isTest
private class ContractLineItemDeleteRelatedAircrafTest
{
  private static final Integer LOTE = 200;

  static testMethod void myUnitTest()
  {
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();

    ServiceContract opp = SObjectInstanceTest.createServiceContract();
    opp.Pricebook2Id = stdPB;
    Database.insert(opp);

    Product2 prod = SObjectInstanceTest.createProduct2();
    Database.insert(prod);

    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
    Database.insert(pbe);

    ContractLineItem oli = SObjectInstanceTest.createContractLineItem(opp.Id, pbe.Id);
    Database.insert(oli);

    Aircraft__c air = SObjectInstanceTest.createAircraft();
    Database.insert(air);

    Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
    rel.Contract_Line_Item__c = oli.Id;
    Database.insert(rel);

    Test.startTest();
    Database.delete(oli);
    Test.stopTest();

    list<Related_Aircraft__c> lstRel = [SELECT Id from Related_Aircraft__c WHERE Contract_Line_Item__c =: oli.Id];
    system.assert(lstRel.isEmpty());
  }

  static testMethod void lote()
  {
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();

    list<ServiceContract> lstOpp = new list<ServiceContract>();
    for (Integer i = 0; i < LOTE ; i++ )
    {
      ServiceContract opp = SObjectInstanceTest.createServiceContract();
      opp.Pricebook2Id = stdPB;
      lstOpp.add(opp);
    }
    Database.insert(lstOpp);

    Product2 prod = SObjectInstanceTest.createProduct2();
    Database.insert(prod);

    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
    Database.insert(pbe);

    list<ContractLineItem> lstOli = new list<ContractLineItem>();
    for (Integer i = 0; i < LOTE ; i++ )
    {
      lstOli.add(SObjectInstanceTest.createContractLineItem(lstOpp.get(i).Id, pbe.Id));
    }
    Database.insert(lstOli);

    Aircraft__c air = SObjectInstanceTest.createAircraft();
    Database.insert(air);

    list<String> lstIds = new list<String>();
    list<Related_Aircraft__c> lstRel = new list<Related_Aircraft__c>();
    for (Integer i = 0; i < LOTE ; i++ )
    {
      Related_Aircraft__c rel = SObjectInstanceTest.createRelatedAirCraft(air.Id);
      rel.Contract_Line_Item__c = lstOli.get(i).Id;
      lstIds.add(lstOli.get(i).Id);
      lstRel.add(rel);
    }
    Database.insert(lstRel);

    Test.startTest();
    Database.delete(lstOli);
    Test.stopTest();

    list<Related_Aircraft__c> lstRelResult = [SELECT Id from Related_Aircraft__c WHERE Contract_Line_Item__c =: lstIds];
    system.assert(lstRelResult.isEmpty());
  }
}