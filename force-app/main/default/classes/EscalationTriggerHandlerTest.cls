@isTest
private class EscalationTriggerHandlerTest
{

    
    static testMethod void insertEscalationsTest()
    {   
        
        Product2 productTest = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
        database.insert(productTest);

        PricebookEntry priceBookEntryTest = SObjectInstanceTest.createPricebookEntry(test.getStandardPricebookId(), productTest.Id);
        database.insert(priceBookEntryTest);
        
        Account accountTest = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
        database.insert(accountTest);
        
        List<Agreement__c> agreementsList = new List<Agreement__c>();
        List<Aircraft_Price__c> acPriceList = new List<Aircraft_Price__c>();
        
        for(integer i = 0;i<=5;i++){
            agreementsList.add(SObjectInstanceTest.createApttusAgreement(accountTest.id));
        }
        database.insert(agreementsList);
        
        for(integer i = 0;i<=5;i++){
            acPriceList.add(SObjectInstanceTest.createAircraftPrice(agreementsList[i].Id, productTest.Id));
        }
        database.insert(acPriceList);
        
        test.startTest();
        
            //insert agreementsList;
        	//insert acPriceList;
        
            list<Agreement_Aircraft__c> agreementAircraftList = new list<Agreement_Aircraft__c>();
   
            integer acCounter = 1;
        	integer agCounter = 0;
        
            for(integer i = 0;i<=240;i++)
            {
                if(acCounter >= 48){
                    acCounter = 1;
                    agCounter++;
                }
                    
                Agreement_Aircraft__c agreementAircraft = SObjectInstanceTest.createApttusAgreementLineItem(productTest.Id, agreementsList[agCounter].Id, 'Firm', acPriceList[agCounter].Id);
                Agreement_Aircraft__c agreementAircraftRelated = SObjectInstanceTest.createApttusAgreementLineItem(productTest.Id, agreementsList[agCounter].Id, 'Firm', acPriceList[agCounter].Id);
				agreementAircraft.Aircraft_Price__c = acPriceList[0].Id;
                agreementAircraft.Agreement__c = acPriceList[0].Commercial_Agreement__c;
                agreementAircraft.Contractual_Delivery_Month__c = date.today().addMonths(-acCounter);
                agreementAircraftList.add(agreementAircraft);
                acCounter++;
            }
        
            insert agreementAircraftList;
        
        test.stopTest();
        list<Escalation__c> escalationList = new list<Escalation__c>();
        
        for(integer i = 1;i<=48;i++)
        {
            for(Agreement__c agreement : agreementsList)
            {
                Escalation__c escalation = SObjectInstanceTest.createEscalation(agreement.Id);
                escalation.Rate_Effective_Date__c = date.today().addMonths(-i);   
                escalationList.add(escalation);
            }
        }
        
        insert escalationList;
       
        escalationList[0].Rate_Effective_Date__c = date.newInstance(2016, 10, 01);
        update escalationList[0];
        
        delete escalationList[0];
        
        undelete escalationList[0];
        
        EscalationTriggerHandler handler = new EscalationTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
        
        
       /*
        test.startTest();
            database.insert(new List<Escalation__c>{escalation1,escalation2,escalation3});
            
            //assert to check if the relationship beetwen AgreementLineItem and Escalation is correct ON INSERT
            system.assert([SELECT Escalation__c FROM Agreement_Aircraft__c WHERE ID =: agreementAircraft1.id].Escalation__c != null);
            system.assert([SELECT Escalation__c FROM Agreement_Aircraft__c WHERE ID =: agreementAircraft2.id].Escalation__c != null);
            system.assert([SELECT Escalation__c FROM Agreement_Aircraft__c WHERE ID =: agreementAircraft3.id].Escalation__c != null);        
            
            //trigger must be fire just if Rate_Effective_Date__c field changes
            escalation1.Rate_Effective_Date__c = date.newInstance(2016, 10, 01);
            escalation2.Rate_Effective_Date__c = date.newInstance(2016, 02, 01);
            database.update(new List<Escalation__c>{escalation1,escalation2,escalation3});
        
            system.debug('SQL1 >>> ' + [SELECT name, Contractual_Delivery_Month__c FROM Agreement_Aircraft__c]);
            system.debug('SQL2 >>> ' + [SELECT name, Rate_Effective_Date__c FROM Escalation__c]);
            
            //assert to check if the relationship beetwen AgreementLineItem and Escalation is correct ON UPDATE
            system.assert([SELECT Escalation__c FROM Agreement_Aircraft__c WHERE ID =: agreementAircraft3.id].Escalation__c != null);
            system.assert([SELECT Escalation__c FROM Agreement_Aircraft__c WHERE ID =: agreementAircraft2.id].Escalation__c == null);
        test.stopTest();
        */
    }
}