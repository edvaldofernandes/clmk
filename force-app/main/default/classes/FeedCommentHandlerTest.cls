@isTest
private class FeedCommentHandlerTest {

    static FeedComment loadData() {
        
        FeedComment comment = new FeedComment();
		comment.CommentBody = 'Comentario';
		comment.FeedItemId = [SELECT Id FROM FeedItem WHERE ParentId = :[SELECT Id FROM CollaborationGroup WHERE Name like '%QMI -%' LIMIT 1] LIMIT 1].Id;
        
        return comment;
    }
    
    static FeedComment loadData2() {
        
        FeedComment comment = loadData();
		comment.CommentBody = 'Comentario 2';		
        
        return comment;
    }            
    
    @isTest(seeAllData = true)
    static void insertMultipleComments() {		
		
        FeedComment comment = loadData();
        FeedComment comment2 = loadData2();
        
        List<FeedComment> comments = new List<FeedComment>();
        comments.add(comment);
        comments.add(comment2);
        
        Test.startTest();
        
        List<Database.SaveResult> result = Database.insert(comments, false);

        Test.stopTest();

        System.assert(result.get(0).isSuccess());
                
    }
    
    @isTest(seeAllData = true)
    static void insertComment() {		
		
        FeedComment comment = loadData();
        
        Test.startTest();
        
        Database.SaveResult result = Database.insert(comment, false);

        Test.stopTest();

        System.assert(result.isSuccess());
                
    }
    
   	@isTest(seeAllData = true)
    static void testBody() {
        
        FeedComment comment = loadData();                        
        
        User myUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        System.runAs(myUser){
        
        	insert comment;                
            System.debug('Heitooor... ' + comment);
        }
                
        
        FeedComment fc = [SELECT Id, FeedItemId, RelatedRecordId, CommentBody, CreatedDate, CreatedById
                  			FROM FeedComment
                  			WHERE Id = :comment.Id
                         	LIMIT 1];
        
        Test.startTest();
        
        String body = FeedCommentHandler.buildBody(fc);
        
        Test.stopTest();
		
		System.assertNotEquals('', body);
    }
    
    @isTest(seeAllData = true)
    static void testCallWS() {
        
        Transaction_Delivery__c td = [ SELECT Name, URL__c, Authorization__c
										FROM Transaction_Delivery__c
										WHERE Name = 'CPSFeedback'
										LIMIT 1];
        
        Test.startTest();
        
        
        String body = 'asd';
        
        Test.setMock( HttpCalloutMock.class , new CPSMockServer() );
        FeedCommentHandler.callWS( td.URL__c , td.Authorization__c , body);
        
        Test.stopTest();
		
    }
}