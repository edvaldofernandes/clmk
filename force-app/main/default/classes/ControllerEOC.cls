public class ControllerEOC {
    public ControllerEOC(ApexPages.StandardController controller){
        CampaignMember members = [SELECT Functions_attending_EOCWW15__c,CampaignId FROM CampaignMember WHERE Id = :controller.getID() ];
		eventParticipate = members.CampaignId;
        Functions_attending = new Map<String, Boolean>();
        Functions_attending.put('Special Dinner', false);
        Functions_attending.put('MCW and Side Events Welcome Dinner', false);
        Functions_attending.put('Welcome Cocktail', false);
        
        for(String FAtt : members.Functions_attending_EOCWW15__c.split(';')){
            if(Functions_attending.containsKey(FAtt))
            	Functions_attending.put(FAtt,true);
            
            if(FAtt.contains('Day1'))
				optnsD1Selected = FAtt;
               
            if(FAtt.contains('Day2'))
				optnsD2Selected = FAtt;
            
            if(FAtt.contains('customers'))
                sideEvent = FAtt;
            
            if(FAtt.contains('MMEL'))
                sideEvent = FAtt;
            
            if(FAtt.contains('Training Workshop'))
                sideEvent = FAtt;
        }          
    }

    public Map<String, Boolean> Functions_attending {get;set;}    
    public String optnsD1Selected {get; set;}    
    public String optnsD2Selected {get; set;}
    public String sideEvent {get;set;}
    public String eventParticipate {get;set;}
}