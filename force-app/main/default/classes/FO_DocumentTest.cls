@isTest
public class FO_DocumentTest {
    public static ContentVersion generateContentVersion(Id recordId){
        Blob loremIpsumBlob = EncodingUtil.base64Decode('Lorem Ipsum');
        ContentVersion conVer = new ContentVersion(
            ContentLocation = 'S',
            PathOnClient = 'lorem_ipsum',
            Title = 'Lorem Ipsum',   
            VersionData = loremIpsumBlob
        );
        insert conVer;        
        Id documentId = [
            SELECT
                ContentDocumentId
            FROM
                ContentVersion
            WHERE
                Id =:conVer.Id
        ].ContentDocumentId;
        
        
        ContentDocumentLink cDe = new ContentDocumentLink(
            ContentDocumentId = documentId,
            LinkedEntityId = recordId,
            ShareType = 'I',
            Visibility = 'AllUsers'        
        );
        insert cDe;
        
        return [
            SELECT
            	Id,
				ContentLocation,
            	ContentDocumentId,
            	PathOnClient,
            	Title,
            	VersionData
            FROM
            	ContentVersion
            WHERE 
            	Id =: conVer.Id
        ];
    }
      
    @isTest static void testCanCreateTemporaryFile(){
        EOD__c eod = new FO_EODTestDataBuilder().build();
        Id recordId = eod.Id;
        ContentVersion cv = generateContentVersion(recordId);
        
        Id documentId = cv.ContentDocumentId;
        
        FO_Document document = new FO_DocumentTestDataBuilder()
            .withAssociatedDocuments(new List<Id>{documentId})
            .build();
        
        Blob temporaryFileContent = EncodingUtil.base64Decode('Lorem Ipsum');
       	FO_Document.insertTemporaryFile(temporaryFileContent, documentId);
        
        List<Attachment> temporaryFiles = [
            SELECT
            	Id
            FROM 
            	Attachment
            WHERE
            	Name =: '[TEMP]_' + documentId + '_0'
        ];
       	System.assert( !temporaryFiles.isEmpty() );
        
    }
    @isTest static void testCanBuildContent(){
		FO_Document document = new FO_DocumentTestDataBuilder().build();
        document.build();
        Blob content = document.getContent();
        System.assert(content != null);
    }
    @isTest static void testCanBuildFilenameProperly(){
        String namePattern = FO_Document.TEMPORARY_FILE_NAME_PATTERN;
        Case c = new Case();
        insert c;
        Id recordId = c.Id;
        
        ContentVersion cv = generateContentVersion(recordId);
        Id documentId = cv.ContentDocumentId;        
        String correctName = String.format(namePattern, new List<String>{documentId, '0'});
        
        System.assertEquals(correctName, FO_Document.buildTemporaryFilename(documentId, 0));
    }
    @isTest static void testCanAttachDocumentToRecord(){
        Case c = new Case();
        insert c;
        Id recordId = c.Id;
        FO_Document document = new FO_DocumentTestDataBuilder()
            .withFilename('Lorem Ipsum')
            .withTitle('Lorem Ipsum')
            .build();
        
        document.build();
        document.attachToRecord(recordId);
        
    }
    @isTest static void testCanDeleteTemporaryFile(){
        Blob loremIpsumBlob = EncodingUtil.base64Decode('Lorem Ipsum');
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion cv = generateContentVersion(eod.Id);
        Id documentId = cv.ContentDocumentId;        
        String filename = String.format(
            FO_Document.TEMPORARY_FILE_NAME_PATTERN,
            new List<String>{documentId, '0'}
        );
        
        Attachment attachment = new Attachment(
        	Name = filename,
            ContentType = 'image/png',
         	ParentId = eod.Id,          
            Body = loremIpsumBlob
        );     
      	insert attachment;
        
        FO_Document document = new FO_DocumentTestDataBuilder()
            .withAssociatedDocuments(new List<Id>{documentId})
            .build();
        document.deleteAttachmentsTemporaryFiles();
        
        
        
        String expression = String.format(
			FO_Document.TEMPORARY_FILE_NAME_PATTERN,
            new List<String>{'%', '%'}
        );
        List<Attachment> temporaryFiles = [
            SELECT
            	Id
            FROM 
            	Attachment
            WHERE
            	Name LIKE :expression
        ];
       	System.assert( temporaryFiles.isEmpty() );        
        
    }
    @isTest static void testCanGetTemporaryFile(){
        
        Blob loremIpsumBlob = EncodingUtil.base64Decode('Lorem Ipsum');
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion cv = generateContentVersion(eod.Id);
        Id documentId = cv.ContentDocumentId;        
        String filename = String.format(
            FO_Document.TEMPORARY_FILE_NAME_PATTERN,
            new List<String>{documentId, '0'}
        );
        
        Attachment attachment = new Attachment(
        	Name = filename,
            ContentType = 'image/png',
         	ParentId = eod.Id,          
            Body = loremIpsumBlob
        );     
      	insert attachment;
        Map<String, List<Attachment>> temporaryFiles = FO_Document.getAttachmentsTemporaryFiles(new List<Id>{documentId});

        System.assertEquals(attachment.Id, temporaryFiles.get(documentId).get(0).Id);
    }
    @isTest static void testCanGetImages(){
        Blob loremIpsumBlob = EncodingUtil.base64Decode('Lorem Ipsum');
        EOD__c eod = new FO_EODTestDataBuilder().build();
        ContentVersion cv = generateContentVersion(eod.Id);
        Id documentId = cv.ContentDocumentId;        
        String filename = String.format(
            FO_Document.TEMPORARY_FILE_NAME_PATTERN,
            new List<String>{documentId, '0'}
        );
        
        Attachment attachment = new Attachment(
        	Name = filename,
            ContentType = 'image/png',
         	ParentId = eod.Id,          
            Body = loremIpsumBlob
        );     
      	insert attachment;
        Map<String, List<Attachment>> temporaryFiles = FO_Document.getAttachmentsImages(new List<Id>{documentId});
        //Map<String, List<Attachment>> temporaryFiles = FO_Document.getAttachmentsTemporaryFiles(new List<Id>{documentId});

        System.assertEquals(attachment.Id, temporaryFiles.get('Lorem Ipsum').get(0).Id);
    }
    @isTest static void testIfPagesAreSorted(){
        Map<Integer, Attachment> pagesMap = new Map<Integer, Attachment>{
            9 => new Attachment(Name='9'),
            8 => new Attachment(Name='8'),
            7 => new Attachment(Name='7'),
            6 => new Attachment(Name='6'),
            5 => new Attachment(Name='5'),
            4 => new Attachment(Name='4'),
            3 => new Attachment(Name='3'),
            2 => new Attachment(Name='2'),
            1 => new Attachment(Name='1')
        };
        List<Attachment> sortedPages = FO_Document.sortPages(pagesMap);
        for(Integer i = 0; i < 9; i++){
            Attachment page = sortedPages.get(i);
            System.assertEquals(i+1, Integer.valueOf(page.Name));
        }
        
    }
}