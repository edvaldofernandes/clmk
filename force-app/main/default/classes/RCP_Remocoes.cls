global class RCP_Remocoes {
    
    webservice Integer numeroSerieAeronave;
    webservice String partNumberNoSerieOn;
    webservice String partNumberNoSerieOff;
    webservice String partNumber;
    webservice String codPnSubtt;
    webservice String codigoProjeto;
    webservice DateTime dataEventRemocoes;
	webservice String razaoRemocao;
    webservice String classificacao;
    
    public RCP_Remocoes(){}
}