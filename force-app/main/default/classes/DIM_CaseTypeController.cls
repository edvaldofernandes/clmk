public class DIM_CaseTypeController {

    @AuraEnabled
    public static Case getCaseRecordType(String caseId) {
        return [SELECT RecordType.Name, RecordType.Description FROM Case WHERE Id =: caseId LIMIT 1];
    }
}