@isTest
public class NewAgreementControllerTest {
	
    static testMethod void createAgreementTest(){
    	
        Account testAcc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(testAcc);
        
        Opportunity testOpp = SObjectInstanceTest.createOpportunity();
        database.insert(testOpp);
        
        Kickoff__c testKickoff = new Kickoff__c();
        testKickoff.Opportunity__c = testOpp.Id;
        testKickoff.Name = 'Test Kickoff 1';
        database.insert(testKickoff);
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testOpp.Id);
        
        NewAgreementController vc = new NewAgreementController();
        vc.saveAgreement();
        
        List<Agreement__c> testAgreements = [SELECT Id, Related_Opportunity__c FROM Agreement__c WHERE Related_Opportunity__r.Id =: vc.sourceOpportunity.Id];
        System.assertEquals(1,testAgreements.size());
    }
}