@IsTest 
public with sharing class CLMSECPRReportControllerTest
{    
        static testMethod void unitTest1()
        {
            
            Test.startTest();        

            product2 aircraft = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
            insert aircraft;
       
            PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(Test.getStandardPricebookId(), aircraft.Id);
            insert pbe;  
        
            Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
            insert acc;
            
            Agreement__c agreement = new Agreement__c();
            agreement.Name = 'Test 1';
            agreement.Account__c = acc.Id;
            agreement.Country__c = 'Iceland';
            agreement.Agreement_Code__c = 'ABCDEFG';
            agreement.Region_Code__c = 'RegionCODE';
            agreement.Status_Category__c = 'PA Signed';
            agreement.RecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
            agreement.Company_Signed_Date__c = Date.today(); 
            agreement.Kickoff_trigger_already_executed__c = false; 
            agreement.Admin_Mode__c = 'true';
            agreement.Agreement_Color__c = '0000FF';
            insert agreement;
        
            Aircraft_Price__c aircraftPrice = new Aircraft_Price__c();
            aircraftPrice.Model__c = aircraft.Id;
            aircraftPrice.Aircraft_Version__c = 'IGW';
            aircraftPrice.Economic_Condition__c = '132131';
            aircraftPrice.Commercial_Agreement__c = agreement.Id;
            aircraftPrice.Aircraft_List_Price__c = 32131231.00;
            aircraftPrice.Aircraft_Basic_Price__c = 32141212321.00;
            insert aircraftPrice;
                
            Agreement_Aircraft__c contractAircraft = new Agreement_Aircraft__c();
            contractAircraft.Aircraft__c  = aircraft.Id;
            contractAircraft.Delivery_Date__c = Date.today();
            contractAircraft.Contract_Aircraft_Number__c = 5;
            contractAircraft.Order_Type__c = 'Firm';
            contractAircraft.Status__c = 'Planned';
            contractAircraft.Basic_Price__c = 200;
            contractAircraft.Aircraft_Configuration__c = 'AK';
            contractAircraft.Agreement__c = agreement.Id;
            contractAircraft.AFA__c = Date.today();
            contractAircraft.Skyline_Summary_Calculated__c = 'Firms E1';
            contractAircraft.PRODUCTION_SEC_SCORE__C = 100;
            contractAircraft.Contractual_Delivery_Month__c = Date.today();
            contractAircraft.MFA_Score__c = 8.0;
            contractAircraft.Actual_Delivery_Date__c = Date.today();
            contractAircraft.Delivery_SEC_Score__c = 90.0;
            contractAircraft.TREND_Delivery_Date__c = Date.today();
            contractAircraft.Aircraft_Price__c = aircraftPrice.id;
            insert contractAircraft;
            
            Agreement_Aircraft__c contractAircraft2 = new Agreement_Aircraft__c();
            contractAircraft2.Aircraft__c  = aircraft.Id;
            contractAircraft2.Delivery_Date__c = Date.today();
            contractAircraft2.Contract_Aircraft_Number__c = 5;
            contractAircraft2.Status__c = 'Planned';
            contractAircraft2.Order_Type__c = 'Firm';
            contractAircraft2.Basic_Price__c = 200;
            contractAircraft2.Aircraft_Configuration__c = 'AK';
            contractAircraft2.Agreement__c = agreement.Id;
            contractAircraft2.AFA__c = Date.today().addMonths(-1);
            contractAircraft2.Skyline_Summary_Calculated__c = 'Firms E2';
            contractAircraft2.PRODUCTION_SEC_SCORE__C  = 100;
            contractAircraft2.Contractual_Delivery_Month__c = Date.today();
            contractAircraft2.MFA_Score__c = 8.0;
            contractAircraft2.Actual_Delivery_Date__c = Date.today();
            contractAircraft2.Delivery_SEC_Score__c = 90.0;
            contractAircraft2.TREND_Delivery_Date__c = Date.today();
            contractAircraft2.Aircraft_Price__c = aircraftPrice.id;
            insert contractAircraft2;
            
            DCTReportsSettings__c goals = new DCTReportsSettings__c();
            goals.name = string.valueOf(date.today().year());
            goals.JanuaryGoal__c= 100;
            goals.FebruaryGoal__c= 200;
            goals.MarchGoal__c= 300;
            goals.AprilGoal__c= 400;
            goals.MayGoal__c= 500;
            goals.JuneGoal__c= 600;
            goals.JulyGoal__c= 700;
            goals.AugustGoal__c= 800;
            goals.SeptemberGoal__c= 900;
            goals.OctoberGoal__c= 1000;
            goals.NovemberGoal__c= 1100;
            goals.DecemberGoal__c= 1200;
            insert goals;
            
            agreement.Status_Category__c = 'PA Signed';
            update agreement;

            List<CLMSECPRReportController.ChartDataWrapper> loadChartDataRemoting = CLMSECPRReportController.loadChartData(string.valueOf(date.today().year()));
            //System.assertEquals(string.valueOf(date.today().year()), '2017');
            CLMSECPRReportController.ChartDataWrapper innerClass = new CLMSECPRReportController.ChartDataWrapper('Teste',double.valueOf('10.0'),double.valueOf('2.0'),double.valueOf('8.0'),1) ;
            
            Test.stopTest();
            
            PageReference pageRef = Page.CLMSECPRReport;
            
            CLMSECPRReportController controller = new CLMSECPRReportController(); 
        
            Test.setCurrentPage(pageRef);
            
            controller.changeSettings();
            controller.updateParameters();
            controller.loadChart();
            
            List<SelectOption> yearsI = controller.getFilterInitialYears();
            String filterInitialYear = CLMSECPRReportController.filterInitialYear;
            list<string> seriesNames = controller.seriesNames;
            
            //CLMMFAReportController.ChartDataWrapper innerClass = new CLMMFAReportController.ChartDataWrapper('Teste',100,new List<Integer>{150},1,new List<String>{'teste Rodrigo'} ) ;
        }
}