public class AutoNamingRules {
   
    public static void autoNamingFromAircraft(Map<Id,Agreement_Aircraft__c> oldRM, 
        List<Agreement_Aircraft__c> newR, Boolean isUpdate){
        Boolean isFuture = false;
        Map<Id,Agreement_Aircraft__c> oldRecordsMap = new Map<Id,Agreement_Aircraft__c>(); 
        List<Agreement_Aircraft__c> newRecords = new List<Agreement_Aircraft__c>();
        //boolean isExecuting = false;

        oldRecordsMap = oldRM;
        newRecords    = newR;
        //String s = '';
        Set<Id> agreementIdSet = new Set<Id>();
        
        for(Agreement_Aircraft__c agreementAircraft : newRecords){
            
            //Select only agreements with changed aircrafts or new aircrafts 
            if(isUpdate){
                Boolean productChanged = agreementAircraft.Aircraft__c != oldRecordsMap.get(agreementAircraft.Id).Aircraft__c;
                Boolean configChanged = agreementAircraft.Aircraft_Configuration__c != oldRecordsMap.get(agreementAircraft.Id).Aircraft_Configuration__c;
                Boolean trendDateChanged = agreementAircraft.TREND_Delivery_Date__c != oldRecordsMap.get(agreementAircraft.Id).TREND_Delivery_Date__c;
                Boolean deliveryDateChanged = agreementAircraft.Contractual_Delivery_Month__c != oldRecordsMap.get(agreementAircraft.Id).Contractual_Delivery_Month__c;
                Boolean orderTypeChanged = agreementAircraft.Order_Type__c != oldRecordsMap.get(agreementAircraft.Id).Order_Type__c;

                if( configChanged || trendDateChanged ||  deliveryDateChanged || productChanged || orderTypeChanged )
                agreementIdSet.add (agreementAircraft.Agreement__c);
            }
            else
                agreementIdSet.add (agreementAircraft.Agreement__c);
        }
        if (!agreementIdSet.isEmpty()){
            List<CLMAircraftToRename__c> agrList = new List<CLMAircraftToRename__c>();
            Set<Id> idList = getAgreementToUpdate();
            for (Id agrId : agreementIdSet){
                if (!idList.contains(agrId)){
                    CLMAircraftToRename__c atr = new CLMAircraftToRename__c();
                    atr.Commercial_Agreement_ID__c = agrId;
                    agrList.add(atr);
                }
            }
            if (!agrList.isEmpty()){
                for(Agreement__c isFutureAgreement : [SELECT id, Future_Method__c FROM Agreement__c WHERE Id IN :agreementIdSet]){
                    if(isFutureAgreement.Future_Method__c) isFuture = true;
                }
                database.insert(agrList);
                if(isFuture) setNames();
                else futureSetNames();
            }
        }                 
        //if (!agreementIdSet.isEmpty()){
        //	setNames(agreementIdSet);
		//}
        
    }
    
    public static void autoNamingFromAgreement(Map<Id,Agreement__c> oldRM, List<Agreement__c> newR){
        Boolean isFuture = false;
        Set<Id> agreementIdSet = new Set<Id>();
        for(Agreement__c agr : newR){
            Agreement__c oldAgr = oldRM.get(agr.Id);
            Id pARecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByDeveloperName().get('Purchase_Agreement').getRecordTypeId();
            if (oldAgr==null) continue;
            /*if (agr.Region_Code__c    != oldAgr.Region_Code__c || 
                agr.Agreement_Code__c != oldAgr.Agreement_Code__c){*/
            if ((agr.Region_Code__c    != oldAgr.Region_Code__c || 
                agr.Agreement_Code__c != oldAgr.Agreement_Code__c) &&
                agr.RecordTypeId==pARecordTypeId 
               ){
                agreementIdSet.add(agr.Id);
                if(agr.Future_Method__c) isFuture = true;
            }
        }
        if (!agreementIdSet.isEmpty()){
            List<CLMAircraftToRename__c> agrList = new List<CLMAircraftToRename__c>();
            Set<Id> idList = getAgreementToUpdate();
            for (Id agrId : agreementIdSet){
                if (!idList.contains(agrId)){
                    CLMAircraftToRename__c atr = new CLMAircraftToRename__c();
                    atr.Commercial_Agreement_ID__c = agrId;
                    agrList.add(atr);
                }
            }
            system.debug('agrList: '+agrList);
            if (!agrList.isEmpty()){
                database.insert(agrList);
				System.debug('isFuture: '+ isFuture);
                if(isFuture) setNames();
                else futureSetNames();
            }
        }   
        //if (!agreementIdSet.isEmpty()){
        //	setNames(agreementIdSet);
		//}
    }
    
   
    @future
	public static void futureSetNames(){
        system.debug('setName');
        List<CLMAircraftToRename__c> atrList = [SELECT Id, Commercial_Agreement_ID__c FROM CLMAircraftToRename__c];
        Set<String> agreementIdSet = new Set<String>();
        for (CLMAircraftToRename__c atr : atrList){
            agreementIdSet.add(atr.Commercial_Agreement_ID__c);
        }
        if(agreementIdSet.isEmpty())
            return;
        database.delete(new List<Id>(new Map<Id, CLMAircraftToRename__c>(atrList).keySet()));
        system.debug('setName exec');
        Map<Date,Agreement_Aircraft__c> contractualDateMap = new Map<Date,Agreement_Aircraft__c>();
		Map<Date,Agreement_Aircraft__c> trendDateMap = new Map<Date,Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> aircraftByContractualDate = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> returnLisTContract = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> returnListTrend = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> aircraftByTrendDate = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> orderingList = new List<Agreement_Aircraft__c>();
		String lastAircraftFamily = '';
        //Select all aircrafts from the agreements selected before
        aircraftByContractualDate = [SELECT Id, Aircraft__r.name, Aircraft__r.Aircraft_Family__c, Aircraft__r.DF_Grouping__c, Aircraft_Configuration__c, Order_Type__c,
                                Agreement__c, Agreement__r.RecordType.name, Agreement__r.Status_Category__c, Agreement__r.Agreement_Code__c,
                                DF_Code__c, DF_Country_Code__c, TREND_DF_Code__c, TREND_Country_Code__c, TREND_Delivery_Date__c,
                           		Agreement__r.Region_Code__c FROM Agreement_Aircraft__c WHERE Agreement__c IN :agreementIdSet
                      			ORDER BY Agreement__c, Aircraft__r.Aircraft_Family__c, Contractual_Delivery_Month__c ASC, Aircraft__r.Name];
		System.debug(aircraftByContractualDate);
        returnLisTContract = setIfDFOrTrend(aircraftByContractualDate,false);
        if(returnLisTContract.size()>=1)
        	database.update(returnLisTContract);
        
        aircraftByTrendDate = [SELECT Id, Aircraft__r.name, Aircraft__r.Aircraft_Family__c, Aircraft__r.DF_Grouping__c, Aircraft_Configuration__c, Order_Type__c,
                                Agreement__c, Agreement__r.RecordType.name, Agreement__r.Status_Category__c, Agreement__r.Agreement_Code__c,
                                DF_Code__c, DF_Country_Code__c, TREND_DF_Code__c, TREND_Country_Code__c, TREND_Delivery_Date__c,
                           		Agreement__r.Region_Code__c FROM Agreement_Aircraft__c WHERE Agreement__c IN :agreementIdSet
                      			ORDER BY Agreement__c, Aircraft__r.Aircraft_Family__c, TREND_Delivery_Date__c ASC, Aircraft__r.Name];
		System.debug(aircraftByTrendDate);
        returnListTrend = setIfDFOrTrend(aircraftByTrendDate,true);
        if(returnListTrend.size()>=1)
        	database.update(returnListTrend);
    }
    
    public static void setNames(){
        system.debug('setName');
        List<CLMAircraftToRename__c> atrList = [SELECT Id, Commercial_Agreement_ID__c FROM CLMAircraftToRename__c];
        Set<String> agreementIdSet = new Set<String>();
        for (CLMAircraftToRename__c atr : atrList){
            agreementIdSet.add(atr.Commercial_Agreement_ID__c);
        }
        if(agreementIdSet.isEmpty())
            return;
        database.delete(new List<Id>(new Map<Id, CLMAircraftToRename__c>(atrList).keySet()));
        system.debug('setName exec');
        Map<Date,Agreement_Aircraft__c> contractualDateMap = new Map<Date,Agreement_Aircraft__c>();
		Map<Date,Agreement_Aircraft__c> trendDateMap = new Map<Date,Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> aircraftByContractualDate = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> returnLisTContract = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> returnListTrend = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> aircraftByTrendDate = new List<Agreement_Aircraft__c>();
        List<Agreement_Aircraft__c> orderingList = new List<Agreement_Aircraft__c>();
		String lastAircraftFamily = '';
        //Select all aircrafts from the agreements selected before
        aircraftByContractualDate = [SELECT Id, Aircraft__r.name, Aircraft__r.Aircraft_Family__c, Aircraft__r.DF_Grouping__c, Aircraft_Configuration__c, Order_Type__c,
                                Agreement__c, Agreement__r.RecordType.name, Agreement__r.Status_Category__c, Agreement__r.Agreement_Code__c,
                                DF_Code__c, DF_Country_Code__c, TREND_DF_Code__c, TREND_Country_Code__c, TREND_Delivery_Date__c,
                           		Agreement__r.Region_Code__c FROM Agreement_Aircraft__c WHERE Agreement__c IN :agreementIdSet
                      			ORDER BY Agreement__c, Aircraft__r.Aircraft_Family__c, Contractual_Delivery_Month__c ASC, Aircraft__r.Name];
		System.debug(aircraftByContractualDate);
        returnLisTContract = setIfDFOrTrend(aircraftByContractualDate,false);
        if(returnLisTContract.size()>=1)
        	database.update(returnLisTContract);
        
        aircraftByTrendDate = [SELECT Id, Aircraft__r.name, Aircraft__r.Aircraft_Family__c, Aircraft__r.DF_Grouping__c, Aircraft_Configuration__c, Order_Type__c,
                                Agreement__c, Agreement__r.RecordType.name, Agreement__r.Status_Category__c, Agreement__r.Agreement_Code__c,
                                DF_Code__c, DF_Country_Code__c, TREND_DF_Code__c, TREND_Country_Code__c, TREND_Delivery_Date__c,
                           		Agreement__r.Region_Code__c FROM Agreement_Aircraft__c WHERE Agreement__c IN :agreementIdSet
                      			ORDER BY Agreement__c, Aircraft__r.Aircraft_Family__c, TREND_Delivery_Date__c ASC, Aircraft__r.Name];
		System.debug(aircraftByTrendDate);
        returnListTrend = setIfDFOrTrend(aircraftByTrendDate,true);
        if(returnListTrend.size()>=1)
        	database.update(returnListTrend);
    }

    private static List<Agreement_Aircraft__c> setIfDFOrTrend(List<Agreement_Aircraft__c> aircraftByContractualDate, Boolean isTrend){
        Integer counter       = 0;
        String  stringCounter = '';
        String  dfCode        = '';
        String  rgCode        = '';
		String 	agreementId   = '';
        String  lastFamily    = '';
        
        Map<String, Integer> aircraftConfig   = new Map<String, Integer>();
        Map<String, Integer> agreementCounter = new Map<String, Integer>();
        Map<String, String>  dfCodeMap        = new Map<String, String>();
        Map<String, String>  rgCodeMap        = new Map<String, String>();

        for(Agreement_Aircraft__c agreementAircraft : aircraftByContractualDate){
            if(agreementAircraft.Agreement__c != null)
                agreementId = String.valueof(agreementAircraft.Agreement__c);
            else
                agreementId = null;

            //Aircraft counter by agreement
            counter = agreementCounter.get(agreementId);
            if (counter == null || lastFamily != agreementAircraft.Aircraft__r.Aircraft_Family__c){
                counter = 1;
                lastFamily = agreementAircraft.Aircraft__r.Aircraft_Family__c;
            } else {
                counter++;
            }
            agreementCounter.put(agreementId, counter);

            //Set substrings for DF and Region Code for each agreement if it is not defined yet
            rgCode = rgCodeMap.get(agreementId);
            dfCode = dfCodeMap.get(agreementId);
            if(rgCode==null){
                if(agreementAircraft.Agreement__r.Agreement_Code__c == null || agreementAircraft.Agreement__r.Agreement_Code__c == ''){
                    agreementAircraft.Agreement__r.Agreement_Code__c = 'EMB';
                } 
                dfCode = (agreementAircraft.Agreement__r.Agreement_Code__c.length() >= 4 ? agreementAircraft.Agreement__r.Agreement_Code__c.substring(0,4) + '-' : agreementAircraft.Agreement__r.Agreement_Code__c + '-');
                if(agreementAircraft.Agreement__r.Region_Code__c == null || agreementAircraft.Agreement__r.Region_Code__c == ''){
                    rgCode = 'REG-';
                    agreementAircraft.Agreement__r.Region_Code__c = 'REG';
                } else{
                    rgCode = (agreementAircraft.Agreement__r.Region_Code__c.length() >= 4 ? agreementAircraft.Agreement__r.Region_Code__c.substring(0,4) + '-' : agreementAircraft.Agreement__r.Region_Code__c + '-');
                }
                dfCodeMap.put(agreementId, dfCode);
                rgCodeMap.put(agreementId, rgCode);
            }
        
            //Adding counter value to the variables 
            stringCounter = String.valueOf(counter);
            dfCode += (stringCounter.length() < 2 ? '0' : '') + stringCounter;
            rgCode += (stringCounter.length() < 2 ? '0' : '') + stringCounter;

            //Only add second counter if aircraft has "Aircraft_Configuration__c"
            if (agreementAircraft.Aircraft_Configuration__c != null){

                //Aircraft second counter by configuration
                counter = aircraftConfig.get(agreementId + ' ' + agreementAircraft.Aircraft_Configuration__c);
                if (counter == null || lastFamily != agreementAircraft.Aircraft__r.Aircraft_Family__c){
                    counter = 1;
                    lastFamily = agreementAircraft.Aircraft__r.Aircraft_Family__c;
                } else {
                    counter++;
                }
                aircraftConfig.put(agreementId + ' ' + agreementAircraft.Aircraft_Configuration__c, counter);

                //Adding second counter value to the variables
                stringCounter = String.valueOf(counter);
                dfCode += '(' +  (agreementAircraft.Aircraft_Configuration__c.length() >= 2 ? agreementAircraft.Aircraft_Configuration__c.substring(0,2) + '-' : agreementAircraft.Aircraft_Configuration__c + '-') 
                       + (stringCounter.length() < 2 ? '0' : '') + stringCounter + ')';
                rgCode += '(' + (agreementAircraft.Aircraft_Configuration__c.length() >= 2 ? agreementAircraft.Aircraft_Configuration__c.substring(0,2) + '-' : agreementAircraft.Aircraft_Configuration__c + '-') 
                       + (stringCounter.length() < 2 ? '0' : '') + stringCounter + ')';
                }

            //Add Optional info
            if(agreementAircraft.Order_Type__c != null && agreementAircraft.Order_Type__c.equalsIgnoreCase('Option')){
                dfCode += ' OP';
                rgCode += ' OP';
            }
            
            if(!isTrend){
                agreementAircraft.DF_Code__c = dfCode;
                agreementAircraft.DF_Country_Code__c = rgCode;
            }else{
                agreementAircraft.TREND_DF_Code__c = dfCode;
                agreementAircraft.TREND_Country_Code__c = rgCode; 
            }  
        }
        return aircraftByContractualDate;
    }
    
    public static Set<Id> getAgreementToUpdate(){
        List<CLMAircraftToRename__c> atrList = [SELECT Id, Commercial_Agreement_ID__c FROM CLMAircraftToRename__c];
        Set<Id> idSet = new Set<Id>();
        for(CLMAircraftToRename__c iditem : atrList) idSet.add(iditem.Commercial_Agreement_ID__c);
        return idSet;
    }
    

}