@isTest
private class SpecialInvestigationsTest{
    @isTest static void testIsBatchDuplicate_True(){
        Test.startTest();
        try{
            SpecialInvestigationsTestDataFactory.createSpecialInvestigationDataWithDuplicate();
            system.assert(false, 'Expected an exception, but the cases inserted successfully.');
        }catch(Exception e){
            System.assert(e.getMessage().contains('This case is a duplicate within the insert batch.'));
        }
        Test.stopTest();
    }
    
    @isTest static void testIsBatchDuplicate_False(){
        Test.startTest();
        try{
            SpecialInvestigationsTestDataFactory.createCasesNoDuplicate();
            system.assert(true, 'No error on insertion.');
        }catch(Exception e){
            System.assert(false, 'Error on insert: ' + e.getMessage());
        }
        Test.stopTest();
    }
    
    @isTest static void testIsDatabaseDuplicate_True(){
        Test.startTest();
        try{
            SpecialInvestigationsTestDataFactory.createDuplicatesInSeparateBatches();
            system.assert(false, 'Expected an exception, but the cases inserted successfully.');
        }catch(Exception e){
            System.assert(e.getMessage().contains('This case has a duplicate in the database.'));
        }
        Test.stopTest();
    }
    
    @isTest static void testIsDatabaseDuplicate_False(){
        Test.startTest();
        try{
            SpecialInvestigationsTestDataFactory.createNonDuplicatesInSeparateBatches();
            system.assert(true, 'No error on insertion.');
        }catch(Exception e){
            System.assert(false, 'Error on insert: ' + e.getMessage());
        }
        Test.stopTest();
    }
    
    @isTest static void testPopulateRogueCase(){
        Test.startTest();
        SpecialInvestigationsTestDataFactory.createSpecialInvestigationDataRogue();
        Test.stopTest();
        
        List<Case> rogueCaseResult = [SELECT Reason_for_Requesting_Investigation__c FROM Case LIMIT 1];
        System.assertEquals('Rogue', rogueCaseResult[0].Reason_for_Requesting_Investigation__c);
    }
    
    @isTest static void testPopulateCustomerRequestedLTOWCase(){
        Test.startTest();
        SpecialInvestigationsTestDataFactory.createSpecialInvestigationDataCustomerIdentifiedLTOW();
        Test.stopTest();
        
        List<Case> custLTOWCaseResult = [SELECT Reason_for_Requesting_Investigation__c FROM Case LIMIT 1];
        System.assertEquals('Low Time On Wing;Customer Request', custLTOWCaseResult[0].Reason_for_Requesting_Investigation__c);
    }
    
    @isTest static void testPopulateInvestigationCase(){
        Test.startTest();
        SpecialInvestigationsTestDataFactory.createSpecialInvestigationDataInvestigationItemDetected();
        Test.stopTest();
        
        List<Case> investigationCaseResult = [SELECT Reason_for_Requesting_Investigation__c FROM Case LIMIT 1];
        System.assertEquals('Investigation', investigationCaseResult[0].Reason_for_Requesting_Investigation__c);
    }
    
    @isTest static void testPopulateNewCase_NoTsn(){
        Test.startTest();
        SpecialInvestigationsTestDataFactory.createSpecialInvestigationNoTSNSection();
        Test.stopTest();
        
        List<Case> result = [SELECT Reason_for_Removal__c FROM Case LIMIT 1];
        System.assertEquals('LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1', result[0].Reason_for_Removal__c);	
    }
    
    @isTest static void testPopulateNewCase_NoTsnOrDaysOnWing(){
        Test.startTest();
        SpecialInvestigationsTestDataFactory.createSpecialInvestigationNoTsnOrDaysOnWing();
        Test.stopTest();
        
        List<Case> result = [SELECT Reason_for_Removal__c FROM Case LIMIT 1];
        System.assertEquals('LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1', result[0].Reason_for_Removal__c);	
    }
    
    @isTest static void testPopulateNewCase_NoTsnOrDaysOnWingOrSapHistory(){
        Test.startTest();
        SpecialInvestigationsTestDataFactory.createSpecialInvestigationNoTsnOrDaysOnWingOrSapHistory();
        Test.stopTest();
        
        List<Case> result = [SELECT Reason_for_Removal__c FROM Case LIMIT 1];
        System.assertEquals('LT T12ADS FADEC DISAGREE [E1B] FC 73216724E1', result[0].Reason_for_Removal__c);	
    }
}