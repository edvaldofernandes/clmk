public with sharing class CFController
    {





            public Integer EMEA_1 {get; set;}
            public Integer EMEA_2 {get; set;}
            public Integer EMEA_3 {get; set;}
            public Integer EMEA_4 {get; set;}
            public Integer EMEA_5 {get; set;}
            public Integer EMEA_6 {get; set;}
            public Integer EMEA_7 {get; set;}
            public Integer ASIA_1 {get; set;}
            public Integer ASIA_2 {get; set;}
            public Integer ASIA_3 {get; set;}
            public Integer ASIA_4 {get; set;}
            public Integer ASIA_5 {get; set;}
            public Integer ASIA_6 {get; set;}
            public Integer ASIA_7 {get; set;}
            public Integer NAMER_1 {get; set;}
            public Integer NAMER_2 {get; set;}
            public Integer NAMER_3 {get; set;}
            public Integer NAMER_4 {get; set;}
            public Integer NAMER_5 {get; set;}
            public Integer NAMER_6 {get; set;}
            public Integer NAMER_7 {get; set;}
            public Integer CHINA_1 {get; set;}
            public Integer CHINA_2 {get; set;}
            public Integer CHINA_3 {get; set;}
            public Integer CHINA_4 {get; set;}
            public Integer CHINA_5 {get; set;}
            public Integer CHINA_6 {get; set;}
            public Integer CHINA_7 {get; set;}
            public Integer LATAM_1 {get; set;}
            public Integer LATAM_2 {get; set;}
            public Integer LATAM_3 {get; set;}
            public Integer LATAM_4 {get; set;}
            public Integer LATAM_5 {get; set;}
            public Integer LATAM_6 {get; set;}
            public Integer LATAM_7 {get; set;}



    public CFController(ApexPages.StandardController stdController) {
            EMEA_1 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Prospecting'];
            EMEA_2 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'LOI'];
            EMEA_3 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Credit Analysis'];
            EMEA_4 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Proposal Negotiation'];
            EMEA_5 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Transaction Approval'];
            EMEA_6 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Contract Negotiation'];
            EMEA_7 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Disbursement'];
            ASIA_1 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Prospecting'];
            ASIA_2 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'LOI'];
            ASIA_3 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Credit Analysis'];
            ASIA_4 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Proposal Negotiation'];
            ASIA_5 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Transaction Approval'];
            ASIA_6 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Contract Negotiation'];
            ASIA_7 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Disbursement'];
            NAMER_1 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Prospecting'];
            NAMER_2 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'LOI'];
            NAMER_3 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Credit Analysis'];
            NAMER_4 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Proposal Negotiation'];
            NAMER_5 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Transaction Approval'];
            NAMER_6 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Contract Negotiation'];
            NAMER_7 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Disbursement'];
            CHINA_1 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Prospecting'];
            CHINA_2 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'LOI'];
            CHINA_3 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Credit Analysis'];
            CHINA_4 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Proposal Negotiation'];
            CHINA_5 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Transaction Approval'];
            CHINA_6 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Contract Negotiation'];
            CHINA_7 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Disbursement'];
            LATAM_1 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Prospecting'];
            LATAM_2 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'LOI'];
            LATAM_3 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Credit Analysis'];
            LATAM_4 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Proposal Negotiation'];
            LATAM_5 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Transaction Approval'];
            LATAM_6 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Contract Negotiation'];
            LATAM_7 = [SELECT count() FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Disbursement'];




 

    }




// EMEA------------------------------------------------------------------
// ----------------------------------------------------------------------

List<Customer_Finance_Deal__c> deals_EMEA_1; public List<Customer_Finance_Deal__c> getDeals_EMEA_1()    { if(deals_EMEA_1 == null) deals_EMEA_1 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Prospecting']; return deals_EMEA_1;}
List<Customer_Finance_Deal__c> deals_EMEA_2; public List<Customer_Finance_Deal__c> getDeals_EMEA_2()    { if(deals_EMEA_2 == null) deals_EMEA_2 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'LOI']; return deals_EMEA_2;}
List<Customer_Finance_Deal__c> deals_EMEA_3; public List<Customer_Finance_Deal__c> getDeals_EMEA_3()    { if(deals_EMEA_3 == null) deals_EMEA_3 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Credit Analysis']; return deals_EMEA_3;}
List<Customer_Finance_Deal__c> deals_EMEA_4; public List<Customer_Finance_Deal__c> getDeals_EMEA_4()    { if(deals_EMEA_4 == null) deals_EMEA_4 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Proposal Negotiation']; return deals_EMEA_4;}
List<Customer_Finance_Deal__c> deals_EMEA_5; public List<Customer_Finance_Deal__c> getDeals_EMEA_5()    { if(deals_EMEA_5 == null) deals_EMEA_5 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Transaction Approval']; return deals_EMEA_5;}
List<Customer_Finance_Deal__c> deals_EMEA_6; public List<Customer_Finance_Deal__c> getDeals_EMEA_6()    { if(deals_EMEA_6 == null) deals_EMEA_6 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Contract Negotiation']; return deals_EMEA_6;}
List<Customer_Finance_Deal__c> deals_EMEA_7; public List<Customer_Finance_Deal__c> getDeals_EMEA_7()    { if(deals_EMEA_7 == null) deals_EMEA_7 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='EMEA' and  Deal_Stage__c = 'Disbursement']; return deals_EMEA_7;}


// CHINA------------------------------------------------------------------
// ----------------------------------------------------------------------

List<Customer_Finance_Deal__c> deals_CHINA_1; public List<Customer_Finance_Deal__c> getDeals_CHINA_1()    { if(deals_CHINA_1 == null) deals_CHINA_1 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Prospecting']; return deals_CHINA_1;}
List<Customer_Finance_Deal__c> deals_CHINA_2; public List<Customer_Finance_Deal__c> getDeals_CHINA_2()    { if(deals_CHINA_2 == null) deals_CHINA_2 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'LOI']; return deals_CHINA_2;}
List<Customer_Finance_Deal__c> deals_CHINA_3; public List<Customer_Finance_Deal__c> getDeals_CHINA_3()    { if(deals_CHINA_3 == null) deals_CHINA_3 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Credit Analysis']; return deals_CHINA_3;}
List<Customer_Finance_Deal__c> deals_CHINA_4; public List<Customer_Finance_Deal__c> getDeals_CHINA_4()    { if(deals_CHINA_4 == null) deals_CHINA_4 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Proposal Negotiation']; return deals_CHINA_4;}
List<Customer_Finance_Deal__c> deals_CHINA_5; public List<Customer_Finance_Deal__c> getDeals_CHINA_5()    { if(deals_CHINA_5 == null) deals_CHINA_5 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Transaction Approval']; return deals_CHINA_5;}
List<Customer_Finance_Deal__c> deals_CHINA_6; public List<Customer_Finance_Deal__c> getDeals_CHINA_6()    { if(deals_CHINA_6 == null) deals_CHINA_6 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Contract Negotiation']; return deals_CHINA_6;}
List<Customer_Finance_Deal__c> deals_CHINA_7; public List<Customer_Finance_Deal__c> getDeals_CHINA_7()    { if(deals_CHINA_7 == null) deals_CHINA_7 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='CHINA' and  Deal_Stage__c = 'Disbursement']; return deals_CHINA_7;}

// ASIA------------------------------------------------------------------
// ----------------------------------------------------------------------

List<Customer_Finance_Deal__c> deals_ASIA_1; public List<Customer_Finance_Deal__c> getDeals_ASIA_1()    { if(deals_ASIA_1 == null) deals_ASIA_1 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Prospecting']; return deals_ASIA_1;}
List<Customer_Finance_Deal__c> deals_ASIA_2; public List<Customer_Finance_Deal__c> getDeals_ASIA_2()    { if(deals_ASIA_2 == null) deals_ASIA_2 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'LOI']; return deals_ASIA_2;}
List<Customer_Finance_Deal__c> deals_ASIA_3; public List<Customer_Finance_Deal__c> getDeals_ASIA_3()    { if(deals_ASIA_3 == null) deals_ASIA_3 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Credit Analysis']; return deals_ASIA_3;}
List<Customer_Finance_Deal__c> deals_ASIA_4; public List<Customer_Finance_Deal__c> getDeals_ASIA_4()    { if(deals_ASIA_4 == null) deals_ASIA_4 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Proposal Negotiation']; return deals_ASIA_4;}
List<Customer_Finance_Deal__c> deals_ASIA_5; public List<Customer_Finance_Deal__c> getDeals_ASIA_5()    { if(deals_ASIA_5 == null) deals_ASIA_5 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Transaction Approval']; return deals_ASIA_5;}
List<Customer_Finance_Deal__c> deals_ASIA_6; public List<Customer_Finance_Deal__c> getDeals_ASIA_6()    { if(deals_ASIA_6 == null) deals_ASIA_6 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Contract Negotiation']; return deals_ASIA_6;}
List<Customer_Finance_Deal__c> deals_ASIA_7; public List<Customer_Finance_Deal__c> getDeals_ASIA_7()    { if(deals_ASIA_7 == null) deals_ASIA_7 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='ASIA PACIFIC' and  Deal_Stage__c = 'Disbursement']; return deals_ASIA_7;}

// NAMER------------------------------------------------------------------
// ----------------------------------------------------------------------

List<Customer_Finance_Deal__c> deals_NAMER_1; public List<Customer_Finance_Deal__c> getDeals_NAMER_1()    { if(deals_NAMER_1 == null) deals_NAMER_1 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Prospecting']; return deals_NAMER_1;}
List<Customer_Finance_Deal__c> deals_NAMER_2; public List<Customer_Finance_Deal__c> getDeals_NAMER_2()    { if(deals_NAMER_2 == null) deals_NAMER_2 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'LOI']; return deals_NAMER_2;}
List<Customer_Finance_Deal__c> deals_NAMER_3; public List<Customer_Finance_Deal__c> getDeals_NAMER_3()    { if(deals_NAMER_3 == null) deals_NAMER_3 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Credit Analysis']; return deals_NAMER_3;}
List<Customer_Finance_Deal__c> deals_NAMER_4; public List<Customer_Finance_Deal__c> getDeals_NAMER_4()    { if(deals_NAMER_4 == null) deals_NAMER_4 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Proposal Negotiation']; return deals_NAMER_4;}
List<Customer_Finance_Deal__c> deals_NAMER_5; public List<Customer_Finance_Deal__c> getDeals_NAMER_5()    { if(deals_NAMER_5 == null) deals_NAMER_5 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Transaction Approval']; return deals_NAMER_5;}
List<Customer_Finance_Deal__c> deals_NAMER_6; public List<Customer_Finance_Deal__c> getDeals_NAMER_6()    { if(deals_NAMER_6 == null) deals_NAMER_6 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Contract Negotiation']; return deals_NAMER_6;}
List<Customer_Finance_Deal__c> deals_NAMER_7; public List<Customer_Finance_Deal__c> getDeals_NAMER_7()    { if(deals_NAMER_7 == null) deals_NAMER_7 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='NORTH AMERICA' and  Deal_Stage__c = 'Disbursement']; return deals_NAMER_7;}

// LATAM------------------------------------------------------------------
// ----------------------------------------------------------------------

List<Customer_Finance_Deal__c> deals_LATAM_1; public List<Customer_Finance_Deal__c> getDeals_LATAM_1()    { if(deals_LATAM_1 == null) deals_LATAM_1 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Prospecting']; return deals_LATAM_1;}
List<Customer_Finance_Deal__c> deals_LATAM_2; public List<Customer_Finance_Deal__c> getDeals_LATAM_2()    { if(deals_LATAM_2 == null) deals_LATAM_2 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'LOI']; return deals_LATAM_2;}
List<Customer_Finance_Deal__c> deals_LATAM_3; public List<Customer_Finance_Deal__c> getDeals_LATAM_3()    { if(deals_LATAM_3 == null) deals_LATAM_3 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Credit Analysis']; return deals_LATAM_3;}
List<Customer_Finance_Deal__c> deals_LATAM_4; public List<Customer_Finance_Deal__c> getDeals_LATAM_4()    { if(deals_LATAM_4 == null) deals_LATAM_4 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Proposal Negotiation']; return deals_LATAM_4;}
List<Customer_Finance_Deal__c> deals_LATAM_5; public List<Customer_Finance_Deal__c> getDeals_LATAM_5()    { if(deals_LATAM_5 == null) deals_LATAM_5 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Transaction Approval']; return deals_LATAM_5;}
List<Customer_Finance_Deal__c> deals_LATAM_6; public List<Customer_Finance_Deal__c> getDeals_LATAM_6()    { if(deals_LATAM_6 == null) deals_LATAM_6 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Contract Negotiation']; return deals_LATAM_6;}
List<Customer_Finance_Deal__c> deals_LATAM_7; public List<Customer_Finance_Deal__c> getDeals_LATAM_7()    { if(deals_LATAM_7 == null) deals_LATAM_7 = [SELECT id,name,Aircraft_Qty_and_Model__c,Client_Airline_Account__r.name,Source_Account__r.name,RecordType.name FROM  Customer_Finance_Deal__c  WHERE Region__c='LATAM' and  Deal_Stage__c = 'Disbursement']; return deals_LATAM_7;}






}