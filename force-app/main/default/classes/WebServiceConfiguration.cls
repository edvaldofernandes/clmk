public class WebServiceConfiguration {
    
    public static String getEnPoint(String name){
                
        EventConfiguration__c eventConfiguration = [SELECT endPointUrl__c FROM EventConfiguration__c WHERE name = :name];
        return eventConfiguration.endPointUrl__c;
    }
    
    public static String getAuthentication(String name){
                
        EventConfiguration__c eventConfiguration = [SELECT password__c, username__c 
                                                    FROM EventConfiguration__c 
                                                    WHERE name = :name];
        
        return (eventConfiguration.password__c + ':' + eventConfiguration.username__c);
    }
}