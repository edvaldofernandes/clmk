/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Dec-2016.
**/

global class UpdateSummarySkyline implements Database.Batchable<sObject>, Database.Stateful 
{
    
    global Database.Querylocator start( Database.BatchableContext BC )
    {
              
        string query = '';
        
        query = 'Select Id,Status__c,Risk__c,Order_Type__c,Lessee_Defined__c, SkylineSummaryCalculated__c,CompanyType__c,Aircraft_Family__c  from Apttus__AgreementLineItem__c';  
        
        return Database.getQueryLocator( query );
    }
    
    
    global void execute( Database.BatchableContext BC, List<sObject> scope )
    {
        List<Apttus__AgreementLineItem__c> recordList = new List<Apttus__AgreementLineItem__c>();
        Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c();
        
        
        for(sObject objeto : scope )
        {
            ali = (Apttus__AgreementLineItem__c)objeto;
            
            if((ali.Status__c == 'Planned' || ali.Status__c == 'Delivered') && ali.Order_Type__c == 'Firm')
            {
                if(ali.Risk__c =='Critical')
                {
                    System.Debug('High Risk');
                    ali.SkylineSummaryCalculated__c = 'High Risk ' + ali.Aircraft_Family__c;
                }
                else
                {
                    if(ali.CompanyType__c == 'Leasing Company' && ali.lessee_Defined__c != true)
                    {
                        System.Debug(' Comp. w/o Lessee');
                        ali.SkylineSummaryCalculated__c = ali.Aircraft_Family__c + ' Comp. w/o Lessee';
                    }
                    else
                    {
                        System.Debug('É firm');
                        ali.SkylineSummaryCalculated__c = 'Firms  ' + ali.Aircraft_Family__c;
                    }
                }
            }
            else
            {
                ali.SkylineSummaryCalculated__c = '';
            }
            recordList.add(ali);
        }
        
        if(!recordList.isEmpty())
            database.update(recordList,false);
    
        
    }
    
    global void finish( Database.BatchableContext bcMain )
    {
        
    }   

}