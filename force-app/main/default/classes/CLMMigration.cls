/**
* @author Marcilio Leite de Souza
* @date 06/05/2019
* @description: Class for migration data from apttus
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           06 MAY 2019             Original Version
**/
global without sharing class CLMMigration {
   
   public static String apttusAgree =  'SELECT RecordType.Name, ' + 
                                 Utils.getAllFields('Apttus__APTS_Agreement__c')  +
                         ' FROM Apttus__APTS_Agreement__c';
   
   public static String apttusAircraft =  'SELECT  ' + 
                                 Utils.getAllFields('Apttus__AgreementLineItem__c')  +
                         ' FROM Apttus__AgreementLineItem__c';

   public static Agreement__c  apttusToCommercial(Apttus__APTS_Agreement__c apttusAgreement){
       Agreement__c commercialAgreement = new Agreement__c();

       String apttusRecordTypeName = GEN_Util.getRecordTypeNameById('Apttus__APTS_Agreement__c', apttusAgreement.RecordTypeId);

       commercialAgreement.RecordTypeId = GEN_Util.getRecordTypeIdbyName('Agreement__c', apttusRecordTypeName);
       commercialAgreement.Apptus_Agreement_ID__c = apttusAgreement.Id;
       
       commercialAgreement.Name = apttusAgreement.Name;

       //Inactive owner
       if(apttusAgreement.OwnerId == '005i0000004xmKL'){
            //Adilson
            commercialAgreement.OwnerId = '005i0000005S66B';
       }else{
           commercialAgreement.OwnerId = apttusAgreement.OwnerId;
       }

       if(apttusAgreement.Country__c == 'Republic of Belarus'){
           commercialAgreement.Country__c = 'Belarus';
       }else if(apttusAgreement.Country__c == 'N/A'){
           commercialAgreement.Country__c = 'Brazil';
       }else if(apttusAgreement.Country__c == 'UK'){
           commercialAgreement.Country__c = 'United Kingdom';
       }else if(apttusAgreement.Country__c == 'USA'){
           commercialAgreement.Country__c = 'United States';
       }else if(apttusAgreement.Country__c == 'The Netherlands'){
           commercialAgreement.Country__c = 'Netherlands';
       }else if(apttusAgreement.Country__c == 'Validation in Australia'){
           commercialAgreement.Country__c = 'Australia';
       }else if(apttusAgreement.Country__c == 'Costa Rica and Peru'){
           commercialAgreement.Country__c = 'Costa Rica';
       }else if(apttusAgreement.Country__c == 'Validated in Israel by the ICAA'){
           commercialAgreement.Country__c = 'Israel';
       }else if(apttusAgreement.Country__c == 'Validated in Azerbaijan'){
           commercialAgreement.Country__c = 'Azerbaijan';
       }else if(apttusAgreement.Country__c == 'Validated in Mozambique by the local Airworthiness Authority'){
           commercialAgreement.Country__c = 'Mozambique';
       }else if(apttusAgreement.Country__c == 'England'){
           commercialAgreement.Country__c = 'United Kingdom';
       }else if(apttusAgreement.Country__c == 'validation in Nigeria of the type certification'){
           commercialAgreement.Country__c = 'Nigeria';
       }else if(apttusAgreement.Country__c == 'Validated in Japan'){
           commercialAgreement.Country__c = 'Japan';
       }else if(apttusAgreement.Country__c == 'Moldova'){
           commercialAgreement.Country__c = 'Moldova, Republic of';
       }else{
           commercialAgreement.Country__c = apttusAgreement.Country__c ;
       }

   if(apttusAgreement.Country_for_Validation__c == 'Republic of Belarus'){
           commercialAgreement.Country_for_Validation__c = 'Belarus';
       }else if(apttusAgreement.Country_for_Validation__c == 'N/A'){
           commercialAgreement.Country_for_Validation__c = 'Brazil';
       }else if(apttusAgreement.Country_for_Validation__c == 'UK'){
           commercialAgreement.Country_for_Validation__c = 'United Kingdom';
       }else if(apttusAgreement.Country_for_Validation__c == 'USA'){
           commercialAgreement.Country_for_Validation__c = 'United States';
       }else if(apttusAgreement.Country_for_Validation__c == 'The Netherlands'){
           commercialAgreement.Country_for_Validation__c = 'Netherlands';
       }else if(apttusAgreement.Country_for_Validation__c == 'Validation in Australia'){
           commercialAgreement.Country_for_Validation__c = 'Australia';
       }else if(apttusAgreement.Country_for_Validation__c == 'Costa Rica and Peru'){
           commercialAgreement.Country_for_Validation__c = 'Costa Rica';
       }else if(apttusAgreement.Country_for_Validation__c == 'Validated in Israel by the ICAA'){
           commercialAgreement.Country_for_Validation__c = 'Israel';
       }else if(apttusAgreement.Country_for_Validation__c == 'Validated in Azerbaijan'){
           commercialAgreement.Country_for_Validation__c = 'Azerbaijan';
       }else if(apttusAgreement.Country_for_Validation__c == 'Validated in Mozambique by the local Airworthiness Authority'){
           commercialAgreement.Country_for_Validation__c = 'Mozambique';
       }else if(apttusAgreement.Country_for_Validation__c == 'England'){
           commercialAgreement.Country_for_Validation__c = 'United Kingdom';
       }else if(apttusAgreement.Country_for_Validation__c == 'validation in Nigeria of the type certification'){
           commercialAgreement.Country_for_Validation__c = 'Nigeria';
       }else if(apttusAgreement.Country_for_Validation__c == 'Validated in Japan'){
           commercialAgreement.Country_for_Validation__c = 'Japan';
       }else if(apttusAgreement.Country_for_Validation__c == 'Moldova'){
           commercialAgreement.Country_for_Validation__c = 'Moldova, Republic of';
       }else{
           commercialAgreement.Country_for_Validation__c = apttusAgreement.Country_for_Validation__c ;
       }

       if (String.isBlank(apttusAgreement.Agreement_Color__c) || 
           String.isEmpty(apttusAgreement.Agreement_Color__c)) {
           commercialAgreement.Agreement_Color__c = '990000'; 
       }else {
           commercialAgreement.Agreement_Color__c = apttusAgreement.Agreement_Color__c ;
       }

       //has to be update after
       //commercialAgreement.Related_Proposal__c = apttusAgreement.Related_Proposal__c ;
       commercialAgreement.Related_Proposal__c = null ;

       commercialAgreement.Account__c = apttusAgreement.Apttus__Account__c ;
       commercialAgreement.Account_Search_Field__c = apttusAgreement.Apttus__Account_Search_Field__c ;
       //*commercialAgreement.Account_Type__c = apttusAgreement.Account_Type__c ;
       commercialAgreement.Activated_Date__c = apttusAgreement.Apttus__Activated_Date__c ;
       commercialAgreement.Admin_Mode__c = apttusAgreement.Admin_Mode__c ;
       //commercialAgreement.Agr_Status__c = apttusAgreement. ;
       //commercialAgreement.Agreement_Category__c = apttusAgreement.Apttus__Agreement_Category__c ;
       commercialAgreement.Agreement_Code__c = apttusAgreement.Agreement_Code__c ;

       //commercialAgreement.Agreement_General_Comments_Hot_Issues__c = apttusAgreement.General_Comments__c ;
       commercialAgreement.Agreement_ID__c = apttusAgreement.Agreement_ID__c ;
       //commercialAgreement.Agreement_Number__c = apttusAgreement.Apttus__Agreement_Number__c ;
       commercialAgreement.Agreement_Status__c = apttusAgreement.Agreement_Status__c ;
       commercialAgreement.AgreementExternalID__c = apttusAgreement.AgreementExternalID__c ;
       commercialAgreement.Aircraft_Basic_Price__c = apttusAgreement.Aircraft_Basic_Price__c ;
       commercialAgreement.Aircraft_General_Comments__c = apttusAgreement.Aircraft_General_Comments__c ;
       //commercialAgreement.Aircraft_Price__c = apttusAgreement. ;
       //commercialAgreement.AllowableOutputFormats__c = apttusAgreement.Apttus__AllowableOutputFormats__c ;
       //*commercialAgreement.Amend__c = apttusAgreement.Apttus__FF_Amend__c ;
       commercialAgreement.Amendment_Done__c = apttusAgreement.Amendment_Done__c ;
       commercialAgreement.Amendment_Effective_Date__c = apttusAgreement.Apttus__Amendment_Effective_Date__c ;
       commercialAgreement.Applicable_Law__c = apttusAgreement.Applicable_Law__c ;
       commercialAgreement.Applicable_Law_Comments__c = apttusAgreement.Applicable_Law_Comments__c ;
       commercialAgreement.Auto_Renew_Consent__c = apttusAgreement.Apttus__Auto_Renew_Consent__c ;
       //commercialAgreement.Auto_Renew_Term_Months__c = apttusAgreement.Apttus__Auto_Renew_Term_Months__c ;
       commercialAgreement.Auto_Renewal__c = apttusAgreement.Apttus__Auto_Renewal__c ;
       commercialAgreement.Auto_Renewal_Terms__c = apttusAgreement.Apttus__Auto_Renewal_Terms__c ;
       commercialAgreement.Basic_Certification__c = apttusAgreement.Basic_Certification__c ;
       commercialAgreement.Business_Day__c = apttusAgreement.Business_Day__c ;
       commercialAgreement.Business_Day_City__c = apttusAgreement.Business_Day_City__c ;
       commercialAgreement.Business_Day_Country__c = apttusAgreement.Business_Day_Country__c ;
       commercialAgreement.Buyer__c = apttusAgreement.Buyer__c ;
       commercialAgreement.Bypass_Approval__c = apttusAgreement.Bypass_Approval__c ;
       commercialAgreement.Bypass_Comments__c = apttusAgreement.Bypass_Comments__c ;
       commercialAgreement.Certification_Basis__c = apttusAgreement.Certification_Basis__c ;
       commercialAgreement.Certification_Comments__c = apttusAgreement.Certification_Comments__c ;
       commercialAgreement.Change_History__c = apttusAgreement.Change_History__c ;
       commercialAgreement.Client_Address__c = apttusAgreement.Client_Address__c ;
       commercialAgreement.Client_Name__c = apttusAgreement.Client_Name__c ;
       commercialAgreement.CLM_Bypass_Proposal_Approval__c = apttusAgreement.CLM_Bypass_Proposal_Approval__c ;
       //commercialAgreement.CLM_Record_Type_Name__c = apttusAgreement.CLM_Record_Type_Name__c ;
       //*commercialAgreement.CLM_Terminate__c = apttusAgreement.CLM_Terminate__c ;
       //*commercialAgreement.Color_Preview__c = apttusAgreement.Color_Preview__c ;
       commercialAgreement.COM_Code__c = apttusAgreement.COM_Code__c ;
       commercialAgreement.Comments__c = apttusAgreement.Comments__c ;
       commercialAgreement.Company_Signed_Date__c = apttusAgreement.Apttus__Company_Signed_Date__c ;
       commercialAgreement.Company_Signed_Title__c = apttusAgreement.Apttus__Company_Signed_Title__c ;
       commercialAgreement.Configuration_Comments__c = apttusAgreement.Configuration_Comments__c ;
       commercialAgreement.Configuration_Comments__c = apttusAgreement.Contract_Code__c ;
       commercialAgreement.Contract_End_Date__c = apttusAgreement.Apttus__Contract_End_Date__c ;
       //*commercialAgreement.Contract_Number__c = apttusAgreement.Apttus__Contract_Number__c ;
       commercialAgreement.Contract_Signature_Date__c = apttusAgreement.Contract_Signature_Date__c ;
       commercialAgreement.Contract_Start_Date__c = apttusAgreement.Apttus__Contract_Start_Date__c ;
       commercialAgreement.ContractOwner__c = apttusAgreement.ContractOwner__c ;

       commercialAgreement.DataReviewRequired__c = apttusAgreement.DataReviewRequired__c ;
       //*commercialAgreement.Deal_Value__c = apttusAgreement.Deal_Value__c ;
       commercialAgreement.Description__c = apttusAgreement.Apttus__Description__c ;
       commercialAgreement.DF_Color__c = apttusAgreement.DF_Color__c ;
       commercialAgreement.DMCG__c = apttusAgreement.DMCG__c ;
       commercialAgreement.DOCCON_Number__c = apttusAgreement.DOCCON_Number__c ;
       commercialAgreement.DRG__c = apttusAgreement.DRG__c ;
       commercialAgreement.EIS_Date__c = apttusAgreement.EIS_Date__c ;
       commercialAgreement.Elaboration_Date__c = apttusAgreement.Contract_Elaboration_Date__c ;
       commercialAgreement.Embraer_Warranty__c = apttusAgreement.Embraer_warranty__c ;
       commercialAgreement.Enable_Sync__c = apttusAgreement.Enable_Sync__c ;
       commercialAgreement.Escalation_CAP_Comments__c = apttusAgreement.Escalation_CAP_Comments__c ;
       //commercialAgreement.Executed_Copy_Mailed_Out_Date__c = apttusAgreement.Apttus__Executed_Copy_Mailed_Out_Date__c ;
       commercialAgreement.Expiration_Settlement_Conditions__c = apttusAgreement.Expiration_Settlement_Conditions__c ;
       commercialAgreement.Fleet_Reliability_Specialist__c = apttusAgreement.Fleet_Reliability_Specialist__c ;
       commercialAgreement.Flight_Attendant_Training__c = apttusAgreement.Flight_Attendant_Training__c ;
       //commercialAgreement.Generate__c = apttusAgreement.CLM_Generate__c ;
       commercialAgreement.Guarantee_Comments__c = apttusAgreement.Guarantee_Comments__c ;
       //commercialAgreement.Initiation_Type__c = apttusAgreement.Apttus__Initiation_Type__c ;
       //commercialAgreement.Internal_Renewal_Notification_Days__c = apttusAgreement.Apttus__Internal_Renewal_Notification_Days__c ;
       commercialAgreement.Is_Amendment__c = apttusAgreement.Is_Amendment__c ;
       //commercialAgreement.Is_System_Update__c = apttusAgreement.Apttus__Is_System_Update__c ;
       //commercialAgreement.IsInternalReview__c = apttusAgreement.Apttus__IsInternalReview__c ;
       commercialAgreement.IsLocked__c = apttusAgreement.Apttus__IsLocked__c ;
       commercialAgreement.Kickoff_trigger_already_executed__c = apttusAgreement.Kickoff_trigger_already_executed__c ;
       commercialAgreement.LA_Main_Changes__c = apttusAgreement.LA_Main_Changes__c ;
       //commercialAgreement.LatestDocId__c = apttusAgreement.Apttus__LatestDocId__c ;
       commercialAgreement.Loaded_Record__c = apttusAgreement.LoadedRecord__c ;
       commercialAgreement.Maintenance_Program_Specialist__c = apttusAgreement.Maintenance_Program_Specialist__c ;
       commercialAgreement.Mechanic_Training__c = apttusAgreement.Mechanic_Training__c ;
       commercialAgreement.Mechanics__c = apttusAgreement.Mechanics__c ;
       commercialAgreement.MFIR_Buyer__c = apttusAgreement.MFIR_Buyer__c ;
       commercialAgreement.Nickname__c = apttusAgreement.Nickname__c ;
       //commercialAgreement.Non_Standard_Legal_Language__c = apttusAgreement.Apttus__Non_Standard_Legal_Language__c ;
       //*commercialAgreement.Number_Of_Aircraft_Price__c = apttusAgreement.Number_Of_Aircraft_Price__c ;
       //*commercialAgreement.Number_of_Contract_Aircraft__c = apttusAgreement.Number_of_Contract_Aircraft__c ;
       commercialAgreement.Old_Total_Agreement_Value__c = apttusAgreement.Old_Total_Agreement_Value__c ;
       commercialAgreement.On_Site_Support_Comments__c = apttusAgreement.On_Site_Support_Comments__c ;
       commercialAgreement.Operations_Engineer__c = apttusAgreement.Operations_Engineer__c ;
       commercialAgreement.Original_Agreement_for_Amendment__c = apttusAgreement.Original_Agreement_for_Amendment__c ;
       commercialAgreement.Original_PA_Signature_Date__c = apttusAgreement.Original_PA_Signature_Date__c ;
       //commercialAgreement.Other_Party_Returned_Date__c = apttusAgreement.Apttus__Other_Party_Returned_Date__c ;
       //commercialAgreement.Other_Party_Sent_Date__c = apttusAgreement.Apttus__Other_Party_Sent_Date__c ;
       //commercialAgreement.Other_Party_Signed_By_Unlisted__c = apttusAgreement.Apttus__Other_Party_Signed_By_Unlisted__c ;
       //commercialAgreement.Other_Party_Signed_Date__c = apttusAgreement.Apttus__Other_Party_Signed_Date__c ;
       //commercialAgreement.Other_Party_Signed_Title__c = apttusAgreement.Apttus__Other_Party_Signed_Title__c ;
       //commercialAgreement.Owner_Expiration_Notice__c = apttusAgreement.Apttus__Owner_Expiration_Notice__c ;
       commercialAgreement.PA_Already_set__c = apttusAgreement.PA_Already_set__c ;
       //commercialAgreement.PA_Signature_Due_Date__c = apttusAgreement.PA_Signature_Due_Date__c ;
       //commercialAgreement.PA_Signature_Duedate__c = apttusAgreement.PA_Signature_Duedate__c ;
       commercialAgreement.Payment_Date__c = apttusAgreement.Payment_Date__c ;
       commercialAgreement.PDP_CAP__c = apttusAgreement.PDP_CAP__c ;
       commercialAgreement.PDP_CAP_Comments__c = apttusAgreement.PDP_CAP_Comments__c ;
       //commercialAgreement.PDP_on_cash__c = apttusAgreement.PDP_on_cash__c ;
       commercialAgreement.PEP_Agreement__c = apttusAgreement.PEP_Agreement__c ;
       //commercialAgreement.Perpetual__c = apttusAgreement.Apttus__Perpetual__c ;
       commercialAgreement.PG__c = apttusAgreement.PG__c ;
       commercialAgreement.Pilot_Training__c = apttusAgreement.Pilot_Training__c ;
       commercialAgreement.Pilots__c = apttusAgreement.Pilots__c ;
       //*commercialAgreement.Price_Count__c = apttusAgreement.Price_Count__c ;
       //*commercialAgreement.Progress_Bar__c = apttusAgreement.Progress_Bar__c ;
       commercialAgreement.Proposal_ID__c = apttusAgreement.Proposal_ID__c ;
       commercialAgreement.Proposal_Validity__c = apttusAgreement.Proposal_Validity__c ;
       commercialAgreement.Region_Code__c = apttusAgreement.Region_Code__c ;
       
       //commercialAgreement.Renewal_Notice_Days__c = apttusAgreement.Apttus__Renewal_Notice_Days__c ;
       //commercialAgreement.Request_Date__c = apttusAgreement.Apttus__Request_Date__c ;
       //commercialAgreement.RetentionDate__c = apttusAgreement.Apttus__RetentionDate__c ;
       commercialAgreement.Revision_Service__c = apttusAgreement.Revision_Service__c ;
       commercialAgreement.Risk__c = apttusAgreement.Risk__c ;
       commercialAgreement.Risk_Rating__c = apttusAgreement.Apttus__Risk_Rating__c ;
       commercialAgreement.SAP_Contract__c = apttusAgreement.SAP_Contract__c ;
       commercialAgreement.Settlement__c = apttusAgreement.Settlement__c ;
       commercialAgreement.Show_All_Aircraft_As__c = apttusAgreement.Show_All_Aircraft_As__c ;
       commercialAgreement.Signature_Date__c = apttusAgreement.Signature_Date__c ;
       commercialAgreement.SLG__c = apttusAgreement.SLG__c ;
       commercialAgreement.SOB_Snapshot_Control_Field__c = apttusAgreement.SOB_Snapshot_Control_Field__c ;
       commercialAgreement.Source__c = apttusAgreement.Apttus__Source__c ;
       commercialAgreement.Special_Credit_Comments__c = apttusAgreement.Special_Credit_Comments__c ;
       commercialAgreement.Special_Terms__c = apttusAgreement.Apttus__Special_Terms__c ;
       commercialAgreement.Specific_Agreement_Aircrafts_Present__c = apttusAgreement.Specific_Agreement_Aircrafts__c ;
       commercialAgreement.Specific_Agreement_Aircrafts_Present_2__c = apttusAgreement.Specific_Agreement_Aircrafts_Present_2__c ;
       commercialAgreement.Start_Date_Is_Changed__c = apttusAgreement.Start_Date_Is_Changed__c ;
       commercialAgreement.Status__c = apttusAgreement.Apttus__Status__c ;
       commercialAgreement.Status_Category__c = apttusAgreement.Apttus__Status_Category__c ;
       //commercialAgreement.Submit_Request_Mode__c = apttusAgreement.Apttus__Submit_Request_Mode__c ;
       //commercialAgreement.Subtype__c = apttusAgreement.Apttus__Subtype__c ;
       commercialAgreement.TAV_Is_Changed__c = apttusAgreement.TAV_Is_Changed__c ;
       commercialAgreement.Tech_Pubs_Perf_Sw_Comments__c = apttusAgreement.Tech_Pubs_Perf_Sw_Comments__c ;
       commercialAgreement.Tech_Pubs_Specialist__c = apttusAgreement.Tech_Pubs_Specialist__c ;
       commercialAgreement.Tech_Rep__c = apttusAgreement.Tech_Rep__c ;
       commercialAgreement.Technical_Comments_Hot_Issues__c = apttusAgreement.Technical_Comments_Hot_Issues__c ;
       //commercialAgreement.Term_Months__c = apttusAgreement.Apttus__Term_Months__c ;
       commercialAgreement.Termination_Date__c = apttusAgreement.Termination_Date__c ;
       //commercialAgreement.Time_To_Exercise_Options__c = apttusAgreement.Time_To_Exercise_Options__c ;
       //commercialAgreement.Time_To_Notify_Jurisdiction__c = apttusAgreement.Time_To_Notify_Jurisdiction__c ;
       commercialAgreement.Training_Comments__c = apttusAgreement.Training_Comments__c ;
       commercialAgreement.Trigger_Execution__c = apttusAgreement.Trigger_Execution__c ;
       commercialAgreement.Undisclosed_Agreement__c = apttusAgreement.Undisclosed_Agreement__c ;
       commercialAgreement.Undisclosed_Client__c = apttusAgreement.Undisclosed_Client__c ;
       commercialAgreement.Undisclosed_Nickname__c = apttusAgreement.Undisclosed_Nickname__c ;
       commercialAgreement.Vendor_warranty__c = apttusAgreement.Vendor_warranty__c ;
       commercialAgreement.Warranty_Comments__c = apttusAgreement.Warranty_Comments__c ;

       return commercialAgreement;
   } 

    public static Agreement_Aircraft__c  apttusToCommercialAircraft(Apttus__AgreementLineItem__c apttusAircraft){
        Agreement_Aircraft__c aircraftAgreement = new Agreement_Aircraft__c();

        aircraftAgreement.Apptus_Aircraft_ID__c = apttusAircraft.Id;

        Agreement__c agreementRef = new Agreement__c(Apptus_Agreement_ID__c=apttusAircraft.Apttus__AgreementId__c);   
        //system.debug('apttusToCommercialAircraft.agreementRef: ' + agreementRef);
        aircraftAgreement.Agreement__r = agreementRef;

        //*aircraftAgreement.AC_Order__c = apttusAircraft.AC_Order__c;
        aircraftAgreement.Actual_Delivery_Date__c = apttusAircraft.Actual_Delivery_Date__c;
        aircraftAgreement.Actual_Delivery_Start_Date__c = apttusAircraft.Actual_Delivery_Start_Date__c;
        aircraftAgreement.AFA__c = apttusAircraft.AFA__c;
        
        if(apttusAircraft.Summary_SKYLINE__c == 'Firms  E1'){
            aircraftAgreement.Summary_SKYLINE__c = 'Firms E1';
        }else if(apttusAircraft.Summary_SKYLINE__c == 'Firms  E2'){
            aircraftAgreement.Summary_SKYLINE__c = 'Firms E2';
        }else{
            aircraftAgreement.Summary_SKYLINE__c = apttusAircraft.Summary_SKYLINE__c;
        }

        //aircraftAgreement. = apttusAircraft.Agreement_RT_Name__c;
        //aircraftAgreement.Agreement_Status__c = apttusAircraft.Agr_Status__c;
        aircraftAgreement.Aircraft__c = apttusAircraft.Aircraft__c;
        aircraftAgreement.Basic_Price__c = apttusAircraft.Basic_Price__c;
        aircraftAgreement.Aircraft_Basic_Price_EC__c = apttusAircraft.Aircraft_Basic_Price_EC__c;
        aircraftAgreement.Aircraft_Certification_Designation__c = apttusAircraft.Aircraft_Certification_Designation__c;
        aircraftAgreement.Aircraft_Configuration__c = apttusAircraft.Aircraft_Configuration__c;
        aircraftAgreement.Aircraft_Delivery__c = apttusAircraft.Aircraft_Delivery__c;
        //*aircraftAgreement.Aircraft_Family__c = apttusAircraft.Aircraft_Family__c;
        //*aircraftAgreement.Aircraft_ID__c = apttusAircraft.Aircraft_ID__c;
        //*aircraftAgreement.Aircraft_Model__c = apttusAircraft.Aircraft_Model__c;
        //*aircraftAgreement.Aircraft_Name__c = apttusAircraft.Aircraft_Name__c;
        aircraftAgreement.Aircraft_Price__c = apttusAircraft.Aircraft_Price__c;
        //*aircraftAgreement.Aircraft_Purchase_Price__c = apttusAircraft.Aircraft_Purchase_Price__c;
        //*aircraftAgreement.Aircraft_Purchase_Price_EC__c = apttusAircraft.Aircraft_Purchase_Price_EC__c;
        //*aircraftAgreement.Aircraft_Version__c = apttusAircraft.Aircraft_Version__c;
        //aircraftAgreement. = apttusAircraft.AC_Version__c;
        aircraftAgreement.Airport_of_Destination__c = apttusAircraft.Airport_of_Destination__c;
        aircraftAgreement.APU__c = apttusAircraft.APU__c;
        aircraftAgreement.Ato_Concessorio__c = apttusAircraft.Ato_Concessorio__c;
        //*aircraftAgreement.Auto_Update_Trend_Date__c = apttusAircraft.Auto_Update_Trend_Date__c;
        //*aircraftAgreement.Balance__c = apttusAircraft.Balance__c;
        aircraftAgreement.Bill_to_Aircraft__c = apttusAircraft.Bill_to_Aircraft__c;
        aircraftAgreement.Bill_to_Kit__c = apttusAircraft.Bill_to_Kit__c;
        aircraftAgreement.Book_Status__c = apttusAircraft.Book_Status__c;
        //*aircraftAgreement.Calc_Expir_date__c = apttusAircraft.Calc_Expir_date__c;
        //aircraftAgreement.Chave_DF__c = apttusAircraft.ChaveDF__c;
        aircraftAgreement.Comments__c = apttusAircraft.Comments__c;
        aircraftAgreement.Comments_Delivery_Details__c = apttusAircraft.Comments_Delivery_Details__c;
        aircraftAgreement.Comments_Price__c = apttusAircraft.Comments_Price__c;
        //aircraftAgreement. = apttusAircraft.CompanyType__c;
        aircraftAgreement.Contract_Administrator__c = apttusAircraft.Contract_Administrator__c;
        aircraftAgreement.Contract_Aircraft_Number__c = apttusAircraft.Contract_Aircraft_Number__c;
        //*aircraftAgreement.Contract_Buyer__c = apttusAircraft.Contract_Buyer__c;
        aircraftAgreement.Contract_Engineer__c = apttusAircraft.Contract_Engineer__c;
        //*aircraftAgreement.Contractual_Delivery_First_Month_Day__c = apttusAircraft.Contractual_Delivery_First_Month_Day_del__c;
        aircraftAgreement.Contractual_Delivery_Month__c = apttusAircraft.Dellivery_Month__c;
        aircraftAgreement.Counter__c = apttusAircraft.Counter__c;
        aircraftAgreement.Order_Type__c = apttusAircraft.Order_Type__c;
        aircraftAgreement.Current_Snapshot_Version__c = apttusAircraft.Current_Snapshot_Version__c;
        //*aircraftAgreement.Customer_Nickname__c = apttusAircraft.Customer_Nickname__c;
        aircraftAgreement.Customs_Clearance_At__c = apttusAircraft.Customs_Clearance_At__c;
        aircraftAgreement.Data_do_Ato_Concessorio__c = apttusAircraft.Data_do_Ato_Concessorio__c;
        aircraftAgreement.Delivered__c = apttusAircraft.Delivered__c;
        aircraftAgreement.Delivery_Coordinator__c = apttusAircraft.Delivery_Coordinator__c;
        aircraftAgreement.Delivery_Pilot__c = apttusAircraft.Delivery_Pilot__c;
        aircraftAgreement.Delivery_SEC_Score__c = apttusAircraft.Delivery_SEC_Score__c;
        aircraftAgreement.DF_Code__c = apttusAircraft.DF_Code__c;
        aircraftAgreement.DF_Country_Code__c = apttusAircraft.DF_Country_Code__c;
        aircraftAgreement.Discount_or_Increase__c = apttusAircraft.Discount_or_Increase__c;
        //aircraftAgreement.Escalation__c = apttusAircraft.Escalation__c;
        aircraftAgreement.Escalation_Code__c = apttusAircraft.Escalation__c;
        //*aircraftAgreement.Escalation_Factor__c = apttusAircraft.Escalation_Factor__c;
        aircraftAgreement.Estimated_Closing_Date__c = apttusAircraft.Estimated_Closing_Date__c;
        aircraftAgreement.Exercised_Date__c = apttusAircraft.Exercised_Date__c;
        aircraftAgreement.Expiration_Date__c = apttusAircraft.Expiration_Date__c;
        //aircraftAgreement. = apttusAircraft.Apttus__ExtendedPrice__c;
        aircraftAgreement.Ferry_Flight_Pilot_1__c = apttusAircraft.Ferry_Flight_Pilot_1__c;
        aircraftAgreement.Ferry_Flight_Pilot_2__c = apttusAircraft.Ferry_Flight_Pilot_2__c;
        //*aircraftAgreement.Final_SEC_Delivery__c = apttusAircraft.Final_SEC_Delivery__c;
        //*aircraftAgreement.Final_SEC_Production__c = apttusAircraft.Final_SEC_Production__c;
        aircraftAgreement.Firm_Cancelled_and_Transfered__c = apttusAircraft.Firm_Cancelled_and_Transfered__c;
        aircraftAgreement.Firm_Included__c = apttusAircraft.Firm_Included__c;
        aircraftAgreement.Firm_Terminated__c = apttusAircraft.Firm_Terminated__c;
        aircraftAgreement.Firm_Transfered__c = apttusAircraft.Firm_Transfered__c;
        aircraftAgreement.First_Notification__c = apttusAircraft.First_Notification__c;
        aircraftAgreement.Aircraft_SAP_ID__c = apttusAircraft.IDAircraftSAP__c;
        aircraftAgreement.Included_Event_Confirmed__c = apttusAircraft.Included_Event_Confirmed__c;
        aircraftAgreement.Initial_Airworthiness_Team__c = apttusAircraft.Initial_Airworthiness_Team__c;
        //*aircraftAgreement.Inspection_Week_First_Day__c = apttusAircraft.Inspection_Week_First_Day__c;
        aircraftAgreement.Interest_Amount__c = apttusAircraft.Interest_Amount__c;
        aircraftAgreement.Invoice__c = apttusAircraft.Invoice__c;
        aircraftAgreement.Invoice_Interest_Amount__c = apttusAircraft.Invoice_Interest_Amount__c;
        aircraftAgreement.Invoice_No_Charge__c = apttusAircraft.Invoice_No_Charge__c;
        aircraftAgreement.Invoice_PDP_Return__c = apttusAircraft.Invoice_PDP_Return__c;
        aircraftAgreement.Last_Notification__c = apttusAircraft.Last_Notification__c;
        aircraftAgreement.Left_Engine__c = apttusAircraft.Left_Engine__c;
        aircraftAgreement.Lessee_Defined__c = apttusAircraft.Lessee_Defined__c;
        //aircraftAgreement. = apttusAircraft.Apttus__ListPrice__c;
        //*aircraftAgreement.List_Price__c = apttusAircraft.List_Price__c;
        aircraftAgreement.Material_Code__c = apttusAircraft.Material_Code__c;
        aircraftAgreement.MFA_Score__c = apttusAircraft.MFA_Score__c;
        //aircraftAgreement. = apttusAircraft.MFIR_Bill_to_Aircraft__c;
        //aircraftAgreement. = apttusAircraft.MFIR_Bill_to_Kit__c;
        aircraftAgreement.MFIR_Kit_Bill_To__c = apttusAircraft.MFIR_Kit_Bill_To__c;
        aircraftAgreement.MFIR_Kit_Ship_To__c = apttusAircraft.MFIR_Kit_Ship_To__c;
        //aircraftAgreement. = apttusAircraft.MFIR_Ship_to_Aircraft__c;
        //aircraftAgreement. = apttusAircraft.MFIR_Ship_to_Kit__c;
        aircraftAgreement.MSN__c = apttusAircraft.MSN__c;
        aircraftAgreement.No_Charge_Amount__c = apttusAircraft.No_Charge_Amount__c;
        aircraftAgreement.Operator_Aircraft_Number__c = apttusAircraft.Operator_Aircraft_Number__c;
        aircraftAgreement.Option_Execution_Date__c = apttusAircraft.Option_Execution_Date__c;
        aircraftAgreement.Option_Expiration_Date__c = apttusAircraft.Option_Expiration_Date__c;
        aircraftAgreement.Option_In_Execution__c = apttusAircraft.Option_In_Execution__c;
        aircraftAgreement.Option_Under_negotiation__c = apttusAircraft.Option_Under_negotiation__c;
        //aircraftAgreement. = apttusAircraft.Order_Type_Original__c;
        aircraftAgreement.Original_Order_Type__c = apttusAircraft.Original_Order_Type__c;
        aircraftAgreement.Outbound_Delivery__c = apttusAircraft.Outbound_Delivery__c;
        //*aircraftAgreement.Paid__c = apttusAircraft.Paid__c;
        aircraftAgreement.PDP_Already_set__c = apttusAircraft.PDP_Already_set__c;
        aircraftAgreement.PDP_Return__c = apttusAircraft.PDP_Return__c;
        aircraftAgreement.PEP_Aircraft__c = apttusAircraft.PEP_Aircraft__c;
        aircraftAgreement.PEP_Delivery__c = apttusAircraft.PEP_Delivery__c;
        aircraftAgreement.PEP_Preservation__c = apttusAircraft.PEP_Preservation__c;
        aircraftAgreement.PEP_Warranty__c = apttusAircraft.PEP_Warranty__c;
        //aircraftAgreement.PRA_Execution_Date__c = apttusAircraft.PRA_Execution_Date__c;
        //aircraftAgreement.PRA_Expiration_Date__c = apttusAircraft.PRA_Expiration_Date__c;
        //aircraftAgreement.PRA_Under_negotiation__c = apttusAircraft.PRA_Under_negotiation__c;
        aircraftAgreement.Escalated_Price__c = apttusAircraft.Escalated_Price__c;
        aircraftAgreement.Production_SEC_Score__c = apttusAircraft.Production_SEC_Score__c;
        //aircraftAgreement.Production_Support_Engineer__c = apttusAircraft.Production_Support_Engineer__c;
        //*aircraftAgreement.Progress_Bar__c = apttusAircraft.Progress_Bar__c;
        aircraftAgreement.Quality_Documentation__c = apttusAircraft.Quality_Documentation__c;
        aircraftAgreement.Quality_Focal_Point_1st_Shift__c = apttusAircraft.Quality_Focal_Point_1st_Shift__c;
        aircraftAgreement.Quality_Focal_Point_2nd_Shift__c = apttusAircraft.Quality_Focal_Point_2nd_Shift__c;
        aircraftAgreement.RC__c = apttusAircraft.RC__c;
        aircraftAgreement.RE__c = apttusAircraft.RE__c;
        aircraftAgreement.Registration_Mark__c = apttusAircraft.Registration_Mark__c;
        //aircraftAgreement. = apttusAircraft.Related_Agreement_Line_Item__c;
        aircraftAgreement.Right_Engine__c = apttusAircraft.Right_Engine__c;
        aircraftAgreement.Risk__c = apttusAircraft.Risk__c;
        aircraftAgreement.SAP_Batch__c = apttusAircraft.SAP_Batch__c;
        //*aircraftAgreement.SAP_Contract__c = apttusAircraft.SAP_Contract__c;
        aircraftAgreement.SAP_Item__c = apttusAircraft.SAP_Item__c;
        aircraftAgreement.Delivery_Date__c = apttusAircraft.Delivery_Date__c;
        aircraftAgreement.Scheduled_Inspection_Date__c = apttusAircraft.Scheduled_Inspection_Date__c;
        //aircraftAgreement. = apttusAircraft.Ship_to_Aircraft__c;
        //aircraftAgreement. = apttusAircraft.Ship_to_Kit__c;
        aircraftAgreement.Show_Aircraft_As__c = apttusAircraft.Show_Aircraft_As__c;
        aircraftAgreement.Skyline_Summary_Calculated__c = apttusAircraft.SkylineSummaryCalculated__c;
        aircraftAgreement.Status__c = apttusAircraft.Status__c;
        aircraftAgreement.Stop_Escalation__c = apttusAircraft.Stop_Escalation__c;
        
        aircraftAgreement.Take_off_Date_Time__c = apttusAircraft.Take_off_Date_Time__c;
        aircraftAgreement.Technical_Leader_1st_Shift__c = apttusAircraft.Technical_Leader_1st_Shift__c;
        aircraftAgreement.Technical_Leader_2nd_Shift__c = apttusAircraft.Technical_Leader_2nd_Shift__c;
        aircraftAgreement.Temporary_Brazilian_Registration__c = apttusAircraft.Temporary_Brazilian_Registration__c;
        aircraftAgreement.TREND_Country_Code__c = apttusAircraft.TREND_Country_Code__c;
        aircraftAgreement.TREND_Delivery_Date__c = apttusAircraft.TREND_Delivery_Date__c;
        aircraftAgreement.TREND_DF_Code__c = apttusAircraft.TREND_DF_Code__c;
        aircraftAgreement.Undisclosed_Aircraft__c = apttusAircraft.Undisclosed_Aircraft__c;
        

       return aircraftAgreement;
    }

   global class CLMMigrationErroModel{

       global String recordId;
       global StatusCode erroCode;
       global String erroMsg;
       global List<String> listFields;

       global CLMMigrationErroModel(){}

   }
}