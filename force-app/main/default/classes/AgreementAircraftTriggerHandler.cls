public without sharing class AgreementAircraftTriggerHandler{
    public Map<Id,Agreement_Aircraft__c> newRecordsMap = new Map<Id,Agreement_Aircraft__c>();
    public Map<Id,Agreement_Aircraft__c> oldRecordsMap = new Map<Id,Agreement_Aircraft__c>(); 
    public List<Agreement_Aircraft__c> newRecords = new List<Agreement_Aircraft__c>();
    public List<Agreement_Aircraft__c> oldRecords = new List<Agreement_Aircraft__c>();
    
    public Map<Id,Agreement_Aircraft__c> newRecordsMapWithAgreementFields;
    
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false; 
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isFirstRun = false;
    public static boolean isRecursive = false;

    // CONSTANTES
    private final Integer NAME_CHANGE_CONTRACTUAL    = 0;
    private final Integer NAME_CHANGE_TREND          = 1;

    
    private Map<String,String> dateFieldsMap;
    private CLMTaskSetupUtil tsUtil;
    
    public AgreementAircraftTriggerHandler(boolean isFirstRun) {
        this.isFirstRun = isFirstRun;
    }

    public void OnBeforeInsert()
    {
        //forceTrendDateEqualsToContractualDate ();
        //updateSummarySkylineField();
    }

    public void OnBeforeUpdate()
    {
        
    }

    public void OnBeforeDelete()
    {

    }

    public void OnAfterInsert()
    {
        if (isFirstRun)
            AutoNamingRules.autoNamingFromAircraft(oldRecordsMap, newRecords, false);
        CLMSetAircraftEscalation.newAircraft(newRecords);
    }

    public void OnAfterUpdate()
    {
        if (isFirstRun)
            AutoNamingRules.autoNamingFromAircraft(oldRecordsMap, newRecords, true);
        CLMAircraftSOBEventCreation.updateSOBEvent(newRecordsMap,oldRecordsMap,newRecords);
        CLMSetAircraftEscalation.updatedAircraft(oldRecordsMap, newRecords);
    }

    public void OnAfterDelete()
    {

    }

    public void OnUndelete()
    {

    }
}