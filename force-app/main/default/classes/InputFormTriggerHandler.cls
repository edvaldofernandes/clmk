public without sharing class InputFormTriggerHandler
{
    
    public Map<Id,InputForm__c> newRecordsMap = new Map<Id,InputForm__c>();
    public Map<Id,InputForm__c> oldRecordsMap = new Map<Id,InputForm__c>(); 
    public List<InputForm__c> newRecords = new List<InputForm__c>();
    public List<InputForm__c> oldRecords = new List<InputForm__c>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
    
    private static Id RecordTypeIdRFPOnline = Schema.SObjectType.InputForm__c.getRecordTypeInfosByName().get('RFP Online').getRecordTypeId();
    private boolean Enable_RFP_Notification_System 	= CRM_Customer_RFP__c.getInstance().Enable_RFP_Notification_System__c;
    private string emailAdressToAllECAMs			= CRM_Customer_RFP__c.getInstance().Email_address_to_send_by_default__c;
    private String [] rolesToSendEmail 				= CRM_Customer_RFP__c.getInstance().Account_Team_Roles_To_Receive_Email__c.Split(',');
    //private String [] forceSendEmailTo 				= new String [] {'luciano.alves@embraer.net.br','guilherme.oliva@embraer.net.br','rodrigo.seo@embraer.net.br','mateus.caviglione@embraer.net.br'};
    
    public InputFormTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }
    public void OnBeforeInsert()
    {
        
    }
    public void OnAfterInsert()
    {
        sendEmailToNotifyAccountTeam();
    }
    public void OnBeforeUpdate()
    {
        
    }
    public void OnAfterUpdate()
    {
    }
    
    public void OnBeforeDelete()
    {
        // BEFORE DELETE LOGIC
    }
    public void OnAfterDelete()
    {
        // AFTER DELETE LOGIC
    }
    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }
    
    public void sendEmailToNotifyAccountTeam(){
        Messaging.reserveSingleEmailCapacity(3);
        
        System.debug('Roles ' + rolesToSendEmail);
        for(InputForm__c input : newRecords){
            if(input.RecordTypeId == RecordTypeIdRFPOnline && Enable_RFP_Notification_System == true){
                
                List<Contact> requester = [select Name, AccountId, Email from Contact where Email = :input.Requester_Email__c];
                //System.debug('Requester Contact: '+ requester);
                
                if(requester.size() > 0){
                    // FOUND CONTACT
                    Account acc = [select Name, Company_Nickname__c,  Id from Account where Id = :requester[0].AccountId];
                    List<AccountTeamMember> responsibleEcamList = [ select AccountId, TeamMemberRole, UserId from AccountTeamMember where AccountId = :requester[0].AccountId AND TeamMemberRole IN :rolesToSendEmail ];
                    //System.debug('Found ECAM: '+ responsibleEcamList);
                    if(responsibleEcamList.size() == 0){
                        // DIDNT FOUND ANY ACCOUNT ECAM OR ACCOUNT
                        if(input.Send_RFP_to_requester__c == true){
                            Messaging.SingleEmailMessage GenericEcamMail 	 = getEmailForGenericECAM ( emailAdressToAllECAMs, input);
                            Messaging.SingleEmailMessage ClienteMail = getEmailForRequester ( input);
                            System.Debug('Emails : gen, client');
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { GenericEcamMail, ClienteMail });
                        }
                        else {
                            Messaging.SingleEmailMessage GenericEcamMail = getEmailForGenericECAM ( emailAdressToAllECAMs, input);
                            System.Debug('Emails : gen');
                            
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { GenericEcamMail }); 
                        }
                    }
                    else if(responsibleEcamList.size() > 0){
                        // FOUND ECAM PROCEDS TO NOTIFY ECAM
                        String[] toAddresses = new String[] {};
                            
                            for(AccountTeamMember accT : responsibleEcamList){
                                List<User> responsibleEcam  = [ select Email  from User where Id = :accT.UserId  ];
                                toAddresses.add(responsibleEcam[0].Email);
                            }
                        System.debug('Found Emails: '+ toAddresses);
                        
                        if (toAddresses.Size() > 0){
                            if(input.Send_RFP_to_requester__c == true){
                                Messaging.SingleEmailMessage EcamMail 	 = getEmailForEspecificECAM (toAddresses, acc, input);
                                Messaging.SingleEmailMessage ClienteMail = getEmailForRequester ( input);
                                System.Debug('Emails : ecam, client');
                                
                                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { EcamMail, ClienteMail });
                            }
                            else {
                                Messaging.SingleEmailMessage EcamMail = getEmailForEspecificECAM (toAddresses, acc, input);
                                System.Debug('Emails : ecam');
                                
                                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { EcamMail }); 
                            }
                            
                            
                        }
                    }
                }
                else{
                    // DIDNT FOUND CONTACT
                    if(input.Send_RFP_to_requester__c == true){
                        Messaging.SingleEmailMessage GenericEcamMail 	 = getEmailForGenericECAM (emailAdressToAllECAMs, input);
                        Messaging.SingleEmailMessage ClienteMail = getEmailForRequester ( input);
                        
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { GenericEcamMail, ClienteMail });
                    }
                    else {
                        Messaging.SingleEmailMessage GenericEcamMail = getEmailForGenericECAM (emailAdressToAllECAMs, input);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { GenericEcamMail }); 
                    }
                }
            }
        }
        
    }
    
    public Messaging.SingleEmailMessage getEmailForEspecificECAM (String[] toAddresses, Account acc, InputForm__c input){
        
        // Creating Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Embraer Commercial Aviation');
        mail.setSubject(acc.Company_Nickname__c + ' - There is a new request to be evaluated');
        
        // Creating URL to input form
        string inputFormLink = URL.getOrgDomainUrl().toExternalForm() + '/' + input.Id;
        
        // Creating HTML to email body
        String htmlBodyEmailECAM = '';
        htmlBodyEmailECAM +='<style>.disclaimer    {font-style: italic;font-size: .8em;font-family: Arial, Helvetica, sans-serif;color: #606060; font-weight: light;}.container {margin-left:25px; width:800px;}body {background-color:#f1f1f1;}h1   {font-family: "Lucida Console", Courier, monospace; color: Black;}p    {font-family: Arial, Helvetica, sans-serif;color: black;}.question-title {font-weight: bold; Color: #009898; margin-bottom: 1rem;}</style>'+
            '<p>A new Customer RFP was created for ' + acc.Name + '.</p>' +
            '<p>'+ input.Requester_Name__c + ' made a request for proposal for ' + input.Subject__c + ' available on following link:<br>' +
            '<a href="'+ inputFormLink +'">Redirect for RFP in Salesforce.</a></p>' +
            '<p>The account team will continue with the RFP process.</p>' ;
        if(input.Send_RFP_to_requester__c){
            htmlBodyEmailECAM += '<p>A copy of the RFP was sent to the requester\'s email.</p>';
        }
        htmlBodyEmailECAM += '<p class="disclaimer">This email is directed to the account team responsible for ' + input.Requester_Company__c +'. If you are not part of this team, please notify them through the '+emailAdressToAllECAMs+'.</p>';
        
        // Setting Email Body
        mail.setHtmlBody(htmlBodyEmailECAM);
        System.Debug('Email ECAM, Addresses '+ toAddresses);
        
        return mail;
    }
    
    public Messaging.SingleEmailMessage getEmailForGenericECAM (String emailDestination, InputForm__c input){
        
        // Creating Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailDestination};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Embraer Commercial Aviation');
        mail.setSubject(input.Requester_Company__c + ' - There is a new RFP to be evaluated');
        
        // Creating URL to input form
        string inputFormLink = URL.getOrgDomainUrl().toExternalForm() + '/' + input.Id;
        
        // Creating HTMl for Email Body
        String htmlBodyEmailNoECAM ='';
        htmlBodyEmailNoECAM +='<style>p    {font-family: Arial, Helvetica, sans-serif;color: black;}.disclaimer    {font-style: italic;font-size: .8em;font-family: Arial, Helvetica, sans-serif;color: #606060; font-weight: light;}.container {margin-left:25px; width:800px;}body {background-color:#f1f1f1;}h1   {font-family: "Lucida Console", Courier, monospace; color: Black;}.text    {font-family: Arial, Helvetica, sans-serif;color: #606060;}.question-title {font-weight: bold; Color: #009898; margin-bottom: 1rem;}</style>'+
            '<p>A new Customer RFP was created for ' + input.Requester_Company__c + '.</p>' +
            '<p>The system was not able to link the requester to an especific contact and/or account.</p>'+
            '<p>The contact information of the Customer RFP is:'+
            '<p>Requester Company: ' + input.Requester_Company__c + 
            '<br>Requester Name: '+ input.Requester_Name__c + 
            '<br>Requester Email: '+ input.Requester_Email__c +
            '<br><br>RFP available on following link:<br>' +
            '<a href="'+ inputFormLink +'">Redirect for RFP in Salesforce.</a></p>' +
            '<p>If you identify this account to be under your responsibility, please continue with the RFP process and notify the other account team members.</p>' ;
        if(input.Send_RFP_to_requester__c){
            htmlBodyEmailNoECAM += '<p>A copy of the RFP was sent to the requester\'s email.</p>';
        }
        htmlBodyEmailNoECAM += '<p class="disclaimer">This email is directed to the account team responsible for ' + input.Requester_Company__c +'. If you are not part of this team, please notify them through the '+emailAdressToAllECAMs+'.</p>';
        
        // Setting Email Body
        mail.setHtmlBody(htmlBodyEmailNoECAM);
        System.Debug('Email Generic, Addresses '+ emailDestination);

        return mail;
    }
    
    public Messaging.SingleEmailMessage getEmailForRequester ( InputForm__c input){
        
        // Creating Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {input.Requester_Email__c};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Embraer Commercial Aviation');
        mail.setSubject('RFP received by Embraer Commercial Aviation');
        
        // Creating HTMl for Email Body
        
        String htmlBodyEmailRequester = '<style>.disclaimer    {font-style: italic;font-size: .8em;font-family: Arial, Helvetica, sans-serif;color: #606060; font-weight: light;}.container {margin-left:25px; width:800px;}body {background-color:#f1f1f1;}h3 {font-family: Arial, Helvetica, sans-serif; font-weight: 900; Color: black; margin-bottom: 1rem;}.text    {font-family: Arial, Helvetica, sans-serif;color: #606060;}.question-title {font-weight: bold; Color: #009898; margin-bottom: 1rem;}</style>' +
            '<p> You have created a request for proposal entitled '+ input.Subject__c + '.</p>'+
            
            '<div class="container">' +
            '<h2> Request for proposal</h2>'+
            '<h3> Proposal Details</h3>'+
            '<p class="question-title">Subject:</p>' + 
            '<p class="text">' + input.Subject__c + '</p>' +
            
            '<p class="question-title">Description of desired Service or Product Change Request:</p>' + 
            //'<p>When applicable, sketches, drawings, pictures or specs that can support clarification are welcome. If product change comprises an EJET re-configuration, a specific check list “RCL” must be provided. (Re-configuration means: Pitch re-arrangement, seats inclusion/removal, monuments changes, authority conversion).</p>' +
            '<p class="text">' + input.Description__c  + '</p>' +
            
            '<p class="question-title">Reason for Request and Expected Benefits:</p>' +
            '<p class="text">' + input.Reason_for_Request_and_Expected_Benefits__c  + '</p>' +
            
            '<p class="question-title">Type of Proposal Required: </p>' +
            '<p class="text">' + input.Required_proposal_type__c  + '</p>' +
            
            '<p class="question-title">Airplane Type (s): </p>' +
            '<p class="text">' + input.Aircraft_Model__c  + '</p>' +
            
            '<p class="question-title">Quantity of Airplanes Affected: </p>' +
            '<p class="text">' + input.QtyOfAircraft__c  + '</p>' +
            
            '<p class="question-title">Airplanes Identification (Serial Numbers):</p>' +
            '<p class="text">' + input.AircraftSNANDTail__c  + '</p>' +
            '<h3> Customer Expectations</h3>'+
            
            '<p class="question-title">Would you like to have mod implemented in production line too?</p>' +
            '<p class="text">' + input.Required_for_production_line__c  + '</p>' +
            
            '<p class="question-title">Expected time to the have mod or service available:</p>' +
            '<p class="text">' + input.Expected_Availability_Time__c  + '</p>' +
            
            '<p class="question-title">If the time choosen above is not acceptable, please provide us a timeline and a justification for internal evaluation:</p>' +
            '<p class="text">' + input.Expected_Availability_Time_Justification__c  + '</p>' +
            
            '<p class="question-title">Is there an expectation to implement it during a check? Specify when.</p>' +
            '<p class="text">' + input.Expected_Implement_During_Check__c  + '</p>' +
            
            '<p class="question-title">Expected price for this mod or service:</p>' +
            '<p class="text">' + input.Expected_Price_for_mod_or_service__c  + '</p>' +
            
            '<p class="question-title">Are you looking for an Embraer complete solution (development with parts)?</p>' +
            '<p class="text">' + input.Looking_for_Complete_Solution__c  + '</p>' +
            
            '<p class="question-title">Will there be another solution being implemented with the one requested in this RFP? Please, provide us more details:</p>' +
            '<p class="text">' + input.Other_Solutions__c  + '</p>' +
            
            '<p class="question-title">Please, provide us more details about the implementation plans (if there will be another solution being implemented with the one requested in this RFP, please, mention it and specify how you intend to implement them):</p><p class="text">' + input.Detail_Other_Solutions__c  + '</p>' +
            
            '<p class="question-title">Would you be interested in having this solution as part of a package (with another solution)? Please, provide us more details:</p>' +
            '<p class="text">' + input.Package_With_other_Solutions__c  + '</p>' +
            
            '<p class="question-title">Please, provide us the following information for the affected fleet (if avionics related):</p><br/>' +
            //'<p class="text" >- APM options PN;- Desired Load to be installed with the implementation of this request;<br/>- DVDR was factory incorporated or modified through a SB (if not sure, inform us the DVDR PN);<br/>- Aircraft Pentium configuration currently installed.</p>' +
            '<p class="text">' + input.Avionics_Details__c  + '</p>' +
            '<h3> Requester information</h3>'+
            
            '<p class="question-title">Name:</p>' +
            '<p class="text">' + input.Requester_Name__c  + '</p>' +
            
            '<p class="question-title">Company:</p>' +
            '<p class="text">' + input.Requester_Company__c  + '</p>' +
            
            '<p class="question-title">Position:</p>' +
            '<p class="text">' + input.Requester_Position__c  + '</p>' +
            
            '<p class="question-title">Email:</p>' +
            '<p class="text">' + input.Requester_Email__c  + '</p>' +
            
            '<p class="question-title">Phone:</p>' +
            '<p class="text">' + input.Requester_Phone__c  + '</p>' +
            
            '<p class="question-title">Send copy of RFP to requester:</p>' +
            '<p class="text" >If this checkbox is checked the requester will recieve a email with the information on this page.<br/></p>' +
            '<p class="text">' + input.Send_RFP_to_requester__c + '</p></div>'+
            '<p class="disclaimer">Soon an Embraer account representative shall contact you.</p>';
        
        // Setting Email Body
        mail.setHtmlBody(htmlBodyEmailRequester);
        System.Debug('Client Email');

        return mail;
    }
}