@isTest
public class FO_HomeControllerTest {
    
    static testMethod void test(){
        
        test.startTest();
        
        FO_HomeController hc = new FO_HomeController();
        hc.runCaseSearch();
        hc.toggleSortCase();
        
        hc.runTaskSearch();
        hc.toggleSortTask();
        
        hc.runCaseFUPSearch();
        hc.toggleSortCaseFUP();
        
        FO_HomeController.wrapper w = new FO_HomeController.wrapper();
        w.t = new Task();
        w.c = new Case();
                            
        test.stopTest();
    }

}