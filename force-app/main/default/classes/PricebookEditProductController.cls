/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Controller class for the visualforce page PricebookEditProduct
*
* NAME: PricebookEditProductController.cls
* AUTHOR:JFS                                                DATE: 08/10/2014
*
* Alterações solicitadas pelo Eduardo
* AUTHOR: DPF                                               DATE: 30/10/2014
*
* Alterações solicitadas pelo Eduardo                       DATE: 01/11/2014
* AUTHOR: JFS
*******************************************************************************/
public with sharing class PricebookEditProductController
{
  private static final Id SRecProductTailored = RecordTypeMemory.getRecType('Product2', 'Tailored');
  private static final Id SRecProductPilots = RecordTypeMemory.getRecType('Product2', 'Pilot_services');
  private static final Id SRecProductTraining = RecordTypeMemory.getRecType('Product2', 'Training');
  
  public id RecProductTailored { get { return SRecProductTailored; } }
  public id RecProductPilots { get { return SRecProductPilots; } }
  public id RecProductTraining { get { return SRecProductTraining; } }

  public list<PricebookEntryWrapper> Pricebookprodutos {get;set;}

  public list<SelectOption> moedas { get; set; }

  public Id IdPricebook;
  public Boolean isStandard { get; set; }
  public Boolean isNew { get; set; }
  //Alterações solicitadas pelo Eduardo
  public Double annualEscalation { get; set; }
  
  public String labelPrice1 { get; set; }
  public String labelPrice2 { get; set; }
  public String labelPrice3 { get; set; }
  
  private static final Map<String, String> mapRecordType = new Map<String, String>{
     'Aircraft Modification / Technical Services' => 'Aircraft_Modification',
     'eSolutions' => 'eSolutions',
     'Mix Products' => '',
     'Pilot Services / On Site Services' => 'Pilot_services',
     'Tailored' => '',
     'Training' => 'Training',
     'Material Solutions' => 'MaterialSolutions',
     'Maintenance Services' => 'MaintenanceServices',
     'Technical Services' => 'TechnicalServices'
  };

  public class PricebookEntryWrapper
  {
    public Id RecordTypeId { get; set; }
    public String productName { get; set; }
    public String Id { get; set; }
    public String Product2Id { get; set; }
    public String Pricebook2id { get; set; }
    public Boolean UseStandardPrice { get; set; }
    public String UnitPrice { get; set; }
    public String RECPrice { get; set; }
    public Boolean IsActive { get; set; }
    public String Margin_0 { get; set; }
    public String Maximum_Discount { get; set; }
    public String Maximum_Discount_REC { get; set; }
    public String Margin_0_REC { get; set; }
    public String Entry_fee { get; set; }
    public String Maximum_disccount_Entry_fee { get; set; }
    public String CurrencyIsoCode { get; set; }
    public String ProductUnit { get; set; }

    public PricebookEntryWrapper( Id RecordTypeId, String productName, String Id, String Product2Id,
      String Pricebook2id, Boolean UseStandardPrice, String UnitPrice, String RECPrice, Boolean IsActive,
      String Margin_0, String Maximum_Discount, String Maximum_Discount_REC, String Margin_0_REC,
      String Entry_fee, String Maximum_disccount_Entry_fee, String CurrencyIsoCode, String ProductUnit )
    {
      this.RecordTypeId = RecordTypeId;
      this.productName = productName;
      this.Id = Id;
      this.Product2Id = Product2Id;
      this.Pricebook2id = Pricebook2id;
      this.UseStandardPrice = UseStandardPrice;
      this.UnitPrice = UnitPrice;
      this.RECPrice = RECPrice;
      this.IsActive = IsActive;
      this.Margin_0 = Margin_0;
      this.Maximum_Discount = Maximum_Discount;
      this.Maximum_Discount_REC = Maximum_Discount_REC;
      this.Margin_0_REC = Margin_0_REC;
      this.Entry_fee = Entry_fee;
      this.Maximum_disccount_Entry_fee = Maximum_disccount_Entry_fee;
      this.CurrencyIsoCode = CurrencyIsoCode;
      this.ProductUnit = ProductUnit;

      if(SRecProductTailored == RecordTypeId || SRecProductPilots == RecordTypeId)
      {
        this.Entry_fee = '0';
      }
    }
  }

  public PricebookEditProductController(ApexPages.StandardController controller)
  {
    IdPricebook = controller.getId();
    moedas = new list<SelectOption>();
    moedas.add(new SelectOption('USD', 'U.S. Dollar'));

    for (Schema.PicklistEntry a : PricebookEntry.getSObjectType().getDescribe().fields.getmap().get('CurrencyIsoCode').getDescribe().getPicklistValues() )
    {
      if ( a.getValue() == 'USD' ) continue;
      moedas.add(new SelectOption(a.getValue(), a.getLabel()));
    }

    list<Pricebook2> lstPb2 = [SELECT isStandard, Type__c FROM Pricebook2 WHERE Id =: IdPricebook];
    if ( !lstPb2.isEmpty() )
    {
    	isStandard = lstPb2[0].isStandard;
    	labelPrice1 = Utils.returnLabelField(mapRecordType.get(lstPb2[0].Type__c), 'Price1__c');
      labelPrice2 = Utils.returnLabelField(mapRecordType.get(lstPb2[0].Type__c), 'Price2__c');
      labelPrice3 = Utils.returnLabelField(mapRecordType.get(lstPb2[0].Type__c), 'Price3__c');
    }

    Pricebookprodutos = new list<PricebookEntryWrapper>();

    String ids = apexpages.currentPage().getParameters().get('lstaPb');
    if ( ids != null )
    {
      list<String> lstaPb = new list<String>();
      for ( String id : ids.split(',')) lstaPb.add(id);
      SelectProduct(lstaPb);
      isNew = true;
    }
    else
    {
      for ( PricebookEntry iPbe : [SELECT Id, name, Product2Id, ProductCode, Pricebook2id,
        Pricebook2.name, Pricebook2.Annual_Escalation__c, UseStandardPrice, Entry_fee__c,
        UnitPrice, RECPrice__c,Maximum_Disccount_Entry_fee__c,Maximum_Discount_REC__c, IsActive,
        Margin_0__c, Maximum_Discount__c, Product2.Name, Product2.RecordTypeId, Margin_0_REC__c,
        CurrencyIsoCode, Product2.Unit__c
        FROM PricebookEntry
        WHERE Pricebook2id = :IdPricebook
        order BY Pricebook2.IsStandard desc, Product2.Name ] )
      {
        PricebookEntryWrapper pbeWrapper = new PricebookEntryWrapper(
          iPbe.Product2.RecordTypeId,
          iPbe.Product2.Name,
          iPbe.Id,
          iPbe.Product2Id,
          iPbe.Pricebook2id,
          iPbe.UseStandardPrice,
          String.valueOf( iPbe.UnitPrice ),
          String.valueOf( iPbe.RECPrice__c ),
          iPbe.IsActive,
          String.valueOf( iPbe.Margin_0__c ),
          String.valueOf( iPbe.Maximum_Discount__c ),
          String.valueOf( iPbe.Maximum_Discount_REC__c ),
          String.valueOf( iPbe.Margin_0_REC__c ),
          String.valueOf( iPbe.Entry_fee__c ),
          String.valueOf( iPbe.Maximum_Disccount_Entry_fee__c ),
          iPbe.CurrencyIsoCode,
          iPbe.Product2.Unit__c
        );

        Pricebookprodutos.add(pbeWrapper);
      }
    }
  }

  public PageReference mysave()
  {
  	//retorno caso não ocorra erros
    PageReference pageRef = new PageReference('/' + IdPricebook);
    pageRef.setRedirect(true);
    
  	list<Id> lstIdProduto = new list<Id>();
  	for (PricebookEntryWrapper iPbeWrapper : Pricebookprodutos )
    {
      if ( !iPbeWrapper.useStandardPrice && String.isBlank(iPbeWrapper.UnitPrice) )
      {
        adicionaErro('List price - NREC ($) is required.');
        return null;
      }      
      lstIdProduto.add(iPbeWrapper.Product2Id);
    }
    if ( lstIdProduto.isEmpty() ) return pageRef;
    
    //mapa para pegar valores do catálogo padrão e set para verificar se está tentando inserir registro duplicado.
    map<String, PricebookEntry> mapPB2Standard = new map<String, PricebookEntry>();
    set< String > lSetPBE = new set< String >();
    for ( PricebookEntry pbe : [Select Id, Product2Id, RECPrice__c, UnitPrice,
      Maximum_Discount__c, Maximum_Discount_REC__c, Margin_0__c, Margin_0_REC__c,
      Entry_fee__c, Maximum_disccount_Entry_fee__c, CurrencyIsoCode, Pricebook2.IsStandard,
      Pricebook2Id
      from PricebookEntry
      Where Product2Id =:lstIdProduto ] )
    {
      if ( pbe.Pricebook2.IsStandard ) mapPB2Standard.put(pbe.Product2Id + '|' + pbe.CurrencyIsoCode, pbe);
      lSetPBE.add( pbe.PriceBook2Id + '|' + pbe.Product2Id + '|' + pbe.CurrencyIsoCode );
    }
    
    for ( PricebookEntryWrapper iPbeWrapper : Pricebookprodutos )
    {
    	//verifico se já existe entrada para este produto com a mesma moeda
      if ( lSetPBE.contains( iPbeWrapper.Pricebook2id + '|' + iPbeWrapper.Product2Id + '|' + iPbeWrapper.CurrencyIsoCode ) && iPbeWrapper.Id == null )
      {
        adicionaErro('This price definition already exists in this price book');
        return null;
      }
      
      //verfico se existe entrada de preço no catálogo padrão
      PricebookEntry std = mapPB2Standard.get(iPbeWrapper.Product2Id + '|' + iPbeWrapper.CurrencyIsoCode );
      if ( !isStandard && std == null )
      {
        adicionaErro('There is no Standard Pricebook Entry added with ' + iPbeWrapper.CurrencyIsoCode + ' currency.');
        return null;
      }
    }

    list<PricebookEntry> lstPbe = new list<PricebookEntry>();
    if ( !preencheValidaNumeros( lstPbe,  mapPB2Standard ) ) return null;
    
    Savepoint sp = Database.setSavepoint();
    
    for ( PricebookEntry iPbe : lstPbe )
    {
      if ( annualEscalation != null && !iPbe.useStandardPrice )
      {
        if ( iPbe.unitPrice != null ) iPbe.unitPrice = iPbe.unitPrice + ( iPbe.unitPrice * (annualEscalation/100));
        if ( iPbe.RECPrice__c != null ) iPbe.RECPrice__c = iPbe.RECPrice__c + ( iPbe.RECPrice__c * (annualEscalation/100));
        if ( iPbe.Margin_0__c != null ) iPbe.Margin_0__c = iPbe.Margin_0__c + ( iPbe.Margin_0__c * (annualEscalation/100));
        if ( iPbe.Margin_0_REC__c != null ) iPbe.Margin_0_REC__c = iPbe.Margin_0_REC__c + ( iPbe.Margin_0_REC__c * (annualEscalation/100));
        if ( iPbe.Entry_fee__c != null ) iPbe.Entry_fee__c = iPbe.Entry_fee__c + ( iPbe.Entry_fee__c * (annualEscalation/100));
      }
    }
    if ( !lstPbe.isEmpty())
    {
      Database.Upsertresult[] result = Database.upsert( lstPbe, false );
    
      String erros = '';
      for ( Database.Upsertresult r : result )
      {
        if ( !r.isSuccess() ) erros += r.getErrors()[0].getMessage() + '\n ';
      }
      if ( String.isNotBlank(erros) )
      {
        adicionaErro(erros);
        Database.rollback(sp);
        return null;
      }
    }

    if ( !Pricebookprodutos.isEmpty() )
    {
      Pricebook2 prd = new Pricebook2();
      prd.Id = Pricebookprodutos[0].pricebook2Id;
      prd.Annual_Escalation__c = annualEscalation;
      update prd;
    }

    return pageRef;
  }

  private void SelectProduct(list<String> lstaPb)
  {
    PricebookEntry pbeStandard = null;

    list < Product2 > lstPb2 = [SELECT Id, RecordTypeId, Name, Unit__c  FROM Product2 WHERE id =:lstaPb];

    map <String, PricebookEntry > mapPbeStd = new map < String, PricebookEntry >();
    for(PricebookEntry PbeStd: [SELECT Product2Id, RECPrice__c, UnitPrice,
      Maximum_Discount__c, Maximum_Discount_REC__c, Margin_0__c, Margin_0_REC__c,
      Entry_fee__c, Maximum_disccount_Entry_fee__c, CurrencyIsoCode
      FROM PricebookEntry 
      WHERE Pricebook2.IsStandard = true and Product2Id =: lstPb2])
    {
      mapPbeStd.put(PbeStd.Product2Id, PbeStd);
    }

    Pricebookprodutos = new list<PricebookEntryWrapper>();

    for(Product2 lprd: lstPb2)
    {
      pbeStandard = mapPbeStd.get(lprd.id);
      if (pbeStandard == null) continue;

      PricebookEntryWrapper pbeWrapper = new PricebookEntryWrapper(
        lprd.RecordTypeId,
        lprd.Name,
        null,
        lprd.Id,
        String.valueOf(IdPricebook),
        false,
        pbeStandard != null ? ( pbeStandard.UnitPrice != null ? String.valueOf(pbeStandard.UnitPrice) : null ) : null ,
        pbeStandard != null ? ( pbeStandard.RECPrice__c != null ? String.valueOf(pbeStandard.RECPrice__c) : null )  : null,
        true,
        pbeStandard != null ? ( pbeStandard.Margin_0__c != null ? String.valueOf(pbeStandard.Margin_0__c) : null )  : null,
        pbeStandard != null ? ( pbeStandard.Maximum_Discount__c != null ? String.valueOf(pbeStandard.Maximum_Discount__c) : null )  : null,
        pbeStandard != null ? ( pbeStandard.Maximum_Discount_REC__c != null ? String.valueOf(pbeStandard.Maximum_Discount_REC__c) : null )  : null,
        pbeStandard != null ? ( pbeStandard.Margin_0_REC__c != null ? String.valueOf(pbeStandard.Margin_0_REC__c) : null )  : null,
        pbeStandard != null ? ( pbeStandard.Entry_fee__c != null ? String.valueOf(pbeStandard.Entry_fee__c) : null )  : null,
        pbeStandard != null ? ( pbeStandard.Maximum_disccount_Entry_fee__c != null ? String.valueOf(pbeStandard.Maximum_disccount_Entry_fee__c) : null )  : null,
        null,
        lprd.Unit__c 
      );
      Pricebookprodutos.add(pbeWrapper);
    }
  }
  
  private void adicionaErro(String msg)
  {
    ApexPages.addMessage( new ApexPages.Message(ApexPages.severity.ERROR, msg) );
  }
  
  private Boolean preencheValidaNumeros( List<PricebookEntry> lstPbe, map<String, PricebookEntry> mapPB2Standard  )
  {
  	try {
  		for ( PricebookEntryWrapper iPbeWrapper : Pricebookprodutos )
  		{
  			PricebookEntry std = mapPB2Standard.get(iPbeWrapper.Product2Id + '|' + iPbeWrapper.CurrencyIsoCode );
  			if ( std == null ) continue;
  			
  			PricebookEntry pbe = new PricebookEntry();
	      pbe.Id = iPbeWrapper.Id;
	      if ( pbe.Id == null )
	      {
	        pbe.Pricebook2Id = iPbeWrapper.Pricebook2id;
	        pbe.Product2Id = iPbeWrapper.Product2Id;
          pbe.CurrencyIsoCode = iPbeWrapper.CurrencyIsoCode;
	      }
	      pbe.IsActive = iPbeWrapper.IsActive;
	      pbe.UseStandardPrice = iPbeWrapper.UseStandardPrice;
	      
	      if ( pbe.UseStandardPrice )
	      {
	        pbe.UnitPrice = std.UnitPrice;
	        pbe.RECPrice__c = std.RECPrice__c;
	        pbe.Margin_0__c = std.Margin_0__c;
	        pbe.Maximum_discount__c = std.Maximum_discount__c;
	        pbe.Maximum_discount_REC__c = std.Maximum_discount_REC__c;
	        pbe.Margin_0_REC__c = std.Margin_0_REC__c;
	        pbe.Entry_fee__c = std.Entry_fee__c;
	        pbe.Maximum_disccount_Entry_fee__c = std.Maximum_disccount_Entry_fee__c;
	      }
	      else
	      {
		      pbe.UnitPrice = utils.formatNumber(iPbeWrapper.UnitPrice);
		      pbe.RECPrice__c = utils.formatNumber(iPbeWrapper.RECPrice);
		      pbe.Margin_0__c = utils.formatNumber(iPbeWrapper.Margin_0);
		      pbe.Maximum_discount__c = utils.formatNumber(iPbeWrapper.Maximum_Discount);
		      pbe.Maximum_discount_REC__c = utils.formatNumber(iPbeWrapper.Maximum_Discount_REC);
		      pbe.Margin_0_REC__c = utils.formatNumber(iPbeWrapper.Margin_0_REC);
		      pbe.Entry_fee__c = utils.formatNumber(iPbeWrapper.Entry_fee);
		      pbe.Maximum_disccount_Entry_fee__c = utils.formatNumber(iPbeWrapper.Maximum_disccount_Entry_fee);
	      }  
	      lstPbe.add(pbe);
  		}
  	}
    catch( Exception e )
    {
    	adicionaErro(e.getMessage());
    	return false;
    }
    return true;
  }
}