@isTest
private class UpdatePartsInBackOrderTest {
    @testSetup static void setup() {
        List<Account> accounts = new List<Account>();
        List<LLPDatabase__c> parts = new List<LLPDatabase__c>();
        List<Case> cases = new List<Case>();
        
        //create accounts
        accounts.add(new Account(Name='test acct fll', Company_Nickname__c='test acct fll', PRC_Support_Plant__c='FLL'));
        accounts.add(new Account(Name='test acct lbg', Company_Nickname__c='test acct lbg', PRC_Support_Plant__c='LBG'));
        accounts.add(new Account(Name='test acct sin', Company_Nickname__c='test acct sin', PRC_Support_Plant__c='SIN'));
        accounts.add(new Account(Name='test acct sjk', Company_Nickname__c='test acct sjk', PRC_Support_Plant__c='SJK'));
        insert accounts;
        
        //create parts
        parts.add(new LLPDatabase__c(Name='104003-2', Description__c='test', Ecode__c='3883442', Top_Most__c='3883442', Is_Part_Serialized__c='Serialized'));
        parts.add(new LLPDatabase__c(Name='104003-1', Description__c='test', Ecode__c='3883441', Top_Most__c='3883442', Is_Part_Serialized__c='Serialized'));
        parts.add(new LLPDatabase__c(Name='1116-42-1116 MODS 604', Description__c='test', Ecode__c='2074610', Top_Most__c='9408215', Is_Part_Serialized__c='Serialized'));
        insert parts;
        
        //create cases
        Id rTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pool Exchanges').getRecordTypeId();
        cases.add(new Case(RecordTypeId=rTypeId, Subject='test case fll', Description='is back order', AccountId=accounts[0].Id, Part_Number__c=parts[1].Id));
        cases.add(new Case(RecordTypeId=rTypeId, Subject='test case lbg', Description='is back order', AccountId=accounts[1].Id, Part_Number__c=parts[2].Id));
        cases.add(new Case(RecordTypeId=rTypeId, Subject='test case sin', Description='is back order', AccountId=accounts[2].Id, Part_Number__c=parts[2].Id));
        cases.add(new Case(RecordTypeId=rTypeId, Subject='test case sjk', Description='is back order', AccountId=accounts[3].Id, Part_Number__c=parts[2].Id));
    	insert cases;
        
        //update phase to back order for the test cases
        List<Case> casesToUpdate = [SELECT Id, Phase__c FROM Case];
        for(Case c : casesToUpdate){
            c.Phase__c = 'Back Order';
        }
        update casesToUpdate;
    }
    
    @isTest static void test() {
        Test.startTest();
        	UpdatePartsInBackOrder.Run();
        Test.stopTest();
        // after the testing stops, assert records were updated properly
        System.assertEquals(2, [SELECT count() FROM LLPDatabase__c WHERE Is_On_Back_Order_FLL__c = true]);	//should be two parts with FLL back orders
        System.assertEquals(1, [SELECT count() FROM LLPDatabase__c WHERE Is_On_Back_Order_LBG__c = true]);	//should be one part with LBG back orders
        System.assertEquals(1, [SELECT count() FROM LLPDatabase__c WHERE Is_On_Back_Order_SIN__c = true]);	//should be one part with SIN back orders
        System.assertEquals(1, [SELECT count() FROM LLPDatabase__c WHERE Is_On_Back_Order_SJK__c = true]);	//should be one part with SJK back orders
    }
    @isTest static void testSchedule(){
        Test.startTest();
			String jobId = System.schedule('Test Job Name', '0 0 0 15 3 ? 2022', new ScheduleUpdatePartsInBackOrder());
        Test.stopTest();
        // after the testing stops, assert was scheduled
        System.assert([SELECT count() FROM CronTrigger WHERE Id = :jobID] > 0);
    }
}