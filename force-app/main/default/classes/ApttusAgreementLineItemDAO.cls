/* Classe implementadora de SOBjectDAO para operações DML no objeto Apttus__AgreementLineItem__c, utilizando pattern de Singleton.
* @author - Elvia Serpa
*           elvsantos@deloitte.com
* @version 1.0 18/01/2016
*/
public with sharing class ApttusAgreementLineItemDAO
{

    private static final ApttusAgreementLineItemDAO instance = new ApttusAgreementLineItemDAO();

    private ApttusAgreementLineItemDAO(){
    }

    public static ApttusAgreementLineItemDAO getInstance(){
        return instance;
    }


    public List<Apttus__AgreementLineItem__c> getApttusAgreementLineItem(Set<String> setRelatedAgreement)
    {
        List<Apttus__AgreementLineItem__c> lstApttusAgreementLineItem = [ Select
                                                                            id ,
                                                                            Apttus__AgreementId__c ,
                                                                            Basic_Price__c
                                                                          From
                                                                             Apttus__AgreementLineItem__c
                                                                          Where
                                                                             Apttus__AgreementId__c IN :setRelatedAgreement
                                                                          And
                                                                             Delivered__c = false
                                                                          And
                                                                            Stop_Escalation__c = false
                                                                          And
                                                                             Apttus__AgreementId__r.Apttus__Status__c <> 'Terminated'
                                                                          And
                                                                             Apttus__AgreementId__r.Apttus__Status__c <> 'Cancelled Request' ];
        return lstApttusAgreementLineItem;
    }

    public List<Apttus__AgreementLineItem__c> getApttusAgreementLineItemById (String relatedAgreement)
    {
        List<Apttus__AgreementLineItem__c> lstApttusAgreementLineItem = [ Select
                                                                             Id ,
                                                                             Escalated_Price__c ,
                                                                             Delivery_Date__c ,
                                                                             PDP_already_set__c ,
                                                                             Basic_Price__c ,
                                                                             Dellivery_Month__c
                                                                           From
                                                                             Apttus__AgreementLineItem__c
                                                                           Where
                                                                             Apttus__AgreementId__c = :relatedAgreement  ];
        return lstApttusAgreementLineItem;
    }

    public List<Apttus__AgreementLineItem__c> getAgreementLineItem(Set<String> setAgreementId)
    {
        List<Apttus__AgreementLineItem__c> lstAgreementLineItem =  [Select
                                                                        Aircraft_Configuration_Code__c ,
                                                                        Apttus__AgreementId__c ,
                                                                        Order_Type__c
                                                                     From
                                                                        Apttus__AgreementLineItem__c
                                                                     Where
                                                                        Apttus__AgreementId__c IN : setAgreementId ];
        return lstAgreementLineItem;
    }

    public List<Apttus__AgreementLineItem__c> getAgreementLineItemByAgreementId(Map<String, Apttus__APTS_Agreement__c> mapAgreementAptt)
    {
            List<Apttus__AgreementLineItem__c>  lstAgreementLineItem = [ Select AFA__c ,
                                                                                Apttus__AgreementId__c ,
                                                                                Aircraft__c ,
                                                                                Aircraft_Configuration__c ,
                                                                                Aircraft_Configuration_Code__c ,
                                                                                Aircraft_Name__c ,
                                                                                Basic_Price__c ,
                                                                                Comments__c ,
                                                                                Contract_Aircraft_Number__c ,
                                                                                //Date_to_Exercise_Option__c ,
                                                                                Delivered__c ,
                                                                                Delivery_Date__c ,
                                                                                Dellivery_Month__c ,
                                                                                Discount_or_Increase__c ,
                                                                                Escalated_Price__c ,
                                                                                Final_Price__c ,
                                                                                List_Price__c ,
                                                                                Option_Execution_Date__c ,
                                                                                Option_In_Execution__c ,
                                                                                Order_Type__c ,
                                                                                PDP_already_set__c ,
                                                                                Stop_Escalation__c ,
                                                                                CurrencyIsoCode,
                                                                                Status__c
                                                                            From
                                                                                Apttus__AgreementLineItem__c
                                                                            Where
                                                                                Apttus__AgreementId__c IN :mapAgreementAptt.KeySet() ];
        return lstAgreementLineItem;
    }
}