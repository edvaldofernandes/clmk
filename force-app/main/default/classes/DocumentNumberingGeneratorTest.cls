@isTest
/*
 * Test Class for DocumentNumberingGenerator, DocumentNumberingTriggerHandler and DocumentNumberingTrigger
*/
public class DocumentNumberingGeneratorTest {
    static testMethod void creatingDocuments(){
        test.startTest();
        Document_Numbering__c dn1 = new Document_Numbering__c();
        dn1.Name = 'Teste1';
        dn1.Type__c = 'Other';
        database.insert(dn1);
        Document_Numbering__c dn2 = new Document_Numbering__c();
        dn2.Name = 'Teste2';
        dn2.Type__c = 'Other';
        Document_Numbering__c dn3 = new Document_Numbering__c();
        dn3.Name = 'Teste3';
        dn3.Type__c = 'Other';
        database.insert(new List<Document_Numbering__c>{dn2,dn3});
        test.stopTest();
        
		system.assert(([SELECT Id, Number__c FROM Document_Numbering__c WHERE Name = 'Teste1' LIMIT 1][0].Number__c.split('-'))[0] == '0001');
		system.assert(([SELECT Id, Number__c FROM Document_Numbering__c WHERE Name = 'Teste3' LIMIT 1][0].Number__c.split('-'))[0] == '0003');
        database.delete([SELECT Id FROM Document_Numbering__c LIMIT 3]);
        system.assert([SELECT Id FROM Document_Numbering__c].isEmpty());
    }
}