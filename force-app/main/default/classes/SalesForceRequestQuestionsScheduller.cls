global class SalesForceRequestQuestionsScheduller implements Schedulable, Database.AllowsCallouts{
        
    global void execute(SchedulableContext SC) {
        
        executeEtrackCallout();
    }
    
    @Future(callout=true)
    public static void executeEtrackCallout(){
        
        SyncLSIETrackOutboundService service = new SyncLSIETrackOutboundService();
        
        if(Test.isRunningTest())
            return;
        
        service.execute();
    }
}