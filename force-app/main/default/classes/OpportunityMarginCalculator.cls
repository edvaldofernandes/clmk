/* Class that handles Margin Calcultor updates. It calculates gross margins based on values from Product Cost Control and Opportunity Products
* @author - Mateus Caviglione
*           mateus.caviglione@embraer.net.br
* @version 2.1 19/02/2020
* Delete
*/

public class OpportunityMarginCalculator {
    
    private Opportunity opp;
    private List<OpportunityLineItem> oppProductList = new List<OpportunityLineItem>();
    private List<Id> oppProductIDList = new List<Id>();
    
    private Set<Id> productIdList = new Set<Id>();
    private List<Product_Cost_Control__c> costControlList;
    private List<ID> PMCIdList = new List<ID>();
    
    private Map<ID,OpportunityLineItem> oppProductMap = new Map<ID,OpportunityLineItem>();
    private Map<ID,Product_Cost_Control__c> costControlMap = new Map<ID,Product_Cost_Control__c>();
    private Map<ID,Product_Margin_Calculation__c> PMCMap = new Map<ID,Product_Margin_Calculation__c>();
    private List<Product_Margin_Calculation__c> oldPMCList = new List<Product_Margin_Calculation__c>();
    
    private Product_Cost_Control__c prodCC = new Product_Cost_Control__c();
    private Product_Margin_Calculation__c prodMC = null;
    private static String ACMODProductRecordType = 'Aircraft Modification';
    //Add Tailred
    private static String TailoredProductRecordType = 'Tailored';

    
    private Id mixedProductOPP_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Mix Products').getRecordTypeId();
    private Id ACMODProductOPP_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
    Private List<Id> availableOpportunityRecordType = new List<Id>();
    private integer calculationStatus = 0;
    
    public OpportunityMarginCalculator(){
    }
    
    public void setAvailableRecordTypes(){
        this.availableOpportunityRecordType.add(this.mixedProductOPP_RecordType);
        this.availableOpportunityRecordType.add(this.ACMODProductOPP_RecordType);
    }
    
    public void setOpportunity(Id opportunityId){
        List<Opportunity> oppLI  = [select ID, NREC_Gross_Margin__c , REC_Gross_Margin__c, Total_Gross_Margin__c, Calculation_Status__c, RecordTypeId
                    from Opportunity
                    where Id = :opportunityId and RecordTypeId in : this.availableOpportunityRecordType limit 1];
        if(oppLI.size() != 0){
            this.opp = oppLI[0];
        }
        else{
            this.opp = null;
        }
    }
    
    public List<OpportunityLineItem> setProductsFromOpportunity(Id opportunityId){
        List<OpportunityLineItem> productList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> ACMODproductList = new List<OpportunityLineItem>();
        
        productList = [ SELECT Id, Product2Id, Quantity, UnitPrice, Entry_fee__c, Princing__c, CreatedById, OpportunityId, Total_Amount__c, Product_record_type__c
                       FROM OpportunityLineItem
                       WHERE OpportunityId = : opportunityId
                      		AND
                      		Princing__c != 'Entry Fee'];
        
        //get all products of opportunity, only put the AC MODS on the list, If Size is diferenct Calc Status -3 (other Products)
        for(OpportunityLineItem oppProd : productList){
            if(oppProd.Product_record_type__c == ACMODProductRecordType || oppProd.Product_record_type__c == TailoredProductRecordType){
                ACMODproductList.add(oppProd);
            }
        }
        if(productList.size() == 0){
            this.calculationStatus = -2;   
            return null; 
        }
        if(ACMODproductList.size() == 0){
            this.calculationStatus = -5;
            return null;
        }
        if(ACMODproductList.size() != productList.size()){
            this.calculationStatus = -3;
        }
        if(ACMODproductList.size() > 0){
            this.oppProductList = ACMODproductList;
            for(OpportunityLineItem oppLI : ACMODproductList){
                this.oppProductMap.put(oppLI.Product2Id, oppLI);
                this.oppProductIDList.add(oppLI.Id);
                this.productIdList.add(oppLI.Product2Id);
            }
            return productList;
        }
        return null;
    }
    
    public List<Product_Cost_Control__c> getCostControlList(){
        List<Product_Cost_Control__c> costControlList;
        costControlList = [ SELECT Amortization_Cost_per_a_c__c, Tech_Pubs_SB_Revision_Costs__c, Sales_Deduction__c, NREC__c, REC__c, Is_Active__c, Related_Product__c
                           FROM Product_Cost_Control__c
                           WHERE Related_Product__c in :this.productIdList and Is_Active__c = :true];
        
        if(costControlList.size() == 0) {
            this.calculationStatus = -1;
            return null;    
        }
        if(costControlList.size() == this.productIdList.size()){
            
            this.costControlList = costControlList;
            for(Product_Cost_Control__c pCCLI : costControlList){
                if(pCCLI.Amortization_Cost_per_a_c__c == null || pCCLI.Tech_Pubs_SB_Revision_Costs__c == null || pCCLI.Sales_Deduction__c == null | pCCLI.REC__c == null){
                    this.calculationStatus = -4;
                }
                this.costControlMap.put(pCCLI.Related_Product__c, pCCLI);
            }
            return costControlList;
        } else {
            this.calculationStatus = -1;
            return null;    
        }
        
    }
    
    public integer calculateMarginForProductList(){
        /*	Repetitive verification
         * if(this.calculationStatus != 0 && this.calculationStatus != -3 ){
            return this.calculationStatus;
        }*/
        
        for (OpportunityLineItem oppProduct :  oppProductList){
            
            this.prodCC = null;
            this.prodMC = null;    
            this.prodCC = costControlMap.get(oppProduct.Product2Id);
            this.prodMC = PMCMap.get(oppProduct.Product2Id);
            
            /*	Repetitive verification
            *if(this.prodCC == null){
                    this.calculationStatus = -2;
            }*/
            if(this.prodMC == null){
                this.prodMC = new Product_Margin_Calculation__c();
                this.prodMC.Related_Opportunity__c = oppProduct.OpportunityId;
                this.prodMC.Related_Product__c = oppProduct.Product2Id;
                this.prodMC.CreatedById = oppProduct.CreatedById;
                //PMCMap.put(this.prodMC.Related_Product__c, this.prodMC);
            }
            
            if(oppProduct.Princing__c == 'NREC'){
                //System.debug('NREC PROCD');
                if(this.prodCC.Amortization_Cost_per_a_c__c != null){
                    this.prodMC.Amortization_Cost_per_a_c__c = this.prodCC.Amortization_Cost_per_a_c__c;
                }
                else{
                    this.prodMC.Amortization_Cost_per_a_c__c = 0;
                }
                if(prodCC.Tech_Pubs_SB_Revision_Costs__c != null){
                    this.prodMC.Tech_Pubs_SB_Revision_cost__c = this.prodCC.Tech_Pubs_SB_Revision_Costs__c;
                }
                else{
                    this.prodMC.Tech_Pubs_SB_Revision_cost__c = 0;
                }
                if(prodCC.Sales_Deduction__c != null){
                    this.prodMC.Sales_Deduction_Percent__c = this.prodCC.Sales_Deduction__c;
                }
                else{
                    this.prodMC.Sales_Deduction_Percent__c = 0;
                }
                if(oppProduct.Total_amount__c != null){
                    this.prodMC.Total_Sales_Price_NREC__c = oppProduct.Total_amount__c;
                }
                else{
                    this.prodMC.Total_Sales_Price_NREC__c = 0;
                }
                
                this.prodMC.QTY_Aircraft_for_NREC__c = oppProduct.Quantity;
                this.prodMC.Has_NREC__c = true;
                
            }
            
            else if(oppProduct.Princing__c == 'REC'){
                System.debug('REC PROCD');
                if(prodCC.REC__c != null){
                    this.prodMC.Cost_Price_per_Aircraft_REC__c = this.prodCC.REC__c;
                    this.prodMC.Sales_Price_per_Aircraft_REC__c = oppProduct.UnitPrice;
                    this.prodMC.QTY_Aircraft_for_REC__c = oppProduct.Quantity;
                    this.prodMC.Has_REC__c = true;
                    System.debug(this.prodMC.QTY_Aircraft_for_REC__c + '  ' + this.prodMC.Sales_Price_per_Aircraft_REC__c + '  ' + this.prodMC.Cost_Price_per_Aircraft_REC__c);
                }
                
            }
            // Call Calculations
            this.prodMC = generateMarginCalculation( this.prodMC );
            this.PMCMap.put(this.prodMC.Related_Product__c, this.prodMC);
            
            // Deactivate old Calculations
            // Update All Lists
        }
        
        return 0;
    }
    
    public void setOldMarginCalculations(){
        
        List<Product_Margin_Calculation__c> oldPMCList = [ SELECT Id, Is_Active__c, Related_Product__c, Related_Opportunity__c
                                                          FROM Product_Margin_Calculation__c
                                                          WHERE Is_Active__c = :true and Related_Opportunity__c = :this.opp.Id ];
        
        for(Product_Margin_Calculation__c pmc : oldPMCList){
            pmc.Is_Active__c = false;
            this.oldPMCList.add( pmc);
        }
    }
    
    public Product_Margin_Calculation__c generateMarginCalculation( Product_Margin_Calculation__c pmc){
        System.debug('PMC = '+ PMC.Related_Product__c + ' full :' +  pmc);
        if(pmc.Has_NREC__c == true && pmc.Has_REC__c == false){
            //Procedure for NREC Only
            System.debug('PMC ' + pmc);
            pmc.Sales_Deduction_Total__c =  pmc.Sales_Deduction_Percent__c/100 * pmc.Total_Sales_Price_NREC__c;
            pmc.Net_Revenues__c = pmc.Total_Sales_Price_NREC__c - pmc.Sales_Deduction_Total__c;
            pmc.Total_Cost_Price_NREC__c = 	( pmc.Amortization_Cost_per_a_c__c * pmc.QTY_Aircraft_for_NREC__c ) 		+
                pmc.Tech_Pubs_SB_Revision_cost__c;
            
            
            
            pmc.Total_Cost_Price_REC__c = 0;
            pmc.Total_Proposal_Price_REC__c = 0;
            pmc.Gross_Margin_for_REC__c	 = 0;
            pmc.Gross_Profit_for_NREC__c = pmc.Net_Revenues__c - pmc.Total_Cost_Price_NREC__c;
            pmc.Gross_Profit__c = pmc.Gross_Profit_for_NREC__c;
            if(pmc.Net_Revenues__c != 0){
                pmc.Gross_Margin_for_NREC__c = 100 *  (pmc.Net_Revenues__c - pmc.Total_Cost_Price_NREC__c) / pmc.Net_Revenues__c;
                
            }
            else{
                pmc.Gross_Margin_for_NREC__c = 0;
            }
            if(pmc.Gross_Margin_for_NREC__c < -100){
                pmc.Gross_Margin_for_NREC__c = -100.00;
            }
                //pmc.Gross_Margin_for_NREC__c = 100 * (pmc.Total_Sales_Price_NREC__c - pmc.Total_Cost_Price_NREC__c) / pmc.Total_Sales_Price_NREC__c;
            pmc.Gross_Margin_for_product__c = pmc.Gross_Margin_for_NREC__c;
            
        }
        else if(pmc.Has_NREC__c == false && pmc.Has_REC__c == true){
            //Procedure for REC Only
            
            pmc.Total_Sales_Price_NREC__c = 0;
            pmc.Net_Revenues__c = 0;
            pmc.Total_Cost_Price_NREC__c = 0;
            pmc.Total_Proposal_Price_REC__c = pmc.QTY_Aircraft_for_REC__c * pmc.Sales_Price_per_Aircraft_REC__c;
            pmc.Total_Cost_Price_REC__c = 	  pmc.QTY_Aircraft_for_REC__c * pmc.Cost_Price_per_Aircraft_REC__c;
            
            
            pmc.Gross_Margin_for_NREC__c = 0;
            pmc.Gross_Profit_for_REC__c = pmc.Total_Proposal_Price_REC__c - pmc.Total_Cost_Price_REC__c;
            pmc.Gross_Margin_for_REC__c  = 100 * (pmc.Total_Proposal_Price_REC__c - pmc.Total_Cost_Price_REC__c) / pmc.Total_Proposal_Price_REC__c;
            if(pmc.Gross_Margin_for_REC__c < -100){
                pmc.Gross_Margin_for_REC__c = -100.00;
            }
            pmc.Gross_Margin_for_product__c = pmc.Gross_Margin_for_REC__c;
            pmc.Gross_Profit__c = pmc.Gross_Profit_for_REC__c;

        }
        else if(pmc.Has_NREC__c == true && pmc.Has_REC__c == true){

            pmc.Sales_Deduction_Total__c =  pmc.Sales_Deduction_Percent__c/100 * pmc.Total_Sales_Price_NREC__c;
            pmc.Net_Revenues__c = pmc.Total_Sales_Price_NREC__c - pmc.Sales_Deduction_Total__c;
            pmc.Total_Cost_Price_NREC__c = 	( pmc.Amortization_Cost_per_a_c__c * pmc.QTY_Aircraft_for_NREC__c ) 		+
                pmc.Tech_Pubs_SB_Revision_cost__c;
            
            pmc.Total_Proposal_Price_REC__c = pmc.QTY_Aircraft_for_REC__c * pmc.Sales_Price_per_Aircraft_REC__c;
            pmc.Total_Cost_Price_REC__c = 	  pmc.QTY_Aircraft_for_REC__c * pmc.Cost_Price_per_Aircraft_REC__c;
            
            
            pmc.Gross_Profit_for_REC__c = pmc.Total_Proposal_Price_REC__c - pmc.Total_Cost_Price_REC__c;
            pmc.Gross_Margin_for_REC__c  = 100 * (pmc.Total_Proposal_Price_REC__c - pmc.Total_Cost_Price_REC__c) / pmc.Total_Proposal_Price_REC__c;
            if(pmc.Gross_Margin_for_REC__c < -100){
                pmc.Gross_Margin_for_REC__c = -100.00;
            }
            
            pmc.Gross_Profit_for_NREC__c = pmc.Net_Revenues__c - pmc.Total_Cost_Price_NREC__c;
            if(pmc.Net_Revenues__c != 0){
                pmc.Gross_Margin_for_NREC__c = 100 *  (pmc.Net_Revenues__c - pmc.Total_Cost_Price_NREC__c) / pmc.Net_Revenues__c;
                
            }
            else{
                pmc.Gross_Margin_for_NREC__c = 0;
            }
            if(pmc.Gross_Margin_for_NREC__c < -100){
                pmc.Gross_Margin_for_NREC__c = -100.00;
            }
            
            pmc.Gross_Margin_for_product__c = 100* (((pmc.Net_Revenues__c + pmc.Total_Proposal_Price_REC__c)-(pmc.Total_Cost_Price_NREC__c + pmc.Total_Cost_Price_REC__c))/
                                                    (pmc.Total_Proposal_Price_REC__c + pmc.Net_Revenues__c));
            if(pmc.Gross_Margin_for_product__c < -100){
                pmc.Gross_Margin_for_product__c = -100.00;
            }
            
            /*pmc.Gross_Margin_for_product__c = ((pmc.Net_Revenues__c * pmc.Gross_Margin_for_NREC__c) + 
                                                   (pmc.Total_Proposal_Price_REC__c * pmc.Gross_Margin_for_REC__c)) / (pmc.Net_Revenues__c + pmc.Total_Proposal_Price_REC__c);*/
            pmc.Gross_Profit__c = pmc.Gross_Profit_for_NREC__c + pmc.Gross_Profit_for_REC__c;
            
            
            
            // OLD Procedure for REC and NREC
            /*pmc.Sales_Deduction_Total__c =  pmc.Sales_Deduction_Percent__c/100 * pmc.Total_Sales_Price_NREC__c;
            pmc.Total_Cost_Price_NREC__c = 	pmc.Amortization_Cost_per_a_c__c * pmc.QTY_Aircraft_for_NREC__c 		+
            pmc.Tech_Pubs_SB_Revision_cost__c + pmc.Sales_Deduction_Total__c ;
            
            
            
            pmc.Total_Proposal_Price_REC__c = pmc.QTY_Aircraft_for_REC__c * pmc.Sales_Price_per_Aircraft_REC__c;
            pmc.Total_Cost_Price_REC__c = pmc.Cost_Price_per_Aircraft_REC__c * pmc.QTY_Aircraft_for_REC__c;
            
            pmc.Gross_Margin_for_NREC__c = 100 * (pmc.Total_Sales_Price_NREC__c - pmc.Total_Cost_Price_NREC__c) / pmc.Total_Sales_Price_NREC__c;
            pmc.Gross_Margin_for_REC__c	 = 100 * (pmc.Total_Proposal_Price_REC__c - pmc.Total_Cost_Price_REC__c) / pmc.Total_Proposal_Price_REC__c;
            
            pmc.Gross_Margin_for_product__c	= ((pmc.Total_Sales_Price_NREC__c * pmc.Gross_Margin_for_NREC__c) + (pmc.Total_Proposal_Price_REC__c * pmc.Gross_Margin_for_REC__c))
            /(pmc.Total_Sales_Price_NREC__c + pmc.Total_Proposal_Price_REC__c);
            System.debug('Gross Margins NREC' + pmc.Gross_Margin_for_NREC__c + 'REC' + pmc.Gross_Margin_for_REC__c + ' TOT' + pmc.Gross_Margin_for_product__c );
*/        
        }
        pmc.Is_Active__c = true;
        
        return pmc;
    }
    
    public void updateDataIntoOpportunity(){
        double oppNRECTotalProposal = 0;
        double oppNRECTotalCost = 0; 
        double oppRECTotalProposal = 0;
        double oppRECTotalCost = 0;
        
        Double oppTotalNRECGrossMargin = 0;
        Double oppTotalRECGrossMargin = 0;
        Double oppTotalGrossMargin = 0;
        String status = '';
        
        if(this.calculationStatus == 0 || this.calculationStatus == -3){
            //(oppNRECTotalProposal + oppRECTotalProposal) != 0
            // Query to get info from all the calculators created on last for
            
            
            // Loads data into variables
            //System.debug('PMCMap Size ;' +  PMCMap.size());
            //System.debug('ProductId Size ;' +  productIdList.size());
            
            for (Id key : productIdList ){
                this.prodMC = this.PMCMap.get(key);
                System.debug(PMCMap.values());
                //System.Debug('Calc = '+ c);
                if(this.prodMC != null){
                    //System.debug('This ProdMC :' +  this.prodMC );
                    oppNRECTotalProposal += this.prodMC.Net_Revenues__c; // Opportunity_Net_Revenue__c Total_Cost Gross_Profit
                    oppNRECTotalCost += this.prodMC.Total_Cost_Price_NREC__c;
                    oppRECTotalProposal += this.prodMC.Total_Proposal_Price_REC__c;
                    oppRECTotalCost += this.prodMC.Total_Cost_Price_REC__c;
                    
                }
            }
            // Calculates total margin for the opportunity if it valid
            if(oppNRECTotalProposal != 0){
                oppTotalNRECGrossMargin = (oppNRECTotalProposal - oppNRECTotalCost)* 100 / oppNRECTotalProposal;
                //System.Debug('Opp NREC Total' + oppTotalNRECGrossMargin);
            }
            if(oppRECTotalProposal != 0){
                oppTotalRECGrossMargin = (oppRECTotalProposal - oppRECTotalCost)* 100 / oppRECTotalProposal;
                //System.Debug('Opp REC Total' + oppTotalRECGrossMargin);
            }
            
            if((oppNRECTotalProposal + oppRECTotalProposal) != 0){
                oppTotalGrossMargin = (((oppNRECTotalProposal - oppNRECTotalCost) + (oppRECTotalProposal - oppRECTotalCost)))* 100 / (oppNRECTotalProposal + oppRECTotalProposal);
            }
            status = 'Valid';
            this.opp.Opportunity_Net_Revenue__c = oppNRECTotalProposal + oppRECTotalProposal;
            this.opp.Total_Cost__c = oppNRECTotalCost + oppRECTotalCost;
            this.opp.Gross_Profit__c = this.opp.Opportunity_Net_Revenue__c - this.opp.Total_Cost__c;
            if(this.calculationStatus == -3){
                status = 'Valid, but there are products that arent aicraft modifications and will not be considered in the gross margin calculation.';
            }
            
        } else If (this.calculationStatus == -1){
            // If status is invalid it will reset all margins
            oppTotalNRECGrossMargin = 0;
            oppTotalRECGrossMargin = 0;
            oppTotalGrossMargin = 0;
            status = 'All Products must have a Product Cost Control associated with it. At least one does not have a cost associated to the product. Please contact your BD.';
        } else If (this.calculationStatus == -4){
            // If status is invalid it will reset all margins
            oppTotalNRECGrossMargin = 0;
            oppTotalRECGrossMargin = 0;
            oppTotalGrossMargin = 0;
            status = 'All Product Cost Control must be completed. At least one does not have the required values. Please contact your BD.';
        } else if (this.calculationStatus == -2){
            oppTotalNRECGrossMargin = 0;
            oppTotalRECGrossMargin = 0;
            oppTotalGrossMargin = 0;
            status = 'This opportunity has no products associated with it.';
        } else if (this.calculationStatus == -5){
            oppTotalNRECGrossMargin = 0;
            oppTotalRECGrossMargin = 0;
            oppTotalGrossMargin = 0;
            status = 'This opportunity has no AC Mods products associated with it.';
        }
        // Creates opportunity to updates Status and all margins of the opportunity
        
        if(oppTotalNRECGrossMargin < -100){
            oppTotalNRECGrossMargin = -100.00;
        }
        if(oppTotalRECGrossMargin < -100){
            oppTotalRECGrossMargin = -100.00;            
        }
        if(oppTotalGrossMargin < -100){
            oppTotalGrossMargin = -100.00;
        }
        
        this.opp.NREC_Gross_Margin__c = oppTotalNRECGrossMargin;
        this.opp.REC_Gross_Margin__c = oppTotalRECGrossMargin;
        this.opp.Total_Gross_Margin__c = oppTotalGrossMargin;
        this.opp.Calculation_Status__c = status;
    }
    
    public void updateOppProductData(){
        //System.Debug('oppProdList :'+this.oppProductList);
        for(OpportunityLineItem oppProd : this.oppProductList){
            Product_Margin_Calculation__c pmc = PMCMap.get(oppProd.Product2Id);
            if(oppProd.Princing__c == 'NREC'){
                oppProd.Gross_Margin__c = pmc.Gross_Margin_for_NREC__c;
                //System.Debug('NREC GROSS :'+oppProd.Gross_Margin__c);
                
            }
            if(oppProd.Princing__c == 'REC'){
                oppProd.Gross_Margin__c = pmc.Gross_Margin_for_REC__c;
                //System.Debug('REC GROSS :'+oppProd.Gross_Margin__c);
                
            }
            oppProd.Total_Gross_Margin__c = pmc.Gross_Margin_for_product__c;
            //System.Debug('TT GRoss :'+oppProd.Total_Gross_Margin__c);
            
        }
        //System.Debug('oppProdList :'+this.oppProductList);
        
    }
    
    public integer updateData(){
        if(this.oldPMCList != null){
            update this.oldPMCList;
        }
        if(this.PMCMap.values() != null){
            insert this.PMCMap.values();
        }
        if(this.opp != null){
            update this.opp;
        }
        if(this.oppProductList != null){
            update this.oppProductList;
        }
        return 0;
    }
    

    
    /*public void deleteProcedure(List<OpportunityLineitem> oldOppProducts){
        List<Id> prodList = new List<Id>();
        setAvailableRecordTypes();
        for(OpportunityLineitem oppProd : oldOppProducts){
            prodList.add(oppProd.Product2Id);
            setOpportunity(oppProd.OpportunityId);
            
        }
        
        List<Product_Margin_Calculation__c> oldPMCList = [ SELECT Id, Is_Active__c, Related_Product__c, Related_Opportunity__c
                                                          FROM Product_Margin_Calculation__c
                                                          WHERE Is_Active__c = :true and Related_Opportunity__c = :this.opp.Id ];
        
        for(Product_Margin_Calculation__c pmc : oldPMCList){
            pmc.Is_Active__c = false;
            this.oldPMCList.add( pmc);
        }
    }*/
    
    /*public void loadData(Id oppId){
        setOpportunity(oppId);
        if(this.opp == null){return;}
        if(setProductsFromOpportunity(oppId) == null) {
            //System.debug('Null Prod OPP');
        }
        System.debug('Opp Product List size :' + this.oppProductList.size());
        System.debug('Opp Product List :' + this.oppProductList);
        System.debug('Opp Product Map :' + this.oppProductMap);
        System.debug('Opp Product ID List size :' + this.oppProductIDList.size());
        System.debug('Opp Product ID List :' + this.oppProductIDList);
        System.debug('Product ID List size :' + this.productIDList.size());
        System.debug('Product ID List :' + this.productIDList);
        if(this.calculationStatus == 0 || this.calculationStatus == -3) {
            getCostControlList();
            calculateMarginForProductList();
            setOldMarginCalculations();
            updateDataIntoOpportunity();
            updateOppProductData();
            updateData();
        }*/
        /*System.Debug('Product Cost Control List size: ' + this.costControlList.size());
        System.Debug('Product Cost Control List : ' + this.costControlList);
        System.Debug('Product Cost Control Map : ' + this.costControlMap);
        System.Debug('PMC NEW Map : ' + this.PMCMap);
        System.Debug('PMC OLD Map : ' + this.oldPMCList);
                
        System.Debug('Calc Stat' + this.calculationStatus);
    }*/
    
    public void updateOrInsert(Id oppId){
        setAvailableRecordTypes();
        setOpportunity(oppId);
        if(this.opp == null){SYstem.debug('Opp = null');return;}
        setProductsFromOpportunity(oppId);
        if(this.calculationStatus == -2 || this.calculationStatus == -5){
            updateDataIntoOpportunity();
            if(this.opp != null){
                update this.opp;
            }
        }
        else if(this.calculationStatus == 0 || this.calculationStatus == -3) {
            getCostControlList();
            if(this.calculationStatus != -1 && this.calculationStatus != -4){
                calculateMarginForProductList();
                setOldMarginCalculations();
                updateDataIntoOpportunity();
                updateOppProductData();
                updateData();
            }else{
                updateDataIntoOpportunity();
                if(this.opp != null){
                    update this.opp;
                }
            }
        }
        //System.Debug('Calc Stat' + this.calculationStatus);
    }
    /*public void beforeDeleteHandler(List<OpportunityLineitem> oldList){
        deleteProcedure(oldList);
        
    }*/
    /*public void afterDeleteHandler(List<OpportunityLineitem> oldList){
        Set<Id> opportunityIds = new Set<Id>();
        for(OpportunityLineItem oppProd : oldList){
            opportunityIds.add(oppProd.OpportunityId);
        }
        for(ID oppId : opportunityIds){
            updateOrInsert(oppId);
        }
    }*/
    
    public void triggerDataHandler(List<OpportunityLineItem> oppProductList){
        //System.debug('Opp Prodct List from Trigger :' + oppProductList);
        Set<Id> opportunityIdSet = new Set<Id>();
        for(OpportunityLineItem oppProduct : oppProductList){
            //SYstem.debug('Teste for Trigger Ammoutn');
            opportunityIdSet.add(oppProduct.OpportunityId);
        }
        
        for(Id oppId : opportunityIdSet){                
            updateOrInsert(oppId);
        }
        
    }
}