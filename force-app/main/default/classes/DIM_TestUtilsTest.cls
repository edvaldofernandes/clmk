// Used by DIM - Market Intelligence.
@isTest 
public class DIM_TestUtilsTest {

    //--------------------------------------------------------------------------
    static testMethod void testIsRunningTestEnabled() {
        System.assertEquals(true, DIM_TestUtils.isRunningTest());
    }
    
    //--------------------------------------------------------------------------
    static testMethod void testIsRunningTestDisabled() {
        DIM_TestUtils.enableIsRunningTest = false;
        System.assertEquals(false, DIM_TestUtils.isRunningTest());
    }
    
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    private static User user;
    private static Account account;
    
    public static testMethod Case newCase() {
    
        if (user == null) {
            Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            user = new User(Alias = 'test', Email='testtesttest@embraer.com.br', 
                EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='GMT', 
                UserName='testtesttest@embraer.com.br');
            insert user;
        }
        
        if (account == null) {
            account = new Account();
            account.Name = 'Test Air';
            account.Company_Nickname__c = 'Test';
            insert Account;
        }
             
        Case caso = new Case();
        caso.Subject = 'Case Study';
        caso.Due_Date__c = Date.today();
        caso.Status = 'Open';
        caso.Requested_By__c = user.Id;
        caso.AccountId = account.Id;
        caso.Study_Type__c = 'Performance';
        
        return caso;
    }
}