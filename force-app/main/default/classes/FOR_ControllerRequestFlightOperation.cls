/**
* @author Marcilio Leite de Souza
* @date 18/06/2018
* @description: Controller for the site flight operation requests
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           18 JUN 2018             Original Version
**/
public class FOR_ControllerRequestFlightOperation {

    public Case objCase {get; set;}
    public FOR_Flight_Leg__c objLeg {get; set;}
    public File objFile{get; set;} 
    public String strStartDate {get; set;}
    public String strEndDate {get; set;}
    public String strStartTime {get; set;}
    public String error {get; set;}
    
    /**
	* @description : Constructor VisualForce Page 
	* @return void
	*/
    public FOR_ControllerRequestFlightOperation(){
        objCase = new Case();
        objLeg = new FOR_Flight_Leg__c();
        objFile = new File('Request file attachment');
        error = '';
    }
    
    /**
	* @description : Execute actions to save the case
	* @return PageReference : value null
	*/
    public PageReference saveCase(){

        objCase.Status = 'New';
        objCase.Origin = 'Web';
        
        try{
            FOR_FlightOperationsBusinessRules flightRules = new FOR_FlightOperationsBusinessRules();
            objCase.RecordTypeId = flightRules.getRecordTypeIdToFOR();
            objCase.AccountId = flightRules.getAccountIdToFOR(objCase.FOR_Requester_Customer__c);
            objCase.OwnerId = flightRules.getQueueIdToFOR();
            
            system.debug('FOR_ControllerRequestFlightOperation.saveCase.objCase: ' + objCase);
            
            insert objCase;

            objLeg.FOR_Related_Case__c = objCase.Id;
            objLeg.FOR_Start_Date__c = GEN_Util.getDateFromStringDate(strStartDate);
            objLeg.FOR_Start_Time__c = GEN_Util.getTimeFromStringTime(strStartTime);
            
            if(!String.isBlank(strEndDate)){
                objLeg.FOR_End_Date__c = GEN_Util.getDateFromStringDate(strEndDate);
            }

            system.debug('FOR_ControllerRequestFlightOperation.saveCase.objLeg: ' + objLeg);
            
            insert objLeg;
            
            system.debug('FOR_ControllerRequestFlightOperation.saveCase.objFile: ' + objFile);
            if(!String.isBlank(objFile.body)){
                createAtt(objFile, objCase.Id);
            }
            
            initializeObj();
        }catch(Exception e){
            objFile = new File('Request file attachment');
            system.debug('FOR_ControllerRequestFlightOperation.saveCase.e.getMessage(): ' + e.getMessage());
            error = e.getMessage();
        }
       
        return null;
    }
    
    /**
	* @description : Getter objCase
	* @return Case
	*/
    public Case getCase() {return this.objCase;}
    
    /**
	* @description : Initialize the objects values
	* @return void
	*/
    private void initializeObj(){
        objCase = new Case();
        system.debug('initializeObj.objCase:' + objCase);
        objLeg = new FOR_Flight_Leg__c();
        objFile = new File('Request file attachment');
        error = '';
        strStartDate = '';
        strEndDate = '';
        strStartTime = '';
    }
    
    /**
	* @description : Create a file and link with case
	* @return void
	*/
     private void createAtt(File file, Id idCase){
         
         ContentVersion cv = new ContentVersion();
         cv.versionData = EncodingUtil.base64Decode(file.body.substringAfter(','));
         cv.ContentLocation = 'S';
         cv.title = file.name;
         cv.pathOnClient = file.name;

         insert cv;
         
         ContentDocumentLink cdl = new ContentDocumentLink();
         cdl.LinkedEntityId = idCase;
         cdl.ShareType = 'V';
         cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;

         insert cdl;
    }
    
    /**
	* Inner class File
	*/
    public class File{
        public String body{get; set;}              
        public String typeFile{get; set;}
        public String name{get; set;}     
        public String description{get; set;}    
        
        public File(String description){
            this.description = description;
        }
    }

    /**
	* @description : Get Case Activities picklist values
	* @return List<SelectOption>
	*/
    public List<SelectOption> getCaseActivities(){
        return GEN_Util.getSelectOptionFromPickList(Case.FOR_Activity__c.getDescribe().getPicklistValues(), true);
    }
    
    /**
	* @description : Get Case Arcraft Types picklist values
	* @return List<SelectOption>
	*/
    public List<SelectOption> getCaseAircraftTypes(){
        return GEN_Util.getSelectOptionFromPickList(Case.FOR_Aircraft_Type__c.getDescribe().getPicklistValues(), false);
    }
    
    /**
	* @description : Get Service Request picklist values
	* @return List<String>
	*/
    public List<String> getCaseServiceRequest(){
        return GEN_Util.getListStringFromPickList(Case.FOR_Service_Request__C.getDescribe().getPicklistValues(), false);
    }

    /**
	* @description : Get Case Priorities picklist values
	* @return List<SelectOption>
	*/
    public List<SelectOption> getCasePriorities(){
        return GEN_Util.getSelectOptionFromPickList(FOR_Flight_Operation_Request_Setting__mdt.FOR_Priority__c.getDescribe().getPicklistValues(), false);
    }
}