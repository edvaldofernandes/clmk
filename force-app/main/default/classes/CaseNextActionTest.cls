@IsTest
public class CaseNextActionTest {
    
    private static final Id CRC = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();
    private static final Id BACKORDER = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Backorder').getRecordTypeId();
    private static final Id PRICING = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Spare Parts Price').getRecordTypeId();

    @TestSetup
    private static void testSetup(){
        
        Case parentCaseAOG = createParentCase(CRC, 'Processing', 'AOG NFO');
        Case parentCaseScheduled = createParentCase(CRC, 'Processing', 'Scheduled');
        Database.insert(new List<Case>{parentCaseAOG, parentCaseScheduled});
        
    }
    
    @IsTest
    public static void shouldAssignNextActionDateBackOrderAOG(){
        
        Case parentCase = getParentCaseAOG();
        Case childCase = createChildCase(parentCase.Id, BACKORDER, 'Back order', 'AOG NFO');
        
        Test.startTest();
        	Database.insert(childCase);
        Test.stopTest();

        parentCase = getParentCaseAOG();
        
        System.assert(parentCase.Next_Action_Required_Date_and_Time__c != null);
    }
    
    @IsTest
    public static void shouldAssignNextActionDateBackOrderScheduled(){
        
        Case parentCase = getParentCaseAOG();
        Case childCase = createChildCase(parentCase.Id, BACKORDER, 'Back order', 'Scheduled');
        
        Test.startTest();
        	Database.insert(childCase);
        Test.stopTest();

        parentCase = getParentCaseAOG();
        
        System.assert(parentCase.Next_Action_Required_Date_and_Time__c != null);
        
    }
    
    @IsTest
    public static void shouldAssignNextActionDatePricingAOG(){
        
        Case parentCase = getParentCaseAOG();
        Case childCase = createChildCase(parentCase.Id, PRICING, 'Pricing', 'AOG NFO');
        
        Test.startTest();
        	Database.insert(childCase);
        Test.stopTest();

        parentCase = getParentCaseAOG();
        
        System.assert(parentCase.Next_Action_Required_Date_and_Time__c != null);
    }
    
    @IsTest
    public static void shouldAssignNextActionDatePricingScheduled(){
        
        Case parentCase = getParentCaseAOG();
        Case childCase = createChildCase(parentCase.Id, PRICING, 'Pricing', 'Scheduled');
        
        Test.startTest();
        	Database.insert(childCase);
        Test.stopTest();

        parentCase = getParentCaseAOG();
        
        System.assert(parentCase.Next_Action_Required_Date_and_Time__c != null);
    }
        
    @IsTest
    public static void shouldAssignNextActionDateWarehouseAOG(){
        
        Case parentCase = getParentCaseAOG();
        
        Test.startTest();
        
        	parentCase.Status = 'Warehouse';
        	Database.update(parentCase);
        
        Test.stopTest();
        
        parentCase = getParentCaseAOG();
        
        System.assert(parentCase.Next_Action_Required_Date_and_Time__c != null);
    }
    
    @IsTest
    public static void shouldAssignNextActionDateWarehouseScheduled(){
        Case parentCase = getParentCaseScheduled();
        
        Test.startTest();
        
        	parentCase.Status = 'Warehouse';
        	Database.update(parentCase);
        
        Test.stopTest();
        
        parentCase = getParentCaseScheduled();
        
        System.assert(parentCase.Next_Action_Required_Date_and_Time__c != null);
    }
    
    private static Case getChildCase(String status, String priority){
        return [SELECT Id, Status, Next_Action_Required_Date_and_Time__c FROM Case WHERE ParentId != null AND Priority = :priority AND Status = :status];
    }
    
    private static Case getParentCaseAOG(){
        return [SELECT Id, Status, Next_Action_Required_Date_and_Time__c FROM Case WHERE ParentId = null AND Priority = 'AOG NFO' LIMIT 1];
    }
    
    private static Case getParentCaseScheduled(){
        return [SELECT Id, Status, Next_Action_Required_Date_and_Time__c FROM Case WHERE ParentId = null AND Priority = 'Scheduled' LIMIT 1];
    }
    
    private static Case createChildCase(Id parentId, Id recordTypeId, String status, String priority){
        
        Case childCase = createCase(parentId, recordTypeId, status, priority);
        
        return childCase;
    }
    
    private static Case createParentCase(Id recordTypeId, String status, String priority){
        
        Case parentCase = createCase(null, recordTypeId, status, priority);
        
        return parentCase;
    }
    
    private static Case createCase(Id parentId, Id recordTypeId, String status, String priority){
        
        Case c = new Case(
        	RecordTypeId = recordTypeId,
            Status = status,
            Due_Date__c = Date.today().addMonths(1),
            Subject = 'CRC',
            ParentId = parentId,
            Type = 'TBC - To be classified',
            Study_Type__c = 'Performance',
            Priority = priority,
            Phase_c__c = 'Dispatch',
            Status__c = 'Dispatch.',
            Next_Action_required_details__c = 'Required Details',
            Requested_By__c = UserInfo.getUserId()
        );
        
        return c;
    }
}