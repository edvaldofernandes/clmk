public class RequestProposalController {
	public Opportunity opportunity { get; set; }
    
    public RequestProposalController(ApexPages.StandardController controller){
        
        if (ApexPages.currentPage().getParameters().get('id') == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID parameter not found'));
            return;
        }
        
        opportunity = [SELECT Id, Sum_of_Kickoffs__c, Requested_Proposal__c FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        if(opportunity == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Opportunity not found','Failed search for '+ApexPages.currentPage().getParameters().get('id')));
    	}
    }
    
    public PageReference requestProposal(){
        if(!opportunity.Requested_Proposal__c && opportunity.Sum_of_Kickoffs__c == 1){
			opportunity.Requested_Proposal__c = true;
			Database.update(opportunity);            
            
            PageReference returnPage = new PageReference('/'+ opportunity.id);  
            returnPage.setRedirect(true);
            return returnPage;
            
        }else{
            if (opportunity.Requested_Proposal__c){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Proposal already requested, for more information please contact a contract negotiator.'));
            	return null;
        	}else{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to set one kickoff record for this opportunity to be able to request a proposal'));
            	return null;
        	}
        }
    }
}