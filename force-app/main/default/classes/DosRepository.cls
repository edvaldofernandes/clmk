public class DosRepository {
   
    List<MFIR__c> mfirRecords = [SELECT Name, Account__c FROM MFIR__c];
    //returns all dos cases from the DB
    public List<Case> GetAllDosCases(){
        return [SELECT Id, ESD__c, AccountID, Account.Core_Return_Contractual_Terms__c, 
                Account.Exchange_Contractual_Term__c, CaseNumber, Part_Number__c, Part_Number__r.Name, Comments__c, Priority, Need_by_Date__c, 
                SO__c, MFIR__C, Customer_Name__c, Case_Aging__c, Top_Most_Ecode__c, Part_Description__c, Transfer_PO__c,//RESS_ID__c,
                WRCS_Vendor_s__c, LBG_Vendor__c, Case_Aging_Number__c, Phase__c, RecordTypeId, RecordType.Name, Status, 
                Repair_Contract_Terms__c,PRC_Comments_For_DOS__c,Supporting_ESD__c,Supporting_Notification__c, POS_Comments_For_DOS__c, Ecode__c, EmbCode__c, Main_Category__c,
                (SELECT Comments__c, Department__c FROM Comments__r) 
                FROM Case 
                WHERE Status!='Closed' 
                       AND Status!='Cancelled' 
                       AND (RecordType.Name='Pool Exchanges' 
                            OR RecordType.Name='MSPReplesh') 
                       AND Phase__c='Back Order' 
                       AND (PRC_Support_Plant__c='FLL' 
                            OR PRC_Support_Plant__c='LBG' 
                            OR PRC_Support_Plant__c='SJK' 
                            OR PRC_Support_Plant__c='' 
                            OR PRC_Support_Plant__c=null)
                            AND MFIR__r.name !='517010'
                /* (OLD REMOVED BY NANCY A) Account.Name!='Azul Linhas Aéreas'*/
                        
                ORDER BY Account.PRC_Support_Plant__c ASC, Case_Aging_Number__c DESC];
        /*OR (Status='Order Not Overdue' AND Days_Since_Due_Date__c>-8
                           AND (RecordType.Name='Pool Exchanges' 
                                OR RecordType.Name='MSPReplesh')*/
    }
    
    //gets dos cases matching the region passed as an argument
    public List<Case> GetDosCasesByRegion(string region){
        return [SELECT Id, ESD__c, AccountID, Account.Core_Return_Contractual_Terms__c, Account.Exchange_Contractual_Term__c, 
                CaseNumber, Part_Number__c, Part_Number__r.Name, Comments__c, Priority, Need_by_Date__c, SO__c, Transfer_PO__c,//removed ressid
                Customer_Name__c, Case_Aging__c, Top_Most_Ecode__c, Part_Description__c, WRCS_Vendor_s__c, LBG_Vendor__c, Case_Aging_Number__c, 
                Phase__c, RecordTypeId, RecordType.Name, Status,Supporting_ESD__c,Supporting_Notification__c,Repair_Contract_Terms__c,PRC_Comments_For_DOS__c, 
                POS_Comments_For_DOS__c, Ecode__c, EmbCode__c, Main_Category__c,
                (SELECT Comments__c, Department__c FROM Comments__r) 
                FROM Case 
                WHERE Status != 'Closed' 
                        AND Status != 'Cancelled' 
                        AND (RecordType.Name = 'Pool Exchanges' 
                             OR RecordType.Name = 'MSPReplesh') 
                        AND Phase__c = 'Back Order' 
                        AND PRC_Support_Plant__c = :region
                        AND MFIR__r.name !='517010'
                /* (OLD REMOVED BY NANCY A) Account.Name!='Azul Linhas Aéreas'*/
                ORDER BY Case_Aging_Number__c DESC];
    }
    
    // Nancy Added gets dos cases for priority AOG
    public List<Case> GetDosCasesByPriority(string region){
        return [SELECT Id, ESD__c,  AccountID, Account.Core_Return_Contractual_Terms__c, Account.Exchange_Contractual_Term__c, 
                CaseNumber, Part_Number__c, Part_Number__r.Name, Comments__c, Priority, Need_by_Date__c, SO__c, Transfer_PO__c, //RESS_ID__c, 
                Customer_Name__c, Case_Aging__c, Top_Most_Ecode__c, Part_Description__c, WRCS_Vendor_s__c, LBG_Vendor__c, Case_Aging_Number__c, 
                Phase__c, RecordTypeId, RecordType.Name, Status,Supporting_ESD__c,Supporting_Notification__c, Repair_Contract_Terms__c,PRC_Comments_For_DOS__c, 
                POS_Comments_For_DOS__c, Ecode__c, EmbCode__c, Main_Category__c,
                (SELECT Comments__c, Department__c FROM Comments__r) 
                FROM Case 
                WHERE Status != 'Closed' 
                        AND Status != 'Cancelled' 
                        AND (RecordType.Name = 'Pool Exchanges' 
                             OR RecordType.Name = 'MSPReplesh') 
                        AND Phase__c = 'Back Order' 
                        AND (PRC_Support_Plant__c='FLL' 
                            OR PRC_Support_Plant__c='LBG' 
                            OR PRC_Support_Plant__c='SJK' )           
                		AND (Priority = 'AOG' OR Priority = 'AOG NFO')
                        AND MFIR__r.name !='517010'
                /* (OLD REMOVED BY NANCY A) Account.Name!='Azul Linhas Aéreas'*/
                ORDER BY Case_Aging_Number__c DESC];
    }
    
   /* public List<Case> GetRepairAdminDosCases(String region){
        if(region=='REP FLL'){  //get FLL cases
            return [SELECT ID, AccountID, Account.Name, Part_Number__c, Notification__c, Status, Comments__c, RESS_ID__c, ESD__c, 
                    Actual_Contracted_TAT__c, Top_Most_Ecode__c, WRCS_Vendor_s__c, LBG_Vendor__c, Part_Description__c, Repair_Contract_Terms__c, CaseNumber
                    FROM Case
                    WHERE ((RecordType.Name='Warranty' AND Actual_Contracted_TAT__c>45 AND PRC_Support_Plant__c='FLL') OR (RecordType.Name='Repair Administ.' AND ((Actual_Contracted_TAT__c>25 AND PRC_Support_Plant__c='FLL')))) AND Status!='Cancelled' AND Status!='Closed'
                    ORDER BY Actual_Contracted_TAT__c DESC];
        }else if(region=='REP LBG'){    //get LBG Cases
            return [SELECT ID, AccountID, Account.Name, Part_Number__c, Notification__c, Status, Comments__c, RESS_ID__c, ESD__c, Actual_Contracted_TAT_EMEA__c, 
                    Top_Most_Ecode__c, WRCS_Vendor_s__c, LBG_Vendor__c, Part_Description__c, Repair_Contract_Terms__c, CaseNumber
                    FROM Case
                    WHERE ((RecordType.Name='Warranty' AND Actual_Contracted_TAT_EMEA__c>45 AND PRC_Support_Plant__c='LBG') OR (RecordType.Name='Repair Administ.' AND (Actual_Contracted_TAT_EMEA__c>25 AND (Account.Name='HOP!' OR Account.Name='SA Airlink Pty.')))) AND Status!='Cancelled' AND Status!='Closed'
                    ORDER BY Actual_Contracted_TAT_EMEA__c DESC];
        }else{  //get azul cases
            return [SELECT ID, AccountID, Account.Name, Part_Number__c, Notification__c, Status, Comments__c, RESS_ID__c, ESD__c, Azul_Actual_TAT__c, 
                    Top_Most_Ecode__c, WRCS_Vendor_s__c, LBG_Vendor__c, Part_Description__c, Repair_Contract_Terms__c, CaseNumber
                    FROM Case
                    WHERE RecordType.Name='Repair Administ.' AND Account.Name='Azul Linhas Aéreas' AND Is_Above_Azul_TAT__c=True AND Status!='Cancelled' AND Status!='Closed'
                    ORDER BY Azul_Actual_TAT__c DESC];
        }
    } */
    
    //returns all part numbers with these topmosts
    public List<LLPDatabase__c> GetPartNumbersByTopmost(List<String> TopMostStrings){
        return [SELECT Id, Ecode__c, Top_Most__c, Name, Description__c, Prime__c, Part_Cost__c, Program__c, Pool_Customer__c, Essentiality__c, FLL1__c, FLL2__c, FLL3__c, US13__c, Blocked_Qty__c, LBG3__c, LBG2__c, LBG1__c, Blocked_LBG3__c, LBG3_Excess__c 
                FROM LLPDatabase__c WHERE Top_Most__c IN :TopMostStrings];
    }
    
    //returns all cores with a topmost in the list
    public List<Core_Information__c> GetCoresByTopmost(List<String> TopMostStrings){
        return [SELECT Topmost__c, Category__c, Category_Aging__c, Customer_Name__c, Epool_status__c, Notification__c, Part_Number__c, Problem_Description__c, QM_Number__c, Responsible_Department__c, Total_Aging__c, Support_Plant__c, AWB_to_FLL_to_Repair__c, Epool_Notes__c
                FROM Core_Information__c WHERE Topmost__c IN :TopMostStrings ORDER BY Support_Plant__c ASC, Total_Aging__c DESC];
    }
    
    //returns cores by region
    public List<Core_Information__c> GetCoresByRegion(List<String> TopMostStrings, string region){
        return [SELECT Topmost__c, Category__c, Category_Aging__c, Customer_Name__c, Epool_status__c, Notification__c, Part_Number__c, Problem_Description__c, QM_Number__c, Responsible_Department__c, Total_Aging__c, Support_Plant__c, AWB_to_FLL_to_Repair__c, Epool_Notes__c
                FROM Core_Information__c WHERE Topmost__c IN :TopMostStrings and Support_Plant__c=:region ORDER BY Support_Plant__c ASC, Total_Aging__c DESC];
    }
    
    //returns all repair info with topmost in the list
    public List<Repair_Information__c> GetRepairInfoByTopmost(List<String> TopMostStrings){
        return [SELECT Topmost__c, Actual_Repair_Station__c, eRepair_Queue__c, Repair_Contract_Terms__c,RM_Materials_Comments__c, Repair_Customer__c, Repair_Ecode__c, Repair_Exchange_Prov__c, Repair_Notification__c, Repair_Part_Number__c, Repair_User_Notes__c, RM_TAT_At_Repair__c, SOD_Notes__c, TAT_At_Repair__c, Site__c
                FROM Repair_Information__c WHERE Topmost__c IN :TopMostStrings ORDER BY Site__c ASC, Tat_At_Repair_Formula__c DESC];
    }
    
     //returns repair info by region
    public List<Repair_Information__c> GetRepairInfoByRegion(List<String> TopMostStrings, string region){
        return [SELECT Topmost__c, Actual_Repair_Station__c, eRepair_Queue__c, Repair_Contract_Terms__c,RM_Materials_Comments__c, Repair_Customer__c, Repair_Ecode__c, Repair_Exchange_Prov__c, Repair_Notification__c, Repair_Part_Number__c, Repair_User_Notes__c, RM_TAT_At_Repair__c, SOD_Notes__c, TAT_At_Repair__c, Site__c
                FROM Repair_Information__c WHERE Topmost__c IN :TopMostStrings and Site__c=:region ORDER BY Tat_At_Repair_Formula__c DESC];
    }
    
    //returns all ress info with topmosts in the list
 //   public List<RESS_Info__c> GetRessInfoByTopmost(List<String> TopMostStrings){
    //    return [SELECT Id, Topmost__c, Customer_Name__c, Part_Number__c, RESS_ID__c, Sales_Order__r.SO__c, Sales_Order__r.Transfer_PO__c, Sales_Order__r.Id, rm_comments__c, Site__c
    //            FROM RESS_Info__c WHERE Topmost__c IN :TopMostStrings and RESS_ID__c!=null and Tracking_Sent_Date_and_Time__c=null and Alternate_Action_Date_and_Time__c=null ORDER BY Site__c ASC];
  //  }
    
    //returns ress info with topmosts in the list AND where site matches region parameter
  //  public List<RESS_Info__c> GetRessInfoByRegion(List<String> TopMostStrings, string region){
    //    return [SELECT Id, Topmost__c, Customer_Name__c, Part_Number__c, RESS_ID__c, Sales_Order__r.SO__c, Sales_Order__r.Transfer_PO__c, Sales_Order__r.Id, rm_comments__c, Site__c
      //          FROM RESS_Info__c WHERE Topmost__c IN :TopMostStrings and RESS_ID__c!=null and Tracking_Sent_Date_and_Time__c=null and Alternate_Action_Date_and_Time__c=null and Site__c = :region];
  //  }
    
    //returns QM info with topmosts in the list
    public List<QM__c> GetQMsByTopmost(List<String> TopMostStrings){
        return [SELECT Id, Name, Responsible__c, Aging__c, Last_Comment__c, Site__c, Part_Number__r.Top_Most__c 
                FROM QM__c 
                WHERE Part_Number__r.Top_Most__c IN :TopMostStrings AND Closed__c = false 
                ORDER BY Site__c, Aging__c DESC];
    }
    
    //returns QM info with topmosts in the list AND where site matches region parameter
    public List<QM__c> GetQMsByRegion(List<String> TopMostStrings, string region){
        return [SELECT Id, Name, Responsible__c, Aging__c, Last_Comment__c, Site__c, Part_Number__r.Top_Most__c 
                FROM QM__c 
                WHERE Part_Number__r.Top_Most__c IN :TopMostStrings AND Region__c = :region AND Closed__c = false
                ORDER BY Aging__c DESC];
    }
    
    //get comments
    public Case GetCommentsByCase(String selectedCaseNumber){
        return [SELECT Id, Main_Category__c, (SELECT Comments__c, Department__c, CreatedById, Date_Time__c FROM Comments__r ORDER BY Date_Time__c) FROM Case WHERE CaseNumber = :selectedCaseNumber]; //return a case containing its related comments
    }
    
    //insert a comment into DB
    public void UpdateCase(Case currentCase){
        update currentCase; //update the case in salesforce DB
    }
}