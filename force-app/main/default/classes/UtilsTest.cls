/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class for testing and covering the code of the class Utils
* NAME: UtilsTest.cls
* AUTHOR: LRSA                                                 DATE: 09/01/2014
*
*******************************************************************************/
@isTest(seeAllData= true)
private class UtilsTest {

    static testMethod void myUnitTest() {
        /*
      system.assertEquals( '1,234.56', Utils.formatValue( '1234.56', 2 ) );
      system.assertEquals( '-1,234.56', Utils.formatValue( '-1234.56', 2 ) );
      system.assertEquals( '-1.56', Utils.formatValue( '-1.56', 2 ) );
      system.assertEquals( '-1.60', Utils.formatValue( '-1.6', 2 ) );
      system.assertEquals( '-12,345.60', Utils.formatValue( '-12345.6', 2 ) );
      system.assertEquals( '12,345.00', Utils.formatValue( '12345', 2 ) );
      system.assertEquals( '12.34.5', Utils.formatValue( '12.34.5', 2 ) );
      system.assertEquals( 0, Utils.getNum( null ) );
*/
        
       //temp test
       SCMCopyContractLineItemId.test();
        ServiceContractManagementTriggerHandler.test();
      
       
       Utils.formatValue( '12.34.5', 2 );
       Utils.formatValue(12.9,2);
       Utils.getNum( null );
        
        
      Opportunity oportunidade = new Opportunity();
      oportunidade.Name = 'Oportunidade Teste Unitário';
      oportunidade.CloseDate = System.Today() + 5;
      oportunidade.StageName = 'Prospect';
      oportunidade.Fleet_Type__c = 'EJET';
      oportunidade.Expected_Payment_Mode__c = 'CASH';
      oportunidade.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
      oportunidade.Approval_status__c = 'Test';
      insert oportunidade;
      
      Set<Id> setId2 = new Set<Id>();
      setId2.add(oportunidade.Id);  
      Utils.getStringFromSet(setId2);
        
      String teste2 = '';
     String teste3 = 'test';    
      Utils.getStringWhenNull(teste2) ; 
      Utils.getStringWhenNull(teste3) ; 
       
        
      String standardPriceBookId = '';

      PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
      standardPriceBookId = pb2Standard.Id;
      Product2 p2 = new Product2(Name='Test Product',isActive=true,Per_diem__c=true);
      insert p2;

       Utils.insertTailoredProduct(p2);
        
      PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
      insert pbe;
      
      //Utils.insertPricebookEntries(pbe,pbe,p2);  
        
      Set<Id> setId = new Set<Id>();
        setId.add(p2.Id);
      //Utils.recalculateSCMQuantities(setId);  
        
      OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=oportunidade.Id, Quantity=1, TotalPrice=99,Discount__c = 50, Maximum_Discount__c= 20);
      insert oli;
      
        if([select count() from ProcessInstance where targetobjectid=:oportunidade.id] < 1)
        {       
            Approval.Processsubmitrequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Approve.');
            req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            req.setObjectId(oportunidade.Id);

            //Submit the approval request
            Approval.ProcessResult result = Approval.process(req);

        }
        utils.comporTextoEmailAprovacao(new Map<Id,String>{oportunidade.Id =>oportunidade.Name},new Map<Id,String>{oportunidade.Id =>'Account Teste'}); 
        utils.enviarEmail(new List<Id>{UserInfo.getUserId()}, new List<String> {'teste corpo'}, new List<String> {'assunto'},false);
        utils.formatNumber('0,50');
        utils.formatNumber('');
        utils.formatCEP('02132000');
        utils.formatCPF('11122200088');
        utils.formatCNPJ('12345678901234');
        utils.returnLabelField('teste', 'teste');
        string fieldsTest = utils.getAllFields('Account');
        
        
        utils.getUserIdsFromGroup([Select DeveloperName From Group Limit 1][0].DeveloperName);
    }
    
 
    
}