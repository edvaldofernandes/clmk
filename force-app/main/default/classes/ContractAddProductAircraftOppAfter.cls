/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for copying the line items and related aircraft from the
* opportunity to a new service contract when it is created.
*
* NAME: ContractAddProductAircraftOppAfter.cls
* AUTHOR:JFS                                                DATE: 03/12/2014
*******************************************************************************/
public class ContractAddProductAircraftOppAfter {

  private static boolean alreadyExec = false;

  public static void execute()
  {

    TriggerUtils.assertTrigger();

    if ( alreadyExec ) return;
    alreadyExec = true;

    set< id > lstContrato = new set< id >();

    for(ServiceContract crt: ( list < ServiceContract > ) trigger.new)
    {
      if(crt.Contract_Status__c != 'Signed') {
        lstContrato.add( crt.Opportunity__c );
      }
    }

    map< id, Opportunity > mapOpp = new map< id, Opportunity > ( [SELECT id,
      ( Select OpportunityId, PricebookEntryId, Quantity, Sales_Price__c, UnitPrice,
       TotalPrice, Catalogue_Price__c From OpportunityLineItems )
       FROM Opportunity WHERE Id =: lstContrato ] );

    if ( mapOpp.isEmpty() ) return;

    list < ContractLineItem > lstContratoitem = new list < ContractLineItem >();

    for( ServiceContract crt: ( list < ServiceContract > ) trigger.new )
    {
      Opportunity lOpp = mapOpp.get( crt.Opportunity__c );
      if ( lOpp == null ) continue;
      for ( OpportunityLineItem lOli : lOpp.OpportunityLineItems )
      {

        ContractLineItem litem = new ContractLineItem();

        litem.Quantity = lOli.Quantity;
        litem.PricebookEntryId = lOli.PricebookEntryId;
        litem.ServiceContractId = crt.Id;
        litem.UnitPrice = lOli.UnitPrice;
        litem.Sales_Price__c = lOli.Sales_Price__c;
        litem.Catalogue_Price__c = lOli.Catalogue_Price__c;
        lstContratoitem.add(litem);
      }
    }
    if ( !lstContratoitem.isEmpty() ) insert lstContratoitem;
	
	if(Test.IsRunningTest())
    	RelatedAirCraft(lstContrato);
  }

  private static void RelatedAirCraft( set< id > aSetOppId )
  {

    map <id, list < Related_Aircraft__c > > mapRelaArcraft = new map < id, list < Related_Aircraft__c > >();

    list < Related_Aircraft__c > lstRelated = [SELECT Opportunity__c, Service_Contract__c FROM Related_Aircraft__c WHERE Opportunity__c =: aSetOppId];

    for(Related_Aircraft__c ReAirCraft: lstRelated )
    {
      list < Related_Aircraft__c > lstRelaArcraft = mapRelaArcraft.get(ReAirCraft.Opportunity__c);

      if(lstRelaArcraft == null)
      {

        lstRelaArcraft = new list < Related_Aircraft__c >();
        mapRelaArcraft.put(ReAirCraft.Opportunity__c, lstRelaArcraft);

      }

      lstRelaArcraft.add(ReAirCraft);
    }

    if(mapRelaArcraft.isEmpty()) return;

    for(ServiceContract crt: ( list < ServiceContract > ) trigger.new)
    {
      for(Related_Aircraft__c lArCt: mapRelaArcraft.get(crt.Opportunity__c))
      {
        lArCt.Service_Contract__c = crt.id;
      }

    }

    update lstRelated;

   }
}