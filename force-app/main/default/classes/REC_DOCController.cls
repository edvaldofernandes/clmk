public without sharing class REC_DOCController {
  static List<String> textareaFields = new List<String>{
    'Summary__c',
    'Investigation__c',
    'References__c',
    'Recommendation__c',
    'Next_steps__c',
    'Update__c'
  };

  static Pattern imagePattern = Pattern.compile(
    '\\s*<img.*?src\\=\\"(.*?)\\".*?alt\\=\\"(.*?)\\"\\s*'
  );

  public static Map<String, List<Object>> getNewImages(
    Communications__c recObject,
    Boolean makeCallout
  ) {
    Map<String, List<Object>> newImages = new Map<String, List<Object>>();
    for (String field : textareaFields) {
      String fieldValue = (String) recObject.get(field);
      if (!String.isBlank(fieldValue)) {
        Matcher ptn = imagePattern.matcher(fieldValue);
        while (ptn.find()) {
          String url = ptn.group(1);
          String alt = ptn.group(2);
          //i.e., if url is a private domain
          if (url.contains('/rtaImage')) {
            if (makeCallout == true) {
              newImages.put(
                url,
                new List<Object>{ alt, getBlobFromString(url), field }
              );
            } else {
              newImages.put(
                url,
                new List<Object>{ alt, Blob.valueOf(url), field }
              );
            }
          }
        }
      }
    }
    return newImages;
  }

  public static Map<String, List<Object>> getNewImages(
    Communications__c recObject
  ) {
    return getNewImages(recObject, true);
  }

  // Communications__c teste = (Communications__c) getAllFieldsSOQL('Communications__c', 'Name = \'Teste Marião\'');

  public static Blob getBlobFromString(String imageURL) {
    Blob page;
    if (Test.isRunningTest()) {
      page = blob.valueOf(imageURL);
    } else {
      String decodedURL = imageURL.unescapeHtml4();
      page = new PageReference(decodedURL).getContent();
    }
    return page;
    // return 'data:image/jpeg;base64,' + EncodingUtil.base64Encode(b);
  }

  public static String sanitizeImageName(String rawName) {
    String filename = rawName.split('\\.')[0];
    filename = filename.replaceAll('^[^A-z]+|[^A-z0-9]$', '')
      .replaceAll('[^A-z0-9_]+', '_');
    return filename;
  }

  public static Id createNewPublicDocument(
    String filename,
    Blob body,
    String mimeType,
    Id folderId
  ) {
    List<Document> registeredDoc = [
      SELECT id
      FROM document
      WHERE
        bodylength = :body.size()
        AND folderId = :folderId
        AND name = :filename
      LIMIT 1
    ];
    if (registeredDoc.size() == 0) {
      Document doc = new Document();
      doc.body = body;
      system.debug('creating file');
      doc.developerName =
        filename +
        '' +
        String.valueof(Math.abs(Crypto.getRandomLong()));
      system.debug(doc.developerName);
      doc.name = filename;

      if (mimeType == 'png') {
        doc.ContentType = 'image/png';
      } else {
        doc.ContentType = 'image/jpeg';
      }

      doc.folderId = (String) folderId;
      doc.isPublic = true;
      doc.type = mimeType;

      insert doc;
      return doc.Id;
    }
    return registeredDoc[0].Id;
  }

  @future(callout=true)
  public static void hostImageInPublicDomain(Id recId) {
    Communications__c recObject = [
      SELECT
        Summary__c,
        Investigation__c,
        References__c,
        Recommendation__c,
        Next_steps__c,
        Update__c
      FROM Communications__c
      WHERE Id = :recId
    ];

    Id folderId = UserInfo.getUserId();
    Id docId;
    Map<String, List<Object>> newImages = getNewImages(recObject);

    for (String url : newImages.keySet()) {
      String alt = (String) newImages.get(url)[0];
      Blob body = (Blob) newImages.get(url)[1];
      String field = (String) newImages.get(url)[2];
      String filename = sanitizeImageName(alt);
      String mimeType = alt.split('\\.')[1].toLowerCase();
      docId = createNewPublicDocument(filename, body, mimeType, folderId);
      changeImageUrl(url, field, recObject, docId);
    }
  }

  public static void changeImageUrl(
    String url,
    String field,
    Communications__c recObject,
    Id docId
  ) {
    Organization org = [SELECT id, isSandBox FROM Organization LIMIT 1];
    String rootDomain = '';
    if (org.isSandBox == true) {
      rootDomain = 'https://commercialaviation--fltperf--c.documentforce.com/servlet/servlet.ImageServer';
    } else {
      rootDomain = 'https://commercialaviation--c.documentforce.com/servlet/servlet.ImageServer';
    }
    String publicDomain =
      rootDomain +
      '?id=' +
      (String) docId +
      '&oid=' +
      org.Id;
    String fieldValue = (String) recObject.get(field);
    system.debug(recObject.get(field));
    recObject.put(field, fieldValue.replace(url, publicDomain));
    system.debug(recObject.get(field));
    Database.update(recObject);
  }
}