/**
* Browsing History Controller
* Lucas Calegari
* lucaoliveira@deloitte.com
*/
public with sharing class CFCBrowsingComponentController   
{
    
    public List<RecentlyViewed> lstRecentlyViewed {get; set;}
    public List<Product2> lstProductsRecentlyViewed {get; set;}
    public Boolean mostrarOutrosProdutos {get;set;}
    public Boolean mostrarTodosProdutos {get;set;}    
    public Boolean isPardotActive {get;set;}
    
    public CFCBrowsingComponentController()
    { 
        VerifyPardot();
        
        SET<String> idRecentlyViewed = new SET<String>();
        lstRecentlyViewed = [ SELECT Id, Name FROM RecentlyViewed  Where Type = 'product2' ORDER BY LastViewedDate DESC LIMIT 10 ];  
        System.debug('CFCBrowsingHistoryComponentController.lstRecentlyViewed ' + lstRecentlyViewed);
        System.debug('CFCBrowsingHistoryComponentController.lstRecentlyViewed.size() ' + lstRecentlyViewed.size());
        
        for(RecentlyViewed rctViewed : lstRecentlyViewed) {
            idRecentlyViewed.add(rctViewed.Id);
        }
        System.debug('CFCBrowsingHistoryComponentController.idRecentlyViewed ' + idRecentlyViewed);
        
        lstProductsRecentlyViewed = Product2DAO.getInstance().getListBySetId(idRecentlyViewed);
        for(Product2 item : lstProductsRecentlyViewed) {
            system.debug('item name ' + item.name);
        }
        System.debug('CFCBrowsingHistoryComponentController.lstProductsRecentlyViewed.size() ' + lstProductsRecentlyViewed.size());

          
        /** Validando as páginas **/
        Integer contador = 0;
        for(Product2 countProducts : lstProductsRecentlyViewed) {contador++;}
        
        if(contador >= 6){mostrarOutrosProdutos = true;mostrarTodosProdutos = true;}else if(contador > 3 && contador < 6){mostrarOutrosProdutos = true;mostrarTodosProdutos = false;}

    }
    
    public void VerifyPardot(){
        system.debug('CFCBrowsingHistoryComponentController.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCBrowsingHistoryComponentController.isPardotActive >>> ' + isPardotActive);
    }
    
    
}