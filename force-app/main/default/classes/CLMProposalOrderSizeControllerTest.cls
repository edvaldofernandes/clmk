@isTest
public class CLMProposalOrderSizeControllerTest {
     @testSetup static void methodName() {
         Agreement__c agr = new Agreement__c();
         agr.Name = 'Agreement 1';
         Opportunity opp = new Opportunity();
         opp.Name = 'Opp 1';
         opp.Fleet_Type__c = 'Turboprop';
         opp.StageName = 'Validation_of_approvals';
         opp.CloseDate = system.today();
         insert opp;
         Kickoff__c ko = new Kickoff__c();
         ko.Name = 'Checklist 1';
         ko.Opportunity__c = opp.Id;
         ko.Customer_Type__c = 'New Customer';
         ko.submitted__c = true;
         ko.Kick_Off_Meeting_Date__c = system.today();
         insert ko;
         agr.Kickoff__c = [SELECT Id FROM Kickoff__c LIMIT 1][0].Id;
         agr.RecordTypeId = [SELECT Id FROM recordType WHERE SObjectType='Agreement__c' AND developerName='Proposal' LIMIT 1][0].Id;
         insert agr;
         List<OrderSizeItem__c> OSKAList = new List<OrderSizeItem__c>();
         OrderSizeItem__c order = new OrderSizeItem__c();
         while (OSKAList.size()<4){
			order = new OrderSizeItem__c();
            order.Firm__c = OSKAList.size();
            order.Commercial_Agreement__c = agr.Id;
            order.Info_Type__c = 'Order Size';
            OSKAList.add(order);
        }
        insert OSKAList;
     }
    @isTest static void listTest(){
        Id koId = [SELECT Id from Agreement__c LIMIT 1].Id;
        System.assert([SELECT Count() FROM OrderSizeItem__c]==4);
        List<OrderSizeItem__c> koAircraftOrderSizeItemsList = CLMProposalOrderSizeApexController.getOrderSizeItems(koId);
        System.assert(koAircraftOrderSizeItemsList.size()==4);
        for(OrderSizeItem__c ka : koAircraftOrderSizeItemsList){
            ka.Firm__c = 55;
        }
        update koAircraftOrderSizeItemsList;
        CLMProposalOrderSizeApexController.saveEdits(koAircraftOrderSizeItemsList);
        System.assert([SELECT Count() FROM OrderSizeItem__c WHERE Info_Type__c = 'Order Size' AND Firm__c = 55]==4);
    }
    
    @isTest static void getList(){
        System.assert([SELECT Count() FROM OrderSizeItem__c]==4);
        Id koId = [SELECT Id FROM Agreement__c ].Id;
        List<OrderSizeItem__c> koAircraftOrderSizeItemsList = CLMProposalOrderSizeApexController.getOrderSizeItems(koId);
   		System.debug(koAircraftOrderSizeItemsList);
        System.assert(koAircraftOrderSizeItemsList.size()==4);
        
    }
   
    @isTest static void getTryCatch(){
        CLMProposalOrderSizeApexController.getOrderSizeItems(null);
    }
    @isTest static void coverTryCatch(){
        CLMProposalOrderSizeApexController.saveEdits(null);
    }
}