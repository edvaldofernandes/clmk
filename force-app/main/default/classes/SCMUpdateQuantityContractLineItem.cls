/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* When a Service_Contract_Management__c is created, updated or deleted,
* this class updates the field Delivered__c of the 
* related ContractLineItem or the field Converted__c, if its record type is
* Entitlement_Conversion.
*
* NAME: SCMUpdateQuantityContractLineItem.cls
* AUTHOR: KHPS                                                DATE: 30/12/2014
*
*******************************************************************************/

public with sharing class SCMUpdateQuantityContractLineItem {
  
  private static final Id recTypeConversion = RecordTypeMemory.getRecType('Service_Contract_Management__c', 'Entitlement_Conversion');
  
  public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    list<Service_Contract_Management__c> lstSvcContractManagement = new list<Service_Contract_Management__c>();
    set<Id> lstContractLineItemId = new set<Id>();
    
    if(Trigger.isUpdate || Trigger.isInsert) {
    
      for(Service_Contract_Management__c scm : (list<Service_Contract_Management__c>)trigger.new) {
        if(TriggerUtils.wasChanged(scm, Service_Contract_Management__c.Quantity__c)) {
          if (scm.Contract_Line_Item_Deliverable__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_Deliverable__c);
          if (scm.Contract_Line_Item_conversion__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_conversion__c);
          lstSvcContractManagement.add(scm);
        }
      }
    
    }
    else if(Trigger.isDelete) {
      
      for(Service_Contract_Management__c scm : (list<Service_Contract_Management__c>)trigger.old) {
        if (scm.Contract_Line_Item_Deliverable__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_Deliverable__c);
        if (scm.Contract_Line_Item_conversion__c != null ) lstContractLineItemId.add(scm.Contract_Line_Item_conversion__c);
        lstSvcContractManagement.add(scm);
      }
      
    }
    
    if(lstSvcContractManagement.isEmpty()) return;
    
    map<Id, ContractLineItem> mapContractLineItem = new map<Id, ContractLineItem>([SELECT Id, Delivered__c, Quantity, Converted__c 
     FROM ContractLineItem WHERE Id = :lstContractLineItemId ]); //and Status__c != 'Converted']);
     
    if(mapContractLineItem.isEmpty()) return;
    
    list<ContractLineItem> lstUpdateContractLineItem = new list<ContractLineItem>();
    
    for(Service_Contract_Management__c scm : lstSvcContractManagement) {
      Decimal qtd = scm.Quantity__c != null ? scm.Quantity__c : 0;
      
      ContractLineItem conli;
      if ( scm.RecordTypeId == recTypeConversion )
      {
        conli = mapContractLineItem.get(scm.Contract_Line_Item_conversion__c);
      }
      else
      {
        conli = mapContractLineItem.get(scm.Contract_Line_Item_Deliverable__c);
      }
      if(conli == null) return;
      
      if ( scm.RecordTypeId == recTypeConversion )
      {
        if(conli.Converted__c == null) conli.Converted__c = 0;
      
        if(Trigger.isUpdate )
        {
          Decimal qtdOld = ( ( Service_Contract_Management__c )trigger.oldmap.get( scm.id ) ).Quantity__c;
          conli.Converted__c -= qtdOld != null ? qtdOld : 0;
        }
        
        if(Trigger.isUpdate || Trigger.isInsert) {
          conli.Converted__c += qtd;
        }
        else if(Trigger.isDelete) {
          if(conli.Converted__c > 0) {
            conli.Converted__c -= qtd;
          }
          else {
            conli.Converted__c = 0;
          }
        }
      }
      else
      {
        if(conli.Delivered__c == null) conli.Delivered__c = 0;
        
        if(Trigger.isUpdate )
        {
          Decimal qtdOld = ( ( Service_Contract_Management__c )trigger.oldmap.get( scm.id ) ).Quantity__c;
          conli.Delivered__c -= qtdOld != null ? qtdOld : 0;
        }
        
        if(Trigger.isUpdate || Trigger.isInsert) {
          conli.Delivered__c += qtd;
        }
        else if(Trigger.isDelete) {
          if(conli.Delivered__c > 0) {
            conli.Delivered__c -= qtd;
          }
          else {
            conli.Delivered__c = 0;
          }
        }
      }
      
      lstUpdateContractLineItem.add(conli);
    }
    
    if(!lstUpdateContractLineItem.isEmpty()) update lstUpdateContractLineItem;
    
  }

}