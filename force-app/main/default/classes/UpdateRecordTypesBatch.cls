/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Sep-2015.
**/

global class UpdateRecordTypesBatch implements Database.Batchable<sObject>, Database.Stateful 
{
    global List<Opportunity> listaOportunidades = new List<Opportunity>(); 
    global List<Quote> listaQuotes = new List<Quote>();
    
    
    global Database.Querylocator start( Database.BatchableContext BC )
    {
        string tipo = 'Technical Services';
        string query = '';
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
        
        
        //query = 'Select RecordTypeId, (Select RecordTypeId From Quotes) From Opportunity Where RecordTypeId = \'' + recordTypeId +'\'';
        query = 'Select RecordTypeId From Opportunity Where RecordTypeId = \'' + recordTypeId +'\'';
        query += ' AND Type = \'' + tipo + '\' ORDER BY RecordTypeId';  
		
        return Database.getQueryLocator( query );
    }
    
    
    global void execute( Database.BatchableContext BC, List<sObject> scope )
    {
        Id recordTypeIdOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId(); 
        Id recordTypeIdQuote = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();
        Set<Id> idsOpp = new Set<Id>();
        
        System.Debug('Tamanho scope ' + scope.size());
        for ( sObject obj : scope )
        {
            Opportunity oportunidade = (Opportunity) obj;
            
            oportunidade.RecordtypeId = recordTypeIdOpp;
            idsOpp.add(oportunidade.Id);
            listaOportunidades.add(oportunidade);
		}
        for(Quote cotacao : [Select RecordTypeId from Quote where OpportunityId in : idsOpp])
        {
        	cotacao.RecordTypeId = recordTypeIdQuote;
            listaQuotes.add(cotacao);
        }
        
       	update listaOportunidades;
        update listaQuotes;
        
    }
    
    
    global void finish( Database.BatchableContext bcMain )
    {
        
    }   

}