@isTest
public class CRM360_FinanceControllerTest {
    
    private static String accountName = 'Test Account';
    private static String mfirName = '522487';
    //SELECT MFIR_number__c, To_be_applied_QK__c, Block_Status_QK__c, Credit_Limit_QK__c, Dispute_QK__c, Exposure_QK__c, Overdue_QK__c, To_be_due_QK__c FROM MFIR__c WHERE Account__c = :accId];
    
    @testSetup 
    static void setup(){

        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);

        insert accountsToInsert;
        
        List<MFIR__c> mfirsToInsert = new List<MFIR__c>();
        
        MFIR__c mfir1 = new MFIR__c(Name = mfirName);
    	mfir1.Name = mfirName;
        mfir1.MFIR_number__c =mfirName;
        mfir1.Account__c = account.Id;
        mfir1.To_be_applied_QK__c = 1000;
        mfir1.Block_Status_QK__c = 'CIA';
        mfir1.Credit_Limit_QK__c = 1000;
        mfir1.To_be_due_QK__c = 1000;
        mfir1.Dispute_QK__c = 1000;
        mfir1.Overdue_QK__c = 1000;
        mfir1.Exposure_QK__c = 1000;
        mfirsToInsert.add(mfir1);
        
        insert mfirsToInsert;
    }

    
    @isTest
    public static void testMfir(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        //List<Case> Cases = [SELECT Id FROM Case WHERE Subject = :case1subject];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_FinancePage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));

        Test.setCurrentPage(pageRef);
        
        CRM360_FinanceController testAccPlan = new CRM360_FinanceController();
        
        testAccPlan.getThisAccount();
        testAccPlan.getMfirs();
        
        Test.stopTest();
    }

}