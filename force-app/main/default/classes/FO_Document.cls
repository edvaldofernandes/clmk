public class FO_Document{
    //[TEMP]_documentId_pageNumber
    private static final Schema.SObjectType ALLOWED_TYPE = EOD__c.getSObjectType();
    public static final String TEMPORARY_FILE_NAME_PATTERN = '[TEMP]_{0}_{1}';
    private PageReference visualforcePage;
    private List<Id> associatedContentDocumentIds = new List<Id>();
    private List<ContentVersion> associatedContentVersions;
    private String title;
    private String filename;
    private Map<Id, List<Attachment>> temporaryFiles;
    private Blob content;
    
    
   
    public String getTitle(){
        return this.title;
    }
    public void setTitle(String title){
        this.title = title;
    }   
    public String getFilename(){
        return this.filename;
    }
    public void setFilename(String filename){
        this.filename = filename;
    }
    
    public FO_Document(PageReference visualforcePage, List<Id> attachmentsIds){
        this.visualforcePage = visualforcePage;
        this.associatedContentDocumentIds = attachmentsIds;
    }
    public FO_Document(PageReference visualforcePage){
        this.visualforcePage = visualforcePage;
    }
    public void build(Map<String, String> arguments){
        visualforcePage.getParameters().putAll(arguments);
        this.build();
    }
    public void build(){
        if (Test.IsRunningTest()){
            content = Blob.valueOf('UNIT.TEST');
        }
        else{
            content = visualforcePage.getContentAsPDF();
        }
        this.deleteAttachmentsTemporaryFiles();
    }
    public Blob getContent(){
        if(content == null){
            build();
        }
        return content;
    }
    public void attachToRecord(Id recordId){
        ContentVersion conVer = new ContentVersion(
            ContentLocation = 'S',
            PathOnClient = this.filename,
            Title = this.title,
            VersionData = content
        );
        insert conVer;        
        

        Id contentDocumentId = [
            SELECT
                ContentDocumentId
            FROM
                ContentVersion
            WHERE
                Id =:conVer.Id
        ].ContentDocumentId;
        
        ContentDocumentLink cDe = new ContentDocumentLink(
            ContentDocumentId = contentDocumentId,
            LinkedEntityId = recordId,
            ShareType = 'I',
            Visibility = 'AllUsers'        
        );
        insert cDe;
    }
    public static Map<String, List<Attachment>> getAttachmentsImages(List<Id> documentIds){
        Map<Id, ContentVersion> documentMap = new Map<Id, ContentVersion>();
        Map<Id, ContentVersion> contentVersionMap = new Map<Id, ContentVersion>([
            SELECT
                Id,
                Title,
            	ContentDocumentId,
                VersionData
            FROM
                ContentVersion
            WHERE
                ContentDocumentId IN :documentIds
        ]);
        for(Id cvId : contentVersionMap.keyset()){
            ContentVersion cv = contentVersionMap.get(cvId);
            documentMap.put(cv.ContentDocumentId, cv);
        }    
        
        Map<String, List<Attachment>> temporaryFiles = FO_Document.getAttachmentsTemporaryFiles(documentIds);
        
        // renomeia as keys para ser o título de cada anexo
        Map<String, List<Attachment>> response = new  Map<String, List<Attachment>>();
        for(String documentId : temporaryFiles.keySet()){
            ContentVersion cv = documentMap.get(documentId);
            List<Attachment> tempFiles = temporaryFiles.get(documentId);
            
            if(!tempFiles.isEmpty()){
            	String title = cv.title;
                response.put(title, tempFiles);
            }
        }
        return response;
    }
    
    public static void insertTemporaryFile(Blob file, Id documentId){
        FO_Document.insertTemporaryFile(file, documentId, 0);
    }
    public static void insertTemporaryFile(Blob file, Id documentId, Integer pageNumber){
        
        List<ContentDocumentLink> links = [
            SELECT
            	ContentDocumentId,
            	LinkedEntity.Id,
            	LinkedEntity.type
            FROM
            	ContentDocumentLink
            WHERE
            	ContentDocumentId =: documentId
        ];
        List<Attachment> temporaryFiles = new List<Attachment>();
        for(ContentDocumentLink link : links){
            if(link.LinkedEntity.Id != null){
                if(link.LinkedEntity.type == ALLOWED_TYPE.getDescribe().getName()){
                    Attachment attachment = new Attachment(
                        Name=buildTemporaryFilename(documentId, pageNumber),
                        ContentType='image/png',
                        ParentId= link.LinkedEntity.Id,          
                        Body = file
                    );
                    temporaryFiles.add(attachment);
                }
            }                 
        }
        insert temporaryFiles;
    }
    
    public Map<String, List<Attachment>> getAttachmentsTemporaryFiles(){
        return getAttachmentsTemporaryFiles(associatedContentDocumentIds);
    }
    public static Map<String, List<Attachment>> getAttachmentsTemporaryFiles(List<Id> documentIds){
        Map<String, Map<Integer, Attachment>> documentTemporaryFiles = new Map<String, Map<Integer, Attachment>>();
		List<String> expressions = buildTemporaryFilename(documentIds, '%');
        for(Id documentId  : documentIds){
            documentTemporaryFiles
                .put(documentId, new Map<Integer, Attachment>());
        }
        List<Attachment> temporaryFiles = getTemporaryFilesByName(expressions);

        for(Attachment temporaryFile : temporaryFiles){
            List<String> splitName = temporaryFile.Name.split('_');
            String attachmentId = splitName[1];
            Integer pageNumber = Integer.valueOf(splitName[2]);
            
            Map<Integer, Attachment> pageMap = documentTemporaryFiles
                .get(attachmentId);
            
            pageMap.put(pageNumber, temporaryFile);
            documentTemporaryFiles.put(attachmentId, pageMap);
        }
        
        Map< String, List<Attachment> > result = new Map< String, List<Attachment> >();
        for(String documentId : documentTemporaryFiles.keySet()){
            Map<Integer, Attachment> pageMap = documentTemporaryFiles.get(documentId);
            List<Attachment> pages = sortPages(pageMap);
            result.put(documentId, pages);
        }
        return result;
    } 
    
    public void deleteAttachmentsTemporaryFiles(){
        String expression = String.format(
			TEMPORARY_FILE_NAME_PATTERN,
            new List<String>{'%', '%'}
        );
        List<Attachment> temporaryFiles = getTemporaryFilesByName(
            new List<String>{expression} );
        delete temporaryFiles;
    }
    
    public static List<String> buildTemporaryFilename(List<Id> documentIds, String page){
        List<String> expressions = new List<String>();
        for(Id documentId  : documentIds){
            String expression = String.format(
                TEMPORARY_FILE_NAME_PATTERN,
                new List<String>{documentId, '%'}
            );
            expressions.add(expression);
        }
        return expressions;
    }    
    public static String buildTemporaryFilename(Id documentId, Integer pageNumber){
        List<String> args = new List<String>{documentId, String.valueOf(pageNumber)};
        return String.format(
            TEMPORARY_FILE_NAME_PATTERN,
            args
        );
    }
    
    public static List<Attachment> getTemporaryFilesByName(List<String> names){
        List<Attachment> temporaryFiles = [
            SELECT 
            	Id,
            	Name
            FROM 
            	Attachment
            WHERE
            	Name LIKE :names 
            ORDER BY Name ASC
        ];   
        return temporaryFiles;
    }
    public static List<Attachment> sortPages (Map<Integer, Attachment> pagesMap){
        List<Integer> pageIndex = new List<Integer>(pagesMap.keySet());
        List<Attachment> pages = new List<Attachment>();
        pageIndex.sort();
        for(Integer index : pageIndex){
            Attachment page = pagesMap.get(index);
            pages.add(page);
        }
        return pages;      
    }
}