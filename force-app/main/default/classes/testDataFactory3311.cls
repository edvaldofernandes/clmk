@isTest
public class testDataFactory3311 {
    
    //key: method names have suffix with format "_mX_cX"
    //     for example method name "create3311data_m1_c0" means
    //     the LLPDatabase__c record has a value for manufacturer__c
    //     and no value for city_state_of_manufacturer__c
    
    
    //creates LLPDatabase record for use in create3311test
    @isTest public static ApexPages.StandardController create3311data_m1_c1(){
        LLPDatabase__c part = new LLPDatabase__c(Name='PN1', Description__c='description1', manufacturer__c='manufacturer1', city_state_of_manufacture__c='city1', Is_Part_Serialized__c='Serialized');
        insert part;													//insert new LLPDatabase record
        PageReference pageRef = Page.attach3311xdp;						//create page reference instance of "attach3311xdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', part.id);		//set ID of current page to the ID of the LLPDatabase record
        return new ApexPages.StandardController(part);					//return a standard controller for LLPDatabase__C
    }
    
    //creates LLPDatabase record for use in create3311test
    @isTest public static ApexPages.StandardController create3311data_m1_c0(){
        LLPDatabase__c part = new LLPDatabase__c(Name='PN2', Description__c='description2', manufacturer__c='manufacturer2', Is_Part_Serialized__c='Serialized');
        insert part;													//insert new LLPDatabase record
        PageReference pageRef = Page.attach3311xdp;						//create page reference instance of "attach3311xdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', part.id);		//set ID of current page to the ID of the LLPDatabase record
        return new ApexPages.StandardController(part);					//return a standard controller for LLPDatabase__C
    }
    
    //creates LLPDatabase record for use in create3311test
    @isTest public static ApexPages.StandardController create3311data_m0_c1(){
        LLPDatabase__c part = new LLPDatabase__c(Name='PN3', Description__c='description3', city_state_of_manufacture__c='city3', Is_Part_Serialized__c='Serialized');
        insert part;													//insert new LLPDatabase record
        PageReference pageRef = Page.attach3311xdp;						//create page reference instance of "attach3311xdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', part.id);		//set ID of current page to the ID of the LLPDatabase record
        return new ApexPages.StandardController(part);					//return a standard controller for LLPDatabase__C
    }
    
    //creates LLPDatabase record for use in create3311test
    @isTest public static ApexPages.StandardController create3311data_m0_c0(){
        LLPDatabase__c part = new LLPDatabase__c(Name='PN4', Description__c='description4', Is_Part_Serialized__c='Serialized');
        insert part;													//insert new LLPDatabase record
        PageReference pageRef = Page.attach3311xdp;						//create page reference instance of "attach3311xdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', part.id);		//set ID of current page to the ID of the LLPDatabase record
        return new ApexPages.StandardController(part);					//return a standard controller for LLPDatabase__C
    }
}