@isTest
private class IdpJitHandlerTest {

    @isTest 
    private static void createUpdateAndDenyUser() {
        //Test Setup
        final String EMPLOYEE_NUMBER = '506050';
        final String ALIAS = 'rcecarv';
        final String WEBGRUPO = 'web-salesforce-defense-qas, web-salesforce-defense-prd';
        final String DEPARTMENT1 =  'VPI/DPS/GGG//';
        final String DEPARTMENT2 =  'CCC/BBB/AAA//';
        final String MANAGER =  'CN=BOSSUSER,OU=SEGUNDAVARIAVEL,OU=TERCEIRA VARIAVEL';
        final Id samlSsoProviderId = '0LE000000000000';
        final Id communityId = null;//'0DB000000000000';
        final Id portalId = null;//'0DB000000000000';
        final String federationIdentifier = 'aaaatestetestetest@embraer.net.br';
        final Map<String, String> attributes = new Map<String, String> {
            'EmployeeNumber' => EMPLOYEE_NUMBER,
            'Alias' => ALIAS,
            'Department' => DEPARTMENT1,
            'LastName' => 'LastName',
            'mail2' => federationIdentifier,
            'Company' => 'EMBRAER S.A.',
            'FirstName' => 'PrimeiroNome',
            'Manager' => MANAGER,
            'WebGroupList' => 'NOPE'
        };
        final Map<String, String> attributes2 = new Map<String, String> {
            'EmployeeNumber' => EMPLOYEE_NUMBER,
            'Alias' => ALIAS,
            'Department' => DEPARTMENT2,
            'LastName' => 'LastName',
            'mail2' => federationIdentifier,
            'Company' => 'EMBRAER S.A.',
            'FirstName' => 'PrimeiroNome',
            'Manager' => MANAGER,
            'WebGroupList' => WEBGRUPO
        };
            final Map<String, String> attributes3 = new Map<String, String> {
            'EmployeeNumber' => EMPLOYEE_NUMBER,
            'Alias' => ALIAS,
            'Department' => DEPARTMENT2,
            'mail2' => federationIdentifier,
            'Company' => 'EMBRAER S.A.',
            'FirstName' => 'PrimeiroNome',
            'LastName' => 'LastName',
            'Manager' => MANAGER,
            'WebGroupList' => 'WithoutWebGroups'
        };
        final String assertion = 'assertion';
		
		//Create User Tests
		//List<User_Request__c> newUR = [SELECT Id, Employee_Number__c, Alias__c, Department__c, E_mail__c FROM User_Request__c];
        //System.debug('newUser' + newUR);
        //System.assertEquals(true, newUR.size()>0);
        //System.assertEquals(EMPLOYEE_NUMBER, newUR[0].Employee_Number__c);
        //System.assertEquals(ALIAS, newUR[0].Alias__c);
        //System.assertEquals(DEPARTMENT1, newUR[0].Department__c);
        //System.assertEquals(federationIdentifier, newUR[0].E_mail__c);
        
        user u = new User();
        u.Username = federationIdentifier;
        u.Alias = ALIAS;
        u.Email = federationIdentifier;
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1][0].Id;
        u.FederationIdentifier = federationIdentifier;
        u.LastName = 'LastName';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'America/Sao_Paulo';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
		insert u;
        
        Test.startTest();
        IdpJitHandler handler = new IdpJitHandler();
        handler.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes2, assertion);
        u = [SELECT id from User WHERE FederationIdentifier =: federationIdentifier];
        handler.updateUser(u.id, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes2, assertion);
        User updatedUser = [SELECT id,EmployeeNumber,Alias,Department,FederationIdentifier FROM User ORDER BY CreatedDate DESC LIMIT 1];
        Test.stopTest();
        
        //Update User Tests
		System.assertEquals(EMPLOYEE_NUMBER, updatedUser.EmployeeNumber);
        System.assertEquals(ALIAS, updatedUser.Alias);
        System.assertEquals(DEPARTMENT2, updatedUser.Department);
        System.assertEquals(federationIdentifier, updatedUser.FederationIdentifier);
        
        
        //Waiting for ITWF CONFIG
        //Deny User
        //User deniedUser = [SELECT id,EmployeeNumber,Alias,Department,FederationIdentifier,isActive FROM User ORDER BY CreatedDate DESC LIMIT 1];
        //handler.updateUser(u.id, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        //deniedUser = [SELECT id, isActive FROM User ORDER BY CreatedDate DESC LIMIT 1];
        
        //Deny User Tests
		//System.assertEquals(true, deniedUser.isActive);
        //System.assertEquals(true, [SELECT Id, UserId, IsFrozen FROM UserLogin WHERE UserId =: u.Id].IsFrozen);
    }
}