public class CreateEmbTechDispo {

    
    public string notification{get;set;}
    public string serialNumber{get;set;} 
    public string AircraftSerialNumber{get;set;}
    public string AcType{get;set;} 
    public Date DocDate{get;set;}
    public string PartNumber{get;set;} 
    public string Description{get;set;}
    public string ReasonForRemoval{get;set;} 
    public string CaseNumber{get;set;}
    public string CustomerName{get;set;} 
    public string RepairStation{get;set;}
    public string ShopFinding{get;set;} 
  	public string CSEAnalysis{get;set;}
    public string EngineeringRecomendation{get;set;}
    public string FinalDispo{get;set;}
    public string DamageType{get;set;} 
    public boolean FinalDispoIsBlank{get;set;} {FinalDispoIsBlank=false;}
    public boolean EngineeringRecomendationsIsBlank{get;set;} {EngineeringRecomendationsIsBlank=false;}
    public boolean ShopFindingIsBlank{get;set;} {ShopFindingIsBlank=false;}
    public boolean CSEAnalysisIsBlank{get;set;} {CSEAnalysisIsBlank=false;}
    
    //public boolean RepairStationIsBlank{get;set;} {RepairStationIsBlank=false;}
    public boolean ReasonForRemovalIsBlank{get;set;} {ReasonForRemovalIsBlank=false;}
    //public boolean DamageTypeIsBlank{get;set;} {DamageTypeIsBlank=false;}
    
    
    @TestVisible private Case c; //case 
      

    //constructor CSE_Comments__c
    
    public CreateEmbTechDispo(ApexPages.StandardController standardPageController) {
	c = (Case)standardPageController.getRecord(); //instantiate the Case object for the current record
        c = [SELECT Id,Notification__c,Disposition__c,Part_Number__r.Name, CSE_Comments__c,Part_Number__r.Description__c, Engineering_Recommendation__c,Shop_Findings__c, Repair_Station__r.Name, Customer_Name__c, CaseNumber, Reason_for_Removal__c, Notification_Date__c,Damage_Type__c, A_C_TypeSJK__c,Aircraft_Serial_Number__r.Name,Serial_Number__c 
                FROM Case
                WHERE id = :ApexPages.currentPage().getParameters().get('id')];	//gets the information needed from the case
       
    
//    if(String.Isblank(c.Engineering_Recommendation__c))				   
  //          EngineeringRecomendationsIsBlank=true;
    //    if(String.isBlank(c.Shop_Findings__c))  
      //      ShopFindingIsBlank=true;s
 //       if(String.Isblank(c.CSE_Comments__c))					 
    //        CSEAnalysisIsBlank=true;
   //     if(String.Isblank(c.Reason_for_Removal__c))		
   //         ReasonForRemovalIsBlank=true;
   ////     if(String.isBlank(c.Disposition__c))  
      //      FinalDispoIsBlank=true;
    

    
    }
    
            
    //create and return xml string, adding all the parameters needed
    @TestVisible private String getXmlString(){
        
        //creating xml string for the full document
            string s = '<?xml version="1.0" encoding="UTF-8"?>' +
                '<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' +
                '<f href="S:\\Material Services\\06 - OPERATIONAL SUPPORT\\Developments\\EmbEngDispo\\EmbEngDispoBlank.pdf"/>' +
                '<fields>' +
                '<field name="Date"><value>'+system.today().format()+'</value></field>'+
                '<field name="Notification"><value>'+c.Notification__c+'</value></field>'+
               '<field name="DocDate"><value>'+ system.today().format() + '</value></field>'+
                '<field name="FinalDispo"><value>'+c.Disposition__c+'</value></field>'+
                '<field name="PartNumber"><value>'+c.Part_Number__r.Name+'</value></field>'+
                '<field name="PartDescription"><value>'+c.Part_Number__r.Description__c+'</value></field>'+
                '<field name="EngineeringRecomendation"><value>'+c.Engineering_Recommendation__c+'</value></field>'+
                '<field name="CSEcomments"><value>'+c.CSE_Comments__c+ '</value></field>'+
                '<field name="ShopFindings"><value>'+c.Shop_Findings__c+'</value></field>'+
                '<field name="RepairStation"><value>'+c.Repair_Station__r.Name+'</value></field>'+
                '<field name="CustName"><value>'+c.Customer_Name__c+'</value></field>'+
                '<field name="CaseNumber"><value>'+c.CaseNumber+'</value></field>'+
                '<field name="ReasonForRemoval"><value>'+c.Reason_for_Removal__c+'</value></field>'+
                '<field name="ACType"><value>'+c.A_C_TypeSJK__c+'</value></field>'+
                '<field name="AircraftSerialNumber"><value>'+c.Aircraft_Serial_Number__r.Name+'</value></field>'+
                '<field name="SerialNumber"><value>'+c.Serial_Number__c+'</value></field>'+
                '<field name="DamageType"><value>'+c.Damage_Type__c+'</value></field>' +
 
                '</fields>' +
                '</xfdf>';
            return s;
        }
        
	public PageReference XDPInit() {
//       	if(FinalDispoIsBlank)
  //          c.Disposition__c=FinalDispo;
    //    if(EngineeringRecomendationsIsBlank)
      //      c.Engineering_Recommendation__c=EngineeringRecomendation;
        //if(ShopFindingIsBlank)
          //  c.Shop_Findings__c=ShopFinding;
  //      if(CSEAnalysisIsBlank)
    //        c.CSE_Comments__c=CSEAnalysis;  
      //  if(RepairStationIsBlank)
        //    c.Repair_Station_Name__c=RepairStation;
      //  if(ReasonForRemovalIsBlank)
        //    c.Reason_for_Removal__c=ReasonForRemoval; 
    
        
    //the main function 
	//gets the xml string
	//saves the xdp file as an attachment
	//redirects back to the PNDatabase page
	//public PageReference XDPInit() {
    
        
        
        //creates the string which will be the content of the XDP file
        String xmlContent = getXmlString();
    	
        //creates xdp file out of the xml string then attach it to the case record
		Attachment attachment = new Attachment();
       	attachment.Body = Blob.valueOf(xmlContent);
       	attachment.Name = 'FormEmbEngDispo_'+c.CaseNumber.substring(0,3)+'.XDP';
       	attachment.ParentId = c.Id;
        insert attachment;
        
        //when info is needed, updates the case with the new user input
    //    if(FinalDispoIsBlank || EngineeringRecomendationsIsBlank || ShopFindingIsBlank || CSEAnalysisIsBlank || ReasonForRemovalIsBlank)
      //      update c;
                
        
        //redirect to the original PNDatabase page
        PageReference pageWhereWeWantToGo = new ApexPages.StandardController(c).view(); //we want to redirect the User back to the Account detail page
		pageWhereWeWantToGo.setRedirect(true); //indicate that the redirect should be performed on the client side
		return pageWhereWeWantToGo; //send the User on their way
    }
    
        
}