/**
* @description: FO_EodDocumentController test class.
**/
@isTest
public class FO_EodDocumentControllerTest {
    
    
    /**
    * @description Test if no error occurred while getting the revision record.
    **/
    @isTest static void testIsGettingReviewLog(){
        //The best way to test this is to mock the getReviewLog method,
        //but i do not found a way to do this in Salesforce.
        EOD__c eod = new FO_EODTestDataBuilder().build();  
		ApexPages.currentPage().getParameters().put('id',eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
	    FO_EodDocumentController controller  = new FO_EodDocumentController(stdController);
        controller.getReviewLog();
    }
    
    /**
    * @description Test if no error occurred while getting aircraft related to inputted EOD.
    **/
    @isTest static void testIsGettingAssociatedAircraft(){
        //The best way to test this is to mock the getAssociatedAircraft method,
        //but i do not found a way to do this in Salesforce.
        EOD__c eod = new FO_EODTestDataBuilder().build();  
		ApexPages.currentPage().getParameters().put('id', eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
	    FO_EodDocumentController controller  = new FO_EodDocumentController(stdController);
        controller.getAssociatedAircraft();
    }
    
    @isTest static void testCanGetAttachmentsImages(){
        //needs better tests with mock.
        EOD__c eod = new FO_EODTestDataBuilder().build();  
		ApexPages.currentPage().getParameters().put('id', eod.id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(eod);
	    FO_EodDocumentController controller  = new FO_EodDocumentController(stdController);
		controller.getAttachments();
    }
}