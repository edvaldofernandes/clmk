/* Author: Fabiano Albino Ferreira - TCS
 * Description: Populate aggregate status.
 * CreatedDate: 11/09/2019.
 * LastModifiedDate: 20/09/2019
 */

public class CaseAggregateStatus {
    
    private List<Case> cases;
    private Map<Id, Case> oldMap;
    
    private final String STATUS = Label.CRC_Interface;
    private final Id CRC_TYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRC').getRecordTypeId();
    private final Set<String> statusAggregate = new Set<String>{
        'Back order', 'Contract Creation', 'Pricing', 'Financial', 
        'Engineering', 'Follow Up', 'New Customer', 'Warehouse', 'RTS', 
        'KYC', 'Pending Commercial Decision', 'Pending Cust Info'
    };
    
    public CaseAggregateStatus(List<Case> cases, Map<Id, Case> oldMap){
        this.cases = cases;
        this.oldMap = oldMap;
    }
    
    //Method to populate aggregate status.
    public void assignStatusAggregate(){
        
        for(Case caseSObj : cases){
            
            String oldStatus = '';
            Case oldCase = new Case();
            
            if(oldMap.containsKey(caseSObj.Id)){
                oldCase = oldMap.get(caseSObj.Id);
                oldStatus = oldCase.Status;
            }
            
            /*if(String.isBlank(caseSObj.Status) || caseSObj.Status == oldStatus || caseSObj.RecordTypeId != CRC_TYPE){
                continue;
            }*/
            
            if(caseSObj.RecordTypeId != CRC_TYPE){
                continue;
            }
            
            if(statusAggregate.contains(caseSObj.Status) && caseSObj.Sent_Customer_Notification__c){
                caseSObj.AggregateStatus__c = STATUS;
            }else if(caseSObj.Status == 'Processing' && caseSObj.Sent_Customer_Notification__c){
            	caseSObj.AggregateStatus__c = Label.CRC_CustomerNotification;
            }else if(caseSObj.Status == 'Processing' && !caseSObj.Sent_Customer_Notification__c){
                caseSObj.AggregateStatus__c = 'Processing';
            }else{
				caseSObj.AggregateStatus__c = caseSObj.Status;                
            }
            
        }
        
    }

}