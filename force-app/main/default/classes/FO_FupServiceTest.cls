/**
* @description: FO_FupService test class.
**/
@isTest
public class FO_FupServiceTest {
	/**
    * @description Test if isPublished method returns true when the fup is
    * published.
    **/
    @isTest static void testIsPublishedReturnTrueWhenFupIsPublished(){
        Fup__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
                
        System.assert(FO_FupService.isPublished(fup.Id));
    }
    
	/**
    * @description Test if isPublished method returns false when the fup is not
    * published.
    **/
    @isTest static void testIsPublishedReturnFalseWhenFupIsNotPublished(){
        Fup__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Unpublished')
            .build();
        
        System.assert(FO_FupService.isPublished(fup.Id) == false);
    }
    
    /**
    * @description Test if it is possible to get published fups.
    **/
    @isTest static void testCanGetPublishedFup(){
        Fup__c expectedFup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .build();
        
        
        Fup__c actualFup = FO_FupService.getPublishedFupById(expectedFup.Id);
        
        
        System.assertEquals(expectedFup.Id, actualFup.Id);
    }
    
    @isTest static void testIfExceptionIsThrowWhenGettingAnUnpublishedFup(){
        String exceptionType = 'FlightOpsException';
        Fup__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Unpublished')
            .build();
        try{
            FO_FupService.getPublishedFupById(fup.Id);
        } catch(Exception e){
             System.assertEquals(exceptionType, e.getTypeName());
        }
    }
    
    /**
    * @description Test if it is possible to send FUP Digest to a contact list .
    **/
    @isTest static void testCanFilterUpdatesToFupDigest(){
        // PRECISA TERMINAR
        FUP__c fup = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        FUP_Update__c unexpectedUpdate = new FO_FupUpdateTestDataBuilder()
            .withStatus('Published')
            .withPublishDate(System.today())
            .withParentFup(fup)
            .build();        
        Contact contact = new Contact(
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact;
        List<Fup_Update__c> updates = FO_FupService.getUpdatesForDigestByContact(contact.Id);
        System.debug('updates = ' + updates);
    }   
    
    /**
    * @description Test if it is possible to send FUP Digest to a contact list .
    **/
    /*
    @isTest static void testCanSendFupDigestToAllContactsWhoWantToReceiveIt(){
        User u = [SELECT Id FROM User LIMIT 1];
        System.runAs(u) {
            EmailTemplate emailTemplate = new EmailTemplate(
                DeveloperName = FO_FupRepository.FUP_DIGEST_EMAIL_TEMPLATE_NAME,
                Name = 'Lorem Ipsum',
                TemplateType = 'text',
                IsActive = true,
                FolderId = u.Id
            );
            insert emailTemplate;         
        }  
        Contact contact1 = new Contact(
        	LastName = 'Lorem Ipsum',
            Email = 'lorem@ipsum.com',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact1;
        Contact contact2 = new Contact(
        	LastName = 'qwwerrtm',
            Email = 'ipsum@lorem.com',
            FlightOps_FUP_Report__c = false,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        insert contact2;
        Contact contact3 = new Contact(
        	LastName = 'asdf',
            Email = 'dolor@amet.com',
            FlightOps_FUP_Report__c = false,
            FlightOps_ERJ_WW_FUP_Report__c = false
        );
        insert contact3;
        
        Test.StartTest();
   			FO_FupService.sendFupDigestToContactsWhoWantsToReceiveIt();
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.assertEquals(1, invocations, 'An email has not been sent');  
    }
	*/

    /**
    * @description Test if it is possible to build an map with fups and updates.
    **/
    @isTest static void testCanBuildFupUpdateMap(){
        Map<FUP__c, List<FUP_Update__c>> expectedResponse = new Map<FUP__c, List<FUP_Update__c>>();
        FUP__c parentFup1 = new FO_FupTestDataBuilder().build();
        FUP_Update__c fupUpdate1 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(parentFup1)
            .build();
        FUP_Update__c fupUpdate2 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(parentFup1)
            .build();
        FUP__c parentFup2 = new FO_FupTestDataBuilder().build();
        FUP_Update__c fupUpdate3 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(parentFup2)
            .build();        
        
        Map<FUP__c, List<FUP_Update__c>> actualResponse = FO_FupService.buildFupUpdateMap(
            new List<Fup_Update__c>{
                fupUpdate1,
                fupUpdate2,
                fupUpdate3
        });
        
        
        System.assert(!actualResponse.isEmpty());
        for(FUP__c fup : actualResponse.keyset()){
            List<Fup_Update__c> FupUpdates = actualResponse.get(fup);
            for(Fup_Update__c u : FupUpdates){
                System.assertEquals(fup.Id, u.Fup__c);
            }            
        }
        
    }
    
    /**
    * @description Test if it is possible to build an map with fups and
    * published updates.
    **/
    @isTest static void testCanGetPublishedUpdatesMap(){
        FUP__c parentFup1 = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('EMBRAER 175')
            .build();
        
        FUP_Update__c fupUpdate1 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(parentFup1)
            .build();
        FUP_Update__c fupUpdate2 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(parentFup1)
            .build();
        FUP__c parentFup2 = new FO_FupTestDataBuilder().build();
        FUP_Update__c fupUpdate3 = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .build();        
        
        Map<Id, List<FUP_Update__c>> actualResponse =
            FO_FupService.getPublishedFupsWithUpdatesByAircraftFamily(
                new List<String>{'EJet'}
            );
        
        
        System.assert(!actualResponse.isEmpty());
        for(Id fupId : actualResponse.keyset()){
            List<Fup_Update__c> FupUpdates = actualResponse.get(fupId);
            for(Fup_Update__c u : FupUpdates){
                System.assertEquals(fupId, u.Fup__c);
            }            
        }
    }
    
    
    /**
    * @description Test if can build a map with public fups and its updates.
    **/
    @isTest static void testAllPublicFupsAreObtainedOnMap(){
        Map<Id, List<FUP_Update__c>> expectedResponse = new Map<Id, List<FUP_Update__c>>();
        FUP__c fup1 = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('ERJ-145/140/135')
            .build();
        FUP__c fup2 = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('ERJ-145/140/135')
            .build();
        FUP__c fup3 = new FO_FupTestDataBuilder()
            .withPublishStatus('Published')
            .withAircraftFamily('ERJ-145/140/135')
            .build(); 
        FUP_Update__c fupUpdate1 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(fup1)
            .build();
        FUP_Update__c fupUpdate2 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(fup1)
            .build();
        FUP__c parentFup2 = new FO_FupTestDataBuilder().build();
        FUP_Update__c fupUpdate3 = new FO_FupUpdateTestDataBuilder()
            .withParentFup(fup2)
            .whichIsPublic()
            .build(); 
        
        Map<Id, List<FUP_Update__c>> actualResponse =
            FO_FupService.getPublishedFupsWithUpdatesByAircraftFamily(
                new List<String>{'ERJ'}
            );
        
        System.debug('actualResponse = ' + actualResponse);
        
        System.assert(!actualResponse.isEmpty());
    }
}