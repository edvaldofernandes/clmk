/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Dec-2016.
**/

@isTest
private class UpdateSummarySkylineTest
{
/*
    static testmethod void test() 
    {
       
       product2 aircraft = SObjectInstanceTest.createProduct2(Schema.SObjectType.Product2.getRecordTypeInfosByName().get('DCT Aircraft').getRecordTypeId());
            aircraft.Aircraft_Family__c = 'E1';
            insert aircraft;
       
            PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(Test.getStandardPricebookId(), aircraft.Id);
            insert pbe;  
        
            Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Government').getRecordTypeId());
            insert acc;
            
            Account acc2 = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Leasing Company').getRecordTypeId());
            insert acc2;
            
            Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
            agreement.Name = 'Test 1';
            agreement.Apttus__Account__c = acc.Id;
            agreement.Country__c = 'Brasil';
            agreement.Agreement_Code__c = 'ABCDEFG';
            agreement.Region_Code__c = 'RegionCODE';
            agreement.Apttus__Status_Category__c = 'PA Signed';
            agreement.RecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
            agreement.Apttus__Company_Signed_Date__c = Date.today(); 
            agreement.Kickoff_trigger_already_executed__c = false; 
            agreement.Admin_Mode__c = 'true';
            insert agreement;
            
            Apttus__APTS_Agreement__c agreement1 = new Apttus__APTS_Agreement__c();
            agreement1.Name = 'Test 1';
            agreement1.Apttus__Account__c = acc2.Id;
            agreement1.Country__c = 'Brasil';
            agreement1.Agreement_Code__c = 'ABCDEFG';
            agreement1.Region_Code__c = 'RegionCODE';
            agreement1.Apttus__Status_Category__c = 'PA Signed';
            agreement1.RecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Purchase Agreement').getRecordTypeId();
            agreement1.Apttus__Company_Signed_Date__c = Date.today(); 
            agreement1.Kickoff_trigger_already_executed__c = false; 
            agreement1.Admin_Mode__c = 'true';
            insert agreement1;
        
            Aircraft_Price__c aircraftPrice = new Aircraft_Price__c();
            aircraftPrice.Model__c = aircraft.Id;
            aircraftPrice.Aircraft_Version__c = 'IGW';
            aircraftPrice.Economic_Condition__c = '132131';
            aircraftPrice.Agreement__c = agreement.Id;
            aircraftPrice.Aircraft_List_Price__c = 32131231.00;
            aircraftPrice.Aircraft_Basic_Price__c = 32141212321.00;
            insert aircraftPrice;
            
            Aircraft_Price__c aircraftPrice1 = new Aircraft_Price__c();
            aircraftPrice1.Model__c = aircraft.Id;
            aircraftPrice1.Aircraft_Version__c = 'IGW';
            aircraftPrice1.Economic_Condition__c = '132131';
            aircraftPrice1.Agreement__c = agreement1.Id;
            aircraftPrice1.Aircraft_List_Price__c = 32131231.00;
            aircraftPrice1.Aircraft_Basic_Price__c = 32141212321.00;
            insert aircraftPrice1;
            
            List<Apttus__AgreementLineItem__c> agreementItemList = new List<Apttus__AgreementLineItem__c>();
                
            Apttus__AgreementLineItem__c contractAircraft = new Apttus__AgreementLineItem__c();
            contractAircraft.Aircraft__c  = aircraft.Id;
            contractAircraft.Dellivery_Month__c = Date.today();
            contractAircraft.Contract_Aircraft_Number__c = 5;
            contractAircraft.Order_Type__c = 'Firm';
            contractAircraft.Basic_Price__c = 200;
            contractAircraft.Aircraft_Configuration__c = 'AK';
            contractAircraft.Apttus__AgreementId__c = agreement.Id;
            contractAircraft.TREND_Delivery_Date__c = Date.today();
            contractAircraft.SkylineSummaryCalculated__c = 'Firms E1';
            agreementItemList.add(contractAircraft);
            
            Apttus__AgreementLineItem__c contractAircraft2 = new Apttus__AgreementLineItem__c();
            contractAircraft2.Aircraft__c  = aircraft.Id;
            contractAircraft2.Dellivery_Month__c = Date.today();
            contractAircraft2.Contract_Aircraft_Number__c = 5;
            contractAircraft2.Order_Type__c = 'Firm';
            contractAircraft2.Basic_Price__c = 200;
            contractAircraft2.Aircraft_Configuration__c = 'AK';
            contractAircraft2.Apttus__AgreementId__c = agreement.Id;
            contractAircraft2.TREND_Delivery_Date__c = Date.today().addYears(-1);
            contractAircraft2.SkylineSummaryCalculated__c = 'Firms E2';
            agreementItemList.add(contractAircraft2);
            
            Apttus__AgreementLineItem__c contractAircraft3 = new Apttus__AgreementLineItem__c();
            contractAircraft3.Aircraft__c  = aircraft.Id;
            contractAircraft3.Dellivery_Month__c = Date.today();
            contractAircraft3.Contract_Aircraft_Number__c = 7;
            contractAircraft3.Order_Type__c = 'Firm';
            contractAircraft3.Basic_Price__c = 200;
            contractAircraft3.Aircraft_Configuration__c = 'AK';
            contractAircraft3.Apttus__AgreementId__c = agreement.Id;
            contractAircraft3.TREND_Delivery_Date__c = Date.today().addYears(-1);
            contractAircraft3.SkylineSummaryCalculated__c = 'High Risk E1';
            contractAircraft3.Risk__c ='Critical';
            agreementItemList.add(contractAircraft3);
            
            Apttus__AgreementLineItem__c contractAircraft4 = new Apttus__AgreementLineItem__c();
            contractAircraft4.Aircraft__c  = aircraft.Id;
            contractAircraft4.Dellivery_Month__c = Date.today();
            contractAircraft4.Contract_Aircraft_Number__c = 5;
            contractAircraft4.Order_Type__c = 'Firm';
            contractAircraft4.Basic_Price__c = 200;
            contractAircraft4.Aircraft_Configuration__c = 'AK';
            contractAircraft4.Apttus__AgreementId__c = agreement1.Id;
            contractAircraft4.TREND_Delivery_Date__c = Date.today();
            contractAircraft4.SkylineSummaryCalculated__c = 'Firms E1';
            contractAircraft4.lessee_Defined__c = false;
            agreementItemList.add(contractAircraft4);
            
            Apttus__AgreementLineItem__c contractAircraft5 = new Apttus__AgreementLineItem__c();
            contractAircraft5.Aircraft__c  = aircraft.Id;
            contractAircraft5.Dellivery_Month__c = Date.today();
            contractAircraft5.Contract_Aircraft_Number__c = 5;
            contractAircraft5.Order_Type__c = 'Option';
            contractAircraft5.Basic_Price__c = 200;
            contractAircraft5.Aircraft_Configuration__c = 'AK';
            contractAircraft5.Apttus__AgreementId__c = agreement1.Id;
            contractAircraft5.TREND_Delivery_Date__c = Date.today();
            contractAircraft5.SkylineSummaryCalculated__c = 'Firms E1';
            contractAircraft5.lessee_Defined__c = false;
            agreementItemList.add(contractAircraft5);
            
            insert agreementItemList;
       
       Test.startTest();
       UpdateSummarySkyline  updtSKL = new UpdateSummarySkyline();
       Database.executeBatch(updtSKL);
       Test.stopTest();

    }*/
}