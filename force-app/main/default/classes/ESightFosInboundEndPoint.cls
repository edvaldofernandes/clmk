global with sharing class ESightFosInboundEndPoint {
    
    webservice static void execute(List<ESightFosInfo> eSightFosInfoList){
        
        if(eSightFosInfoList == null)
            return;
        
        ESightFosInfoService eSightFosInfoService = new ESightFosInfoService(eSightFosInfoList);
        eSightFosInfoService.parseToObject();
    }
}