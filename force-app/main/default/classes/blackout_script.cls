public class blackout_script {
    private static final String EMBRAER = 'EMBRAER';
    private static final String BBC = 'BBC';
    private static final String TBD = 'Tbd';
    private static final String STANDARD_USER = 'Standard User';

    public static void runScript(){
        //Embraer Users to freeze
        freezeAndMaskUsers(getUsersToBeUpdated(getUserAndEmails(),getCarveOutEmailsAndUsername(EMBRAER)));

        //BBC Users to update email, login and alias
        updateEmailLoginAndAlias(getUsersToBeUpdated(getUserAndEmails(),getCarveOutEmailsAndUsername(BBC)));
        
    }

    //Get Salesforce Users map using email as key
    private static Map<String, User> getUserAndEmails(){
        List<User> userList = getUsers();
        Map<String, User> mapEmailUser = new Map<String, User>();
        for (User u : userList) mapEmailUser.put(u.Email, u);
        
        system.debug('Queried users: ' + userList.size());
        
        return mapEmailUser;
    }

    //Get a map to relate email and username(Embraer user login) from CarveOut Headcount List
    private static Map<String,String> getCarveOutEmailsAndUsername(String Company){
        List<Carve_Out_Headcount__c> carveOutUserList = getCarveOutUserList(Company);//email e username
        Map<String,String> carveOutEmailList = new Map<String,String>();
        for(Carve_Out_Headcount__c coh : carveOutUserList) carveOutEmailList.put(coh.Email__c, coh.Username__c);
        
        system.debug('Queried Carveout users: ' + carveOutUserList.size());
        
        return carveOutEmailList;
    }

    //Get Salesforce Users map using Username as key
    public static Map<String, User> getUsersToBeUpdated(Map<String, User> mapEmailUser, Map<String,String> carveOutEmailList){
        Map<String, User> usersToBeUpdated = new Map<String, User>();
        for(String email : mapEmailUser.keySet()) If(carveOutEmailList.containsKey(email)) usersToBeUpdated.put(carveOutEmailList.get(email), mapEmailUser.get(email));
        return usersToBeUpdated;
    }

    //Get salesforce Users
    private static List<User> getUsers(){
        //system.debug([SELECT Count() FROM User]);
        return [SELECT 	Alias, Name, IsActive, Email, CompanyName, Username,FirstName,LastName,Title,Department,Country,State,CountryCode,Latitude,Longitude,PostalCode,StateCode,Street,Phone,Extension,Fax,MobilePhone,City 
                FROM User
                WHERE Email LIKE '%embraer.com.br%' OR Username LIKE '%embraer.com.br%'];
    }

	//Get Users from CarveOut Headcount List
    private static List<Carve_Out_Headcount__c> getCarveOutUserList(String Company){
        //system.debug([SELECT Count() FROM Carve_Out_HeadCount__c WHERE Company_Assignment__c = :Company AND Name != :TBD]);
        return [SELECT Username__c, Name, Email__c, Classification__c, Company_Assignment__c, 
                Country_Office__c, Current_job_title__c, Fullname__c, Site__c 
                FROM Carve_Out_HeadCount__c WHERE Company_Assignment__c = :Company AND Name != :TBD];
    }

    private static void freezeAndMaskUsers(Map<String, User> usersToBeFreezed){
        List<User> userList = new List<User>();
        Profile p  = [Select id from Profile where name = :STANDARD_USER];
        for(User u : usersToBeFreezed.values()){
            u.FirstName = 'Funcionário';
            u.LastName = 'Embraer';
            u.Title = 'Masked User';
            u.Department = null ;
            u.City = null;
            u.Division = null;
            u.Country = null;
            u.State = null;
            u.CountryCode = null;
            u.Latitude = null;
            u.Longitude = null;
            u.PostalCode = null;
            u.StateCode = null;
            u.Street = null;
            u.Phone = null;
            u.Extension = null;
            u.Fax = null;
            u.MobilePhone = null;
            u.City = null;
            u.UserRoleId = null;
            u.profileID = p.Id;
            userList.add(u);
        }
        
        system.debug('Deactivated users: ' + userList.size());
        database.update(userList);
        List<UserLogin> ul = [SELECT Id, UserId, IsFrozen From UserLogin WHERE UserId IN :userList];
        for (UserLogin user : ul) user.IsFrozen = true;
        database.update(ul);
    }

    private static void updateEmailLoginAndAlias(Map<String, User> usersToBeUpdated){
        List<User> userList = new List<User>();
        String temp = '';
        for (String login : usersToBeUpdated.keySet()){
            User u = usersToBeUpdated.get(login);
           	u.Email = u.Email.replace('@embraer.com.br', '@embraer.net.br');
            u.Username = u.Username.replace('@embraer.com.br', '@embraer.net.br');
            u.Alias = login;
            userList.add(u);
            
        }
        
        system.debug('Updated users: ' + userList.size());
        database.update(userList);
    }
}