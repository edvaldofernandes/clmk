/**
* @author Marcilio Leite de Souza
* @date 29/05/2018
* @description: Receive information from Fly Embraer and handle with communities Usuers
*
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           29 MAY 2018             Original Version
**/
global class GEN_UserFlyEmbraer implements Auth.SamlJitHandler {
  
    private class JitException extends Exception{}
    private User sfUser {get;set;}
    private Contact sfCon {get;set;}
    
    /**
    * @description Implements Auth.SamlJitHandler method to create customer community user
    * @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
    * @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
    * @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
    * @param String federationId : The ID Salesforce expects to be used for this user.
    * @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    * @param String assertion : The default SAML assertion, base-64 encoded.
	* @return User : User created
    **/
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId, 
                           String federationIdentifier, Map<String, String> attributes, String assertion) {
        
        System.debug('createUser.samlSsoProviderId: ' + samlSsoProviderId);    
        System.debug('createUser.communityId: ' + communityId); 
        System.debug('createUser.portalId: ' + portalId); 
        System.debug('createUser.federationIdentifier: ' + federationIdentifier); 
        System.debug('createUser.attributes: ' + attributes); 
        System.debug('createUser.assertion: ' + assertion); 
        
        return handleUser(communityId, attributes, federationIdentifier);
    }
    
    /**
    * @description Implements Auth.SamlJitHandler method to update customer community user
    * @param Id samlSsoProviderId : The ID of the SamlSsoConfig standard object
    * @param Id communityId : The ID of the community. This parameter can be null if you’re not creating a community user.
    * @param Id portalId : The ID of the portal. This parameter can be null if you’re not creating a portal user.
    * @param String federationId : The ID Salesforce expects to be used for this user.
    * @param Map<String, String> attributes : All of the attributes in the SAML assertion that were added to the default assertion.
    * @param String assertion : The default SAML assertion, base-64 encoded.
	* @return void
    **/
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
                           String federationIdentifier, Map<String, String> attributes, String assertion) {
                                                     
        System.debug('updateUser.samlSsoProviderId: ' + samlSsoProviderId);    
        System.debug('updateUser.communityId: ' + communityId); 
        System.debug('updateUser.portalId: ' + portalId); 
        System.debug('updateUser.federationIdentifier: ' + federationIdentifier); 
        System.debug('updateUser.attributes: ' + attributes); 
        System.debug('updateUser.assertion: ' + assertion); 
                               
        handleUser(communityId, attributes, federationIdentifier);
     }
    
    /**
	* @description Handle rules: Create new user OR new Contact and User Assign Permission Set
    * @param Id communityId : Id for the related community attempt to login
    * @param Map<String, String> attribute : Map attributes from FlyEmbraer
    * @param String federationIdentifier : FlyEmbraer = login Salesforce = User.FederationIdentifier (Federation ID)
	* @return void
	*/
    public User handleUser(Id communityId, Map<String, String> attributes, String federationIdentifier) {
        
        String strJson = JSON.serialize(attributes);
        FlyEmbraerUser flyUser = (FlyEmbraerUser) JSON.deserialize(strJson, FlyEmbraerUser.class);
        flyUser.federationIdentifier = federationIdentifier;
        system.debug('GEN_UserFlyEmbraer.handleUser.flyUser: ' + flyUser);
        
        String str = 'SELECT ' + Utils.getAllFields('Community_Setting__mdt') + ' FROM ';
               str +='Community_Setting__mdt WHERE Community_Id__c = \'' + communityId + '\' LIMIT 1 ';     
        Community_Setting__mdt mdtCommu = Database.query(str);
        system.debug('GEN_UserFlyEmbraer.handleUser.mdtCommu: ' + mdtCommu);
        
        sfUser = UserDAO.getUserByFederationIdentifier(federationIdentifier);
        system.debug('GEN_UserFlyEmbraer.handleUser.sfUser: ' + sfUser);
        
     	Savepoint dml; 
            
        try{
            
            dml = Database.setSavepoint();
            
            if(sfUser == null){
                
                sfCon = GEN_ContactDAO.getCommunityContactByFlyEmbraerId(federationIdentifier);
                system.debug('GEN_UserFlyEmbraer.handleUser.sfCon: ' + sfCon);
                
                if(sfCon == null){
                  	Account a = AccountDAO.getListAccountByFlyEmbraerId(flyUser.embCompanyCode)[0];
                    
                    insert setCommuContact(a , flyUser);
                    insert setCommuUser(sfCon , mdtCommu);
                }else{
                    
                    User u = UserDAO.getPortalUserByContactId(sfCon.Id);

                    if(u == null){
                        insert setCommuUser(sfCon , mdtCommu);
                    }else{
                       setCommuUser(sfCon , mdtCommu);
                       this.sfUser.Id = u.Id;
                       update this.sfUser;
                    }
                    
                }

                if(!UserDAO.isPermissionSetAssingUser(sfUser.Id, mdtCommu.PermissionSet_Id__c))
                   UserDAO.upsertUserPermissionFuture(sfUser.Id, mdtCommu.PermissionSet_Id__c);                
                
            }else{
                
                if(!UserDAO.isPermissionSetAssingUser(sfUser.Id, mdtCommu.PermissionSet_Id__c)){
                    UserDAO.upsertUserPermission(sfUser.Id, mdtCommu.PermissionSet_Id__c);  
                }  
            }
            
        }catch(Exception e){
            System.debug('GEN_UserFlyEmbraer.handleUser.Exception: ' + e);
            Database.rollback(dml);
        }
        system.debug('GEN_UserFlyEmbraer.handleUser.end.sfCon: ' + sfCon);
        system.debug('GEN_UserFlyEmbraer.handleUser.end.sfUser: ' + sfUser);

		return sfUser;
    }

    /**
    * @description Set a community Contact
    * @param Account a : The Account related to the contact
    * @param FlyEmbraerUser flyUser : The user related to FlyEmbraer
	* @return Contact : contact setted
    **/
    private Contact setCommuContact(Account a, FlyEmbraerUser flyUser){
        this.sfCon = new Contact();
        this.sfCon.AccountId = a.Id;
        this.sfCon.Email = flyUser.mail;
        this.sfCon.FlyEmbraerUserId__c = flyUser.federationIdentifier;
        this.sfCon.Contact_Status__c = 'Active';
        this.sfCon.LastName = flyUser.fullName;
        return this.sfCon;
    }
    
    /**
    * @description Set a community User
    * @param Contact c : The Contact related to the user
    * @param Community_Setting__mdt mdt : The community settings related 
	* @return User : user setted
    **/
    private User setCommuUser(Contact c, Community_Setting__mdt mdt){
        this.sfUser = new User();
        this.sfUser.Username = c.FlyEmbraerUserId__c + '@flyembraer.com';
        this.sfUser.LastName = c.LastName.substringAfter(' ');
        this.sfUser.Email = c.Email;
        this.sfUser.ContactId = c.Id;
        this.sfUser.Alias = c.FlyEmbraerUserId__c.left(8);
        this.sfUser.FederationIdentifier = c.FlyEmbraerUserId__c;
        this.sfUser.ProfileId = mdt.Profile_Id__c;
        this.sfUser.TimeZoneSidKey = string.valueOf(UserInfo.getTimeZone());
        this.sfUser.LocaleSidKey = UserInfo.getLocale();
        this.sfUser.EmailEncodingKey = 'ISO-8859-1';
        this.sfUser.LanguageLocaleKey = UserInfo.getLanguage();  
        this.sfUser.IsActive = true;
        return this.sfUser;
    }
    
    /**
    * @description FlyEmbraerUser Model Class to FlyEmbraer user
    **/
    @TestVisible
    private Class FlyEmbraerUser{
        @TestVisible String embCompanyCode;
        @TestVisible String embLogin;
        @TestVisible String fullName ;
        @TestVisible String mail;
        @TestVisible String federationIdentifier;
        @TestVisible FlyEmbraerUser(){}
    }
    
}