@isTest
private class CRM360_CustomerSelectionControllerTest {
    
    private static Id RecordTypeIdEmbSite = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId();
    private static List<String> siteNames = new List<String>{
        'Site', 
        'All Sites'
    };
    private static List<String> customerNames = new List<String>{
        'Customer 1', 
        'Customer 2', 
        'Customer 3', 
        'Customer 4', 
        'Customer 5', 
        'Customer 6', 
        'Customer 7', 
        'Customer 8'
    };
    
    @testSetup
    static void setup(){
          
        List<Account> accountsToInsert = new List<Account>();
        
        //Create an Embraer site
        Account siteAccount = new Account();
        siteAccount.Name = siteNames[0];
        siteAccount.Company_Nickname__c = siteNames[0];
        siteAccount.RecordTypeId = RecordTypeIdEmbSite;
        siteAccount.CRM360_Site__c = true;
        insert siteAccount;
        
        //Create customer accounts relative to the Embraer site 
     
        //With aircratf operator quantity greater than 0, active status and without CRM360 data
        Account customerAccount1 = new Account();
        customerAccount1.Name = customerNames[0];
        customerAccount1.Company_Nickname__c = customerNames[0];
        customerAccount1.Embraer_Site__c = siteAccount.Id;
        customerAccount1.Quantity_Embraer_Aircraft_Operator__c = 1;
        customerAccount1.Company_Status__c = 'Active';
        customerAccount1.Has_CRM360_Data__c = false;
        accountsToInsert.add(customerAccount1); 
        
        //With aircratf owner quantity greater than 0, in EIS process status and without CRM360 data
        Account customerAccount2 = new Account();
        customerAccount2.Name = customerNames[1];
        customerAccount2.Company_Nickname__c = customerNames[1];
        customerAccount2.Embraer_Site__c = siteAccount.Id;
        customerAccount2.Quantity_Embraer_Aircraft_Owner__c = 1;
        customerAccount2.Company_Status__c = 'In EIS Process';
        customerAccount2.Has_CRM360_Data__c = false;
        accountsToInsert.add(customerAccount2);
        
        //With aircratf owner quantity greater than 0, active status and with CRM360 data
        Account customerAccount3 = new Account();
        customerAccount3.Name = customerNames[2];
        customerAccount3.Company_Nickname__c = customerNames[2];
        customerAccount3.Embraer_Site__c = siteAccount.Id;
        customerAccount3.Quantity_Embraer_Aircraft_Owner__c = 1;
        customerAccount3.Company_Status__c = 'Active';
        customerAccount3.Has_CRM360_Data__c = true;
        accountsToInsert.add(customerAccount3);
        
        //With aircratf operator quantity greater than 0, in EIS process status and with CRM360 data
        Account customerAccount4 = new Account();
        customerAccount4.Name = customerNames[3];
        customerAccount4.Company_Nickname__c = customerNames[3];
        customerAccount4.Embraer_Site__c = siteAccount.Id;
        customerAccount4.Quantity_Embraer_Aircraft_Operator__c = 1;
        customerAccount4.Company_Status__c = 'In EIS Process';
        customerAccount4.Has_CRM360_Data__c = true;
        accountsToInsert.add(customerAccount4);
        
        //Create a customer account not relative to the Embraer site

        //With aircratf operator quantity greater than 0, active status and without CRM360 data
        Account customerAccount5 = new Account();
        customerAccount5.Name = customerNames[4];
        customerAccount5.Company_Nickname__c = customerNames[4];
        customerAccount5.Quantity_Embraer_Aircraft_Operator__c = 1;
        customerAccount5.Company_Status__c = 'Active';
        customerAccount5.Has_CRM360_Data__c = false;
        accountsToInsert.add(customerAccount5);
        
        //With aircratf owner quantity greater than 0, in EIS process status and without CRM360 data
        Account customerAccount6 = new Account();
        customerAccount6.Name = customerNames[5];
        customerAccount6.Company_Nickname__c = customerNames[5];
        customerAccount6.Quantity_Embraer_Aircraft_Owner__c = 1;
        customerAccount6.Company_Status__c = 'In EIS Process';
        customerAccount6.Has_CRM360_Data__c = false;
        accountsToInsert.add(customerAccount6);
        
        //With aircratf owner quantity greater than 0, active status and with CRM360 data
		Account customerAccount7 = new Account();
        customerAccount7.Name = customerNames[6];
        customerAccount7.Company_Nickname__c = customerNames[6];
        customerAccount7.Quantity_Embraer_Aircraft_Owner__c = 1;
        customerAccount7.Company_Status__c = 'Active';
        customerAccount7.Has_CRM360_Data__c = true;
        accountsToInsert.add(customerAccount7);       
        
        //With aircratf operator quantity greater than 0, in EIS process status and with CRM360 data
        Account customerAccount8 = new Account();
        customerAccount8.Name = customerNames[7];
        customerAccount8.Company_Nickname__c = customerNames[7];
        customerAccount8.Quantity_Embraer_Aircraft_Operator__c = 1;
        customerAccount8.Company_Status__c = 'In EIS Process';
        customerAccount8.Has_CRM360_Data__c = true;
        accountsToInsert.add(customerAccount8);
        
        insert accountsToInsert;
        
    }
    
    @isTest
    public static void testCustomerSelection(){
        List<Account> siteAccounts = [SELECT Id, Name 
                                      FROM Account 
                                      WHERE (Account_Type__c = 'Embraer Site' AND CRM360_Site__c = true)];
        
        Test.startTest();
        PageReference pageRef = Page.CRM360_CustomerSelectionPage; 
        pageRef.getParameters().put('id', String.valueOf(siteAccounts[0].Id));
        Test.setCurrentPage(pageRef);
        List<Account> customerAccounts = CRM360_CustomerSelectionController.getCustomerList();
        String siteName = CRM360_CustomerSelectionController.getSiteName();
        Test.stopTest();
        
        //Verify if query returns null
        System.assertNotEquals(null, customerAccounts, 'Customer list is null');
        
        //Verify if number of customers on the site is correct
        System.assertEquals(4, customerAccounts.size(), 'Number of customers if incorrect');
        
        //Verify if the customers have been recovered in correct order
        System.assertEquals(customerNames[2], customerAccounts[0].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[3], customerAccounts[1].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[0], customerAccounts[2].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[1], customerAccounts[3].Name, 'Filter is not working correctly');
        
        //Verify if site name is correct
        System.assertEquals(siteNames[0], siteName, 'Site name is incorrect');
    }
    
    @isTest
    public static void testCustomerSelectionAllSites(){
        Test.startTest();
        PageReference pageRef = Page.CRM360_CustomerSelectionPage; 
        Test.setCurrentPage(pageRef);
        List<Account> customerAccounts = CRM360_CustomerSelectionController.getCustomerList();
        String siteName = CRM360_CustomerSelectionController.getSiteName();
        Test.stopTest();
        
        //Verify if query returns null
        System.assertNotEquals(null, customerAccounts, 'Customer list is null');
        
        //Verify if number of customers on the site is correct
        System.assertEquals(8, customerAccounts.size(), 'Number of customers if incorrect');
        
        //Verify if the customers have been recovered in correct order
        System.assertEquals(customerNames[2], customerAccounts[0].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[3], customerAccounts[1].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[6], customerAccounts[2].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[7], customerAccounts[3].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[0], customerAccounts[4].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[1], customerAccounts[5].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[4], customerAccounts[6].Name, 'Filter is not working correctly');
        System.assertEquals(customerNames[5], customerAccounts[7].Name, 'Filter is not working correctly');
        
        //Verify if site name is correct
        System.assertEquals(siteNames[1], siteName, 'Site name is incorrect');
    }

}