/**
* @description: myOperengFupUpdateListController test class.
**/
@isTest
public class myOperengFupUpdateListControllerTest {
    @isTest static void testCanGetPublishedUpdates(){
        FUP_Update__c expectedFupUpdate = new FO_FupUpdateTestDataBuilder()
            .whichIsPublic()
            .build();

        List<FUP_Update__c> fupUpdates = myOperengFupUpdateListController.getUpdates(
            expectedFupUpdate.FUP__c
        );	
        System.assertEquals(expectedFupUpdate.Id, fupUpdates.get(0).Id);
    }
}