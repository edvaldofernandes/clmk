@IsTest 
public with sharing class PublicCalendarViewControllerTest
{
        
        
        static testMethod void unitTest()
        {
        
            Account acc = SObjectInstanceTest.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
            insert acc;
            
            Contact contact = SObjectInstanceTest.createContact(acc.Id);
            insert contact;
            
            List<PublicCalendarSettings__c> calendars = new List<PublicCalendarSettings__c>();
            calendars.add(new PublicCalendarSettings__c(name = 'Calendar 1',CalendarId__c = '023123456789102'));
            calendars.add(new PublicCalendarSettings__c(name = 'Calendar 2',CalendarId__c = '023210987654321'));
            insert calendars;
            
            
            PageReference pageRef = Page.PublicCalendarView;
            
            ApexPages.currentPage().getParameters().put('calendarDate','month');
            ApexPages.currentPage().getParameters().put('calendarId','023210987654321');
            ApexPages.currentPage().getParameters().put('calendarName','Calendar 1');
            ApexPages.currentPage().getParameters().put('eventType','Busy');
            ApexPages.currentPage().getParameters().put('subject','Test');
            
            PublicCalendarViewController controller = new PublicCalendarViewController(null); 
            PublicCalendarViewController controllerSp = new PublicCalendarViewController(); 
            
            controllerSp.InvokeCallReport();
            controllerSp.getSelectedCalendarNames();
            controllerSp.getFilterEventTypes();
            controllerSp.pageLoad();
            controllerSp.selectedDate = string.ValueOf(Date.today());
            controllerSp.newEvent();
            PublicCalendarViewController.eventWrapper eventWrapper = new PublicCalendarViewController.eventWrapper();
            eventWrapper.title = 'Event Test';
            eventWrapper.allDay = true;
            //eventWrapper.startDate = string.ValueOf(date.Today());
            //eventWrapper.endDate = string.ValueOf(date.Today());
            eventWrapper.url  = 'www.google.com';
            eventWrapper.className  = 'testClass';
            
            controllerSp.selectedCalendarId = null;
            controllerSp.newEvent();
            
            ApexPages.currentPage().getParameters().put('calendarDate','agendaWeek');
            controller.updateEventParameters();
            
            
            ApexPages.currentPage().getParameters().put('calendarDate','else');
            controller.updateEventParameters();
            
            Test.setCurrentPage(pageRef);
            
        }
        
        


}