@IsTest
public class CRM360_Mod1Tests {
    @testSetup static void setup(){
        Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();
        Id RecordTypeIdEOCSMeetings = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('EOC Side Meeting').getRecordTypeId();
        Id RecordTypeIdEmbSite = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Embraer Site').getRecordTypeId();

        Account testAccountNull 			= new Account();
        testAccountNull.Name				='Test Account Null';
        testAccountNull.Company_Nickname__c ='NullAccount';
        insert testAccountNull;
        
        Account testAccount 			= new Account();
        testAccount.Name				='Test Account record';
        testAccount.Company_Nickname__c ='TesterComp';
        insert testAccount;
        
        Contact testContact 	= new Contact();
        testContact.FirstName 	= 'John';
        testContact.LastName 	= 'Doe';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Account testSiteAccount 			= new Account();
        testSiteAccount.Name				='Test Site record';
        testSiteAccount.Company_Nickname__c ='SiteTest';
        testSiteAccount.RecordTypeId = RecordTypeIdEmbSite;
        insert testSiteAccount;
        
        S_S_Leads__c testLead 			= new S_S_Leads__c();
        testLead.Contact__c 			= testContact.Id;
        testLead.Sales_Potential__c 	= 25000;
        testLead.Level_of_interest__c 	= 'Very Interested';
        testLead.Event__c 				= 'EOC WW EJET 2019 WARSAW';
        insert testLead;
        
        Campaign testCampaign 			= new Campaign();
        testCampaign.Name 				= 'Embraer E-Jets Operators Conference 2019 Warsaw';
        insert testCampaign;
        
        Account_Cockpit__c testSummary 		= new Account_Cockpit__c();
        testSummary.Name 					= 'Test Meeting';
        testSummary.Customer_Expectation__c = 'Expects to test stuff';
        testSummary.Att_Level__c 			= 'High';
        testSummary.RecordTypeId       = RecordTypeIdEOCSMeetings;
        testSummary.Related_EOC__c 			= 'EOC WW EJET 2019 WARSAW';
        testSummary.Account_Name__c 		= testAccount.Id;
        insert testSummary;
        
        Account_Cockpit__c dashboardDataTest 		= new Account_Cockpit__c();
        dashboardDataTest.Name 						= 'Test Airlines CRM360 Data';
        dashboardDataTest.CRM360_EOC__c 			= 'Expects to test stuff';
        dashboardDataTest.CRM360_EOC_Indicator__c	= 'Action Required';
        dashboardDataTest.RecordTypeId 				= RecordTypeIdCRM360Data;
        dashboardDataTest.Account_Name__c 			= testAccount.Id;
        
        insert dashboardDataTest;
        
        CampaignMember testMember 		= new CampaignMember();
        testMember.CampaignId			= testCampaign.Id;
        testMember.ContactId			= testContact.Id;
        testMember.Status  				= 'Registered';
        insert testMember;
    }
    
    @IsTest
    public static void testEOCInfo1(){
        List<S_S_Leads__c> leadsList 			= new List<S_S_Leads__c>();
        List<Account_Cockpit__c> smeetingList 	= new List<Account_Cockpit__c>();
        List<CampaignMember> membersList 		= new List<CampaignMember>();
        Account acc = [SELECT ID FROM Account WHERE Name = 'Test Account record'];
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(acc.Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
        
        Integer leadAmount = CRM360_CompanyEocInfoController.getLeadAmount();
        Integer sMeetingsAmount = CRM360_CompanyEocInfoController.getSMeetingsAmount();
        Integer registeredAmount = CRM360_CompanyEocInfoController.getRegisteredAmount();
        
        String EOCIndicator = CRM360_CompanyEocInfoController.getEOCIndicator();
        Account AccName = CRM360_CompanyEocInfoController.getAcc();
        leadsList = CRM360_CompanyEocInfoController.getLeadsList();
        smeetingList = CRM360_CompanyEocInfoController.getSideMeetingsList();
        membersList = CRM360_CompanyEocInfoController.getCampaignMembers();
        Test.StopTest();
        
        // Asserts
        system.assertEquals(0, leadAmount, 'leadAmount is 0');
        system.assertEquals(0, sMeetingsAmount, 'sMeetingsAmount is 0');
        system.assertEquals(0, registeredAmount, 'registeredAmount is 0');
        system.assertNotEquals(null, EOCIndicator, 'EOCIndicator is null');
        system.assertNotEquals(null, AccName, 'AccName is null');
        system.assertNotEquals(null, leadsList, 'leadsList is null');
        system.assertNotEquals(null, smeetingList, 'smeetingList is null');
        system.assertEquals(null, membersList, 'membersList is null');

    }
    @IsTest
    public static void testEOCInfo2(){
        List<S_S_Leads__c> leadsList 			= new List<S_S_Leads__c>();
        List<Account_Cockpit__c> smeetingList 	= new List<Account_Cockpit__c>();
        List<CampaignMember> membersList 		= new List<CampaignMember>();
        Account acc = [SELECT ID FROM Account WHERE Name = 'Test Account Null'];
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.CRM360_CompanyEocInfoPage; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(acc.Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CompanyEocInfoController testAccPlan = new CRM360_CompanyEocInfoController();
        
        Integer leadAmount = CRM360_CompanyEocInfoController.getLeadAmount();
        Integer sMeetingsAmount = CRM360_CompanyEocInfoController.getSMeetingsAmount();
        Integer registeredAmount = CRM360_CompanyEocInfoController.getRegisteredAmount();
        
        String EOCIndicator = CRM360_CompanyEocInfoController.getEOCIndicator();
        Account AccName = CRM360_CompanyEocInfoController.getAcc();
        leadsList = CRM360_CompanyEocInfoController.getLeadsList();
        smeetingList = CRM360_CompanyEocInfoController.getSideMeetingsList();
        membersList = CRM360_CompanyEocInfoController.getCampaignMembers();
        Test.StopTest();
        
        // Asserts
        system.assertEquals(0, leadAmount, 'leadAmount is not 0');
        system.assertEquals(0, sMeetingsAmount, 'sMeetingsAmount is not 0');
        system.assertEquals(0, registeredAmount, 'registeredAmount is not 0');
        system.assertEquals(null, EOCIndicator, 'EOCIndicator is not null');
        system.assertNotEquals(null, AccName, 'AccName is null');
        system.assertEquals(null, leadsList, 'leadsList is not null');
        system.assertEquals(null, smeetingList, 'smeetingList is not null');
        system.assertEquals(null, membersList, 'membersList is not null');
		
    }
    
    @IsTest
    public static void testCustomerSelection(){
        List<Account> siteAccounts = new List<Account>();
        Account acc = [SELECT ID FROM Account WHERE Name = 'Test Site record'];
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.CRM360_CustomerSelectionPage; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(acc.Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CustomerSelectionController testAccPlan = new CRM360_CustomerSelectionController();
        
        siteAccounts = CRM360_CustomerSelectionController.getCustomerList();
        String siteName = CRM360_CustomerSelectionController.getSiteName();
        Test.StopTest();
        
        // Asserts
        system.assertNotEquals(null, siteAccounts, 'siteAccounts is null');
        system.assertNotEquals(null, siteName, 'siteName is null');
    }
    
    @IsTest
    public static void testListSites(){
        List<Account> sites = new List<Account>();
        
        Test.StartTest(); 
        sites = CRM360_SiteSelectionController.getSiteList();
        Test.StopTest();
        
        // Asserts
        system.assertNotEquals(null, sites, 'sites is null');

    }
    
    @IsTest
    public static void testDashboard1(){
        Account acc = [SELECT ID FROM Account WHERE Name = 'Test Account record'];
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.CRM360_CustomerDashboardPage; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(acc.Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CustomerDashboardController testAccPlan = new CRM360_CustomerDashboardController();
        
        String license = CRM360_CustomerDashboardController.getLicense();
        
        Account_Cockpit__c dashboardData = CRM360_CustomerDashboardController.getDashboardData();
        Account accTest = CRM360_CustomerDashboardController.getThisAccount();
        Test.StopTest();
        
        // Asserts
        system.assertNotEquals(null, dashboardData, 'dashboardData is null');
        system.assertNotEquals(null, accTest, 'accTest is null');
    }
    @IsTest
    public static void testDashboard2(){
        Account acc = [SELECT ID FROM Account WHERE Name = 'Test Site record'];
        
        Test.StartTest(); 
        
        PageReference pageRef = Page.CRM360_CustomerDashboardPage; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(acc.Id));
        Test.setCurrentPage(pageRef);
        
        CRM360_CustomerDashboardController testAccPlan = new CRM360_CustomerDashboardController();
        
        
        
        Account_Cockpit__c dashboardData = CRM360_CustomerDashboardController.getDashboardData();
        Account accTest = CRM360_CustomerDashboardController.getThisAccount();
        Test.StopTest();
        
        // Asserts
        system.assertEquals(null, dashboardData, 'dashboardData is not null');
        system.assertNotEquals(null, accTest, 'accTest is null');
    }
}