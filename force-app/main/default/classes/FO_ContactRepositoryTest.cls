/**
* @description: FO_ContactRepository test class.
**/
@isTest
public class FO_ContactRepositoryTest {
    @isTest static void testIfCanGetContactById(){
        Contact expectedContact = new Contact(
        	LastName = 'Lorem Ipsum'
        );
        insert expectedContact;
        
        
        Contact actualContact = FO_ContactRepository.getContactById(expectedContact.Id);
        
        
        System.assertEquals(expectedContact.Id, actualContact.Id);       
    }    
    @isTest static void testIfCanGetContactsWhoWantsToReceiveFupReport(){
        Account acc = new Account(
        	Name = 'test',
            Company_Nickname__c = 'test',
            Company_Status__c = 'Active'
        );
        insert acc;
        
        Contact expectedContact = new Contact(
            AccountId = acc.Id,
            Contact_Status__c = 'Active',
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = true,
            FlightOps_ERJ_WW_FUP_Report__c = true
        );
        
        Contact unexpectedContact = new Contact(
        	LastName = 'Lorem Ipsum',
            FlightOps_FUP_Report__c = false,
            FlightOps_ERJ_WW_FUP_Report__c = false
        );
        insert expectedContact;
        insert unexpectedContact;
        
        
        List<Contact> actualContacts = FO_ContactRepository.getContactsReceivingFupReport();
        
        List<Id> contactIds = new List<Id>();
        for(Contact c : actualContacts){
            contactIds.add(c.Id);
        }
        
        
        System.assert( contactIds.contains(expectedContact.Id) ); 
        System.assert( !contactIds.contains(unexpectedContact.Id) );
    }

}