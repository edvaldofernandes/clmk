public class ScheduleUpdatePartsInBackOrder implements Schedulable{
    public void execute(SchedulableContext ctx){
        UpdatePartsInBackOrder.Run();
        
        //no longer running the batchified version
        //UpdatePartsInBackOrderBatch backOrderBatch = new UpdatePartsInBackOrderBatch();
        //Database.executeBatch(backOrderBatch, 2000);
    }
}