/* Author: Fabiano Albino Ferreira - TCS
 * 
 * Description: Test class for AssignAccount
 * 
 * CreatedDate: 20/09/2019.
 * 
 * LastModifiedDate: 20/09/2019
 */

@IsTest
public class AssignAccountTest {
    
    @TestSetup
    private static void testSetup(){
        
        Account acc = createAccount();
        Database.insert(acc);

        Contact contact = createContact(acc.Id);
        Database.insert(contact);
        
        MFIR__c mfir = createMFIR(acc.Id);
        Database.insert(mfir);   
    }

    @IsTest
    private static void shouldAssignContactToCaseByEmail(){

        Case c = createCase();

        Contact contact = [SELECT Id FROM Contact WHERE Email = 'assign_account@test.com.br'];
        
        Test.startTest();
        
        	Database.insert(c);
        
        Test.stopTest();
        
        Case caseWithContactId = [SELECT Id, ContactId FROM Case WHERE Id = :c.Id];
        
        System.assertEquals(contact.Id, caseWithContactId.ContactId);
    }
    
    @IsTest
    private static void shouldAssignAccountToCaseMfirNumber(){
        
        Case c = createCase();
        
        Test.startTest();
        
        	Database.insert(c);
        
        Test.stopTest();
        
        Case caseWithAccountId = [SELECT Id, AccountId FROM Case WHERE Id = :c.Id];
        
        System.assert(String.isNotBlank(caseWithAccountId.AccountId));
    }
    
    @IsTest
    private static void shouldAssignAccountToCaseSapName(){
        
        Case c = createCase();
        c.Description = 'Customer : nome_sap_test -';
        
        Test.startTest();
        
        	Database.insert(c);
        
        Test.stopTest();
        
        Case caseWithAccountId = [SELECT Id, AccountId FROM Case WHERE Id = :c.Id];
         
        System.assert(String.isNotBlank(caseWithAccountId.AccountId));
    }
    
    private static MFIR__C createMFIR(Id accountId){
        
        MFIR__c mfir = new MFIR__c(
        	MFIR_number__c = '335353',
            SAP_Name__c = 'nome_sap_test',
            Account__c = accountId
        );
       
        return mfir;
    }
    
    private static Account createAccount(){
        
        Account acc = new Account(
        	BillingCountry = 'Brazil',
        	Name = 'Test',
        	Company_Nickname__c = 'testNickname',
        	FlyEmbraerId__c = '99999'
        );
       
        return acc;
    }

    private static Contact createContact(Id accountId){

        Contact contact = new Contact(
            FirstName = 'Test-First',
            LastName = 'Test-Second',
            AccountId = accountId,
            Email = 'assign_account@test.com.br'
        );

        return contact;
    }
    
    private static Case createCase(){
        
        Case c = new Case(
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRC').getRecordTypeId(),
            Status = 'New',
            Due_Date__c = Date.today().addMonths(1),
            Subject = 'CRC',
            Study_Type__c = 'Performance',
            Type = 'TBC - To be classified',
            Phase_c__c = 'Dispatch',
            Status__c = 'Dispatch.',
            Next_Action_required_details__c = 'Required Details',
            Next_Action_Required_Date_and_Time__c = System.now(),
            Requested_By__c = UserInfo.getUserId(),
            Description = 'Customer Number:0000335353 * Customer Contact Mail: assign_account@test.com.br ='
        );
        
        return c;
    }
    
}