/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class 
* OpportunityLineItemEntFeeUpdateNrec
*
* NAME: OpportunityLineItemEntFeeUpdateNrecTest.cls
* AUTHOR: AFC                                                  DATE: 25/03/2015
*******************************************************************************/
@isTest
public with sharing class OpportunityLineItemEntFeeUpdateNrecTest 
{
	private static final integer LOTE = 200;
	
	private static id accRecTypeId = RecordTypeMemory.getRecType( 'Account', 'Aircraft_OEM' );
  private static id oppRecTypeId = RecordTypeMemory.getRecType( 'Opportunity', 'Aircraft_Modification' );
    	
	static testMethod void myUnitTest()
  {    
    Account acc = SObjectInstanceTest.createAccount( accRecTypeId );
    database.insert( acc );
    
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
	  pbAirMod.Name = 'Aircraft Modification';
	  pbAirMod.Type__c = 'Aircraft Modification / Technical Services';
    database.insert( pbAirMod );
    
    Opportunity opp = SObjectInstanceTest.createOpportunity();
    opp.RecordTypeId = oppRecTypeId;
    opp.Pricebook2Id = pbAirMod.Id;
    database.insert( opp );
        
    Product2 prod = SObjectInstanceTest.createProduct2();
	  prod.Unit__c = 'Aricraft';
	  prod.Product_Type__c = 'Aircraft Modification';
	  prod.Family = 'Aircraft Modification';
	  prod.Applicability__c = 'Turboprop';
    database.insert( prod );
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    database.insert( pbe );
    
    PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    database.insert( pbeAirMod );
      
    list< OpportunityLineItem > lstOppItem = new list< OpportunityLineItem >();
    
    OpportunityLineItem oppItemEntryFee = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
    oppItemEntryFee.Princing__c = 'Entry fee';
    lstOppItem.add( oppItemEntryFee );
    
    OpportunityLineItem oppItemNrec = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id);      
	  oppItemNrec.Princing__c = 'NREC';
	  oppItemNrec.Entry_fee__c = 200;
    lstOppItem.add( oppItemNrec );
    
    database.insert( lstOppItem ); 

    test.startTest();
    database.delete( oppItemEntryFee );    
    test.stopTest();
    
    list< OpportunityLineItem > lstOppItemNrecUp = new list< OpportunityLineItem >( 
      [ SELECT id, Entry_fee__c FROM OpportunityLineItem WHERE id =: oppItemNrec.Id ] );    
    
    for( OpportunityLineItem OppItemNrecUp : lstOppItemNrecUp )
      system.assertEquals(0, OppItemNrecUp.Entry_fee__c );
  }
  
  static testMethod void testLOTE()
  { 
    Account acc = SObjectInstanceTest.createAccount( accRecTypeId );
    database.insert( acc );
    
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
	  pbAirMod.Name = 'Aircraft Modification';
	  pbAirMod.Type__c = 'Aircraft Modification / Technical Services';
    database.insert( pbAirMod );
    
	  Opportunity opp = SObjectInstanceTest.createOpportunity();
	  opp.RecordTypeId = oppRecTypeId;
	  opp.Pricebook2Id = pbAirMod.Id;
	  database.insert( opp );
     
    Product2 prod = SObjectInstanceTest.createProduct2();
	  prod.Unit__c = 'Aricraft';
	  prod.Product_Type__c = 'Aircraft Modification';
	  prod.Family = 'Aircraft Modification';
	  prod.Applicability__c = 'Turboprop';
    database.insert( prod );
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    database.insert( pbe );
    
    PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    database.insert( pbeAirMod );
      
    list< OpportunityLineItem > lstOppItem = new list< OpportunityLineItem >();
    list< OpportunityLineItem > lstLineItemDelete = new list< OpportunityLineItem >();    
    for(integer i = 0; i < LOTE; i++)
    {
      OpportunityLineItem oppItemEntryFee = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id );
      oppItemEntryFee.Princing__c = 'Entry fee';
      lstOppItem.add( oppItemEntryFee );
      lstLineItemDelete.add( oppItemEntryFee );
      
      OpportunityLineItem oppItemNrec = SObjectInstanceTest.createOppItem( opp.Id, pbeAirMod.Id );
	    oppItemNrec.Princing__c = 'NREC';
	    oppItemNrec.Entry_fee__c = 200;
      lstOppItem.add( oppItemNrec );
    }    
    database.insert( lstOppItem );
    
    test.startTest();
    database.delete( lstLineItemDelete );    
    test.stopTest();
    
    list< OpportunityLineItem > lstOppItemNrecUp = new list< OpportunityLineItem >( [ SELECT id, Entry_fee__c FROM OpportunityLineItem WHERE Princing__c =: 'NREC' ] );   
    
    for( OpportunityLineItem OppItemNrecUp : lstOppItemNrecUp )
      system.assertEquals(0, OppItemNrecUp.Entry_fee__c );
  }
}