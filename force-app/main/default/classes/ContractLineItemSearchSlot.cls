/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* When a contract line item is created or updated this class searchs for available
* slots. If there is an available slot, the class will update the available quantity of
* the slot, acconding to the quantity of the contract line item and will update the fields
* Slot__c, Slot_Image__c and Slot_status__c of the contract line item with the success
* information. If there is no available slot, the class will update the fields Slot_Image__c
* and Slot_status__c with the error information.
*
* NAME: ContractLineItemSearchSlot.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*
*******************************************************************************/

public with sharing class ContractLineItemSearchSlot
{
  public static void newProducts()
  {
    LineItemSearchSlot.newProducts();
  }
}