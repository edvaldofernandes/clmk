/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class JSCloneServiceContract.
*
* NAME: JSCloneServiceContractTest.cls
* AUTHOR: LMdO                                                DATE: 21/12/2014
*
*******************************************************************************/

@isTest(seealldata=true)
private class JSCloneServiceContractTest {
    
    private static final Id recAccount = RecordTypeMemory.getRecType( 'Account', 'Operator' );
    private static final Id recService = RecordTypeMemory.getRecType( 'ServiceContract', 'Training' );
    
    static testMethod void Positivo() {
        
        Product2 produto = SObjectInstanceTest.createProduct2();
      Database.insert(produto);
      
      Pricebook2 pb2 = SObjectInstanceTest.createPricebook2();
      pb2.Name = 'pricebook teste 1';
      pb2.Account_Type__c = 'Operator';
      Database.insert(pb2);
      
      PricebookEntry stdPbe = SObjectInstanceTest.createPricebookEntry(SObjectInstanceTest.catalogoDePrecoPadrao(), produto.Id);
      stdPbe.RECPrice__c = 5000;
      Database.insert(stdPbe);
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2.Id, produto.Id);
      pbe.RECPrice__c = 5000;
      Database.insert(pbe);
        
        Account conta = SObjectInstanceTest.createAccount(recAccount);
        Database.insert(conta);
        
        Contact contato = SObjectInstanceTest.createContact(conta.Id);
        Database.insert(contato);
        
   /*     Opportunity opp = SObjectInstanceTest.createOpportunity();
        opp.AccountId = conta.Id;
        opp.Pricebook2Id = pb2.Id;
        Database.insert(opp);
     */   
        MFIR__c mfir = new MFIR__c();
        mfir.Name = 'Teste mfir';
        mfir.MFIR_number__c = '1234567890';
        mfir.Account__c = conta.Id;
        Database.insert(mfir);
        
        ServiceContract service = SObjectInstanceTest.createServiceContract(conta.Id, recService);
      service.Pricebook2Id = pb2.Id;
      service.Contract_Status__c = 'Draft';
      service.ApprovalStatus = 'Approved';
      service.Comments_for_Signature__c = 'Teste';
   //   service.Signature_Date__c = Date.today();
      service.StartDate = Date.today();
      service.EndDate = Date.today();
      service.Payment_Terms__c = 'Teste!!!!!';
      service.SpecialTerms = 'Tesst';
      service.ContactId = contato.Id;
      service.Submission_Date__c = Date.today();
      service.Validity_Date__c = Date.today();
      service.MFIR__c = mfir.Id;
      Database.insert(service);
      
      ServiceContract serviceHierarchy = SObjectInstanceTest.createServiceContract(conta.Id, recService);
      serviceHierarchy.Name = 'Teste Hierarchy';
      serviceHierarchy.Contract_hierarchy__c = service.Id;
      Database.insert(serviceHierarchy);
      
      Service_Contract_Management__c serviceManagement = new Service_Contract_Management__c();
      serviceManagement.Account__c = conta.Id;
      serviceManagement.Service_Contract__c = service.Id;
      Database.insert(serviceManagement);
      
      Aircraft__c aircraft = SObjectInstanceTest.createAircraft();
      Database.insert(aircraft);
      
      Related_Aircraft__c relatAircraft = SObjectInstanceTest.createRelatedAirCraft(aircraft.Id);
      relatAircraft.Service_Contract__c = service.Id;
      Database.insert(relatAircraft);
  /*    
      OpportunityLineItem item = SObjectInstanceTest.createOppItem(opp.Id, pbe.Id);
      Database.insert(item);
      
      Quote quote = SObjectInstanceTest.createQuote(opp.Id);
      Database.insert(quote);
        
        OpportunityContactRole role = new OpportunityContactRole();
      role.OpportunityId = opp.Id;
      role.ContactId = contato.Id;
      role.Role = 'Other';
      Database.insert(role);
    */  
      ContractLineItem contractItem = SObjectInstanceTest.createContractLineItem(service.Id, pbe.Id);
      Database.insert(contractItem);
  /*      
        opp.StageName = 'Qualification';
        Database.update(opp);
        
        opp.StageName = 'Proposal/Price Quote';
      Database.update(opp);
      
      Quote cotacao = SObjectInstanceTest.createQuote(opp.Id);
      cotacao.ExpirationDate = system.today().addDays(5);
      cotacao.Status = 'Accepted';
      database.insert(cotacao);
      
      opp.SyncedQuoteId = cotacao.Id;
      Database.update(opp);
        
      OpportunityLineItem oli = SObjectInstanceTest.createOppItem(opp.Id, pbe.Id);
      Database.insert(oli);
        
        opp.StageName = 'Contract Negotiation & Review';
      Database.update(opp);
        
        opp.StageName = 'Closed Won';
        opp.Reason_for_Acceptance__c = 'Price';
      Database.update(opp);
      */
      
      service.Contract_Status__c = 'Internal approval';
      Database.update(service);
      
      service.Contract_Status__c = 'Submited to customer';
      Database.update(service);
      
      service.Signature_Date__c = Date.today();
      service.Contract_Status__c = 'Signed';
      service.Reason_of_Signature__c = 'Price';
      Database.update(service);
      
      String idRetorno;
      
      Test.startTest();
      idRetorno = JSCloneServiceContract.processar(String.valueOf(service.Id), false);
      Test.stopTest();
      
      List<string> lstServiceContract = new List<string>{ idRetorno, service.Id, serviceHierarchy.Id };
      
      List<ServiceContract> lstResult = [Select Id from ServiceContract where Id =: lstServiceContract];
      List<ContractLineItem> lstResultContract = [Select Id from ContractLineItem where ServiceContractId =: idRetorno];
      List<Service_Contract_Management__c> lstResultManag = [Select Id from Service_Contract_Management__c where Service_Contract__c =: idRetorno];
      List<Related_Aircraft__c> lstResultAircraft = [Select Id from Related_Aircraft__c where Service_Contract__c =: idRetorno];
      
      System.assert(lstResult.size() == 3, 'Não foi feito o clone do registro.');
      System.assert(lstResultContract != Null && !lstResultContract.isEmpty(), 'Não existe filhos Contract para esse clone.');
      System.assert(lstResultManag != Null && !lstResultManag.isEmpty(), 'Não existe filhos Manag para esse clone.');
      System.assert(lstResultAircraft != Null && !lstResultAircraft.isEmpty(), 'Não existe filhos Aircraft para esse clone.');
    }
    
    static testMethod void Negativo() {
        String erroNull;
        String erroVazio;
        
        Test.startTest();
      erroNull = JSCloneServiceContract.processar(Null, false);
      
      ServiceContract service = SObjectInstanceTest.createServiceContract();
      Database.insert(service);
      
      erroVazio = JSCloneServiceContract.processar('810e0000000EpdTAAS', false);
      Test.stopTest();
      
      System.assert(erroNull == 'Service Contract ID to clone informed. Not performed cloning.', 'Não ocorreu o erro Null esperado.');
      System.assert(erroVazio == 'Internal error. Service Contract not found. Contact your system administrator.', 'Não ocorreu o erro Vazio esperado.');  
    }
    
}