public without sharing class QuoteTriggerHandler
{

    
    public Map<Id,Quote> newRecordsMap = new Map<Id,Quote>();
    public Map<Id,Quote> oldRecordsMap = new Map<Id,Quote>(); 
    public List<Quote> newRecords = new List<Quote>();
    public List<Quote> oldRecords = new List<Quote>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public QuoteTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        //populateRFPDate();
        //updateOppStatus();

    }

 

    public void OnAfterInsert()
    {

        // EXECUTE AFTER INSERT LOGIC

    }

 

    public void OnBeforeUpdate()
    {

        validateeSolutionUser();
       
        updateStatusTrack();
        

    }

 

    public void OnAfterUpdate()
    {
        sendApprovalMessage();
    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }


	private void updateStatusTrack()
	{
		Set<Id> idsQuotes = new Set<Id>();
		string historyEntry = '';
		
		for(Quote qte : this.newRecords)
        {
        	if(qte.Status != this.oldRecordsMap.get(qte.Id).Status)
        	{
        		historyEntry = '';
        		historyEntry = dateTime.now().format() + '    ' + UserInfo.getFirstName() + ' '  + UserInfo.getLastName() + '    ' + 'Changed status from ' + this.oldRecordsMap.get(qte.Id).Status + ' to ' + qte.Status ;
        		
        		if(string.isEmpty(qte.StatusFieldHistory__c))
        		{
        			qte.StatusFieldHistory__c = '' + historyEntry;
        		}
        		else
        		{
        			qte.StatusFieldHistory__c += '\r\n' + historyEntry;	
        		}
        			
        		  	
        	}	
        	
        }
	
	}

	
	/*
	private void updateOppStatus()
	{
		Set<Id> oppIds = new Set<Id>();
		List<Opportunity> oppToUpdate = new List<Opportunity>();
		
		for(Quote qte : this.newRecords)
        {
        	if(qte.OpportunityId != Null)
        		oppIds.add(qte.OpportunityId);
        }
        
        
        if(!oppIds.isEmpty())
        {
        	for(Id idOpp : oppIds)
        		oppToUpdate.add(new Opportunity(Id = idOpp,StageName = 'Contract Negotiation & Review'));
        	
        	if(!oppToUpdate.isEmpty())
        		update oppToUpdate;
        			
        }	
        
	}
	*/
	
    private void validateeSolutionUser()
    {
    
        Id recordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        Set<Id> idUsersEsolutionsGroup = utils.getUserIdsFromGroup('eSolutions_ADMIN');
        Set<Id> idQuotesWitheSolutionsProducts = new Set<Id>(); 
        string product = '';
    
        for(Quote qte : this.newRecords)
        {
            if(qte.recordTypeId == recordTypeId && (qte.esolutions__c > this.oldRecordsMap.get(qte.Id).esolutions__c))
            {
                qte.ItemInsertedbyeSolutionsUser__c = idUsersEsolutionsGroup.contains(UserInfo.getUserId());
                idQuotesWitheSolutionsProducts.add(qte.Id);
            }
        }
        
        if(!idQuotesWitheSolutionsProducts.IsEmpty())
        {
            for(Quote qte : [Select Id, (Select Product_pdf__c From QuoteLineItems Where Product_type_updated__c = 'eSolutions') From Quote Where Id in : idQuotesWitheSolutionsProducts])
            {
                product = '';
                for(QuoteLineItem lineItem : qte.QuoteLineItems)
                {
                    if(lineItem.Product_pdf__c != Null)
                    {
                        if(!product.contains(lineItem.Product_pdf__c))
                        {
                            if(product != '')
                                product += '\n';
                                
                            product += lineItem.Product_pdf__c;
                            this.newRecordsMap.get(qte.Id).eSolutionsProducts__c = product;
                        }
                    }
                }
            }     
        } 
        
    }

    private void sendApprovalMessage()
    {
        List<String> listaCorpo = new List<String>();
        Map<Id,String> mapaIdsObjetoNome = new Map<Id,String>();
        Map<Id,String> mapaIdsAccount  = new Map<Id,String>();  
        List<Id> listaIdsUsuarios = new List<Id>();
        List<String> listaAssunto = new List<String>();
        Set<Id> idQuotes = new Set<Id>();
        
        for(Quote cotacao : this.newRecords)
        {
            if(cotacao.Approval_status__c != this.oldRecordsMap.get(cotacao.Id).Approval_status__c && (cotacao.Approval_status__c == 'Approved' || cotacao.Approval_status__c == 'Rejected'))
            	idQuotes.add(cotacao.Id);
                
        }
        if(!idQuotes.isEmpty())
        {
        	for(Quote cotacao : [Select Id,Name,CreatedById, Account.Name From Quote Where Id in : idQuotes] )
        	{
        		mapaIdsAccount.put(cotacao.Id,cotacao.Account.Name);
        		mapaIdsObjetoNome.put(cotacao.Id,cotacao.Name);
        		listaIdsUsuarios.add(cotacao.CreatedById);
                listaAssunto.add('Your Approval request was concluded');
        	}            
            listaCorpo = utils.comporTextoEmailAprovacao(mapaIdsObjetoNome,mapaIdsAccount);
            utils.enviarEmail(listaIdsUsuarios, listaCorpo, listaAssunto,true);
        }     
    
    }
   
}