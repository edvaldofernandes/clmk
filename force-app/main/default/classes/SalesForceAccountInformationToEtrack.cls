public class SalesForceAccountInformationToEtrack {
    
    private List<AccountInformation> accountList;
    private List<AccountHistoryInformation> accountHistoryInformationList;
    
    public SalesForceAccountInformationToEtrack(){
        
        initCommonObejcts();
    }
    
    private void initCommonObejcts(){
        
        accountList = new  List<AccountInformation>();
        accountHistoryInformationList = new List<AccountHistoryInformation>();
    }
    
    public void informationAfterInsertOrUpdate(Account[] account){
        
        Map<Id, User> userMap = TriggerService.findUserByCreatedById(account[0].id);
        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(account[0].id)){
            
            User user = userMap.get(accountHistory.CreatedById);
            
            String namePhoneAndEmail = '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';

            accountHistoryInformationList.add(new AccountHistoryInformation(accountHistory.CreatedDate, 
                                                                            accountHistory.Field,
                                                                            String.valueOf(accountHistory.OldValue),
                                                                            String.valueOf(accountHistory.NewValue),
                                                                            namePhoneAndEmail));
        }
        
        informationHistoryAfetrInsertOrUpdate(account);
        saveAndFireCallout();
    }
    
    public void informationAfterUpdate(Account[] account){
        
        Map<Id, User> userMap = TriggerService.findUserByCreatedById(account[0].id);
        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(account[0].id)){
            
            User user = userMap.get(accountHistory.CreatedById);
            
            String namePhoneAndEmail = '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';

            accountHistoryInformationList.add(new AccountHistoryInformation(accountHistory.CreatedDate, 
                                                                            accountHistory.Field,
                                                                            String.valueOf(accountHistory.OldValue),
                                                                            String.valueOf(accountHistory.NewValue),
                                                                            namePhoneAndEmail));
        }
        
        informationHistoryAfterUpdate(account);
        saveAndFireCallout();
    }
    
    public void informationAfterDelete(Account[] account){
        
        Map<Id, User> userMap = TriggerService.findUserByCreatedById(account[0].id);
        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(account[0].id)){
            
            User user = userMap.get(accountHistory.CreatedById);
            
            String namePhoneAndEmail = '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';
            
            accountHistoryInformationList.add(new AccountHistoryInformation(accountHistory.CreatedDate, 
                                                                            accountHistory.Field,
                                                                            String.valueOf(accountHistory.OldValue),
                                                                            String.valueOf(accountHistory.NewValue),
                                                                            namePhoneAndEmail));
        }
        
        informationHistoryAfterDelete(account);
        saveAndFireCallout();
    }
    
    private void informationHistoryAfterDelete(Account[] account){
        
        for(Account accountDelete : account){
            accountList.add(new AccountInformation(accountDelete, Constants.OLD_OBJECT_UPDATED_OR_DELETED, Constants.STATUS_DELETED));
        }
    }
    
    private void informationHistoryAfterUpdate(Account[] account){
        
        for(Account accountOld : account){
            accountList.add(new AccountInformation(accountOld, Constants.OLD_OBJECT_UPDATED_OR_DELETED, Constants.STATUS_ACTIVE));
        }    
    }
    
    private void informationHistoryAfetrInsertOrUpdate(Account[] account){
        
        for(Account accountNew : account)
            accountList.add(new AccountInformation(accountNew, Constants.NEW_OBJECT_CREATED, Constants.STATUS_ACTIVE));   
       
    }
    
    public void saveAndFireCallout(){
        
        Visitator visitator = new CallOutRepository();
        visitator.visit(accountList, accountHistoryInformationList, String.valueOf(AccountInformation.class));
        
        if(Test.isRunningTest()) return;
        
        RcpOutBoundProxy.executeAccount();    
        SalesForceToEtrackProxy.executeAccount();
    }
    
    
}