global with sharing class CLMChecklistTask{
    webService static String createTask(String checkListId){
        String checkList = checkListId;
        system.debug('checkListID CREATE>>> ' +checkList);
        
        List<Task> lstTask = new List<Task>();
        //Map<String, Task_Setup__c> mapTaskSetup = new Map<String, Task_Setup__c>();
        
        SObjectType questionnaireType = Schema.getGlobalDescribe().get('Questionnaire__c');
        Map<String,Schema.SObjectField> mapFieldsSchema = questionnaireType.getDescribe().fields.getMap();
        Map<String,String> mapQuestionnaireType = new Map<String,String>();
        
        Questionnaire__c questionnaireSelected = new Questionnaire__c();
        questionnaireSelected = [ Select Commercial_Agreement__c , Contract_Aircraft__c , Delivery_Date__c , RecordTypeId, RecordType.Name ,
                                 AFA__c , Contract_Signature_Date__c , Delivery_Month__c , Assign_Task_to__c, Factory_Tour__c   
                                 From 
                                 Questionnaire__c 
                                 Where 
                                 Id = :checkList];
        String returnValidate = validateFields(questionnaireSelected);
        if(String.isNotBlank(returnValidate)){
            return returnValidate;
        }
       
        
        List<Task_Setup__c> lstTaskSetup = TaskSetupDAO.getInstance().getTaskSetup();
        
        for(Task_Setup__c taskSetup : lstTaskSetup){          
            if(taskSetup.Question_API__c == 'All' && taskSetup.RecordType.Name == questionnaireSelected.RecordType.Name){
               }
        }
        Task_Setup__c sObjectTaskSetup = TaskSetupDAO.getInstance().getSObjectTaskSetup('Deleted_Fields');  
        String JSONString = JSON.serialize(questionnaireSelected);
        JSONParser parser = JSON.createParser(JSONString);
       
        while (parser.nextToken() != null){
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                String keyMap = parser.getText();
                parser.nextToken();
                mapQuestionnaireType.put(keyMap, parser.getText());
            }
        }
        if (sObjectTaskSetup != null){
            for(String sObjectFields : sObjectTaskSetup.Deleted_Fields__c.split(';')){
                mapQuestionnaireType.remove(sObjectFields);
            }       
        }
        
        system.debug('mapQuestionnaireType>>> ' +mapQuestionnaireType);
        system.debug('lstTaskSetup>>> ' +lstTaskSetup);
        
        
        for(Task_Setup__c taskSetup : lstTaskSetup){
            Task sObjctTask = new Task();
            if(taskSetup.Question_API__c == 'All' && taskSetup.RecordType.Name == questionnaireSelected.RecordType.Name){
                   if(questionnaireSelected.Commercial_Agreement__c != null) {
                       sObjctTask.WhatId = questionnaireSelected.Commercial_Agreement__c;
                   } else if(questionnaireSelected.Contract_Aircraft__c != null) {
                       sObjctTask.WhatId = questionnaireSelected.Contract_Aircraft__c;
                   }
                   
                   if(taskSetup.Selected_Date__c == 'Delivery Date'){
                       sObjctTask.ActivityDate = questionnaireSelected.Delivery_Date__c;
                   } 
                   else if(taskSetup.Selected_Date__c == 'AFA'){
                       sObjctTask.ActivityDate =    questionnaireSelected.AFA__c;
                   }
                   else if(taskSetup.Selected_Date__c == 'Contract Signature Date'){
                       sObjctTask.ActivityDate = questionnaireSelected.Contract_Signature_Date__c;  
                   }
                   else if(taskSetup.Selected_Date__c == 'Delivery Month'){
                       sObjctTask.ActivityDate = questionnaireSelected.Delivery_Month__c;
                   }
                   
                   if(taskSetup.Before_Selected_date__c == true){
                       if(sObjctTask.ActivityDate != null)
                           sObjctTask.ActivityDate = sObjctTask.ActivityDate - Integer.valueOf(taskSetup.Leap_Time__c);
                   }
                   else if (taskSetup.Before_Selected_date__c == false){
                       
                       if(sObjctTask.ActivityDate != null)
                           sObjctTask.ActivityDate = sObjctTask.ActivityDate + Integer.valueOf(taskSetup.Leap_Time__c);
                   }

                   
                   sObjctTask.Status = 'Open'; 
                   sObjctTask.Subject   = taskSetup.Description__c;
                   sObjctTask.Checklist__c = questionnaireSelected.Id;
                   
                   if(questionnaireSelected.Assign_Task_to__c != null)
                       sObjctTask.OwnerId = questionnaireSelected.Assign_Task_to__c; //questionnaire.OwnerId;
                   
                   sObjctTask.Description   = taskSetup.Description__c;
                   sObjctTask.Task_Setup_Code__c = String.valueOf(taskSetup.Sequence__c); 
                   
                   lstTask.add(sObjctTask);                     
               }
        }
        
        for(Schema.SObjectField sObjSchema : mapFieldsSchema.values()){
            schema.describefieldresult stringApiName = sObjSchema.getDescribe();
            String sObjField  = mapQuestionnaireType.get(stringApiName.getName());
            
            if(sObjField != null){           
                for(Task_Setup__c taskSetup : lstTaskSetup){
                    Task sObjctTask = new Task();
                    
                    if(stringApiName.getName()  == taskSetup.Question_API__c 
                       && sObjField == taskSetup.Answer__c
                       && taskSetup.RecordType.Name == questionnaireSelected.RecordType.Name)
                    {       
                        if(questionnaireSelected.Commercial_Agreement__c != null){
                            sObjctTask.WhatId = questionnaireSelected.Commercial_Agreement__c;
                        } else if(questionnaireSelected.Contract_Aircraft__c != null){
                            sObjctTask.WhatId = questionnaireSelected.Contract_Aircraft__c;
                        }   
                        
                        if(taskSetup.Selected_Date__c == 'Delivery Date'){
                            sObjctTask.ActivityDate = questionnaireSelected.Delivery_Date__c;
                        } 
                        else if(taskSetup.Selected_Date__c == 'AFA'){
                            sObjctTask.ActivityDate =   questionnaireSelected.AFA__c;
                        }
                        else if(taskSetup.Selected_Date__c == 'Contract Signature Date'){
                            sObjctTask.ActivityDate = questionnaireSelected.Contract_Signature_Date__c; 
                        }
                        else if(taskSetup.Selected_Date__c == 'Delivery Month'){
                            sObjctTask.ActivityDate = questionnaireSelected.Delivery_Month__c;
                        }
                        
                        if(taskSetup.Before_Selected_date__c == true){
                            if(sObjctTask.ActivityDate != null)
                                sObjctTask.ActivityDate = sObjctTask.ActivityDate - Integer.valueOf(taskSetup.Leap_Time__c);
                        }
                        else if (taskSetup.Before_Selected_date__c == false){
                            if(sObjctTask.ActivityDate != null)
                                sObjctTask.ActivityDate = sObjctTask.ActivityDate + Integer.valueOf(taskSetup.Leap_Time__c);
                        }                           
                        
                        sObjctTask.Status = 'Open'; 
                        sObjctTask.Subject  = taskSetup.Description__c; //'Checklist Activities'; 
                        
                        if(questionnaireSelected.Assign_Task_to__c != null)
                            sObjctTask.OwnerId = questionnaireSelected.Assign_Task_to__c;//questionnaire.OwnerId; 
                        
                        sObjctTask.Description  = taskSetup.Description__c; 
                        sObjctTask.Task_Setup_Code__c = String.valueOf(taskSetup.Sequence__c);
                        sObjctTask.Checklist__c = questionnaireSelected.Id;
                        
                        lstTask.add(sObjctTask);
                    }
                }
            }
        }
        List<Database.SaveResult> res = Database.insert(lstTask);
        
        return 'success';
        //System.debug('>>>>> lstTask'+lstTask);
    }
    
    webService static void deleteTask(String checkListId){
        String checkList = checkListId;
        list<Task> lstOpenTask = new list<Task>();
        lstOpenTask = [SELECT Id, Status, Checklist__c FROM Task where Status = 'Open' AND Checklist__c =:checkList];
        if(!lstOpenTask.isEmpty()){
            database.delete(lstOpenTask);
        }
    }
    
    private static String validateFields (Questionnaire__c checkListSelected){
        system.debug('checkListSelected.RecordTypeId>>> ' +checkListSelected.RecordType.Name);
        List<Task_Setup__c> lstTaskSetup = TaskSetupDAO.getInstance().getTaskSetupByRecordType(checkListSelected.RecordType.Name);
        Set<String> setAgreementValidation = new Set<String>();
        Set<String> setAgreementLineItemValidation = new Set<String>();
        
        

        for(Task_Setup__c setValidation :lstTaskSetup){
            if(setValidation.Date_API__c != null){
                if(setValidation.Date_API__c.contains('Agreement__c')){
                    setAgreementValidation.add(setValidation.Date_API__c.substringAfter('.'));
                }else if(setValidation.Date_API__c.contains('Agreement_Aircraft__c')){
                    setAgreementLineItemValidation.add(setValidation.Date_API__c.substringAfter('.'));
                }
            }
        }
         
        system.debug('setAgreementValidation>>>' +setAgreementValidation);     
        system.debug('setAgreementLineItemValidation>>> ' +setAgreementLineItemValidation);
        
        Agreement__c agreement;
        Agreement_Aircraft__c agreementAircraft;
        Map<String,String> mapAgreementType = new Map<String,String>();
        Map<String,String> mapAgreementLineItemType = new Map<String,String>();
        String errorValidation;

        if(checkListSelected.Commercial_Agreement__c != null){
            String querySoql =  'SELECT ' + utils.getAllFields('Agreement__c') +
                            ' FROM Agreement__c' +
                            ' WHERE id = \'' +checkListSelected.Commercial_Agreement__c+ '\'';
            system.debug('querySoql: ' +querySoql);
            agreement = database.query(querySoql);
            
            String JSONString = JSON.serialize(agreement);
            system.debug('VALIDATION JSONString setAgreementLineItemValidation>>> ' +JSONString);
            JSONParser parser = JSON.createParser(JSONString);
            while (parser.nextToken() != null){
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String keyMap = parser.getText();
                    parser.nextToken();
                    mapAgreementType.put(keyMap, parser.getText());
                }
            }

                for(String str :setAgreementValidation){
                    
                    if(mapAgreementType.containsKey(str)){
                        system.debug('mapAgreementType' +mapAgreementType.get(str));
                        if(String.isBlank(mapAgreementType.get(str))){
                            errorValidation = 'O campo '+str+' no Acordo (ou Aircraft contrato) registro precisa ser preenchido para permitir a criação de tarefas.';
                        }
                    } else{
                       errorValidation = 'O campo '+str+' no Acordo (ou Aircraft contrato) registro precisa ser preenchido para permitir a criação de tarefas.';
                    }
                }

            
        } else {
            String querySoqlAgreementLineItem =  'SELECT ' + utils.getAllFields('Agreement_Aircraft__c') +
            //String querySoqlAgreementLineItem =  'SELECT AFA__c, Estimated_Closing_Date__c, Apttus__AgreementId__c'  +
                            ' FROM Agreement_Aircraft__c' +
                            ' WHERE id = \'' +checkListSelected.Contract_Aircraft__c+ '\'';
            agreementAircraft = database.query(querySoqlAgreementLineItem);
            
            String querySoqlAgreement =  'SELECT ' + utils.getAllFields('Agreement__c') +
                            ' FROM Agreement__c' +
                            ' WHERE id = \'' +agreementAircraft.Agreement__c+ '\'';
            agreement = database.query(querySoqlAgreement);
            
            system.debug('querySoqlAgreementLineItem select>>> ' +querySoqlAgreementLineItem);
            system.debug('agreementAircraft select>>> ' +agreementAircraft);
            
            String JSONStringLineItem = JSON.serialize(agreementAircraft);
            JSONParser parser = JSON.createParser(JSONStringLineItem);
            while (parser.nextToken() != null){
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String keyMap = parser.getText();
                    parser.nextToken();
                    mapAgreementLineItemType.put(keyMap, parser.getText());
                }
            }
            
            String JSONStringAgreement = JSON.serialize(agreement);
            JSONParser parserAgreement = JSON.createParser(JSONStringAgreement);
            while (parserAgreement.nextToken() != null){
                if (parserAgreement.getCurrentToken() == JSONToken.FIELD_NAME){
                    String keyMap = parserAgreement.getText();
                    parserAgreement.nextToken();
                    mapAgreementType.put(keyMap, parserAgreement.getText());
                }
            }
            
            for(String str :setAgreementLineItemValidation){
                    system.debug('STR For LineItem>>> ' +str);
                    if(mapAgreementLineItemType.containsKey(str)){
                        system.debug('mapAgreementType LineItem ' +mapAgreementLineItemType.get(str));
                        if(String.isBlank(mapAgreementLineItemType.get(str))){
                            errorValidation = 'O campo '+str+' no Acordo (ou Aircraft contrato) registro precisa ser preenchido para permitir a criação de tarefas.';
                        }
                    }else{
                       errorValidation = 'O campo '+str+' no Acordo (ou Aircraft contrato) registro precisa ser preenchido para permitir a criação de tarefas.';
                    }
                }
            
            for(String str :setAgreementValidation){
                    system.debug('STR For Agreement>>> ' +str);
                    if(mapAgreementType.containsKey(str)){
                        system.debug('mapAgreementType Agreement ' +mapAgreementType.get(str));
                        if(String.isBlank(mapAgreementType.get(str))){
                            errorValidation = 'O campo '+str+' no Acordo (ou Aircraft contrato) registro precisa ser preenchido para permitir a criação de tarefas.';
                        }
                    }else{
                       errorValidation = 'O campo '+str+' no Acordo (ou Aircraft contrato) registro precisa ser preenchido para permitir a criação de tarefas.';
                    }
                }
          
        }
        
        system.debug('agreement>>> ' +agreement);
        system.debug('agreementAircraft>>> ' +agreementAircraft);
        system.debug('mapAgreementLineItemType FIM>>> ' +mapAgreementLineItemType);
        system.debug('mapAgreementType FIM >>> ' +mapAgreementType);
        system.debug('ERRO MESSAGE>>> ' +errorValidation);
        return errorValidation;
    }
}