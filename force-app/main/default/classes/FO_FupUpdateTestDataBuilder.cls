/**
* @description: class for create Follow-up updates test data.
**/
@isTest
public class FO_FupUpdateTestDataBuilder {
    private FUP__c fup = new FO_FupTestDataBuilder().build();
    private String status;
    private String name;
    private Date publishDate;
    
    public FO_FupUpdateTestDataBuilder withStatus(String status){
        this.status = status;
        return this;
    }  
    public FO_FupUpdateTestDataBuilder withParentFup(FUP__c fup){
        this.fup = fup;
        return this;
    }
    public FO_FupUpdateTestDataBuilder withPublishDate(Date publishDate){
        this.publishDate = publishDate;
        return this;
    }
    public FO_FupUpdateTestDataBuilder whichIsPublic(){
        this.status = 'Published';
        this.fup = new FO_FupTestDataBuilder()
            .whichIsPublic()
            .build();
        
		return this;
    }
    /**
    * @description: builds an follow-up using data from this class instance.
    **/
    public FUP_Update__c build(){
        FUP_Update__c fupUpdate = new FUP_Update__c(
            Status__c = status,
            Publish_Date__c = publishDate,
            FUP__c = fup.Id
        );
        insert fupUpdate;
        return fupUpdate;
    }
}