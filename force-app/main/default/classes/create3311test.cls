@isTest
public class create3311test {
    
    //Testing constructor case 1
    @isTest static void testcreate3311_m1_c1(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c1();
        
        Test.startTest();
        create3311 x = new create3311(sc);
        Test.stopTest();
        
        //assert 
        //there x.llp.name should be retrieved from db query, should equal PN1
        System.assertEquals('PN1', x.llp.Name);
    }
    
    //Testing constructor case 2
    @isTest static void testcreate3311_m1_c0(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c0();
        
        Test.startTest();
        create3311 x = new create3311(sc);
        Test.stopTest();
        
        //assert 
        //there x.llp.name should be retrieved from db query, should equal PN2
        System.assertEquals('PN2', x.llp.Name);
        System.assert(x.cityAndStateIsBlank);
    }
    
    //Testing constructor case 3
    @isTest static void testcreate3311_m0_c1(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m0_c1();
        
        Test.startTest();
        create3311 x = new create3311(sc);
        Test.stopTest();
        
        //assert 
        //there x.llp.name should be retrieved from db query, should equal PN3
        System.assertEquals('PN3', x.llp.Name);
        System.assert(x.manufacturerIsBlank);
    }
    
    //Testing constructor case 4
    @isTest static void testcreate3311_m0_c0(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m0_c0();
        
        Test.startTest();
        create3311 x = new create3311(sc);
        Test.stopTest();
        
        //assert 
        //there x.llp.name should be retrieved from db query, should equal PN4
        System.assertEquals('PN4', x.llp.Name);
        System.assert(x.manufacturerIsBlank && x.cityAndStateIsBlank);
    }
    
    //Testing GetXMLString
    @isTest static void testgetXmlString(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c1();
        string notif = 'notif';
        string serial = 'serial';
        decimal val = 100;
        string dec = 'dec';
        string prt = 'port';
        date prtDate = system.today();
        string comp = 'Company2';
        string madeIn = 'China';
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.value = val;
        x.declarant = dec;
        x.port = prt;
        x.portDate = prtDate;
        x.company = comp;
        x.madeIn = madeIn;
        
        //this is the function being tested
        string xmlString = x.getXmlString();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        String s = '<?xml version="1.0" encoding="UTF-8"?>' +
     		'<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' +
            '<f href="S:\\Material Services\\06 - OPERATIONAL SUPPORT\\Developments\\3311\\finalFormWithInvoice.pdf"/>' +
            '<fields>' +
         	'<field name="date"><value>'+system.today().format()+'</value></field>'+
            '<field name="nameOfManufacturer"><value>'+x.llp.manufacturer__c+'</value></field>'+
            '<field name="cityAndStateOfManufacture"><value>'+x.llp.city_state_of_manufacture__c+'</value></field>'+
            '<field name="reasonForReturn"><value>'+'EXCHANGE RETURN'+'</value></field>'+
            '<field name="unclaimed"><value>'+'X'+'</value></field>'+
            '<field name="no"><value>'+'X'+'</value></field>'+
            '<field name="marks1"><value>'+ x.llp.Name +'  '+ x.llp.Description__c +' (S/N: '+serial+')'+'</value></field>'+
            '<field name="marks2"><value>'+'NOTIF: '+ notif +'</value></field>'+
            '<field name="value"><value>'+'$'+ val.setScale(2) +'</value></field>'+
            '<field name="were"><value>'+'X'+'</value></field>'+
            '<field name="have"><value>'+'X'+'</value></field>'+
            '<field name="nameOfDeclarant"><value>'+ dec +'</value></field>'+
            '<field name="titleOfDeclarant"><value>'+'Regulatory Compliance'+'</value></field>'+
            '<field name="nameOfCorporation"><value>'+'EMBRAER AIRCRAFT CUSTOMER SERVICES, INC.'+'</value></field>'+
            '<field name="name"><value>'+ dec +'</value></field>'+
            '<field name="address"><value>'+ '276 SW 34th Street Fort Lauderdale, FL 33315' +'</value></field>'+
            '<field name="port2"><value>'+ prt +'</value></field>'+
            '<field name="date3"><value>'+ prtDate.format() +'</value></field>'+
            '<field name="marks3"><value>'+'Notification Number: '+ notif +'</value></field>'+
            '<field name="number"><value>'+'Part Number:\n'+ x.llp.Name +'\n\nSerial Number:\n'+serial+'</value></field>'+
            '<field name="quantity"><value>'+ '1' +'</value></field>'+
            '<field name="description"><value>'+ x.llp.Description__c +'</value></field>'+
            '<field name="address2"><value>'+ x.companyAddress.get(comp) +'</value></field>'+
            '<field name="company"><value>'+ comp +'</value></field>'+
            '<field name="notification"><value>'+ notif +'</value></field>'+
            '<field name="taxId"><value>'+'Company Registration No and GST No: '+ x.companyTaxID.get(comp) +'</value></field>'+
            '<field name="addressOfManufacture"><value>'+ x.llp.ManufacturerAddress__c +'</value></field>'+
            '<field name="htsCode"><value>'+ x.llp.Htsus__c +'</value></field>'+
            '<field name="partNumber"><value>'+ x.llp.Name +'</value></field>'+
            '<field name="serialNumber"><value>'+ serial +'</value></field>'+
            '<field name="um"><value>'+ 'EA' +'</value></field>'+
            '<field name="madeIn"><value>'+ madeIn +'</value></field>'+
            '<field name="agent"><value>'+ 'Material Services Agent' +'</value></field>'+
            '</fields>' +
            '</xfdf>';
        System.assertEquals(s, xmlString);
    }
    
    //Testing GetXMLString when creating inoice only
    @isTest static void testgetXmlStringInvoiceOnly(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c1();
        string notif = 'notif';
        string serial = 'serial';
        decimal val = 100;
        string dec = 'dec';
        string comp = 'Company2';
        string madeIn = 'China';
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.value = val;
        x.declarant = dec;
        x.company = comp;
        x.madeIn = madeIn;
        x.isInvoiceOnly = true; //invoice only must be true
        
        //this is the function being tested
        string xmlString = x.getXmlString();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        String s = '<?xml version="1.0" encoding="UTF-8"?>' +
     		'<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' +
            '<f href="S:\\Material Services\\06 - OPERATIONAL SUPPORT\\Developments\\3311\\InvoiceForm.pdf"/>' +
            '<fields>' +
         	'<field name="date"><value>'+system.today().format()+'</value></field>'+
            '<field name="nameOfManufacturer"><value>'+x.llp.manufacturer__c+'</value></field>'+
            '<field name="cityAndStateOfManufacture"><value>'+x.llp.city_state_of_manufacture__c+'</value></field>'+
            '<field name="reasonForReturn"><value>'+'EXCHANGE RETURN'+'</value></field>'+
            '<field name="unclaimed"><value>'+'X'+'</value></field>'+
            '<field name="no"><value>'+'X'+'</value></field>'+
            '<field name="marks1"><value>'+ x.llp.Name +'  '+ x.llp.Description__c +' (S/N: '+serial+')'+'</value></field>'+
            '<field name="marks2"><value>'+'NOTIF: '+ notif +'</value></field>'+
            '<field name="value"><value>'+'$'+ val.setScale(2) +'</value></field>'+
            '<field name="were"><value>'+'X'+'</value></field>'+
            '<field name="have"><value>'+'X'+'</value></field>'+
            '<field name="nameOfDeclarant"><value>'+ dec +'</value></field>'+
            '<field name="titleOfDeclarant"><value>'+'Regulatory Compliance'+'</value></field>'+
            '<field name="nameOfCorporation"><value>'+'EMBRAER AIRCRAFT CUSTOMER SERVICES, INC.'+'</value></field>'+
            '<field name="name"><value>'+ dec +'</value></field>'+
            '<field name="address"><value>'+ '276 SW 34th Street Fort Lauderdale, FL 33315' +'</value></field>'+
            '<field name="marks3"><value>'+'Notification Number: '+ notif +'</value></field>'+
            '<field name="number"><value>'+'Part Number:\n'+ x.llp.Name +'\n\nSerial Number:\n'+serial+'</value></field>'+
            '<field name="quantity"><value>'+ '1' +'</value></field>'+
            '<field name="description"><value>'+ x.llp.Description__c +'</value></field>'+
            '<field name="address2"><value>'+ x.companyAddress.get(comp) +'</value></field>'+
            '<field name="company"><value>'+ comp +'</value></field>'+
            '<field name="notification"><value>'+ notif +'</value></field>'+
            '<field name="taxId"><value>'+'Company Registration No and GST No: '+ x.companyTaxID.get(comp) +'</value></field>'+
            '<field name="addressOfManufacture"><value>'+ x.llp.ManufacturerAddress__c +'</value></field>'+
            '<field name="htsCode"><value>'+ x.llp.Htsus__c +'</value></field>'+
            '<field name="partNumber"><value>'+ x.llp.Name +'</value></field>'+
            '<field name="serialNumber"><value>'+ serial +'</value></field>'+
            '<field name="um"><value>'+ 'EA' +'</value></field>'+
            '<field name="madeIn"><value>'+ madeIn +'</value></field>'+
            '<field name="agent"><value>'+ 'Material Services Agent' +'</value></field>'+
            '</fields>' +
            '</xfdf>';
        System.assertEquals(s, xmlString);
    }
    
    //Testing GetXMLString when creating inoice only
    @isTest static void testToggleInvoiceOnly(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c1();
        boolean b = false;
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.isInvoiceOnly = b; //set invoice only to false
        
        //this is the function being tested
        x.toggleInvoiceOnly();
        Test.stopTest();
        
        b = !b;	//toggle our expected value 
        System.assertEquals(b, x.isInvoiceOnly);
    }
    
    //Testing XDPInit base case
    @isTest static void testXDPInit_m1_c1(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c1();
        string notif = 'notif';
        string serial = 'serial';
        decimal val = 100;
        string dec = 'dec';
        string prt = 'port';
        date prtDate = system.today();
        string comp = 'Company2';
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.value = val;
        x.declarant = dec;
        x.port = prt;
        x.portDate = prtDate;
        x.company = comp;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        System.assert(true);
    }
    
    //Testing XDPInit manufacturer is blank
    @isTest static void testXDPInit_m0_c1(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m0_c1();
        string notif = 'notif';
        string serial = 'serial';
        decimal val = 100;
        string dec = 'dec';
        string prt = 'port';
        date prtDate = system.today();
        string comp = 'Company2';
        string manu = 'manu';
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.value = val;
        x.declarant = dec;
        x.port = prt;
        x.portDate = prtDate;
        x.company = comp;
        x.manufacturer = manu;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        LLPDatabase__c part = [select manufacturer__c FROM LLPDatabase__c WHERE Name = 'PN3'];
        System.assertEquals(manu, part.manufacturer__c);
    }
    
    //Testing XDPInit city is blank
    @isTest static void testXDPInit_m1_c0(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c0();
        string notif = 'notif';
        string serial = 'serial';
        decimal val = 100;
        string dec = 'dec';
        string prt = 'port';
        date prtDate = system.today();
        string comp = 'Company2';
        string city = 'city';
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.value = val;
        x.declarant = dec;
        x.port = prt;
        x.portDate = prtDate;
        x.company = comp;
        x.cityState = city;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        //the string s should be equal to xmlString which is returned from the function being tested
        LLPDatabase__c part = [select city_state_of_manufacture__c FROM LLPDatabase__c WHERE Name = 'PN2'];
        System.assertEquals(city, part.city_state_of_manufacture__c);
    }
    
    //Testing XDPInit value
    @isTest static void testXDPInit_value(){
        //generate test data
        ApexPages.StandardController sc = testDataFactory3311.create3311data_m1_c1();
        string notif = 'notif';
        string serial = 'serial';
        decimal val = 100;
        string dec = 'dec';
        string prt = 'port';
        date prtDate = system.today();
        string comp = 'Company2';
        
        Test.startTest();
        //initialize create3311 class
        create3311 x = new create3311(sc);
        x.notification = notif;
        x.serialNumber = serial;
        x.value = val;
        x.declarant = dec;
        x.port = prt;
        x.portDate = prtDate;
        x.company = comp;
        
        //testing this function
        PageReference pr = x.XDPInit();
        Test.stopTest();
        
        //assert 
        val *= 0.84;		//calculate alue with 16% off
        val.setScale(2);	//round to two decimals
        system.assertEquals(val, x.value);	//value should be 16% off
    }
}