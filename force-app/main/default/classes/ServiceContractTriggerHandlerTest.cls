/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:">
* @version 1.0, &nbsp; May-2015.
**/
@isTest
public with sharing class ServiceContractTriggerHandlerTest 
{

    static testMethod void myUnitTest() 
    {
    
         
        Id stdPB = Test.getStandardPricebookId();
       
        Product2 prod = SObjectInstanceTest.createProduct2();
        insert prod;

        PriceBookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
        insert pbe;

        Account conta = SObjectInstanceTest.createAccount( Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert conta;
      
        ServiceContract contrato1 = SObjectInstanceTest.createServiceContract( conta.id, Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Training').getRecordTypeId() );
        contrato1.Pricebook2Id = stdPB;
        insert contrato1;
            
        delete contrato1;
        
        undelete contrato1;
      
        ServiceContractTriggerHandler handler = new ServiceContractTriggerHandler(true);
        System.AssertEquals(handler.IsTriggerContext,true);
        
      
        try
        {
            contrato1.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
            update contrato1;
        }
        catch(exception ex)
        {
            System.Assert(ex.getMessage().contains('Only the members in eSolutions public group can create eSolutions Service Contracts.'));
        }
        finally
        {
      
            try
            {
                ServiceContract contrato = SObjectInstanceTest.createServiceContract( conta.id, Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId() );
                contrato.Pricebook2Id = stdPB;
                insert contrato;
            }
            catch(exception ex)
            {
                System.Assert(ex.getMessage().contains('Only the members in eSolutions public group can create eSolutions Service Contracts.'));
            }
        }
        
    }
    
    static testMethod void unitTest() 
    {
    
         
        Id stdPB = Test.getStandardPricebookId();
       
        Product2 prod = SObjectInstanceTest.createProduct2();
        insert prod;

        PriceBookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
        insert pbe;

        Account conta = SObjectInstanceTest.createAccount( Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert conta;
      
        ServiceContract contrato1 = SObjectInstanceTest.createServiceContract( conta.id, Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Training').getRecordTypeId() );
        contrato1.Pricebook2Id = stdPB;
        
        insert contrato1;
        
        ContractLineItem oli = SObjectInstanceTest.createContractLineItem(contrato1.Id, pbe.Id);
        oli.Slot_status__c = 'Success';
        Database.insert(oli);
        
        
        contrato1.Contract_Status__c = 'Dropped';
        contrato1.Reason_of_Rejection__c = 'Price too high';
        contrato1.Comments_for_Rejection__c = 'teste';
        update contrato1;
            
        
    }
    
    static testMethod void scmUpdateTest() 
    {
    
         
        Id stdPB = Test.getStandardPricebookId();
       
        Product2 prod = SObjectInstanceTest.createProduct2();
        insert prod;

        PriceBookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
        insert pbe;

        Account conta = SObjectInstanceTest.createAccount( Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert conta;
      
        ServiceContract contrato1 = SObjectInstanceTest.createServiceContract( conta.id, Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Training').getRecordTypeId() );
        contrato1.Pricebook2Id = stdPB;
        
        insert contrato1;
        
        ContractLineItem oli = SObjectInstanceTest.createContractLineItem(contrato1.Id, pbe.Id);
        oli.Slot_status__c = 'Success';
        oli.Converted__c = 5;
        Database.insert(oli);
        
        Service_Contract_Management__c lAccRec = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId()  );
        lAccRec.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec.Service_Contract_line_item__c = oli.Id;
        lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec);
        
        lAccRec.Quantity__c = 10;
        
        update lAccRec;
        
        delete lAccRec;
        
        Service_Contract_Management__c lAccRec1 = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId()  );
        lAccRec1.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec1.Service_Contract_line_item__c = oli.Id;
        //lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec1);
        
        lAccRec1.Quantity__c = 2;
        
        update lAccRec1;
        
        delete lAccRec1;
        
        undelete lAccRec1;
        
        
        Service_Contract_Management__c lAccRec2 = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable').getRecordTypeId()  );
        lAccRec2.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec2.Service_Contract_line_item__c = oli.Id;
        //lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec2);
        
        lAccRec2.Quantity__c = 2;
        
        update lAccRec2;
        
        delete lAccRec2;
        
        undelete lAccRec2;
        
       

            
        
    }
    
     static testMethod void serviceContractManagementTests() 
    {
    
         
        Id stdPB = Test.getStandardPricebookId();
       
        Product2 prod = SObjectInstanceTest.createProduct2();
        insert prod;

        PriceBookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
        insert pbe;

        Account conta = SObjectInstanceTest.createAccount( Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert conta;
      
        ServiceContract contrato1 = SObjectInstanceTest.createServiceContract( conta.id, Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Entitlement').getRecordTypeId());
        contrato1.Pricebook2Id = stdPB;
        
        insert contrato1;
        
        ContractLineItem oli = SObjectInstanceTest.createContractLineItem(contrato1.Id, pbe.Id);
        oli.Slot_status__c = 'Success';
        oli.Converted__c = 5;
        Database.insert(oli);
        
        Service_Contract_Management__c lAccRec = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId()  );
        lAccRec.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec.Service_Contract_line_item__c = oli.Id;
        lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        lAccRec.Quantity__c = 5;
        Database.insert(lAccRec);
        
        lAccRec.Quantity__c = 10;
        
        update lAccRec;
        
        delete lAccRec;
        
        Service_Contract_Management__c lAccRec1 = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId()  );
        lAccRec1.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec1.Service_Contract_line_item__c = oli.Id;
        //lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec1);
        
        lAccRec1.Quantity__c = 2;
        
        update lAccRec1;
        
        delete lAccRec1;
        
        undelete lAccRec1;
        
        
        Service_Contract_Management__c lAccRec2 = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable').getRecordTypeId()  );
        lAccRec2.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec2.Service_Contract_line_item__c = oli.Id;
        //lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec2);
        
        lAccRec2.Quantity__c = 2;
        
        update lAccRec2;
        
        delete lAccRec2;
        
        undelete lAccRec2;
        
        //ServiceContractManagementTriggerHandler handler = new ServiceContractManagementTriggerHandler(true);
        //System.AssertEquals(handler.IsTriggerContext,true);
        
        Test.startTest();

        Database.executeBatch(new updateSCMQuantitiesBatch(),200);

       Test.stopTest();
            
        
    }
    
     static testMethod void serviceContractManagementTestsBatch() 
    {
    
         
        Id stdPB = Test.getStandardPricebookId();
       
        Product2 prod = SObjectInstanceTest.createProduct2();
        insert prod;

        PriceBookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
        insert pbe;

        Account conta = SObjectInstanceTest.createAccount( Schema.SObjectType.Account.getRecordTypeInfosByName().get('Airline').getRecordTypeId());
        insert conta;
      
        ServiceContract contrato1 = SObjectInstanceTest.createServiceContract( conta.id, Schema.SObjectType.ServiceContract.getRecordTypeInfosByName().get('Entitlement').getRecordTypeId());
        contrato1.Pricebook2Id = stdPB;
        
        insert contrato1;
        
        ContractLineItem oli = SObjectInstanceTest.createContractLineItem(contrato1.Id, pbe.Id);
        oli.Slot_status__c = 'Success';
        oli.Converted__c = 5;
        Database.insert(oli);
        
        Service_Contract_Management__c lAccRec = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId()  );
        lAccRec.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec.Service_Contract_line_item__c = oli.Id;
        lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        lAccRec.Quantity__c = 5;
        Database.insert(lAccRec);
        
        lAccRec.Quantity__c = 10;
        
        update lAccRec;
        
        delete lAccRec;
        
        Service_Contract_Management__c lAccRec1 = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Entitlement Conversion').getRecordTypeId()  );
        lAccRec1.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec1.Service_Contract_line_item__c = oli.Id;
        //lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec1);
        
        lAccRec1.Quantity__c = 2;
        
        update lAccRec1;
        
        delete lAccRec1;
        
        undelete lAccRec1;
        
        
        Service_Contract_Management__c lAccRec2 = SObjectInstanceTest.createSvcContractMgmt( conta.id, contrato1.id, null, Schema.SObjectType.Service_Contract_Management__c.getRecordTypeInfosByName().get('Deliverable').getRecordTypeId()  );
        lAccRec2.Contract_Line_Item_Deliverable__c = oli.Id;
        lAccRec2.Service_Contract_line_item__c = oli.Id;
        //lAccRec.Contract_Line_Item_conversion__c = oli.Id;
        Database.insert(lAccRec2);
        
        lAccRec2.Quantity__c = 2;
        
        update lAccRec2;
        
        delete lAccRec2;
        
        undelete lAccRec2;
        
        //ServiceContractManagementTriggerHandler handler = new ServiceContractManagementTriggerHandler(true);
        //System.AssertEquals(handler.IsTriggerContext,true);
        
        Test.startTest();

        Database.executeBatch(new UpdateSCMProductRecordTypeBatch(),200);

       Test.stopTest();
            
        
    }
    
    

}