public class DosService {
    
    //method which returns the list of cases grouped by topmost
    //calculates quantity for each topMost object
    public List<DosTopMost> GroupByTopmost(List<Case> orders) {
        List<DosTopMost> results = new List<DosTopMost>();    //declare list to store results
        
        //iterate through each order
        for(Case outerOrder : orders) {
            if(IsUniqueTopmost(outerOrder.Top_Most_Ecode__c, results)){ //only executes if topMost is not already in results
                DosTopMost tm = new DosTopMost();                     	//declare topMost object, tm
                tm.TopMost = outerOrder.Top_Most_Ecode__c;            	//initialize tm's topMost with order's topMost
                tm.VendorName = outerOrder.WRCS_Vendor_s__c;          	//initialize tm's vendorName from order
                tm.VendorNameLBG = outerOrder.LBG_Vendor__c;			//initialize vendor name LBG			
                tm.Quantity = 0;                                      	//initialize quantity with 0
                tm.PartDescription = outerOrder.Part_Description__c;  	//initialize tm's description with order's description
                tm.ContractTerms = outerOrder.Repair_Contract_Terms__c; //initialize contract terms
                //add topmost to a list of topmosts in string form
                //TopMostStrings.add(tm.TopMost);
                
                //iterate through all orders, count number of topmosts
                for(Case innerOrder : orders) {
                    if(tm.TopMost == innerOrder.Top_Most_Ecode__c){
                        tm.Quantity++;
                        tm.Cases.add(innerOrder);//to have a direct link between a Top Most and its cases.
                    }
                }
                //add tm to results after counting number of topmosts
                results.add(tm);
            }
        }
        //when finished return results
        return results;
    }
    
    //assigns ranks to the list of topMost objects and returns the sorted list
    public List<DosTopMost> RankTopMosts(List<DosTopMost> tms){
        integer rank = 1;    	//declare the first rank
        tms.sort();          	//sort the list of DosTopMost descending order by Quantity
        //assign rank to each DosTopMost starting with 1 and incrementing
        for(DosTopMost tm : tms){
            tm.Rank = rank;    	//assign rank to each tm
            rank++;            	//increment rank
        }
        return tms;
    }
    
    //returns a list of all the unique topmosts in string form
    public List<String> getTopMostStrings(List<DosTopMost> tms){
        List<String> tmStrings = new List<String>();
        for(DosTopMost tm: tms){
                tmStrings.add(tm.Topmost);
        }
        return tmStrings;
    }
    
    //set parts on hand for each DosTopMost
    public void SetPartsOnHand(List<DosTopMost> AllTopMosts, List<LLPDatabase__c> AllPartNumbers){
        for(DosTopMost tm : AllTopMosts){
            for(LLPDatabase__c part : AllPartNumbers){
                if(tm.TopMost == part.Top_Most__c){
                    tm.PartsOnHand.add(part);
                }
            }
        }
    } 
    
    //set cores for each topmost
    public void SetCores(List<DosTopMost> AllTopMosts, List<Core_Information__c> AllCores){
        for(DosTopMost tm : AllTopMosts){
            for(Core_Information__c core : AllCores){
                if(tm.TopMost == core.Topmost__c){
                    tm.Cores.add(core);
                }
            }
        }
    }
    
    //set repair info for each topmost
    public void SetRepairInfo(List<DosTopMost> AllTopMosts, List<Repair_Information__c> AllRepairInfo){
        for(DosTopMost tm: AllTopMosts){
            for(Repair_Information__c ri : AllRepairInfo){
                if(tm.TopMost == ri.Topmost__c){
                    tm.RepairInfo.add(ri);
                }
            }
        }
    }
    
    //set RESSes for each topmost
    public void SetResses(List<DosTopMost> AllTopMosts, List<RESS_Info__c> AllResses){
        for(DosTopMost tm: AllTopMosts){
            for(RESS_Info__c ress : AllResses){
                if(tm.TopMost == ress.Topmost__c){
                    tm.Resses.add(ress);
                }
            }
        }
    }
    
    //set QMs for each topmost
    public void SetQMs(List<DosTopMost> AllTopMosts, List<QM__c> AllQMs){
        for(DosTopMost tm: AllTopMosts){
            for(QM__c qm : AllQMs){
                if(tm.TopMost == qm.Part_Number__r.Top_Most__c){
                    tm.QMs.add(qm);
                }
            }
        }
    }
    
    //set selected Contract Terms
    //returns a filtered list of cases with unique Account names
    public List<Case> setCTs(List<Case> cases){
        List<Case> cts = new List<Case>();
        for(Case c: cases){
            if(isUniqueAccount(c.AccountID, cts)){
                cts.add(c);
            }            
        }
        return cts;
    }
    
    //used only by setCTs()
    //returns true if accountID is not yet in the list
    private Boolean isUniqueAccount(Id acctId, List<Case> cts ){
        for(Case c : cts){
            if(acctId == c.AccountId){
                return false;
            }
        }
        return true;
    }
    
    //takes string and list of DosTopMost, returns true if the string is not already in the list
    private Boolean IsUniqueTopmost(string topMost, List<DosTopMost> results){
        for(DosTopMost result : results){
            if(topMost == result.TopMost){
                return false;
            }
        }
        return true;
    }
    
    //insert a comment into DB
    public void AddCommentToDb(Case currentCase, String NewComment, String SelectedDepartment, Boolean NotifyPRC){
        
        //add new comment to the case if newcomment is not blank
        //updating these case fields will kick off a process build which creates new records of Comments__c
        if(!String.isBlank(NewComment)){
            if(SelectedDepartment == 'PRC'){
                currentCase.PRC_Comments_For_DOS__c=NewComment; //if dept. is PRC add comment to the PRC comments field               
            }else{
                currentCase.POS_Comments_For_DOS__c=NewComment; //dept. is POS, add comment to POS field
            }
            if(NotifyPRC)
                currentCase.POS_Dispo__c=true;
            DosRepository dRepo = new DosRepository();
            dRepo.UpdateCase(currentCase); //update the case in salesforce DB
        }
    }
    
    //clear latest comment 
    public void ClearLatestComment(Case currentCase, string department){
        if(department == 'POS')
            currentCase.POS_Comments_For_DOS__c=null;
        if(department == 'PRC')
            currentCase.PRC_Comments_For_DOS__c=null;
        DosRepository dRepo = new DosRepository();
        dRepo.UpdateCase(currentCase); //update the case in salesforce DB
    }
    
    //search and return a topmost
    public DosTopMost SearchTopMosts(string searchCriterion, List<DosTopMost> AllTopMosts){
        for(DosTopMost tm : AllTopMosts){
            if(tm.TopMost == searchCriterion){
                return tm;
            }
            for(Case order: tm.Cases){
                if(order.SO__c == searchCriterion)
                    return tm;
                if(order.Part_Number__r.Name == searchCriterion)
                    return tm;
                if(order.CaseNumber == searchCriterion)
                    return tm;
                if(order.Ecode__c == searchCriterion)
                    return tm;   
                if(order.EmbCode__c == searchCriterion)
                    return tm;
            }
        }
        return new DosTopMost();
    }
    
    //get picklist options Main_Category__c
    public static List<SelectOption> getPicklistMainCategory(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        Schema.DescribeFieldResult fieldResult = Case.Main_Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        return options;
    }
}