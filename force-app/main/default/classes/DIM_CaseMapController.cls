public class DIM_CaseMapController {

    @AuraEnabled
    public static List<Case> getCases(String caseRecordTypeDeveloperName, String caseStatus, String caseRegion, String defaultAccountName) {
    
        List<Case> cases;
        if(caseRegion == '') {
            cases = [SELECT Id, Subject, CaseNumber, Case_Status__c, Account.Name, Owner.Name, Due_Date__c, Requested_By__r.Name, Study_Type__c, Flag__c, 
                                    Account.BillingCity, Account.BillingCountry, 
                                    Account.BillingLatitude, Account.BillingLongitude, 
                                    Account.Embraer_Site__r.BillingLatitude, Account.Embraer_Site__r.BillingLongitude 
                                FROM Case 
                                WHERE RecordType.DeveloperName =: caseRecordTypeDeveloperName AND Status =: caseStatus
                                ORDER BY Due_Date__c ASC];
        }
        else {
            cases = [SELECT Id, Subject, CaseNumber, Case_Status__c, Account.Name, Owner.Name, Due_Date__c, Requested_By__r.Name, Study_Type__c, Flag__c, 
                                    Account.BillingCity, Account.BillingCountry, 
                                    Account.BillingLatitude, Account.BillingLongitude, 
                                    Account.Embraer_Site__r.BillingLatitude, Account.Embraer_Site__r.BillingLongitude 
                                FROM Case 
                                WHERE RecordType.DeveloperName =: caseRecordTypeDeveloperName AND Status =: caseStatus AND Region_Consolidated__c =: caseRegion
                                ORDER BY Due_Date__c ASC];
        }
                            
        defaultAccountName = defaultAccountName + '%';
        Account defaultAccount = [SELECT Id, Account.BillingLatitude, Account.BillingLongitude FROM Account WHERE Account.Name LIKE :defaultAccountName LIMIT 1];
        
        // 1) Account Latitude and Longitude
        // 2) Account City and Country
        // 3) Embraer Site Latitude and Longitude
        // 4) Embraer SJK Latitude and Longitude
        
        for(Case caso: cases) {
        
            caso.Status__c = getIcon(caso.Flag__c);
            caso.SuppliedName = URL.getSalesforceBaseUrl().toExternalForm();
            caso.Study_Type__c = getShortenedStudyType(caso.Study_Type__c);
        
            if(caso.Account.BillingLatitude == null) {
            
                // 2) Account City and Country
                if(caso.Account.BillingCountry != null) {
                    caso.Account.BillingLatitude = null;
                    caso.Account.BillingLongitude = null;
                }
                else {
            
                    // 3) Embraer Site Latitude and Longitude
                    if(caso.Account.Embraer_Site__r.BillingLatitude != null) {
                        caso.Account.BillingLatitude = caso.Account.Embraer_Site__r.BillingLatitude;
                        caso.Account.BillingLongitude = caso.Account.Embraer_Site__r.BillingLongitude;
                    }
                    // 4) Embraer SJK Latitude and Longitude
                    else {
                        caso.Account.BillingLatitude = defaultAccount.BillingLatitude;
                        caso.Account.BillingLongitude = defaultAccount.BillingLongitude;
                    }
                }
            }
        }
        
        return cases;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static String getIcon(String flag) {
        if (flag.contains('green')) {
            return 'standard:address';
        }
        else if (flag.contains('yellow')) {
            return 'standard:answer_public';
        }
        else if (flag.contains('red')) {
            return 'standard:dashboard';
        }
        return 'standard:generic_loading';
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static String getShortenedStudyType(String type) {
    
        String[] types = type.split(';');
        if(types.size() >  2) {
            type = types[0] + ';' + types[1] + ';+' + (types.size() - 2);
        }

        return type.replace(';', ', ');
    }
}