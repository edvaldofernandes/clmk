/**
* @description: This class represents the repository of Follow-ups(FUPs). 
* For more information, visit: https://martinfowler.com/eaaCatalog/repository.html
**/
public class FO_FupRepository {
    public final static String FUP_DIGEST_EMAIL_TEMPLATE_NAME = 'Fup_Digest';
    public static Set<String> EJET_MODELS = new Set<String>{
        'EMBRAER 170',
        'EMBRAER 175',
        'EMBRAER 190',
        'EMBRAER 195'
    };
    public static Set<String> ERJ_MODELS = new Set<String>{
        'ERJ-145/140/135'
    }; 
    public static Set<String> E2_MODELS = new Set<String>{
        'EMBRAER 190-E2',
        'EMBRAER 195-E2',
        'EMBRAER 175-E2'
    };
     
    public static Fup__c getById(Id fupId){
        return [
            SELECT
                ATA__c,
                Aircraft_Family__c,
                Background__c,
                Final_Solution__c,
                Status__c,
            	Summary__c,
                Reference_Number__c,
            	Publish_Status__c,
                Last_Public_Update__c,
            	Authority__c,
            	Publish_Date__c,
            	Name
            FROM 
            	FUP__c
            WHERE
            	Id =: fupId
        ];
    }
    
    public static List<Fup__c> getPublishedFups(){
        List<Fup__c> publishedFups = [
            SELECT
                ATA__c,
                Aircraft_Family__c,
                Background__c,
                Final_Solution__c,
                Status__c,
            	Summary__c,
                Reference_Number__c,
            	Publish_Status__c,
                Last_Public_Update__c,
            	Authority__c,
            	Publish_Date__c,
            	Name
            FROM 
            	FUP__c
            WHERE
            	Publish_Status__c = 'Published'
            ORDER BY
            	Last_Public_Update__c DESC,
            	Publish_Date__c DESC
        ];
        return publishedFups;
    }
    
    public static List<Fup__c> getPublishedFupsByAircraftFamily(List<String> aircraftFamilies){
        String status = 'Published'; 
        Set<String> aircraftModels = getModelsNameByFleetType(aircraftFamilies);
        String familiesFilter = buildAircraftFamilyFilter(aircraftModels);
        String query = 
           ' SELECT ' + 
           '    ATA__c,' +
           '    Aircraft_Family__c,' +
           '    Background__c,' +
           '    Final_Solution__c,' +
           '    Status__c,' +
           ' 	Summary__c,' +
           '    Reference_Number__c,' +
           ' 	Publish_Status__c,' +
           '    Last_Public_Update__c,' +
           ' 	Authority__c,' +
           ' 	Publish_Date__c,' +
           ' 	Name' +
           ' FROM  ' +
           ' 	FUP__c ' +
           ' WHERE ' +
           ' 	Aircraft_family__c INCLUDES (' + familiesFilter + ') ' +
           ' 	AND Publish_Status__c = \''+ status + '\' ' +
           ' ORDER BY ' +
           ' 	ATA__c ASC, ' +
           ' 	Name ASC ';
        List<Fup__c> publishedFups = Database.query(query);
        return publishedFups;
    }
    
    /**
    * @description Get all updates related to a Published Follow-up.
    **/
    public static List<FUP_Update__c> getPublicUpdatesByFup(Id fupId) {
        List<FUP_Update__c> updates = getPublicUpdatesByFup(new List<Id>{fupId});
        return updates;
    }
    
    /**
    * @description Get all updates related to a list of Published Follow-ups.
    **/
    public static List<FUP_Update__c> getPublicUpdatesByFup(List<Id> fupIds) {
        List<FUP_Update__c> updates = [
            SELECT
            	Id,
            	Body__c,
            	FUP__c,
            	Publish_Date__c
            FROM
            	FUP_Update__c
            WHERE
            	fup__c  IN :fupIds
            	AND Status__c = 'Published'
            	AND FUP__r.Publish_Status__c = 'Published'
			ORDER BY 
            	Publish_Date__c DESC
        ];      
        return updates;
    }
    
    /**
    * @description Get all Fup Updates with Published Date greater than current
    * date(today) minus "timeInterval" and filter by aircraft family
    * (EJet, ERJ or E2).
    * 
    **/
    public static List<FUP_Update__c> getLastNMonthsPublicUpdatesByFleetFamily(Integer timeInterval, List<String> aircraftFamilies){
        String status = 'Published'; 
        Set<String> aircraftModels = getModelsNameByFleetType(aircraftFamilies);
        String familiesFilter = buildAircraftFamilyFilter(aircraftModels);
        
        
            String query =
            'SELECT ' + 
            '	Id, ' + 
            '	Body__c, ' + 
            '	Publish_Date__c, ' + 
            '	FUP__r.Name, ' + 
            '	FUP__r.Aircraft_family__c, ' + 
            '	FUP__r.Reference_Number__c ' + 
            'FROM ' + 
            '	FUP_Update__c ' +
            'WHERE ' + 
            '	FUP__r.Aircraft_family__c INCLUDES (' + familiesFilter + ') ' +  
            '	AND Publish_Date__c = LAST_N_MONTHS:' + timeInterval + ' ' +     
            '	AND Status__c = \''+ status + '\' ' +
            '	AND FUP__r.Publish_Status__c = \'' + status + '\' ';
        
        List<FUP_Update__c> updates = Database.query(query);
        return updates;
        
        
    }
    
     public static List<FUP__c> getLastNMonthsPublicNewsByFleetFamily(Integer timeInterval, List<String> aircraftFamilies){
        String status = 'Published'; 
        Set<String> aircraftModels = getModelsNameByFleetType(aircraftFamilies);
        String familiesFilter = buildAircraftFamilyFilter(aircraftModels);
 
        
            String query =
            'SELECT ' + 
            '	Id, ' + 
            '	Summary__c, ' + 
            '	Publish_Date__c, ' + 
            '	Name, ' + 
            '	Aircraft_family__c, ' + 
            '	Reference_Number__c ' + 
            'FROM ' + 
            '	FUP__c ' +
            'WHERE ' + 
            '	Aircraft_family__c INCLUDES (' + familiesFilter + ') ' +  
            '	AND Publish_Date__c = LAST_N_MONTHS:' + timeInterval + ' ' +     
            //'	AND Status__c = \''+ status + '\' ' +
            '	AND Publish_Status__c = \'' + status + '\' ';
        
        List<FUP__c> news = Database.query(query);
        return news;
        
        
    }
    
    /**
    * @description Get the aircraft models by aicraft family. e. g. When the input is
	* "EJet" the output would be FO_FupRepository.EJET_MODELS.
    **/
    public static Set<String> getModelsNameByFleetType(List<String> aircraftFamilies){
        Set<String> models = new Set<String>();
        if(aircraftFamilies.contains('ERJ'))
            models.addAll(ERJ_MODELS);
        
        if(aircraftFamilies.contains('EJet'))
            models.addAll(EJET_MODELS);
        
        if(aircraftFamilies.contains('E2'))
            models.addAll(E2_MODELS);
        
        return models;
    }
    
    public static EmailTemplate getFupDigestEmailTemplate(){
        EmailTemplate template = [
            SELECT
            	Id
            FROM
                EmailTemplate
            WHERE
            	DeveloperName = :FUP_DIGEST_EMAIL_TEMPLATE_NAME
            LIMIT 1
        ]; 
        return template;
    }
        
    /**
    * @description Builds an String to be used on
	* "getLastNdaysPublicUpdatesByFleetFamily" database query. e. g.
	* An input of {'EMBRAER 175', 'EMBRAER 190'} would have
	* "'EMBRAER 175, EMBRAER 190'" as output.
    **/
    @TestVisible
    private static String buildAircraftFamilyFilter(Set<String> aircraftModels){
        if(aircraftModels.isEmpty()){
            return '\'\'';
        }
        String familiesFilter = '';
        Iterator<String> iter = aircraftModels.iterator();
        while(iter.hasNext()){
            familiesFilter += ' \''+ iter.next() +'\' ';
            if(iter.hasNext()){
                familiesFilter += ',';
            }
        }
        return familiesFilter;
    }

}