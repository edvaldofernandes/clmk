/* 
----------------------------------------------------------------------------------------------
-- - Company:     Stefanini
-- - Name:        IndicatorsHeaderDTO
-- - Description: Data Transfer Object to carry CRC Dashboard header data from apex to 
-- 				  lightning component.
-- - @Author: Felipe Gouvea
-- ------------------------------------------------------------------------------------------
--
-- Date              Name               			Version                   
----------------------------------------------------------------------------------------------
-- 29/03/2019       Felipe Gouvea      				1.0
-- 06/05/2019		Tiago de Jesus Rodrigues		1.1
----------------------------------------------------------------------------------------------
*/

public class IndicatorHeaderDTO {
		@AuraEnabled
		public decimal received 			{get; set;} // Number of CRC cases received this month that passed in processing.

		@AuraEnabled
		public decimal onTime 				{get; set;} // Number of CRC cases received this month that passed in processing and did not exceed the answer time.

		@AuraEnabled
		public string goal 					{get; set;} // Expected percentage of CRC cases answered on time.

		@AuraEnabled
		public string reached 				{get; set;} // Real percentage of CRC cases answered on time.

		@AuraEnabled
		public integer createdYesterday 	{get; set;} // Number of CRC cases created yesterday.

		@AuraEnabled
		public integer closedYesterday 		{get; set;} // Number of CRC cases closed yesterday.

		@AuraEnabled
		public integer createdToday 		{get; set;} // Number of CRC cases created today.

		@AuraEnabled
		public integer closedToday 			{get; set;} // Number of CRC cases closed today.

		@AuraEnabled
		public integer inbox 				{get; set;} // Number of CRC cases with STATUS 'Dispatch'.
    
    	@AuraEnabled
		public integer warningInbox 		{get; set;} // Number of CRC cases with STATUS 'Dispatch' and SLA yellow.
    
    	@AuraEnabled
		public integer delayedInbox 		{get; set;} // Number of CRC cases with STATUS 'Dispatch' and SLA red.

		@AuraEnabled
		public integer fup 					{get; set;} // Number of CRC cases with children cases.
	
    // Class constructor method.
	public IndicatorHeaderDTO(){
		received = 0;
		onTime = 0;
		goal = '';
		reached = '';
		createdYesterday = 0;
		closedYesterday = 0;
		createdToday = 0;
		closedToday = 0;
		inbox = 0;
        warningInbox = 0;
        delayedInbox = 0;
		fup = 0;
	}
}