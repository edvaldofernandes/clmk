@isTest
public class MarginCalculatorHandlerTest2 {
    
    private static Id ACMOD_RecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Aircraft Modification').getRecordTypeId();
    private static Id TechServ_RecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Technical Services').getRecordTypeId();

    @testSetup
    static void setup(){
        
        //Create products
        Product2 prodNormal = new Product2(Name = 'Normal Product', recordtypeid = ACMOD_RecordType);
        insert prodNormal;
        Product2 prodNoCostControl = new Product2(Name = 'No Cost Product', recordtypeid = ACMOD_RecordType);
        insert prodNoCostControl;
        Product2 TechServProd = new Product2(Name = 'Normal Product', recordtypeid = TechServ_RecordType);
        insert TechServProd;
        
        //Create Product Cost Control
        Product_Cost_Control__c productCostControl1 = new Product_Cost_Control__c(
            Related_Product__c = prodNormal.Id,
            NREC__c = 10000,
            REC__c = 3500,
            Sales_Deduction__c = 5.2,
            Tech_Pubs_SB_Revision_Costs__c = 120
        );
        insert productCostControl1;        
        
        //Create Opportunities
        Opportunity oppTest3 = new Opportunity(
            Name= 'OppTest 3',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        insert oppTest3;
        Opportunity oppTest4 = new Opportunity(
            Name= 'OppTest 4',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        insert oppTest4;
        Opportunity oppTest5 = new Opportunity(
            Name= 'OppTest 5',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        insert oppTest5;
        Opportunity oppTest6 = new Opportunity(
            Name= 'OppTest 6',
            Fleet_Type__c = 'TestFleet',
            StageName = 'Prospecting',
            CloseDate = Date.today().addDays(7)
        );
        insert oppTest6;
        
        //Create Pricebook
        PricebookEntry pricebookEntry1 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prodNormal.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry1;
        PricebookEntry pricebookEntry2 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = TechServProd.Id, UnitPrice = 1500, IsActive = true);
        insert pricebookEntry2;
        
        //Create Opportunity Products
        OpportunityLineItem oppProduct4 = new OpportunityLineItem(
            OpportunityId = oppTest4.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 7200,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 72000,
            Princing__c = 'REC'
        );
        insert oppProduct4;
        OpportunityLineItem oppProduct5 = new OpportunityLineItem(
            OpportunityId = oppTest5.Id,
            PricebookEntryId = pricebookEntry1.Id,
            Product2Id = prodNormal.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000,
            Princing__c = 'NREC'
        );
        insert oppProduct5;
        OpportunityLineItem oppProduct6 = new OpportunityLineItem(
            OpportunityId = oppTest6.Id,
            PricebookEntryId = pricebookEntry2.Id,
            Product2Id = TechServProd.Id,
            UnitPrice = 13000,
            Quantity = 10,
            Entry_Fee__c = 0,
            Total_amount__c = 130000
        );
        insert oppProduct6;

        
        //Create Bulk Test opportunities
        /*List<Opportunity> opps = new List<Opportunity>();
        for(Integer i=0; i < 50; i++){
            opps.add(new Opportunity(Name= 'Bulk OppTest'+i,Fleet_Type__c = 'TestFleet', StageName = 'Prospecting', CloseDate = Date.today().addDays(7)));
        }
        insert opps;*/
    }
    
    static testmethod void test03_NoProductsProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 3' limit 1];
        Test.startTest();
        MarginCalculatorHandler MCH = new MarginCalculatorHandler();
        MCH.calculateOpportunityMargin(opp.Id);
        Test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('This opportunity has no products associated with it.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(0, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
    static testmethod void test04_NormalRECOnlyProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 4' limit 1];
        test.startTest();
        MarginCalculatorHandler MCH = new MarginCalculatorHandler();
        MCH.calculateOpportunityMargin(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(51.4, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(51.4, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
    static testmethod void test05_NormalNRECOnlyProcedure(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 5' limit 1];
        test.startTest();
        MarginCalculatorHandler MCH = new MarginCalculatorHandler();
        MCH.calculateOpportunityMargin(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid', opp.Calculation_Status__c, 'Status');
        System.assertEquals(17.8, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(17.8, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
    
        static testmethod void test06_NormalProcedureOfMixedOpp(){
        Opportunity opp = new Opportunity();
        opp = [Select Id, CurrencyIsoCode from Opportunity where name = 'OppTest 6' limit 1];
        test.startTest();
        MarginCalculatorHandler MCH = new MarginCalculatorHandler();
        MCH.calculateOpportunityMargin(opp.Id);
        test.stopTest();
        opp = [
            Select
            Calculation_Status__c, 
            NREC_Gross_Margin__c, 
            REC_Gross_Margin__c, 
            Total_Gross_Margin__c 
            from 
            Opportunity
            where 
            Id = :opp.Id
            limit
            1
        ];
        System.assertEquals('Valid, but there are products that arent aicraft modifications and will not be considered in the gross margin calculation.', opp.Calculation_Status__c, 'Status');
        System.assertEquals(0, opp.NREC_Gross_Margin__c.setScale(1), 'NREC');
        System.assertEquals(0, opp.REC_Gross_Margin__c.setScale(1), 'REC');
        System.assertEquals(0, opp.Total_Gross_Margin__c.setScale(1), 'Total');
    }
}