@isTest
public class EpoolCaseTriggerHandlerTest {
    @isTest public static void testPopulateFields_Exchange(){
        Test.startTest();
        EpoolCaseTriggerHandlerTestDataFactory.createEpoolCase_Pool();
        Test.stopTest();
        
        List<Case> result = [SELECT PO__c, SO__c, Notification__c, Part_Number__r.Name, Need_by_Date__c, Priority FROM Case LIMIT 1];
        
        System.assertEquals('078/19', result[0].PO__c);									//verify PO was populated
     	System.assertEquals('24828417', result[0].SO__c);   							//verify SO
        System.assertEquals('300695184', result[0].Notification__c); 					//verify notif
        System.assertEquals('1116-42-1116 MODS 604', result[0].Part_Number__r.Name);	//verify part number
        System.assertEquals(null, result[0].Need_by_Date__c);							//verify need by date is null
        System.assertEquals('Critical', result[0].Priority);							//verify priority
    }
    
    @isTest public static void testPopulateFields_OSS(){
        Test.startTest();
        EpoolCaseTriggerHandlerTestDataFactory.createEpoolCase_OSS();
        Test.stopTest();
        
        List<Case> result = [SELECT PO__c, SO__c, Notification__c, Part_Number__r.Name, Need_by_Date__c, Priority FROM Case LIMIT 1];
        
        System.assertEquals('PX0369319', result[0].PO__c);								//verify PO was populated
     	System.assertEquals('24829259', result[0].SO__c);   							//verify SO
        System.assertEquals('300695293', result[0].Notification__c); 					//verify notif
        System.assertEquals('104003-2', result[0].Part_Number__r.Name); 				//verify part number
        System.assertEquals(null, result[0].Need_by_Date__c);							//verify need by date is null
        System.assertEquals('OSS', result[0].Priority);									//verify priority
        //System.assertEquals('EF0658', result[0].OSS_Consumed_Serial_Number__c);			//verify OSS serial number DEPRECATED
    }
    
    //tests a batch of multiple cases being uploaded
    @isTest public static void testPopulateFields_MultiCases(){
        Test.startTest();
        EpoolCaseTriggerHandlerTestDataFactory.createMultiCases();
        Test.stopTest();
        
        List<Case> result1 = [SELECT PO__c, SO__c, Notification__c, Part_Number__r.Name, Need_by_Date__c, AccountId, ContactId, MFIR__c, Priority FROM Case WHERE Notification__c='300695293' LIMIT 1];
        List<Account> expectedAccount = [SELECT Id FROM Account LIMIT 1];
        List<Contact> expectedContact = [SELECT Id FROM Contact LIMIT 1];
        List<MFIR__c> expectedMFIR = [SELECT Id FROM MFIR__c LIMIT 1];
        
        System.assertEquals('PX0369319', result1[0].PO__c);								//verify PO was populated
     	System.assertEquals('24829259', result1[0].SO__c);   							//verify SO
        System.assertEquals('300695293', result1[0].Notification__c); 					//verify notif
        System.assertEquals('104003-2', result1[0].Part_Number__r.Name); 				//verify part number
        System.assertEquals(null, result1[0].Need_by_Date__c);							//verify need by date
        //System.assertEquals('EF0658', result1[0].OSS_Consumed_Serial_Number__c);		//verify OSS serial number DEPRECATED
        System.assertEquals(expectedAccount[0].Id, result1[0].AccountId);				//verify account
        System.assertEquals(expectedContact[0].Id, result1[0].ContactId);				//verify contact
        System.assertEquals(expectedMFIR[0].Id, result1[0].MFIR__c);					//verify mfir
        System.assertEquals('OSS', result1[0].Priority);								//verify priority
        
        List<Case> result2 = [SELECT PO__c, SO__c, Notification__c, Part_Number__r.Name, Need_by_Date__c, Priority FROM Case WHERE Notification__c='300695184' LIMIT 1];
        
        System.assertEquals('078/19', result2[0].PO__c);								//verify PO was populated
     	System.assertEquals('24828417', result2[0].SO__c);   							//verify SO
        System.assertEquals('300695184', result2[0].Notification__c); 					//verify notif
        System.assertEquals('1116-42-1116 MODS 604', result2[0].Part_Number__r.Name); 	//verify part number
        System.assertEquals(null, result2[0].Need_by_Date__c);							//verify need by date
        System.assertEquals('Critical', result2[0].Priority);							//verify priority
        
        List<Case> result3 = [SELECT PO__c, SO__c, Notification__c, Part_Number__r.Name, Need_by_Date__c, Priority FROM Case WHERE Notification__c='300684698' LIMIT 1];
        
        System.assertEquals('R2427270', result3[0].PO__c);								//verify PO was populated
     	System.assertEquals('24751403', result3[0].SO__c);   							//verify SO
        System.assertEquals('300684698', result3[0].Notification__c); 					//verify notif
        System.assertEquals('104003-2', result3[0].Part_Number__r.Name); 				//verify part number
        System.assertEquals(Date.newInstance(2019, 4, 8), result3[0].Need_by_Date__c);	//verify need by date
        System.assertEquals('Routine', result3[0].Priority);							//verify priority
    }
}