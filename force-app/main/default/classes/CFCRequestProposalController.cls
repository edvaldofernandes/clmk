/**
* Controller class for the visualforce page CFCProductDetails.
* Name: CFCProductEditController
* @author - Guilherme Nascimento - gjesus@deloitte.com
* @version 1.0 - 15/12/2015
**/

public with sharing class CFCRequestProposalController {
    
    public Boolean isAircraftModification {get; set;}
    public Boolean showForm {get; set;}
    
    public Proposal__c proposalDetail {get; set;}
    public List<Proposal_Items__c> itemsProposal {get; set;}
    
    public String deleteItemId {get;set;}    
    
    public String aircraft {get; set;}
    public String proposalType {get; set;}
    public String productionLine {get; set;}
    public String proposalCreatedDate {get;set;}
    public String serialNumber {get;set;}
    public String descriptionRequest {get;set;}
    public String reasonRequest {get;set;}
    
    public Map<String, String> mapThumbnails {get; set;}
    
    public Attachment attachment {get; set;}
    public List<Attachment> listAttachment {get;set;}
    public String removeAttachmentId {get; set;}
    
    public User userCFC {get;set;}
	public Boolean isPardotActive {get;set;}
    
    public CFCRequestProposalController(){
        VerifyPardot();
        
        attachment = new Attachment();
        listAttachment = new List<Attachment>();
        
        //get user
        userCFC = UserDAO.getInstance().getUserById(UserInfo.getUserId());
        system.debug('CFCRequestProposalController.userCFC >>> ' + userCFC);
        system.debug('CFCRequestProposalController.userCFC.Contact.id >>> ' + userCFC.Contact.id);
        system.debug('CFCRequestProposalController.userCFC.Contact.Account.id >>> ' + userCFC.Contact.Account.id);
        
        //get open proposal
        proposalDetail = ProposalDao.getInstance().getOpenProposalByContactAndAccount(userCFC.Contact.id, userCFC.Contact.Account.id);
        system.debug('CFCRequestProposalController.CFCRequestProposalController().proposalDetail >>> '+ proposalDetail);        
        
        //if proposal is valid
        if(proposalDetail != null){
            
            //fill variables
            isAircraftModification = true;
            showForm = false;
            proposalCreatedDate = date.today().format();
            
            //get items by proposal
            itemsProposal = ProposalItemsDao.getInstance().listItemsByProposal(proposalDetail.Id);
            system.debug('CFCRequestProposalController.CFCRequestProposalController().itemsProposal >>> '+ itemsProposal);
            
            //get attachments by proposal
            listAttachment = AttachmentDao.getInstance().listByParentId(proposalDetail.Id);
            system.debug('CFCRequestProposalController.CFCRequestProposalController().listAttachment >>> '+ listAttachment);
            system.debug('CFCRequestProposalController.CFCRequestProposalController().listAttachment.size() >>> '+ listAttachment.size());
            
            if(itemsProposal.size() > 0){
                showForm = true;            
                
                //set recordtype of products
                Set<String> setRecordTypeId = new Set<String>();
                for(Proposal_Items__c item : itemsProposal){
                    setRecordTypeId.add(item.Product__r.RecordTypeId);
                }
                system.debug('CFCRequestProposalController.CFCRequestProposalController().setRecordTypeId >>> '+ setRecordTypeId);
                
                //get recordType
                //List<RecordType> lstRType = [SELECT Id, DeveloperName FROM RecordType Where Id IN :setRecordTypeId];
                List<RecordType> lstRType = RecordTypeDao.getInstance().listBySetId(setRecordTypeId);
                system.debug('CFCRequestProposalController.CFCRequestProposalController().lstRType >>> '+ lstRType);
                
                //verify recordtypes
                for(RecordType rType : lstRType){
                    if(!rType.DeveloperName.equalsIgnoreCase('Aircraft_Modification')){
                        isAircraftModification = false;
                    }
                }
                
                GenerateThumbnails(itemsProposal);
                
                system.debug('CFCRequestProposalController.CFCRequestProposalController().isAircraftModification >>> '+ isAircraftModification);
            }
            
        }
        
    }
    
    public void VerifyPardot(){
        system.debug('CFCRequestProposalController.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCRequestProposalController.isPardotActive >>> ' + isPardotActive);
    }
    
    public pageReference SaveProposal(){
        system.debug('CFCRequestProposalController.SaveProposal()');
        
        Boolean isValidPriceBookEntry = true;
        string idOwner = GenerateOwnerId(); 
        
        //get proposal_items to generate the proposal ID
        List<Proposal_Items__c> listProposalItems = ProposalItemsDao.getInstance().listItemsByProposal(proposalDetail.Id);
        system.debug('CFCRequestProposalController.SaveProposal.listProposalItems >>> ' + listProposalItems);
        
        //generate set of id products
        String idProposal = GenerateIDProposal(listProposalItems);
        system.debug('CFCRequestProposalController.SaveProposal.idProposal >>> ' + idProposal);
        
        //create input form	
        if(isAircraftModification == true){
            InputForm__c inputForm = InputFormDao.getInstance().createByProposal(proposalDetail.Id, aircraft, serialNumber, descriptionRequest, 
                                                                                reasonRequest, proposalType, productionLine);
            system.debug('CFCRequestProposalController.SaveProposal.inputForm >>> ' + inputForm);    
        }
        
        //generate name of opportunity
        string opportunityName = generateOpportunityName(listProposalItems, proposalDetail);
        
        //generateRemarks
        string remarksProposal = generateRemarks(proposalDetail.Remarks__c, listProposalItems);
        
        //update the proposal
        proposalDetail = ProposalDao.getInstance().closeProposal(proposalDetail, idProposal, idOwner, remarksProposal, opportunityName);
        system.debug('CFCRequestProposalController.SaveProposal.proposalDetail >>> ' + proposalDetail);
        
        //redirect to finish page with the proposal id
        PageReference page = new PageReference('/apex/CFCRequestProposalFinish?id=' + proposalDetail.Id);         
        return page;        
    }   
    
    public pageReference DeleteItem(){
        system.debug('CFCRequestProposalController.DeleteItem()');
        system.debug('CFCRequestProposalController.DeleteItem.deleteItemId >>> ' + deleteItemId);
        
        Proposal__c proposal = ProposalDao.getInstance().getOpenProposalByContactAndAccount(userCFC.Contact.id, userCFC.Contact.Account.id);
        system.debug('CFCRequestProposalController.DeleteItem.proposal >>> ' + proposal);
        if(proposal != null){
            Proposal_Items__c item = ProposalItemsDao.getInstance().getItemByProposalAndProduct(proposal.Id, deleteItemId);
            system.debug('CFCRequestProposalController.DeleteItem.item >>> ' + item);
            if(item != null){
                ProposalItemsDao.getInstance().deleteProposalItem(item);
            }
        }
        
        PageReference page = new PageReference('/apex/CFCRequestProposal');
        page.setRedirect(true);
        return page;
    }
    
    public String GenerateIDProposal(List<Proposal_Items__c> listProposalItems){
        Set<String> setIdItem = new Set<String>();
        
        for(Proposal_Items__c item : listProposalItems){
            setIdItem.add(item.Product__c);
        }
        system.debug('CFCRequestProposalController.GenerateIDProposal.setIdItem >>> ' + setIdItem);
        
        String idProposal = userCFC.Contact.Account.id;
        for(String s : setIdItem){
            idProposal+= s;
        }
        system.debug('CFCRequestProposalController.GenerateIDProposal.setIdItem >>> ' + idProposal);
        
        idProposal+=string.valueOf(Date.today().month());
        idProposal+=string.valueOf(Date.today().year());
        system.debug('CFCRequestProposalController.GenerateIDProposal.idProposal >>> ' + idProposal);
        
		return idProposal;
    }
    
    public String GenerateOwnerId(){   
        if(userCFC.Contact.Account.CAM__c != null){
            return userCFC.Contact.Account.CAM__c;
        } else {
            CFC_Configurations__c cs = CFC_Configurations__c.getOrgDefaults(); 
            system.debug('CFCRequestProposalController.GenerateOwnerId.cs >>> ' + cs);
            User userOwnerId = UserDao.getInstance().getByUserName(cs.User_Name__c);
            system.debug('CFCRequestProposalController.GenerateOwnerId.userOwnerId >>> ' + userOwnerId);
            return userOwnerId.id;     
        }
    }
    
    public String GenerateStringFleetType(List<Proposal_Items__c> lstItemsProposal){
        string fleetType = '';
        List<Product2> lstProducts = new List<Product2>();
        
        for(Proposal_Items__c item : lstItemsProposal){
            Product2 prod = Product2DAO.getInstance().getProductPublishById(item.Product__c);
            if(prod != null){
                lstProducts.add(prod);
            }
        }
        System.debug('CFCRequestProposalController.GenerateStringFleetType.lstProducts >>> ' + lstProducts);
        
        for(Product2 prod : lstProducts){
            if(prod.Applicability__c != null && prod.Applicability__c != ''){
            	fleetType+= ';'+prod.Applicability__c;
            }
        }
        System.debug('CFCRequestProposalController.GenerateStringFleetType.fleetType >>> ' + fleetType);
        
        return fleetType;
    }
    
    public void GenerateThumbnails(List<Proposal_Items__c> items){
        system.debug('CFCRequestProposalController.GenerateThumbnails()');
        
        mapThumbnails = new Map<String, String>();        
        Set<String> setIdProducts = new Set<String>();
        RecordType rType = RecordTypeDao.getInstance().getByDeveloperName('Images_to_home');
        
        system.debug('CFCRequestProposalController.GenerateThumbnails.items >>> ' + items);
        //creating a set of products, just to ensure that there is not equals id
        for(Proposal_Items__c item : items){
            setIdProducts.add(item.Product__c);
        }
        
        //populate the map
        for(String item : setIdProducts){
            mapThumbnails.put(item, 'no image');
        }
        system.debug('CFCFavoritesController.GenerateThumbnails.mapThumbnails >>> ' + mapThumbnails);
        
        //get list attchments__c images to home 
        List<Attachments__c> lstAttch = AttachmentCustomDao.getInstance().listBySetProductIdAndRecordTypeId(setIdProducts, rType.Id);
        
        //mapping attchment id by product id
        for(Integer i = 0; i < lstAttch.size(); i++){
            if(mapThumbnails.containsKey(lstAttch.get(i).Product__c)){
                try{
                    mapThumbnails.put(lstAttch.get(i).Product__c, lstAttch.get(i).Attachments[0].id);
                }catch(Exception ex){
                    mapThumbnails.put(lstAttch.get(i).Product__c, 'no image');   
                }
            }            
        }        
        System.debug('CFCRequestProposalController.GenerateThumbnails.mapThumbnails >>> ' + mapThumbnails);
    }
    
    /**
     * DROPDOWNLIST AIRCRAFT 
	**/    
    public List<SelectOption> getAircraftItems() {
        System.debug('CFCRequestProposalController.getAircraftItems()');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();
        
        for(Schema.Picklistentry item : Proposal__c.fields.Aircraft_Model__c.getDescribe().getpicklistvalues()) {
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        return options;         
    }
    
    /**
     * DROPDOWNLIST PROPOSAL TYPE 
	**/    
    public List<SelectOption> getProposalTypeItems() {
        System.debug('CFCRequestProposalController.getProposalTypeItems()');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();        
        
        for(Schema.Picklistentry item : InputForm__c.fields.Required_proposal_type__c.getDescribe().getpicklistvalues()) {
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        return options;         
    }
    
    /**
     * DROPDOWNLIST PRODUCTION LINE
	**/    
    public List<SelectOption> getProductionLineItems() {
        System.debug('CFCRequestProposalController.getProductionLineItems()');       
        
        LIST<SelectOption> options = new LIST<SelectOption>();        
        
        for(Schema.Picklistentry item : InputForm__c.fields.Required_for_production_line__c.getDescribe().getpicklistvalues()) {
            options.add(new SelectOption(item.getLabel(),item.getValue()));
        }
        
        return options;         
    }    
	
    
    public pageReference insertAttachment() {
        system.debug('CFCRequestProposalController.insertAttachment.attachment: ' + attachment);
        if(attachment != null && attachment.Body != null && attachment.Name != null){
            attachment.ParentId = proposalDetail.Id;
            Database.upsert(attachment);
            attachment.Body = null;
            listAttachment.add(attachment);
            attachment = new Attachment();
        }        
        return null;           
    }
    public PageReference removeAtt() {        
        system.debug('CFCRequestProposalController.removeAtt.removeAttachmentId: ' + removeAttachmentId);
        if(removeAttachmentId != null){
            for(Integer i=0; i<listAttachment.size(); i++) {
                if(removeAttachmentId == listAttachment.get(i).id) {
                    Database.delete(listAttachment.get(i).id);
                    listAttachment.remove(i);
                }
            }
        }        
        return null;
    }
    
    private String generateOpportunityName (List<Proposal_Items__c> lstItems, Proposal__c prop){
        system.debug('CFCRequestProposalController.generateOpportunityName()');
        
        //concat name of proposal and replace PRP to CFC
        
        string strName = '';
        try {
        	strName = prop.Name.replace('PRP', 'CFC');
        } catch (Exception e){
        }
        
        system.debug('CFCRequestProposalController.generateOpportunityName.strName >>> ' + strName);
        
		//concat name of account is not null or blank        
        if(!String.isBlank(userCFC.Contact.Account.Name)){
            strName+= ' - ' +userCFC.Contact.Account.Name; 
        }
        system.debug('CFCRequestProposalController.generateOpportunityName.strName >>> ' + strName);
        
        //if list of products is more than one so we contact the string 'multiple products request' else the name of product
        if(lstItems.size() > 1){
            strName += ' - multiple products request';
        } else {
        	if(lstItems.size() == 1){
            	strName += ' - ' + lstItems[0].Product__r.Name;
        	}
        }
        system.debug('CFCRequestProposalController.generateOpportunityName.strName >>> ' + strName);
        system.debug('CFCRequestProposalController.generateOpportunityName.strName.length() >>> ' + strName.length());
        
        //we must ensure that string had 120 of leght
        strName = strName.length() > 120 ? strName.substring(0, 117) + '...' : strName;
        system.debug('CFCRequestProposalController.generateOpportunityName.strName >>> ' + strName);    
        system.debug('CFCRequestProposalController.generateOpportunityName.strName.length() >>> ' + strName.length());
        
        return strName;
    }
    
    private String generateRemarks(string remarks, List<Proposal_Items__c> lstItems){
        system.debug('CFCRequestProposalController.generateRemarks()');
        string newRemarks = '';
        
        if(!string.isBlank(remarks)){
            newRemarks += remarks + '\n';
        }
        system.debug('CFCRequestProposalController.generateRemarks.newRemarks >>> ' + newRemarks);
        
        for(Proposal_Items__c item : lstItems){
            newRemarks += '\n' + item.Product__r.Name;
        }
        system.debug('CFCRequestProposalController.generateRemarks.newRemarks >>> ' + newRemarks);
        
        newRemarks = newRemarks.removeEnd(',');
        system.debug('CFCRequestProposalController.generateRemarks.newRemarks >>> ' + newRemarks);
        
        return newRemarks;
    }
}