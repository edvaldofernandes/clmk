global class FO_FupDigestSender implements Database.Batchable<sObject> {
    global Iterable<sObject> start(Database.BatchableContext bc) {
        return FO_ContactRepository.getContactsReceivingFupReport();
    }
    global void execute(Database.BatchableContext bc, List<Contact> records){
        EmailTemplate emailTemplate = FO_FupRepository.getFupDigestEmailTemplate();
        

        List<Messaging.SingleEmailMessage> messages =
            new List<Messaging.SingleEmailMessage>();
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'opereng@embraer.com.br'];
        
        for(Contact c: records) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            if ( owea.size() > 0 ) {
    			message.setOrgWideEmailAddressId(owea.get(0).Id);
			}
            message.setUseSignature(false);
            message.setTemplateId(emailTemplate.Id);
            message.setTargetObjectId(c.Id);
            messages.add(message);
        }

        Messaging.sendEmail(messages, false);        
        
    }    
    global void finish(Database.BatchableContext bc){}    
}