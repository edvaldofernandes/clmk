/* Classe implementadora de SOBjectDAO para operações DML no objeto Proposal__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*           gjesus@deloitte.com
* @version 1.0 04/01/2016
*/
public without sharing class ProposalDao {
    
    private static final ProposalDao instance = new ProposalDao();    
    
    private ProposalDao(){
    }    
    
    public static ProposalDao getInstance(){
        return instance;
    }

    public Proposal__c getOpenProposalByContactAndAccount(String idContact, String idAccount){
        List<Proposal__c> listProposal;
        try {
            listProposal = database.query(' Select ' + utils.getAllFields('Proposal__c') +
                                                            ' FROM Proposal__c WHERE Contact__c =\''+String.escapeSingleQuotes(idContact)+'\''+
                                                            ' AND Account__c =\''+String.escapeSingleQuotes(idAccount)+'\''+
                                                            ' AND Opportunity_Create__c = FALSE AND ID__c = NULL LIMIT 1');
        } catch (Exception e) {
            listProposal = new List<Proposal__c>();
        }
                
        if(listProposal.size() > 0){
            return listProposal[0];
        }
        
        return null;   
    }
    
    
    public Proposal__c createProposal(String accountId, String contactID, String contactEmail, String contactPhone){
        Proposal__c prop = new Proposal__c();
        prop.Contact__c = contactID;
        prop.Email__c = contactEmail;
        prop.Account__c = accountId;
        prop.Phone__c = contactPhone;
        insert prop;
        
        return prop;              
    }
    
    public Proposal__c closeProposal(Proposal__c proposal, String idProposal, String idOwner, String remarks, string opportunityName){
        
        
        proposal.Opportunity_Create__c = true;
        proposal.Create_Date__c = date.today();
        proposal.ID__c = idProposal;
        proposal.OwnerId = idOwner;
        proposal.Remarks__c = remarks;
        proposal.Opportunity_Name__c = opportunityName;
        update proposal;
        
        return proposal;
    }
    
}