/* Classe implementadora de SOBjectDAO para operações DML no objeto Product2, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 04/01/2016
*/
public without sharing class Product2DAO {
    
    private static final Product2DAO instance = new Product2DAO();    
    
    private Product2DAO(){
    }    
    
    public static Product2DAO getInstance(){
        return instance;
    }
    
    public LIST<Product2> getListBySetId(SET<String> idProduct){
        string setToString = '';
        for(String includeValue : idProduct){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') +' FROM Product2 WHERE Id IN :idProduct ORDER BY CreatedDate desc LIMIT 10 ');
        return listProduct;
    }  
    
    public LIST<Product2> getAllListBySetId(SET<String> idProduct){
        string setToString = '';
        for(String includeValue : idProduct){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') +' FROM Product2 WHERE Id IN :idProduct');
        return listProduct;
    }  
    
    
    public Product2 getProductById(String idProduct){
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') + ' ,RecordType.Name FROM Product2' +
                                                    ' WHERE Id =\''+String.escapeSingleQuotes(idProduct)+'\' LIMIT 1 ');        
        if(listProduct.size() > 0){
            return listProduct[0];
        }
        
        return null;
    }
    
    public Product2 getProductPublishById(String idProduct){        
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') + ' FROM Product2' +
                                                    ' WHERE Id =\''+String.escapeSingleQuotes(idProduct)+'\' AND Publication_Status__c = \'Published\' LIMIT 1 ');
        if(listProduct.size() > 0){
            return listProduct[0];
        }
        return null;
    }

    public List<Product2> listPublishedBySetApplicability(Set<String> setApplicability){
        string setToString = '';
        for(String includeValue : setApplicability){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        system.debug('setToString >>> ' + setToString);
        
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') +
                                         ' FROM Product2 WHERE Applicability__c INCLUDES ('+setToString+')'+
                                         ' AND Publication_Status__c = \'Published\'' +
                                         ' AND Display_on_home_page__c = true');
        return listProduct;
    }
    
    public List<Product2> listPublishedByNotSetApplicability(Set<String> setApplicability){        
        string paramApplicability = '';
        for(String includeValue : setApplicability){
            paramApplicability += '\''+includeValue + '\',';
        }
        paramApplicability = String.isBlank(paramApplicability) ? paramApplicability : paramApplicability.removeEnd(',');

        system.debug('Product2DAO.listPublishedByNotSetApplicability.paramApplicability >>> ' + paramApplicability);
        
        String querySOQL = 'SELECT eTraining_Link__c, Technical_Description__c, Weight_Change__c, Trial_Period__c, Trial_Description__c,'+
            ' Show_Proposal__c, References__c, RecordTypeId, Publication_Status__c, Product_Type__c, Product_Status__c,'+ 
            ' ProductCode, Prerequisites__c, Name, Maintance_Capability__c, Location_Airport__c, Indicates_MRO__c, Id, Family, Estimated_Weight_Change__c,'+
            ' Estimated_Lead_Time_SB_or_Service_del__c, Estimated_Aircraft_Man_Hours__c, Estimated_Aircraft_Downtime__c, Commercial_Description__c,'+
            ' Description__c, Description, Definitions__c, Commercial_Title__c, Commercial_Requirement__c, Category__c, Benefits__c, Applicability_company__c,'+
            ' Applicability__c, Airline_Level_Requirements__c, Aircraft_Level_Requirements__c, ATA_Chapter__c, Aircraft_Model__c, Intended_Audience__c, Pardot_Status__c, Custom_Redirect_Pardot__c'+
            ' FROM Product2';
        
        if(String.isBlank(paramApplicability)){
            system.debug('paramApplicability >>> NULL');
            querySOQL += ' WHERE Applicability__c EXCLUDES (\' \') ';
        } else {
            system.debug('paramApplicability >>> NOT NULL');
            querySOQL += ' WHERE Applicability__c EXCLUDES ('+paramApplicability+')';
        }
        
        querySOQL += ' AND Publication_Status__c = \'Published\' AND Display_on_home_page__c = true';
        
        system.debug('Product2DAO.listPublishedByNotSetApplicability.querySOQL >>> ' + querySOQL);
        return Database.query(querySOQL);
    }
    
    public Product2 getProductByCaseId(String queryStringId) {
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') +
                                                    ' FROM Product2 Where Id =\''+String.escapeSingleQuotes(queryStringId)+'\'');
        if(listProduct.size() > 0) {
            return listProduct[0];
        }
        return null;
    }
    
    public Product2 getAirCraftModelById(String queryStringId) {
        List<Product2> listProduct = database.query(' Select ' + utils.getAllFields('Product2') +
                                                    ' FROM Product2 Where Id =\''+String.escapeSingleQuotes(queryStringId)+'\'');
        if(listProduct.size() > 0) {
            return listProduct[0];
        }
        return null;
    }
    
    public List<Product2> getAllProducts(String family){
        String queryString;
        if(family.equalsIgnoreCase('E1')){
        	queryString = 'E1\'';
        } else if (family.equalsIgnoreCase('E2')){
            queryString = 'E2\'';
        } else {
            queryString = 'E1' + '\' OR name like \'E2\'';
        }
        List<Product2> lstResult = new List<Product2>();
        lstResult = database.query(' Select ' + utils.getAllFields('Product2') + ' FROM Product2 WHERE Aircraft_Family__c = \'' + queryString);
        return lstResult;
    }
}