@isTest
public class AttachmentTriggerHandlerTest 
{
	static testMethod void validateAttachmentDesktop(){
        
        RecordType recordTypeDocument = [ Select Name, Id From RecordType Where SobjectType = 'Attachments__c' And DeveloperName = 'Billboards' limit 1 ];
        
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        system.debug('CFCBillboardControllerTest.myUnitTest.bGroup >>> ' + bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Published';
        database.insert(billboard);
        system.debug('CFCBillboardControllerTest.myUnitTest.billboard >>> ' + billboard);
        
        CFC_Configurations__c cs = new CFC_Configurations__c();
        cs.Mobile_Limits_KB__c = 1;
        cs.Name = 'Test CS';
        cs.Tablet_Limits_KB__c = 1;
        cs.Desktop_Limit_KB__c = 1;
        
        cs.Billboard_max_published_number__c = 5;
        cs.URL_Communities__c = 'wwww.teste.com.br';
        cs.Billboard_Time__c = 5;
        cs.Email_to_send_error_job__c = 'teste@teste.com.br';
        cs.User_Name__c = 'teste@teste.com';
        
        database.insert(cs);
        system.debug('CFCBillboardControllerTest.myUnitTest.cs >>> ' + cs);
        
        //ATTACHMENT DOCUMENT DESKTOP
        Attachments__c attDocument = new Attachments__c();
        attDocument.Billboard__c = billboard.Id;
        attDocument.Active__c = true;
        attDocument.Description__c = 'teste';
        attDocument.Status__c = 'Published';
        attDocument.RecordTypeId = recordTypeDocument.id;
        attDocument.Device_Type__c = 'Desktop';
        database.insert(attDocument);
        system.debug('CFCProductDetailsControllerTest.myUnitTest.attDocument >>> ' + attDocument);
        
        Attachment attachment = new Attachment();  
        attachment.ParentId = attDocument.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = Blob.toPdf('Test Attachment for Parent');
        database.insert(attachment);
        
        Attachment attachmentSelect = new Attachment();                          
        attachmentSelect = [ Select BodyLength From Attachment where ParentId = :attDocument.Id limit 1];
      
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachmentSelect);
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachmentSelect.BodyLength);
                              
        //Attachments__c attachmentDesk = [Select fileSize, item.Device_Type__c From Attachments__c where Id = :attachment.Id];                             
        attachment.Name = 'Update Test Attachment';
        update attachment;
     
        delete attachment;
                                     
    	undelete attachment;
      
        AttachmentTriggerHandler handler = new AttachmentTriggerHandler(true);                        	
        System.AssertEquals(handler.IsTriggerContext,true);
     }
                                      
    static testMethod void validateAttachmentMobile(){
        
        RecordType recordTypeDocument = [ Select Name, Id From RecordType Where SobjectType = 'Attachments__c' And DeveloperName = 'Billboards' limit 1 ];
        
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        system.debug('CFCBillboardControllerTest.myUnitTest.bGroup >>> ' + bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Published';
        database.insert(billboard);
        system.debug('CFCBillboardControllerTest.myUnitTest.billboard >>> ' + billboard);
        
        CFC_Configurations__c cs = new CFC_Configurations__c();
        cs.Mobile_Limits_KB__c = 1;
        cs.Name = 'Test CS';
        cs.Tablet_Limits_KB__c = 1;
        cs.Desktop_Limit_KB__c = 1;
        
        cs.Billboard_max_published_number__c = 5;
        cs.URL_Communities__c = 'wwww.teste.com.br';
        cs.Billboard_Time__c = 5;
        cs.Email_to_send_error_job__c = 'teste@teste.com.br';
        cs.User_Name__c = 'teste@teste.com';
        
        database.insert(cs);
        system.debug('CFCBillboardControllerTest.myUnitTest.cs >>> ' + cs);
        
        
        //ATTACHMENT DOCUMENT MOBILE
        Attachments__c attDocument1 = new Attachments__c();
        attDocument1.Billboard__c = billboard.Id;
        attDocument1.Active__c = true;
        attDocument1.Description__c = 'teste';
        attDocument1.Status__c = 'Published';
        attDocument1.RecordTypeId = recordTypeDocument.id;
        attDocument1.Device_Type__c = 'Mobile';
        database.insert(attDocument1);
        system.debug('CFCProductDetailsControllerTest.myUnitTest.attDocument1 >>> ' + attDocument1);
        
        
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = attDocument1.Id;  
        attachment1.Name = 'Test Attachment for Parent';  
        attachment1.Body = Blob.toPdf('Test Attachment for Parentd');
        database.insert(attachment1);
        
        Attachment attachmentSelect1 = new Attachment();                          
        attachmentSelect1 = [ Select BodyLength From Attachment where ParentId = :attDocument1.Id limit 1];
        
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachmentSelect1);
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachmentSelect1.BodyLength);
        
        attachment1.Name = 'Update Test Attachment';
        update attachment1;
        
        delete attachment1;
        
        undelete attachment1;
        
        AttachmentTriggerHandler handler = new AttachmentTriggerHandler(true);                        	
        System.AssertEquals(handler.IsTriggerContext,true);  
    }
                                      
    static testMethod void validateAttachmentTablet(){
        
        RecordType recordTypeDocument = [ Select Name, Id From RecordType Where SobjectType = 'Attachments__c' And DeveloperName = 'Billboards' limit 1 ];
        
        Billboard_Group__c bGroup = new Billboard_Group__c();
        database.insert(bGroup);
        system.debug('CFCBillboardControllerTest.myUnitTest.bGroup >>> ' + bGroup);
        
        Billboard__c billboard = new Billboard__c();
        billboard.Billboard_Group__c = bGroup.id;
        billboard.External_Link__c = '';
        billboard.Publish_Status__c = 'Published';
        database.insert(billboard);
        system.debug('CFCBillboardControllerTest.myUnitTest.billboard >>> ' + billboard);
        
        CFC_Configurations__c cs = new CFC_Configurations__c();
        cs.Mobile_Limits_KB__c = 1;
        cs.Name = 'Test CS';
        cs.Tablet_Limits_KB__c = 1;
        cs.Desktop_Limit_KB__c = 1;
        
        cs.Billboard_max_published_number__c = 5;
        cs.URL_Communities__c = 'wwww.teste.com.br';
        cs.Billboard_Time__c = 5;
        cs.Email_to_send_error_job__c = 'teste@teste.com.br';
        cs.User_Name__c = 'teste@teste.com';
        
        database.insert(cs);
        system.debug('CFCBillboardControllerTest.myUnitTest.cs >>> ' + cs);
        
        //ATTACHMENT DOCUMENT TABLET
        Attachments__c attDocument2 = new Attachments__c();
        attDocument2.Billboard__c = billboard.Id;
        attDocument2.Active__c = true;
        attDocument2.Description__c = 'teste';
        attDocument2.Status__c = 'Published';
        attDocument2.RecordTypeId = recordTypeDocument.id;
        attDocument2.Device_Type__c = 'Tablet';
        database.insert(attDocument2);
        system.debug('CFCProductDetailsControllerTest.myUnitTest.attDocument2 >>> ' + attDocument2);
        
        Attachment attachment2 = new Attachment();  
        attachment2.ParentId = attDocument2.Id;  
        attachment2.Name = 'Test Attachment for Parent';  
        //attachment2.Body = Blob.toPdf('Test Attachment for ParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgsParentdfsgfdsgfdsgfdsgfdsdfgdfsgfdsgfdfsgfdsdfgdsifgomdsfomgosmdfogdskmfogksdofmgs');
        attachment2.Body = Blob.toPdf('Test Attachment for Parent');
        database.insert(attachment2);   
        
        Attachment attachmentSelect2 = new Attachment();                          
        attachmentSelect2 = [ Select BodyLength From Attachment where ParentId = :attDocument2.Id limit 1];
        
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachmentSelect2);
        system.debug('CFCBillboardControllerTest.myUnitTest.attachment >>> ' + attachmentSelect2.BodyLength);
        
        attachment2.Name = 'Update Test Attachment';
        update attachment2;
        
        delete attachment2;
        
        undelete attachment2;
        
        AttachmentTriggerHandler handler = new AttachmentTriggerHandler(true);                        	
        System.AssertEquals(handler.IsTriggerContext,true);
     	
     }
}