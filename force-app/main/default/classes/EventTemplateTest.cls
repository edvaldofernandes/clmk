@isTest
public class EventTemplateTest {
    
    @isTest static void eventTest(){
        
        Test.startTest();
        BaseOnEvents__c base = new BaseOnEvents__c();
		base.sender__c = 'S_FORCE_COMERCIAL';
		base.receiver__c = 'RCP';
		base.BaseOn__c = 'SF_TO_RCP_TEAM_MEMBER';
		base.application__c = 'RCP_TEAM_MEMBER';
		INSERT base;
        
        EventTemplate event = new EventTemplate();
        
        BaseOnEvents__c baseEvents = event.buildInfoEvent('RCP_TEAM_MEMBER');
        
        event.buildInfoEvent('Received', 'RCP_TEAM_MEMBER');
        
        Queue__c queued = [SELECT sender__c  FROM Queue__c WHERE sender__c = 'S_FORCE_COMERCIAL'];
        
        System.assertEquals(baseEvents.BaseOn__c, 'SF_TO_RCP_TEAM_MEMBER');
        
        System.assertEquals(queued.sender__c, 'S_FORCE_COMERCIAL');
        
        BaseOnEvents__c baseListObj = new BaseOnEvents__c();
		baseListObj.sender__c = 'SF_COMERCIAL';
		baseListObj.receiver__c = 'ETRACK';
		baseListObj.BaseOn__c = 'SYNC_ETRACK_OUT_BOUND';
		baseListObj.application__c = 'ETRACK__c';
		INSERT baseListObj;
        
        List<ETRACK__c> etrackObjList = new List<ETRACK__c>();
        ETRACK__c etrack = new ETRACK__c();
        etrack.ata__c = '12345';
        etrack.doa__c = true;
        etrackObjList.add(etrack);
        
        BaseOnEvents__c baseEventsList = event.buildInfoEvent(etrackObjList);
        
        event.buildInfoEvent((List<ETRACK__c>)etrackObjList, EventQueueStatusType.SUCCESS.name(), 'NO ERROR');
        
        Queue__c queuedList = [SELECT sender__c  FROM Queue__c WHERE sender__c = 'SF_COMERCIAL'];
        
        System.assertEquals(queuedList.sender__c, 'SF_COMERCIAL');
        
        Test.stopTest();
    }
}