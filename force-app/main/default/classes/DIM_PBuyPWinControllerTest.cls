// Used by DIM - Market Intelligence.
@isTest
public class DIM_PBuyPWinControllerTest {

    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void validateOfficeQueryWithValidOption() {
        String query = DIM_PBuyPWinController.getOfficeQuery('\'Office\'');
        System.assert(query.indexOf('AND Account.Embraer_Site__c IN') != 0);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateRSMQueryWithValidOption() {
        String query = DIM_PBuyPWinController.getRSMQuery('\'RSM\'');
        System.assert(query.indexOf('AND OwnerId IN') != 0);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateAccountShortName() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.RecordTypeId = '012i0000001IyKG';
        opportunity.Name = 'Test Opp';
        opportunity.StageName = 'Business Development';
        opportunity.CloseDate = System.today();
        insert opportunity;
        
        opportunity = [SELECT Id, Account.Name, Account.Company_Nickname__c, Account.DIM_Nickname__c FROM Opportunity WHERE Id = :opportunity.Id];
        System.assertEquals('Test', DIM_PBuyPWinController.getAccountShortName(opportunity));
        
        account.DIM_Nickname__c = 'T';
        update account;
        opportunity = [SELECT Id, Account.Name, Account.Company_Nickname__c, Account.DIM_Nickname__c FROM Opportunity WHERE Id = :opportunity.Id];
        System.assertEquals('T', DIM_PBuyPWinController.getAccountShortName(opportunity));
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateRSMList() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.RecordTypeId = '012i0000001IyKG';
        opportunity1.Name = '10 Aircraft';
        opportunity1.StageName = 'Business Development';
        opportunity1.CloseDate = System.today();
        insert opportunity1;
        
        Opportunity opportunity2 = new Opportunity();
        opportunity2.AccountId = account.Id;
        opportunity2.RecordTypeId = '012i0000001IyKG';
        opportunity2.Name = '20 Aircraft';
        opportunity2.StageName = 'Business Development';
        opportunity2.CloseDate = System.today();
        insert opportunity2;
        
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        List<SelectOption> rsms = controller.getRSMs();
        System.assertEquals(2, rsms.size()); // Incluindo o --All--.
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateRemoteActionToGetBubbles() {
         
        Account account = new Account();
        account.Name = 'Test Air';
        account.Company_Nickname__c = 'Test';
        insert account;
        
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.RecordTypeId = '012i0000001IyKG';
        opportunity1.Name = '10 Aircraft';
        opportunity1.StageName = 'Business Development';
        opportunity1.CloseDate = System.today();
        insert opportunity1;
        
        Opportunity opportunity2 = new Opportunity();
        opportunity2.AccountId = account.Id;
        opportunity2.RecordTypeId = '012i0000001IyKG';
        opportunity2.Name = '20 Aircraft';
        opportunity2.StageName = 'Business Development';
        opportunity2.CloseDate = System.today();
        insert opportunity2;
        
        PBuy__c pBuy1 = new PBuy__c();
        pBuy1.Account__c = account.Id;
        pBuy1.Date__c = Date.today();
        insert pBuy1;
        
        PBuy__c pBuy2 = new PBuy__c();
        pBuy2.Account__c = account.Id;
        pBuy2.Date__c = Date.today();
        insert pBuy2;
        
        PWin__c pWin = new PWin__c();
        pWin.Opportunity__c = opportunity1.Id;
        pWin.Date__c = Date.today();
        insert pWin;
        
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        
        List<Object> bubbles = DIM_PBuyPWinController.getBubbles(controller.office, controller.rsm, controller.type, controller.period);
        System.assertEquals(2, bubbles.size());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateDayOfMonthSuffix() {
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        System.assertEquals('st', controller.getDayOfMonthSuffix(1));
        System.assertEquals('nd', controller.getDayOfMonthSuffix(22));
        System.assertEquals('rd', controller.getDayOfMonthSuffix(33));
        System.assertEquals('th', controller.getDayOfMonthSuffix(12));
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateIfImageExists() {
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        String urlDefault = '/resource/DIM_PBuy_PWin/Icons/PWin_Office_All.png';
        String urlReturn = controller.getFinalURL('/resource/DIM_PBuy_PWin/Icons/PWin_Office_NONE.png', urlDefault);
        System.assertEquals(urlDefault, urlReturn);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateOfficeImage() {
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        controller.officeLabel = 'NONE'; 
        System.assertEquals('/resource/DIM_PBuy_PWin/Icons/PWin_Office_All.png', controller.getOfficeImage());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateRSMImage() {
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        controller.rsmLabel = ''; 
        controller.rsm = '';
        System.assertEquals('/profilephoto/005/T', controller.getRSMImage());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateTypeImage() {
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        controller.officeLabel = 'NONE'; 
        System.assertEquals('/resource/DIM_PBuy_PWin/Icons/PWin_Type_All.png', controller.getTypeImage());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateNumberOfPeriodOptions() {
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        System.assertEquals(10, controller.getPeriods().size());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void validateNewLog() {
    
        PageReference page = Page.DIM_PBuy_PWin_Page;
        Test.setCurrentPage(page);
        ApexPages.currentPage().getParameters().put('isMobile', 'TRUE');
        
        /*ApexPages.StandardController controller = new ApexPages.StandardController(Opp);
        AssociateECPController ac = new AssociateECPController(sc);
        
        String id = ApexPages.currentPage().getParameters().get('id');
        System.assert('true',id==null);*/
    
    
    
        DIM_PBuyPWinController controller = new DIM_PBuyPWinController();
        controller.addLog();
        List<DIM_Audit__c> logs = [SELECT Id FROM DIM_Audit__c];
        System.assertEquals(1, logs.size());
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
}