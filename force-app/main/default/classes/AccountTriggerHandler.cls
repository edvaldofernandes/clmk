public without sharing class AccountTriggerHandler
{

    
    public Map<Id,Account> newRecordsMap = new Map<Id,Account>();
    public Map<Id,Account> oldRecordsMap = new Map<Id,Account>(); 
    public List<Account> newRecords = new List<Account>();
    public List<Account> oldRecords = new List<Account>();
    public boolean isInsert = false;
    public boolean isUpdate = false;
    public boolean isUndelete = false;
    public boolean isDelete = false;
    public boolean isBefore = false;
    public boolean isAfter = false;
    private boolean isExecuting = false;
     
    public AccountTriggerHandler(boolean isExecuting)
    {
        this.isExecuting = isExecuting;
    }

 

    public void OnBeforeInsert()
    {

        validateFlyEmbraerFieldsChange();

    }

 

    public void OnAfterInsert()
    {


    }

 

    public void OnBeforeUpdate()
    {
        validateFlyEmbraerFieldsChange();
    }

 

    public void OnAfterUpdate()
    {

        

    }

 

    public void OnBeforeDelete()
    {

        // BEFORE DELETE LOGIC

    }

 

    public void OnAfterDelete()
    {

        // AFTER DELETE LOGIC

    }

 

    public void OnUndelete()
    {

        // AFTER UNDELETE LOGIC

    }

 

    public boolean IsTriggerContext
    {
        get{ return isExecuting;}
    }


    private void validateFlyEmbraerFieldsChange()
    {
    
        //Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eSolutions').getRecordTypeId();
        Set<Id> idUsersEsolutionsGroup = utils.getUserIdsFromGroup('eSolutions_ADMIN');
        boolean usereSolutionsGroup = idUsersEsolutionsGroup.contains(UserInfo.getUserId());
        
        
        for(Account acc : this.newRecords)
        {
            if(!usereSolutionsGroup)
            {
                if(this.isInsert)
                {
                    if(acc.FlyEmbraer_Access_Status__c != null || acc.FlyEmbraer_Account_Type__c != null || acc.FlyEmbraer_Name__c != null)
                        acc.addError('You can´t edit FlyEmbraer fields unless you´re in the eSolutions public group.');
                }
                else
                {
                    if(acc.FlyEmbraer_Access_Status__c != this.oldRecordsMap.get(acc.Id).FlyEmbraer_Access_Status__c || acc.FlyEmbraer_Account_Type__c != this.oldRecordsMap.get(acc.Id).FlyEmbraer_Account_Type__c || acc.FlyEmbraer_Name__c != this.oldRecordsMap.get(acc.Id).FlyEmbraer_Name__c)
                        acc.addError('You can´t edit FlyEmbraer fields unless you´re in the eSolutions public group.');
                }
            } 
        }
        
    }
   





}