public class AircraftHistoryInformation {
    
    private DateTime createdDate;
    private String field;
    private String OldValue;
    private String newValue;
    private String createdBy;
    
    public AircraftHistoryInformation(DateTime createdDate, String field, String OldValue, String newValue, String createdBy){
        
        this.createdDate = createdDate;
        this.field = field;
        this.OldValue = OldValue;
        this.newValue = newValue;
        this.createdBy = createdBy;
    }
    
    public Date getCreatedDate(){
        return Date.newInstance(this.createdDate.year(), 
                                this.createdDate.month(), 
                                this.createdDate.day()); 
    }
    
    public String getField(){
        return this.field;
    }
    
    public String getOldValue(){
        return this.OldValue;
    }
    
    public String getNewValue(){
        return this.newValue;
    }
    
    public String getCreatedBy(){
        return this.createdBy;
    }
}