//update 1 : SuppliedEmail no novo case a ser criado
global class FO_CaseDisclaimerClass {
    
       
    webservice static void CaseDisclaimerClassMethod(String subj,String caseId, String EMId){
          
        EmailMessage emAux = new EmailMessage();
        List<Attachment> attsAux = new List<Attachment>();
        Case caseAux = new Case();
        //Map<Id,Id> Mid = new Map<Id,Id>();

        if ( (caseId.equals('')==false)&&(EMId.equals('')==false) ) { 
        
            //---------------------Case--------------------
            caseAux = [SELECT Id,AccountId,OwnerId,RecordTypeId,Subject,E170__c,ERJ__c,Turboprops__c,SuppliedEmail FROM Case WHERE Id =: caseId ];
        
            Case caseToCreate = new Case(Subject=subj,RecordTypeId=caseAux.RecordTypeId,AccountId=caseAux.AccountId,OwnerId=caseAux.OwnerId,SuppliedEmail=caseAux.SuppliedEmail);
            Database.insert(caseToCreate);
            if (caseAux.E170__c==True){caseToCreate.E170__c=True;}
            if (caseAux.ERJ__c==True){caseToCreate.ERJ__c=True;}
            if (caseAux.Turboprops__c==True){caseToCreate.Turboprops__c=True;}
        
            //--------------------Email Message------------       
            emAux = [SELECT Id,ActivityId,BccAddress,CcAddress,IsDeleted,FromAddress,FromName,HtmlBody,
                     HasAttachment,Headers,Incoming,ParentId,ReplyToEmailMessageId,Status,Subject,TextBody,
                     ToAddress FROM EmailMessage WHERE Id =: EMId ];
            
            EmailMessage emToCreate = new EmailMessage(ActivityId=emAux.ActivityId,BccAddress=emAux.BccAddress,
                                               CcAddress=emAux.CcAddress,FromAddress=emAux.FromAddress,
                                               FromName=emAux.FromName,HtmlBody=emAux.HtmlBody,
                                               Headers=emAux.Headers,Incoming=emAux.Incoming,ParentId=caseToCreate.Id,
                                               ReplyToEmailMessageId=emAux.ReplyToEmailMessageId,
                                               Status=emAux.Status,Subject=emAux.Subject,TextBody=emAux.TextBody,ToAddress=emAux.ToAddress);     
            Database.insert(emToCreate);
            
            //atualiza description do case com text body do email
            caseToCreate.Description = emAux.TextBody;
            Database.update(caseToCreate);
                  
            //---------------------Attachments--------------
            if (emAux.HasAttachment==true){
                
                
                attsAux = [SELECT Id,Body,BodyLength,ContentType,Description,IsPrivate,Name,
                                     OwnerId,ParentId FROM Attachment WHERE ParentId =: emAux.Id];
                
                if (attsAux!=null){
                    
                    for (Attachment at : attsAux){
                        Attachment attToCreate = new Attachment(Body=at.Body,ContentType=at.ContentType,
                                                Description=at.Description,IsPrivate=at.IsPrivate,Name=at.Name,
                                                OwnerId=at.OwnerId,ParentId=emToCreate.Id);
                        Database.insert(attToCreate);
                    }
                    Database.delete(attsAux);
                    
                }
                
            }
            
            Database.delete(emAux);
        }
        
        
       // PageReference returnPage = new PageReference(apexpages.currentPage().getheaders().get('Referer')); 
    	//returnPage.setRedirect(true);
        
    }
}