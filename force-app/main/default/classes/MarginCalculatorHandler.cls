/* Class that handles Margin Calcultor updates. It calculates gross margins based on values from Product Cost Control and Opportunity Products
* @author - Mateus Caviglione
*           mateus.caviglione@embraer.com.br
* @version 0.1 11/08/2019
* Delete
*/
public class MarginCalculatorHandler {
    
    private static String aicraftModRecordType = 'Aircraft Modification';
    /* This method creates an active Product Margin Calculator of the product with ID <productId> in the opportunity <oppId>
    * REC and NREC cost used are from Product Cost Control object and the margins are calculated.
    * Update opportunity margin after calculating margins*/
    public void calculateProductMarginDB(Id productId, Id oppId){
        List<Product_Cost_Control__c> productCCData = new List<Product_Cost_Control__c>();
        
        productCCData = [ SELECT Amortization_Cost_per_a_c__c, Tech_Pubs_SB_Revision_Costs__c, Sales_Deduction__c, NREC__c, REC__c
                         FROM Product_Cost_Control__c
                         WHERE Related_Product__c = :productId
                         order by CreatedDate desc Limit 1];
        
        List<OpportunityLineItem> productList = new List<OpportunityLineItem>();
        // Gets REC and NREC Opportunity Products
        productList = [ SELECT Id, Product2Id, Quantity, UnitPrice, Entry_fee__c, Princing__c, OpportunityId, CreatedById
                       FROM OpportunityLineItem
                       WHERE Product2Id = :productId AND OpportunityId = : oppId AND
                      		 Product_record_type__c = :aicraftModRecordType];
        
        if(productList.size() == 0){return;}
        
        //Instantiate variables where the data will be loaded into to be calculated
        double recTotalPoP = 0;
        double nrecTotalPoP = 0;
        double recPoP = 0;
        double recCost = 0;
        double nrecTotalCost = 0;
        double nrecCost = 0 ;
        double salesDeduction = 0;
        double recTotalCost = 0;
        double techPubsRev = 0;
        double salesDeductionPercent = 0;
        double QTYacREC = 0, QTYacNREC = 0;
        //Product cost control flag, indicates if the product has PCC
        boolean PCCFlag = false;
        
        /* 
        * Procedure if there is a Product Cost Control associated with product
        * Loads information from SOQL queries into the variables
        */
        If (productCCData.size() > 0){
            PCCFlag = true;
            if(productCCData[0].NREC__c != null){
                nrecCost = productCCData[0].NREC__c;
            }
            if(productCCData[0].REC__c != null){
                recCost = productCCData[0].REC__c;
            }
            if(productCCData[0].Tech_Pubs_SB_Revision_Costs__c != null){
            	techPubsRev = productCCData[0].Tech_Pubs_SB_Revision_Costs__c;
            }
            if(productCCData[0].Sales_Deduction__c != null){
            	salesDeductionPercent = productCCData[0].Sales_Deduction__c;
            }
            for(OpportunityLineItem p : productList){
                //System.debug('P? :'+p);
                
                // Procedure if is NREC
                if(p.Princing__c == 'NREC'){
                    QTYacNREC = p.Quantity;
                    p.Total_amount__c = (p.UnitPrice * QTYacNREC) + p.Entry_fee__c;
                    nrecTotalPoP = p.Total_amount__c;
                    //System.debug('Total Amount__c:'+p.Total_amount__c);
                    salesDeduction = (salesDeductionPercent/100 * p.Total_amount__c);
                    nrecTotalCost = (nrecCost * QTYacNREC) +
                        techPubsRev + salesDeduction; // =B4*5.2%
                    
                    /* DEBUG ===========
                    System.debug('Sales Deduction * Total' + productCCData[0].Sales_Deduction__c/100 * p.Total_amount__c);
                    System.debug('Tech Rev' + productCCData[0].Tech_Pubs_SB_Revision_Costs__c);
                    System.debug('Quant' + p.Quantity);
                    System.debug('Total Cost'+ nrecTotalCost);*/
                    
                }
                // Procedure if is REC
                if(p.Princing__c == 'REC'){
                    QTYacREC = p.Quantity;
                    recPoP = p.UnitPrice;
                    
                    recTotalPoP = QTYacREC * p.UnitPrice;
                    recTotalCost = QTYacREC * recCost;
                    
                    /* DEBUG ===========
                    System.debug('Rec Price Prop' + recPoP);
                    System.debug('Rec Cost' + recCost);
                    System.debug('Rec Total' + recTotalPoP);*/
                }
            }
        }
        
        //Instantiate variables that will be calculated
        double recGrossMargin	= 0;
        double nrecGrossMargin	= 0;
        double totalGrossMargin	= 0;
        
        //If there is cost control
        if(nrecTotalPoP != 0){
            nrecGrossMargin 	= (nrecTotalPoP - nrecTotalCost) / nrecTotalPoP;
            /* DEBUG =========== */
            //System.Debug('NREC Gross Margin:'+ nrecGrossMargin + '%');
        }
        if(recPoP != 0){
            recGrossMargin		= (recPoP - recCost) / recPoP;
            /* DEBUG =========== */
            //System.Debug('REC Gross Margin:'+ recGrossMargin +'%');
        }
        // Procedure if there is REC and NREC oppProduct associated with a product in the opportunity
        if(nrecGrossMargin != 0 && recGrossMargin != 0){
            totalGrossMargin	= ((nrecTotalPoP * nrecGrossMargin) + (recTotalPoP * recGrossMargin))
                /(nrecTotalPoP + recTotalPoP);
        }
        // Procedure if there is only NREC oppProduct associated with a product in the opportunity
        else if(recGrossMargin == 0){
            totalGrossMargin = nrecGrossMargin;
        }
        // Procedure if there is only REC oppProduct associated with a product in the opportunity
        else if(nrecGrossMargin == 0){
            totalGrossMargin = recGrossMargin;
        }
        
        /* DEBUG =========== */
        //System.Debug('Total Gross Margin:'+ totalGrossMargin +'%');
        
        
        List<Product_Margin_Calculation__c> calcOld = new List<Product_Margin_Calculation__c>();
        calcOld = [
            select 
            Id
            from 
            Product_Margin_Calculation__c 
            Where 
            Related_Opportunity__c = :productList[0].OpportunityId 
            AND 
            Related_Product__c = :productList[0].Product2Id 
            order by CreatedDate desc Limit 1
        ];
        if(calcOld.size() > 0) {calcOld[0].Is_Active__c = false;}
        
        //Opportunity opp = [Select OwnerId, CurrencyIsoCode FROM Opportunity WHERE Id = :productList[0].OpportunityId limit 1];
        
        //Creates the Product Margin Calculator object associated to the opp Product
        Product_Margin_Calculation__c calc = new Product_Margin_Calculation__c();
        calc.Related_Opportunity__c = productList[0].OpportunityId;
        calc.Related_Product__c = productList[0].Product2Id;
        calc.CreatedById = productList[0].CreatedById;
        //calc.OwnerId = opp.OwnerId;
        //calc.CurrencyIsoCode = opp.CurrencyIsoCode;
        
        calc.Total_Sales_Price_NREC__c = nrecTotalPoP;
        calc.Total_Cost_Price_NREC__c = nrecTotalCost;
        calc.Cost_Price_per_Aircraft_NREC__c = nrecCost;
        calc.Tech_Pubs_SB_Revision_cost__c = techPubsRev;
        calc.QTY_Aircraft_for_NREC__c = QTYacNREC;
        calc.Sales_Deduction_Percent__c = salesDeductionPercent;
        calc.Sales_Deduction_Total__c = salesDeduction;
        calc.Gross_Margin_for_NREC__c = nrecGrossMargin * 100;
        
        calc.Sales_Price_per_Aircraft_REC__c = recPoP;
        calc.Cost_Price_per_Aircraft_REC__c = recCost;
        calc.QTY_Aircraft_for_REC__c = QTYacREC;
        calc.Total_Proposal_Price_REC__c = recTotalPoP;
        calc.Total_Cost_Price_REC__c = recTotalCost;
        calc.Gross_Margin_for_REC__c = recGrossMargin * 100;
        
        calc.Gross_Margin_for_product__c = totalGrossMargin * 100;
        //Calc.Amortization_Cost_per_a_c__c
        calc.Is_Active__c = true;
        
        //insert the Product Margin Calculator created
        if(calcOld.size() > 0) {update calcOld[0];System.debug(calcOld[0].Is_Active__c);}
        insert calc;
        //System.debug(Calc.Is_Active__c);
        
        // Update Gross Values in the Opportunity Products list
        for(OpportunityLineItem p : productList){
            if(p.Princing__c == 'NREC'){
                p.Gross_Margin__c = nrecGrossMargin * 100;
            }
            if(p.Princing__c == 'REC'){
                p.Gross_Margin__c = recGrossMargin * 100;
            }
            p.Total_Gross_Margin__c = totalGrossMargin * 100;
            p.Has_Product_Cost_Control__c = PCCFlag; 
        }
        // Update Gross Values in the Opportunity Products in the database
        update productList;
        //return the ID for the Product Margin Calculator created
    }
    
    /*Gets the Set of ID from the Products that are associated with the opportunity identified via argument <oppId>*/
    public Set<Id> getIdForProductsFromOpp(Id oppId){
        List<OpportunityLineItem> productList = new List<OpportunityLineItem>();
        // Query to get Product2Id from all Opportunity Products the Opportunity
        productList = [ SELECT Product2Id
                       FROM OpportunityLineItem
                       WHERE OpportunityId = :oppId
                      		];
        
        Set<Id> productsSet = new Set<Id>();
        
        for(OpportunityLineItem o: productList){
            productsSet.add(o.Product2Id);
        }
        return productsSet;
    }
    
    /*This method update the opportunity margins based on the active Calculations*/ 
    public void updateOpportunityMargin(Id oppId){
        
        //Create variables to load data into
        string status = 'Not Validated';
        double oppNRECTotalProposal = 0;
        double oppNRECTotalCost = 0; 
        double oppRECTotalProposal = 0;
        double oppRECTotalCost = 0;
        
        Double oppTotalNRECGrossMargin = 0;
        Double oppTotalRECGrossMargin = 0;
        Double oppTotalGrossMargin = 0;
        if(verifyDataForCalculation(oppId) == 0 || verifyDataForCalculation(oppId) == -3){
            //(oppNRECTotalProposal + oppRECTotalProposal) != 0
            // Query to get info from all the calculators created on last for
            List<Product_Margin_Calculation__c> calcList = new List<Product_Margin_Calculation__c>();
            calcList = [
                select 
                Id, 
                Name, 
                Total_Sales_Price_NREC__c, 
                Total_Cost_Price_NREC__c, 
                Total_Proposal_Price_REC__c, 
                Total_Cost_Price_REC__c, 
                Gross_Margin_for_product__c 
                from 
                Product_Margin_Calculation__c 
                where 
                Related_Opportunity__c =:oppId 
                AND
                Is_Active__c = true
            ];
            
            // Loads data into variables
            for (Product_Margin_Calculation__c c : calcList ){
                //System.Debug('Calc = '+ c);
                oppNRECTotalProposal += c.Total_Sales_Price_NREC__c;
                oppNRECTotalCost += c.Total_Cost_Price_NREC__c;
                oppRECTotalProposal += c.Total_Proposal_Price_REC__c;
                oppRECTotalCost += c.Total_Cost_Price_REC__c;
            }
            // Calculates total margin for the opportunity if it valid
            if(oppNRECTotalProposal != 0){
                oppTotalNRECGrossMargin = (oppNRECTotalProposal - oppNRECTotalCost)* 100 / oppNRECTotalProposal;
                //System.Debug('Opp NREC Total' + oppTotalNRECGrossMargin);
            }
            if(oppRECTotalProposal != 0){
                oppTotalRECGrossMargin = (oppRECTotalProposal - oppRECTotalCost)* 100 / oppRECTotalProposal;
                //System.Debug('Opp REC Total' + oppTotalRECGrossMargin);
            }
            
            if((oppNRECTotalProposal + oppRECTotalProposal) != 0){
            oppTotalGrossMargin = (((oppNRECTotalProposal - oppNRECTotalCost) + (oppRECTotalProposal - oppRECTotalCost)))* 100 / (oppNRECTotalProposal + oppRECTotalProposal);
            }
            status = 'Valid';
            if(verifyDataForCalculation(oppId) == -3){
                status = 'Valid, but there are products that arent aicraft modifications and will not be considered in the gross margin calculation.';
            }
        } else If (verifyDataForCalculation(oppId) == -1){
            // If status is invalid it will reset all margins
            oppTotalNRECGrossMargin = 0;
            oppTotalRECGrossMargin = 0;
            oppTotalGrossMargin = 0;
            status = 'All Products must have a Product Cost Control associated with it. At least one does not have a cost associated to the product.';
        } else if (verifyDataForCalculation(oppId) == -2){
            oppTotalNRECGrossMargin = 0;
            oppTotalRECGrossMargin = 0;
            oppTotalGrossMargin = 0;
            status = 'This opportunity has no products associated with it.';
            
        }
        // Creates opportunity to updates Status and all margins of the opportunity
        Opportunity opp = new Opportunity();
        opp.Id = oppId;
        opp.Calculation_Status__c = status;
        opp.NREC_Gross_Margin__c = oppTotalNRECGrossMargin;
        opp.REC_Gross_Margin__c = oppTotalRECGrossMargin;
        opp.Total_Gross_Margin__c = oppTotalGrossMargin;
        update opp;
    }
    
    /*This method calculates margin for all product of Opportunity from <oppId> 
    * and update the opportunity margins */
    public void calculateOpportunityMargin(Id oppId){
        Set<Id> productSet = new Set<Id>();
        productSet = getIdForProductsFromOpp(oppId);
        //System.debug('Product Id List of Opp'+ oppId + 'is : '+productSet);
        for(Id i : productSet){
            calculateProductMarginDB(i, oppId);
        }
        
        updateOpportunityMargin(oppId);
    }
    
    /* This method verifies if the opportunity is valid so the margin can be calculated
     * If there are no product associated with the opportunity it will return -2
     * If a product dont have Cost Control it will return -1
     * If a product aint from type Aircraft Mod it will return -3
     * If it is valid the method returns 0 */
    public integer verifyDataForCalculation(Id oppId){
        List<OpportunityLineItem> oppProductList = new List<OpportunityLineItem>();
        
        oppProductList = [ select Id, Has_Product_Cost_Control__c, Product_record_type__c
                          from OpportunityLineItem
                          where OpportunityId = :oppId];
        if(oppProductList.Size() == 0){return -2;}
        for (OpportunityLineItem oppP: oppProductList){
            if(oppP.Has_Product_Cost_Control__c == false && oppP.Product_record_type__c == aicraftModRecordType){return -1;}
        }
        for (OpportunityLineItem oppP: oppProductList){
            if(oppP.Product_record_type__c != aicraftModRecordType){return -3;}
        }
        return 0;
    }   
    
    /* This method uses Trigger.New and is called once after the update or inser
     * it Update product margin calculator for all product after changes are made on the DB*/
    public void triggerDataHandler(List<OpportunityLineItem> oppProductList){
        Set<Id> opportunityIdSet = new Set<Id>();
        for(OpportunityLineItem oppProduct : oppProductList){
            //SYstem.debug('Teste for Trigger Ammoutn');
            opportunityIdSet.add(oppProduct.OpportunityId);
        }
        
        for(Id oppId : opportunityIdSet){                
            calculateOpportunityMargin(oppId);
        }
        
    }
    
    /* This method makes the old calculators inactive before the product is deleted*/
    public void triggerBeforeDeleteHandler(List<OpportunityLineItem> oppProductList){
        Set<Product_Margin_Calculation__c> calcList = new Set<Product_Margin_Calculation__c>();
        List<Product_Margin_Calculation__c> calc = new List <Product_Margin_Calculation__c>();
        if(oppProductList.size() > 50){ return;}
        
        /*Name, 
        Total_Sales_Price_NREC__c, 
        Total_Cost_Price_NREC__c, 
        Total_Proposal_Price_REC__c, 
        Total_Cost_Price_REC__c, 
        Gross_Margin_for_product__c*/
        
        for (OpportunityLineItem oppP: oppProductList){
            calc = [
                select
                Id, 
            Name, 
            Total_Sales_Price_NREC__c, 
            Total_Cost_Price_NREC__c, 
            Total_Proposal_Price_REC__c, 
            Total_Cost_Price_REC__c, 
            Gross_Margin_for_product__c
                from 
                Product_Margin_Calculation__c 
                where 
                Related_Opportunity__c =:oppP.OpportunityId
                AND
                Related_Product__c = :oppP.Product2Id
                AND
                Is_Active__c = true
                limit 1
            ];
            if(calc.size() != 0){
                calc[0].Is_Active__c = false;
                calcList.addAll(calc);
            }
        }
        List<Product_Margin_Calculation__c> deleteList = new List<Product_Margin_Calculation__c>();
        deleteList.addAll(calcList);
        update deleteList;
    }
    
    /* This method forces new calculations for all products after the a product is deleted*/
    public void triggerAfterDeleteHandler(List<OpportunityLineItem> oppProductList){
        for (OpportunityLineItem oppP: oppProductList){
            calculateOpportunityMargin(oppP.OpportunityId);
        }
    }
    
    //OLD Functions developed to update each product separately 
    //instead of calculating all product of a opportunity at once
    //They weren't used due to the trigger been inconsistent
    //If just a single product is updated it will trigger for all product
    //and also trigger to product that was updated
    /*public void OLDCalculateProductMargin(List<OpportunityLineItem> oppProductList, Id productId, Id oppId){
        List<OpportunityLineItem> productList = new List<OpportunityLineItem>();
        // Gets REC and NREC Opportunity Products
        //productList = [ SELECT Id, Product2Id, Quantity, UnitPrice, Total_amount__c, Princing__c, OpportunityId, CreatedById
        //                FROM OpportunityLineItem
        //                WHERE Product2Id = :productId AND OpportunityId = : oppId];
        for (OpportunityLineItem oppP : oppProductList){
            if(oppP.Product2Id == productId && oppP.OpportunityId == oppId){
                productList.add(oppP);
            }
        }
        //System.debug('calculateProductMargin productList Size: '+ productList.Size());
        if(productList.Size() == 1){
            List<OpportunityLineItem> addProductForCompleteCalc = new List<OpportunityLineItem>();
            addProductForCompleteCalc = 
                [SELECT 
                 Id,Product2Id,Quantity,UnitPrice,Total_amount__c,Princing__c,OpportunityId,CreatedById
                 FROM 
                 OpportunityLineItem
                 WHERE
                 Product2Id = :productList[0].Product2Id 
                 AND 
                 OpportunityId = : productList[0].OpportunityId
                 AND
                 Princing__c != :productList[0].Princing__c
                ];
            if (addProductForCompleteCalc.size() == 1){
                productList.add(addProductForCompleteCalc[0]);
            }
            
        }
        //System.debug('calculateProductMargin productList Size after SOQL: '+ productList.Size());
        
        List<Product_Cost_Control__c> productCCData = new List<Product_Cost_Control__c>();
        
        productCCData = [ SELECT Tech_Pubs_SB_Revision_Costs__c, Sales_Deduction__c, NREC__c, REC__c
                         FROM Product_Cost_Control__c
                         WHERE Related_Product__c = :productId
                         order by CreatedDate desc Limit 1];
        
        //Instantiate variables where the data will be loaded into to be calculated
        double recTotalPoP = 0;
        double nrecTotalPoP = 0;
        double recPoP = 0;
        double recCost = 0;
        double nrecTotalCost = 0;
        double salesDeduction = 0;
        double recTotalCost = 0;
        double techPubsRev = 0;
        double salesDeductionPercent = 0;
        //Product cost control flag, indicates if the product has PCC
        boolean PCCFlag = false;
        
         
        //Procedure if there is a Product Cost Control associated with product
        //Loads information from SOQL queries into the variables

        If (productCCData.size() > 0){
            PCCFlag = true;
            techPubsRev = productCCData[0].Tech_Pubs_SB_Revision_Costs__c;
            salesDeductionPercent = productCCData[0].Sales_Deduction__c;
            for(OpportunityLineItem p : productList){
                // Procedure if is NREC
                if(p.Princing__c == 'NREC'){
                    if( p.Total_amount__c == null){
                        p.Total_amount__c = p.UnitPrice * p.Quantity;
                    }
                    nrecTotalPoP = p.Total_amount__c;
                    
                    salesDeduction = (salesDeductionPercent/100 * p.Total_amount__c);
                    nrecTotalCost = (productCCData[0].NREC__c * p.Quantity) +
                        techPubsRev + salesDeduction; // =B4*5.2%
                    
                    //DEBUG ===========
                    //System.debug('Sales Deduction * Total' + productCCData[0].Sales_Deduction__c/100 * p.Total_amount__c);
                    //System.debug('Tech Rev' + productCCData[0].Tech_Pubs_SB_Revision_Costs__c);
                    //System.debug('Quant' + p.Quantity);
                    //System.debug('Total Cost'+ nrecTotalCost);
                    
                }
                // Procedure if is REC
                if(p.Princing__c == 'REC'){
                    recPoP = p.UnitPrice;
                    recCost = productCCData[0].REC__c;
                    recTotalPoP = p.Quantity * p.UnitPrice;
                    recTotalCost = p.Quantity * recCost;
                    
                    
                    //DEBUG ===========
                    //System.debug('Rec Price Prop' + recPoP);
                    //System.debug('Rec Cost' + recCost);
                    //System.debug('Rec Total' + recTotalPoP);
                }
            }
        }
        
        //Instantiate variables that will be calculated
        double recGrossMargin	= 0;
        double nrecGrossMargin	= 0;
        double totalGrossMargin	= 0;
        
        //If there is cost control
        if(nrecTotalPoP != 0){
            nrecGrossMargin 	= (nrecTotalPoP - nrecTotalCost) / nrecTotalPoP;
            //DEBUG ===========
            //System.Debug('NREC Gross Margin:'+ nrecGrossMargin + '%');
        }
        if(recPoP != 0){
            recGrossMargin		= (recPoP - recCost) / recPoP;
            //DEBUG =========== 
            //System.Debug('REC Gross Margin:'+ recGrossMargin +'%');
        }
        // Procedure if there is REC and NREC oppProduct associated with a product in the opportunity
        if(nrecGrossMargin != 0 && recGrossMargin != 0){
            totalGrossMargin	= ((nrecTotalPoP * nrecGrossMargin) + (recTotalPoP * recGrossMargin))
                /(nrecTotalPoP + recTotalPoP);
        }
        // Procedure if there is only NREC oppProduct associated with a product in the opportunity
        else if(recGrossMargin == 0){
            totalGrossMargin = nrecGrossMargin;
        }
        // Procedure if there is only REC oppProduct associated with a product in the opportunity
        else if(nrecGrossMargin == 0){
            totalGrossMargin = recGrossMargin;
        }
        
        //DEBUG ===========
        //System.Debug('Total Gross Margin:'+ totalGrossMargin +'%');
        Opportunity opp = [Select OwnerId, CurrencyIsoCode FROM Opportunity WHERE Id = :productList[0].OpportunityId Limit 1];
        
        List<Product_Margin_Calculator__c> calcOld = new List<Product_Margin_Calculator__c>();
        calcOld = [
            select 
            Id
            from 
            Product_Margin_Calculator__c 
            Where 
            Related_Opportunity__c = :productList[0].OpportunityId 
            AND 
            Related_Product__c = :productList[0].Product2Id 
            order by CreatedDate desc Limit 1
        ];
        if(calcOld.size() > 0) {calcOld[0].Is_Active__c = false;}
        
        //Creates the Product Margin Calculator object associated to the opp Product
        Product_Margin_Calculator__c calc = new Product_Margin_Calculator__c();
        calc.Related_Opportunity__c = productList[0].OpportunityId;
        calc.Related_Product__c = productList[0].Product2Id;
        calc.CreatedById = productList[0].CreatedById;
        calc.OwnerId = opp.OwnerId;
        calc.CurrencyIsoCode = opp.CurrencyIsoCode;
        calc.QTY_of_a_c__c = productList[0].Quantity;
        
        calc.Sales_Price_NREC__c = nrecTotalPoP;
        calc.Total_Costs_NREC2__c = nrecTotalCost;
        calc.Related_Tech_Pubs_SB_Revision_costs__c = techPubsRev;
        calc.Related_Sales_Deduction_Porc__c = salesDeductionPercent;
        calc.NREC_Gross_Margin__c = nrecGrossMargin * 100;
        
        calc.Sales_Price_REC__c = recPoP;
        calc.Product_Cost_REC__c = recCost;
        calc.REC_Total_Proposal__c = recTotalPoP;
        calc.REC_Total_Cost__c = recTotalCost;
        calc.Gross_Margin_REC__c = recGrossMargin * 100;
        
        calc.Gross_Margin_Total__c = totalGrossMargin * 100;
        //Calc.Amortization_Cost_per_a_c__c
        calc.Sales_Deduction__c = salesDeduction;
        calc.Is_Active__c = true;
        
        //insert the Product Margin Calculator created
        if(calcOld.size() > 0) {update calcOld[0];}
        insert calc;
        System.debug(Calc.Is_Active__c);
        
        // Update Gross Values in the Opportunity Products list
        for(OpportunityLineItem p : productList){
            if(p.Princing__c == 'NREC'){
                p.Gross_Margin__c = nrecGrossMargin * 100;
            }
            if(p.Princing__c == 'REC'){
                p.Gross_Margin__c = recGrossMargin * 100;
            }
            p.Total_Gross_Margin__c = totalGrossMargin * 100;
            p.Has_Product_Cost_Control__c = PCCFlag; 
        }
        // Update Gross Values in the Opportunity Products in the database
        //update productList;
        //return the ID for the Product Margin Calculator created
    }
    public void OLDCalculateOpportunityMargin(List<OpportunityLineItem> oppProductList, Id oppId){
        Set<Id> productSet = new Set<Id>();
        productSet = getIdForProductsFromOpp(oppId);
        
        for(Id i : productSet){
            OLDCalculateProductMargin(oppProductList, i, oppId);
        }
        
        updateOpportunityMargin(oppId);
    }
    public void OLDTriggerDataHandler(List<OpportunityLineItem> oppProductList){
        Map<Id, Set<Id>> opportunityToSetOfProductsMap = new Map<Id, Set<Id>>();
        for(OpportunityLineItem oppProduct : oppProductList){
            SYstem.debug('Teste for Trigger Ammoutn');
            Set<Id> tempProductSet = opportunityToSetOfProductsMap.get(oppProduct.OpportunityId);
            if(tempProductSet == null){
                tempProductSet = new Set<Id>();
                opportunityToSetOfProductsMap.put(oppProduct.OpportunityId, tempProductSet);
            }
            if(!tempProductSet.contains(oppProduct.Product2Id)){
                tempProductSet.add(oppProduct.Product2Id);
            }
        }
        System.Debug('Opp Map From trigger data handler: =====> :' + opportunityToSetOfProductsMap);
        
        for(Id key : opportunityToSetOfProductsMap.keySet()){
            Set<ID> productSet = opportunityToSetOfProductsMap.get(key);
            System.Debug('Product Set From trigger data handler: =====> :' + productSet);
            
            for(Id prodId : productSet){
                
                OLDCalculateProductMargin(oppProductList, prodId , key);
            }
            updateOpportunityMargin(key);
        }
        //opportunityToSetOfProductsMap.put(oppProduct.Product2Id, tempProductSet);
        //tempProductSet.clear();
        
        // calculateProductMargin(oppProductList, oppProduct.Product2Id , oppProduct.OpportunityId);
        //       updateOpportunityMargin(oppProduct.OpportunityId);
        
    }
    */
}