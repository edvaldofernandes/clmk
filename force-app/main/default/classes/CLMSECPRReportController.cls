global class CLMSECPRReportController 
{

    global static string filterInitialYear{get;set;}
    public static string selectedYear{get;set;}
    public list<string> seriesNames { get;set;} 
    public boolean canChangeSettings{get;private set;}

    global CLMSECPRReportController()
    {
        selectedYear = string.valueOf(date.today().year());
        canChangeSettings = [Select Id From Profile Where Name = 'System Administrator'].Id == userInfo.getProfileId();
    }

    public PageReference loadChart()
    {
        return null;
    }

    global List<SelectOption> getFilterInitialYears()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        for(integer year =2009;year <= date.today().addYears(10).year();year++ )
           options.add(new SelectOption(string.valueOf(year), string.valueOf(year)));
   
       return options;
    }
    
    public PageReference updateParameters()
    {
        return null;
    }
    
    public PageReference changeSettings()
    {
        return new PageReference('/apex/DCTReportsSettingsInterface?year='+ selectedYear + '&origin=CLMSECPRReport');
    }
    
    global class ChartDataWrapper 
    {
        public String axisName { get;set;}
        public double scoreCumulativeValue { get;set;}
        public integer totalColumns { get;set;}
        public double managerGoalValue { get;set;}
        public decimal seriesValue{get;set;}
        public string seriesAnnotation{get;set;} 

        public chartDataWrapper(string p_AxisName,
                                double p_SeriesValue,
                                double p_ScoreCumulativeValue,
                                double p_ManagerGoalValue,
                                integer p_TotalColumns) 
        {
            integer totalValues = 0;
            decimal roundedValue = 0;
            decimal decimalValue = 0;
            decimal seriesValueDec = 0;
            decimal seriesValueRounded = 0;
            
            decimalValue = (decimal) p_ScoreCumulativeValue;
            roundedValue = decimalValue.setScale(1, RoundingMode.HALF_UP);
            
            this.axisName = p_AxisName;
            this.scoreCumulativeValue = roundedValue ;
            this.totalColumns  = p_TotalColumns;
            this.managerGoalValue = p_ManagerGoalValue;
            seriesAnnotation = '';
            
            seriesValueDec = (decimal)p_SeriesValue;
            seriesValueRounded = seriesValueDec.setScale(1, RoundingMode.HALF_UP);
            seriesAnnotation = seriesValueRounded==0 ? '' : string.valueOf(seriesValueRounded);
            this.seriesValue = seriesValueRounded;
            
        }
    }
    
    @RemoteAction  
    global static List<ChartDataWrapper> loadChartData(string p_SelectedYear) 
    {
        List<ChartDataWrapper> returnValue = new List<ChartDataWrapper>();
        List<integer> listValues; 
        DCTReportsSettings__c goals;
        string seriesName = '';
        double goal = 0;
        integer deliveryMonth;
        double scoreAVG = 0;
        double scoreCumulative = 0;
        double scoreTotal = 0;
        integer monthCount = 1;
        integer rowCount = 0;        
        map<integer,string> mapMonthName = new map<integer,string>();
        map<integer,double> mapMonthGoal = new map<integer,double>();
        integer parameterYear;
        Map<integer,integer> mapNullRecords = new Map<integer,integer>();
        
        mapMonthName.put(1,'JAN');
        mapMonthName.put(2,'FEB');
        mapMonthName.put(3,'MAR');        
        mapMonthName.put(4,'APR');        
        mapMonthName.put(5,'MAY');        
        mapMonthName.put(6,'JUN');
        mapMonthName.put(7,'JUL');
        mapMonthName.put(8,'AUG');
        mapMonthName.put(9,'SEP');
        mapMonthName.put(10,'OCT');
        mapMonthName.put(11,'NOV');                                            
        mapMonthName.put(12,'DEC');
        
        list<integer> listSeriesValues;
        
        Map<string,list<integer>> mapYearSeriesValues = new Map<string,list<integer>>() ;
        
        parameterYear = integer.valueOf(p_SelectedYear);
        
        selectedYear = p_SelectedYear;
        System.Debug(selectedYear);
        
        goals = DCTReportsSettings__c.getInstance(string.ValueOf(p_SelectedYear));
        if(goals != null) 
        {
            mapMonthGoal.put(1,goals.JanuaryGoalSECPRod__c);
            mapMonthGoal.put(2,goals.FebruaryGoalSECPRod__c);
            mapMonthGoal.put(3,goals.MarchGoalSECPRod__c);
            mapMonthGoal.put(4,goals.AprilGoalSECPRod__c);
            mapMonthGoal.put(5,goals.MayGoalSECPRod__c);
            mapMonthGoal.put(6,goals.JuneGoalSECPRod__c);
            mapMonthGoal.put(7,goals.JulyGoalSECPRod__c);
            mapMonthGoal.put(8,goals.AugustGoalSECPRod__c);
            mapMonthGoal.put(9,goals.SeptemberGoalSECPRod__c);
            mapMonthGoal.put(10,goals.OctoberGoalSECPRod__c);
            mapMonthGoal.put(11,goals.NovemberGoalSECPRod__c);
            mapMonthGoal.put(12,goals.DecemberGoalSECPRod__c);
        }  

        for(AggregateResult result : [SELECT CALENDAR_MONTH(AFA__c) deliveryMonth,
                                      		 AVG(Production_SEC_Score__c) scoreAVG,
                                      		 SUM(Production_SEC_Score__c) scoreTotal,
                                      		 Count(ID) totalRows 
                                      FROM Agreement_Aircraft__c
                                      WHERE Production_SEC_Score__c <> Null  
                                      AND Agreement__r.Status_Category__c = 'Pa Signed' 
                                      AND Calendar_Year(AFA__c ) >= : parameterYear 
                                      And Calendar_Year(AFA__c ) <= : parameterYear 
                                      Group By CALENDAR_MONTH(AFA__c) 
                                      Order By CALENDAR_MONTH(AFA__c)])
        {  
            goal = 0;
            deliveryMonth = integer.ValueOf(result.get('deliveryMonth'));
            seriesName = mapMonthName.get(deliveryMonth) + '/' + p_SelectedYear;
            scoreAVG = double.ValueOf(result.get('scoreAVG'));
            scoreTotal = double.ValueOf(result.get('scoreTotal'));
            scoreCumulative += scoreTotal;
            rowCount += integer.ValueOf(result.get('totalRows'));

            if(mapMonthGoal.get(deliveryMonth) != Null)
                goal = mapMonthGoal.get(deliveryMonth);

            returnValue.add(new ChartDataWrapper(seriesName,scoreAVG,scoreCumulative/rowCount,goal,1));        
            monthCount++;
        
        }
        return returnValue ;
    }
}