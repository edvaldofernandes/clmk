public class CLMPromoteToPAController {
    public boolean isHide {get;set;}
    public boolean isAmendmentReady {get;set;}
    public String newAmendmentId {get;set;}
    //public Agreement__c agreement { get; set; }
    
    public CLMPromoteToPAController(ApexPages.StandardController controller){
        isHide = true;
        isAmendmentReady = false;
        newAmendmentId = '';
    }
    
    public PageReference promote(){
        
        String agreementId = ApexPages.currentPage().getParameters().get('id');
        
        if (agreementId==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ID not found'));
            return null;
        }
        
        agreementId = CLMCloneAgreement.cloneAgreement(agreementId, 'Promote');
        
        if(agreementId=='Already Promoted'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'You have already promoted this record'));
            return null;
        }
        if(agreementId=='Not Approved or Signed Yet'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'The proposal must be approved or signed.'));
            return null;
        }
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your promotion to PA request is being processed. It can take a few seconds.'));
        isHide = !isHide;
        return null;
    }

    public PageReference promotionCompleted(){
        if(isAmendmentReady){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your amendment is ready!'));
            return null;
        }
        Agreement__c agr = [SELECT name FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(agr == null) return null;
        List<Agreement__c> agrList = [SELECT id FROM Agreement__c WHERE name = :(agr.name + + ' PA') LIMIT 1];
        if(agrList.isEmpty()){
            if(!isHide)
		    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your promotion to PA request is being processed. It can take a few seconds.'));
            return null;
        }
        newAmendmentId = String.valueOf(agrList.get(0).id);
        //newAmendmentId = String.valueOf([SELECT id FROM Agreement__c WHERE name = :([SELECT name FROM Agreement__c WHERE id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1].name + + ' Amendment') LIMIT 1].get(0).id);
        if(newAmendmentId!=''){
            isAmendmentReady = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your PA is ready!'));
        }
        return null;
    }

    public PageReference goToNewPA(){
        PageReference returnPage = new PageReference('/'+ newAmendmentId);  
        returnPage.setRedirect(true);
        return returnPage;
    }
}