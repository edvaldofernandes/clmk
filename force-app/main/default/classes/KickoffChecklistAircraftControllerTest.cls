@isTest
public class KickoffChecklistAircraftControllerTest {
    @testSetup static void methodName() {
		Opportunity opp = new Opportunity();
        opp.Name = 'Opp 1';
        opp.Fleet_Type__c = 'Turboprop';
        opp.StageName = 'Validation_of_approvals';
        opp.CloseDate = system.today();
        insert opp;
        Kickoff__c ko = new Kickoff__c();
        ko.Name = 'Checklist 1';
        ko.Opportunity__c = opp.Id;
        ko.Customer_Type__c = 'New Customer';
        ko.submitted__c = true;
        ko.Kick_Off_Meeting_Date__c = system.today();
        insert ko;
        List<Kickoff_Aircraft__c> koAList = new List<Kickoff_Aircraft__c>();
        Kickoff_Aircraft__c koA;
        while (koAList.size()<4){
			koA = new Kickoff_Aircraft__c();
            koA.Kickoff__c = ko.Id;
            koa.Firm__c = koAList.size();
            koA.Info_Type__c = 'Order Size';
            koAList.add(koA);
        }
        while (koAList.size()<7){
			koA = new Kickoff_Aircraft__c();
            koA.Kickoff__c = ko.Id;
            koa.Year0Quantity__c = koAList.size();
            koA.Info_Type__c = 'Delivery Schedule';
            koAList.add(koA);
        }
        insert koAList;
    }
    @isTest static void listTest(){
        Id koId = [SELECT Id from Kickoff__c LIMIT 1].Id;
        System.assert([SELECT Count() FROM Kickoff_Aircraft__c]==7);
        List<Kickoff_Aircraft__c> koAircraftOrderSizeItemsList = KickoffChecklistAircraftApexController.getOrderSizeItems(koId);
        System.assert(koAircraftOrderSizeItemsList.size()==4);
        System.assert(KickoffChecklistAircraftApexController.getScheduleItems(koId).size()==3);
        Kickoff__c ko = new Kickoff__c();
        ko.Id = koId;
        ko.submitted__c = false;
        update ko;
        for(Kickoff_Aircraft__c ka : koAircraftOrderSizeItemsList){
            ka.Firm__c = 55;
        }
        update koAircraftOrderSizeItemsList;
        KickoffChecklistAircraftApexController.saveEdits(koAircraftOrderSizeItemsList);
        System.assert([SELECT Count() FROM Kickoff_Aircraft__c WHERE Info_Type__c = 'Order Size' AND Firm__c = 55]==4);
    }
    
    @isTest static void yearTest(){
        Kickoff__c ko = [SELECT Id, Kick_Off_Meeting_Date__c from Kickoff__c LIMIT 1];
        System.assert(ko.Kick_Off_Meeting_Date__c.Year() == KickoffChecklistAircraftApexController.getYear(ko.Id));
    }
    @isTest static void coverTryCatch(){
        KickoffChecklistAircraftApexController.getYear(null);
        KickoffChecklistAircraftApexController.saveEdits(null);
    }
}