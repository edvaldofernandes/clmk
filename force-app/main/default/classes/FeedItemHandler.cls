public with sharing class FeedItemHandler {
    
    @TestVisible
    @future( callout = true )
    private static void callWS( String url , String authorization , String body ){
    	
    	Http h = new Http();
        
	    HttpRequest req = new HttpRequest();
    	//req.setEndpoint('https://ogwdev.embraer.com.br:443/CPS/CPSReceiveFeedback/proxyServices/CPSReceiveFeedbackPS');
    	req.setHeader('Content-Type', 'text/xml;charset=utf-8');
    	req.setEndpoint(url);
	    req.setMethod('POST');
	    
	    System.debug(body);
	    
        req.setBody(body);
        req.setHeader('Authorization', authorization);

	    HttpResponse res = h.send(req);

    	System.debug(res.getBody());
    }
    
    @TestVisible
    private static string buildBody( FeedItem item ) {
    	
    	String baseUrl = System.URL.getSalesforceBaseUrl().getHost();
		//String attach = '<com:attachment>https://' + baseUrl + '/sfc/servlet.shepherd/version/download/{0}</com:attachment>';
    	String attach = '';
    	
    	if( item.HasContent ) {
    		
    		String url = [SELECT DistributionPublicUrl FROM ContentDistribution WHERE ContentVersionId = :item.RelatedRecordId].get(0).DistributionPublicUrl;
    		attach = '<com:attachment>'+ url +'</com:attachment>';
			//attach = String.format( attach , new String[] { item.RelatedRecordId });
    	}
    	else {
			attach = '<com:attachment/>';
    	}
    	
    	String groupId = String.valueOf(item.ParentId).substring(0, 15);
    	
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.example.org/compass/">'+
   					'<soapenv:Header/>'+
   					'<soapenv:Body>'+
			      		'<com:receiveInformation>'+
			         		'<com:CPSInformation>'+
			            		'<com:groupId>'+ groupId +'</com:groupId>'+
			            		'<com:id>'+ item.Id +'</com:id>'+
			            		'<com:author>'+ item.CreatedBy.Name +'</com:author>'+
			            		'<com:publishDate>'+ Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'America/Sao_Paulo') +'</com:publishDate>'+
			            		'<com:description>'+ item.Body +'</com:description>'+
			            		attach+
			         		'</com:CPSInformation>'+
			      		'</com:receiveInformation>'+
			   		'</soapenv:Body>'+
				'</soapenv:Envelope>';
				
		System.debug(body);
		return body;
    }
	
	public static void onInsert( String feedsJsonIds ){

        Set<Id> itemsIds = ( Set<Id> ) JSON.deserialize( feedsJsonIds , Set<Id>.class );
        
        /*
		* If FeedItem is created in Group: QMI - Quick Market Intelligence
		* then call Compass Application WS
		*/
        
        List<FeedItem> collabFeeds = [SELECT Id, Body, ParentId, CreatedBy.Name, HasContent, RelatedRecordId
										FROM FeedItem
										WHERE ParentId = :[

											SELECT Id
											FROM CollaborationGroup
											WHERE Name = 'QMI - Quick Market Intelligence'
											LIMIT 1
										]
										AND Id IN :itemsIds
										AND CreatedById != :[
								
											SELECT Id
											FROM User
											//WHERE Username = 'integration@00dp000000036ctmai.com'
											WHERE Name = 'CRM VPC Integration'
											LIMIT 1
										]
										];
        

													
		Transaction_Delivery__c td = [ SELECT Name, URL__c, Authorization__c
										FROM Transaction_Delivery__c
										WHERE Name = 'CPSInformation'
										LIMIT 1];
				
		List<ContentDistribution> contentDistributions = new List<ContentDistribution>();				
													
		if(collabFeeds != null && collabFeeds.size() > 0) {

			for( FeedItem collabFeed : collabFeeds ) {
				
				if(collabFeed.HasContent) {
					
					ContentDistribution contentDistribution = new ContentDistribution();
					
					contentDistribution.Name 								= collabFeed.Id;
					contentDistribution.ContentVersionId 					= collabFeed.RelatedRecordId;
					contentDistribution.PreferencesAllowOriginalDownload 	= true;
					contentDistribution.PreferencesAllowPDFDownload 		= true;
					contentDistribution.PreferencesAllowViewInBrowser 		= true;
                    
                                System.debug('LIST FEED ITEM DEBUG SUUPORT***...' + collabFeed.Id);

					
					contentDistributions.add(contentDistribution);															
				}				
			}
			
			insert contentDistributions;
			
			for( FeedItem collabFeed : collabFeeds ) {

				
                if( !Test.isRunningTest() ) 
					callWS( td.URL__c , td.Authorization__c , buildBody( collabFeed ) );
                system.debug('Chegou!');
			}
		}
	}		
}