public without sharing class CLMChecklistTaskController
{

    private Questionnaire__c selectedChecklist;
    public boolean showButton{get;set;}
    public string actionToPerform{get;set;}
    public string statusMessage{get;set;}
    private Set<String> objectFields = new Set<String>();
    private map<string,string> checklistFieldsMap;
    private map<string,map<string,string>> picklistValuesMap;
    private CLMTaskSetupUtil tsUtil;
    
    public CLMChecklistTaskController(ApexPages.StandardController controller)
    {
        actionToPerform = ApexPages.currentPage().getParameters().get('actionToPerform');
        statusMessage = 'Deleting Tasks.Please Wait...';
        
        if(actionToPerform == 'createTasks')
        {
            tsUtil = new CLMTaskSetupUtil(controller.getId());
            if(!test.IsRunningTest())
                controller.addFields(populateFieldsList());
            
            statusMessage = 'Creating Tasks.Please Wait...';
        }
        
        selectedChecklist = (Questionnaire__c) controller.getRecord();
        showButton = false;
        checklistFieldsMap = new Map<string,string>();
        picklistValuesMap = new map<string,map<string,string>>();
        
    
    }
    
    public pageReference returnPreviousPage()
    {
        return new PageReference('/' + selectedChecklist.Id);
    }
    
    public pageReference processar()
    {
        if(actionToPerform == 'createTasks')
        {
            if(createTasks())
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Tasks created sucessfully.'));
        }
        else
        {
            if(deleteTasks())
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Tasks deleted sucessfully.'));
        }
        showButton = true;
        return null;
    }
    
    
    public boolean createTasks()
    {

        boolean returnValue = false;  
        List<Task> taskList = new List<Task>();
        List<Task_Setup__c> taskSetupList = new List<Task_Setup__c>();
        Task taskTemp;
        boolean createTask = false;
        Map<String,String> fieldInfo;
        string answerValue = '';
        string questionAPI = '';
        map<string,string> picklistMapTemp;
        
       
        try
        {
            if(objectFields.isEmpty())
                createObjectFieldsSet();
            
           if(validateDateFields())
     
            { 
                //Get all task setup for the selected RecordType
                taskSetupList = TaskSetupDAO.getInstance().getTaskSetupByRecordType(selectedChecklist.RecordType.Name);
                
                for(Task_Setup__c taskSetup : taskSetupList)
                {
                    taskTemp = new Task();
                    createTask = false;
                    
                    //If the question API = All, then it must be created
                    if(taskSetup.Question__c == 'All')
                    {
                        createTask = true;
                    }
                    else 
                    {
                     
                        questionAPI = '';
                        if(checklistFieldsMap.containsKey(taskSetup.Question__c.toLOwerCase()))
                            questionAPI = checklistFieldsMap.get(taskSetup.Question__c.toLOwerCase());
                            
                        if(string.IsNotEmpty(questionAPI) && string.IsNotEmpty(taskSetup.Answer__c))
                        {
                            System.Debug(questionAPI +  '- ' + createTask  +  '- ' + taskSetup.Answer__c.toLowerCase() + ' - ' + answerValue + ' - QuestionAPI Rodrigo');
                            //answerValue = (selectedChecklist.get(questionAPI) == Null ? '' : string.ValueOf(selectedChecklist.get(questionAPI)).toLowerCase());
                            if(selectedChecklist.get(questionAPI) == Null)
                            {
                                answerValue = '';
                            }
                            else
                            {
                                answerValue = string.ValueOf(selectedChecklist.get(questionAPI)).toLowerCase();
                                if(picklistValuesMap.containsKey(taskSetup.Question__c))
                                {
                                    picklistMapTemp = picklistValuesMap.get(taskSetup.Question__c); 
                                    answerValue = picklistMapTemp.get(string.valueof(selectedChecklist.get(questionAPI))).toLowerCase();
                                }
                            }
                            createTask = (taskSetup.Answer__c.toLowerCase() == answerValue) && !String.isEmpty(answerValue);
                            System.Debug(createTask + ' - create Task Rodrigo');
                        }
                                                
                    }
                    
                    if(createTask)
                    {
                        //General information for every task
                        if(selectedChecklist.Commercial_Agreement__c != null) 
                        {
                            taskTemp.WhatId = selectedChecklist.Commercial_Agreement__c;
                        }
                        else
                        {
                            taskTemp.WhatId = selectedChecklist.Contract_Aircraft__c;
                        }
                        
                        fieldInfo = tsUtil.getFieldInfo(taskSetup.Id);
                        if( fieldInfo != null )
                        {
                            
                            if(selectedCheckList.getSobject(fieldInfo.get('object__r')) == Null)
                            {
                                if(fieldInfo.get('object__r')  == 'Commercial_Agreement__r'  && selectedCheckList.getSobject('Commercial_Agreement__r').getSObject('Contract_Aircraft__r').get(fieldInfo.get('field')) != Null)
                                {
                                    taskTemp.ActivityDate = tsUtil.calculateDate((Date) selectedCheckList.getSobject('Commercial_Agreement__r').getSObject('Contract_Aircraft__r').get(fieldInfo.get('field')), taskSetup.Id);
                                }
                            }
                            else
                            {
                                taskTemp.ActivityDate = tsUtil.calculateDate((Date) selectedCheckList.getSobject(fieldInfo.get('object__r')).get(fieldInfo.get('field')), taskSetup.Id);
                            }
                            

                            taskTemp.Status = 'Open'; 
                            taskTemp.Subject  = taskSetup.Description__c; //'Checklist Activities'; 
                            
                            if(selectedChecklist.Assign_Task_to__c != null)
                                taskTemp.OwnerId = selectedChecklist.Assign_Task_to__c;//questionnaire.OwnerId; 
                            

                            taskTemp.Description  = taskSetup.Description__c; 
                            taskTemp.Task_Setup_Code__c = String.valueOf(taskSetup.Sequence__c);
                            taskTemp.Checklist__c = selectedChecklist.Id;
                            taskTemp.Task_Setup__c = taskSetup.Id;
                            taskList.add(taskTemp);

                
                        }
                    }
                    
                }
                if(!taskList.isEmpty())
                    insert taskList;
                    
                returnValue = true;
            }
            
        }
        catch(exception ex)
        {
            returnValue = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error creating tasks - ' + ex.getMessage() + '- ' + ex.getLineNumber()));
        }
        
        return returnValue;
    
    }
    
    public boolean deleteTasks()
    {
        boolean returnValue = false;
        try
        {
            list<Task> lstOpenTask = new list<Task>();
            lstOpenTask = [SELECT Id, Status, Checklist__c FROM Task where Status = 'Open' AND Checklist__c =:selectedChecklist.Id];
            if(!lstOpenTask.isEmpty())
                database.delete(lstOpenTask);
                
            returnValue = true;
        }
        catch(exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error deleting tasks - ' + ex.getMessage()));
        }
        
        return returnValue;
        
    }
    
    private boolean validateDateFields()
    {
        boolean returnValue = true;
        boolean readRelatedAgreement = false;
        boolean verifyFieldDate = false;
        string answerValue = '';
        string questionAPI = '';
        map<string,string> picklistMapTemp;
        
        
        for(Task_Setup__c taskSetup : TaskSetupDAO.getInstance().getTaskSetupByRecordType(selectedChecklist.RecordType.Name))
        {    
            verifyFieldDate = false;
            
            if(taskSetup.Question__c == 'All')
            {
                verifyFieldDate = true;
            }
            else 
            {
                 questionAPI = '';
                 
                 if(checklistFieldsMap.containsKey(taskSetup.Question__c.toLOwerCase()))
                    questionAPI = checklistFieldsMap.get(taskSetup.Question__c.toLOwerCase());
                            
                if(string.IsNotEmpty(questionAPI))
                {
                    
                    if(string.IsNotEmpty(taskSetup.Answer__c))
                    {
                        //answerValue = (selectedChecklist.get(questionAPI) == Null ? '' : string.ValueOf(selectedChecklist.get(questionAPI)).toLowerCase());
                        if(selectedChecklist.get(questionAPI) == Null)
                        {
                            answerValue = '';
                        }
                        else
                        {
                            answerValue = string.ValueOf(selectedChecklist.get(questionAPI)).toLowerCase();
                            if(picklistValuesMap.containsKey(taskSetup.Question__c))
                            {
                                picklistMapTemp = picklistValuesMap.get(taskSetup.Question__c); 
                                answerValue = picklistMapTemp.get(string.valueof(selectedChecklist.get(questionAPI))).toLowerCase();
                            }
                        }    
                    
                        
                        verifyFieldDate = (taskSetup.Answer__c.toLowerCase() == answerValue) && !String.isEmpty(answerValue);
                    }   
                                    
    
                }
            }
            
            if(verifyFieldDate && taskSetup.Selected_Date__c != Null)
            {
                //GSAMICO>dateFieldsTasksMap.put(taskSetup.Id,taskSetup.Name);
                returnValue = isTaskSetupValid ( taskSetup.Id, taskSetup.Name );
            }
                
            if ( returnValue == false )
            {
                break;
            }
        }    
        
        
        return returnValue;
    }
    
    
    private Boolean isTaskSetupValid ( Id taskSetupId, String taskName )
    {
        boolean returnValue = true;
        Map<String,String> fieldInfo;
        
        fieldInfo = tsUtil.getFieldInfo(taskSetupId);
        if(selectedCheckList.getSobject(fieldInfo.get('object__r')) == Null  && selectedChecklist.RecordType.Name != 'Contract Milestones')
        {
            if(fieldInfo.get('object__r')  == 'Agreement__r')
            {
                if(selectedCheckList.getSobject('Agreement_Aircraft__r').getSObject('Contract_Aircraft__r').get(fieldInfo.get('field')) == Null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The date field ' + fieldInfo.get('label')  +   ' is null and the task ' +  taskName + ' cannot be created. Please check.'));
                    returnValue = false;
                }
            }
            else
            {   
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The relationship ' + fieldInfo.get('object__r')  +   ' is null and the task ' +  taskName + ' cannot be created. Please check.'));            
                returnValue = false;
            }   
            
            
        }
        else
        {
            if(selectedCheckList.getSobject(fieldInfo.get('object__r')).get(fieldInfo.get('field')) == Null)
            {
                returnValue = false; 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The date field ' + fieldInfo.get('label')  +   ' is null and the task ' +  taskName + ' cannot be created. Please check.'));
            }
            
        }
        
        return returnValue;
    }
    
  
    
    @TestVisible private List<String> populateFieldsList()
    {
        List<string> returnValue =  new List<String>{'RecordType.Name'};
        List<String> listObjectFieldDate = new List<String>();
        
        for(string field :  + utils.getAllFields('Questionnaire__c').split(','))
            returnValue.add(field);

        for(String fieldLabel : tsUtil.dateFieldsMap.keySet())
        {
            if(tsUtil.dateFieldsMap.get(fieldLabel).get('object__r') == 'Agreement__r')
            {
                returnValue.add('Agreement_Aircraft__r.Contract_Aircraft__r.'+ tsUtil.dateFieldsMap.get(fieldLabel).get('field'));
            }
            returnValue.add(tsUtil.dateFieldsMap.get(fieldLabel).get('object__r') + '.'+ tsUtil.dateFieldsMap.get(fieldLabel).get('field'));
        }                
        
       return returnValue;
    }
    
    private void createObjectFieldsSet()
    {
    
        Schema.DescribeFieldResult fieldDescribe;
        map<string,string> picklistValuesLabelMap;
        
        
        for(string field : Schema.SObjectType.Questionnaire__c.fields.getMap().keySet())
            objectFields.add(field.toLowerCase());
            
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType fieldSchema = schemaMap.get('Questionnaire__c');
        Map<String, Schema.SObjectField> fieldMap = fieldSchema.getDescribe().fields.getMap();

        for (Schema.sObjectField fieldInstance : fieldMap.values())
        { 
            fieldDescribe = fieldInstance.getDescribe();
            checklistFieldsMap.put(fieldDescribe.getLabel().toLowerCase(),fieldDescribe.getName());
            
            if(string.ValueOf(fieldDescribe.getType()) == 'PICKLIST')
            {
                picklistValuesLabelMap = new map<string,string>();
                List<Schema.PicklistEntry> values = fieldDescribe.getPicklistValues();
                for( Schema.PicklistEntry v : values) 
                    picklistValuesLabelMap.put(v.getValue(),v.getLabel());
                
                
                picklistValuesMap.put(fieldDescribe.getLabel(),picklistValuesLabelMap);
            
            }
        }    
            
        return;
    }
    
    
    
    

    
    public static String getPickListValue(sObject o){
        
        
        return '';
    }
 
    
}