/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class used to make posts on chatter
* NAME: ChatterUtils.cls
* AUTHOR: LRSA                                                DATE: 04/12/2014
*
*******************************************************************************/

public with sharing class ChatterUtils {
	
	public static void mentionTextPost( Id userId, list< Id > userToMentionId, id aObjId, String postText ) 
	{ 

    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    messageInput.messageSegments = new list< ConnectApi.MessageSegmentInput >();
    
    // add the text that was passed
    ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
    textSegment.text = postText;
    messageInput.messageSegments.add(textSegment);

    // add the link
    ConnectApi.LinkSegmentInput inputSegment = new ConnectApi.LinkSegmentInput();
    inputSegment.Url = URL.getSalesforceBaseUrl().toExternalForm() + '/' + aObjId;
    messageInput.messageSegments.add(inputSegment);
    
    // add the mention
    for ( id lUserId : userToMentionId )
    {
      ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
      mentionSegment.id = lUserId;
      messageInput.messageSegments.add( mentionSegment );
      ConnectApi.TextSegmentInput spaceSegment = new ConnectApi.TextSegmentInput();
      spaceSegment.text = ' ';
      messageInput.messageSegments.add(spaceSegment);
    }
    
    ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
    input.body = messageInput;

    // post it
    ConnectApi.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, userId, input, null);

  } 

}