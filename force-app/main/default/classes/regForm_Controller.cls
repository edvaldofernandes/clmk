public class regForm_Controller {
    
    //Variable that receives the input value in Course ID field:
    public String campaignId {get ; set ;}
    //Variable for storing the list of campaigns queried(1):
    public Campaign[] campaigns {get ; set ;}
    //Variable for storing the campaign of the form:
    public Campaign campaign {get ; set ;}
    //Variable for the registration form:
    public Registration_Form__c regForm {get ; set ;}
    //Variable for handling the Course ID validation:
    public String validCampaign {get ; set ;}
    //Variable for handling the "Registration Complete" message:
    public String regStatus {get ; set ;}
    
    //Controller constructor:
    public regForm_Controller(){
        regForm = new Registration_Form__c();
        validCampaign = 'none';
        regStatus = 'incomplete';
    }
    
    //Method to check if the Course ID inserted is valid:
    public void validateCampaign(){
        campaigns = [SELECT Id, Name, Registration_Form_Start_Date__c, Registration_Form_End_Date__c FROM Campaign where Id = :campaignId];
        regStatus = 'incomplete';
        //Checks if a campaign was found:
        if(campaigns.size() != 0){
            campaign = campaigns[0];
            //Checks if the registration form is within the date range:
            if(system.today() >= campaign.Registration_Form_Start_Date__c && system.today() <= campaign.Registration_Form_End_Date__c){
            //If so, the campaign is valid:    
            validCampaign = 'true';
               }
        }else{
            //If not, the campaign is not valid:
             validCampaign = 'false';
        }
    }
    
    //Method for saving the record:
    public void save(){
        try {
            regForm.Campaign__c = campaign.id;
            //Auto-generates the name using the campaign name and student name:
            regForm.Name = campaign.Name + ' - ' + regForm.First_Name__c + ' ' + regForm.Family_Name__c ;
            //Tries to save the campaign:
            insert regForm;
            //Changes the regStatus, so the page can display the "Registration Complete" message:
            regStatus = 'complete';
        } catch(DmlException e) {
            //If the save fails, throw an error:
            System.debug('An unexpected error has occurred: Failed to Save Form - ' + e.getMessage());
        }
    }
    
    //Method for the "Register Another Student" button:
    public void regAnother(){
        //Resets the form:
        regForm = new Registration_Form__c();
        //Changes the regStatus, so the page can display the form again:
        regStatus = 'incomplete';
    }
    
    //Method for search a form in print button
    public void searchForm(){
        regForm = null;
        campaign = null;
        String formId = ApexPages.currentPage().getParameters().get('id');
        regForm = [SELECT Id, First_Name__c, Middle_Name__c, Family_Name__c, Date_of_Birth__c, City_of_Birth__c, Country_of_Birth__c, Company__c, Position__c, E_mail__c, Phone__c, Licenses__c, Type_Ratings__c, Professional_Background__c, Campaign__c FROM Registration_Form__c WHERE Id = :formId];        
        campaign = [SELECT Id, Name, Aircraft__c, StartDate, EndDate, Class_Code__c FROM Campaign where Id = :regForm.Campaign__c]; 
    } 
}