/*******************************************************************************
*                     Copyright (C) 2014 - Cloud2b
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class QuoteSelectProductController
*
* NAME: QuoteSelectProductControllerTest.cls
*
* AUTHOR: JFS                        DATE: 19/12/2014
*******************************************************************************/
@isTest
private class QuoteSelectProductControllerTest {

    static testMethod void myUnitTest() 
    {
    	
    	Discount_policy__c descontoAge;
    	Discount_policy__c descontoAge2;
    	Discount_policy__c descontoFleet;
    	Discount_policy__c descontoContract;
    	Discount_policy__c descontoOther;
    	List<Discount_policy__c> listaDescontos = new List<Discount_policy__c>();
    	
    	descontoAge = new Discount_policy__c();
    	descontoAge.Discount__c = 10;
    	descontoAge.Quantity_max__c = 10;
    	descontoAge.Quantity_min__c = 0;
    	descontoAge.Discount_type__c = 'Age';
    	listaDescontos.add(descontoAge);
    	
    	descontoOther = new Discount_policy__c();
    	descontoOther.Discount__c = 10;
    	descontoOther.eSolutionsDiscountCriteria__c = 'Above 4 years';
    	descontoOther.Discount_type__c = 'Other eSolutions';
    	listaDescontos.add(descontoOther);
    	
    	descontoContract = new Discount_policy__c();
    	descontoContract.Discount__c = 10;
    	descontoContract.eSolutionsDiscountCriteria__c = '3 years';
    	descontoContract.Discount_type__c = 'Contract Time';
    	listaDescontos.add(descontoContract);
    	
    	descontoAge2 = new Discount_policy__c();
    	descontoAge2.Discount__c = 10;
    	descontoAge2.Quantity_max__c = 1;
    	descontoAge2.Quantity_min__c = 1;
    	descontoAge2.Discount_type__c = 'Age';
    	listaDescontos.add(descontoAge2);
    	
    	descontoFleet = new Discount_policy__c();
    	descontoFleet.Discount__c = 20;
    	descontoFleet.Quantity_max__c = 10;
    	descontoFleet.Quantity_min__c = 0;
    	descontoFleet.Discount_type__c = 'Fleet';
    	listaDescontos.add(descontoFleet);
    	
    	insert listadescontos;
    
      
    	
      Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
      Account acc = SObjectInstanceTest.createAccount(recTypeId);
      Database.insert(acc);
      
      Aircraft__c aircraft = SObjectInstanceTest.createAircraft();      
      aircraft.Operator__c = acc.Id;
      aircraft.Fleet_Type__c = 'Turboprop';

      Aircraft__c aircraft2 = SObjectInstanceTest.createAircraft();
      aircraft2.Name = 'aircra21';      
      aircraft2.Operator__c = acc.Id;
      aircraft2.Owner__c = acc.Id;
      aircraft2.Fleet_Type__c = 'Turboprop';
      Database.insert(new list<Aircraft__c> { aircraft, aircraft2 } );
      
      Product2 produto = SObjectInstanceTest.createProduct2();      
      produto.Applicability__c = 'Turboprop';
      Database.insert(produto);
      
      Pricebook2 pb2 = SObjectInstanceTest.createPricebook2();
      pb2.Name = 'pricebook teste 1';
      pb2.Account_Type__c = 'Operator';
      Database.insert(pb2);
      
      Pricebook2 pb2_2 = SObjectInstanceTest.createPricebook2();
      pb2_2.Name = 'pricebook teste 2';
      pb2_2.Account_Type__c = 'Operator';
      Database.insert(pb2_2);
      
      PricebookEntry stdPbe = SObjectInstanceTest.createPricebookEntry(
      SObjectInstanceTest.catalogoDePrecoPadrao(), produto.Id);
      stdPbe.RECPrice__c = 5000;
      Database.insert(stdPbe);
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2.Id, produto.Id);
      pbe.RECPrice__c = 5000;
      Database.insert(pbe);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      opp.AccountId = acc.Id;
      opp.Fleet_Type__c = 'Turboprop';
      Database.insert(opp);
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      database.insert(Qot);      
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      Test.startTest();
      
      
      	String userLocale = controller.userLocale;
      	String regTrain = controller.regionTraining;
      	
		System.AssertEquals('Training',controller.recTypeTraining);
      	System.AssertEquals('Pilot_Services',controller.recTypePilotServices);
      	System.AssertEquals('Aircraft_Modification',controller.recProductModification);
      	System.AssertEquals('Mix_Products',controller.recTypeMixProducts);
      	System.AssertEquals('eSolutions',controller.recTypeeSolutions);
      	System.AssertEquals('Tailored',controller.recProductTailored);
      	System.AssertEquals('TechnicalServices',controller.recTechnicalServices);
      	
      	//rodrigo
      	System.AssertEquals('MaterialSolutions',controller.recTypeMaterial);
      	System.AssertEquals('MaintenanceServices',controller.recTypeMRO);
      	controller.applyContractTimeDiscount = false;
  		controller.applyOtherEsolutionsDiscount  = false;
 	 	controller.idProductSelectedRow  = '12345567';
  		controller.pricingSelectedRow  = '1';
  		controller.princingOliSelComp  = '123456';
      	
      	controller.exibePrerequisitos = true;
       
      //selecionar catálogo de preços 
      controller.pb2Id = pb2.Id;
      controller.salvarPricebook2();
      
      //selecionar produto para adicionar ao carrinho de compras
      QuoteSelectProductController.ProdutoWrapper[] produtosDisponiveis = controller.ProdutosDisponiveis;
      produtosDisponiveis[0].isSelected = true;      
      controller.adicionarCarrinho(); 
      
      //selecionar aircraft
      controller.idPbeSelComp = pbe.Id;
      controller.exibeComponente();
      QuoteSelectProductController.AircraftWrapper[] lstAircraft = controller.listAircraft;
      lstAircraft[0].isSelected = true;
      controller.filtroOrderBy = 'Aircraft_Status__c';
      controller.ordenarAircrafts();
            
      controller.filtroOrderBy = 'Operator__r';
      controller.ordenarAircrafts();
      controller.closePopup();
     
      //preencher quantidade e salvar
      QuoteSelectProductController.Carrinho[] carrinhoCompras = controller.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho QotOLi : carrinhoCompras ) QotOLi.opp.Quantity = 2;
      controller.salvar();   
      
      Test.stopTest();
      
      List<QuoteLineItem> lstQotOli = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: Qot.Id];
      system.assertEquals(2, lstQotOli.size(), 'Erro ao gravar produtos da oportunidade');
      
      List<String> lstId = new List<String>();
      for (QuoteLineItem iOli: lstQotOli )
      {
        lstId.add(iOli.Id);
      }
      
    }
    
    static testMethod void myUnitTestUpdate() 
    {
      Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
      Account acc = SObjectInstanceTest.createAccount(recTypeId);
      Database.insert(acc);
      
      Aircraft__c aircraft = SObjectInstanceTest.createAircraft();      
      aircraft.Operator__c = acc.Id;
      aircraft.Fleet_Type__c = 'Turboprop';
      
      Aircraft__c aircraft2 = SObjectInstanceTest.createAircraft();      
      aircraft2.Operator__c = acc.Id;      
      aircraft2.Name = '654321ai';
      aircraft2.Fleet_Type__c = 'Turboprop';
      Database.insert(new list<Aircraft__c> { aircraft, aircraft2 } );
      
      Product2 produto = SObjectInstanceTest.createProduct2();         
      produto.Applicability__c = 'Turboprop';
      Database.insert(produto);
      
      Pricebook2 pb2 = SObjectInstanceTest.createPricebook2();
      pb2.Account_Type__c = 'Operator';
      Database.insert(pb2);
      
      PricebookEntry stdPbe = SObjectInstanceTest.createPricebookEntry(
        SObjectInstanceTest.catalogoDePrecoPadrao(), produto.Id);
      Database.insert(stdPbe);
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2.Id, produto.Id);
      pbe.RECPrice__c = 500;
      Database.insert(pbe);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      opp.AccountId = acc.Id;
      opp.Fleet_Type__c = 'Turboprop';
      Database.insert(opp);
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      database.insert(Qot);
      
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      //selecionar catálogo de preços 
      controller.pb2Id = pb2.Id;
      controller.salvarPricebook2();
      
      //selecionar produto para adicionar ao carrinho de compras
      controller.carregarProdutos();
      QuoteSelectProductController.ProdutoWrapper[] produtosDisponiveis = controller.ProdutosDisponiveis;
      produtosDisponiveis[0].isSelected = true;     
      controller.adicionarCarrinho();
      
      //selecionar aircraft
      controller.idPbeSelComp = pbe.Id;
      controller.applyFleetDiscount = false;
      controller.applyAgeDiscount = true;
      controller.exibeComponente();
      QuoteSelectProductController.AircraftWrapper[] lstAircraft = controller.listAircraft;
      controller.filtroOrderBy = 'Aircraft_Status__c';
      controller.ordenarAircrafts();
            
      controller.filtroOrderBy = 'Operator__r';
      controller.ordenarAircrafts();
      controller.closePopup();
      
      //preencher quantidade e salvar
      QuoteSelectProductController.Carrinho[] carrinhoCompras = controller.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho oli : carrinhoCompras ) oli.opp.Quantity = 2;
      controller.salvar();   
      
      Test.startTest();
      
      //carregar tela novamente
      QuoteSelectProductController newController = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      //alterar aircraft selecionado
      newController.idPbeSelComp = pbe.Id;
      newController.exibeComponente();
      QuoteSelectProductController.AircraftWrapper[] lstAircraft2 = newController.listAircraft;
      controller.filtroOrderBy = 'Aircraft_Status__c';
      controller.ordenarAircrafts();
            
      controller.filtroOrderBy = 'Operator__r';
      controller.ordenarAircrafts();
      newController.closePopup();
      newcontroller.applyFleetDiscount = false;
      newcontroller.applyAgeDiscount = true;
      
      carrinhoCompras = newController.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho oli : carrinhoCompras ) oli.opp.Quantity = 2;
      
      //salvar
      newController.salvar();   
      
      List<QuoteLineItem> lstOli = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: Qot.Id];
       List<String> lstId = new List<String>();
      for (QuoteLineItem iOli: lstOli )
      {
        lstId.add(iOli.Id);
      }      
      List<Related_Aircraft__c> lstRelAir = [SELECT Aircraft__c FROM Related_Aircraft__c WHERE Opportunity_product_id__c =: lstId ];
            
      Test.stopTest();
      
    lstOli = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: Qot.Id];
      
    lstId = new List<String>();
      for (QuoteLineItem iOli: lstOli )
      {
        iOli.Quantity = 2;
        lstId.add(iOli.Id);
      }
      
      lstRelAir = [SELECT Aircraft__c FROM Related_Aircraft__c WHERE Opportunity_product_id__c =: lstId ];
      
    }
    
    static testMethod void myUnitTestDelete() 
    {
      Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
      Account acc = SObjectInstanceTest.createAccount(recTypeId);
      Database.insert(acc);
      
      Aircraft__c aircraft = SObjectInstanceTest.createAircraft();      
      aircraft.Operator__c = acc.Id;
      aircraft.Fleet_Type__c = 'Turboprop';
      
      Aircraft__c aircraft2 = SObjectInstanceTest.createAircraft();      
      aircraft2.Operator__c = acc.Id;
      aircraft2.Name = '654321rc';
      aircraft2.Fleet_Type__c = 'Turboprop';
      Database.insert(new list<Aircraft__c> { aircraft, aircraft2 } );
      
      Product2 produto = SObjectInstanceTest.createProduct2();         
      produto.Applicability__c = 'Turboprop';
      Database.insert(produto);
      
      Pricebook2 pb2 = SObjectInstanceTest.createPricebook2();
      pb2.Account_Type__c = 'Operator';
      Database.insert(pb2);
      
      PricebookEntry stdPbe = SObjectInstanceTest.createPricebookEntry(
        SObjectInstanceTest.catalogoDePrecoPadrao(), produto.Id);
      Database.insert(stdPbe);
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2.Id, produto.Id);
      pbe.RECPrice__c = 500;
      Database.insert(pbe);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      opp.AccountId = acc.Id;
      opp.Fleet_Type__c = 'Turboprop';
      Database.insert(opp);
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      database.insert(Qot);
      
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      //selecionar catálogo de preços 
      controller.pb2Id = pb2.Id;
      controller.applyFleetDiscount = false;
      controller.applyAgeDiscount = true;
      
      controller.salvarPricebook2();
      
      //selecionar produto para adicionar ao carrinho de compras
      controller.carregarProdutos();
      QuoteSelectProductController.ProdutoWrapper[] produtosDisponiveis = controller.ProdutosDisponiveis;
      produtosDisponiveis[0].isSelected = true;     
      controller.adicionarCarrinho();
      
      //selecionar aircraft
      controller.idPbeSelComp = pbe.Id;
      controller.exibeComponente();
      QuoteSelectProductController.AircraftWrapper[] lstAircraft = controller.ListAircraft;
      controller.filtroOrderBy = 'Aircraft_Status__c';
      controller.ordenarAircrafts();
            
      controller.filtroOrderBy = 'Operator__r';
      controller.ordenarAircrafts();
      controller.closePopup();
      
      //preencher quantidade e salvar
      QuoteSelectProductController.Carrinho[] carrinhoCompras = controller.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho oli : carrinhoCompras ) oli.opp.Quantity = 2;
      controller.salvar();   
      
      Test.startTest();
      
      //carregar tela novamente
      QuoteSelectProductController newController = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      //remover item do carrinho
      carrinhoCompras = controller.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho oli : carrinhoCompras )
      {
        newController.IdPBE = oli.opp.PricebookEntryId; 
        newController.Idprincing = oli.opp.princing__c;  
        newController.countSelecionado = oli.indice;   
        newController.removeItem();
      }
      //salvar
      newController.salvar(); 
      
      Test.stopTest();
      
      List<QuoteLineItem> lstOli = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: Qot.Id];
      system.assertEquals(0, lstOli.size(), 'Erro ao excluir produtos da oportunidade');
            
    }
    
    static testMethod void cancelar() 
    {
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      Database.insert(opp);
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      database.insert(Qot);
      
      Test.startTest();
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      controller.cancelar();
      Test.stopTest();
    }
    
    static testMethod void atualizar() 
    {
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      Database.insert(opp);
      
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      database.insert(Qot);
      
      Test.startTest();
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      controller.atualizar();
      Test.stopTest();
    }
    
    static testMethod void testTraining()
    {
      Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
      Account acc = SObjectInstanceTest.createAccount(recTypeId);
      Database.insert(acc);
      
      Id recypeTraining = RecordTypeMemory.getRecType('Opportunity', 'Training');
      Id rectypeprod = RecordTypeMemory.getRecType('Product2', 'Training');
            
      Product2 produto = SObjectInstanceTest.createProduct2();
      produto.Per_diem__c = true;
      produto.RecordTypeId = rectypeprod;
      produto.Applicability__c = 'EJET';
      Database.insert(produto);
      
      Product2 produtoPD = SObjectInstanceTest.createProduct2();
      produtoPD.ProductCode = 'Per Diem';
      produtoPD.Name = 'Per Diem';
      produtoPD.RecordTypeId = rectypeprod;
      Database.insert(produtoPD);
      
      Slots__c slot = SObjectInstanceTest.createSlot();
      Database.insert(slot);
      
      Id rectypeperdiem = RecordTypeMemory.getRecType('Service_catalog_setup__c', 'Per_diem');
      
      Service_catalog_setup__c serviceCatalog = SObjectInstanceTest.serviceCatalog();
      serviceCatalog.RecordTypeId = rectypeperdiem;
      Database.insert(serviceCatalog);
      
      Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
            
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(stdPB, produto.Id);
      Database.insert(pbe);
      
      PricebookEntry pbePD = SObjectInstanceTest.createPricebookEntry(stdPB, produtoPD.Id);
      Database.insert(pbePD);
      
      Pricebook2 pb2 = SObjectInstanceTest.createPricebook2();
      pb2.Account_Type__c = 'Operator';
      Database.insert(pb2);
      
      PricebookEntry pbe2 = SObjectInstanceTest.createPricebookEntry(pb2.Id, produto.Id);
      Database.insert(pbe2);
      
      PricebookEntry pbePD2 = SObjectInstanceTest.createPricebookEntry(pb2.Id, produtoPD.Id);
      Database.insert(pbePD2);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      opp.RecordTypeId = recypeTraining;
      opp.AccountId = acc.Id;
      opp.Pricebook2Id = pb2.Id;
      opp.Fleet_Type__c = 'EJET';
      Database.insert(opp);
      
       Id recypeTrainiQuote = RecordTypeMemory.getRecType('Quote', 'Training');
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      Qot.RecordTypeId = recypeTrainiQuote;
      database.insert(Qot);
      
      Test.startTest();
      
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      //selecionar catálogo de preços 
      controller.pb2Id = pb2.Id;
      controller.salvarPricebook2();
      
      controller.carregarProdutos();
      QuoteSelectProductController.ProdutoWrapper[] produtosDisponiveis = controller.ProdutosDisponiveis;
      produtosDisponiveis[0].isSelected = true;     
      controller.adicionarCarrinho();
      
      List<QuoteSelectProductController.Carrinho> carrinho = controller.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho oli : carrinho ) oli.opp.Region__c = 'Africa';
      
      controller.idPbeSelTrainingId = pbe2.Id;
      controller.idPbeSelComp = pbePD2.Id;
      
      controller.countSelecionado = 0;
      controller.exibeValoresPerDiem();
      
      List<QuoteSelectProductController.PerDiemWrapper> lstPDW = controller.lstPerDiem;
      lstPDW[0].ehPerDiemHotel = true;
      
      controller.closePopupPerDiem();
      
      controller.verificarSlots();
      
      controller.salvar();
      
      Test.stopTest();
    }
    
    static testMethod void auxiliar()
    {
      Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
      Account acc = SObjectInstanceTest.createAccount(recTypeId);
      Database.insert(acc);
      
      Aircraft__c aircraft = SObjectInstanceTest.createAircraft();      
      aircraft.Operator__c = acc.Id;
      aircraft.Fleet_Type__c = 'Turboprop';
      insert aircraft;
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      Database.insert(opp);
      
      Test.startTest();
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(opp));
      controller.applyAgeDiscount = true;
      controller.applyFleetDiscount = true;
      QuoteSelectProductController.AircraftWrapper lx = new QuoteSelectProductController.AircraftWrapper( aircraft, null, null, null );
      controller.Cancel();
      controller.exibeErros();
      Test.stopTest();
      
    }
    
    static testMethod void testeDiscountESolutions() 
    {
      Id recTypeId = RecordTypeMemory.getRecType('Account', 'Operator');
      Account acc = SObjectInstanceTest.createAccount(recTypeId);
      Database.insert(acc);
      
      Aircraft__c aircraft = SObjectInstanceTest.createAircraft();      
      aircraft.Operator__c = acc.Id;
      aircraft.Fleet_Type__c = 'Turboprop';

      Aircraft__c aircraft2 = SObjectInstanceTest.createAircraft();
      aircraft2.Name = 'aircrft2';      
      aircraft2.Operator__c = acc.Id;
      aircraft2.Owner__c = acc.Id;
      aircraft2.Fleet_Type__c = 'Turboprop';
      Database.insert(new list<Aircraft__c> { aircraft, aircraft2 } );
      
      Product2 produto = SObjectInstanceTest.createProduct2();      
      produto.Applicability__c = 'Turboprop';
      produto.Progressive_discount__c = 'Policy 1';
      Database.insert(produto);
      
      Pricebook2 pb2 = SObjectInstanceTest.createPricebook2();
      pb2.Name = 'pricebook teste 1';
      pb2.Account_Type__c = 'Operator';
      Database.insert(pb2);
      
      Pricebook2 pb2_2 = SObjectInstanceTest.createPricebook2();
      pb2_2.Name = 'pricebook teste 2';
      pb2_2.Account_Type__c = 'Operator';
      Database.insert(pb2_2);
      
      PricebookEntry stdPbe = SObjectInstanceTest.createPricebookEntry(
        Test.getStandardPricebookId(), produto.Id);
      stdPbe.RECPrice__c = 5000;
      Database.insert(stdPbe);
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pb2.Id, produto.Id);
      pbe.RECPrice__c = 5000;
      Database.insert(pbe);
      
      Opportunity opp = SObjectInstanceTest.createOpportunity();
      opp.AccountId = acc.Id;
      opp.Fleet_Type__c = 'Turboprop';
      Database.insert(opp);
      
      Quote Qot = SObjectInstanceTest.createQuote(opp.id);
      database.insert(Qot);
      
      Discount_policy__c policy = SObjectInstanceTest.discountPolicy();
      database.insert(policy);
      
      QuoteSelectProductController controller = new QuoteSelectProductController(new ApexPages.Standardcontroller(Qot));
      
      Test.startTest();
      
      //controller.stdPricebookEntry = stdPbe;
      //controller.tailoredPricebookEntry = pbe;
      //selecionar catálogo de preços
      controller.pb2Id = pb2.Id;
      controller.salvarPricebook2();
      
      //selecionar produto para adicionar ao carrinho de compras
      QuoteSelectProductController.ProdutoWrapper[] produtosDisponiveis = controller.ProdutosDisponiveis;
      produtosDisponiveis[0].isSelected = true;      
      controller.adicionarCarrinho();
                  
      //preencher quantidade e salvar
      QuoteSelectProductController.Carrinho[] carrinhoCompras = controller.carrinhoCompras;
      for ( QuoteSelectProductController.Carrinho oli : carrinhoCompras )
      {
        oli.opp.Quantity = 2;
        controller.idPbeSelComp = oli.opp.PricebookEntry.Product2ID;
        controller.princingOliSelComp = oli.opp.Princing__c;
      }
      controller.calculaDescontoESolutions();  
      
      QuoteSelectProductController.pricebookEntryWrapper wrapperPb = new QuoteSelectProductController.pricebookEntryWrapper();
      wrapperPb.pricebook2Name = 'Teste';
      wrapperPb.isStandardPricebook  = false;
      wrapperPb.Id = '12345600';
      wrapperPb.Product2Id  = '1234569';
      wrapperPb.Pricebook2id  = '666777888';
      wrapperPb.UseStandardPrice  = false;
      wrapperPb.UnitPrice  = '100';
      wrapperPb.RECPrice  = '100';
      wrapperPb.IsActive  = true;
      wrapperPb.Margin_0 = '100';
      wrapperPb.Maximum_Discount = '100';
      wrapperPb.Maximum_Discount_REC = '100';
      wrapperPb.Margin_0_REC = '100';
      wrapperPb.CurrencyIsoCode = 'USD';
      
      controller.calculaDescontoESolutions();   
      
      controller.showCreateProduct();
      
      controller.closePrerequisitos();
      controller.closeErros();
      controller.cancelPopupPerDiem();
      controller.createProduct();
      controller.updateESolutionsDiscount();
      
       
      
      Test.stopTest();
      
    }
}