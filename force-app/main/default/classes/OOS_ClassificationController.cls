public class OOS_ClassificationController {
  public static List<AccountTeamMember> getCurrentTeamRoleMember() {
    Id userId = UserInfo.getUserId();
    return [
      SELECT AccountId, TeamMemberRole
      FROM AccountTeamMember
      WHERE userId = :userId
    ];
  }

  @AuraEnabled
  public static String getProfileName() {
    Id profileId = UserInfo.getProfileId();
    return [SELECT Name FROM Profile WHERE Id = :profileId].Name;
  }

  @AuraEnabled
  public static List<Out_of_service__c> getOOSRecords(String currentId) {
    String profileName = getProfileName();
    String templateString =
      'SELECT ' +
      Utils.getAllFields('Out_of_service__c') +
      ' FROM Out_of_service__c WHERE ({0}' +
      false +
      ' and Disregarded_Classification__c = ' +
      false +
      ') or Id = :currentId ORDER BY Start_Date__c ASC';

    if (profileName.contains('Service & Support Representative')) {
      List<Id> accountIds = new List<Id>();
      for (AccountTeamMember atm : getCurrentTeamRoleMember()) {
        if (atm.TeamMemberRole == 'Tech Rep') {
          accountIds.add(atm.AccountId);
        }
      }
      return Database.query(
        String.Format(
          templateString,
          new List<String>{
            'Serial_Number__r.Operator__c IN :accountIds and TechRep_Classified__c = '
          }
        )
      );
    } else if (profileName.contains('Customer Data Reader')) {
      return Database.query(
        String.Format(
          templateString,
          new List<String>{ 'EMIT_RTS_Classified__c = ' }
        )
      );
    } else if (
      profileName.contains('Quality for Chatter Only') ||
      profileName.contains('Quality Full')
    ) {
      return Database.query(
        String.Format(
          templateString,
          new List<String>{ 'Quality_Classified__c = ' }
        )
      );
    } else {
      return Database.query(
        'SELECT ' +
        Utils.getAllFields('Out_of_service__c') +
        ' FROM Out_of_service__c WHERE Disregarded_Classification__c = ' +
        false +
        ' or Id = :currentId ORDER BY Aircraft_Register__c, Start_Date__c ASC'
      );
    }
  }
}