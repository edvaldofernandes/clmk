@isTest
public class CFCVerifyAcountTeamMemberScheduleTest {
    public static testMethod void testschedule() {
        Test.StartTest();
        CFCVerifyAcountTeamMemberSchedule testsche = new CFCVerifyAcountTeamMemberSchedule();
        String sch = '0 0 23 * * ?';
        system.schedule('VerifyAccount Test', sch, testsche );
        Test.stopTest();
    }
    
    public static testMethod void testscheduleMe() {
        
        User userTest = CFCTestSetup.getUser();
        
        System.runAs(userTest) 
        {
            CFC_Configurations__c cs = new CFC_Configurations__c();
            cs.Mobile_Limits_KB__c = 1;
            cs.Name = 'Test CS';
            cs.Tablet_Limits_KB__c = 1;
            cs.Desktop_Limit_KB__c = 1;
            
            cs.Billboard_max_published_number__c = 5;
            cs.URL_Communities__c = 'wwww.teste.com.br';
            cs.Billboard_Time__c = 5;
            cs.Email_to_send_error_job__c = 'teste@teste.com.br';
            cs.User_Name__c = 'teste@teste.com';
            insert cs;
        }        
        
        Test.StartTest();
        CFCVerifyAcountTeamMemberSchedule.scheduleMe();
        Test.stopTest();
    }
}