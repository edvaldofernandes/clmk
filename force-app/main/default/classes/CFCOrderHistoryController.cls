/**
* Controller class for the visualforce page CFCOrderHistoryController.
* Name: CFCOrderHistoryController
* @author - Guilherme Nascimento - gjesus@deloitte.com
* @version 1.0 - 15/12/2015
**/
public without sharing class CFCOrderHistoryController {
    
    public String selectedValueOrderBy 								{get;set;}
    public List<ServiceContract> lstServiceContract 				{get;set;}

    public String numberOrders										{get;set;}
    public Boolean historicoNulo 									{get;set;}
    public Boolean controleMap {get;set;}
    public Integer lastDays											{get;set;}
	public Boolean isPardotActive {get;set;}
    
    public List<ContractLineItem> lstContractItems {get;set;}
    public List<CFCPurchaseHistory.serviceContractWrapper> lstServiceWrapper {get;set;}
    public List<CFCPurchaseHistory.contractLineItemWrapper> lstContractLineItemWrapper {get;set;}
    public Map<String, List<CFCPurchaseHistory.contractLineItemWrapper>> mapContractItems	{get;set;}
    
    public CFCOrderHistoryController(){ 
        VerifyPardot();
        searchOrderHistory();
    }
    
    public void VerifyPardot(){
        system.debug('CFCHomeController.GenerateViewProducts.VerifyPardot()');
        isPardotActive = false;
        
        Pardot_Setings__c cs = Pardot_Setings__c.getInstance('Pardot General');
        if(cs != null && cs.Pardot_Status__c != null){
            isPardotActive = cs.Pardot_Status__c == 'Active'? true : false;
        }        
        system.debug('CFCHomeController.GenerateViewProducts.isPardotActive >>> ' + isPardotActive);
    }
    
    public List<SelectOption> getOptionsOrderBy() {
        List<SelectOption> options = new LIST<SelectOption>();
        options.add(new SelectOption('Past 6 months', 'Past 6 months'));
        Datetime dt = Datetime.now();
        for(Integer i=1;i<=3;i++)
        {
            options.add(new SelectOption(String.valueOf(dt.year()), String.valueOf(dt.year())));
            dt = dt.addYears(-1);
        }
        
        return options;
    }        
    
    public pageReference MethodOne() { 
        system.debug('CFCFavoritesController.MethodOne.selectedValueOrderBy >>> ' + selectedValueOrderBy);
        searchOrderHistory();
        return null;
    }   
    
    public void searchOrderHistory() {
        Datetime currentDate = Datetime.now();
        
        User userCFC = UserDAO.getInstance().getUserById(UserInfo.getUserId());
        system.debug('CFCOrderHistoryController.userCFC >>> '+ userCFC);
        
        if(selectedValueOrderBy == null || selectedValueOrderBy.equalsIgnoreCase('Past 6 months'))
        {
            lstServiceContract = [SELECT 
                                  Id,
                                  ContractNumber,
                                  Signature_Date__c, 
                                  Contract_Status__c, 
                                  OwnerId,
                                  CreatedDate
                              FROM ServiceContract 
                              WHERE AccountId =: userCFC.Contact.Account.id 
                              AND Signature_Date__c != null 
                              AND Contract_Status__c = 'Signed'
                              AND CreatedDate = LAST_N_DAYS:180];
        }else if(selectedValueOrderBy.equalsIgnoreCase(String.valueOf(currentDate.year())))
        {
                lstServiceContract = [SELECT 
                                  Id,
                                  ContractNumber,
                                  Signature_Date__c, 
                                  Contract_Status__c, 
                                  OwnerId,
                                  CreatedDate
                              FROM ServiceContract 
                              WHERE AccountId =: userCFC.Contact.Account.id 
                              AND Signature_Date__c != null 
                              AND Contract_Status__c = 'Signed'
                              AND CreatedDate = THIS_YEAR];
        }else if(selectedValueOrderBy.equalsIgnoreCase(String.valueOf(currentDate.addYears(-1).year())))
        {
            lstServiceContract = [SELECT 
                                  Id,
                                  ContractNumber,
                                  Signature_Date__c, 
                                  Contract_Status__c, 
                                  OwnerId,
                                  CreatedDate
                              FROM ServiceContract 
                              WHERE AccountId =: userCFC.Contact.Account.id 
                              AND Signature_Date__c != null 
                              AND Contract_Status__c = 'Signed'
                              AND CreatedDate = LAST_YEAR];
        }else if(selectedValueOrderBy.equalsIgnoreCase(String.valueOf(currentDate.addYears(-2).year())))
        {
            lstServiceContract = [SELECT 
                                  Id,
                                  ContractNumber,
                                  Signature_Date__c, 
                                  Contract_Status__c, 
                                  OwnerId,
                                  CreatedDate
                              FROM ServiceContract 
                              WHERE AccountId =: userCFC.Contact.Account.id 
                              AND Signature_Date__c != null 
                              AND Contract_Status__c = 'Signed'
                              AND CreatedDate = LAST_N_YEARS:2];
        }
        
        historicoNulo = lstServiceContract.size() > 0 ? false : true;
        
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.lstServiceContract >>> ' + lstServiceContract);
        
        Set<String> idServiceContract = new Set<String>();
        for(ServiceContract service : lstServiceContract )
        {
        	idServiceContract.add(service.Id);
        }
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.idServiceContract >>> ' + idServiceContract);
        
        lstContractItems = ContractLineItemDao.getInstance().listBySetServiceContract(idServiceContract);
        
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.lstContractItems >>> ' + lstContractItems);        
        Set<String> setValidServiceContract = new Set<String>();
        for(ContractLineItem ci : lstContractItems){
            setValidServiceContract.add(ci.ServiceContractId);
        }
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.setValidServiceContract >>> ' + setValidServiceContract);        
        lstServiceContract.clear();
        lstServiceContract = [SELECT Id,ContractNumber,Signature_Date__c,Contract_Status__c,OwnerId,CreatedDate FROM ServiceContract WHERE AccountId =: userCFC.Contact.Account.id AND Signature_Date__c != null AND Contract_Status__c = 'Signed' AND id IN :setValidServiceContract];
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.lstServiceContract >>> ' + lstServiceContract);
        
        mapContractItems = new Map<String, List<CFCPurchaseHistory.contractLineItemWrapper>>();    
        
        CFCPurchaseHistory ph = new CFCPurchaseHistory(lstServiceContract,lstContractItems);
        lstServiceWrapper = ph.serviceContractList;
        lstContractLineItemWrapper = ph.contractItemList;
        
        for(CFCPurchaseHistory.contractLineItemWrapper contract : lstContractLineItemWrapper){
            if(mapContractItems.containsKey(contract.ServiceContractId)){
                mapContractItems.get(contract.ServiceContractId).add(contract);
            }else
            {
                controleMap = false;
                List<CFCPurchaseHistory.contractLineItemWrapper> tempContract = new List<CFCPurchaseHistory.contractLineItemWrapper>();
                tempContract.add(contract);
                mapContractItems.put(contract.ServiceContractId, tempContract);
            }
        }
        numberOrders = String.valueOf(lstServiceWrapper.size());
        
        /*for(Contract_Items__c contract :lstContractItems)
        {
            if(mapContractItems.containsKey(contract.Service_Contract__c))
            {
                mapContractItems.get(contract.Service_Contract__c).add(contract);
            }else
            {
                controleMap = false;
                List<Contract_Items__c> tempContract = new List<Contract_Items__c>();
                tempContract.add(contract);
                mapContractItems.put(contract.Service_Contract__c, tempContract);
            }
        }*/
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.lstServiceWrapper >>> ' + lstServiceWrapper);
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.lstContractLineItemWrapper >>> ' + lstContractLineItemWrapper);
        system.debug('CFCOrderHistoryController.CFCOrderHistoryController.mapContractItems >>> ' + mapContractItems);
        
    }     
}