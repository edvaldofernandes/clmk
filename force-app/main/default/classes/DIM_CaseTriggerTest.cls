// Used by DIM - Market Intelligence.
@isTest
private class DIM_CaseTriggerTest {

    @isTest(SeeAllData=true)
    static void checkTriggerBeforeInsertWithValidUser() {

        Case caso = DIM_TestUtilsTest.newCase();
        caso.SuppliedEmail = 'salestools.commercial@embraer.com.br';
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;

        List<User> users = [SELECT Id, Email FROM User WHERE Email LIKE 'salestools.commercial@embraer.com.br%' LIMIT 1];

        System.assertEquals(1, users.size());        
        System.assert(caso.SuppliedEmail.contains('salestools.commercial@embraer.com.br'));
        System.assert(users[0].Email.contains('salestools.commercial@embraer.com.br'));
        
        caso.Requested_By__c = users[0].Id;
        update caso;
        System.assertEquals(users[0].Id, caso.Requested_By__c);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkTriggerBeforeInsertWithInvalidUser() {

        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;

        List<User> users = [SELECT Id FROM User WHERE Email LIKE :caso.SuppliedEmail LIMIT 1];
        System.assertEquals(0, users.size());        
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static testMethod void checkTriggerBeforeUpdateWithValidStatusChange() {

        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        caso.Status = 'In Progress';
        update caso;
        
        caso = [SELECT Id, Case_Folder_Id__c FROM Case WHERE Id =: caso.Id];
        System.assertNotEquals(null, caso.Case_Folder_Id__c); 
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void checkTriggerBeforeUpdateWithValidStatusChangeAndFirstCaseOfTheYear() {
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
        
        caso.Status = 'In Progress';
        update caso;        
        
        caso = [SELECT Id, Case_Folder_Id__c FROM Case WHERE Id =: caso.Id];
        String currentYear = String.valueOf(System.Today().Year() - 2000);
        System.assertEquals(currentYear + '0001', caso.Case_Folder_Id__c); 
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void checkTriggerBeforeUpdateWithValidStatusChangeAndSecondCaseOfTheYear() {
    
        Case caso1 = DIM_TestUtilsTest.newCase();
        caso1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso1;
        
        caso1.Status = 'In Progress';
        update caso1;
        
        Case caso2 = DIM_TestUtilsTest.newCase();
        caso2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso2;
        
        caso2.Status = 'In Progress';
        update caso2;        
        
        caso2 = [SELECT Id, Case_Folder_Id__c FROM Case WHERE Id =: caso2.Id];
        String currentYear = String.valueOf(System.Today().Year() - 2000);
        System.assertEquals(currentYear + '0002', caso2.Case_Folder_Id__c); 
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void checkTriggerBeforeInsertSales() {
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
        insert caso;      
        
        caso = [SELECT Id, Case_Folder_Id__c FROM Case WHERE Id =: caso.Id];
        String currentYear = String.valueOf(System.Today().Year() - 2000);
        System.assertEquals(currentYear + '0001', caso.Case_Folder_Id__c); 
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    static testMethod void checkTriggerWithPendingFeedback() {   
    
        DIM_Case_Feedback_Settings__c setting = new DIM_Case_Feedback_Settings__c();
        setting.Name = 'Is To Block Pending?';
        setting.Is_To_Block_Pending__c = true;
        insert setting;
    
        Case caso = DIM_TestUtilsTest.newCase();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        insert caso;
    
        Feedback__c feedback = new Feedback__c();
        feedback.Status__c = 'Pending';
        feedback.OwnerId = caso.Requested_By__c;
        feedback.Case__c = caso.Id;
        insert feedback;
     
        Case caso2 = DIM_TestUtilsTest.newCase();
        caso2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        
        try {        
            insert caso2;        
        }
        catch(Exception e) {
            System.assertEquals(e.getMessage().contains('Feedbacks'), true);
        }   
    }
}