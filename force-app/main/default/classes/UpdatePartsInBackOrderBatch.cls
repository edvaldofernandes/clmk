global class UpdatePartsInBackOrderBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, Is_On_Back_Order_FLL__c, Is_On_Back_Order_LBG__c, Is_On_Back_Order_SIN__c, Is_On_Back_Order_SJK__c, Top_Most__c FROM LLPDatabase__c');
    }
    global void execute(Database.BatchableContext bc, List<LLPDatabase__c> partNumbers){
        List<Case> backOrders = [SELECT Top_Most_Ecode__c, PRC_Support_Plant__c FROM Case 
                                 WHERE Status!='Closed' AND Status!='Cancelled' 
                                 AND (RecordType.Name='Pool Exchanges' OR RecordType.Name='MSPReplesh') 
                                 AND Phase__c='Back Order' 
                                 AND (PRC_Support_Plant__c='FLL' OR PRC_Support_Plant__c='LBG' OR PRC_Support_Plant__c='SIN' OR PRC_Support_Plant__c='SJK')];
        
        for(LLPDatabase__c partNumber : partNumbers){
            partNumber.Is_On_Back_Order_FLL__c = false; 
            partNumber.Is_On_Back_Order_LBG__c = false;
            partNumber.Is_On_Back_Order_SIN__c = false;
            partNumber.Is_On_Back_Order_SJK__c = false;
            for(Case backOrder : backOrders){
                if(partNumber.Top_Most__c == backOrder.Top_Most_Ecode__c){
                    if(backOrder.PRC_Support_Plant__c == 'FLL'){
                        partNumber.Is_On_Back_Order_FLL__c = true;
                    }else if(backOrder.PRC_Support_Plant__c == 'LBG'){
                        partNumber.Is_On_Back_Order_LBG__c = true;
                    }else if(backOrder.PRC_Support_Plant__c == 'SIN'){
                        partNumber.Is_On_Back_Order_SIN__c = true;
                    }else{
                        partNumber.Is_On_Back_Order_SJK__c = true;
                    }
                }
            }
        }
        update partNumbers;
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}