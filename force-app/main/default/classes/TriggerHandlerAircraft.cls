/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Trigger Handler for Aircraft Object       
* @comments: This version use TriggerRollupAircraft_Test to cover test
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
public class TriggerHandlerAircraft implements GEN_ITrigger{ 
    
    public List<Aircraft__c> newList;
    public List<Aircraft__c> oldList;
    public Map<Id, Aircraft__c> newMapRecords;
    public Map<Id, Aircraft__c> oldMapRecords;
   
    private static Boolean recursive = false;
    
    /**
    * @description : Constructor setting all Trigger attributes
    **/
    public TriggerHandlerAircraft() {
        this.newList = (List<Aircraft__c>) Trigger.New;
        this.oldList = (List<Aircraft__c>) Trigger.Old;
        this.oldMapRecords = (Map<Id, Aircraft__c>) Trigger.OldMap;
        this.newMapRecords = (Map<Id, Aircraft__c>) Trigger.NewMap;
    }
    
    /**
    * @description : Bulk before execute all before methods
    **/
    public void bulkBefore() { 
        /**  Implement when it is necessary
            switch on Trigger.operationType {
                
                when BEFORE_DELETE {
                }
                when BEFORE_INSERT {
                }
                when BEFORE_UPDATE {
                }
                when else {
                    system.debug('TriggerHandlerAircraft.bulkBefore.Trigger.operationType: ' + Trigger.operationType);
                }
            }
		**/
    }
    
    /**
    * @description : Bulk after execute all after methods
    **/
    public void bulkAfter() {
        switch on Trigger.operationType {
            
            when AFTER_DELETE {
            new TriggerRollupAircraft(oldList);

            
            }
            when AFTER_INSERT {
                new TriggerRollupAircraft(newList);
                TriggerHandlerAircraft.recursive = true;

                
                

            }
            when AFTER_UNDELETE {
                new TriggerRollupAircraft(newList);
            }
            when AFTER_UPDATE {
                if(!TriggerHandlerAircraft.recursive){
                    new TriggerRollupAircraft(newList);
                }
                TriggerHandlerAircraft.recursive = true;

                
                
            }
            when else {
                system.debug('TriggerHandlerAircraft.bulkAfter.Trigger.operationType: ' + Trigger.operationType);
            }
        }
    }    
    
    public void andFinally() {
        system.debug('TriggerHandlerAircraft.andFinally');
    }
}