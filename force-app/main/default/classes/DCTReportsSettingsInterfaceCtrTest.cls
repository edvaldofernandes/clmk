@IsTest 
public with sharing class DCTReportsSettingsInterfaceCtrTest
{
        
        
        static testMethod void unitTest1()
        {
            
            DCTReportsSettings__c goals = new DCTReportsSettings__c();
            goals.name = string.valueOf(date.today().year());
            goals.YearGoal__c = 1000;
            goals.JanuaryGoal__c = 100;
            goals.FebruaryGoal__c= 200;
            goals.MarchGoal__c= 300;
            goals.AprilGoal__c= 400;
            goals.MayGoal__c= 500;
            goals.JuneGoal__c= 600;
            goals.JulyGoal__c= 700;
            goals.AugustGoal__c= 800;
            goals.SeptemberGoal__c= 900;
            goals.OctoberGoal__c= 1000;
            goals.NovemberGoal__c= 1100;
            goals.DecemberGoal__c= 1200;
            goals.JanuaryGoalSECD__c = 100;
            goals.FebruaryGoalSECD__c= 200;
            goals.MarchGoalSECD__c= 300;
            goals.AprilGoalSECD__c= 400;
            goals.MayGoalSECD__c= 500;
            goals.JuneGoalSECD__c= 600;
            goals.JulyGoalSECD__c= 700;
            goals.AugustGoalSECD__c= 800;
            goals.SeptemberGoalSECD__c= 900;
            goals.OctoberGoalSECD__c= 1000;
            goals.NovemberGoalSECD__c= 1100;
            goals.DecemberGoalSECD__c= 1200;
            
            goals.JanuaryGoalSECPROD__c = 100;
            goals.FebruaryGoalSECPROD__c= 200;
            goals.MarchGoalSECPROD__c= 300;
            goals.AprilGoalSECPROD__c= 400;
            goals.MayGoalSECPROD__c= 500;
            goals.JuneGoalSECPROD__c= 600;
            goals.JulyGoalSECPROD__c= 700;
            goals.AugustGoalSECPROD__c= 800;
            goals.SeptemberGoalSECPROD__c= 900;
            goals.OctoberGoalSECPROD__c= 1000;
            goals.NovemberGoalSECPROD__c= 1100;
            goals.DecemberGoalSECPROD__c= 1200;


            insert goals;
            
            
            PageReference pageRef = Page.DCTReportsSettingsInterface;
            
            ApexPages.currentPage().getParameters().put('origin','SkylineReport');
            
            DCTReportsSettingsInterfaceController controller = new DCTReportsSettingsInterfaceController(null); 
            
            Test.setCurrentPage(pageRef);
            
            controller.newRecord();
            controller.backToPrevious();
            controller.saveRecord();
            controller.getAvaliableYearsSettings();
            
            
            //CLMMFAReportController.ChartDataWrapper innerClass = new CLMMFAReportController.ChartDataWrapper('Teste',100,new List<Integer>{150},1,new List<String>{'teste Rodrigo'} ) ;
        
            
            
        }
        
        
        static testMethod void unitTest2()
        {
            
            DCTReportsSettings__c goals = new DCTReportsSettings__c();
            goals.name = string.valueOf(date.today().year());
            goals.YearGoal__c = 1000;
            goals.JanuaryGoal__c = 100;
            goals.FebruaryGoal__c= 200;
            goals.MarchGoal__c= 300;
            goals.AprilGoal__c= 400;
            goals.MayGoal__c= 500;
            goals.JuneGoal__c= 600;
            goals.JulyGoal__c= 700;
            goals.AugustGoal__c= 800;
            goals.SeptemberGoal__c= 900;
            goals.OctoberGoal__c= 1000;
            goals.NovemberGoal__c= 1100;
            goals.DecemberGoal__c= 1200;
            goals.JanuaryGoalSECD__c = 100;
            goals.FebruaryGoalSECD__c= 200;
            goals.MarchGoalSECD__c= 300;
            goals.AprilGoalSECD__c= 400;
            goals.MayGoalSECD__c= 500;
            goals.JuneGoalSECD__c= 600;
            goals.JulyGoalSECD__c= 700;
            goals.AugustGoalSECD__c= 800;
            goals.SeptemberGoalSECD__c= 900;
            goals.OctoberGoalSECD__c= 1000;
            goals.NovemberGoalSECD__c= 1100;
            goals.DecemberGoalSECD__c= 1200;
            
            goals.JanuaryGoalSECPROD__c = 100;
            goals.FebruaryGoalSECPROD__c= 200;
            goals.MarchGoalSECPROD__c= 300;
            goals.AprilGoalSECPROD__c= 400;
            goals.MayGoalSECPROD__c= 500;
            goals.JuneGoalSECPROD__c= 600;
            goals.JulyGoalSECPROD__c= 700;
            goals.AugustGoalSECPROD__c= 800;
            goals.SeptemberGoalSECPROD__c= 900;
            goals.OctoberGoalSECPROD__c= 1000;
            goals.NovemberGoalSECPROD__c= 1100;
            goals.DecemberGoalSECPROD__c= 1200;



            insert goals;
            
            
            PageReference pageRef = Page.DCTReportsSettingsInterface;
            
            ApexPages.currentPage().getParameters().put('origin','SkylineReport');
            ApexPages.currentPage().getParameters().put('year',string.valueOf(date.today().year()));
            
            DCTReportsSettingsInterfaceController controller = new DCTReportsSettingsInterfaceController(null); 
            
            Test.setCurrentPage(pageRef);
            
            
            controller.backToPrevious();
            controller.saveRecord();
            controller.getAvaliableYearsSettings();
            controller.newRecord();
            
            
            //CLMMFAReportController.ChartDataWrapper innerClass = new CLMMFAReportController.ChartDataWrapper('Teste',100,new List<Integer>{150},1,new List<String>{'teste Rodrigo'} ) ;
        
            
            
        }
}