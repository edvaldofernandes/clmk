public class NextActionDatemdtDAO {
    
    public NextActionDatemdtDAO(){}
    
    public List<Next_Action_Date__mdt> getAllNext_Action_Date(){
        return[ 
            SELECT Id,
            	   Priority__c,
            	   Status__c,
                   Miliseconds__c
            FROM Next_Action_Date__mdt
        ];
    }

}