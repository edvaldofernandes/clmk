/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class responsible for copying the custom fields from the opportunity line items
* to related quote line items when they are created.
*
* NAME: QuoteCopyLineItemFromOpp.cls
* AUTHOR: DPF                                                DATE: 10/12/2014
*
*******************************************************************************/

public with sharing class QuoteCopyLineItemFromOpp {

  public static void execute() {

    TriggerUtils.assertTrigger();

    Set<Id> lstQuoteId = new Set<Id>();
    list<QuoteLineItem> lstQuoteLineItem = new list<QuoteLineItem>();
    for ( QuoteLineItem iQuote : (List<QuoteLineItem>) trigger.new  )
    {
      lstQuoteLineItem.add(iQuote);
      lstQuoteId.add(iQuote.QuoteId);
    }

    map<Id, Id> mapQuoteOpportunityId = new map<Id, Id>();
    for ( Quote iQuote : [SELECT Id, OpportunityId FROM Quote WHERE Id =: lstQuoteId ])
    {
      mapQuoteOpportunityId.put(iQuote.Id, iQuote.OpportunityId);
    }
    if ( mapQuoteOpportunityId.isEmpty() ) return;

    map<Id, list<OpportunityLineItem>> mapOppOli = new Map<Id, list<OpportunityLineItem>>();
    for ( OpportunityLineItem oli: [SELECT OpportunityId, PricebookEntryId,
      End_date__c, Entry_fee__c, Hotel__c, Margin_0__c, Maximum_Discount__c,
      Pilot__c, princing__c, Region__c, Slot__c, Slot_Errors__c, Slot_Image__c,
      Slot_status__c, Start_date__c, Training_id__c, Unit__c,
      Quantity, UnitPrice, Sales_Price__c, Catalogue_Price__c, Discount__c
      FROM OpportunityLineItem
      WHERE OpportunityId =: mapQuoteOpportunityId.Values() ])
    {
      list<OpportunityLineItem> lstOli = mapOppOli.get(oli.OpportunityId);
      if ( lstOli == null )
      {
        lstOli = new list<OpportunityLineItem>();
        mapOppOli.put(oli.OpportunityId, lstOli);
      }
      lstOli.add(oli);
    }
    if ( mapOppOli.isEmpty() ) return;

    for ( QuoteLineItem iQuote : lstQuoteLineItem )
    {
      Id idOpp = mapQuoteOpportunityId.get(iQuote.QuoteId);
      if ( IdOpp == null ) continue;

      list<OpportunityLineItem> lstOli = mapOppOli.get(idOpp);
      if ( lstOli == null ) continue;

      Integer i = -1;
      for ( OpportunityLineItem oli : lstOli )
      {
        i++;
        if ( oli.PricebookEntryId == iQuote.PricebookEntryId  && oli.Quantity == iQuote.Quantity
          && oli.UnitPrice == iQuote.UnitPrice )
        {
          iQuote.End_date__c = oli.End_date__c;
          iQuote.Entry_fee__c = oli.Entry_fee__c;
          iQuote.Hotel__c = oli.Hotel__c;
          iQuote.Margin_0__c = oli.Margin_0__c;
          iQuote.Maximum_Discount__c = oli.Maximum_Discount__c;
          iQuote.Pilot__c = oli.Pilot__c;
          iQuote.princing__c = oli.princing__c;
          iQuote.Region__c = oli.Region__c;
          iQuote.Slot__c = oli.Slot__c;
          iQuote.Slot_Errors__c = oli.Slot_Errors__c;
          iQuote.Slot_Image__c = oli.Slot_Image__c;
          iQuote.Slot_status__c = oli.Slot_status__c;
          iQuote.Start_date__c = oli.Start_date__c;
          iQuote.Training_id__c = oli.Training_id__c;
          iQuote.Unit__c = oli.Unit__c;
          //iQuote.UnitPrice = oli.UnitPrice;
          //iQuote.Sales_Price__c = oli.Sales_Price__c;          
          iQuote.Catalogue_Price__c = oli.Catalogue_Price__c;
          iQuote.Discount__c = oli.Discount__c;
          break;
        }
      }
      if(lstOli.size() > i && i >= 0 ) lstOli.remove(i);
    }
  }
}