/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class 
* ContractLineItemEntryFeeUpdateNrec
*
* NAME: ContractLineItemEntryFeeUpdateNrecTest.cls
* AUTHOR: AFC                                                  DATE: 20/03/2015
*******************************************************************************/
@isTest
public with sharing class ContractLineItemEntryFeeUpdateNrecTest 
{
	private static final integer LOTE = 200;
	
	private static id accRecTypeId = RecordTypeMemory.getRecType( 'Account', 'Aircraft_OEM' );
	private static id sCtrRecTypeId = RecordTypeMemory.getRecType( 'ServiceContract', 'Aircraft_Modification' );
	
	static testMethod void myUnitTest()
	{
    
    Account acc = SObjectInstanceTest.createAccount( accRecTypeId );
    database.insert( acc );
    
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
    pbAirMod.Name = 'Aircraft Modification';
    pbAirMod.Type__c = 'Aircraft Modification / Technical Services';
    database.insert( pbAirMod );
    
    ServiceContract sContract = SObjectInstanceTest.createServiceContract( acc.Id, sCtrRecTypeId );
    sContract.Pricebook2Id = pbAirMod.Id;
    database.insert( sContract );   
    
    Product2 prod = SObjectInstanceTest.createProduct2();
    prod.Unit__c = 'Aricraft';
    prod.Product_Type__c = 'Aircraft Modification';
    prod.Family = 'Aircraft Modification';
    prod.Applicability__c = 'Turboprop';
    database.insert( prod );
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    database.insert( pbe );
    
    PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    database.insert( pbeAirMod );
			
		list< ContractLineItem > lstCLItem = new list< ContractLineItem >();
		
		ContractLineItem clItemEntryFee = SObjectInstanceTest.createContractLineItem( 
		  sContract.Id, pbeAirMod.Id );		  
	  clItemEntryFee.Princing__c = 'Entry fee';
		lstCLItem.add( clItemEntryFee );
		
		ContractLineItem clItemNrec = SObjectInstanceTest.createContractLineItem( 
		  sContract.Id, pbeAirMod.Id );		  
    clItemNrec.Princing__c = 'NREC';
    clItemNrec.Entry_fee__c = 200;
    lstCLItem.add( clItemNrec );
    
		database.insert( lstCLItem );	

		test.startTest();
		database.delete( clItemEntryFee );    
		test.stopTest();
		
		list< ContractLineItem > lstCLItemNrecUp = new list< ContractLineItem >( 
		  [ SELECT id, Entry_fee__c FROM ContractLineItem WHERE id =: clItemNrec.Id ] );		
		
		for( ContractLineItem CLItemNrecUp : lstCLItemNrecUp )
		  system.assertEquals(0, CLItemNrecUp.Entry_fee__c );
	}

	static testMethod void testLOTE()
	{
    
    Account acc = SObjectInstanceTest.createAccount( accRecTypeId );
    database.insert( acc );
    
    Id stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    Pricebook2 pbAirMod = SObjectInstanceTest.createPricebook2();
    pbAirMod.Name = 'Aircraft Modification';
    pbAirMod.Type__c = 'Aircraft Modification / Technical Services';
    database.insert( pbAirMod );
    
    ServiceContract sContract = SObjectInstanceTest.createServiceContract( acc.Id, sCtrRecTypeId );
    sContract.Pricebook2Id = pbAirMod.Id;
	  database.insert( sContract );
	   
    Product2 prod = SObjectInstanceTest.createProduct2();
    prod.Unit__c = 'Aricraft';
    prod.Product_Type__c = 'Aircraft Modification';
    prod.Family = 'Aircraft Modification';
    prod.Applicability__c = 'Turboprop';
    database.insert( prod );
    
    PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry( stdPB, prod.Id );
    database.insert( pbe );
    
    PricebookEntry pbeAirMod = SObjectInstanceTest.createPricebookEntry( pbAirMod.Id, prod.Id );
    database.insert( pbeAirMod );
      
    list< ContractLineItem > lstCLItem = new list< ContractLineItem >();
    list< ContractLineItem > lstLineItemDelete = new list< ContractLineItem >();   
    for(integer i = 0; i < LOTE; i++)
    {
	    ContractLineItem clItemEntryFee = SObjectInstanceTest.createContractLineItem( sContract.Id, pbeAirMod.Id );
	    clItemEntryFee.Princing__c = 'Entry fee';
	    lstCLItem.add( clItemEntryFee );
	    lstLineItemDelete.add( clItemEntryFee );
	    
	    ContractLineItem clItemNrec = SObjectInstanceTest.createContractLineItem( sContract.Id, pbeAirMod.Id );
	    clItemNrec.Princing__c = 'NREC';
	    clItemNrec.Entry_fee__c = 200;
	    lstCLItem.add( clItemNrec );
    }    
    database.insert( lstCLItem );
    
    test.startTest();
    database.delete( lstLineItemDelete );    
    test.stopTest();
    
    list< ContractLineItem > lstCLItemNrecUp = new list< ContractLineItem >( [ SELECT id, Entry_fee__c FROM ContractLineItem WHERE Princing__c =: 'NREC' ] );   
    
    for( ContractLineItem CLItemNrecUp : lstCLItemNrecUp )
      system.assertEquals(0, CLItemNrecUp.Entry_fee__c );
	}
}