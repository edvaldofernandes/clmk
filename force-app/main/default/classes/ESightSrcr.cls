global class ESightSrcr {
    
    webservice Decimal cr;
    webservice String family;
    webservice long operatorId;
    webservice String operatorName;
    webservice Decimal rateCr;
    webservice Decimal rateSr;
    webservice DateTime referenceDate;
    webservice Decimal sr;
    webservice List<SrcrRankCrs> srcrRankCrsList;
    webservice List<RankSr> rankSrList;
    
    public ESightSrcr(){
        srcrRankCrsList = new List<SrcrRankCrs>();
        rankSrList = new List<RankSr>();
    }
    
    public ESightSrcr(List<SrcrRankCrs> srcrRankCrsList, List<RankSr> rankSrList){
        
        this.srcrRankCrsList = srcrRankCrsList;
        this.rankSrList = rankSrList;     
    }
}