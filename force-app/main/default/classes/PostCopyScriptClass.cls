/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com.br">
* @version 1.0, &nbsp; Mar-2017.
**/
global class PostCopyScriptClass implements Database.batchable<SObject>
{ 
  
   private map<string,string> keyObjectTypeMap = new map<string,string>();
   private map<string,map<string,string>> objectFieldTypeMap = new map<string,map<string,string>>();
   private Map<String,Schema.SobjectType> describeMap = Schema.getGlobalDescribe();
   private integer counterFields = 0;
   private list<id> idRecordsToHardDelete = new list<Id>();
   private string errorMessages = '';
   
   
   global Iterable<Sobject> start(Database.batchableContext info)
   { 
       Iterable<SObject> myIter = (Iterable<SObject>) new CustomIterableClass();
       return myIter; 
   }     
   
    global void execute(Database.batchableContext info, List<SObject> scope)
    { 
        string sObjectId;
        string sObjectTypeName = '';
        map<string,string> fieldTypeMap = new map<string,string>();            
        list<sObject> sObjectsToUpdateList = new list<sObject>();
        list<sObject> sObjectsToDeleteList = new list<sObject>();
        string stringTemp = '';
        string phoneTemp = '';
        
        populatesObjectMap();
               
        for(sObject tempObject : scope)
        {
            sObjectId = (string)tempObject.get('Id');
               
            if(keyObjectTypeMap.containsKey(sObjectId.left(3)))
                sObjectTypeName = keyObjectTypeMap.get(sObjectId.left(3));
               
               
            if(sObjectTypeName == 'Account' || sObjectTypeName == 'Contact')
            {
                if(!objectFieldTypeMap.containsKey(sObjectTypeName))
                    populateFieldsMap(sObjectTypeName);
                   
                fieldTypeMap = objectFieldTypeMap.get(sObjectTypeName);
                    
                for(string fieldName : fieldTypeMap.keySet())
                {
                    if(fieldTypeMap.get(fieldName) == 'PHONE')
                    {
                        tempObject.put(fieldName,string.valueOf(counterFields).left(16));
                    }
                    else
                    {
                        tempObject.put(fieldName,'phonyEmail_' + string.valueOf(counterFields) + '@mail.com'); 
                    }
                    

                }
                counterFields++;                
                sObjectsToUpdateList.add(tempObject);    
            }
            else
            {
                sObjectsToDeleteList.add(tempObject);    
                idRecordsToHardDelete.add(tempObject.Id);
            }
        }                               
        
        if(!sObjectsToUpdateList.isEmpty())
        {
            for (Database.SaveResult sr : database.Update(sObjectsToUpdateList,false)) 
            {
                if (!sr.isSuccess()) 
                {
                    for(Database.Error err : sr.getErrors()) 
                        errorMessages += sr.getId() + ' - ' + err.getStatusCode() + ': ' + err.getMessage() + '/n'; 
                }                        
            }
        }    
        
        if(!sObjectsToDeleteList.isEmpty())
        {
        
            for (Database.DeleteResult dr : database.delete(sObjectsToDeleteList,false)) 
            {
                if (!dr.isSuccess()) 
                {
                    for(Database.Error err : dr.getErrors()) 
                        errorMessages += dr.getId() + ' - ' + err.getStatusCode() + ': ' + err.getMessage() + '/n'; 
                }                        
            }
        
        
        
        
        }                                 
           
    }     
   
    global void finish(Database.batchableContext info)
    {   
       
        if(!idRecordsToHardDelete.isEmpty())
        {
            
            for (database.EmptyRecycleBinResult emptyR : database.emptyRecycleBin(idRecordsToHardDelete)) 
            {
                if (!emptyR.isSuccess()) 
                {
                    for(Database.Error err : emptyR.getErrors()) 
                        errorMessages += emptyR.getId() + ' - ' + err.getStatusCode() + ': ' + err.getMessage() + '/n'; 
                }                        
            }
        }
        
        
        System.Debug(string.isEmpty(errorMessages) + ' tá vazio'); 
        if(!string.isEmpty(errorMessages))
            createTextErrorFile();
   } 
   
   private void createTextErrorFile()
   {
        Folder folderTemp = [Select Id from Folder Where Name = 'Shared Documents' Limit 1];
        Document errorfile = new Document();
        errorfile.Name = 'Post Copy Script Error File';
        errorfile.Body = Blob.valueOf(errorMessages);
        errorfile.ContentType = 'text/plain';
        errorFile.FolderId = folderTemp.id;
        errorfile.Type = 'txt';
        insert errorfile;
   }
   
   private void populatesObjectMap()
   {
       
        for(String objectName : describeMap.keyset())
            keyObjectTypeMap.put(describeMap.get(objectName).getDescribe().getKeyPrefix(),objectName);
       
       return;
   }
   
   private void populateFieldsMap(string p_sObjectName)
   {
       map<String, Schema.SObjectField> fieldMap = describeMap.get(p_sObjectName).getDescribe().fields.getMap();
       Schema.SObjectField field;
       map<string,string> fieldTypeMap = new map<string,string>();
       
       for(string fieldName : fieldMap.keySet())
       {
           field = fieldMap.get(fieldName);
           if(string.ValueOf(field.getDescribe().getType()) == 'EMAIL' || string.ValueOf(field.getDescribe().getType()) == 'PHONE')
               fieldTypeMap.put(fieldName,string.ValueOf(field.getDescribe().getType()));                    
       }    
       objectFieldTypeMap.put(p_sObjectName,fieldTypeMap);
       
       
       return;
   }
   
   
   
}