/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class QuoteUpdateSlot
* NAME: QuoteUpdateSlotTest.cls
* AUTHOR: KHPS                                                DATE: 20/12/2014
*
*******************************************************************************/

@isTest
private class QuoteUpdateSlotTest {

		private static final id recTypeAcc = RecordTypeMemory.getRecType('Account', 'Operator');

    static testMethod void testeFuncional() {
    	
    	Id pbkId = SObjectInstanceTest.catalogoDePrecoPadrao();
    	
    	Product2 prod = SObjectInstanceTest.createProduct2(); 	
    	Database.insert(prod);
    	
    	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pbkId, prod.Id);
    	Database.insert(pbe);

    	Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
    	Database.insert(acc);
    	
    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.AccountId = acc.Id;
    	opp.Fleet_Type__c = 'EJET';
    	Database.insert(opp);    	
    	
    	Quote cotacao = SObjectInstanceTest.createQuote(opp.Id);
    	cotacao.Pricebook2Id = pbkId;
    	Database.insert(cotacao);
    	
    	QuoteLineItem qli = SObjectInstanceTest.createQuoteLineItem(cotacao.Id, pbe.Id);
    	qli.Start_date__c = System.today();
    	qli.End_date__c = System.today().addDays(10);
    	qli.Quantity = 100;
    	Database.insert(qli);  	
    	
    	Test.startTest();
    	Database.delete(cotacao);
    	Test.stopTest();    	
    	
    }
    
		static testMethod void testeUpdate() {
			
    	Id pbkId = SObjectInstanceTest.catalogoDePrecoPadrao();
    	
    	Product2 prod = SObjectInstanceTest.createProduct2();
    	Database.insert(prod);
    	
    	PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(pbkId, prod.Id);
    	Database.insert(pbe);

    	Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
    	Database.insert(acc);
    	
    	Opportunity opp = SObjectInstanceTest.createOpportunity();
    	opp.AccountId = acc.Id;
    	opp.Fleet_Type__c = 'EJET';
    	Database.insert(opp);     	
    	
    	Quote cotacao = SObjectInstanceTest.createQuote(opp.Id);
    	cotacao.Status = 'In Review';
    	cotacao.Pricebook2Id = pbkId;
    	Database.insert(cotacao);
    	
    	QuoteLineItem qli = SObjectInstanceTest.createQuoteLineItem(cotacao.Id, pbe.Id);
    	qli.Start_date__c = System.today();
    	qli.End_date__c = System.today().addDays(10);
    	qli.Quantity = 100;
    	Database.insert(qli);  	
    	
    	cotacao.Status = 'Denied';
    	Test.startTest();
    	Database.update(cotacao);
    	Test.stopTest(); 			
			
		}
		
}