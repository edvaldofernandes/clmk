/**
* @author Rodrigo Gimenes Rodrigues.   
* @author <a href="mailto:rodrigo.gimenes@globmail.com">
* @version 1.0, &nbsp; Jun-2016.
**/
public class PublicCalendarViewController
{

    public list<eventWrapper> events {get;set;}
    public string selectedCalendarName{get;set;}
    public string selectedCalendarId{get;set;}
    public string buttonCaption{get;set;}
    public string selectedDate{get;set;}
    public string filterEventType{get;set;}
    public string filterSubject{get;set;}
    public string filterTextPrintable{get;set;}
    

    public class eventWrapper
    {
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startDate {get;private set;}
        public String endDate {get;private set;}
        public String url {get;set;}
        public String className {get;set;}
    }
    
    
    private map<string,string> eventClassMap;
    private string dateFormat = 'yyyy-MM-dd HH:mm:ss';

    public PublicCalendarViewController(ApexPages.StandardController controller) 
    {
        
        eventClassMap = new map<string,string>();
        eventClassMap.put('Busy','event-busy');
        eventClassMap.put('OutOfOffice','event-outOfOffice');
        string eventParameter = (string.IsEmpty(filterEventType) ? '' : '?eventType='+ filterEventType) ;
        string subjectParameter = (string.IsEmpty(filterSubject) ? '' : '?subject='+ filterSubject) ;
        filterTextPrintable = '<b>Filtered by : <br> Public Calendar Name - </b>' + selectedCalendarName  + ' <br> <b>Event Type - </b>' + (string.IsEmpty(eventParameter) ? ' --None--' : eventParameter)  + '<br><b>Subject - </b>' + (string.IsEmpty(subjectParameter) ? ' --None--' : subjectParameter) + '<br><b>Printed By : '+ UserInfo.getName() + '</b>';
        selectedDate = date.today().format();
        clearFilters();
        
        


    }
    
    public PublicCalendarViewController() 
    {

        eventClassMap = new map<string,string>();
        eventClassMap.put('Busy','event-busy');
        eventClassMap.put('OutOfOffice','event-outOfOffice');
        clearFilters();
        filterTextPrintable = '<b>Filtered by : <br> Public Calendar Name : </b>' + selectedCalendarName  + ' <br> <b>Event Type : </b>' + (string.IsEmpty(filterEventType) ? ' --None--' : filterEventType)  + '<br><b>Subject : </b>' + (string.IsEmpty(filterSubject ) ? ' --None--' : filterSubject) + '<br><b>Printed By : '+ UserInfo.getName() + '</b>';        
        


    }

    public pageReference clearFilters()
    {
        

        string calendarIdParameter = ApexPages.currentPage().getParameters().get('calendarId');
        string calendarNameParameter = ApexPages.currentPage().getParameters().get('calendarName');
        string eventTypeParameter = ApexPages.currentPage().getParameters().get('eventType');
        string subjectParameter = ApexPages.currentPage().getParameters().get('subject');
        
        selectedCalendarName = 'No Calendar Selected';
        filterEventType = '--None--';
        filterSubject = '';

        if(string.IsNotEmpty(calendarIdParameter))
            selectedCalendarId = calendarIdParameter;
            
        if(string.IsNotEmpty(calendarNameParameter))
            selectedCalendarName = calendarNameParameter;
        
        if(string.IsNotEmpty(eventTypeParameter))
            filterEventType = eventTypeParameter;
        
        if(string.IsNotEmpty(subjectParameter))
            filterSubject = subjectParameter;    

        updateEventParameters();
        
        
        return Null;
    }
    
    public PageReference InvokeCallReport()
    {
        string eventParameter = (string.IsEmpty(filterEventType) ? '' : '&eventType='+ EncodingUtil.urlEncode(filterEventType,'UTF-8')) ;
        string subjectParameter = (string.IsEmpty(filterSubject) ? '' : '&subject='+ EncodingUtil.urlEncode(filterSubject,'UTF-8')) ;
        
        PageReference p = new PageReference('/apex/PublicCalendarPrintableView?calendarId='+selectedCalendarId+'&calendarname='+EncodingUtil.urlEncode(selectedCalendarName,'UTF-8') + eventParameter+subjectParameter);
        p.setredirect(true);
        return p;

    }
    

    public List<SelectOption> getSelectedCalendarNames()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Map<String, PublicCalendarSettings__c> calendars = PublicCalendarSettings__c.getAll();
        
        options.add(new SelectOption('', 'No Calendar Selected'));
        
        for (PublicCalendarSettings__c calendarTemp : calendars.values()) 
            options.add(new SelectOption(calendarTemp.calendarId__c, calendarTemp.Name));


                
        return options;
    }
    
    public List<SelectOption> getFilterEventTypes()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('', '--None--'));
        
        Schema.DescribeFieldResult fieldResult = Event.Event_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
           options.add(new SelectOption(f.getLabel(), f.getValue()));
   
       return options;
       
    }
    
    
    public PageReference pageLoad() 
    {
        
        DateTime startDateCalendar; 
        DateTime endDateCalendar;       
        eventWrapper myEvent;
        events = new list<eventWrapper>();
        string query = '';
        string filters = buildSOQL();
        
        if(string.IsNotEmpty(filters))
        {
            query = 'select Id, Subject,Owner.Name, ShowAs,isAllDayEvent, StartDateTime, EndDateTime from Event ' + (filters.length()>0 ? ' Where '  + filters : ' ') ;
            
            for(Event eventTemp: database.query(query))
            //for(Event eventTemp: [select Id, Subject,Owner.Name, ShowAs,isAllDayEvent, StartDateTime, EndDateTime from Event where OwnerID = :selectedCalendarId])
            {
                selectedCalendarName = eventTemp.Owner.Name;
                startDateCalendar = eventTemp.StartDateTime;
                endDateCalendar = eventTemp.EndDateTime;
                
                if(eventTemp.isAllDayEvent)
                     startDateCalendar = startDateCalendar.addDays(1);
                
                 myEvent = new eventWrapper();
                 myEvent.title = eventTemp.Subject;
                 myEvent.allDay = eventTemp.isAllDayEvent;
                 myEvent.startDate = startDateCalendar.format(dateFormat , string.valueOf(UserInfo.getTimeZone()));
                 myEvent.endDate = endDateCalendar.format(dateFormat, string.valueOf(UserInfo.getTimeZone()));
                 myEvent.url = '/' + eventTemp.Id;
                 myEvent.className = eventClassMap.get(eventTemp.showAs);
                 
                 events.add(myEvent);
            }
        }    
        return null;

    }
    
    public PageReference updateEventParameters() 
    {
        string viewName = Apexpages.currentPage().getParameters().get('calendarDate');
        
        if(viewName == 'month')
        {
            buttonCaption  =  'This Month';
        }
        else if(viewName == 'agendaWeek')
        {
            buttonCaption  =  'This Week';
        }
        else
        {
            buttonCaption  =  'Today';
        }
        
        return null;
    }

    public PageReference newEvent() 
    {
        PageReference  pageRef;
        string url = '';
       
        
        if(selectedCalendarId == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'To create a new event, you must select and load an existing Calendar.'));
            return null;
        }
        
        url = '/00U/e?'; 
        url += 'evt4=' + EncodingUtil.urlEncode(selectedDate, 'UTF-8');
        url += '&aid=' + EncodingUtil.urlEncode(selectedCalendarId, 'UTF-8');
        url += '&anm=' + EncodingUtil.urlEncode(selectedCalendarName, 'UTF-8');
        url += '&retURL=/apex/PublicCalendarView?calendarId=' + selectedCalendarId;
        
        pageRef = new PageReference(url);        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    private string buildSOQL()
    {
        
        string filter = '';
        string formatedDate = '';
        
        
        
        try
        {
        
            if(string.IsNotEmpty(selectedCalendarId) && selectedCalendarId != 'null' )
                filter = ' OwnerID = \'' + selectedCalendarId + '\''  ;
            
            if(filterEventType != '--None--' && string.IsNotEmpty(filterEventType) )
                filter += (filter.length()>0 ? ' and ' : ' ') + ' Event_Type__c = \'' + filterEventType + '\'';
                
            if(string.IsNotEmpty(filterSubject))
                filter += (filter.length()>0 ? ' and ' : ' ') + ' Subject = \'' + filterSubject + '\'';
    
        
           System.Debug('Filter - ' + filter);
            
        }
        catch(exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error creating SOQL filter - ' + ex.getMessage()));
            return null;
        }
    
        return filter;
    
    }

}