/* Author: Fabiano Albino Ferreira - TCS
 * 
 * Description: Test class for MFIRDAO
 * 
 * CreatedDate: 20/09/2019.
 * 
 * LastModifiedDate: 20/09/2019
 */

@IsTest
public class MFIRDAOTest {
    
    @TestSetup
    private static void testSetup(){
        
        Account acc = createAccount();
        Database.insert(acc);
        
        MFIR__c mfir = createMFIR(acc.Id);
        Database.insert(mfir);
        
    }
    
    @IsTest
    private static void shouldGetMFIRByNumberORSapName(){
     	
        Set<String> mfirNumbers = new Set<String>{'335353'};
        
        Test.startTest();
       
        	List<MFIR__c> listMfir = new MFIRDAO().getMFIRByNumberORSapName(mfirNumbers);
        
        	System.assert(listMfir != null);
        	System.assertEquals('335353', listMfir[0].Mfir_Number__c);
        
        Test.stopTest();
        
    }
    
    private static MFIR__C createMFIR(Id accountId){
        
        MFIR__c mfir = new MFIR__c(
        	MFIR_number__c = '335353',
            SAP_Name__c = 'nome_sap_test',
            Account__c = accountId
        );
       
        return mfir;
    }
    
    private static Account createAccount(){
        
        Account acc = new Account(
        	BillingCountry = 'Brazil',
        	Name = 'Test',
        	Company_Nickname__c = 'testNickname',
        	FlyEmbraerId__c = '99999'
        );
       
        return acc;
    }
  
}