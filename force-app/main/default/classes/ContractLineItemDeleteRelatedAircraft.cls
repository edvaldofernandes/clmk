/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Class responsible for deleting related aircraft when a contract line item
* is deleted.
*
* NAME: ContractLineItemDeleteRelatedAircraft.cls
* AUTHOR: DPF                                                DATE: 09/12/2014
*******************************************************************************/

public with sharing class ContractLineItemDeleteRelatedAircraft {

  public static void execute() {
    
    TriggerUtils.assertTrigger();
    
    List<Id> lstProdutoOppId = new List<Id>();
    for ( ContractLineItem oli: (List<ContractLineItem>) trigger.old)
    {
      lstProdutoOppId.add(oli.Id);
    }
    if ( lstProdutoOppId.isEmpty() ) return;
    
    List<Related_Aircraft__c> lstRelAircrafts = [ SELECT Id FROM Related_Aircraft__c 
      WHERE Contract_Line_Item__c =: lstProdutoOppId ];
      
    if ( !lstRelAircrafts.isEmpty() ) delete lstRelAircrafts;
  }
  
}