/* Classe implementadora de SOBjectDAO para operações DML no objeto Favoritee__c, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 07/01/2016
*/
public without sharing class FavoriteDao {
    
    private static final FavoriteDao instance = new FavoriteDao();    
    
    private FavoriteDao(){
    }    
    
    public static FavoriteDao getInstance(){
        return instance;
    }
    
    public Favorite__c getByUserIdAndProductId (String idUser, String idProduct){
        List<Favorite__c> listFavorite = database.query(' Select ' + utils.getAllFields('Favorite__c') +
                                                        ' FROM Favorite__c WHERE User__c =\''+String.escapeSingleQuotes(idUser)+'\''+
                                                        ' AND Product__c =\''+String.escapeSingleQuotes(idProduct)+'\' LIMIT 1');
                                                        
        /*List<Favorite__c> listFavorite = [Select User__c, Product__c, OwnerId, Name, Id, ID__c, Active__c 
                                          FROM Favorite__c 
                                          WHERE User__c = :idUser AND Product__c = :idProduct
                                          LIMIT 1];*/
        if(listFavorite.size() > 0){
            return listFavorite[0];
        }
        
      	return null;
    }
    
    public Favorite__c createFavorite(String idUser, String idProduct){
        
        Favorite__c fav = new Favorite__c();
        fav.Active__c = true;
        fav.Product__c = idProduct;
        fav.User__c = idUser; 
        insert fav;
        
        return fav;
    }
    
    public Favorite__c setActive(Favorite__c fav, Boolean statusActive){
        fav.Active__c = statusActive;
        update fav;
        
        return fav;
    }
    
}