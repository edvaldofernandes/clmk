@isTest
public class CFCProductEditControllerTest {
    
    static testMethod void myUnitTest(){
        RecordType recordType = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        system.debug('CFCProductEditControllerTest.myUnitTest.recordType >>> ' + recordType);
     
        Product2 Produto = new Product2();
        Produto.Name = 'Produto Teste';
        Produto.ProductCode = 'a488';
        Produto.Product_Status__c = 'Active';
        Produto.RecordTypeId = recordType.Id;
        Produto.IsActive = true;
        //Produto.Publication_Status__c = 'Published';
        database.insert(Produto); 
        system.debug('CFCProductEditControllerTest.myUnitTest.Produto 111111 >>> ' + Produto);
        
        Produto.Publication_Status__c = 'Published';
        database.update(Produto); 
        
        system.debug('FIELDPRODUCTTEST+++' +[Select Publication_Status__c From Product2 Where Id = :Produto.Id ]);
        
        CFCProductEditController controller = new CFCProductEditController(new ApexPages.StandardController(Produto));
        controller.validatePublication();
        controller.saveProduct();
        
        system.assert(controller.fieldProduct != null);
    }
    
     static testMethod void myUnitTest2() 
    {
        RecordType recordType1 = [ Select Name, Id From RecordType Where SobjectType = 'Product2' And DeveloperName = 'Training' limit 1 ];
        system.debug('CFCProductEditControllerTest.myUnitTest.recordType >>> ' + recordType1);
     
        Product2 Produto1 = new Product2();
        Produto1.Name = 'Produto Teste';
        Produto1.ProductCode = 'a488';
        Produto1.Product_Status__c = 'Active';
        Produto1.RecordTypeId = recordType1.Id;
        Produto1.IsActive = true;
        //Produto1.Publication_Status__c = 'Not published';
        database.insert(Produto1); 
        system.debug('CFCProductEditControllerTest.myUnitTest.Produto >>> ' + Produto1);
        
        Produto1.Publication_Status__c = 'Not published';
        database.update(Produto1); 
        
        system.debug('FIELDPRODUCTTEST111+++' +[Select Publication_Status__c From Product2 Where Id = :Produto1.Id ]);
        
        CFCProductEditController controller1 = new CFCProductEditController(new ApexPages.StandardController(Produto1));
        controller1.validatePublication();
        
        system.assert(controller1.fieldProduct != null);
    }
    
}