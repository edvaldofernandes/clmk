@isTest
public class CRM360_MaterialPerformanceControllerTest {
    
    private static String accountName = 'Test Account';
    
    @testSetup 
    static void setup(){
        
        List<Account> accountsToInsert = new List<Account>();
        
        //Create an account
        Account account = new Account();
        account.Name = accountName;
        account.Company_Nickname__c = accountName;
        accountsToInsert.add(account);

        insert accountsToInsert;
    }
    
     @isTest
    public static void testGetAccount(){
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountName];
        //List<Case> Cases = [SELECT Id FROM Case WHERE Subject = :case1subject];

        Test.startTest();
        
        PageReference pageRef = Page.CRM360_MaterialPerformancePage;
        pageRef.getParameters().put('id', String.valueOf(accounts[0].Id));

        Test.setCurrentPage(pageRef);
        
        CRM360_MaterialPerformanceController testAccPlan = new CRM360_MaterialPerformanceController();
        
        testAccPlan.getAccount();
        
        Test.stopTest();


    }
}