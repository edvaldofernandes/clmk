public class RFP_OnlineController {
    
    public InputForm__c inputForm {get;set;}
    
    private static Id RecordTypeIdRFPOnline = Schema.SObjectType.InputForm__c.getRecordTypeInfosByName().get('RFP Online').getRecordTypeId();
    
    
    public RFP_OnlineController(){
        inputForm = new InputForm__c(RecordTypeId = RecordTypeIdRFPOnline);
    }
    public RFP_OnlineController( InputForm__c inputF){
        this.inputForm = inputF;
    }
    
    //Method for saving the record:
    public PageReference save(){
        try {
            System.debug('Inserting Input Form');
            if(inputForm.Subject__c != null || inputForm.Requester_Name__c != null ||  inputForm.Requester_Email__c != null ||  inputForm.Requester_Company__c != null ||  inputForm.Requester_Phone__c != null){
                insert inputForm;
                PageReference pageRef = new PageReference('/RFP_OnlineSavedPage');
                return pageRef;
                
            }
            System.debug('Did not insert Input Form');
            return null;
            
            
        } catch(DmlException e) {
            //If the save fails, throw an error:
            System.debug('An unexpected error has occurred: Failed to Save Form - ' + e.getMessage());
            return null;
        }
    }
}