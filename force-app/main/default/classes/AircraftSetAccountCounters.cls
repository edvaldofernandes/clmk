/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Classe que preenche os contadores de aeronave na conta correspondente,
* de acordo com o operador
*
* NAME: AircraftSetAccountCounters.cls
* AUTHOR: RLdO                                                 DATE: 27/05/2014
*******************************************************************************/
public class AircraftSetAccountCounters
{
  private static final String QTDE_EMBRAER_OPER      = 'Quantity_Embraer_Aircraft_Operator__c';
  private static final String QTDE_NON_EMBRAER_OPER  = 'Quantity_Non_Embraer_Aircraft_Operator__c';
  private static final String QTDE_EMBRAER_OWNER     = 'Quantity_Embraer_Aircraft_Owner__c';
  private static final String QTDE_NON_EMBRAER_OWNER = 'Quantity_Non_Embraer_Aircraft_Owner__c';
  
  private static final String FIELD_OPERADOR = 'Operator__c';
  private static final String FIELD_OWNER = 'Owner__c';
  
  // tipo de registro de aeronave Embraer: outros tipos de registro sao contados como 'Non-Embraer'
  private static Id rtAircraftEMB = RecordTypeMemory.getRecType('Aircraft__c', 'Embraer');
  // semaforo
  private static Boolean EmExecucao = false;
  private static map< Id, Account > mapContas;
  
  private static void resetContadores( set< id > aSetContaId )
  {
    if ( aSetContaId == null || mapContas == null ) return;

    for ( id lContaId : aSetContaId )
    {
    	if ( mapContas.containsKey( lContaId ) ) continue;
      Account acc = new Account( Id = lContaId );
	    acc.put( QTDE_EMBRAER_OPER, 0 );      
	    acc.put( QTDE_NON_EMBRAER_OPER, 0 );
	    acc.put( QTDE_EMBRAER_OWNER, 0 );      
	    acc.put( QTDE_NON_EMBRAER_OWNER, 0 );
      mapContas.put( acc.id, acc );
    }
  }
  
  private static void atualizaConta( String aFieldName, list< AggregateResult > aList )
  {
    for ( AggregateResult aggR: aList )
    {
      // Id da conta
      Id idAccount = (Id)aggR.get( aFieldName );
      // Id do tipo de registro da aeronave
      Id idRecTypeAcc = (Id)aggR.get('RecordTypeId');
      
      // determina o contador a ser atualizado pelo tipo de registro
      String sAccField = (idRecTypeAcc == rtAircraftEMB)
        ? ( aFieldName == FIELD_OPERADOR ? QTDE_EMBRAER_OPER : QTDE_EMBRAER_OWNER )
        : ( aFieldName == FIELD_OPERADOR ? QTDE_NON_EMBRAER_OPER: QTDE_NON_EMBRAER_OWNER );
        
      Account acc = mapContas.get(idAccount);
      if ( acc == null ) continue;

      // atualiza o contador apropriado
      decimal lValue = ( decimal )acc.get( sAccField ) + ( decimal )aggR.get( 'Qtd' );
      
      system.debug('ROG=conta(' + acc.id + ') campoOrg('+aFieldName+') campoAtualizado('+sAccField+') valorAntes('+( decimal )acc.get( sAccField )+') valorSomado(' +( decimal )aggR.get( 'Qtd' )+') valorDepois(' +lValue+')');
      
      acc.put( sAccField, lValue );
    }
  }
  
  private static void carregaContas( set< id > aSetIDs, list< Aircraft__c > aList )
  {
    if ( aList != null )
      for ( Aircraft__c lAviao : aList )
      {
        if ( lAviao.Operator__c != null ) aSetIDs.add( lAviao.Operator__c );
        if ( lAviao.Owner__c != null ) aSetIDs.add( lAviao.Owner__c );
      }
  }

  public static void processar()
  {
    // semaforiza a execucao
    if (EmExecucao) return;
    EmExecucao = true;
    
    TriggerUtils.assertTrigger();
    
    set< id > lSetIDs = new set< id >();
    carregaContas( lSetIDs, ( list< Aircraft__c > )Trigger.old );
    carregaContas( lSetIDs, ( list< Aircraft__c > )Trigger.new );
    
    processar( lSetIDs );
  }
  
  @future
  public static void processar( set< id > aSetConta )
  {
    mapContas = new map< Id, Account >();
    
    resetContadores( aSetConta );

    if ( mapContas.isEmpty() ) return;
    
    atualizaConta( FIELD_OPERADOR, [ select Operator__c, RecordTypeId, count(Id) Qtd
                                    from Aircraft__c
                                    where Operator__c = :mapContas.keySet()
                                    group by Operator__c, RecordTypeId ] );
                                    
    atualizaConta( FIELD_OWNER, [ select Owner__c, RecordTypeId, count(Id) Qtd
                                    from Aircraft__c
                                    where Owner__c = :mapContas.keySet()
                                    group by Owner__c, RecordTypeId ] );


    if ( !mapContas.isEmpty() ) update mapContas.values();
    
  }
}