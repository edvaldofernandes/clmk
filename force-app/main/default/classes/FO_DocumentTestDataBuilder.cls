@isTest
public class FO_DocumentTestDataBuilder {
    //[TEMP]_documentId_pageNumber
    String testUrl = 'https://www.salesforce.com/';
    private PageReference visualforcePage = new PageReference(testUrl);
    private List<Id> associatedContentDocuments = new List<Id>();
    private String title;
    private String filename;
    private Blob content;
    
    public FO_DocumentTestDataBuilder withTitle(String title){
        this.title = title;
        return this;
    }
    public FO_DocumentTestDataBuilder withFilename(String filename){
        this.filename = filename;
        return this;
    }    
    public FO_DocumentTestDataBuilder withVisualforcePage(PageReference visualforcePage){
        this.visualforcePage = visualforcePage;
        return this;
    }    
    public FO_DocumentTestDataBuilder withAssociatedDocuments(List<Id> associatedDocuments){
        this.associatedContentDocuments = associatedContentDocuments;
        return this;
    }   
    public FO_DocumentTestDataBuilder withContent(Blob content){
        this.content = content;
        return this;
    }
    public FO_Document build(){
        FO_Document document = new FO_Document(this.visualforcePage, this.associatedContentDocuments);
        document.setFilename(this.filename);
        document.setTitle(this.title);
        return document;
    }
}