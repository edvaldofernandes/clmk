public class DosTopMost implements Comparable {
    public string TopMost{get;set;}
    public string VendorName{get;set;}
    public string VendorNameLBG{get;set;}
    public integer Quantity{get;set;}
    public string PartDescription{get;set;} 
    public integer Rank{get;set;}
    public string ContractTerms{get;set;}
    public List<Case> Cases{get;set;} {Cases = new List<Case>();}
    public List<LLPDatabase__c> PartsOnHand{get;set;} {PartsOnHand = new List<LLPDatabase__c>();}
    public List<Core_Information__c> Cores{get;set;} {Cores = new List<Core_Information__c>();}
    Public List<Repair_Information__c> RepairInfo{get;set;} {RepairInfo = new List<Repair_Information__c>();}
    Public List<RESS_Info__c> Resses{get;set;} {Resses = new List<RESS_Info__c>();}
    Public List<QM__c> QMs{get;set;} {QMs = new List<QM__c>();}
    
    //Implement the compareTo() method
    public Integer compareTo(Object compareTo) {
        DosTopMost compareToDtm = (DosTopMost)compareTo;
        if(Quantity == compareToDtm.Quantity) return 0;
        if(Quantity < compareToDtm.Quantity) return 1;
        return -1;
    }
}