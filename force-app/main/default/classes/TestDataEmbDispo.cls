@isTest
public class TestDataEmbDispo {

 
    
    
    //creates case records for use in TESTEMBDISPO
    @isTest public static ApexPages.StandardController createEmbDispo_m1_c0(){
        //SELECT    A_C_TypeSJK__c,Aircraft_Serial_Number__r.Name,Serial_Number__c 

        case c = new case(Reason_for_Removal__c ='reason1',Shop_Findings__c='ShopFinding1', Engineering_Recommendation__c = 'Recomendation1',Disposition__c='Dispo1');
        insert c;													//insert new case record
        PageReference pageRef = Page.CreateEmbTechDispoxdp;						//create page reference instance of "CreateEmbTechDispoxdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', c.id);		//set ID of current page to the ID of the case record
        return new ApexPages.StandardController(c);					//return a standard controller for case
    }
    
    //creates case record for use in TestEmbDispo
    @isTest public static ApexPages.StandardController createEmbDispo_m1_c1(){
        Case c = new case(Reason_for_Removal__c ='',Damage_Type__c ='',Disposition__c='Dispo2',Shop_Findings__c='ShopFinding2');
        insert c;													//insert new case record
        PageReference pageRef = Page.CreateEmbTechDispoxdp;						//create page reference instance of "CreateEmbTechDispoxdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', c.id);		//set ID of current page to the ID of the case record
        return new ApexPages.StandardController(c);					//return a standard controller for case
    }
    
    //creates case record for use in TestEmbDispo
    ////blank required fields
    @isTest public static ApexPages.StandardController createEmbDispo_m1_c2(){
        case c = new case(Reason_for_Removal__c ='',Shop_Findings__c='ShopFin', Engineering_Recommendation__c = '',Disposition__c='Dispo3');
        insert c;													//insert new case record
        PageReference pageRef = Page.CreateEmbTechDispoxdp;						//create page reference instance of "CreateEmbTechDispoxdp.vfp"
        Test.setCurrentPage(pageRef);									//set current page to the above page reference
        ApexPages.currentPage().getParameters().put('id', c.id);		//set ID of current page to the ID of the case record
        return new ApexPages.StandardController(c);					//return a standard controller for case
    }
    
    
}