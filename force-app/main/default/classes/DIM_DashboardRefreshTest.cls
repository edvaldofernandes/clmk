// Used by DIM - Market Intelligence.
@isTest
public class DIM_DashboardRefreshTest {

    private class DIM_DashboardRefreshMockup implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('OK');
            res.setStatusCode(200);
            return res;
        }
    }

    //--------------------------------------------------------------------------
    @isTest(SeeAllData='true')
    public static void validateRefresh() {
         
        DIM_TestUtils.enableIsRunningTest = false;
        Dashboard dashboard = [SELECT Id FROM Dashboard LIMIT 1];
        
        Test.setMock(HttpCalloutMock.class, new DIM_DashboardRefreshMockup());
        Test.startTest();
            List<Id> dashboardIds = new List<Id>();
            dashboardIds.add(dashboard.Id);
            DIM_DashboardRefresh.callRefresh(dashboardIds);
        Test.stopTest();
    }
}