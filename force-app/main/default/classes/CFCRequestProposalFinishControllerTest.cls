@isTest
public class CFCRequestProposalFinishControllerTest {
    static testMethod void myUnitTest() 
    {
        User userTest = CFCTestSetup.getUser();
        
       
        system.debug('userTest >>> ' + userTest);
        system.debug('userTest.Contact.Account.Cam__c >>> ' + userTest.Contact.Account.Cam__c);
        
        System.runAs(userTest) 
        { 
            CFCRequestProposalFinishController controller = new CFCRequestProposalFinishController();
             controller.contactCFC  = [Select Id,AccountId From Contact Where id = :userTest.ContactId];
             controller.accountCFC = new Account();
        controller.proposalCFC  = new ProposaL__c();
    
        
            system.assert(controller != null);
        }
    }
    
    static testMethod void myUnitTest1() 
    {
        User userTest = CFCTestSetup.getUserWithoutAccountCam();
        System.runAs(userTest) 
        {
            CFC_Configurations__c cs = new CFC_Configurations__c();
            cs.Mobile_Limits_KB__c = 50;
            cs.Name = 'Test CS';
            cs.Tablet_Limits_KB__c = 50;
            cs.Desktop_Limit_KB__c = 50;
            cs.Billboard_Time__c = 1000;
            cs.User_Name__c = userTest.Username;
            
            cs.Billboard_max_published_number__c = 5;
            cs.URL_Communities__c = 'wwww.teste.com.br';
            cs.Billboard_Time__c = 5;
            cs.Email_to_send_error_job__c = 'teste@teste.com.br';
            
            database.insert(cs);
            system.debug('CFCRequestProposalFinishControllerTest.myUnitTest1.cs >>> ' + cs);        
            
            CFCRequestProposalFinishController controller = new CFCRequestProposalFinishController();
            system.assert(controller != null);
        }        
        
    }
}