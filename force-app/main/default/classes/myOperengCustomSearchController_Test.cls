@isTest
public class myOperengCustomSearchController_Test {
	@isTest static void testIfSearchIsFindingCases(){
        List<Case> case_set;
        String query_argument = 'test';
        RecordType accRT = [SELECT
                            	Id
                            FROM
                            	RecordType
                            WHERE SobjectType='Account'
                            	AND Name = 'Airline'
                           ];
        Account a = new Account(
            Name='Embraer testCompany0',
            Geographical_Area__c = 'North America',
            Company_Nickname__c = 'testCompany0',
            FlightOps_Account_Domains__c = '@teste0',
            Embraer_Fleet_Type__c='EJet',
            RecordTypeId = accRT.Id
        );
        insert a;
        Contact c = new Contact(
                FirstName = 'TestFirstName0',
                LastName = 'Test.LastName0',
                AccountId = a.Id,
                Email = 'TesteFirstName0@embraer.com.br'
        );
        insert c;
        
        
        User user = new User(
                ProfileId = [SELECT Id FROM Profile where Name='Customer Community User'].Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName,
                ContactId = c.Id,
                Username = c.Email,
                EmailEncodingKey = 'ISO-8859-1',
                Alias = 'Alias0',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                CommunityNickname = 'Nickname0' 
        ); 
        insert user;      
        system.debug('user = ' + user);
        
        RecordType recordType = [SELECT Id FROM Recordtype WHERE Name = 'FlightOps Case Record Type'];
        Case cs = New Case(
            Subject = query_argument,
            Customer_Visible__c = TRUE,
            RecordTypeId = recordType.Id,
            AccountId = user.contact.account.Id
        );
        insert cs;  
        
        
        EmailMessage email = New EmailMessage(
            Subject = query_argument,
            parentId = cs.Id
        );
        insert email;

        
		Id [] fixedSearchResults = new Id[]{
            cs.Id,
            email.Id
        };        
        Test.setFixedSearchResults(fixedSearchResults);

        system.runAs(user)
        {
            Test.startTest();
                case_set = myOperengCustomSearchController.searchForIds(query_argument);
            Test.stopTest();
        }
        
        system.debug('case_set = ' + case_set);
        system.debug('user = ' + user);
        
            
		System.assertNotEquals(case_set, NULL, 'Search response is null'); 
    }  

	@isTest static void testIfQuerySplitsTokens(){
        String argument = 'ABCD BCDA CDAB DABC';
        String query = myOperengCustomSearchController.buildQuery(argument);
        //Pattern.matches('(\*.*\* OR )*', '');
        system.debug('query = ' + query);
        System.assertEquals(query, '*ABCD* OR *BCDA* OR *CDAB* OR *DABC*', 'Cant build the query properly');
        
    }
    @isTest static void testGetCurrentUserAccount(){
        Id accountIdTested;
		User user = [SELECT contact.account.Id FROM User WHERE AccountId != NULL AND IsActive = TRUE LIMIT 1];
        
		Id accountIdExpected = user.contact.account.Id; 
        
        
        system.runAs(user)
        {
            Test.startTest();
                accountIdTested = myOperengCustomSearchController.getCurrentUserAccountId();
            Test.stopTest();
        }
        
        system.debug('accountIdExpected = ' + accountIdExpected);  
        system.debug('accountIdTested = ' + accountIdTested);      
        
		System.assertEquals(accountIdExpected, accountIdTested, 'Cant get the right account Id');        
    }
    @isTest static void testGetCasesByEmails(){
        String query_argument = 'test';
        RecordType recordType = [SELECT Id FROM Recordtype WHERE Name = 'FlightOps Case Record Type'];
        Case c = New Case(
            Subject = query_argument,
            Customer_Visible__c = TRUE,
            RecordTypeId = recordType.Id
        );
        insert c;  
        
        List<EmailMessage> email_list = New List<EmailMessage>{
            New EmailMessage(
                Subject = query_argument,
                parentId = c.Id
            )    
        };
        insert email_list;
        
        List<Case> case_list = myOperengCustomSearchController.getCasesByEmails(email_list);
        for(Case c2 : case_list){
            System.assertEquals(c.Id, c2.Id, 'Cant find e-mail related cases');
        }
    }
    @isTest static void testCanGetEmails(){
        String query_argument = 'test';
        Case c = New Case(
            Subject = query_argument
        );
        insert c;
        
        EmailMessage email = New EmailMessage(
            Subject = query_argument,
            parentId = c.Id
        );
        insert email;

		Id [] fixedSearchResults = new Id[]{
            email.Id
        };        
        Test.setFixedSearchResults(fixedSearchResults);
        
        List<List<sObject>> results = myOperengCustomSearchController.searchInCasesAndEmails(query_argument);
        
        System.assert(results[1].size() > 0, 'Cant find any e-mail');
        System.assertEquals(email.Id, results[1][0].Id, 'Cant find e-mail by subject');
    }
    @isTest static void testCanGetCases(){
        String query_argument = 'test';
        RecordType recordType = [SELECT Id FROM Recordtype WHERE Name = 'FlightOps Case Record Type'];
        Case c = New Case(
            Subject = query_argument,
            Customer_Visible__c = TRUE,
            RecordTypeId = recordType.Id
        );
        insert c;     
        
		Id [] fixedSearchResults = new Id[]{
            c.Id
        };        
        Test.setFixedSearchResults(fixedSearchResults);
              
        List<List<sObject>> results = myOperengCustomSearchController.searchInCasesAndEmails(query_argument);
        System.assert(results[0].size() > 0, 'Can not find any case');
        System.assertEquals(c.Id, results[0][0].Id, 'Can not find cases by subject');
    }
    @isTest static void testDuplicatedResultsDoesNotExist(){
        String query_argument = 'test';
        RecordType recordType = [SELECT Id FROM Recordtype WHERE Name = 'FlightOps Case Record Type'];
        Case c = New Case(
            Subject = query_argument,
            Customer_Visible__c = FALSE,
            RecordTypeId = recordType.Id
        );
        insert c;
        
        EmailMessage email = New EmailMessage(
            Subject = query_argument,
            parentId = c.Id
        );
        insert email;
        
		Id [] fixedSearchResults = new Id[]{
            c.Id,
            email.Id
        };        
        Test.setFixedSearchResults(fixedSearchResults);
              
        List<Case> results = myOperengCustomSearchController.searchForIds(query_argument);
        
        system.debug('dul_results = ' + results);
//        System.assert(results.size() == 1, 'Found duplicated cases');
    }
    @isTest static void testInvisibleCasesAreNotSeen(){
        String query_argument = 'test';
        RecordType recordType = [SELECT Id FROM Recordtype WHERE Name = 'FlightOps Case Record Type'];
        Case c = New Case(
            Subject = query_argument,
            Customer_Visible__c = FALSE,
            RecordTypeId = recordType.Id
        );
        insert c;     
        
		Id [] fixedSearchResults = new Id[]{
            c.Id
        };        
        Test.setFixedSearchResults(fixedSearchResults);
              
        List<List<sObject>> results = myOperengCustomSearchController.searchInCasesAndEmails(query_argument);
        System.assert(results[0].size() == 0, 'Found invisible cases');
    }
    @isTest static void testArchivedMessagesAreNotSeen(){
        String query_argument = 'test';
        RecordType recordType = [SELECT Id FROM Recordtype WHERE Name = 'FlightOps Case Record Type'];
        Case c = New Case(
            Subject = query_argument,
            Customer_Visible__c = TRUE,
            Status = 'Archived',
            RecordTypeId = recordType.Id
        );
        insert c;     
        
		Id [] fixedSearchResults = new Id[]{
            c.Id
        };        
        Test.setFixedSearchResults(fixedSearchResults);
              
        List<List<sObject>> results = myOperengCustomSearchController.searchInCasesAndEmails(query_argument);
        system.debug('archived = ' + results);                                                
        System.assert(results[0].size() == 0, 'Found archived cases');
    }
}