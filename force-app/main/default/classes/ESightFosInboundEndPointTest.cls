@isTest(SeeAllData=true)
global class ESightFosInboundEndPointTest {

    @isTest static global void testEsightFozEndPoint() {
        
        try{
            
            Test.startTest();
        
        	List<ESightFosInfo> eSightFosInfoList = new List<ESightFosInfo>();
        
        	Account account = new Account();
        	account.Name = 'Test account name';
        	account.Company_Nickname__c = 'Test c nickename ';
        	account.COD_ORGz__c = 12345;
        
        	insert account;
        
        	ESightFosInfo eSightFosInfo = new ESightFosInfo();    
               
        	eSightFosInfo.aircraftsDelivered = 12345;
        	eSightFosInfo.aircraftsInService = 12345;
        	eSightFosInfo.averageFlightTime  = 10.2;
        	eSightFosInfo.dailyUtilizationCycles = 12345;
        	eSightFosInfo.dailyUtilizationHours = 12345;
        	eSightFosInfo.family = 'test';
        	eSightFosInfo.fleetLeaderCycles = 'test';
        	eSightFosInfo.fleetLeaderHours = 'test';
        	eSightFosInfo.lastUpdate = Date.newInstance(2017, 01, 31);
        	eSightFosInfo.leaderCycles = 12345;
        	eSightFosInfo.leaderHours = 12345;
        	eSightFosInfo.operatorId = 12345;
        	eSightFosInfo.operatorName = 'Test';
        	eSightFosInfo.referenceDate = Date.newInstance(2017, 01, 31);
        	eSightFosInfo.totalFlightCycles = 12345;
        	eSightFosInfo.totalFlightHours = 12345; 
               
        	eSightFosInfoList.add(eSightFosInfo);

            ESightFosInboundEndPoint.execute(eSightFosInfoList);

            List<ESightFos__c> eSightFosObjList = [SELECT id, 
                                         		operatorId__c,
                                         		Account__c
                                         FROM ESightFos__c
                                         WHERE operatorId__c = 12345];

            System.assertEquals(12345, eSightFosObjList.get(0).operatorId__c);
           
            Test.stopTest();
            
        }catch(TypeException e){
            
            System.debug('........ error .......' + e.getMessage());
        }
    }
}