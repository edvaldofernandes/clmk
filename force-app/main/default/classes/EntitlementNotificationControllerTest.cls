/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class EntitlementNotificationController 
* NAME: EntitlementNotificationControllerTest.cls
* AUTHOR: DPF                                                DATE: 13/02/2015
*
*******************************************************************************/
@isTest
private class EntitlementNotificationControllerTest {

    static testMethod void myUnitTest() {
    
      Product2 p2 = SObjectInstanceTest.createProduct2();
      Database.insert(p2);
      
      PricebookEntry pbe = SObjectInstanceTest.createPricebookEntry(SObjectInstanceTest.catalogoDePrecoPadrao(), p2.Id);
      Database.insert(pbe);
    
      ServiceContract sc = SObjectInstanceTest.createServiceContract();
      sc.Pricebook2Id = SObjectInstanceTest.catalogoDePrecoPadrao();
      Database.insert(sc);
      
      Aircraft__c a = SObjectInstanceTest.createAircraft();
      Database.insert(a);
      
      Related_Aircraft__c ra = SObjectInstanceTest.createRelatedAirCraft(a.Id);
      ra.Service_Contract__c = sc.Id;
      Database.insert(ra);
      
      ContractLineItem cli = SObjectInstanceTest.createContractLineItem(sc.Id, pbe.Id);
      Database.insert(cli);
      
      Id recTypeAcc = RecordTypeMemory.getRecType('Account', 'Operator');
      
      Account acc = SObjectInstanceTest.createAccount(recTypeAcc);
      Database.insert(acc);
      
      Id recTypeScm = RecordTypeMemory.getRecType('Service_Contract_Management__c', 'Deliverable');
      
      Service_Contract_Management__c scm = SObjectInstanceTest.createSvcContractMgmt(acc.Id, sc.Id, null, recTypeScm);
      scm.Service_Contract_line_item__c = cli.Id;
      Database.insert(scm);
      
      Test.startTest();
      EntitlementNotificationController controller = new EntitlementNotificationController(new ApexPages.Standardcontroller(sc));
      Test.stopTest();
    }
}