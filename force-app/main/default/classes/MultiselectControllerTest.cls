@isTest
private class MultiselectControllerTest {
  private testMethod static void testMultiselectController() {
    Communications__c rec = createREC();
    Database.insert(rec);

    Test.startTest();
    MultiselectController c = new MultiselectController();

    c.leftOptions = new List<SelectOption>();
    c.rightOptions = new List<SelectOption>();
    c.parentObjId = rec.Id;

    c.leftOptionsHidden = 'A&a&b&b';
    c.rightOptionsHidden = 'C&c';

    System.assertEquals(
      c.leftOptions.size(),
      2,
      '[ERROR] Left options doesnt match!'
    );
    System.assertEquals(
      c.rightOptions.size(),
      1,
      '[ERROR] Right options doesnt match!'
    );

    c.setMessage();
    Communications__c parentObj = [
      SELECT Id, Email_Attachments__c
      FROM Communications__c
      WHERE Id = :rec.Id
    ];

    System.assertEquals(
      parentObj.Email_Attachments__c,
      c.rightOptions[0].getValue() +
      ';' +
      c.rightOptions[0].getLabel(),
      '[ERROR] Email attachments doesnt match'
    );

    Test.stopTest();
  }

  private static Communications__c createREC() {
    Id idRECRecordType = Schema.SObjectType.Communications__c.getRecordTypeInfosByName()
      .get('REC')
      .getRecordTypeId();
    Communications__c rec = new Communications__c();
    rec.RecordTypeId = idRECRecordType;
    rec.communication_status__c = 'draft';
    rec.reference_date__c = system.today();
    rec.ata_chapter__c = '00 GENEREAL';
    rec.technology__c = 'AMS';
    rec.fleet_type__c = '195-E2';
    rec.Name = 'rectest';
    return rec;
  }
}