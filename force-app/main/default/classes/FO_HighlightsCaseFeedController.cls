public class FO_HighlightsCaseFeedController {

    public String currentRecordId {get;set;}
	public Case caso{get;set;}
    public Case casoAux{get;set;}
 
    public FO_HighlightsCaseFeedController() {
        
        if (ApexPages.CurrentPage().getparameters().get('id')!=null){
            
        currentRecordId = ApexPages.CurrentPage().getparameters().get('id');        
        caso = [SELECT id,RecordTypeId,CaseNumber,Account.Name,FlightOps_LastComment__c,AccountId,ContactId,Contact.Name,Subject,Status,FlightOps_Initial_Feedback__c,Next_Follow_up__c FROM Case WHERE id =: currentRecordId ];       
     	//caseAux criado apenas para auxilio e aparecer na tela
        casoAux = new Case(RecordTypeId=caso.RecordTypeId,AccountId = caso.AccountId,FlightOps_LastComment__c = caso.FlightOps_LastComment__c,ContactId=caso.ContactId,Subject=caso.Subject,Status=caso.Status,Next_Follow_up__c=caso.Next_Follow_up__c);
       
        }
    }
    
    public PageReference runCaseUpdate(){

        if ((casoAux!=null)&&(caso!=null)) {
            
            CaseComment[] cc = [SELECT id FROM CaseComment WHERE CommentBody =: casoAux.FlightOps_LastComment__c AND ParentId =: caso.Id];
            
            //cria novo comentario, se nao existe
            if (cc.isEmpty()) {
            	CaseComment cCToCreate = new CaseComment(CommentBody=casoAux.FlightOps_LastComment__c,ParentId=caso.Id);
            	insert(cCToCreate);
            }
            
            caso.FlightOps_LastComment__c = casoAux.FlightOps_LastComment__c;
            caso.Subject = casoAux.Subject;
            caso.Next_Follow_up__c = casoAux.Next_Follow_up__c;
            caso.Status = casoAux.Status;
        	update(caso);  
        }
                
        PageReference tempPage = ApexPages.currentPage();           
        tempPage.setRedirect(true);
        return tempPage ;
       
    }

}