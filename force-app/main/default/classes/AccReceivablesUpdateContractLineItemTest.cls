/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Class for testing and covering the code of the class AccReceivablesUpdateContractLineItem 
*
* NAME: AccReceivablesUpdateContractLineItemTest.cls
* AUTHOR: RLdO                                                DATE: 20/12/2014
*******************************************************************************/
@isTest
private class AccReceivablesUpdateContractLineItemTest
{
  private static final id RecTypeAcc = RecordTypeMemory.getRecType( 'Account', 'Operator' );
  private static final id RecTypeSC = RecordTypeMemory.getRecType( 'ServiceContract', 'Aircraft_Modification' );
  private static final id RecTypeEnt = RecordTypeMemory.getRecType( 'Service_Contract_Management__c', 'NREC' );
  
    private static Product2 prod;
    private static PricebookEntry pbe;
    private static Account lAcc;
    private static ServiceContract lServC;
    private static ContractLineItem oli;
    private static Service_Contract_Management__c lAccRec;
    private static Id stdPB;
  
  static {
    stdPB = SObjectInstanceTest.catalogoDePrecoPadrao();
    
    prod = SObjectInstanceTest.createProduct2();
    Database.insert(prod);

    pbe = SObjectInstanceTest.createPricebookEntry(stdPB, prod.Id);
    Database.insert(pbe);

    lAcc = SObjectInstanceTest.createAccount( RecTypeAcc );
    Database.insert(lAcc);
      
    lServC = SObjectInstanceTest.createServiceContract( lAcc.id, RecTypeSC );
    lServC.Pricebook2Id = stdPB;
    Database.insert(lServC);
      
 //   lEnt = SObjectInstanceTest.createEntitlement( lAcc.id );
    /* Commented by Fabio (Embraer) during the deploy in QA (23/Dec/2014) field Credits__c does not exist 
    lEnt.Credits__c = 10;*/
    
    
 //   lEnt.ServiceContractId = lServC.id;
 //   Database.insert(lEnt);
      
    oli = SObjectInstanceTest.createContractLineItem(lServC.Id, pbe.Id);
    Database.insert(oli);

    lAccRec = SObjectInstanceTest.createSvcContractMgmt( lAcc.id, lServC.id, null, RecTypeEnt );
    lAccRec.Service_Contract_line_item__c = oli.Id;
    
    Database.insert(lAccRec);
    
  }
  
  
  static testMethod void TestUpdate() {
    
    oli.Total_of_REC_and_NREC__c = 2;
    Database.update(oli);
    
    lAccRec.Percentage_of_proposal_NREC__c = 10;
    lAccRec.Percentage_of_proposal_REC__c = 2;
    
    Database.update(lAccRec);
    
    List<ContractLineItem> lstResultContract = [Select Id, Total_of_REC_and_NREC__c from ContractLineItem where Id =: oli.Id]; 
    List<Service_Contract_Management__c> lstResultService = [Select Id, Percentage_of_proposal_NREC__c, Percentage_of_proposal_REC__c from Service_Contract_Management__c where Id =: lAccRec.Id];
    
  }
  
  static testMethod void TestDelete() {
    
    Database.delete(lAccRec);
    
    List<Service_Contract_Management__c> lstResultService = [Select Id from Service_Contract_Management__c where Id =: lAccRec.Id];
    
  }
}