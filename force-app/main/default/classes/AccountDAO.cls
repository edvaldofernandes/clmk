/* Classe implementadora de SOBjectDAO para operações DML no objeto Account, utilizando pattern de Singleton.
* @author - Guilherme Nascimento
*			gjesus@deloitte.com
* @version 1.0 06/01/2016
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           29 MAY 2018             Add Account by FlyEmbraerId
*                                                           Methods: getListAccountByFlyEmbraerId
*                                                                   
**/
public without sharing class AccountDAO {
    private static final AccountDAO instance = new AccountDAO();    
    
    private AccountDAO(){
    }    
    
    public static AccountDAO getInstance() {
        return instance;
    }
    
    public List<Account> listByRecordTypeIdAndMaintanceCapability(String idRecordType, Set<String> setMaintanceCapability){        
        string setToString = '';
        for(String includeValue : setMaintanceCapability){
            setToString += '\''+includeValue + '\',';
        }
        setToString = setToString.removeEnd(',');
        
        List<Account> listAccount = database.query(' Select ' + utils.getAllFields('Account') + ' FROM Account' +
                                                   ' WHERE RecordTypeId =\''+String.escapeSingleQuotes(idRecordType)+'\'' + 
                                                   ' AND Maintance_Capability__c INCLUDES('+setToString+')' +
                                                   ' AND (EOSC__c = true OR EASC__c = true) AND Company_Status__c = \'Active\'');
        return listAccount;
    }    
    
    /**
	* @description : List of Account by FlyEmbraerId
    * @param String idFlyEmbraer : idFlyEmbraer filter
	* @return List<Account> Account list
	*/
    public static Account getListAccountByFlyEmbraerId(String idFlyEmbraer){
        System.debug('idFly '+idFlyEmbraer);
        Account acont = new Account();
        acont = [SELECT id, name,FlyEmbraerId__c,phone  FROM Account WHERE FlyEmbraerId__c=:idFlyEmbraer limit 1];
        
        //System.debug('lista '+listac.si;
        if(acont == null){return null;}
        return acont;
    }
    
}