/*
 * MultiselectController synchronizes the values of the hidden elements to the
 * SelectOption lists.
 */
public without sharing class MultiselectController {
  // SelectOption lists for public consumption
  public SelectOption[] leftOptions { get; set; }
  public SelectOption[] rightOptions { get; set; }
  public Id parentObjId { get; set; } //Parent SObject

  // Parse &-separated values and labels from value and
  // put them in option
  private void setOptions(SelectOption[] options, String value) {
    options.clear();
    String[] parts = value.split('&');
    for (Integer i = 0; i < parts.size() / 2; i++) {
      options.add(
        new SelectOption(
          EncodingUtil.urlDecode(parts[i * 2], 'UTF-8'),
          EncodingUtil.urlDecode(parts[(i * 2) + 1], 'UTF-8')
        )
      );
    }
  }

  // Backing for hidden text field containing the options from the
  // left list
  public String leftOptionsHidden {
    get;
    set {
      leftOptionsHidden = value;
      setOptions(leftOptions, value);
    }
  }

  // Backing for hidden text field containing the options from the
  // right list
  public String rightOptionsHidden {
    get;
    set {
      rightOptionsHidden = value;
      setOptions(rightOptions, value);
    }
  }

  public static String setAttachments(List<SelectOption> options) {
    String message = '';
    Boolean first = true;
    for (SelectOption so : options) {
      if (!first) {
        message += '\n';
      }
      message += so.getValue() + ';' + so.getLabel();
      first = false;
    }
    return message;
  }

  public void setMessage() {
    String message = setAttachments(this.rightOptions);

    // An implementation to persist data in Email_Attachments__c (text area) of Communications__c Object
    if (
      parentObjId.getSObjectType().getDescribe().getName() ==
      'Communications__c'
    ) {
      Communications__c parentObj = [
        SELECT Id, Email_Attachments__c
        FROM Communications__c
        WHERE Id = :parentObjId
      ];
      parentObj.Email_Attachments__c = message;
      Database.update(parentObj);
    }
  }
}