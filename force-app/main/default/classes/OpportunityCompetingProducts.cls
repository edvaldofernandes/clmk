/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* 
* Class responsible for searching and inserting competing products for an 
* opportunity line item when the opportunity line item is created.
*
* NAME: OpportunityCompetingProducts.cls
* AUTHOR:JFS                                                DATE: 04/12/2014
*******************************************************************************/
public without sharing class OpportunityCompetingProducts {

  private static final id RectypeNoEmbraer = RecordTypeMemory.getRecType('Product2', 'Non_Embraer');
  
  public static void execute()
  {
    TriggerUtils.assertTrigger();
  
    set < id >  setpbe = new set < id >();
    set < id >  setOpp = new set < id >();
    
    for(OpportunityLineItem Oli: ( list <  OpportunityLineItem > ) trigger.new)
    {
      
      setpbe.add(Oli.PricebookEntryId);
      setOpp.add(Oli.OpportunityId);
    }
    
    map< id, id > mapPBExProd = new map < id, id >();
    
    for(PricebookEntry pbe : [SELECT id, Product2Id FROM PricebookEntry WHERE id =: setpbe and isactive = true ])
    { 
  
      mapPBExProd.put( pbe.id, pbe.Product2Id ); 
      
    }
    
    if(mapPBExProd.isEmpty()) return;
   
    map < id, list < Product2 > > mapPrdCompet = new map < id, list < Product2 > >();
    set < id > Produtocomp = new set < id >();
   
    for(Product2 lprd:  [SELECT Embraer_Product__c, id FROM Product2 WHERE Embraer_Product__c =: mapPBExProd.values() and RecordTypeId =: RectypeNoEmbraer and isActive = true])
    {
      
      list < Product2 > lstprodutoComp = mapPrdCompet.get(lprd.Embraer_Product__c);
      
      if( lstprodutoComp == null )
      {
        lstprodutoComp = new list < Product2 >();
        mapPrdCompet.put(lprd.Embraer_Product__c, lstprodutoComp);
      }
    
      lstprodutoComp.add(lprd); 
      Produtocomp.add(lprd.id);     
    }
    
    if(mapPrdCompet.IsEmpty()) return;
    
    set< String > lSetCompProd = new set< String >();
   
    for ( Competing_Product__c lComProd : [SELECT Opportunity__c, Embraer_Product__c, Competing_Product__c
       FROM Competing_Product__c WHERE Embraer_Product__c =: mapPBExProd.values() and Opportunity__c =:setOpp and Competing_Product__c =:Produtocomp] )
    {
      lSetCompProd.add( lComProd.Opportunity__c + '|' + lComProd.Embraer_Product__c + '|' + lComProd.Competing_Product__c );
    }
    
    set< Competing_Product__c > setCompeting = new set< Competing_Product__c >();
     
    for(OpportunityLineItem Oli: ( list <  OpportunityLineItem > ) trigger.new)
    {
      
      id lProdId = mapPBExProd.get( oli.PricebookEntryId );
      
      if(!mapPrdCompet.containsKey( lProdId )) continue;
      for ( Product2 lprodComp : mapPrdCompet.get( lProdId ) )
      {
      
        if ( lSetCompProd.contains( oli.OpportunityId + '|' + lProdId + '|' + lprodComp.id ) ) continue; 
       
         Competing_Product__c Prdcompt = new Competing_Product__c();
         Prdcompt.Competing_Product__c = lprodComp.id;
         Prdcompt.Embraer_Product__c = lProdId;
         Prdcompt.Opportunity__c = Oli.OpportunityId;
         setCompeting.add(Prdcompt);
      }
    }
    
    list< Competing_Product__c > lstCompeting = new list< Competing_Product__c >();
    lstCompeting.addAll(setCompeting);
    
    if(!lstCompeting.Isempty()) insert lstCompeting;   

  }

}