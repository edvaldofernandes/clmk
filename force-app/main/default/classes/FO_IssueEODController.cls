/**
* @author Marcos Vinicius de Oliveira Duque
* @date 28/11/2018
* @description: This class is the controller for issuing an EOD.
**/
public class FO_IssueEODController {
    private Id eodId;

    /**
    * @description The class constructor. It gets the current record Id.
    * 
    * @param ApexPages.StandardController stdController : The standard controller.
    **/
    public FO_IssueEODController(ApexPages.StandardController stdController) {
        eodId = stdController.getId();
    }
    
    /**
    * @description Publish and EOD and returns to its record page.
    * 
    * @return PageReference : The record page reference.
    **/
    public PageReference redirect(){
        FO_EODService.publish(this.eodId);
		PageReference pageRef = new PageReference('/' + eodId);
        pageRef.setRedirect(true);    
        return pageRef;
    }
}