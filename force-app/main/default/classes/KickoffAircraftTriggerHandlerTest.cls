@isTest
public class KickoffAircraftTriggerHandlerTest {
	@isTest 
    static void preventDeleteTest(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp 1';
        opp.Fleet_Type__c = 'Turboprop';
        opp.StageName = 'Validation_of_approvals';
        opp.CloseDate = system.today();
        insert opp;
        Kickoff__c Ko = new Kickoff__c();
        Ko.Name = 'Checklist 1';
        Ko.Opportunity__c = opp.Id;
        Ko.Customer_Type__c = 'New Customer';
        Ko.submitted__c = true;
        insert Ko;
        Kickoff_Aircraft__c KoA = new Kickoff_Aircraft__c();
        KoA.Kickoff__c = Ko.Id;
        KoA.Info_Type__c = 'Order Size';
        insert KoA;
        System.assert([SELECT Count() FROM Kickoff__c]==1); 
        System.assert([SELECT Count() FROM Kickoff_Aircraft__c]==1); 
        try{
            if(Test.isRunningTest()) delete KoA;
        } catch (DMLException ex) {
    		// We don't wanna catch them all       
		}
        System.assert([SELECT Count() FROM Kickoff_Aircraft__c]==1);
        Ko.submitted__c = false;
        update Ko;
        try{
            if(Test.isRunningTest()) delete KoA;
        } catch (DMLException ex) {
    		// We don't wanna catch them all       
		}
        System.assert([SELECT Count() FROM Kickoff_Aircraft__c]==0);
    }
}