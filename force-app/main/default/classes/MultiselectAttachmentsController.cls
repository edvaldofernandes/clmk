public without sharing class MultiselectAttachmentsController {
  public SelectOption[] selectedAttachs { get; set; }
  public SelectOption[] allRelatedAttachs { get; set; }
  public Id recId { get; set; }
  public String message { get; set; }

  public MultiselectAttachmentsController(
    ApexPages.StandardController stdController
  ) {
    this.recId = stdController.getId();

    Set<Id> documentIds = new Set<Id>();
    for (ContentDocumentLink cdl : [
      SELECT ContentDocumentId
      FROM ContentDocumentLink
      WHERE LinkedEntityId = :this.recId
    ]) {
      documentIds.add(cdl.ContentDocumentId);
    }

    selectedAttachs = getPersistedSelectedAttachs(this.recId);
    for (SelectOption op : selectedAttachs) {
      documentIds.remove(op.getValue());
    }

    allRelatedAttachs = new List<SelectOption>();
    for (ContentVersion c : [
      SELECT ContentDocumentId, Title
      FROM ContentVersion
      WHERE ContentDocumentId IN :documentIds
    ]) {
      allRelatedAttachs.add(new SelectOption(c.ContentDocumentId, c.Title));
    }
  }

  public static List<SelectOption> getPersistedSelectedAttachs(Id recId) {
    List<SelectOption> selectedAttachs = new List<SelectOption>();
    String selectedAttachsTextArea = [
      SELECT Email_attachments__c
      FROM Communications__c
      WHERE Id = :recId
    ]
    .Email_attachments__c;

    String currentAttachments = '';
    Boolean isFirst = true;
    if (!String.isBlank(selectedAttachsTextArea)) {
      for (String selectedAttachRecord : selectedAttachsTextArea.split('\n')) {
        List<String> keyAndValue = selectedAttachRecord.split(';');
        if (
          ![SELECT id FROM ContentDocument WHERE id = :keyAndValue[0] LIMIT 1]
            .isEmpty()
        ) {
          selectedAttachs.add(new SelectOption(keyAndValue[0], keyAndValue[1]));
        }
      }
    }
    return selectedAttachs;
  }
}