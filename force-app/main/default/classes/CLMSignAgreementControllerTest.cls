@isTest
public class CLMSignAgreementControllerTest {
	
    @testSetup static void generateTestData(){
        Account testAcc = SObjectInstanceTest.createAccount(RecordTypeMemory.getRecType('Account', 'Government'));
        database.insert(testAcc);
        
        ContentDocument doc = createdocument(' 1');
        ContentDocument doc2 = createdocument(' 2');
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp 1';
        opp.Fleet_Type__c = 'Turboprop';
        opp.StageName = 'Validation_of_approvals';
        opp.CloseDate = system.today();
        insert opp;
        Kickoff__c ko = new Kickoff__c();
        ko.Name = 'Checklist 1';
        ko.Opportunity__c = opp.Id;
        ko.Customer_Type__c = 'New Customer';
        ko.submitted__c = true;
        ko.Kick_Off_Meeting_Date__c = system.today();
        insert ko;       
        
        Agreement__c notApprovedProposal = new Agreement__c();
        notApprovedProposal.Name = 'Not Approved Proposal';
        notApprovedProposal.Kickoff__c = ko.Id;
        notApprovedProposal.Account__c = testAcc.Id;
        notApprovedProposal.Buyer__c = testAcc.Id;
        notApprovedProposal.Proposal_Validity__c = Date.today();
        notApprovedProposal.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        notApprovedProposal.Agreement_Color__c = '0000FF';
        notApprovedProposal.DF_Color__c = 'Blue';
        notApprovedProposal.Admin_Mode__c = 'true';
        notApprovedProposal.DOCCON_Number__c = '123';
        notApprovedProposal.Status_Category__c = 'Proposal Request';
        //validProposal.Bypass_Approval__c = true;
        //validProposal.Bypass_Comments__c = ' ok ';
        notApprovedProposal.Country__c = 'Brazil';
        notApprovedProposal.Signature_Due_Date__c = Date.today();
        notApprovedProposal.Signature_Date__c = Date.today();
        database.insert(notApprovedProposal);
        Agreement__c notApprovedPro = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Not Approved Proposal'];
        ContentDocumentLink document10 = createContentDocumentLink(notApprovedPro, doc);
        ContentDocumentLink document11 = createContentDocumentLink(notApprovedPro, doc2);
        database.insert(new List<ContentDocumentLink>{document10,document11});
        
        Agreement__c validProposal = new Agreement__c();
        validProposal.Name = 'Valid Proposal';
        validProposal.Kickoff__c = ko.Id;
        validProposal.Account__c = testAcc.Id;
        validProposal.Buyer__c = testAcc.Id;
        validProposal.Proposal_Validity__c = Date.today();
        validProposal.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        validProposal.Agreement_Color__c = '0000FF';
        validProposal.DF_Color__c = 'Blue';
        validProposal.Admin_Mode__c = 'true';
        validProposal.DOCCON_Number__c = '123';
        validProposal.Status_Category__c = 'Proposal Approved';
        //validProposal.Bypass_Approval__c = true;
        //validProposal.Bypass_Comments__c = ' ok ';
        validProposal.Country__c = 'Brazil';
        validProposal.Signature_Due_Date__c = Date.today();
        validProposal.Signature_Date__c = Date.today();
        database.insert(validProposal);
        Agreement__c vaPro = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid Proposal'];
        ContentDocumentLink document5 = createContentDocumentLink(vaPro, doc);
        ContentDocumentLink document6 = createContentDocumentLink(vaPro, doc2);
        vaPro.Status_Category__c = 'Proposal Approved';
        database.update(new List<Agreement__c>{vaPro});
        database.insert(new List<ContentDocumentLink>{document5,document6});
        
        Agreement__c notValidProposal = new Agreement__c();
        notValidProposal.Name = 'Not Valid Proposal';
        notValidProposal.Account__c = testAcc.Id;
        notValidProposal.Kickoff__c = ko.Id;
        notValidProposal.Buyer__c = testAcc.Id;
        notValidProposal.Proposal_Validity__c = Date.today();
        notValidProposal.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Proposal');
        notValidProposal.Agreement_Color__c = '0000FF';
        notValidProposal.DF_Color__c = 'Blue';
        notValidProposal.Admin_Mode__c = 'true';
        notValidProposal.DOCCON_Number__c = '123';
        notValidProposal.Status_Category__c = 'Proposal Approved';
        //notValidProposal.Bypass_Approval__c = true;
        //notValidProposal.Bypass_Comments__c = ' ok ';
        notValidProposal.Country__c = 'Brazil';
        database.insert(notValidProposal);
        ContentDocumentLink document8 = createContentDocumentLink(notValidProposal, doc);
        ContentDocumentLink document9 = createContentDocumentLink(notValidProposal, doc2);
        Agreement__c notVaPro = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Not Valid Proposal'];
        notVaPro.Status_Category__c = 'Proposal Approved';
        database.update(new List<Agreement__c>{notVaPro});
        database.insert(new List<SObject>{document8,document9});
        
        Agreement__c validPA = new Agreement__c();
        validPA.Name = 'Valid PA';
        validPA.Account__c = testAcc.Id;
        validPA.Buyer__c = testAcc.Id;
        validPA.Proposal_Validity__c = Date.today();
        validPA.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        validPA.Agreement_Color__c = '0000FF';
        validPA.DF_Color__c = 'Blue';
        validPA.Admin_Mode__c = 'true';
        validPA.DOCCON_Number__c = '123';
        validPA.Status_Category__c = 'PA Approved';
        //validPA.Bypass_Approval__c = true;
        //validPA.Bypass_Comments__c = ' ok ';
        validPA.Country__c = 'Brazil';
        validPA.Signature_Due_Date__c = Date.today();
        validPA.Signature_Date__c = Date.today();
        validPA.Nickname__c = 'Valid PA';
        validPA.Change_History__c = 'Valid PA signature';
        database.insert(validPA);
        Agreement__c vaPA = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid PA'];
        ContentDocumentLink document1 = createContentDocumentLink(vaPA, doc);
        ContentDocumentLink document2 = createContentDocumentLink(vaPA, doc2);
        vaPA.Status_Category__c = 'PA Approved';
        database.update(new List<Agreement__c>{vaPA});
        SOB_Event__c sob = new SOB_Event__c();
        sob.Commercial_Agreement__c = vaPA.Id;
        database.insert(new List<SObject>{document1,document2,sob});
        //ContentDocumentLink
        //
        Agreement__c notValidPA = new Agreement__c();
        notValidPA.Name = 'Not Valid PA';
        notValidPA.Account__c = testAcc.Id;
        notValidPA.Buyer__c = testAcc.Id;
        notValidPA.Proposal_Validity__c = Date.today();
        notValidPA.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        notValidPA.Agreement_Color__c = '0000FF';
        notValidPA.DF_Color__c = 'Blue';
        notValidPA.Admin_Mode__c = 'true';
        notValidPA.DOCCON_Number__c = '123';
        notValidPA.Status_Category__c = 'PA Approved';
        notValidPA.Is_Amendment__c = true;
        //notValidPA.Bypass_Comments__c = ' ok ';
        notValidPA.Country__c = 'Brazil';
        database.insert(notValidPA);
        Agreement__c notVaPA = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Not Valid PA'];
        ContentDocumentLink document3 = createContentDocumentLink(notVaPA, doc);
        ContentDocumentLink document4 = createContentDocumentLink(notVaPA, doc2);
        notVaPA.Status_Category__c = 'PA Approved';  
        database.update(new List<Agreement__c>{notVaPA});
        database.insert(new List<ContentDocumentLink>{document3,document4});
        
        Agreement__c notValidPAwithoutfilesParent = new Agreement__c();
        notValidPAwithoutfilesParent.Name = 'notValidPAwithoutfilesParent';
        notValidPAwithoutfilesParent.Account__c = testAcc.Id;
        notValidPAwithoutfilesParent.Buyer__c = testAcc.Id;
        notValidPAwithoutfilesParent.Proposal_Validity__c = Date.today();
        notValidPAwithoutfilesParent.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        notValidPAwithoutfilesParent.Agreement_Color__c = '0000FF';
        notValidPAwithoutfilesParent.DF_Color__c = 'Blue';
        notValidPAwithoutfilesParent.Admin_Mode__c = 'true';
        notValidPAwithoutfilesParent.DOCCON_Number__c = '123';
        notValidPAwithoutfilesParent.Status_Category__c = 'PA Approved';
        //validPA.Bypass_Approval__c = true;
        //validPA.Bypass_Comments__c = ' ok ';
        notValidPAwithoutfilesParent.Country__c = 'Brazil';
        notValidPAwithoutfilesParent.Signature_Due_Date__c = Date.today();
        notValidPAwithoutfilesParent.Signature_Date__c = Date.today();
        notValidPAwithoutfilesParent.Nickname__c = 'Valid PA';
        notValidPAwithoutfilesParent.Change_History__c = 'Valid PA signature';
        database.insert(notValidPAwithoutfilesParent);
        
        Agreement__c notValidPAwithoutfiles = new Agreement__c();
        notValidPAwithoutfiles.Name = 'notValidPAwithoutfiles';
        notValidPAwithoutfiles.Account__c = testAcc.Id;
        notValidPAwithoutfiles.Buyer__c = testAcc.Id;
        notValidPAwithoutfiles.Proposal_Validity__c = Date.today();
        notValidPAwithoutfiles.RecordTypeId = RecordTypeMemory.getRecType('Agreement__c', 'Purchase_Agreement');
        notValidPAwithoutfiles.Agreement_Color__c = '0000FF';
        notValidPAwithoutfiles.DF_Color__c = 'Blue';
        notValidPAwithoutfiles.Admin_Mode__c = 'true';
        notValidPAwithoutfiles.DOCCON_Number__c = '123';
        notValidPAwithoutfiles.Status_Category__c = 'PA Approved';
        //validPA.Bypass_Approval__c = true;
        //validPA.Bypass_Comments__c = ' ok ';
        notValidPAwithoutfiles.Country__c = 'Brazil';
        notValidPAwithoutfiles.Signature_Due_Date__c = Date.today();
        notValidPAwithoutfiles.Signature_Date__c = Date.today();
        notValidPAwithoutfiles.Nickname__c = 'Valid PA';
        notValidPAwithoutfiles.Change_History__c = 'Valid PA signature';
        notValidPAwithoutfiles.Parent_Agreement__c = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'notValidPAwithoutfilesParent'][0].Id;
        database.insert(notValidPAwithoutfiles);
        Agreement__c vaPAWFf = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'notValidPAwithoutfiles'];
        ContentDocumentLink document1wf = createContentDocumentLink(vaPAWFf, doc);
        ContentDocumentLink document2wf = createContentDocumentLink(vaPAWFf, doc2);
        vaPAWFf.Status_Category__c = 'PA Approved';
        database.update(new List<Agreement__c>{vaPAWFf});
        SOB_Event__c sobwf = new SOB_Event__c();
        sob.Commercial_Agreement__c = vaPAWFf.Id;
        database.insert(new List<SObject>{document1wf,document2wf,sobwf});
    }
    
    static testMethod void signNotApprovedProposalTest(){
        SYSTEM.debug('signNotValidProposalTest');
        Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Not Approved Proposal' and Status_Category__c = 'Proposal Request' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Not Approved Proposal' and Status_Category__c = 'Proposal Signed'];
        System.assertEquals(0,testAgreements.size());
    
    }
    
    static testMethod void signValidProposalTest(){
        SYSTEM.debug('signValidProposalTest');
    	Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Valid Proposal' and Status_Category__c = 'Proposal Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid Proposal' and Status_Category__c = 'Proposal Signed'];
        System.assertEquals(1,testAgreements.size());
    }
    
    static testMethod void signNotValidProposalTest(){
        SYSTEM.debug('signNotValidProposalTest');
        Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Not Valid Proposal' and Status_Category__c = 'Proposal Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Not Valid Proposal' and Status_Category__c = 'Proposal Signed'];
        System.assertEquals(0,testAgreements.size());
    
    }
    
    static testMethod void signValidPATest(){
        SYSTEM.debug('signValidPATest');
    	Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Valid PA' and Status_Category__c = 'PA Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
        pc.signCompleted();
        pc.goToAgreement();
        
        system.debug([SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid PA']);
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid PA' and Status_Category__c = 'PA Signed'];
        System.assertEquals(1,testAgreements.size());
    }
    
    static testMethod void signValidPATestWithParent(){
        SYSTEM.debug('signValidPATest');
    	Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'notValidPAwithoutfiles' and Status_Category__c = 'PA Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
        pc.signCompleted();
        pc.goToAgreement();
        
        system.debug([SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'notValidPAwithoutfiles']);
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'notValidPAwithoutfiles' and Status_Category__c = 'PA Signed'];
        System.assertEquals(1,testAgreements.size());
    }
    
    static testMethod void signedPATest(){
        SYSTEM.debug('signValidPATest');
    	Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Valid PA' and Status_Category__c = 'PA Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        pc.sign();
        test.stopTest();
        
        system.debug([SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid PA']);
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid PA' and Status_Category__c = 'PA Signed'];
        System.assertEquals(1,testAgreements.size());
        List<Agreement__c> testAgreement2 = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Valid PA' and Status_Category__c = 'PA Approved'];
        System.assertEquals(0,testAgreement2.size());
    }
    
    static testMethod void signNotValidPATest(){
        SYSTEM.debug('signNotValidPATest');
        Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Not Valid PA' and Status_Category__c = 'PA Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',testAgreement.Id);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
        
        List<Agreement__c> testAgreements = [SELECT Id, Status_Category__c FROM Agreement__c WHERE Name = 'Not Valid PA' and Status_Category__c = 'PA Signed'];
        System.assertEquals(0,testAgreements.size());
    }
    
    static testMethod void invalidId(){
        SYSTEM.debug('invalidId');
        Agreement__c testAgreement = [SELECT Id, recordtype.name, Status_Category__c, Status__c, Country__c, Signature_Due_Date__c, Signature_Date__c, Proposal_Validity__c,Nickname__c, Change_History__c 
                                      FROM Agreement__c 
                                      WHERE Name = 'Valid PA' and Status_Category__c = 'PA Approved' LIMIT 1];
        
        Test.setCurrentPage(Page.NewAgreement);
        ApexPages.currentPage().getParameters().put('id',null);
        
        test.startTest();
        ApexPages.StandardController vp = new ApexPages.StandardController(testAgreement);
        CLMSignAgreementController pc = new CLMSignAgreementController(vp);
        pc.sign();
        test.stopTest();
    }
    
    static ContentDocument createdocument(String var){
        String name = 'Test ' + var;
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = name,
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        system.debug([SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE Title = :name]);
        List<ContentDocument>documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE Title = :name];
        return documents[0];
    }
    
    static ContentDocumentLink createContentDocumentLink(Agreement__c agr, ContentDocument doc){
        ContentDocumentLink docLink = new ContentDocumentLink();
        docLink.LinkedEntityId = agr.Id;
        docLink.ContentDocumentId = doc.Id;
        docLink.ShareType = 'V';
        return docLink;
    }
    
    
}