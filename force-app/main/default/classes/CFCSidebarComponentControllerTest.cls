@isTest
public class CFCSidebarComponentControllerTest {

   	 static testMethod void myUnitTest1() {
    	CFCSidebarComponentController cfc = new CFCSidebarComponentController();
        cfc.getOperationItems();
        cfc.getProductFamilyItems();
        cfc.getApplicabilityCompanyItems();
        cfc.getProductTypeItems();
        cfc.getAtaChapterItems();        
        cfc.setOperationContext('Test sidebar');
        cfc.getOperationContext();
        String[] ab = new String[1];
        cfc.setProductFamily(ab);
        cfc.getProductFamily();
        cfc.setApplicabilityCompany(ab);
        cfc.getApplicabilityCompany();
        cfc.setProductType(ab);
        cfc.getProductType();
        cfc.setAtaChapter(ab);
        cfc.getAtaChapter();
        cfc.fleetType = 'testing';
        cfc.queryString = 'cfc';
        cfc.setOperationContext('Test filter');        
        System.currentPageReference().getParameters().put('q', 'Test');
        System.currentPageReference().getParameters().put('productCategory', 'Test Embraer');        
        cfc.filter();
         system.assert(cfc != null); 
    }    
}