public class CRM360_CustomerDashboardController {
    
    
    public CRM360_CustomerDashboardController(){
        this.amountCases = [SELECT count() FROM Case WHERE CRM360_view__c = true 
                        AND AccountId = :getAccountId()
                        AND Status = 'OPEN'];
        this.delayedCases = [SELECT count() FROM Case WHERE CRM360_view__c = true 
                        AND AccountId = :getAccountId()
                        AND Status = 'OPEN' AND IsEscalated = true];
        getCSWs();
    }

    //Loading data from account based on parameter ID
    private static Id accId = (Id) ApexPages.currentPage().getParameters().get('id');
    private static Account thisAccount = [SELECT Name, Account_Type__c, Company_Nickname__c, Geographical_Area__c,Non_Embraer_Other_fleet__c,Quantity_Non_Embraer_Aircraft_Operator__c,Quantity_Non_Embraer_Aircraft_Owner__c,Non_Embraer_Aircraft_Lessor__c,
                                          Site, Id, Quantity_Embraer_Aircraft_Operator__c, Embraer_Fleet_Type__c, 
                                          Embraer_a_c_in_service_only__c, Quantity_Embraer_Aircraft_Owner__c, Embraer_Site__c, Embraer_Site_Nickname__c
                                          FROM Account WHERE Id = :accId];
    public Id getAccountId(){
        return accId;
    }
    public boolean hasCSW{get; set;}
    public Integer amountCases {get; set;}
    public Integer delayedCases {get; set;}
    
    public List<Early_Warning__c> getCSWs(){
        List <Early_Warning__c> CSWList = new List<Early_Warning__c>();
        
        CSWList = [select Name, Id, Status__c from Early_Warning__c where Account__c = :accId AND Status__c = 'Active'];
        
        if (CSWList.size() > 0){
            this.hasCSW = true;
            return CSWList;
        }
        else {
            this.hasCSW = false;
            return null;
        }
    }
    
    /*public AccountTeamMember getCam(){
        List<AccountTeamMember> CAM = new List<AccountTeamMember>();
        
        CAM = [select Name, Id, Status__c from AccountTeamMember where AccountId = :accId AND TeamMemberRole = 'CAM - Customer Account Manager'];
        
        If(CAM.)
    }*/
    
    // Get General Customer information
    // Use Account name: thisAccount
    // Fields: Name, Account_Type__c, Company_Nickname__c, Site, Id,
    // Quantity_Embraer_Aircraft_Operator__c, Embraer_Fleet_Type__c,
    // Embraer_a_c_in_service_only__c, Quantity_Embraer_Aircraft_Owner__c,
    // Embraer_Site__c
    public static Account getThisAccount(){return thisAccount;}
    
    // Get Dashboard Information for the Customer
    // Use summary name: dashboardData
    // Fields: CRM360_EOC__c, CRM360_EOC_Indicator__c, CRM360_Finance_Description__c,
    // CRM360_Operational_Performance_Indicator__c,CRM360_Parts_Performance__c, 
    // CRM360_Parts_Performance_Indicator__c, CRM360_Service_Sales__c, 
    // CRM360_Service_Sales_Indicator__c, CRM360_Tech_Issues__c, 
    // CRM360_Finance_Indicator__c, CRM360_Operational_Performance__c,
    // CRM360_Tech_Issues_Indicactor__c, Id
    public static Account_Cockpit__c getDashboardData(){
        List<Account_Cockpit__c> dashboardData = [SELECT CRM360_EOC__c, 
                                            	  CRM360_EOC_Indicator__c, 
                                                  CRM360_EOC_Date__c,
                                            	  CRM360_Finance_Description__c, 
                                                  CRM360_Finance_Indicator__c, 
                                            	  CRM360_Finance_Date__c, 
                                                  CRM360_Operational_Performance__c,
                                            	  CRM360_Operational_Performance_Indicator__c, 
                                            	  CRM360_Operational_Performance_Date__c,
                                            	  CRM360_Parts_Performance__c, 
                                                  CRM360_Parts_Performance_Indicator__c,
                                                  CRM360_Parts_Performance_Date__c,
                                            	  CRM360_Service_Sales__c, 
                                                  CRM360_Service_Sales_Indicator__c,
                                                  CRM360_Service_Sales_Date__c,
                                            	  CRM360_Tech_Issues__c, 
                                                  CRM360_Tech_Issues_Indicator__c, 
                                                  CRM360_Tech_Issues_Date__c,
                                                  CRM360_Highlights__c,
                                                  CRM360_Highlights_Indicator__c,
                                                  CRM360_Highlights_Date__c,
                                                  Creation_Date__c,
                                                  Id
                                            FROM Account_Cockpit__c
                                            WHERE (Record_Type_ID_ref__c = 'CMR360_Dashboard_Data'
                                                   AND Account_Name__c =:thisAccount.Id
                                                   AND Is_Record__c = false)];
        if (dashboardData.size() > 0){
            //dashboardData[0].CRM360_Finance_Description__c = breakLineFromString(dashboardData[0].CRM360_Finance_Description__c.split('\r\n'));
        	return dashboardData[0];
        }
        return null;
    }
    
    public static String getLicense(){
        String profileId = UserInfo.getProfileId();
        Profile profile = [select Name, Id, UserLicenseId from Profile where Id = :profileId];
        UserLicense userLicense = [select Name, Id from UserLicense where Id = :profile.UserLicenseId];
        return userLicense.Name;
    }
    

}