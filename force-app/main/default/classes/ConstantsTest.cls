@isTest
public class ConstantsTest {
    
    @isTest static void checksConstastsValues(){
        
        System.assertEquals(Constants.ETRACK_GET_QUESTIONS, 'ETRACK_GET_QUESTIONS');
    	System.assertEquals(Constants.RCP_OUT_BOUND_PROXY, 'RcpOutBoundProxy');
    
    	System.assertEquals(Constants.BASIC_AUTHORIZATION, 'Basic ');
    	System.assertEquals(Constants.AUTHORIZATION, 'Authorization');
    
    	System.assertEquals(Constants.DAYS, 1);
    	System.assertEquals(Constants.STATUS_CALL_OUT, 'Received');
    	System.assertEquals(Constants.STATUS_RESPONSE, 'SUCCESS');
    
    	System.assertEquals(Constants.NEW_OBJECT_CREATED, 'New Object');
    	System.assertEquals(Constants.OLD_OBJECT_UPDATED_OR_DELETED, 'Old Object');
    	System.assertEquals(Constants.STATUS_ACTIVE, 'Active');
    	System.assertEquals(Constants.STATUS_DELETED, 'Deleted');
    
    	System.assertEquals(Constants.PAYLOAD_SIZE_ERROR, 'STRING_TOO_LONG, Payload: data value too large:');
    
    	System.assertEquals(Constants.RECORD_TYPE_ID, '012i0000001IU3DAAW');
    
    	System.assertEquals(Constants.RCP_ACCOUNT, 'RCP_ACCOUNT');
    	System.assertEquals(Constants.RCP_AIRCRAFT, 'RCP_AIRCRAFT');
    	System.assertEquals(Constants.RCP_TEAM_MEMBER, 'RCP_TEAM_MEMBER');
    
    }
}