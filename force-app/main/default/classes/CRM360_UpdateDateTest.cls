@isTest
private class CRM360_UpdateDateTest {

	private static Id RecordTypeIdCRM360Data = Schema.SObjectType.Account_Cockpit__c.getRecordTypeInfosByName().get('CRM360 Dashboard Data').getRecordTypeId();
    
    @testSetup
    static void setup(){
		//Create an account
        Account testAccount = new Account(Name = 'Test Account record',
                                          Company_Nickname__c = 'TesterComp'); 
        insert testAccount;
        
        List<Account_Cockpit__c> acToInsert = new List<Account_Cockpit__c>();
        
        //Create an Executive Summary relative to the account
        Account_Cockpit__c ac = new Account_Cockpit__c();
        ac.Name = 'Test Meeting';
        ac.Customer_Expectation__c = 'Expects to test stuff';
        ac.Att_Level__c = 'High';
        ac.RecordTypeId = RecordTypeIdCRM360Data;
        ac.Related_EOC__c = 'EOC WW EJET 2019 WARSAW';
        ac.Account_Name__c = testAccount.Id;
        ac.Is_Record__c = false;
        acToInsert.add(ac);
        
        //Create an Executive Summary record relative to the account
        Account_Cockpit__c acRecord = new Account_Cockpit__c();
        acRecord.Name = 'Test Meeting Record';
        acRecord.Customer_Expectation__c = 'Expects to test stuff';
        acRecord.Att_Level__c = 'High';
        acRecord.RecordTypeId = RecordTypeIdCRM360Data;
        acRecord.Related_EOC__c = 'EOC WW EJET 2019 WARSAW';
        acRecord.Account_Name__c = testAccount.Id;
        acRecord.Is_Record__c = true;
        acRecord.Is_Updated__c = false;
        acToInsert.add(acRecord);
        
        insert acToInsert;
        
        //Create an user with Chatter Only license
        Profile profile = [SELECT Id FROM Profile WHERE Name='Account manager for Chatter Only'];
        User user = new User(Alias = 'Test',
                           Country='United Kingdom',
                           Email='test@embraer.com',
                           EmailEncodingKey='UTF-8', 
                           LastName='Test', 
                           LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US',
                           ProfileId = profile.Id,
                           TimeZoneSidKey='America/Los_Angeles', 
                           UserName='testuserpermissions@testorganization.org');
        insert user;
        
    }
    
    @isTest
    static void TestUpdateDateBeforeUpdate(){ 
        
        List<Account_Cockpit__c> acsToUpdate = [SELECT CRM360_EOC_Date__c,
                                                CRM360_Finance_Date__c,
                                                CRM360_Operational_Performance_Date__c,
                                                CRM360_Parts_Performance_Date__c,
                                                CRM360_Service_Sales_Date__c,
                                                CRM360_Tech_Issues_Date__c
                                                FROM Account_Cockpit__c
                                                WHERE RecordTypeId = :RecordTypeIdCRM360Data
                                                AND Is_Record__c = false];

        Test.startTest();
        acsToUpdate[0].CRM360_EOC__c = 'Test';
        acsToUpdate[0].CRM360_Finance_Description__c = 'Test';
        acsToUpdate[0].CRM360_Operational_Performance__c = 'Test';
        acsToUpdate[0].CRM360_Service_Sales__c = 'Test';
        acsToUpdate[0].CRM360_Parts_Performance__c = 'Test';
        acsToUpdate[0].CRM360_Tech_Issues__c = 'Test';
        update acsToUpdate;
        Test.stopTest();
        
        List<Account_Cockpit__c> acTest = [SELECT CRM360_EOC_Date__c,
                                           CRM360_Finance_Date__c,
                                           CRM360_Operational_Performance_Date__c,
                                           CRM360_Parts_Performance_Date__c,
                                           CRM360_Service_Sales_Date__c,
                                           CRM360_Tech_Issues_Date__c,
                                           CRM360_EOC__c,
                                           CRM360_Finance_Description__c,
                                           CRM360_Operational_Performance__c,
                                           CRM360_Service_Sales__c,
                                           CRM360_Parts_Performance__c,
                                           CRM360_Tech_Issues__c
                                           FROM Account_Cockpit__c
                                           WHERE RecordTypeId = :RecordTypeIdCRM360Data];
        
        //Verify if all fields have been updated
		System.assertEquals('Test', acTest[0].CRM360_EOC__c);
		System.assertEquals('Test', acTest[0].CRM360_Finance_Description__c);
        System.assertEquals('Test', acTest[0].CRM360_Operational_Performance__c);
        System.assertEquals('Test', acTest[0].CRM360_Service_Sales__c);
        System.assertEquals('Test', acTest[0].CRM360_Parts_Performance__c);
        System.assertEquals('Test', acTest[0].CRM360_Tech_Issues__c);
        
        //Verify if update date has been updated for all fields
        System.assertEquals(Date.today(), acTest[0].CRM360_EOC_Date__c);
        System.assertEquals(Date.today(), acTest[0].CRM360_Finance_Date__c);
        System.assertEquals(Date.today(), acTest[0].CRM360_Operational_Performance_Date__c);
        System.assertEquals(Date.today(), acTest[0].CRM360_Parts_Performance_Date__c);
        System.assertEquals(Date.today(), acTest[0].CRM360_Service_Sales_Date__c);
        System.assertEquals(Date.today(), acTest[0].CRM360_Tech_Issues_Date__c);
        
    }
    
    @isTest
    static void TestChangeUpdateStatus(){
        
        List<Account_Cockpit__c> acTest = [SELECT Is_Updated__c
                                           FROM Account_Cockpit__c
                                           WHERE RecordTypeId = :RecordTypeIdCRM360Data
                                           AND Is_Record__c = true];
        
		Test.startTest();
        
        //Verify if a record can have its update status changed
        try{
            for(Account_Cockpit__c a : acTest){
                a.Is_Updated__c = true;
            }
            update acTest;
        }
        catch(DMLException e){
            System.assert(e.getMessage().contains('Can´t change the update status'));
        }
        
        Test.stopTest();        
        
    }
    
    static testMethod void TestUserPermissions(){
    
        Test.startTest();
        
        List<Account> accounts = [SELECT Id FROM Account];
            
        Account_Cockpit__c ac = new Account_Cockpit__c();
        ac.Name = 'Test Meeting';
        ac.Customer_Expectation__c = 'Expects to test stuff';
        ac.Att_Level__c = 'High';
        ac.RecordTypeId = RecordTypeIdCRM360Data;
        ac.Related_EOC__c = 'EOC WW EJET 2019 WARSAW';
        ac.Account_Name__c = accounts[0].Id;
        ac.Is_Record__c = true;
        ac.Is_Updated__c = false;
        
        List<User> users = [SELECT Alias, 
                            	   Country, 
                            	   Email, 
                            	   EmailEncodingKey, 
                             	   LastName, 
                            	   LanguageLocaleKey, 
                            	   LocaleSidKey, 
                             	   ProfileId, 
                            	   TimeZoneSidKey, 
                            	   UserName
                            FROM User
                            WHERE Alias = 'Test'];
        
        System.runAs(users[0]){
                        
            insert ac;
            
            List<Account_Cockpit__c> acTest = [SELECT CRM360_EOC_Date__c,
                                          CRM360_Finance_Date__c,
                                          CRM360_Operational_Performance_Date__c,
                                          CRM360_Parts_Performance_Date__c,
                                          CRM360_Service_Sales_Date__c,
                                          CRM360_Tech_Issues_Date__c,
                                          Is_Record__c
                                          FROM Account_Cockpit__c
                                          WHERE Id = :ac.Id];    
            
            System.debug('acTest: '+acTest);                        
            
            System.debug([select Name, Id, UserLicenseId from Profile where Id = :UserInfo.getProfileId()]);
            
            //Verify if register can be updated by the user
            try{
                update acTest;
            }
            catch(DMLException e){
                System.assert(e.getMessage().contains('Register can´t be updated'));
            }
            
            //Verify if register can be deleted by the user
            try{
                delete acTest;
            }
            catch(DMLException e){
                System.assert(e.getMessage().contains('Register can´t be deleted'));
            }
            
            Test.stopTest();          
		}	
    }
	
	@isTest
    static void TestDeleteRecord(){
        
        List<Account_Cockpit__c> acTest = [SELECT Id
                                          FROM Account_Cockpit__c
                                          WHERE RecordTypeId = :RecordTypeIdCRM360Data
                                          AND Is_Record__c = true];
        
        Test.startTest();
        
        //Verify if record can be deleted
        try{
            delete acTest;
        }
        catch(DMLException e){
            System.assert(e.getMessage().contains('Register can´t be deleted'));
        }
        Test.stopTest();
        
    }   
    
}