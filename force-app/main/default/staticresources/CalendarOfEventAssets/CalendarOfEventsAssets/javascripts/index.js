var calendar = function() {
	'use strict';

	function init() {
		scrollActiveSection()
		scrollFixedMenu();
		activeMenuMobile();
		scrollTop();
		// scrollActiveMenu();
	}

	function scrollFixedMenu() {
		var wrapper = $('.ec-wrapper-nav');
		var topSiblings = $(wrapper).prevAll().innerHeight();
		var heightNav = $('nav').height();   
		
		$(wrapper).css({'height': heightNav});
		
		$(document).on('scroll', function(e) {
			
			if($(this).scrollTop() >= topSiblings) {
				$(wrapper).addClass('ec-is-fixed');
			} else {
				$(wrapper).removeClass('ec-is-fixed');
			}
		});
	}

	function scrollTop() {
		$('nav a, .ec-nav-mobile a, .ec-arrow')
			.not('.ec-btn-calendar-mobile').on('click', function(event) {
				event.preventDefault();
				var $id = $(this).attr('href');
				
				$('html, body').animate({
					scrollTop: $($id).offset().top + 1
				}, 1000);

				scrollActiveMenu();
		});
	}

	function scrollActiveMenu() {
		$(document).on('scroll', function() {
			var scrollPosition = $(document).scrollTop();
			
			$('.ec-nav-link').not('.ec-btn-calendar-mobile').each(eachLinksNav);
			
			function eachLinksNav() {
				var $currentLink = $(this);
				var $id = $($currentLink).attr('href');
				var $element = $($id);
				var $offsetTopElement = $element.offset().top;
				
				if($offsetTopElement <= scrollPosition && $offsetTopElement + $($element).height() > scrollPosition) {
					$('.ec-nav .ec-nav-link').removeClass('ec-is-active');
					$('.ec-nav-mobile .ec-nav-link').removeClass('ec-is-active');
					$($currentLink).addClass('ec-is-active');
				} else {
					$($currentLink).removeClass('ec-is-active');
				}
			}
		});
	}

	function scrollActiveSection() {
		$(window).on('scroll', function() {
			var sections = $('section.ec-observing-nav');
			
			for(var i = 0; i < sections.length; i++) {
				var element = $(sections)[i];

				if($(this).scrollTop() >= $(element).offset().top - ($(this).height() / 1.2)) {
					$(element).addClass('is-active');
				} else {
					$(element).removeClass('is-active');
				}
			}
		});
	}

	function activeMenuMobile() {
		$('.ec-btn-calendar-mobile').on('click', function(e) {
			e.preventDefault();
			var $nav = $(this).closest('nav');

			$($nav).toggleClass('ec-nav-mobile-active');
		});

		$(document).on('click', function(e) {
			var $nav = $('.ec-nav');

			if($nav !== e.target && !$nav.has(e.target).length) {
				$($nav).removeClass('ec-nav-mobile-active');
			}
		});
	}

	return {init: init};
}();

$(document).ready(calendar.init());