/*#####################################################################################################
 * CRIADOR: Ayrton Saue Cossuol 
 * DATA: 27/09/2021
 * DESCRICAO: Arquivo que faz a criacao de um PDF de acordo com a tabela do Salesforce fazendo
 * tratamentos dos textos de acordo com os nomes dos avioes.
 * 
 * OBS.: Para capturar a string base64 da imagem a ser usada, utilize essa url para fazer a conversao
 * https://www.base64-image.de/
 *#####################################################################################################
 */

/**
 * Variaveis Globais
 */
// Dimensao da folha do PowerPoint
 let dimension = [254, 143] 
 let marginTable = {
                        top: 30,
                        left: 18,
                        right: 18,
                        bottom: 20,
                    }
let tableSize = dimension[0] - (marginTable.left + marginTable.right)

/**
 * Funcao que gera um PDF personalizavel trazendo algumas informacoes de avioes
 * @param {String} filter 
 * @param {Object} myTable
 * @param {Object} collectionTr
 * @param {Object} details
 * 
 * O resultado dessa funcao e a geracao do PDF 
 */
window.createPDF = function(contents, details) {
    // Inicializacao do metodo jsPDF junto com as dimensoes base do PDF
    window.jsPDF = window.jspdf.jsPDF
    var pdf = new jsPDF({
        orientation: 'l',
        unit: 'mm',
        format: dimension
    })

    Object.entries(contents.body).forEach((value, index) => {
        console.log(value)
        createContentPDF(pdf, details, contents.head, value)

        if ((index + 1) < Object.keys(contents.body).length) {
            pdf.addPage()
        }
    })

    let final_date = datePDF()

    pdf.save("Embraer Aircraft Availabitlity Report " + final_date + ".pdf")

}

/**
 * Funcao que vai fazer a escrita do conteudo no PDF, com todas as caracteristicas 
 * e estilos
 * @param {Object} details
 * @param {Object} pdf 
 * @param {Object} res 
 * @param {Object} dt 
 * @param {Number} count 
 * @param {String} filter 
 */

const createContentPDF = function (pdf, details, headerContent, bodyContent) {
    
    let configWidthColumns = widthColumns(details)

    pdf.autoTable({
        head: [headerContent],
        body: bodyContent[1],

        didDrawPage: function () {
            // Criacao das variaveis de data 
            var data = new Date()
            var complet_string = data.toDateString().split(' ')
            var final_date = complet_string[1] + ' ' + complet_string[2] + nth(complet_string[2]) + ' ' + complet_string[3]


            // Criacao do Header
            pdf.setTextColor(0, 75, 231)
            pdf.setFontSize(5)
            pdf.setFont(undefined, 'bold')
            pdf.text('COMMERCIAL \nAVIATION', 5, 6)

            // Criacao do Titulo
            pdf.setTextColor(0, 75, 231)
            pdf.setFontSize(22)
            pdf.text(`EMBRAER AIRCRAFT AVAILABILITY - ${bodyContent[0]}`, 23, 20)

            // Insercao das linhas do header da tabela
            pdf.setDrawColor(10, 10, 10)
            pdf.setLineWidth(0.2)
            pdf.line(18, 29, 236, 29)
            pdf.line(18, 38.8, 236, 38.8)

            // Rodape
            // OBS.: O texto precisa ter esses espacos para que fique correto na inscricao do PDF
            pdf.setTextColor(0)
            pdf.setFontSize(8)
            pdf.setFont(undefined, 'bold')
            pdf.text('Report generated on ' + final_date, 18, 127)
            pdf.setFont(undefined, 'bold')
            pdf.text('Disclaimer: ', 18, 131)
            pdf.setFont(undefined, 'normal')
            pdf.text('                     The information contained herein is strictly confidential and proprietary of Embraer. The distribution to any third party must be expressly preapproved and\
                \ncannot be used or reproduced without written permission. We do not represent or warrant that such information is or will be always up-to-date, complete, or accurate.\
                \nAny representation or warranty that might be otherwise implied is expressly disclaimed.', 18, 131)

            // Imagem do aviao mais a direita
            var image_plane = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAMAAADzapwJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAzUExURQAAAABQ3wBI5wBK5ABI5wBJ5gBK5wBJ5gBK5wBL6ABK6QBK6ABJ6ABK6ABK6ABK6ABK6ELY+zMAAAAQdFJOUwAQIDBAUGBwgI+fr7/P3+8jGoKKAAAACXBIWXMAABcRAAAXEQHKJvM/AAAAjElEQVQoU2WRWxLEIAgENc91o8D9TxuQMYnaf9NVUjiEv4iUNUxc6uVE+FDMJ4SXSObLgviwsHnZER+2qudB8CUiN073vCE3bH3jh9zI8NljIQAtQqRPiB04na+8o2oHSu6+lWBTt2NbsP/p4aOHXla3/QBUxQciiLXw8UKxrnaNLVkbPB3NVqPhAiHcqasKs92AEGQAAAAASUVORK5CYII="
            pdf.addImage(image_plane, 'PNG', 243, 4, 4, 4)

            // Designer do retangulo
            pdf.setFillColor(details.disclaimer.color.R,
                details.disclaimer.color.G,
                details.disclaimer.color.B);
            pdf.rect(249, 0, 5, 143, 'F')

            // Texto na vertical ao lado direito
            pdf.setFontSize(5.5)
            pdf.setTextColor(255)
            pdf.text(details.disclaimer.name.toUpperCase() + ' INFORMATION / This information belongs to Embraer and cannot be used or reproduced without written permission from the Company.', 252, 137, null, 90)
        },

        // Proporcoes da tabela
        margin: marginTable,

        // funcao para fazer a configuracao do tamanho das colunas
        columnStyles: configWidthColumns,

        // funcao para que a pagina seja quebrada quando o texto excede o tamanho da tabela
        rowPageBreak: 'avoid',

        // Estilos aplicados a tabela 
        styles: {
            cellPadding: { top: 2.5, right: 2.5, bottom: 2.5, left: 2.5 },
            overflow: 'linebreak',
            valign: 'middle',
            halign: 'center',
            fontSize: 9,
            textColor: [0, 0, 0]
        },

        headStyles: {
            fillColor: [255, 255, 255],
            lineColor: [255, 255, 255],
            textColor: [0, 0, 0]
        },

        alternateRowStyles: {
            fillColor: [220, 220, 220],
        },
    })
}

/**
 * Funcao que cria a estrutura em json que e preciso para que as configuracoes da largura
 * das colunas possam ser aplicadas
 * @param {Object} details 
 * @returns Um json com todas as caracteristicas de largura da tabela
 */
const widthColumns = function (details) {
    let json = {}
    console.log(details)
    details.columns.forEach((name, index) => {
        if (details.columnsAutoWidth === false) {
            var _width = (tableSize * parseFloat(name.width.replace('%', '').trim())) / 100.0
            json = { ...json, [index]: { cellWidth: _width } }
        } else {
            json = { ...json, [index]: { cellWidth: 'auto' } }
        }
    })

    return json
}

/**
 * Funcao que auxilia a geracao da formatacao do dia
 * @param {Number} d 
 * @returns Retorna a formatacao adequada para o dia relacionado
 */
const nth = function (d) {
    if (d > 3 && d < 21) return 'th';

    switch (d % 10) {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
        default: return "th";
    }
}

/**
 * Funcao que faz a estilizacao do numero do dia 
 * @returns Retorna o dia estilizado
 */
const datePDF = function () {
    var date = new Date()
    var complet_string = date.toDateString().split(' ')
    var date_month
    var final_date

    if ((date.getMonth() + 1) < 10) {
        date_month = "0" + (date.getMonth() + 1)
    } else {
        date_month = date.getMonth() + 1
    }
    final_date = complet_string[3] + '.' + date_month + '.' + complet_string[2]

    return final_date
}