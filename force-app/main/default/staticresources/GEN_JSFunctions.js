/* JavaScript file with some utilities fuctions
 * Some functions for visualforce page with lightining design system SLDS
 * JQuery dependency | Loader JQueryUI dependency
 * 
 * Author: Marcilio Leite de Souza
 */

/*
* Check the field is required
* SLDS - error
*/
function isRequired(idDiv, idLabel, idElem){
    
    var erro = false;
    var msg = '<div id="error-'+idElem+'" class="slds-form-element__help"> '+$.elem(idLabel).text().replace('*','')+' is required. </div>';
    
    if($.isBlank($.elem(idElem).val()) || $.elem(idElem).val() == '--None--'){
        erro = true;
    }
    generateDivError(erro, msg, idDiv, idElem);
    return erro;
}

/*
* Check the field is a valid email
* SLDS - error
*/
function isEmail(idDiv, idLabel, idElem){
    var erro = false;
    var msg = '<div id="error-'+idElem+'" class="slds-form-element__help"> Email invalid. '+'</div>';
    
    if(!regexEmail($.elem(idElem).val())){
        erro = true;
    }
    generateDivError(erro, msg, idDiv, idElem);
    return erro;
}

/*
* Return the div error msg for SLDS
*/
function getDivErro(idElem, msg){
    return '<div id="error-'+idElem+'" class="slds-form-element__help"> ' + msg +' </div>';
}

/*
* Check date format DD/MM/AAAA
* SLDS - error
*/
function isDate(dt) {
    var erro = false;
    var r = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
    
    if (!((dt.match(r)) && (!$.isBlank(dt)))) {
        erro = true;
    }
    return erro;
};

/*
* Check the field time is valid
* SLDS - error
*/
function isTimeValid(idDiv, idLabel, idElem){
    var erro = false;
    var msg = '<div id="error-'+idElem+'" class="slds-form-element__help"> '+$.elem(idLabel).text().replace('*','')+' is invalid. </div>';
    
    erro = isTime($.elem(idElem).val());
    
    //End time could be blank
    //if($.isBlank($.elem('idEndTime').val())){erro = false}
    
    generateDivError(erro, msg, idDiv, idElem); console.log('istimevalid' + erro);
    return erro;
   
}

/*
* Check time format HH:MM
*/
function isTime(tm) {
    var erro = true;
    
    if(tm.length < 5){
        erro = true;
    }else if((tm.substr(0,2) >= 0 && tm.substr(0,2) <= 24) &&
             (tm.substr(3,2) >= 0 && tm.substr(3,2) <= 59)){
        erro = false;
    }
    
    return erro;
};

/*
* Regex validate email format
*/
function regexEmail(email) {
    var r = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return r.test(email);
};

/*
* Return object field and if it is valid
*/
function fieldValid(idElem, isValid) {
    return {
        idElem: idElem,
        isValid: isValid
    };
}

/*
* Return int value
*/
function getIntFromElem(idElem){
    if($.isBlank($.elem(idElem).val())){
        return 0;
    }else{
        return parseInt($.elem(idElem).val());
    }
}

/*
 * Restrict input numbers to text fields
 */
$("input.numerico").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        (e.keyCode == 65 && e.ctrlKey === true) || 
        (e.keyCode == 86 && e.ctrlKey === true) || 
        (e.keyCode == 67 && e.ctrlKey === true) || 
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

/*
* Restrict paste text to numbers fields
*/
$('input.numerico').on('paste', function () {
    var e = this;
    setTimeout(function () {
        var text = $(e).val();
        if(!$.isNumeric(text)) {
            $(e).val("");
        }	             
    }, 100);
});

/*
* Check value is blank or null
*/
(function($){
    $.isBlank = function(obj){
        return(!obj || $.trim(obj) === "");
    };
})(jQuery);

/*
 * Get visualforce element by id
 */
(function($){
    $.elem = function(idElem){
        return(jQuery('[id$='+idElem+']'));
    };
})(jQuery);

/*
 * Get visualforce element by class
 */
(function($){
    $.elemClass = function(idClass){
        return(jQuery("."+idClass));
    };
})(jQuery);