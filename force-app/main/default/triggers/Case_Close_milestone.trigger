trigger Case_Close_milestone on Case (after delete, after insert, after undelete, after update) 
{   
    if(!TriggerUtils.isEnabled('Case')) return;
    
    if (Trigger.isUpdate) 
    {
        Case contextCase = Trigger.New[0];
        if (CRC_TriggerValidation.isCRCCase(contextCase)){}
        else{
            CaseTrgCloseMilestone.processar();
            CaseTrgCloseMilestone_YTime.processar();
            // Caseupdatemilestonestdate.execute();
        }
    }
    
    if(Trigger.isInsert){
        new CaseUpdateSubject(Trigger.new).updateSubjectToInsert();
    }
    
}