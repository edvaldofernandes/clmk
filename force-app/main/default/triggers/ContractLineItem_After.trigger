/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
* Trigger to dispatch After actions on ContractLineItem
*
* NAME: ContractLineItem_After.trigger
* AUTHOR: DPF                                                DATE: 16/12/2014
*
*******************************************************************************/
trigger ContractLineItem_After on ContractLineItem (after delete, after insert, after undelete, 
after update) {
	
	if(!TriggerUtils.isEnabled('ContractLineItem')) return;
  
  if (Trigger.isInsert) {
    ContractLineItemCopyRelatedAircraft.execute();
    LineItemSearchSlot.recalcSlots();
  }
  else
  if (Trigger.isUpdate) {
    LineItemSearchSlot.recalcSlots();
  }
  else
  if (Trigger.isDelete) {
    LineItemSearchSlot.recalcSlots();
    ContractLineItemEntryFeeUpdateNrec.clearEntryFee();
  }
}