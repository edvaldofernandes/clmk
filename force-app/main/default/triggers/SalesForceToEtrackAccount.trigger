trigger SalesForceToEtrackAccount on Account (after insert, after update, after delete) {
    
    SalesForceAccountInformationToEtrack sfAccountInformationToEtrack = new SalesForceAccountInformationToEtrack();
    
    if(Trigger.IsInsert || Trigger.IsUpdate)
        sfAccountInformationToEtrack.informationAfterInsertOrUpdate(Trigger.new);
    
    if(Trigger.IsUpdate)
        sfAccountInformationToEtrack.informationAfterUpdate(Trigger.old);
    
    if(Trigger.IsUpdate)
        sfAccountInformationToEtrack.informationAfterDelete(Trigger.old);
    
    /*if(Test.isRunningTest())return;
    
    List<AccountInformation> accountList = new  List<AccountInformation>();
    List<AccountHistoryInformation> accountHistoryInformationList = new List<AccountHistoryInformation>();
    
    if(Trigger.IsInsert || Trigger.IsUpdate){
        
        Map<Id, User> userMap = TriggerService.findUserByCreatedById(Trigger.new[0].id);
        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(Trigger.new[0].id)){
            
            User user = userMap.get(accountHistory.CreatedById);
            
            String namePhoneAndEmail = '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';

            accountHistoryInformationList.add(new AccountHistoryInformation(accountHistory.CreatedDate, 
                                                                            accountHistory.Field,
                                                                            String.valueOf(accountHistory.OldValue),
                                                                            String.valueOf(accountHistory.NewValue),
                                                                            namePhoneAndEmail));
        }
        
        for(Account accountNew : Trigger.new){
            accountList.add(new AccountInformation(accountNew, Constants.NEW_OBJECT_CREATED, Constants.STATUS_ACTIVE));   
        }
    }
    
    if(Trigger.IsUpdate){
        
        Map<Id, User> userMap = TriggerService.findUserByCreatedById(Trigger.old[0].id);

        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(Trigger.old[0].id)){
            
            User user = userMap.get(accountHistory.CreatedById);
            
            String namePhoneAndEmail = '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';

            accountHistoryInformationList.add(new AccountHistoryInformation(accountHistory.CreatedDate, 
                                                                            accountHistory.Field,
                                                                            String.valueOf(accountHistory.OldValue),
                                                                            String.valueOf(accountHistory.NewValue),
                                                                            namePhoneAndEmail));
        }
        
        for(Account accountOld : Trigger.old)
            accountList.add(new AccountInformation(accountOld, Constants.OLD_OBJECT_UPDATED_OR_DELETED, Constants.STATUS_ACTIVE));
    }
    
    if(Trigger.IsDelete){
        
        Map<Id, User> userMap = TriggerService.findUserByCreatedById(Trigger.old[0].id);
        
        for(AccountHistory accountHistory : RcpRepository.findAccountHistoryByAccountId(Trigger.old[0].id)){
            
            User user = userMap.get(accountHistory.CreatedById);
            
            String namePhoneAndEmail = '[Phone : '+user.Phone+' : email : '+user.E_mail__c+']';
            
            accountHistoryInformationList.add(new AccountHistoryInformation(accountHistory.CreatedDate, 
                                                                            accountHistory.Field,
                                                                            String.valueOf(accountHistory.OldValue),
                                                                            String.valueOf(accountHistory.NewValue),
                                                                            namePhoneAndEmail));
        }
        
        for(Account accountDelete : Trigger.old)
            accountList.add(new AccountInformation(accountDelete, Constants.OLD_OBJECT_UPDATED_OR_DELETED, Constants.STATUS_DELETED));
    }
    
    Visitator visitator = new CallOutRepository();
    visitator.visit(accountList, accountHistoryInformationList, String.valueOf(AccountInformation.class));
    
    RcpOutBoundProxy.executeAccount();    
    SalesForceToEtrackProxy.executeAccount();*/
    
}