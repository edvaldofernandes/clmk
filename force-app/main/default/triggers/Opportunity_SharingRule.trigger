/*******************************************************************************
*                               Embraer - 2015
*-------------------------------------------------------------------------------
*
* Trigger to create a sharing rule for Oppportunity
* NAME: ServiceContract_SharingRule.trigger
* AUTHOR: Bruno de Oliveira Severino                           DATE: 08/04/2015
*
*******************************************************************************/
trigger Opportunity_SharingRule on Opportunity (after insert,before update) {    
    if (Trigger.isBefore){
        TriggerUtils.assertTrigger();
        List<Id> listOpp = new List<Id>();
    	for ( Opportunity opp : (list<Opportunity>) trigger.new )
      		if ( TriggerUtils.wasChanged(opp, Opportunity.Service_Program_Type__c) || TriggerUtils.wasChanged(opp, Opportunity.OwnerId)) //only if the owner or service program has changed
                listOpp.add(opp.Id);       
        
    	if ( !listOpp.isEmpty() )  
        	delete [select id from OpportunityShare where OpportunityId IN :listOpp and RowCause = 'Manual'];
    }
    
    Opportunity_SharingRule oppSR = new Opportunity_SharingRule();
    oppSR.newSharingRule();
}