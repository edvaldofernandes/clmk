/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Aircraft Trigger execute in handler TriggerHandlerAircraft
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
trigger TriggerAircraft on Aircraft__c (after delete, after insert, after undelete, after update)  {
	 
    if(!GEN_TriggerHelper.isTriggerEnabled()) { 
        return ; 
    } 
    
    GEN_TriggerFactory.createhandler(Aircraft__c.sObjectType);  
}