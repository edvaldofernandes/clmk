/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on OpportunityLineItem
* NAME: OpportunityLineItem_Before.trigger
* AUTHOR: LRSA                                                DATE: 02/12/2014
*
*******************************************************************************/
trigger OpportunityLineItem_Before on OpportunityLineItem (before delete, before insert, before update) {
	
	if(!TriggerUtils.isEnabled('OpportunityLineItem')) return;
  
  if (Trigger.isInsert) {
    OppLineItemSearchSlot.newProducts();  
  }
  else
  if (Trigger.isUpdate) {
    OppLineItemSearchSlot.newProducts();
  }

}