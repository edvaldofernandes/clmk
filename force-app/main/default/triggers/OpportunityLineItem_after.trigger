/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch After actions on OpportunityLineItem
* NAME: OpportunityLineItem_after.trigger
* AUTHOR: LRSA                                                DATE: 04/12/2014
*
*******************************************************************************/
trigger OpportunityLineItem_after on OpportunityLineItem (after delete, after insert, after undelete, 
  after update) 
{
	if(!TriggerUtils.isEnabled('OpportunityLineItem')) return;
  
  if (Trigger.isInsert) {
    OpportunityCompetingProducts.execute();
    LineItemSearchSlot.recalcSlots();
  }
  if (Trigger.isUpdate) {
    LineItemSearchSlot.recalcSlots();
  }
  else
  if (Trigger.isDelete) {
    OppLineItemDeleteRelatedAircraft.execute();
    LineItemSearchSlot.recalcSlots();
    OpportunityLineItemEntFeeUpdateNrec.clearEntryFee();
  }
}