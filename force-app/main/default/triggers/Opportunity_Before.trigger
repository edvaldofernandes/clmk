/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on Opportunity
* NAME: Opportunity_Before.trigger
* AUTHOR: DPF                                                DATE: 09/12/2014
*
*******************************************************************************/
trigger Opportunity_Before on Opportunity (before delete, before insert, before update) {

  if(!TriggerUtils.isEnabled('Opportunity')) return;

  if (Trigger.isInsert) {
    OpportunityCheckContactRole.checkUpdateStatus();
  }
  else
  if (Trigger.isDelete) {
    OpportunityDeleteRelatedAircraft.execute();
    OpportunityUpdateSlot.delOpp();
  }
  else
  if (Trigger.isUpdate) {
    OpportunityCheckContactRole.checkUpdateStatus();
    OpportunityCheckContactRole.checkUpdateAccount();
  }
}