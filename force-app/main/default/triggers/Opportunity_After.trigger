/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch After actions on Opportunity
* NAME: Opportunity_After.trigger
* AUTHOR: LRSA                                                DATE: 03/12/2014
*
*******************************************************************************/

trigger Opportunity_After on Opportunity (after delete, after insert, after undelete,
after update)
{
  if(!TriggerUtils.isEnabled('Opportunity')) return;

  if (Trigger.isUpdate) {
    OpportunityUpdateSlot.closeOpp();
    OpportunityUpdateQuote.execute();
  }
}