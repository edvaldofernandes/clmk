/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Trigger to dispatch After actions on ServiceContract
*
* NAME: ServiceContract_After.trigger
* AUTHOR: JFS                                                 DATE: 04/12/2014
*******************************************************************************/
trigger ServiceContract_After on ServiceContract (after delete, after insert, after undelete,
after update) {

  if(!TriggerUtils.isEnabled('ServiceContract')) return;

  if (Trigger.isInsert) {  
    ContractAddProductAircraftOppAfter.execute();
    ServiceContractChatter.newServiceContract();
    ServiceContractUpdateOpportunity.execute();
  }
  else
  if (Trigger.isUpdate) {
    ServiceContractChatter.updateServiceContract();
    ServiceContractUpdateOpportunity.execute();
    ServiceContractUpdateSlot.closeOpp();
    //SvcContractUpdateStatusSvcContract.execute();
  }
}