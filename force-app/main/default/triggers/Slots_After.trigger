/*******************************************************************************
*                              Cloud2b - 2015
*-------------------------------------------------------------------------------
* Trigger to dispatch After actions on Slots__c
*
* NAME: Slots_After.trigger
* AUTHOR: RLdO                                                 DATE: 10/03/2015
*******************************************************************************/
trigger Slots_After on Slots__c (after delete, after insert, after undelete, 
  after update)
{
  if(!TriggerUtils.isEnabled('Slots__c')) return;

  if (Trigger.isUpdate)
  {
    LineItemSearchSlot.updateQuantity();
  }
}