trigger Case_Before on Case (before delete, before insert, before update) 
{
    if(!TriggerUtils.isEnabled('Case')) return;
    
    if (Trigger.isInsert) 
    {
        //CaseTrgUpdateEntitlement.processar();
        CaseAssignEntitlementCase.execute();
        new CaseAggregateStatus(Trigger.new, new Map<Id, Case>()).assignStatusAggregate();
        System.debug('##CASE_BEFORE ' + Trigger.new);
        if(Label.CaseAutomation == 'true'){
            System.debug('##AUTOMATION TRUE');
            new AssignAccount(Trigger.new).assignAccountToCase();
        	new AssignContact(Trigger.new).assignContact();
        }
        System.debug('##CASE_AFTER ' + Trigger.new);
        new CaseNextAction(Trigger.new, new Map<Id, Case>()).setDateNextAction();
    }
    
    if(Trigger.isUpdate) {
        new CaseAggregateStatus(Trigger.new, (Map<Id, Case>) Trigger.oldMap).assignStatusAggregate();
        new CaseNextAction(Trigger.new, (Map<Id, Case>) Trigger.oldMap).setDateNextAction();
        new CaseUpdateSubject(Trigger.new).updateSubject();
    } 

}