trigger OOS_Trigger on Out_of_Service__c(
  after insert,
  after update,
  before insert,
  before update
) {
  OOS_TriggerHandler handler = new OOS_TriggerHandler();

  handler.isInsert = Trigger.isInsert;
  handler.isUpdate = Trigger.isUpdate;
  handler.isBefore = Trigger.isBefore;
  handler.isAfter = Trigger.isAfter;
  handler.newRecords = Trigger.new;
  handler.oldRecords = Trigger.old;

  if (Trigger.isBefore) {
    if (Trigger.isInsert) {
      handler.onBeforeInsert();
    }
    if (Trigger.isUpdate) {
      handler.onBeforeUpdate();
    }
  } else {
    if (Trigger.isInsert) {
      handler.onAfterInsert();
    }
  }

}