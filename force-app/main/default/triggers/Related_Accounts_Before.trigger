/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Acoes before no objeto Related_accounts__c
*
* NAME: Related_Accounts_Before.trigger
* AUTHOR: RLdO                                                 DATE: 19/05/2014
*******************************************************************************/
trigger Related_Accounts_Before on Related_accounts__c (before delete, before insert, before update) 
{
  if(!TriggerUtils.isEnabled('Related_accounts__c')) return;
  
	if (Trigger.isInsert) {
		RelatedAccountCriaRegistro.processar();
	}
	else if ( Trigger.isDelete ) {
		RelatedAccountCriaRegistro.excluir();
	}
}