trigger AccountHasCRM360Data on Account_Cockpit__c (after insert, after update, after delete) {
    
    
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        List <Id> accountsIds = new List<Id>();
        for(Account_Cockpit__c eSum: Trigger.New){
            If(eSum.Record_Type_ID_ref__c == 'CMR360_Dashboard_Data'){
                accountsIds.add(eSum.Account_Name__c);
                System.debug(eSum.Account_Name__c);
            }
        }
        System.debug(accountsIds);
        
        List<Account> AccList = new List<Account>();
        AccList = [select Id, Name, Has_CRM360_Data__c from Account where Id in :accountsIds];
        System.debug(AccList);
        
        if(AccList.size() > 0){
            for(Account acc : AccList){
                if(acc.Has_CRM360_Data__c == false){
                    acc.Has_CRM360_Data__c = true;
                }
            }
        }
        update AccList;
    }
    if(Trigger.isAfter && Trigger.isDelete){
        List <Id> accountsIds = new List<Id>();
        for(Account_Cockpit__c eSum: Trigger.Old){
            If(eSum.Record_Type_ID_ref__c == 'CMR360_Dashboard_Data'){
                accountsIds.add(eSum.Account_Name__c);
                System.debug(eSum.Account_Name__c);
            }
        }
        
        System.debug(accountsIds);
        
        List<Account> AccList = new List<Account>();
        AccList = [select Id, Name, Has_CRM360_Data__c from Account where Id in :accountsIds];
        System.debug(AccList);
        
        if(AccList.size() > 0){
            for(Account acc : AccList){
                if(acc.Has_CRM360_Data__c == true){
                    acc.Has_CRM360_Data__c = false;
                }
            }
        }
        update AccList;
    }

}