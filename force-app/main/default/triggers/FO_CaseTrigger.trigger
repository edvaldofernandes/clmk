trigger FO_CaseTrigger on Case (after delete, after insert, after undelete,after update, before delete, before insert, before update) 
{
    FO_CaseTriggerHandler handler = new FO_CaseTriggerHandler(true);
     Set<String> accIdSet = new Set<String>(); 
    handler.isInsert = Trigger.isInsert;
    handler.isUpdate = Trigger.isUpdate;
    handler.isDelete = Trigger.isDelete;
    handler.isUndelete = Trigger.isUndelete;
    handler.isBefore = Trigger.isBefore;
    handler.isAfter = Trigger.isAfter;
    handler.newRecords = Trigger.new;
    handler.oldRecords = Trigger.old;
    handler.newRecordsMap = Trigger.newMap;
    handler.oldRecordsMap = Trigger.oldMap;
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.OnBeforeInsert();
            SpecialInvestigationsTriggerHandler.beforeInsert(Trigger.new);
            
        }
        if(Trigger.isUpdate){
            handler.OnBeforeUpdate();
            
           

        }
        if(Trigger.isDelete){
            handler.OnBeforeDelete();
        }
    }
    else{
        if(Trigger.isInsert){
            handler.OnAfterInsert();
            EpoolCaseTriggerHandler.handleNewEpoolCasesAsync(Trigger.new);
            SpecialInvestigationsTriggerHandler.afterInsert(Trigger.new);
           
            
        }
        if(Trigger.isUpdate){
            handler.OnAfterUpdate();
          //  UpdatePartsInBackOrder.Run();
         //  if(ClassHandlerRm.isFirstTime)
    //{
      //  ClassHandlerRm.isFirstTime = false; 
        //   UpdateReservedCaseTrigger.afterInsert(Trigger.new);
          //  }
        }
        if(Trigger.isDelete){
            handler.OnAfterDelete();
        }
        if(Trigger.isUnDelete){
            handler.OnUndelete();
        }
    }
}