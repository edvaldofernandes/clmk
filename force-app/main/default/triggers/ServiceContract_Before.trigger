/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Trigger to dispatch Before actions on ServiceContract
*
* NAME: ServiceContract_Before.trigger
* AUTHOR: JFS                                                 DATE: 04/12/2014
*******************************************************************************/
trigger ServiceContract_Before on ServiceContract (before delete, before insert, before update) {

 if (Trigger.isInsert) { 	
 	  ContractAddProductAircraftOppBefore.execute();
 	  ServiceContractCopyContactFromOpp.execute();
  }
  else
  if (Trigger.isDelete) {
    ServiceContractDeleteRelatedAircraft.execute();
    ServiceContractUpdateSlot.delOpp();
  }
}