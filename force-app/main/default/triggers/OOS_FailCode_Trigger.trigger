trigger OOS_FailCode_Trigger on FC_OOS_Association__c(after insert) {
  List<Id> newIds = new List<Id>();

  for (FC_OOS_Association__c fcOOS : Trigger.new) {
    newIds.add(fcOOS.Id);
  }

  List<FC_OOS_Association__c> allFcOOS = [
    SELECT
      Out_of_service__r.ATA__c,
      Out_of_service__r.Technology__c,
      Out_of_service__r.Fail_Code__c,
      Fail_Code__r.Name,
      Fail_Code__r.ATA__c,
      Fail_Code__r.Technology_Name__c
    FROM FC_OOS_Association__c
    WHERE Id IN :newIds
  ];
  for (FC_OOS_Association__c fcOOS : allFcOOS) {
    if (fcOOS.Out_of_service__r.ATA__c == null) {
      fcOOS.Out_of_service__r.ATA__c = fcOOS.Fail_Code__r.ATA__c;
      fcOOS.Out_of_service__r.Fail_Code__c = fcOOS.Fail_Code__r.Name;
      fcOOS.Out_of_service__r.Technology__c = fcOOS.Fail_Code__r.Technology_Name__c;
      Database.update(fcOOS.Out_of_service__r);
    }
  }
}