trigger UpdateMarginCalculatorTrigger on OpportunityLineItem (after insert, after update, before delete, after delete) {
    MarginCalculatorHandler mch = new MarginCalculatorHandler();
    boolean hasInserted = false;
    

    if(Trigger.isInsert){
        /*System.debug('Trigger New Size = ' + Trigger.new.Size());
        System.debug('Trigger Is Insert');*/
        mch.triggerDataHandler(Trigger.new);
    } else if(Trigger.isUpdate){
        Integer count = 0;
        for( OpportunityLineItem oppNew: Trigger.new){
            OpportunityLineItem oppOld = Trigger.oldMap.get(oppNew.Id);
            if(oppOld.Quantity != oppNew.Quantity || oppOld.UnitPrice !=oppNew.UnitPrice){
                count++;
            }
        }
        /*if(count == Trigger.new.size()){
            System.debug('count OK');
        }
        else System.debug('count N Ok');*/
        System.debug('size: '+Trigger.new.size());
        System.debug('count: '+count);
        if(count != 0){
            if(CheckRecursiveTriggerUtils.runOnce()){
                /*System.debug('It ran Once');
                System.debug('Trigger New Size REC = ' + Trigger.new.size());
                //System.debug('Trigger Content:' + Trigger.new);
                for( OpportunityLineItem opp: Trigger.old){
                    System.debug('Trigger old Content:' + opp.Quantity);
                }
                for( OpportunityLineItem opp: Trigger.new){
                    System.debug('Trigger new Content:' + opp.Quantity);
                    System.debug('Database: '+[SELECT Quantity FROM OpportunityLineItem WHERE Id = :opp.Id]);
                }            
                System.debug('Trigger New Size = ' + Trigger.new.size());
                System.debug('Trigger Is update');
                System.debug('Trigger Content:' + Trigger.new);*/
                mch.triggerDataHandler(Trigger.new);
            }
        }
        /*else{
            System.debug('Recursive Test');
            System.debug('Trigger New Size REC = ' + Trigger.new.size());
            //System.debug('Trigger Content:' + Trigger.new);
            for( OpportunityLineItem opp: Trigger.old){
                System.debug('Trigger old Content:' + opp.Quantity);
            }
            for( OpportunityLineItem opp: Trigger.new){
                System.debug('Trigger new Content:' + opp.Quantity);
                System.debug('Database: '+[SELECT Quantity FROM OpportunityLineItem WHERE Id = :opp.Id]);
            }
        }*/
        
        
        
    }
    
    if( Trigger.isDelete ){
        if(Trigger.isBefore){
            mch.triggerBeforeDeleteHandler(Trigger.old);
        }
        if(Trigger.isAfter){
            mch.triggerAfterDeleteHandler(Trigger.old);
        }
        
    }
}