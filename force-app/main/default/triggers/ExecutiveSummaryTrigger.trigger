trigger ExecutiveSummaryTrigger on Account_Cockpit__c (after delete, after insert, after undelete,after update, before delete, before insert, before update) 
{

    ExecutiveSummaryTriggerHandler handler = new ExecutiveSummaryTriggerHandler(true);
    
    handler.isInsert = Trigger.isInsert;
    handler.isUpdate = Trigger.isUpdate;
    handler.isDelete = Trigger.isDelete;
    handler.isUndelete = Trigger.isUndelete;
    handler.isBefore = Trigger.isBefore;
    handler.isAfter = Trigger.isAfter;
    handler.newRecords = Trigger.new;
    handler.oldRecords = Trigger.old;
    handler.newRecordsMap = Trigger.newMap;
    handler.oldRecordsMap = Trigger.oldMap;
    
    if(Trigger.isBefore)
    {
       
        if(Trigger.isInsert)
            handler.OnBeforeInsert();
        
        if(Trigger.isUpdate)
            handler.OnBeforeUpdate();
        
         
        if(Trigger.isDelete)
            handler.OnBeforeDelete();
           
          
         
    }
    else
    {
        
        if(Trigger.isInsert)
            handler.OnAfterInsert();
         
        if(Trigger.isUpdate)
            handler.OnAfterUpdate();
        
        if(Trigger.isDelete)
            handler.OnAfterDelete();
    
        if(Trigger.isUnDelete)
            handler.OnUndelete();
        
    }
    

}