/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on Pricebook2
*
* NAME: Pricebook_Before.trigger
* AUTHOR: KHPS                                               DATE: 18/12/2014
*******************************************************************************/
trigger Pricebook_Before on Pricebook2 (before delete, before insert, before update) 
{
	if(!TriggerUtils.isEnabled('Pricebook2')) return;
	
	if(Trigger.isInsert) {
		Pricebook2PreventsName.execute();
	}
}