trigger FinancialDisputeTrigger on Financial_Dispute__c (after delete, after insert, after undelete,after update, before delete, before insert, before update) 
{

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            for(Financial_Dispute__c dispute : Trigger.new){
                dispute.Comments_History__c = '(' + Date.Today().format() +' by '+ UserInfo.getName() +'): ' + dispute.Comments__c;
            }            
        }
        if(Trigger.isUpdate){
            for(Financial_Dispute__c dispute : Trigger.new){
                if(dispute.Comments__c != null){ 
                    dispute.Comments_History__c = '(' + Date.Today().format() +' by '+ UserInfo.getName() +'): ' + dispute.Comments__c + '\n' + Trigger.oldMap.get(dispute.Id).Comments_History__c;
                    dispute.Comments__c= null;
                }
            }            
        }
    }
}