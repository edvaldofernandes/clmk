trigger CRM360_UpdateDate on Account_Cockpit__c (before update, before delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            for(Account_Cockpit__c accountCockpitNew : trigger.new){

                if(accountCockpitNew.Record_Type_ID_ref__c == 'CMR360_Dashboard_Data'){                  
                    
                    Account_Cockpit__c accountCockpitOld = Trigger.oldMap.get(accountCockpitNew.Id);

                    if(accountCockpitOld.Is_Record__c){
                        
                        String profileId = UserInfo.getProfileId();
                        Profile profile = [select Name, Id, UserLicenseId from Profile where Id = :profileId];
                        
                        Account account = [SELECT Id, Name, OwnerId 
                                           FROM Account 
                                           WHERE Id = :accountCockpitNew.Account_Name__c];
                                                          
                        if(profile.Name != 'Salesforce Administrator' 
                           && profile.Name != 'System Administrator'
                           && account.OwnerId != UserInfo.getUserId()){
                            accountCockpitNew.addError('Register can´t be updated.');
                        }
                        else if(account.OwnerId == UserInfo.getUserId()
                           && accountCockpitNew.Is_Updated__c != accountCockpitOld.Is_Updated__c){
                            accountCockpitNew.addError('Can´t change the update status');
                        }
                        
                    }
                    
                    if(accountCockpitNew.CRM360_Highlights__c != accountCockpitOld.CRM360_Highlights__c ||
                       accountCockpitNew.CRM360_Highlights_Indicator__c != accountCockpitOld.CRM360_Highlights_Indicator__c){
                           accountCockpitNew.CRM360_Highlights_Date__c = Date.today();
                       }
                    if(accountCockpitNew.CRM360_EOC__c != accountCockpitOld.CRM360_EOC__c ||
                       accountCockpitNew.CRM360_EOC_Indicator__c != accountCockpitOld.CRM360_EOC_Indicator__c){
                        accountCockpitNew.CRM360_EOC_Date__c = Date.today();
                    }
                        
                    if(accountCockpitNew.CRM360_Finance_Description__c != accountCockpitOld.CRM360_Finance_Description__c ||
                      accountCockpitNew.CRM360_Finance_Indicator__c != accountCockpitOld.CRM360_Finance_Indicator__c){
                        accountCockpitNew.CRM360_Finance_Date__c = Date.today(); 
                    }
                    if(accountCockpitNew.CRM360_Operational_Performance__c != accountCockpitOld.CRM360_Operational_Performance__c ||
                      accountCockpitNew.CRM360_Operational_Performance_Indicator__c != accountCockpitOld.CRM360_Operational_Performance_Indicator__c){
                        accountCockpitNew.CRM360_Operational_Performance_Date__c = Date.today();
                    }
                        
                    if(accountCockpitNew.CRM360_Parts_Performance__c != accountCockpitOld.CRM360_Parts_Performance__c ||
                      accountCockpitNew.CRM360_Parts_Performance_Indicator__c != accountCockpitOld.CRM360_Parts_Performance_Indicator__c){
                        accountCockpitNew.CRM360_Parts_Performance_Date__c = Date.today();
                    }
                        
                    if(accountCockpitNew.CRM360_Service_Sales__c != accountCockpitOld.CRM360_Service_Sales__c ||
                      accountCockpitNew.CRM360_Service_Sales_Indicator__c != accountCockpitOld.CRM360_Service_Sales_Indicator__c){
                        accountCockpitNew.CRM360_Service_Sales_Date__c = Date.today();
                    }
                        
                    if(accountCockpitNew.CRM360_Tech_Issues__c != accountCockpitOld.CRM360_Tech_Issues__c ||
                      accountCockpitNew.CRM360_Tech_Issues_Indicator__c != accountCockpitOld.CRM360_Tech_Issues_Indicator__c){
                        accountCockpitNew.CRM360_Tech_Issues_Date__c = Date.today();
                    }
                }
            }
        }
        if(Trigger.isDelete){
            for(Account_Cockpit__c accountCockpitOld : trigger.old){
                if(accountCockpitOld.Record_Type_ID_ref__c == 'CMR360_Dashboard_Data'){
                    if(accountCockpitOld.Is_Record__c){
                        String profileId = UserInfo.getProfileId();
                        Profile profile = [select Name, Id, UserLicenseId from Profile where Id = :profileId];
                         
                        Account account = [SELECT Id, Name, OwnerId 
                                               FROM Account 
                                               WHERE Id = :accountCockpitOld.Account_Name__c];
                        
                        /*System.debug('Profile: '+profile.name);
                        
                        System.debug('OwnerId: '+account.OwnerId);
                        System.debug('UserId: '+UserInfo.getUserId());
                        
                        System.debug('Condição1: '+(profile.Name != 'Salesforce Administrator'));
                        System.debug('Condição2: '+(profile.Name != 'System Administrator'));
                        System.debug('Condição3: '+(account.OwnerId != UserInfo.getUserId()));
                        System.debug('Condição: '+(profile.Name != 'Salesforce Administrator' 
                               && profile.Name != 'System Administrator'
                               && account.OwnerId != UserInfo.getUserId()));*/
                        
                        if(profile.Name != 'Salesforce Administrator' 
                           && profile.Name != 'System Administrator'
                           && account.OwnerId != UserInfo.getUserId()
                          ){
                            accountCockpitOld.addError('Register can´t be deleted');
                        }
                    }
                }
            }
        }
    }

}