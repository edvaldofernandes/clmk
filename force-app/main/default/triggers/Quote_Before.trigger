/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on Quote
* NAME: Quote_Before.trigger
* AUTHOR: DPF                                                DATE: 09/12/2014
*
*******************************************************************************/

trigger Quote_Before on Quote (before delete, before insert, before update) {

  if(!TriggerUtils.isEnabled('Quote')) return;
  
  if (Trigger.isDelete) {
    QuoteDeleteRelatedAircraft.execute();
  }
}