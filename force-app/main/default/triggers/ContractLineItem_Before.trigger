/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on ContractLineItem
* NAME: ContractLineItem_Before.trigger
* AUTHOR: DPF                                                DATE: 09/12/2014
*
*******************************************************************************/

trigger ContractLineItem_Before on ContractLineItem (before delete, before insert, before update) {

  if(!TriggerUtils.isEnabled('ContractLineItem')) return;
  
  if (Trigger.isInsert) {
    ContractLineItemSearchSlot.newProducts();
    ContractCopyLineItem.execute();
  }
  else
  if (Trigger.isDelete) {
    ContractLineItemDeleteRelatedAircraft.execute();
  }
  else
  if (Trigger.isUpdate) {
    ContractLineItemSearchSlot.newProducts();
  }

}