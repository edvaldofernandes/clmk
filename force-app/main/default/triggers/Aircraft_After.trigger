/*******************************************************************************
*                              Cloud2b - 2014
*-------------------------------------------------------------------------------
* Acoes after no objeto Aircraft__c
*
* NAME: Aircraft_After.trigger
* AUTHOR: RLdO                                                 DATE: 27/05/2014
*******************************************************************************/
trigger Aircraft_After on Aircraft__c (after delete, after insert, after undelete, 
  after update) 
{
  if(!TriggerUtils.isEnabled('Aircraft__c')) return;
  
  if (Trigger.isInsert) {
    AircraftSetAccountCounters.processar();
  }
  else
  if (Trigger.isDelete) {
    AircraftSetAccountCounters.processar();
  }
  else
  if (Trigger.isUpdate) {
    AircraftSetAccountCounters.processar();
  }
  else
  if (Trigger.isUnDelete) {
    AircraftSetAccountCounters.processar();
  }
}