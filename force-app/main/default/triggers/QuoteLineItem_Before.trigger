/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on QuoteLineItem
* NAME: QuoteLineItem_Before.trigger
* AUTHOR: DPF                                                DATE: 09/12/2014
*
*******************************************************************************/

trigger QuoteLineItem_Before on QuoteLineItem (before delete, before insert, before update) {

  if(!TriggerUtils.isEnabled('QuoteLineItem')) return;
  
  if (Trigger.isInsert) {
    QuoteCopyLineItemFromOpp.execute();
    QuoteLineItemSearchSlot.newProducts();
  }
  else
  if (Trigger.isDelete) {
    QuoteLineItemDeleteRelatedAircraft.execute();
  }
  else
  if (Trigger.isUpdate) {
    QuoteLineItemSearchSlot.newProducts();
  }
  
}