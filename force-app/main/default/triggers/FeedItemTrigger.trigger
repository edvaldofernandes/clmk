trigger FeedItemTrigger on FeedItem ( after insert , before insert) {
    
    if( Trigger.isBefore ) {
        
        for( FeedItem feedToUpdate : Trigger.NEW ) {
            
            //feedToUpdate.CreatedById = '005i0000007kTgHAAU';
            
            if( feedToUpdate.Body == null )
                continue;
                        
            else if( feedToUpdate.Body.contains('<b>') || feedToUpdate.Body.contains('<u>') || feedToUpdate.Body.contains('<i>') )
                feedToUpdate.IsRichText = true;
        }
    }
    else if( Trigger.isAfter ) {
        
        String jsonFeedItemsIds = JSON.serialize( Trigger.newMap.keySet() );
        
        FeedItemHandler.onInsert( jsonFeedItemsIds );
    }
    
}