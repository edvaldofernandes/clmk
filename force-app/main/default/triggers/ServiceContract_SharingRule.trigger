/*******************************************************************************
*                               Embraer - 2015
*-------------------------------------------------------------------------------
*
* Trigger to create a sharing rule for service contract
* NAME: ServiceContract_SharingRule.trigger
* AUTHOR: Bruno de Oliveira Severino                           DATE: 07/04/2015
*
*******************************************************************************/
trigger ServiceContract_SharingRule on ServiceContract (after insert,before update) { 
    if (Trigger.isBefore){
        TriggerUtils.assertTrigger();
        List<Id> listSC= new List<Id>();
    	for ( ServiceContract sc : (list<ServiceContract>) trigger.new )
      		if ( TriggerUtils.wasChanged(sc, ServiceContract.Service_Program_Type__c) || TriggerUtils.wasChanged(sc, ServiceContract.OwnerId)) //only if the owner or service program has changed
                listSC.add(sc.Id);       
        
    	if ( !listSC.isEmpty() )  
        	delete [select id from ServiceContractShare where ParentId IN :listSC and RowCause = 'Manual'];
    }
    ServiceContract_SharingRule scSR = new ServiceContract_SharingRule();
    scSR.newSharingRule();
}