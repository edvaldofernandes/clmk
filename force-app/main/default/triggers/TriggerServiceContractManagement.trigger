/**
* @author Marcilio Leite de Souza
* @date 27/08/2018
* @description: Service Contract Management Trigger execute in handler TriggerHandlerServiceContractMgmt
* @reference: https://developer.secure.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers              
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                          Date                    Description
* ---------------                   -----------             ----------------------------------------------
* Marcilio Leite de Souza           27 AGO 2018             Original Version
**/
trigger TriggerServiceContractManagement on Service_Contract_Management__c (before update, before insert, after delete, after insert, after undelete, after update) {

     if(!GEN_TriggerHelper.isTriggerEnabled()) { 
        return ; 
    } 
    
    GEN_TriggerFactory.createhandler(Service_Contract_Management__c.sObjectType);  
}