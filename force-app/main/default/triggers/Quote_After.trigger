/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch After actions on Quote
* NAME: Quote_After.trigger
* AUTHOR: DPF                                                DATE: 17/12/2014
*
*******************************************************************************/
trigger Quote_After on Quote (after delete, after insert, after undelete, 
after update) {	
	 
  if(!TriggerUtils.isEnabled('Quote')) return;
  
  if (Trigger.isDelete) {
    QuoteUpdateSlot.delOpp();
  }
  else
  if (Trigger.isUpdate) {
  	QuoteUpdateSlot.closeOpp();
  }
  else
  if (Trigger.isUnDelete) {
  }

}