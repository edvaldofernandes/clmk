// Used by DIM - Market Intelligence.
trigger DIM_CaseTrigger on Case (before Insert, after Insert, before Update, after Update) {
    
    Id salesRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
    Id salesSupportRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sales_Support').getRecordTypeId();
        
    for (Case currentCase : Trigger.New) {
    
        if(currentCase.RecordTypeId == salesSupportRecordTypeId) {
        
            if(Trigger.isInsert && Trigger.isBefore) {

                Boolean isToBlockPending = (DIM_Case_Feedback_Settings__c.getInstance(Userinfo.getProfileId())).Is_To_Block_Pending__c;
                Id requesterId = (currentCase.Requested_By__c == null) ? currentCase.OwnerId : currentCase.Requested_By__c;
                
                Integer pendingFeedbacks = [SELECT count() FROM Feedback__c WHERE OwnerId =: requesterId AND Status__c = 'Pending'];
                if(pendingFeedbacks > 0 && isToBlockPending) {
                    currentCase.addError('Please go to the "Feedbacks" tab to review and submit your pending items before opening a new Case.');
                }
                else {                
                    if(currentCase.SuppliedEmail != null) {
                        List<User> users = [SELECT Id FROM User WHERE Email LIKE :currentCase.SuppliedEmail LIMIT 1];
                        if(users.size() > 0) {
                            currentCase.Requested_By__c = String.valueOf(users[0].Id);
                        }
                    }
                }
            }
                    
            else if(Trigger.isUpdate  && Trigger.isBefore) {
                    
                if(Trigger.oldMap.get(currentCase.Id).Status == 'Open' && currentCase.Status == 'In Progress' && String.isBlank(currentCase.Case_Folder_Id__c)) {
                    currentCase.Case_Folder_Id__c = getCaseFolderId(salesSupportRecordTypeId, null);
                }
            }
        }
        
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
        else if(currentCase.RecordTypeId == salesRecordTypeId) {
        
            if(Trigger.isInsert && Trigger.isBefore) {
                currentCase.Case_Folder_Id__c = getCaseFolderId(salesRecordTypeId, currentCase.Region_Consolidated__c);
            }
        }
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    
    @TestVisible
    private static String getCaseFolderId(Id recordTypeId, String region) {
        String currentYear = String.valueOf(System.Today().Year() - 2000);
        String currentYearFilter = currentYear + '%';
        
        AggregateResult result;
        if(region == null) {
            result = [SELECT MAX(Case_Folder_Id__c) FROM Case WHERE RecordTypeId = :recordTypeId AND Case_Folder_Id__c LIKE :currentYearFilter];
        }
        else {
            result = [SELECT MAX(Case_Folder_Id__c) FROM Case WHERE RecordTypeId = :recordTypeId AND Case_Folder_Id__c LIKE :currentYearFilter AND Region_Consolidated__c = :region];
        }           
                   
        // Caso seja o primeiro Case do ano.           
        if(String.isBlank(String.valueOf(result.get('expr0')))) {
            return currentYear + '0001';
        }
        // Para os demais Cases.
        else {
            Integer caseId = Integer.valueOf(result.get('expr0')) + 1;        
            return String.valueOf(caseId) ;
        }
    }
}