trigger SalesForceToEtrackAircraft on Aircraft__c (after insert, after update, after delete) {
    
    
    SalesForceAircraftInformationToEtrack sfAircraftInformationToEtrack = new SalesForceAircraftInformationToEtrack();
    
    if(Trigger.IsInsert || Trigger.IsUpdate)
        sfAircraftInformationToEtrack.informationAfterInsertOrUpdate(Trigger.new);
    
    if(Trigger.IsUpdate)
        sfAircraftInformationToEtrack.informationBeforeUpdate(Trigger.old);
    
    if(Trigger.IsDelete)
        sfAircraftInformationToEtrack.informationDelete(Trigger.old);
    
    /*if(Test.isRunningTest())
        return;
    
    List<AircraftInformation> aircraftInformationList = new List<AircraftInformation>();
    List<AircraftHistoryInformation> aircraftHistoryList = new List<AircraftHistoryInformation>();
        
    if(Trigger.IsInsert || Trigger.IsUpdate){
        
        Aircraft__c aircraftNew = Trigger.new[0];
        
        Account account = [SELECT id, flyEmbraerId__c FROM Account WHERE id = :aircraftNew.Owner__c];
        
        for(Aircraft__History aircraftHistory : RcpRepository.findAircraftHistoryByAircratId(aircraftNew.id)){
            
            String namePhoneAndEmail = RcpRepository.findUserByUserId(aircraftHistory.CreatedById);
            
            aircraftHistoryList.add(new AircraftHistoryInformation(aircraftHistory.CreatedDate, 
                                                                   aircraftHistory.Field,
                                                                   String.valueOf(aircraftHistory.OldValue),
                                                                   String.valueOf(aircraftHistory.NewValue),
                                                                   namePhoneAndEmail));
        }
        
        for(Aircraft__c aircraft : Trigger.new){     
            aircraftInformationList.add(new AircraftInformation(aircraft, 
                                                                account,
                                                                Constants.NEW_OBJECT_CREATED, 
                                                                Constants.STATUS_ACTIVE));
        }
    }
    
    if(Trigger.IsUpdate){
        
        Aircraft__c aircraftOld = Trigger.old[0];
        
        Account account = [SELECT id, flyEmbraerId__c FROM Account WHERE id = :aircraftOld.Owner__c];
        
        for(Aircraft__History aircraftHistory : RcpRepository.findAircraftHistoryByAircratId(aircraftOld.id)){
            
            String namePhoneAndEmail = RcpRepository.findUserByUserId(aircraftHistory.CreatedById);
            
            aircraftHistoryList.add(new AircraftHistoryInformation(aircraftHistory.CreatedDate, 
                                                                   aircraftHistory.Field,
                                                                   String.valueOf(aircraftHistory.OldValue),
                                                                   String.valueOf(aircraftHistory.NewValue),
                                                                   namePhoneAndEmail));
        }
        
        for(Aircraft__c aircraft : Trigger.old){
            
            
            aircraftInformationList.add(new AircraftInformation(aircraft, 
                                                                account,  
                                                                Constants.OLD_OBJECT_UPDATED_OR_DELETED, 
                                                                Constants.STATUS_ACTIVE));
        }
    }
    
    if(Trigger.IsDelete){
        
        Aircraft__c aircraftDelete = Trigger.old[0];
        
        Account account = [SELECT id, flyEmbraerId__c FROM Account WHERE id = :aircraftDelete.Owner__c];
        
        for(Aircraft__History aircraftHistory : RcpRepository.findAircraftHistoryByAircratId(aircraftDelete.id)){
            
            String namePhoneAndEmail = RcpRepository.findUserByUserId(aircraftHistory.CreatedById);
            
            aircraftHistoryList.add(new AircraftHistoryInformation(aircraftHistory.CreatedDate, 
                                                                   aircraftHistory.Field,
                                                                   String.valueOf(aircraftHistory.OldValue),
                                                                   String.valueOf(aircraftHistory.NewValue),
                                                                   namePhoneAndEmail));
        }
        
        for(Aircraft__c aircraft : Trigger.old){
            aircraftInformationList.add(new AircraftInformation(aircraft, 
                                                                account, 
                                                                Constants.OLD_OBJECT_UPDATED_OR_DELETED, 
                                                                Constants.STATUS_DELETED));
        }
    }
        
    Visitator visitator = new CallOutRepository();
    visitator.visit(aircraftInformationList, aircraftHistoryList,String.valueOf(AircraftInformation.class)); 

    RcpOutBoundProxy.executeAircraft();    
    SalesForceToEtrackProxy.executeAircraft();*/
    
}