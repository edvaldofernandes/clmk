/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch After actions on QuoteLineItem
* NAME: QuoteLineItem_After.trigger
* AUTHOR: DPF                                                DATE: 09/12/2014
*
*******************************************************************************/
trigger QuoteLineItem_After on QuoteLineItem (after delete, after insert, after undelete, 
after update)
{
  if(!TriggerUtils.isEnabled('QuoteLineItem')) return;
  
  if (Trigger.isInsert) {
  	QuoteLineItemCopyRelatedAircraft.execute();
  	LineItemSearchSlot.recalcSlots();
  }
  else
  if (Trigger.isUpdate) {
    LineItemSearchSlot.recalcSlots();
  }
  else
  if (Trigger.isDelete) {
    LineItemSearchSlot.recalcSlots();
    QuoteLineItemEntryFeeUpdateNrec.clearEntryFee();
  }
}