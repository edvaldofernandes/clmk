trigger ActionItem_Trigger on Action_Item__c(after insert) {
  Messaging.SingleEmailMessage message;
  List<Messaging.SendEmailResult> results;
  List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

  List<Id> userIds = new List<Id>();
  for (Action_Item__c ac : Trigger.new) {
    if (ac.Responsible__c != null) {
      userIds.add(ac.Responsible__c);
    }
    if (ac.Co_Responsible__c != null) {
      userIds.add(ac.Co_Responsible__c);
    }
  }

  Map<Id, String> userNames = new Map<Id, String>();

  for (User u : [SELECT Id, Name FROM User WHERE Id IN :userIds]) {
    userNames.put(u.Id, u.Name);
  }

  for (Action_Item__c ac : Trigger.new) {
    if (ac.Status__c == 'PENDING FEEDBACK') {
      message = new Messaging.SingleEmailMessage();
      List<Id> toAddresses = new List<Id>();
      String names;
      if (ac.Responsible__c != null) {
        toAddresses.add(ac.Responsible__c);
        names = userNames.get(ac.Responsible__c);
      }
      if (ac.Co_Responsible__c != null) {
        toAddresses.add(ac.Co_Responsible__c);
        names += ' and ' + userNames.get(ac.Co_Responsible__c);
      }

      message.toAddresses = toAddresses;
      message.subject = 'Action Item - ' + ac.Title__c;

      message.setReplyTo('fleet.reliability@embraer.net.br');
      message.setSenderDisplayName('Fleet Reliability');

      String dataUrl =
        Url.getSalesforceBaseUrl().toExternalForm() +
        '/lightning/r/Action_Item__c/' +
        ac.Id +
        '/view';
      String messageBody =
        '<p>Dear ' +
        names +
        '.</p>' +
        '<p>An action item related to the E2 Availability Process (Out-of-Service events\' analysis) was assigned to you. To access it, please click in the following link:<br />' +
        '<a href="' +
        dataUrl +
        '">Availability Process - Action item</a>';
      if (ac.Deadline__c != null) {
        messageBody += '<br />The deadline to this item is: ' + ac.Deadline__c;
      }
      messageBody +=
        '</p>' +
        '<p>Kind Regards,</p>' +
        '<div><b>Fleet Performance, Reliability & Economics</b><br />' +
        'Services & Support<br />' +
        'Embraer Commercial Aviation<br />' +
        'P: +55 12 3313 0416 / 3294 / 1009 / 3145 / 7926 / 2835<br />' +
        'fleet.reliability@embraer.net.br</div>';
      message.htmlBody = messageBody;
      messages.add(message);
    }
  }
  if (messages.size() > 0) {
    results = Messaging.sendEmail(messages);
  }

}