/*******************************************************************************
*                               Cloud2b - 2015
*-------------------------------------------------------------------------------
*
* Trigger to dispatch Before actions on Service_Contract_Management__c
* NAME: SvcContractManagement_Before.trigger
* AUTHOR: DPF                                                DATE: 29/01/2015
*
*******************************************************************************/
trigger SvcContractManagement_Before on Service_Contract_Management__c (before delete, before insert, before update) {
 
 if(!TriggerUtils.isEnabled('Service_Contract_Management__c')) return;

  if (Trigger.isInsert) {
    SCMCopyContractLineItemId.execute();
  }
  else
  if (Trigger.isDelete) {
  }
  else
  if (Trigger.isUpdate) {
    SCMCopyContractLineItemId.execute();
  }
}