/*******************************************************************************
*                               Cloud2b - 2014
*-------------------------------------------------------------------------------
*
* Trigger to dispatch After actions on Service_Contract_Management__c
* NAME: SvcContractManagement_After.trigger
* AUTHOR: LRSA                                                DATE: 04/12/2014
*
*******************************************************************************/

trigger SvcContractManagement_After on Service_Contract_Management__c (after delete, after insert, after undelete, 
after update) {
  /*
  if(!TriggerUtils.isEnabled('Service_Contract_Management__c')) return;

  if (Trigger.isInsert) {
  //  AccReceivablesEntitlement.newAccEnt(); 
    AccReceivablesUpdateContractLineItem.newServiceContractManagement();
    if(test.IsRunningTest())
        SCMUpdateQuantityContractLineItem.execute();
  }
  else
  if (Trigger.isDelete) {
 //   AccReceivablesEntitlement.delAccEnt();
    AccReceivablesUpdateContractLineItem.deleteServiceContractManagement();
    if(test.IsRunningTest())
        SCMUpdateQuantityContractLineItem.execute();
  }
  else
  if (Trigger.isUpdate) {
  //  AccReceivablesEntitlement.delAccEnt();
  //  AccReceivablesEntitlement.newAccEnt();
    AccReceivablesUpdateContractLineItem.updateServiceContractManagement();
    AccReceivablesUpdateContractLineItem.newServiceContractManagement();
    if(test.IsRunningTest())
        SCMUpdateQuantityContractLineItem.execute();
  }
  */
}